/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.common.StateDefinitionMessageParser.StateDefinitionMessage;
import ru.omnicomm.pegasus.messaging.processing.TimerParser;
import ru.omnicomm.pegasus.messaging.settings.statedefinition.StateDefinitionSettingsParser.StateDefinitionMessageSettings;
import ru.omnicomm.pegasus.messaging.settings.statedefinition.StateDefinitionSettingsParser.StateDefinitionMessageBuffer;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер определения парковкм;
 * реализация согласно требованиям TTP-PSS-01-(Parking Server)
 * @author khotyan
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("StateDefinitionServerImplementation")
public class StateDefinitionServer implements ServerImplementation {

    private final int blockId = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private long timeInterval;
    private long theTime;
    private StateDefinitionBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public StateDefinitionServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {

        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
        //таймерное сообщение
//        if(header.getMessageType()== TimerParser.Timer.getDefaultInstance().getMessageType()){
//            processTimerMessage(message);
//        }
        //обычное сообщение

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(StateDefinitionServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }

    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;
            int[] types;

            //считывание типов сообщений из настроек:
            try {
                //инициализация таймера
                //Сервер подписывается на таймерные сообщения. Период таймера равен значению настройки настройки Задержка Обработки
                String[] tintSettings;
                tintSettings = this.settingsService.get(blockId, "SRV_SYNC_TIMEOUT");
                if(tintSettings==null || tintSettings.length==0){
                    LOGGER.log(Level.ERROR, "Can't read SRV_SYNC_TIMEOUT settings");
                }
                this.timeInterval = Long.parseLong(tintSettings[0]);
                LOGGER.log(Level.DEBUG, "SRV_SYNC_TIMEOUT settings: "+tintSettings[0]);

                Handler handler = initSignal.getHandler();
                handler.setTimer("timer0", Handler.TimerType.RATE_BASED, 1000L, timeInterval);

                types = getMessageTypesFromSettings();
                router.registerServer(StateDefinitionServer.class, self, types);
            } catch (SettingsStorageServiceException e) {
                LOGGER.log(Level.ERROR, "Can't read message types from settings", e);
            } catch (NumberFormatException e) {
                LOGGER.log(Level.ERROR, "Can't parse message types from settings", e);
            }

        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(StateDefinitionServer.class, self);
        } else if (signal instanceof TimerEvent) { // Входящее таймерное сообщение
            TimerEvent timerEvent = (TimerEvent) signal;
            LOGGER.debug("TimerMessage received. Id= "+timerEvent.id());
            processTimerMessage(timerEvent);
        }
    }

    /**
     * Возвращает массив типов сообщений из настроек,
     * на которые следует подписаться
     * @return
     * @throws SettingsStorageServiceException
     */
    private int[] getMessageTypesFromSettings() throws SettingsStorageServiceException {
        int[] types = null;
            String[] messageTypes = this.settingsService.get(blockId, "SRV_STATES_INBOUND_MESSAGE");
            if(messageTypes==null || messageTypes.length==0){
                LOGGER.log(Level.ERROR, "Message types from settings is null");
            }
            else{
                types = new int[messageTypes.length];
                int i = 0;
                //подписываемся на сообщения из настроек
                for(String messageType : messageTypes){
                    types[i] = Integer.parseInt(messageType);
                    i++;
                }
            }
        return types;
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processTimerMessage(TimerEvent timerEvent) {
        try {
            int[] types = getMessageTypesFromSettings();
            router.unregisterServer(StateDefinitionServer.class, self);
            router.registerServer(StateDefinitionServer.class, self, types);

            long t = System.currentTimeMillis();
            this.currentBuffer.processTimerData(router,jmsBridgeService,dataStorageService,t,blockId);

        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read message types from settings", e);
        }
    }

    /**
     * Выполняется после загрузки настроек при получении сообщения
     * @param message
     * @throws Exception
     */
    private void processMessage(MessageLite message) throws Exception {
        StateDefinitionMessage header = null;

        byte[] messageBody = message.toByteArray();
        try {
            LOGGER.log(Level.DEBUG, "Process message");
            header = StateDefinitionMessage.parseFrom(message.toByteArray());

        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
        //Сервер подписывается на таймерные сообщения. Период таймера равен значению настройки настройки Задержка Обработки
        String[] tintSettings;
        tintSettings = this.settingsService.get(header.getMessageBlock(), "SRV_SYNC_TIMEOUT");
        if(tintSettings==null || tintSettings.length==0){
            LOGGER.log(Level.ERROR, "Can't read SRV_SYNC_TIMEOUT settings");
        }
        this.timeInterval = Long.parseLong(tintSettings[0]);
        LOGGER.log(Level.DEBUG, "SRV_SYNC_TIMEOUT settings: "+tintSettings[0]);

        //	Сервер считывает значение (TCur) настройки Текущий момент обработки
        String[] thetimeSettings = this.settingsService.get(header.getMessageBlock(), "SRV_STATES_TIMEPOINT");
        if(thetimeSettings==null || thetimeSettings.length==0){
            LOGGER.log(Level.ERROR, "Can't read SRV_STATES_TIMEPOINT settings");
        }
        LOGGER.log(Level.DEBUG, "SRV_STATES_TIMEPOINT settings: "+thetimeSettings[0]);
        this.theTime = Long.parseLong(thetimeSettings[0]);

        if (header.getSourceId() != this.currentSourceId) {
            if (this.currentBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(this.theTime, this.timeInterval), this.getBufferName((int)header.getSourceId()));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentBuffer = this.getBuffer((int)header.getSourceId());
            if (currentBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(header.getSourceId())
                        .append(" wasn't created. All data, received from this process, will be ignored by the StateDefinitionServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentBuffer == null) {
            return;
        }

        this.currentBuffer.processData(router,jmsBridgeService,dataStorageService,header,timeInterval);
    }

    private String getBufferName(int sourceId) {
        return String.format("src%d:movementStates:buffer", sourceId);
    }

    private String getSettingsName(int sourceId) {
        return String.format("src%d:movementStates:settings", sourceId);
    }

    /**
     * Получает нужный буффер по processId
     * @param sourceId
     * @return
     */
    private StateDefinitionBuffer getBuffer(int sourceId) {
        StateDefinitionMessageSettings settings;
        StateDefinitionMessageBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for sourceId %d wasn't found", sourceId));
                return null;
            }
            settings = StateDefinitionMessageSettings.parseFrom(settingsObject.toByteArray());

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for sourceId %d", sourceId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for sourceId %d wasn't found", sourceId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getBufferName(sourceId));
            if(settingsObject!=null){
                buffer = StateDefinitionMessageBuffer.parseFrom(settingsObject.toByteArray());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for sourceId %d", sourceId), ex);
        }

        if (buffer == null) {
            return new StateDefinitionBuffer(settings, this.settingsService);
        } else {
            return new StateDefinitionBuffer(settings, this.settingsService, buffer);
        }
    }
}
