/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.util.*;

import ru.omnicomm.pegasus.messaging.common.StateDefinitionMessageParser.StateDefinitionMessage;
import ru.omnicomm.pegasus.messaging.online.COState.COStateParser;
import ru.omnicomm.pegasus.messaging.settings.statedefinition.StateDefinitionSettingsParser.StateDefinitionMessageSettings;
import ru.omnicomm.pegasus.messaging.settings.statedefinition.StateDefinitionSettingsParser.StateDefinitionMessageBuffer;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;


/**
 * Буфер алгоритма определения состояний по движению
 * @author alexander
 */
public class StateDefinitionBuffer {
    private final int blockId = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private SettingsStorageService settingsService;

    private final long sourceId;

    private final List<StateDefinitionMessageBuffer.Data> buffer;
    private final Comparator<StateDefinitionMessageBuffer.Data> bufferByMessageTimeComparator;
    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public StateDefinitionBuffer(StateDefinitionMessageSettings settings, SettingsStorageService settingsService) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.settingsService = settingsService;

        this.buffer = new ArrayList<StateDefinitionMessageBuffer.Data>();
        this.bufferByMessageTimeComparator = new Comparator<StateDefinitionMessageBuffer.Data>() {

            @Override
            public int compare(StateDefinitionMessageBuffer.Data obj1, StateDefinitionMessageBuffer.Data obj2) {
                return ((Long)obj1.getTimeStamp()).compareTo((Long)obj2.getTimeStamp());
            }
        };

    }

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public StateDefinitionBuffer(StateDefinitionMessageSettings settings, SettingsStorageService settingsService,
                                 StateDefinitionMessageBuffer buffer) throws IllegalArgumentException {

        this(settings, settingsService);
        if(settings.hasTime() == buffer.hasTime() &&
           settings.getTimeInterval() == buffer.getTimeInterval()){

            this.buffer.addAll(buffer.getBufferList());

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(long theTime, long timeInterval){

        StateDefinitionMessageBuffer.Builder builder =
                StateDefinitionMessageBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(StateDefinitionMessageBuffer.getDefaultInstance().getMessageType())
                .setTime(theTime)
                .setTimeInterval(timeInterval)
                .addAllBuffer(this.buffer);

        
        return builder.build();

    }

    /**
     * Выполняет обработку входных данных от таймера согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param t время
     */
    public void processTimerData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,
                            long t, int messageBlockId) throws InvalidProtocolBufferException {
        try {
            //3)	Сервер считывает из сообщения значение времени (T)
//            long t = header.getTimeStamp();

            //2)	Сервер считывает значение (Tint) настройки Задержка
            String[] timeInterval = getSettings(blockId, "SRV_SYNC_TIMEOUT");
            if(timeInterval==null)
                return;
            long timeIntervalL = Long.parseLong(timeInterval[0]);
            //4)	Сервер считывает значение (TCur) настройки Текущий момент обработки
            String[] tcur = getSettings(blockId, "SRV_STATES_TIMEPOINT");
            if(tcur==null)
                return;

            //5)	Сервер инициализирует пустой массив M
            ArrayList<M> mList = new ArrayList<M>();

            //6)	Сервер выполняет 5.3 Выполнение действий по таймеру передавая туда TCur
            timerDataExecutor(tcur[0], timeInterval[0], mList);
            //добавим в лог информацию о полученном массиве М
            LOGGER.log(Level.DEBUG, "M array after initializing in 5.3:");
            for(M m : mList)
                LOGGER.log(Level.DEBUG, "M:"+ m.toString());

            //7)	Обработка сообщений из буфера
            Long tm = 0L;
            if(buffer.size()!=0){
                //a.	Сервер находит в буфере сообщение с максимальным timestamp считывает timestamp этого сообщения Tm
                tm = buffer.get(buffer.size()-1).getTimeStamp();
            } else {
                LOGGER.log(Level.DEBUG, "buffer size is null");
            }
            //b.	Сервер выбирает из буфера задержки все сообщения, c timestamp, меньшим, чем Tm – Tint
            ArrayList<StateDefinitionMessageBuffer.Data> tempBufferList = new ArrayList<StateDefinitionMessageBuffer.Data>();
            for(StateDefinitionMessageBuffer.Data bufferItem : buffer){
                if(bufferItem.getTimeStamp()<tm-timeIntervalL){
                    LOGGER.log(Level.DEBUG, "Add to temporary buffer: "+bufferItem.getTimeStamp());
                    tempBufferList.add(bufferItem);
                }
            }
            //c.	Сервер идёт по выбранным на предыдущем шаге сообщениям, сортирует их по возрастанию timestamp
            Collections.sort(tempBufferList, this.bufferByMessageTimeComparator);
            // и затем для каждого из них (последовательно от первого к последнему) выполняет 5.3 Обработка сообщения из буфера

            for(StateDefinitionMessageBuffer.Data bufferItem : buffer){
                bufferDataExecutor(tcur[0], timeInterval[0], mList, bufferItem);
            }

            //todo: d.	Сервер исключает из буфера все обработанные сообщения
            this.buffer.removeAll(buffer);

            //e.	Сервер устанавливает значение Текущий момент обработки равным = Tm – Tint
            Long theTimeL = tm-timeIntervalL;
            LOGGER.log(Level.DEBUG, "Tcur: "+theTimeL);

            //8)	Группируем в массиве M все записи по COid.
            HashMap<String, ArrayList<M>> mListsByCOId = new HashMap<String, ArrayList<M>>();
            for(M m : mList){
                String coId = m.coId;
                if(mListsByCOId.containsKey(coId)){
                    mListsByCOId.get(coId).add(m);
                }
                else{
                    ArrayList<M> newMList = new ArrayList<M>();
                    newMList.add(m);
                    mListsByCOId.put(coId, newMList);
                }
            }
            //Идём по всем различным COid и для каждого из них:
            for (Map.Entry<String, ArrayList<M>> entryCOId : mListsByCOId.entrySet()) {
                //a.	Выбираем все записи для данного COId c признаком EventType = 0
                ArrayList<M> mCOIdList = entryCOId.getValue();
                //b.	Группируем выбранные записи по полю Time
                // (сразу выбираем и для EventType = 1)
                HashMap<Long, ArrayList<Integer>> mListsByTime0 = new HashMap<Long, ArrayList<Integer>>();
                HashMap<Long, ArrayList<Integer>> mListsByTime1 = new HashMap<Long, ArrayList<Integer>>();
                for(M m : mCOIdList){
                    Long time = m.time;
                    if(m.eventType.equals("0")){
                        if(mListsByTime0.containsKey(time)){
                            mListsByTime0.get(time).add(Integer.parseInt(m.coStateId));
                        } else {
                            ArrayList<Integer> newMList = new ArrayList<Integer>();
                            newMList.add(Integer.parseInt(m.coStateId));
                            mListsByTime0.put(time, newMList);
                        }
                    }
                    else {
                        if(mListsByTime1.containsKey(time)){
                            mListsByTime1.get(time).add(Integer.parseInt(m.coStateId));
                        } else {
                            ArrayList<Integer> newMList = new ArrayList<Integer>();
                            newMList.add(Integer.parseInt(m.coStateId));
                            mListsByTime1.put(time, newMList);
                        }
                    }
                }
                //c.	Идём по всем различным значениям поля Time, определённым на предыдущем шаге, и для каждого из них (Обозн. Tx)
                //i.	Сервер формирует направляет на сервисы JMS, Router и сохраняет в хранилище следующее сообщение
                for(Map.Entry<Long, ArrayList<Integer>> entryTime : mListsByTime0.entrySet()){
                    this.sendToServices(router, jmsBridgeService, dataStorageService,
                            this.getCOStateONMessage(Long.parseLong(entryCOId.getKey()), entryTime.getKey(), entryTime.getValue(),messageBlockId));
                }
                for(Map.Entry<Long, ArrayList<Integer>> entryTime : mListsByTime1.entrySet()){
                    this.sendToServices(router, jmsBridgeService, dataStorageService,
                            this.getCOStateOFFMessage(Long.parseLong(entryCOId.getKey()), entryTime.getKey(), entryTime.getValue(),messageBlockId));
                }
            }
        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read data from settings", e);
        } catch (Exception e) {
            LOGGER.log(Level.ERROR,  e.getMessage());
        }
    }

    //5.3	Обработка сообщения из буфера
    private void bufferDataExecutor(String theTime, String timeInterval,
                                   ArrayList<M> mList, StateDefinitionMessageBuffer.Data bufferItem)
                                                                                throws SettingsStorageServiceException {
        LOGGER.log(Level.DEBUG, "Execute bufferItem by algorithm 5.3 "+bufferItem.toString());
        //1)	Сервер считывает из заголовка сообщения источник полученного сообщения (SrcID),
        // временной штамп полученного сообщения (Т), а также тип сообщения (MsgType).
        Long msgType = bufferItem.getMessageType();
        Long t = bufferItem.getTimeStamp();
        Long tIntervalL = Long.parseLong(timeInterval);

        //2)	Сервер определяет текущее состояние для всех различных автоматов
        //a.	Сервер считывает массив MachineArray различных автоматов для данного SourceId из настроек
        String[] machineIds = getSettings(blockId, "MEALY_MACHINE-" + this.sourceId);
        if(machineIds==null)
            return;
        //b.	Для каждого из настроек считывается текущее состояние и добавляется в массив MState.
        // Если настройка не найдена, в массив добавляется значение 0
        ArrayList<MState> mStatesList = new ArrayList<MState>();
        for(String machineId : machineIds){
            LOGGER.log(Level.DEBUG, "MachineId from MEALY_MACHINE settings: "+machineId);
            String[] machineState = getSettings(blockId, "MEALY_MACHINESTATE-" + machineId + "-" + this.sourceId);
            MState mstate = new MState(machineId);
            if(machineState == null){
                mstate.stateId = "0";
            }
            else {
                mstate.stateId = machineState[0];
            }
            mStatesList.add(mstate);
        }
        //3)	Сервер идёт по массиву состояний MState и для каждого из них (Обозн. MStateId):
        for(MState mstate : mStatesList){
            String state2;
            String conditionId;
            String isNegation;

            ArrayList<Condition> conditionList = new ArrayList<Condition>();

            //a.	Сервер считывает значение настройки перехода из этого состояния для данного типа сообщения
            // и соответствующего состоянию автомата по ключу MEALY_TRANSITION–<MsgType>–<SrcID>–<MachineId>–<id текущего состояния>
            //i.	key = MEALY_TRANSITION-<MType>-<SourceId>-<MachineId>–<State1>
            //ii.	value = <State2>-<ConditionId>-<IsNegation>
            String key = "MEALY_TRANSITION-"+msgType+"-"+sourceId+"-"+mstate.machineId+"–"+mstate.stateId;
            String[] statesSettings = getSettings(blockId, key);
            //b.	Если настройка найдена
            if(statesSettings != null) {
                //i.	Считываем из значения настройки State2, ConditionId, IsNegation
                String[] statesSettingsValues = statesSettings[0].split("-");
                if(statesSettingsValues == null || statesSettingsValues.length<3){
                    LOGGER.log(Level.ERROR, "Can't parse MEALY_TRANSITION settings. Value= "+statesSettings[0]);
                    continue;
                }
                LOGGER.log(Level.DEBUG, "MEALY_TRANSITION settings: "+statesSettingsValues[0]);
                state2 = statesSettingsValues[0];
                conditionId = statesSettingsValues[1];
                isNegation = statesSettingsValues[2];

                //ii.	Считываем из настроек компоненты условия по ключу MEALY_CONDITION-<ConditionId>
                //i.	key = MEALY_CONDITION-<ConditionId>
                //ii.	value = <FieldType>-<FieldValueIndex>-<OperatorId>-<ComprValue>
                String[] conditionSettings = getSettings(blockId, "MEALY_CONDITION-" + conditionId);
                if(conditionSettings != null){
                    for(String conditionSetting : conditionSettings){
                        String[] conditionSettingsValues = conditionSetting.split("-");
                        if(conditionSettingsValues == null || conditionSettingsValues.length<4){
                            LOGGER.log(Level.ERROR, "Can't parse MEALY_CONDITION settings. Value= "+conditionSettingsValues[0]);
                            continue;
                        }
                        LOGGER.log(Level.DEBUG, "MEALY_CONDITION settings: "+conditionSettingsValues[0]);
                        Condition condition = new Condition(conditionSettingsValues);
                        conditionList.add(condition);
                    }
                }

                //iii.	Присваиваем переменной CondResult = True
                boolean condResult = true;
                //iv.	Для каждого из компонентов полученных на шаге ii (если таких компонентов нет, пропускаем данный шаг):
                for(Condition condition : conditionList){
                    //1.	Считываем из тела сообщения поле FieldType
                    String fieldType = condition.fieldType;
                    //2.	Если FieldValueIndex > 0, считываем из поля соответствующее порядковое значение
                    //3.	Выполняем операцию сравнения, заданную параметром OperatorId для значения определённого
                    // на шаге 2 и параметра ComprValue (результат операции обозн. Resalt1)
                    boolean result1 = compareOperation(condition, bufferItem);

                    //4.	Если NOT Result1, устанавливаем CondResult = (IsNegation = 1) и переходим на следующий шаг v (прерываем цикл).
                    if(!result1){
                        condResult = true; //todo так ли это?
                        isNegation = "1";
                        break;
                    }

                }
                //v.	Если CondResult = True, то
                if(condResult){
                    //1.	Сервер сохраняет новое состояние State2 для текущего автомата (соответствие установлено на шаге 2) и SourceId
                    //i.	key = MEALY_MACHINESTATE-<MachineID>-<SourceId>
                    //ii.	value = <MachineStateId>
                    String key1 = "MEALY_MACHINESTATE-"+mstate.machineId+"-"+this.sourceId;
                    String value1 = state2;
                    LOGGER.log(Level.DEBUG, "Add  settings. Key= "+key1+" Value: "+value1);
                    settingsService.add(blockId, key1, value1);

                    //2.	Сервер считывает из настроек таймаут (Tout) и новое состояние (State3id) для состояния State2 по соответствующему ключу
                    //i.	Key = MACHINESTATE_TIMEOUT–<MachineID>–<StateId>
                    //ii.	Value = <TheTimeout>-<NewStateId>
                    String timeoutKey = "MACHINESTATE_TIMEOUT-"+mstate.machineId+"-"+state2;
                    String[] machineStateTimeout = getSettings(blockId, timeoutKey);
                    if(machineStateTimeout == null ){
                        break;
                    }
                    String[] timeoutStatePair = machineStateTimeout[0].split("-");
                    if(timeoutStatePair == null || timeoutStatePair.length<2){
                        LOGGER.log(Level.ERROR, "Can't parse MACHINESTATE_TIMEOUT from settings. Value= "+machineStateTimeout[0]);
                    }
                    //3.	Если настройка найдена и T>0
                    else {
                        Long tout = Long.parseLong(timeoutStatePair[0]);
                        String state3id = timeoutStatePair[1];
                        if(t>0){
                            //a.	Удаляем старый таймер
                            //i.	Key = MACHINESTATE_TIMER_REF-<MachineID>-<SourceId>
                            //ii.	Value = <SettingId>
                            String key2 = "MACHINESTATE_TIMER_REF-"+mstate.machineId+"-"+this.sourceId;
                            settingsService.delete(blockId, key2);

                            //b.	Сервер вычисляет T1 = T + Tout
                            Long t1 = t + tout;

                            //c.	Сервер округляет T1 в большую сторону так, чтобы полученная величина делилась нацело на значение настройки Задержка обработки
                            while(t1%tIntervalL!=0){
                                t1++;
                            }

                            //d.	Сервер сохраняет таймер состояния для времени T1 состояния State2 автомата MachineId источника SourceId и нового состояния State3Id
                            //i.	Key = MACHINESTATE_TIMER-<TheTime>
                            //ii.	Value = <MachineId>-<SourceId>-<State1>-<State2>
                            String key3 = "MACHINESTATE_TIMER-"+t1;
                            String value3 = mstate.machineId+"-"+this.sourceId+"-"+state2+"-"+state3id;
                            LOGGER.log(Level.DEBUG, "Put into MACHINESTATE_TIMEOUT settings. Key= "+key3+" Value= "+value3);
                            settingsService.add(blockId, key3, value3);

                            //i.	Key = MACHINESTATE_TIMER_REF-<MachineID>-<SourceId>
                            //ii.	Value = <SettingId>
                            String key4 = "MACHINESTATE_TIMER_REF-"+mstate.machineId+"-"+this.sourceId;
                            String value4 = key3;
                            LOGGER.log(Level.DEBUG, "Put into MACHINESTATE_TIMER_REF settings. Key= "+key4+" Value= "+value4);
                            settingsService.add(blockId, key4, value4);

                        }
                    }
                    //4.	Сервер считывает настройки формирования исходящих сообщений для соответствующего состоянию автомата и перехода из MStateId в State2
                    String[] machineMessage = getSettings(blockId, "MACHINEMESSAGE-" + mstate.machineId + "-" + mstate.stateId + "-" + state2);
                    if(machineMessage == null ){
                        return;
                    }
                    //5.	Если на предыдущем шаге найдена настройка сервер добавляет соответствующую запись во входящий массив M.
                    String[] machineMessageSettings = machineMessage[0].split("-");
                    if(machineMessageSettings == null || machineMessageSettings.length<3){
                        LOGGER.log(Level.ERROR, "Can't parse MACHINEMESSAGE from settings. Value= "+machineMessage[0]);
                        return;
                    }
                    LOGGER.log(Level.DEBUG, "Add to M: "+machineMessage+", t= "+t);
                    mList.add(new M(machineMessageSettings, t));
                }

            }

        }
    }

    public boolean compareOperation(Condition condition, StateDefinitionMessageBuffer.Data bufferItem){
        List valueList = null;
        Object comprValue = null;

        if (condition.fieldType.equals("int32")){
            comprValue = (Integer)condition.comprValue;
            valueList = bufferItem.getIntValueList();
        } else if(condition.fieldType.equals("int64")){
            comprValue = (Long)condition.comprValue;
            valueList = bufferItem.getLongValueList();
        } else if(condition.fieldType.equals("bool")){
            comprValue = (Boolean)condition.comprValue;
            valueList = bufferItem.getBooleanValueList();
        } else if(condition.fieldType.equals("double")){
            comprValue = (Double)condition.comprValue;
            valueList = bufferItem.getDoubleValueList();
        } else {
            LOGGER.log(Level.DEBUG, "Cannot find this field type: "+condition.fieldType);
        }
        //FieldValueNumber – порядковый номер элемента массива, из которого необходимо считать значение для сравнения.
        // Целое неотрицательное число. Если равно 0, то значит, что условие применяется целиком к массиву
        if(!valueList.isEmpty() && condition.fieldValueIndex>0){
            List list = new ArrayList();
            list.add(valueList.get(condition.fieldValueIndex));
            return compare(list, comprValue, condition.operatorId);
        } else if(!valueList.isEmpty() && condition.fieldValueIndex==0){
            return compare(valueList, comprValue, condition.operatorId);
        } else {
            LOGGER.log(Level.ERROR, "Cannot compare. Condition: "+condition.toString());
            return true;
        }
    }

    public boolean compare(List objList, Object obj2, int operationId){
        boolean result = true;
        switch(operationId){
            //	содержит (OperatorId = 1). Среди массива значений в поле содержится заданное
            case 1:
                result = objList.contains(obj2);
                break;
            //	не содержит (OperatorId = 2). Ни одно из значений массива в поле не совпадает с заданным
            case 2:
                result = !objList.contains(obj2);
                break;
            //	равно (OperatorId = 3). Точное совпадение с зданным значением
            case 3:
                result = objList.get(0).equals(obj2);
                break;
            //	не равно (OperatorId = 4). В поле ровно одно значение и оно НЕ равно заданному
            case 4:
                result = !objList.get(0).equals(obj2);
                break;
            //	больше. В поле ровно одно значение и оно строго больше заданного (OperatorId = 5).
            case 5:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0)> (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0)> (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0)> (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
            //	меньше либо равно (OperatorId = 6). В поле ровно одно значение и оно меньше либо равно заданного
            case 6:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0) <= (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0) <= (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0) <= (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
            //	меньше (OperatorId = 7). В поле ровно одно значение и оно строго меньше заданного
            case 7:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0) < (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0) < (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0) < (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
            //	больше либо равно (OperatorId = 8). В поле ровно одно значение и оно больше либо равно заданного.
            case 8:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0) >= (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0) >= (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0) >= (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
        }
        return result;
    }

    private String[] getSettings(int blockId, String key) throws SettingsStorageServiceException {
        String[] settings = settingsService.get(blockId, key);
        if(settings==null || settings.length==0){
            LOGGER.log(Level.DEBUG, "Can't read "+key+" from settings");
            return null;
        }
        LOGGER.log(Level.DEBUG, "Key: "+key);
        for(String setting : settings){
            LOGGER.log(Level.DEBUG, "setting: "+setting);
        }
        return settings;
    }

    //5.2	Выполнение действий по таймеру
    private void timerDataExecutor(String theTime, String timeInterval,
                                   ArrayList<M> mList) throws SettingsStorageServiceException {
        Long timeIntervalL = Long.parseLong(timeInterval);
        Long theTimeL = Long.parseLong(theTime);
        //1)	Сервер считывает настройки текущих таймеров состояний по ключу MACHINE_TIMER-<TTime>
        //	Key = MACHINESTATE_TIMER-<TheTime>
        //	Value = <MachineId>-<SourceId>-<State1>-<State2>
        String[] timeoutStates = getSettings(blockId, "MACHINESTATE_TIMER-" + theTime);
        if(timeoutStates==null)
            return;

        //2)	Для каждой из полученных настроек:
        //a.	Сервер считывает параметры MachineId, State1, State2, SourceId из значения соответствующей настройки.
        ArrayList<MachineStateTimer> machineStateTimersList = getMachineStateTimers(timeoutStates);
        //b.	Сервер сохраняет новое состояние State2 для автомата MachineID и SourceId

        for(MachineStateTimer machineStateTimer : machineStateTimersList){
            LOGGER.log(Level.DEBUG, "machineStateTimer: "+machineStateTimer.toString());
            //	Текущее состояние автомата
            String[] currentState = getSettings(blockId, "MEALY_MACHINESTATE-" + machineStateTimer.machineId + "-" + machineStateTimer.sourceId);
            if(currentState == null){
                return;
            }
            machineStateTimer.machineStateId = currentState[0];

            //c.	Сервер считывает из настроек таймаут (T) и новое состояние (NewStateId) для состояния State2 по соответствующему ключу
            //i.	Key = MACHINESTATE_TIMEOUT–<MachineID>–<StateId>
            //ii.	Value = <TheTimeout>-<NewStateId>
            String[] machineStateTimeout = getSettings(blockId, "MACHINESTATE_TIMEOUT-" + machineStateTimer.machineId + "-" + machineStateTimer.state2);
            if(machineStateTimeout == null){
                return;
            }
            String[] timeoutStatePair = machineStateTimeout[0].split("-");
            if(timeoutStatePair == null || timeoutStatePair.length<2){
                LOGGER.log(Level.ERROR, "Can't parse MACHINESTATE_TIMEOUT from settings. Value= "+machineStateTimeout[0]);
                return;
            }
            machineStateTimer.timeout = timeoutStatePair[0];
            machineStateTimer.newState2 = timeoutStatePair[1];

            //d.	Если T>0, то
            if(Long.parseLong(machineStateTimer.timeout)>0){
                //i.	Сервер вычисляет T1 = TTime + T
                long t1 = Long.parseLong(machineStateTimer.timeout)+Long.parseLong(theTime);
                //ii.	Сервер округляет T1 в большую сторону так, чтобы полученная величина делилась нацело на значение настройки Задержка обработки
                while(t1%timeIntervalL!=0){
                    t1++;
                }
                //iii.	Сервер сохраняет таймер состояния для времени T1 состояния State2 автомата MachineId источника SourceId и нового состояния NewStateId
                //i.	Key = MACHINESTATE_TIMER-<TheTime>
                //ii.	Value = <MachineId>-<SourceId>-<State1>-<State2>
                String key = "MACHINESTATE_TIMER-"+t1;
                String value = machineStateTimer.machineId+"-"+machineStateTimer.sourceId+"-"+
                        machineStateTimer.state2+"-"+machineStateTimer.newState2;
                settingsService.add(blockId, key, value);
                LOGGER.log(Level.DEBUG, "Save settings.  Key: "+key+" Value= "+value);
            }

            //e.	Сервер считывает настройки формирования исходящих сообщений для автомата MachineID и состояний State1 и State2
            //a.	Key = MACHINEMESSAGE-<MachineID>–<State1>-<State2>
            //b.	Value = <EventType>-<COStateId>-<COId>
            String[] machineMessage = getSettings(blockId, "MACHINEMESSAGE-" + machineStateTimer.machineId + "-"
                    + machineStateTimer.state1 + "-" + machineStateTimer.state2);
            if(machineMessage == null)
                return;

            //f.	Если на предыдущем шаге найдена настройка:
            //i.	Сервер добавляет соответствующую запись во входящий массив M (timestamp TTime).
            String[] machineMessageSettings = machineMessage[0].split("-");
            if(machineMessageSettings == null || machineMessageSettings.length<3){
                LOGGER.log(Level.ERROR, "Can't parse MACHINEMESSAGE from settings. Value= "+machineMessage[0]);
                return;
            }
            mList.add(new M(machineMessageSettings, theTimeL));

            //3)	Сервер удаляет все настройки по ключу MACHINE_TIMER-<TTime>
            String key = "MACHINESTATE_TIMER-"+theTime;
            settingsService.delete(blockId, key);
        }
    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param header тело сообщения
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,
                            StateDefinitionMessage header, long timeInterval) throws InvalidProtocolBufferException {

        try {
            int messageBlockId = header.getMessageBlock();
            //1)	Сервер считывает из сообщения значение временного штампа (T) из тела сообщения
            //2)	Сервер считывает значение (TCur) настройки Текущий момент обработки
            //3)	Если T < TCur, то
            if(header.getTimeStamp()<timeInterval){
                //a.	Сервер формирует, сохраняет в хранилище, а также направляет в сервис JMS и Router сообщение о сбое в обработке состояний
                String[] coIdSettings = getSettings(header.getMessageBlock(), "COID-" + header.getSourceId());
                if(coIdSettings==null){
                    return;
                }
                int coId = Integer.parseInt(coIdSettings[0]);
                int errorCode = 3; //сбой из-за рассинхронизации обработки маршрутов
                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getCOStateFailureMessage(coId, header.getTimeStamp(), errorCode, messageBlockId));
            } else {
                //a.	Сервер добавляет текущее сообщение в буфер задержки.
                this.buffer.add(StateDefinitionMessageBuffer.Data.newBuilder()
                        .setTimeStamp(header.getTimeStamp())
                        .setMessageType(header.getMessageType())
                        .addAllBooleanValue(header.getBooleanValueList())
                        .addAllDoubleValue(header.getDoubleValueList())
                        .addAllIntValue(header.getIntValueList())
                        .addAllLongValue(header.getLongValueList())
                        .build());
                Collections.sort(this.buffer, this.bufferByMessageTimeComparator);
                return;
            }

        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read data from settings", e);
        }

    }

    private class Condition {
        String fieldType;
        int fieldValueIndex;
        int operatorId;
        Object comprValue;
        Condition(String[] settings){
            this.fieldType = settings[0];
            this.fieldValueIndex = Integer.parseInt(settings[1]);
            this.operatorId = Integer.parseInt(settings[2]);
            this.comprValue = settings[3];
        }
    }

    private class MState {
        String machineId;
        String stateId;
        MState(String machineId){
            this.machineId = machineId;
        }
    }

    private class M {
        String machineId;
        String coId;
        String coStateId;
        String eventType;
        Long time;
        M(){

        }
        M(String[] settings, Long time){
            this.eventType = settings[0];
            this.coStateId = settings[1];
            this.coId = settings[2];
            this.time = time;
        }

        @Override
        public String toString() {
            return "M{" +
                    "machineId='" + machineId + '\'' +
                    ", coId='" + coId + '\'' +
                    ", coStateId='" + coStateId + '\'' +
                    ", eventType='" + eventType + '\'' +
                    ", time=" + time +
                    '}';
        }
    }

    private class MachineStateTimer {
        MachineStateTimer(String[] settings){
            this.machineId = settings[0];
            this.sourceId = settings[1];
            this.state1 = settings[2];
            this.state2 = settings[3];
        }
        String machineStateId;
        String machineId;
        String sourceId;
        String state1;
        String state2;
        String newState2;
        String timeout;

        @Override
        public String toString() {
            return "MachineStateTimer{" +
                    "machineStateId='" + machineStateId + '\'' +
                    ", machineId='" + machineId + '\'' +
                    ", sourceId='" + sourceId + '\'' +
                    ", state1='" + state1 + '\'' +
                    ", state2='" + state2 + '\'' +
                    ", newState2='" + newState2 + '\'' +
                    ", timeout='" + timeout + '\'' +
                    '}';
        }
    }

    //<MachineId>-<SourceId>-<State1>-<State2>
    private ArrayList<MachineStateTimer> getMachineStateTimers(String[] machineStateTimers){
        ArrayList<MachineStateTimer> result = new ArrayList<MachineStateTimer>();
        for(String s: machineStateTimers){
            String[] settings = s.split("-");
            if(settings.length>=4){
                MachineStateTimer b = new MachineStateTimer(settings);
                result.add(b);
            } else {
                LOGGER.log(Level.ERROR, "Can't read MACHINESTATE_TIMER from settings. Value= "+s);
            }
        }
        return result;
    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getCOStateONMessage(long coId, long timeStamp, ArrayList<Integer> states, int blockId) {
        return COStateParser.COStateON.newBuilder()
                .setSourceId(coId).setMessageType(COStateParser.COStateON.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setMessageBlock(blockId)
                .addAllStateIds(states)
                .build();
    }

    private MessageLite getCOStateOFFMessage(long coId, long timeStamp, ArrayList<Integer> states, int blockId) {
        return COStateParser.COStateOFF.newBuilder()
                .setSourceId(coId).setMessageType(COStateParser.COStateOFF.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setMessageBlock(blockId)
                .addAllStateIds(states)
                .build();
    }

    private MessageLite getCOStateFailureMessage(long sourceId, long timeStamp, int errorCode, int blockId) {
        return COStateParser.COStateFailure.newBuilder()
                .setSourceId(sourceId).setMessageType(COStateParser.COStateFailure.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setErrorCode(errorCode)
                .setMessageBlock(blockId)
                .build();
    }

}
