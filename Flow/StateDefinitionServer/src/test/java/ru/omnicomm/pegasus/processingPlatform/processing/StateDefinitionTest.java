package ru.omnicomm.pegasus.processingPlatform.processing;

import com.sun.corba.se.impl.orb.ParserTable;
import org.junit.Test;
import ru.omnicomm.pegasus.messaging.settings.statedefinition.StateDefinitionSettingsParser;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * ru.omnicomm.pegasus.processingPlatform.processing
 * <p/>
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * <p/>
 * For internal use only.
 *
 * @author Georgy Khotyan
 * @version 0.1, 12.04.13 16:18
 */
public class StateDefinitionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger();

    @Test
    public void typesTest(){
        long l = 123133131112L;
        double d = (double)l;

        long l1 = (long)d;
        System.out.println(d);
        System.out.println(l1);
    }

    @Test
    public void comparasionTest(){
        List<Integer> list = new ArrayList<Integer>();
        list.add(50);
        list.add(200);
        StateDefinitionSettingsParser.StateDefinitionMessageBuffer.Data data
                = StateDefinitionSettingsParser.StateDefinitionMessageBuffer.Data.newBuilder()
                .addAllIntValue(list)
                .setMessageType(0)
                .setTimeStamp(0L)
                .build();

        Condition condition = new Condition();
        condition.comprValue = 510;
        condition.fieldType = "int32";
        condition.fieldValueIndex = 0;
        condition.operatorId = 1;

        compareOperation(condition, data);
    }

    private class Condition {
        String fieldType;
        int fieldValueIndex;
        int operatorId;
        Object comprValue;
        Condition(){

        }
        Condition(String[] settings){
            this.fieldType = settings[0];
            this.fieldValueIndex = Integer.parseInt(settings[1]);
            this.operatorId = Integer.parseInt(settings[2]);
            this.comprValue = settings[3];
        }
    }

    public boolean compareOperation(Condition condition, StateDefinitionSettingsParser.StateDefinitionMessageBuffer.Data bufferItem){
        List valueList = null;
        Object comprValue = null;

        if (condition.fieldType.equals("int32")){
            comprValue = (Integer)condition.comprValue;
            valueList = bufferItem.getIntValueList();
        } else if(condition.fieldType.equals("int64")){
            comprValue = (Long)condition.comprValue;
            valueList = bufferItem.getLongValueList();
        } else if(condition.fieldType.equals("bool")){
            comprValue = (Boolean)condition.comprValue;
            valueList = bufferItem.getBooleanValueList();
        } else if(condition.fieldType.equals("double")){
            comprValue = (Double)condition.comprValue;
            valueList = bufferItem.getDoubleValueList();
        } else {
            LOGGER.log(Level.DEBUG, "Cannot find this field type: "+condition.fieldType);
        }
        //FieldValueNumber – порядковый номер элемента массива, из которого необходимо считать значение для сравнения.
        // Целое неотрицательное число. Если равно 0, то значит, что условие применяется целиком к массиву
        if(!valueList.isEmpty() && condition.fieldValueIndex>0){
            List list = new ArrayList();
            list.add(valueList.get(condition.fieldValueIndex));
            return compare(list, comprValue, condition.operatorId);
        } else if(!valueList.isEmpty() && condition.fieldValueIndex==0){
            return compare(valueList, comprValue, condition.operatorId);
        } else {
            LOGGER.log(Level.ERROR, "Cannot compare. Condition: "+condition.toString());
            return true;
        }
    }

    public boolean compare(List objList, Object obj2, int operationId){
        boolean result = true;
        switch(operationId){
            //	содержит (OperatorId = 1). Среди массива значений в поле содержится заданное
            case 1:
                result = objList.contains(obj2);
                break;
            //	не содержит (OperatorId = 2). Ни одно из значений массива в поле не совпадает с заданным
            case 2:
                result = !objList.contains(obj2);
                break;
            //	равно (OperatorId = 3). Точное совпадение с зданным значением
            case 3:
                result = objList.get(0).equals(obj2);
                break;
            //	не равно (OperatorId = 4). В поле ровно одно значение и оно НЕ равно заданному
            case 4:
                result = !objList.get(0).equals(obj2);
                break;
            //	больше. В поле ровно одно значение и оно строго больше заданного (OperatorId = 5).
            case 5:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0)> (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0)> (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0)> (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
            //	меньше либо равно (OperatorId = 6). В поле ровно одно значение и оно меньше либо равно заданного
            case 6:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0) <= (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0) <= (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0) <= (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
            //	меньше (OperatorId = 7). В поле ровно одно значение и оно строго меньше заданного
            case 7:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0) < (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0) < (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0) < (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
            //	больше либо равно (OperatorId = 8). В поле ровно одно значение и оно больше либо равно заданного.
            case 8:
                if(obj2 instanceof Integer && objList.get(0) instanceof Integer)
                    result = (Integer)objList.get(0) >= (Integer)obj2;
                else if (obj2 instanceof Long && objList.get(0) instanceof Long)
                    result = (Long)objList.get(0) >= (Long)obj2;
                else if (obj2 instanceof Double && objList.get(0) instanceof Double)
                    result = (Double)objList.get(0) >= (Double)obj2;
                else
                    LOGGER.log(Level.DEBUG, "Cannot apply operation "+operationId+" to type "+obj2.getClass());
                break;
        }
        return result;
    }

}
