/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing.drainingsrefuelings;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер поиска сливов и заправок.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DrainingsRefuelingsServerImplementation")
public class RefuelingsAndDrainingsDetectionServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private static final int FUEL_VALUE_RAW_TYPE = 519;

    private Router router;
    private DataStorageService dataStorageService;

    private SettingsStorageService settingsStorageService;

    private int currentSourceId = -1;
    private SearchingBuffer currentBuffer = null;

    private Server self;
    private Handler handler;

    /**
     * Конструктор.
     *
     * @param router                 сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     * @param settingsStorageService сервис хранения настроек.
     */
    @ServerConstructor
    public RefuelingsAndDrainingsDetectionServer(Router router, DataStorageService dataStorageService, SettingsStorageService settingsStorageService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.settingsStorageService = settingsStorageService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {

        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = (int)header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(RefuelingsAndDrainingsDetectionServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }

        }

       


    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException, SettingsStorageServiceException {
        //TODO: парсим сообщение FuelValueRaw
        int sourceId = 0; //TODO: читать значение из заголовка
        if(sourceId!=this.currentSourceId){
            
            if(this.currentBuffer!=null){
                this.settingsStorageService.addBinaryData(getSearchingBufferName(this.currentSourceId), this.currentBuffer.getBytes());
            }
            this.currentSourceId = sourceId;
            //TODO: загружаем настройки из settingsStorageService для работы алгоритма
            byte[] bytes = this.settingsStorageService.getBinaryData(getSearchingBufferName(this.currentSourceId));
            if(bytes==null){
                //TODO: создаем currentBuffer путем вызова конструктора SearchingBuffer(int bf, int lfu, int lfd, int nc, int dIntTr, int rIntTr)
            } else {
                //TODO: создаем currentBuffer путем вызова конструктора SearchingBuffer(int bf, int lfu, int lfd, int nc, int dIntTr, int rIntTr, byte[] bytes)
            }
        }
        
        //TODO: читаем messageTime и value из FuelValueRaw
        int t = 0; //TODO: t = messageTime;
        int v = 0; //TODO: v = value
        
        this.currentBuffer.processData(t, v);
    }
   

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) { // Инициализация.
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            handler = initSignal.getHandler();

            router.registerServer(RefuelingsAndDrainingsDetectionServer.class, self, FUEL_VALUE_RAW_TYPE);
        } else if (signal instanceof Terminate) { // Завершение работы.
            router.unregisterServer(RefuelingsAndDrainingsDetectionServer.class, self);
        } else if (signal instanceof CaughtException) { // Ошибка.
            Throwable exception = ((CaughtException) signal).getException();
            LOGGER.log(Level.ERROR, exception.getMessage(), exception);
        } 

    }


    private String getSearchingBufferName(int sourceId) {

        return String.format("src%d-drainingRefuelingSearching-buffer", sourceId);

    }




}