/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processingPlatform.processing.drainingsrefuelings;

import java.util.LinkedList;

/**
 *
 * @author alexander
 */
public class SearchingBuffer {
    
    private final LinkedList<BufferItem> buffer = new LinkedList<>();

    private final int bf;
    private final int lfu;
    private final int lfd;
    private final int nc;
    private final int dIntTr;
    private final int rIntTr;

    private int u;
    private SearchingBufferState r;
    private int kstart;
    private int kend;
    private BufferItem dataStart;
    private BufferItem dataEnd;
    private boolean fixDRStart = false;
    private boolean fixRFStart = false;
    
 
    public SearchingBuffer(int bf, int lfu, int lfd, int nc, int dIntTr, int rIntTr) {
        this.bf = bf;
        this.lfu = lfu;
        this.lfd = lfd;
        this.nc = nc;
        this.dIntTr = dIntTr;
        this.rIntTr = rIntTr;
    }
    
    public SearchingBuffer(int bf, int lfu, int lfd, int nc, int dIntTr, int rIntTr, byte[] bytes) {
        this(bf,lfu,lfd,nc,dIntTr,rIntTr);
        //инициализируем not final переменные исходя из содержимого bytes
    }
    
    /**
     * возвращает сериализованное представление буфера в формате protobuf
     * @return 
     */
    public byte[] getBytes(){
        throw new UnsupportedOperationException("Not implemented yet");
    }
    
    public void processData(int t, int v){
        while(this.buffer.size()>=bf*2-1){
            this.buffer.pollFirst();
        }
        this.buffer.addLast(new BufferItem(t,v));
        
        if(this.buffer.size()<=bf){
            u = this.buffer.size()-1;
            return;
        }
        
        if(this.buffer.size()>2*bf){
            return;
        }
        
        switch(this.r){
            case NORMAL:
                //Проверяем, есть ли условия для начала заправки в точке U, если условия для начала заправки есть:
                if(isRefuelingConditions()){
                    
                }
                break;
            case REFUELING:
                break;
            case DRAINING:
                break;
        }
        
    }
    
    /**
     * 5.3	Определение начала предполагаемой заправки в точке U
     * @return true, если подтверждается информация о начале предполагаемойзаправки
     */
    private boolean isRefuelingConditions(){
        if(this.buffer.get(u).v<this.buffer.get(u+1).v){
            return true;
        }
        return 
                (this.buffer.get(u).v==this.buffer.get(u+1).v) && 
                (this.buffer.get(u+2).v>this.buffer.get(u+1).v);
            
        
   
    }

    
    private class BufferItem {
        private final int t;
        private final int v;

        public BufferItem(int t, int v) {
            this.t = t;
            this.v = v;
        }
        
        
    }
    
    private enum SearchingBufferState {
    
        NORMAL,
        REFUELING,
        DRAINING
    
    }
    
}
