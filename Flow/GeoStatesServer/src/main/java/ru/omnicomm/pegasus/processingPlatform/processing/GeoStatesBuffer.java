/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;

import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser;
import ru.omnicomm.pegasus.messaging.data.location.GeoZonesParser.GeoZoneEND;
import ru.omnicomm.pegasus.messaging.data.location.GeoZonesParser.GeoZoneSTART;
import ru.omnicomm.pegasus.messaging.data.location.GeoZonesParser.GeoZones;
import ru.omnicomm.pegasus.messaging.settings.location.GeoStateDefinitionSettingsParser.CoordinatesSet;
import ru.omnicomm.pegasus.messaging.settings.location.GeoStateDefinitionSettingsParser.GeoStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.location.GeoStateDefinitionSettingsParser.GeoStateDefinitionSettings;
import ru.omnicomm.pegasus.messaging.settings.location.GeoStateDefinitionSettingsParser.GeoZone;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * Буфер алгоритма определения состония по геозонам
 * @author alexander
 */
public class GeoStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final long sourceId;
    private final List<GeoZone> geoZones;
    private final List<GeoZone> locationGeoZones;

    /**
     * Конструктор
     * @param settings настройки алгоритма 
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public GeoStatesBuffer(GeoStateDefinitionSettings  settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.geoZones = settings.getGeoZonesList();
        this.locationGeoZones = new ArrayList<GeoZone>();
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public GeoStatesBuffer(GeoStateDefinitionSettings settings,
            GeoStateDefinitionBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.getGeoZonesList().equals(buffer.getGeoZonesList())){
            this.locationGeoZones.addAll(buffer.getLocationGeoZonesList());
        } 
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        GeoStateDefinitionBuffer.Builder builder =
                GeoStateDefinitionBuffer.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(GeoStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setTimeStamp(0)
                .addAllGeoZones(this.geoZones)
                .addAllLocationGeoZones(this.locationGeoZones);

        return builder.build();

    }

    /**
     * Производит обработку данных согласно алгоритму, изложенному в требованиях
     * @param router сервис маршрутизатора сообщений
     * @param jmsBridgeService  сервис jms-bridge
     * @param dataStorageService сервис хранения данных
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, CoordinateParser.Coordinate dataMessage) {
        //int messageTime = dataMessage.getMessageTime();
        long timestamp = dataMessage.getTimeStamp();
        double latitude = dataMessage.getLatitude();
        double longitude = dataMessage.getLongitude();
        int blockId = dataMessage.getMessageBlock();

        boolean listChanged = false;
        for(GeoZone geoZone : this.geoZones){

            boolean result;
            switch(geoZone.getType()){

                case CIRCLE:
                    result = isInCircle(geoZone, latitude, longitude);
                    break;

                case RECTANGLE:
                    result = isInRectangle(geoZone, latitude, longitude);
                    break;

                case POLYGON:
                    result = isInPolygon(geoZone, latitude, longitude);
                    break;

                default:
                    throw new IllegalArgumentException("Unknown geozone type");

            }

            boolean containsGeoZone = false;
            GeoZone containingGeoZone = null;
            for(GeoZone locationGeoZone : this.locationGeoZones){
                if(locationGeoZone.getId() == geoZone.getId()){
                    containsGeoZone = true;
                    containingGeoZone = locationGeoZone;
                    break;
                }
            }

            //if(result && !this.locationGeoZones.contains(geoZone)){
            List<Integer> startGeoZones = new ArrayList<Integer>();
            if(result && !containsGeoZone){
                //this.sendToServices(router, jmsBridgeService, dataStorageService, getGeoZoneSTARTMessage(sourceId,messageTime,geoZone.getId()));
                startGeoZones.add(geoZone.getId());
                this.locationGeoZones.add(geoZone);
                listChanged=true;
            }
            if(!startGeoZones.isEmpty()){
                this.sendToServices(router, jmsBridgeService, dataStorageService, getGeoZoneSTARTMessage(sourceId,timestamp,startGeoZones, blockId));
            }

            List<Integer> endGeoZones = new ArrayList<Integer>();
            if(!result && containsGeoZone){
                //this.sendToServices(router, jmsBridgeService, dataStorageService, getGeoZoneENDMessage(sourceId,messageTime,containingGeoZone.getId()));
                endGeoZones.add(containingGeoZone.getId());
                this.locationGeoZones.remove(containingGeoZone);
                listChanged=true;
            }
            if(!endGeoZones.isEmpty()){
                this.sendToServices(router, jmsBridgeService, dataStorageService, getGeoZoneENDMessage(sourceId,timestamp,endGeoZones, blockId));
            }

        }

        if(listChanged){
            List<Integer> ids = new ArrayList<Integer>();
            for(GeoZone locationGeoZone : this.locationGeoZones){
                ids.add(locationGeoZone.getId());
            }
            this.sendToServices(router, jmsBridgeService, dataStorageService, getGeoZonesMessage(sourceId,timestamp,ids, blockId));

        }

    }

    /**
     * Проверка вхождения в геозону типа "окружность"
     * @param geoZone геозона
     * @param latitude широта точки
     * @param longitude долгота точки
     * @return true если точка входит в геозону
     */
    private boolean isInCircle(GeoZone geoZone, double latitude, double longitude){
     
        double l1 = latitude;
        double g1 = longitude;

        double l2 = geoZone.getCoordinatesSets(0).getCoordinatesList().get(0).getLatitude();
        double g2 = geoZone.getCoordinatesSets(0).getCoordinatesList().get(0).getLongitude();

        long radius = geoZone.getRadius(); //mm (согласно требованиям)
        long earth_radius = 6372000000L; //mm
        double s =
                2L * earth_radius * Math.asin(
                    Math.sqrt(
                        Math.pow(sin((l1 - l2) / 2.0), 2) +
                        cos(l1) * cos(l2) *
                        Math.pow(sin((g1 - g2) / 2.0), 2)
                    )
                );

        return (double)radius - s >= 0 ;

    }

    /**
     * Проверка вхождения в геозону типа "прямоугольник"
     * @param geoZone геозона
     * @param latitude широта точки
     * @param longitude долгота точки
     * @return true если точка входит в геозону
     */
    private boolean isInRectangle(GeoZone geoZone, double latitude, double longitude){

        double l = latitude;
        double g = longitude;

        double l1 = geoZone.getCoordinatesSets(0).getCoordinatesList().get(0).getLatitude();
        double g1 = geoZone.getCoordinatesSets(0).getCoordinatesList().get(0).getLongitude();

        double l2 = geoZone.getCoordinatesSets(0).getCoordinatesList().get(1).getLatitude();
        double g2 = geoZone.getCoordinatesSets(0).getCoordinatesList().get(1).getLongitude();
        
        if(l1>l2){
            double buf = l1;
            l1 = l2;
            l2 = buf;
        }
        
        if(g1>g2){
            double buf = g1;
            g1 = g2;
            g2 = buf;
        }

        return (l1 <= l) && (l <= l2) && (g1 <= g) && (g <= g2);

    }

    /**
     * Проверка вхождения в геозону типа "многоугольник"
     * @param geoZone геозона
     * @param latitude широта точки
     * @param longitude долгота точки
     * @return true если точка входит в геозону
     */
    private static boolean isInPolygon(GeoZone geoZone, double latitude, double longitude){

        for(CoordinatesSet coordinatesSet : geoZone.getCoordinatesSetsList()){
            if(isInTriangle(coordinatesSet,latitude,longitude)){
                return true;
            }
        }
        return false;
    }


    private static boolean isInTriangle(CoordinatesSet coordinatesSet, double latitude, double longitude){
        
        long x1 = (long)(coordinatesSet.getCoordinates(0).getLatitude()  * 10000000L);
        long y1 = (long)(coordinatesSet.getCoordinates(0).getLongitude() * 10000000L);

        long x2 = (long)(coordinatesSet.getCoordinates(1).getLatitude()  * 10000000L);
        long y2 = (long)(coordinatesSet.getCoordinates(1).getLongitude() * 10000000L);

        long x3 = (long)(coordinatesSet.getCoordinates(2).getLatitude()  * 10000000L);
        long y3 = (long)(coordinatesSet.getCoordinates(2).getLongitude() * 10000000L);

        long x =  (long)                                      (latitude  * 10000000L);
        long y =  (long)                                      (longitude * 10000000L);       

        //Точка принадлежит треугольнику тогда и только тогда, когда все, не равные нулю, из следующих величин имеют один знак.
        long d1 = getDeterminant(x,y,x1,y1,x2,y2);
        long d2 = getDeterminant(x,y,x2,y2,x3,y3);
        long d3 = getDeterminant(x,y,x3,y3,x1,y1);
        

        return ((d1>0||d1==0)&&(d2>0||d2==0)&&(d3>0||d3==0))||((d1<0||d1==0)&&(d2<0||d2==0)&&(d3<0||d3==0));

    }

    
    /**
     * Возвращает определитель          <br/>
     * | x  y  1 |                      <br/>
     * | x1 y1 1 |                      <br/>
     * | x2 y2 1 |                      <br/>
     * 
     * @param x x
     * @param y y
     * @param x1 x1
     * @param y1 y1
     * @param x2 x2
     * @param y2 y2
     * @return определитель матрицы
     */
    private static long getDeterminant(long x, long y, long x1, long y1, long x2, long y2) {
        long a = x1 - x;
        long b = y1 - y;
        long c = x2 - x;
        long d = y2 - y;
        
        return a*d - b*c;
    }

    private double sin(double deg){
        return Math.sin(Math.toRadians(deg));
    }

    private double cos(double deg){
        return Math.cos(Math.toRadians(deg));
    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        //LOGGER.log(Level.INFO, new StringBuilder().append("GeoStatesServer output: ").append(message).toString());
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getGeoZoneSTARTMessage(long sourceId, long timestamp, List<Integer> ids, int blockId) {
        return GeoZoneSTART.newBuilder()
                .setSourceId(sourceId).setMessageType(GeoZoneSTART.getDefaultInstance().getMessageType())
                .setTimeStamp(timestamp)
                .setMessageBlock(blockId)
                .addAllId(ids)
                .build();
    }

    private MessageLite getGeoZoneENDMessage(long sourceId, long timestamp, List<Integer> ids, int blockId) {
        return GeoZoneEND.newBuilder()
                .setSourceId(sourceId).setMessageType(GeoZoneEND.getDefaultInstance().getMessageType())
                .setTimeStamp(timestamp)
                .setMessageBlock(blockId)
                .addAllId(ids)
                .build();
    }

    private MessageLite getGeoZonesMessage(long sourceId, long timestamp, List<Integer> ids, int blockId) {
        return GeoZones.newBuilder()
                .setSourceId(sourceId).setMessageType(GeoZones.getDefaultInstance().getMessageType())
                .setTimeStamp(timestamp)
                .setMessageBlock(blockId)
                .addAllIds(ids)
                .build();
    }


}
