/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.common.SettingsErrorParser.SettingsErrorMessage;
import ru.omnicomm.pegasus.messaging.common.ValueMessageParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер калибровки.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ArithmeticServerImplementation")
public class ArithmeticServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private final int blockId = 1;
    private Router router;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;

    private long currentSourceId;

    /**
     * Конструктор.
     *
     * @param router                 сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     * @param settingsService сервис хранения настроек.
     */
    @ServerConstructor
    public ArithmeticServer(Router router, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }


        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(ArithmeticServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }

        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            currentSourceId = -1;
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            int[] types;

            try {
                types = getMessageTypesFromSettings();
                router.registerServer(ArithmeticServer.class, self, types);
            } catch (SettingsStorageServiceException e) {
                LOGGER.log(Level.ERROR, "Can't read message types from settings", e);
            }
        } else if (signal instanceof Pause) {
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(ArithmeticServer.class, self);
        }
    }

    /**
     * Возвращает массив типов сообщений из настроек,
     * на которые следует подписаться
     * @return
     * @throws ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException
     */
    private int[] getMessageTypesFromSettings() throws SettingsStorageServiceException {
        int[] types = null;
        String[] messageTypes = this.settingsService.get(blockId, "SRV_ARIFM_INBOUND_MESSAGE");
        if(messageTypes==null || messageTypes.length==0){
            LOGGER.log(Level.ERROR, "Message types from settings is null");
        }
        else{
            types = new int[messageTypes.length];
            int i = 0;
            //подписываемся на сообщения из настроек
            for(String messageType : messageTypes){
                types[i] = Integer.parseInt(messageType);
                i++;
            }
        }
        return types;
    }

    /**
     * Получение значения настройки по ключу и номеру блока
     * @param blockId
     * @param key
     * @return
     * @throws SettingsStorageServiceException
     */
    private String[] getSettings(int blockId, String key) throws SettingsStorageServiceException {
        String[] settings = settingsService.get(blockId, key);
        if(settings==null || settings.length==0){
            LOGGER.log(Level.DEBUG, "Can't read "+key+" from settings");
            return null;
        }
        LOGGER.log(Level.DEBUG, "Key: "+key);
        for(String setting : settings){
            LOGGER.log(Level.DEBUG, "setting: "+setting);
        }
        return settings;
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }
            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message){
        try{
            ValueMessageParser.ValueMessage valueMessage = ValueMessageParser.ValueMessage.parseFrom(message.toByteArray());
            //1)	Сервер считывает из заголовка сообщения источник полученного сообщения (SourceID),
            // временной штамп полученного сообщения (Т), а также тип сообщения (MsgType).
            long sourceId = valueMessage.getSourceId();
            long timestamp = valueMessage.getTimeStamp();
            long typeId = valueMessage.getMessageType();
            int blockId = valueMessage.getMessageBlock();

            //2)	Сервер запрашивает у сервиса настроек все настройки, для которых ключ ARIFM_SERV<SourceId><MsgType>,
            // заданный в заголовке входящего сообщения, соответственно равен ключу ARIFM_SERV<SourceId><InMsgType>, хранящемуся в сервисе настроек
            //  Value = <InDataType><OperationType><OutDataType><OutMsgType>
            String[] arifmServSettings = getSettings(blockId, "ARIFM_SERV-"+sourceId+"-"+typeId);
            if(arifmServSettings==null)
                return;


            //4)	Для каждой считанной настройки
            for (String arifmServSetting : arifmServSettings){
                String[] values = arifmServSetting.split("-");
                if(values == null || values.length<4){
                    LOGGER.log(Level.ERROR, "Can't parse ARIFM_SERV settings. Value= "+arifmServSetting);
                    continue;
                }
                String inDataType = values[0];
                int operationType = Integer.parseInt(values[1]);
                String outDataType = values[2];
                int outMsgType = Integer.parseInt(values[3]);

                List valueList = null;
                if(inDataType.equals("int32"))
                    valueList = valueMessage.getIntValueList();
                if(inDataType.equals("int64"))
                    valueList = valueMessage.getLongValueList();
                if(inDataType.equals("double"))
                    valueList = valueMessage.getDoubleValueList();

                //d.	Если на предыдущем шаге поле не найдено
                if(valueList==null || valueList.isEmpty()){
                    MessageLite settingsErrorMessage = getSettingsErrorMessage(sourceId, timestamp, 3);
                    sendToServices(router, dataStorageService, settingsErrorMessage);
                    return;
                }

                Double result = calculation(operationType, valueList);

                //f.	Приводим значение переменной Result, возвращенной операцией на шаге 4e, к типу OutDataType
                if(outDataType.equals("int32")){
                    Integer outValue = convertToInt(result);
                    //g.	Если на предыдущем шаге или в ходе выполнения расчета произошла ошибка (например, переполнение типа)
                    if(outValue==null){
                        MessageLite settingsErrorMessage = getSettingsErrorMessage(sourceId, timestamp, 2);
                        sendToServices(router, dataStorageService, settingsErrorMessage);
                        return;
                    }
                    MessageLite resultMessage = getValueMessageBuilder(sourceId, outMsgType, blockId, timestamp).addIntValue(outValue).build();
                    sendToServices(router, dataStorageService, resultMessage);
                }
                if(outDataType.equals("int64")){
                    Long outValue = convertToLong(result);
                    //g.	Если на предыдущем шаге или в ходе выполнения расчета произошла ошибка (например, переполнение типа)
                    if(outValue==null){
                        MessageLite settingsErrorMessage = getSettingsErrorMessage(sourceId, timestamp, 2);
                        sendToServices(router, dataStorageService, settingsErrorMessage);
                        return;
                    }
                    MessageLite resultMessage = getValueMessageBuilder(sourceId, outMsgType, blockId, timestamp).addLongValue(outValue).build();
                    sendToServices(router, dataStorageService, resultMessage);
                }
                if(outDataType.equals("double")){
                    MessageLite resultMessage = getValueMessageBuilder(sourceId, outMsgType, blockId, timestamp).addDoubleValue(result).build();
                    sendToServices(router, dataStorageService, resultMessage);
                }

            }


        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read data from settings", e);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
    }

    private Double calculation(int operationType, List valueList){
        Double result = 0d;
        switch (operationType){
            //i.	Если значение OperationType=1, то выполняем алгоритм 5.2 Суммирование
            case 1:
                for(int i=0; i<valueList.size(); i++){
                    if(valueList.get(i) instanceof Integer)
                        result+=((Integer)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Long)
                        result+=((Long)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Double)
                        result+=(Double)valueList.get(i);
                }
                return result;
            //ii.	Если значение OperationType=2, то выполняем алгоритм 5.3 Нахождение среднего значения
            case 2:
                for(int i=0; i<valueList.size(); i++){
                    if(valueList.get(i) instanceof Integer)
                        result+=((Integer)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Long)
                        result+=((Long)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Double)
                        result+=(Double)valueList.get(i);
                }
                return result/valueList.size();
            //iii.	Если значение OperationType=3, то выполняем алгоритм 5.4 Нахождение минимального значения
            case 3:
                Collections.sort(valueList);
                return Double.parseDouble(valueList.get(0).toString());
            //iv.	Если значение OperationType=4, то выполняем алгоритм 5.5 Нахождение максимального значения
            case 4:
                Collections.sort(valueList);
                return Double.parseDouble(valueList.get(valueList.size()-1).toString());
        }
        return null;
    }

    private Integer convertToInt(Double d){
        if(d>Integer.MAX_VALUE || d<Integer.MIN_VALUE)
            return null;
        else
            return d.intValue();
    }

    private Long convertToLong(Double d){
        if(d>Long.MAX_VALUE || d<Long.MIN_VALUE)
            return null;
        else
            return d.longValue();
    }

    private void addToS(ArrayList s, String dataType, String value){
        if(dataType.equals("int32"))
            s.add(Integer.parseInt(value));
        if(dataType.equals("int64"))
            s.add(Long.parseLong(value));
        if(dataType.equals("double"))
            s.add(Double.parseDouble(value));
    }

    private void sendToServices(Router router,  DataStorageService dataStorageService, MessageLite message){
        router.send(message);

        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private ValueMessageParser.ValueMessage.Builder getValueMessageBuilder(long sourceId, long messageType, int blockId, long timeStamp) {
        return ValueMessageParser.ValueMessage.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(messageType)
                .setMessageBlock(blockId)
                .setTimeStamp(timeStamp);
    }

    private MessageLite getSettingsErrorMessage(long sourceId, long timeStamp, int value) {
        return SettingsErrorMessage.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(SettingsErrorMessage.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setBody(value)
                .build();
    }

}
