package ru.omnicomm.pegasus.processingPlatform.processing;

import org.junit.Before;
import org.junit.Test;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ru.omnicomm.pegasus.processingPlatform.processing
 * <p/>
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * <p/>
 * For internal use only.
 *
 * @author Georgy Khotyan
 * @version 0.1, 12.04.13 16:18
 */
public class ArithmeticServerTest {
    ArrayList intList = new ArrayList();
    ArrayList longList = new ArrayList();
    ArrayList doubleList = new ArrayList();
    int operationType = 2;

    @Before
    public void typesTest(){
        int i1 = 1; int i2 = 2; int i3 = 2;
        intList.add(i1); intList.add(i2); intList.add(i3);

        long l1 = 1; long l2 = 2; long l3 = 2;
        longList.add(l1); longList.add(l2); longList.add(l3);

        double d1 = 1; double d2 = 2; double d3 = 2;
        doubleList.add(d1); doubleList.add(d2); doubleList.add(d3);
    }

    @Test
    public void conversionTest(){

        Double result = 213424323432143323.99d;
        for(int i=0; i<100; i++)
            result+=result;
        if(result>Integer.MAX_VALUE || result<Integer.MIN_VALUE)
            System.out.println("Error");
        int iResult = result.intValue();
        System.out.println(result+" ; "+iResult);
    }

    @Test
    public void intCalculationTest(){
        Double result = calculation(operationType, intList);
        int iResult = result.intValue();
        System.out.println(result+" ; "+iResult);
    }

    @Test
    public void longCalculationTest(){
        Double result = calculation(operationType, longList);
        long iResult = result.longValue();
        System.out.println(result+" ; "+iResult);
    }

    @Test
    public void doubleCalculationTest(){
        Double result = calculation(operationType, doubleList);
        System.out.println(result);
    }

    private Double calculation(int operationType, List valueList){
        Double result = 0d;
        switch (operationType){
            //i.	Если значение OperationType=1, то выполняем алгоритм 5.2 Суммирование
            case 1:
                for(int i=0; i<valueList.size(); i++){
                    if(valueList.get(i) instanceof Integer)
                        result+=((Integer)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Long)
                        result+=((Long)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Double)
                        result+=(Double)valueList.get(i);
                }
                return result;
            //ii.	Если значение OperationType=2, то выполняем алгоритм 5.3 Нахождение среднего значения
            case 2:
                for(int i=0; i<valueList.size(); i++){
                    if(valueList.get(i) instanceof Integer)
                        result+=((Integer)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Long)
                        result+=((Long)valueList.get(i)).doubleValue();
                    if(valueList.get(i) instanceof Double)
                        result+=(Double)valueList.get(i);
                }
                return result/valueList.size();
            //iii.	Если значение OperationType=3, то выполняем алгоритм 5.4 Нахождение минимального значения
            case 3:
                Collections.sort(valueList);
                return Double.parseDouble(valueList.get(0).toString());
            //iv.	Если значение OperationType=4, то выполняем алгоритм 5.5 Нахождение максимального значения
            case 4:
                Collections.sort(valueList);
                return Double.parseDouble(valueList.get(valueList.size()-1).toString());
        }
        return null;
    }

}
