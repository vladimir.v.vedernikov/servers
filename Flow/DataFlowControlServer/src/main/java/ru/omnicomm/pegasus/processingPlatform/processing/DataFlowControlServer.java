/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер определения парковкм;
 * реализация согласно требованиям TTP-PSS-11-(DataFlowControl Server)
 * @author khotyan
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DataFlowControlServerImplementation")
public class DataFlowControlServer implements ServerImplementation {
    private final int blockId = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private DataFlowControlBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public DataFlowControlServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
        this.currentBuffer =  new DataFlowControlBuffer(this.settingsService);
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;

        try {
            header =  CommonParser.MessageHeader.parseFrom(message.toByteArray());

        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(header);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
           long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(DataFlowControlServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;
            int[] types;
            int messagesCount = 1000;

            try {
                //1)	Сервер подписывается в сервисе Router на все сообщения
                types = new int[messagesCount];
                int i =0;
                while(i<messagesCount){
                    types[i] = ++i;
                }
                router.registerServer(DataFlowControlServer.class, self, types);
                //2)	Сервер запрашивает у менеджера слоя набор блоков, с которыми работает данный слой.

                //3)	Сервер подписывается на таймерные сообщения (1 раз в минуту), указывая соответствующие блоки
                Handler handler = initSignal.getHandler();
                handler.setTimer("timer0", Handler.TimerType.RATE_BASED, 1000L, 60000L);

                //4)	Сервер считывает значение настройки IDLETIMER_LAST. Если значение настройки не найдено,
                // Сервер устанавливает значение этой настройки, равным текущему времени (округление вниз до целого числа минут)
                String idletimeLast = getSetting(this.settingsService, blockId, "IDLETIMER_LAST");
                if(idletimeLast==null){
                    Long tcur = System.currentTimeMillis();
                    while(tcur%60000L!=0){
                        tcur--;
                    }
                    this.settingsService.add(blockId, "IDLETIMER_LAST", String.valueOf(tcur));
                }

            } catch (Exception e) {
                LOGGER.log(Level.ERROR, "Can't register server", e);
            }

        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(DataFlowControlServer.class, self);
        } else if (signal instanceof TimerEvent) { // Входящее таймерное сообщение
            TimerEvent timerEvent = (TimerEvent) signal;
            LOGGER.debug("TimerMessage received. Id= "+timerEvent.id());
            processTimerMessage(timerEvent);
        }
    }

    private String getSetting(SettingsStorageService settingsService, int blockId, String key) throws SettingsStorageServiceException {
        String[] settings = settingsService.get(blockId, key);
        if(settings==null || settings.length==0){
            LOGGER.log(Level.ERROR, "Can't read "+key+" from settings");
            return null;
        }
        LOGGER.log(Level.DEBUG, key+" settings: "+settings[0]);
        return settings[0];
    }

    private void processTimerMessage(TimerEvent timerEvent) {
        try {
            long t = System.currentTimeMillis();
            this.currentBuffer.processTimerData(router,jmsBridgeService,dataStorageService,t);

        } catch (InvalidProtocolBufferException e) {
                LOGGER.log(Level.ERROR, e.getMessage(), e);
        } catch (Exception e) {
            LOGGER.log(Level.ERROR, "Can't read message types from settings", e);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    /**
     * Выполняется после загрузки настроек при получении сообщения
     * @param header
     * @throws Exception
     */
    private void processMessage(CommonParser.MessageHeader header) throws Exception {

        this.currentBuffer.processData(router,jmsBridgeService,dataStorageService,header);
    }

}
