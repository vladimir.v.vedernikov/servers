/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.online.IDLEStateParser;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;


/**
 * Буфер алгоритма определения состояний по движению
 * @author alexander
 */
public class DataFlowControlBuffer {
    private final int blockId = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private static final long STARTING_POINT = 1293840000000L;
    private SettingsStorageService settingsService;


    /**
     * Конструктор
     */
    public DataFlowControlBuffer(SettingsStorageService settingsService) {
        this.settingsService = settingsService;
    }

    /**
     * Выполняет обработку входных данных от таймера согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param ttime время
     */
    public void processTimerData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,
                                 long ttime) throws InvalidProtocolBufferException {
        try {
            //1)	Сервер получает значения блоков из таймерного сообщения
            //2)	По каждому из блоков:
            //a.	Сервер считывает значение TimeX настройки IDLETIMER_LAST
            String timeX = getSetting(settingsService, blockId, "IDLETIMER_LAST");
            if(timeX==null)
                return;
            Long timeXL = Long.parseLong(timeX);

            //2)b.	Цикл по всем целым значениям Tk в полуинтервале [TimeX, TTime) (предполагаются целые значения в минутах):
            for(long tk=timeXL; tk<=ttime; tk++){
                if(tk%60000L!=0){
                    //i.	Сервер считывает настройки таймеров по ключу IDLETIMER-<Tk>
                    String idleTimer = getSetting(settingsService, blockId, "IDLETIMER-"+tk);
                    if(idleTimer==null)
                        return;
                    Long coId = Long.parseLong(idleTimer);
                    //ii.	Для каждой из полученных настроек:
                    //1.	Сервер считывает COid из значения настройки
                    //2.	Сервер формирует, сохраняет в хранилище, а также направляет в сервисы JMS и Router сообщение типа IdleSTART
                    this.sendToServices(router, jmsBridgeService, dataStorageService,
                            this.getIdleStartMessage(coId, tk, blockId));
                    //3.	Сервер удаляет текущую обрабатываемую настройку
                    settingsService.delete(blockId, "IDLETIMER-"+tk);
                    //4.	Сервер удаляет настройку, которая ссылается на обрабатываемую для данного COid
                    settingsService.delete(blockId, "IDLETIMER_REF-"+coId, "IDLETIMER-"+tk);
                }
            }
            //c.	Сервер устанавливает значение настройки IDLETIMER_LAST равным TTime
            settingsService.delete(blockId, "IDLETIMER_LAST");
            settingsService.add(blockId, "IDLETIMER_LAST", String.valueOf(ttime));

        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read data from settings", e);
        }

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param header тело сообщения
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,
                            CommonParser.MessageHeader header) throws InvalidProtocolBufferException {

        try {
            int blockId = header.getMessageBlock();

            //2.	Сервер находит по SourceId соответствующий идентификатор КО (COid), из соответствующей настройки
            String coId = getSetting(settingsService, blockId, "COID-"+header.getSourceId());
            if(coId==null)
                return;

            //3.	Сервер считывает текущее состояния поступления данных для данного КО из соответствующей настройки
            //IDLESTATE-<COID>
            String idleState = getSetting(settingsService, blockId, "IDLESTATE-"+coId);
            if(idleState==null)
                return;

            //4.	Если состояние = 1 (данные поступают), то необходимо обновить время таймера переключения в состояние непоступления данных
            if(idleState.equals("1") || idleState.equals("2")){
                //a.	Сервер считывает ссылку на настройку таймера из для COid соответствующей настройки
                //IDLETIMER_REF-<COID>
                //В значении указывается id настройки вида IDLETIMER-<Time> для данного COid
                String idleTimerRef = getSetting(settingsService, blockId, "IDLETIMER_REF-"+coId);
                if(idleTimerRef==null)
                    return;

                //b.	Сервер удаляет настройку таймера
                //IDLETIMER-<Time>
                settingsService.delete(blockId, idleTimerRef);

                //c.	Сервер считывает timestamp сообщения равный T
                Long t = header.getTimeStamp();

                //d.	Сервер считывает настройку IDLEMAX для SourceId (обозн. dT)
                //IDLEMAX-<SourceId>
                String dt = getSetting(settingsService, blockId, "IDLEMAX-"+header.getSourceId());
                if(dt==null)
                    return;
                Long dtL = Long.parseLong(dt);

                //e.	Сервер устанавливает TheTime = T + dT
                Long theTime = t+dtL;

                //f.	Сервер создаёт новую настройку key = IDLETIMER-<TheTime> value = <COid>
                settingsService.add(blockId, "IDLETIMER-"+theTime, coId);

                //g.	Сервер присваивает значению настройки полученной на шаге "a" значение ID настройки созданной на шаге f
                settingsService.delete(blockId, "IDLETIMER_REF-"+coId);
                settingsService.add(blockId, "IDLETIMER_REF-"+coId, "IDLETIMER-"+theTime);
            }
            //5.	Если состояние = 2 (данные не поступают):
            //b.	Сервер формирует, сохраняет в хранилище, а также направляет в сервисы JMS и Router сообщение типа IdleEND со следующими полями:
            //i.	SourceID = COid
            //ii.	Timestamp = T
            //iii.	(пустое тело сообщения)
            this.sendToServices(router, jmsBridgeService, dataStorageService,
                    this.getIdleEndMessage(header.getSourceId(), header.getTimeStamp(), blockId));

        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read data from settings", e);
        }

    }

    private String getSetting(SettingsStorageService settingsService, int blockId, String key) throws SettingsStorageServiceException {
        String[] settings = settingsService.get(blockId, key);
        if(settings==null || settings.length==0){
            LOGGER.log(Level.ERROR, "Can't read "+key+" from settings");
            return null;
        }
        LOGGER.log(Level.DEBUG, key+" settings: "+settings[0]);
        return settings[0];
    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getIdleStartMessage(long sourceId, long timeStamp, int blockId) {
        return IDLEStateParser.IDLESTART.newBuilder()
                .setSourceId(sourceId).setMessageType(IDLEStateParser.IDLESTART.getDefaultInstance().getMessageType()).setTimeStamp(timeStamp)
                .build();
    }

    private MessageLite getIdleEndMessage(long sourceId, long timeStamp, int blockId) {
        return IDLEStateParser.IDLEEND.newBuilder()
                .setSourceId(sourceId).setMessageType(IDLEStateParser.IDLEEND.getDefaultInstance().getMessageType()).setTimeStamp(timeStamp)
                .build();
    }

    private static long getMessageTime(long unixTimeStamp){
        return (unixTimeStamp-STARTING_POINT);
    }

}
