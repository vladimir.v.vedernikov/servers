#!/bin/sh

DEST_PATH=../java

#######################################
# Create parsers for required messages #
#######################################
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/common
for FILE_NAME in MessageHeader.proto  ValueMessage.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done


########################################
# Create parser for required settings  #
########################################
FILE_NAME=MultiplyData.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/general
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}



echo "done"
