/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.*;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.common.ValueMessageParser.ValueMessage;
import ru.omnicomm.pegasus.messaging.settings.general.MultiplyDataSettingsParser.MultiplyDataServerSettings;
import ru.omnicomm.pegasus.messaging.settings.general.MultiplyDataSettingsParser.MultiplyDataServerSettings.MultiplyDataServerSettingsItem;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер умножения.
 * Основным функционалом сервера умножения является умножение значения во
 * входящим сообщением на заданный коэффициент. Входной информацией для сервера
 * умножения является информация, пришедшая с датчика или транспортного
 * средства. Этот сервер предназначен для работы с пакетом данных одного типа.
 * Пакет может состоять из одного числа. Сервер умножения отличается от
 * большинства серверов тем, что в зависимости от типа входящего сообщения, он
 * меняет тип исходящего сообщения.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("MultiplyDataServerImplementation")
public class MultiplyDataServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;
    private HashMap<Integer,List<SettingsItem>> settings;

    private Server self;
    private long currentSourceId;

    /**
     * Конструктор.
     *
     * @param router          сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     * @param settingsService сервис хранения настроек.
     */
    @ServerConstructor
    public MultiplyDataServer(Router router, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;
        try {
            header = MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(MultiplyDataServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            //Загрузка настроек сервера
            try {
                MessageLite settingsObject = this.settingsService.lookup(this.getMultiplyDataServerSettingsName());

                this.settings = new HashMap<Integer,List<SettingsItem>>();

                if(settingsObject!=null){
                    MultiplyDataServerSettings serverSettings = MultiplyDataServerSettings.parseFrom(settingsObject.toByteArray());
                    LOGGER.info("Multiply server settings:\n"+serverSettings);
                    for(MultiplyDataServerSettingsItem item : serverSettings.getSettingsItemList()){
                        int sourceId = (int)item.getSourceId();
                        List<SettingsItem> list = this.settings.get(sourceId);
                        if(list==null){
                            list = new ArrayList<SettingsItem>();
                            this.settings.put(sourceId, list);
                        }
                        list.add(new SettingsItem(item));
                    }
                }


                HashSet<Integer> uniqueItems = new HashSet<Integer>();
                uniqueItems.add(59); //default input message type
                for(List<SettingsItem> list : this.settings.values()){
                    Collections.sort(list); //Сортировка по типу входящего сообщения
                    for(SettingsItem item : list){
                        uniqueItems.add(item.inputMessageType);
                    }
                }
                Integer[] tmp = uniqueItems.toArray(new Integer[uniqueItems.size()]);
                int[] types = new int[tmp.length];
                for (int i = 0; i < types.length; i++) {
                    types[i] = tmp[i].intValue();
                }

                //Подписка на роутер
                router.registerServer(MultiplyDataServer.class, self, types);
            } catch (SettingsStorageServiceException ex) {
                LOGGER.log(Level.WARN, "Can''t find settings for MultiplyDataServer", ex);
            } catch (Exception ex) {
                LOGGER.log(Level.WARN, "Wrong settings for MultiplyDataServer", ex);
            }


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(MultiplyDataServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException {

        ValueMessage inputMessage = ValueMessage.parseFrom(message.toByteArray());
        if (inputMessage.hasValid()) {
            if (!inputMessage.getValid()) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.log(Level.DEBUG, new StringBuilder().append("Message is not valid: ").append(message).toString());
                }
                return;
            }
        }
        int sourceId = (int)inputMessage.getSourceId();
        long timeStamp = inputMessage.getTimeStamp();
        Integer inputMessageType = (int)inputMessage.getMessageType();
        int outputMessageType = 60;
        MessageValueType inputValueType = MessageValueType.INT;
        MessageValueType outputValueType = MessageValueType.DOUBLE;        
        double multiplier = 1.0;
        List<SettingsItem> list =  this.settings.get(sourceId);
        if(list!=null){

            int index = Collections.binarySearch(list, inputMessageType, null);
            if(index>=0){
                SettingsItem item = list.get(index);
                outputMessageType = item.outputMessageType;
                inputValueType = item.inputMessageValueType;
                outputValueType = item.outputMessageValueType;
                multiplier = item.multiplier;
            } // else use default values

        } // else use default values


        List<? extends Number> inputList = null;
        switch (inputValueType) {
            case INT:
                inputList = inputMessage.getIntValueList();
                break;

            case LONG:
                inputList = inputMessage.getLongValueList();
                break;

            case FLOAT:
                inputList = inputMessage.getFloatValueList();
                break;

            case DOUBLE:
                inputList = inputMessage.getDoubleValueList();
                break;

            default:
                throw new IllegalArgumentException("Unknown inputValueType");


        }

        ValueMessage.Builder valueMessageBuilder = ValueMessage.newBuilder().
                setSourceId(sourceId).
                setMessageType(outputMessageType).
                setTimeStamp(timeStamp).setValid(true);

        switch (outputValueType) {
            case INT:
                for (Number inputValue : inputList) {
                    int result = (int) (inputValue.doubleValue() * multiplier);
                    if(result<=Integer.MIN_VALUE || result>=Integer.MAX_VALUE){
                        throw new ArithmeticException("Overflow of INT");
                    }
                    valueMessageBuilder.addIntValue(result);
                }

                break;

            case LONG:

                for (Number inputValue : inputList) {
                    long result = (long) (inputValue.doubleValue() * multiplier);
                    if(result<=Long.MIN_VALUE || result>=Long.MAX_VALUE){
                        throw new ArithmeticException("Overflow of LONG");
                    }
                    valueMessageBuilder.addLongValue(result);
                }
                break;

            case FLOAT:
                for (Number inputValue : inputList) {
                    float result = (float)(inputValue.doubleValue() * multiplier);
                    if(Float.isInfinite(result)){
                        throw new ArithmeticException("Overflow of FLOAT");
                    }
                    valueMessageBuilder.addFloatValue( result);
                }
                break;

            case DOUBLE:
                for (Number inputValue : inputList) {
                    double result = inputValue.doubleValue() * multiplier;
                    if(Double.isInfinite(result)){
                        throw new ArithmeticException("Overflow of DOUBLE");
                    }
                    valueMessageBuilder.addDoubleValue(result);
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown outputValueType");


        }

        ValueMessage outputMessage = valueMessageBuilder.build();
        sendToServices(router,dataStorageService,outputMessage);
    }

    private void sendToServices(Router router,  DataStorageService dataStorageService, MessageLite message){
        router.send(message);

        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }
    

    private String getMultiplyDataServerSettingsName() {

        return "src0:multiplyDataServer:settings";

    }

    private class SettingsItem implements Comparable {

        public final Integer inputMessageType;
        public final MessageValueType inputMessageValueType;
        public final int outputMessageType;
        public final MessageValueType outputMessageValueType;
        public final double multiplier;

        public SettingsItem(MultiplyDataServerSettingsItem item) {
            this.inputMessageType = item.getInputMessageType();
            this.inputMessageValueType = MessageValueType.lookup(item.getInputMessageValueType());
            this.outputMessageType = item.getOutputMessageType();
            this.outputMessageValueType = MessageValueType.lookup(item.getOutputMessageValueType());
            this.multiplier = item.getMultiplier();
        }

        @Override
        public int compareTo(Object obj) {
            if (obj instanceof SettingsItem) {
                return this.inputMessageType.compareTo(((SettingsItem) obj).inputMessageType);
            } else
            if (obj instanceof Integer) {
                return this.inputMessageType.compareTo((Integer) obj);
            } else {
                throw new IllegalArgumentException("Argument should be instance of "+this.getClass().getName() + " or Integer");
            }
        }



    }




}
