/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alexander
 */
public enum MessageValueType {

    INT(1),
    LONG(2),
    FLOAT(3),
    DOUBLE(4);

    private final int code;
    private static final Map<Integer, MessageValueType> map = new HashMap<Integer, MessageValueType>();

    static {
        for (MessageValueType type : EnumSet.allOf(MessageValueType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private MessageValueType(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static MessageValueType lookup(int code) {
        return map.get(code);
    }

}
