#!/bin/sh

DEST_PATH=../java

##############################################
# Create common parser (for parsing headers) #
##############################################
FILE_NAME=MessageHeader.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/common
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


########################################
# Create parsers for required messages #
########################################
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/fuel
for FILE_NAME in GroupRawData.proto GroupUnsmoothedData.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done


########################################
# Create parser for required settings  #
########################################
FILE_NAME=Calibration.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/fuel
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


echo "done"
