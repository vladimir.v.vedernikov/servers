/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.common.SettingsErrorParser.SettingsErrorMessage;
import ru.omnicomm.pegasus.messaging.common.ValueMessageParser;
import ru.omnicomm.pegasus.messaging.data.fuel.GroupRawDataParser.GroupRawData;
import ru.omnicomm.pegasus.messaging.data.fuel.GroupUnsmoothedDataParser.GroupUnsmoothedData;
import ru.omnicomm.pegasus.messaging.settings.fuel.CalibrationTablesParser.FullCalibrationTablesSet;
import ru.omnicomm.pegasus.messaging.settings.fuel.CalibrationTablesParser.FullCalibrationTablesSet.FullCalibrationTable;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер калибровки.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("CalibrationServerImplementation")
public class CalibrationServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private final int blockId = 1;
    private Router router;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;

    private long currentSourceId;

    /**
     * Конструктор.
     *
     * @param router                 сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     * @param settingsService сервис хранения настроек.
     */
    @ServerConstructor
    public CalibrationServer(Router router, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }


        try{
            newProcessMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(CalibrationServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }

        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            currentSourceId = -1;
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            int[] types;

            try {
                types = getMessageTypesFromSettings();
                router.registerServer(CalibrationServer.class, self, types);
            } catch (SettingsStorageServiceException e) {
                LOGGER.log(Level.ERROR, "Can't read message types from settings", e);
            }
        } else if (signal instanceof Pause) {
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(CalibrationServer.class, self);
        }
    }

    /**
     * Возвращает массив типов сообщений из настроек,
     * на которые следует подписаться
     * @return
     * @throws ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException
     */
    private int[] getMessageTypesFromSettings() throws SettingsStorageServiceException {
        int[] types = null;
        String[] messageTypes = this.settingsService.get(blockId, "SRV_CALIBR_INBOUND_MESSAGE");
        if(messageTypes==null || messageTypes.length==0){
            LOGGER.log(Level.ERROR, "Message types from settings is null");
        }
        else{
            types = new int[messageTypes.length];
            int i = 0;
            //подписываемся на сообщения из настроек
            for(String messageType : messageTypes){
                types[i] = Integer.parseInt(messageType);
                i++;
            }
        }
        return types;
    }

    /**
     * Получение значения настройки по ключу и номеру блока
     * @param blockId
     * @param key
     * @return
     * @throws SettingsStorageServiceException
     */
    private String[] getSettings(int blockId, String key) throws SettingsStorageServiceException {
        String[] settings = settingsService.get(blockId, key);
        if(settings==null || settings.length==0){
            LOGGER.log(Level.DEBUG, "Can't read "+key+" from settings");
            return null;
        }
        LOGGER.log(Level.DEBUG, "Key: "+key);
        for(String setting : settings){
            LOGGER.log(Level.DEBUG, "setting: "+setting);
        }
        return settings;
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }
            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void newProcessMessage(MessageLite message){
        try{
            ValueMessageParser.ValueMessage valueMessage = ValueMessageParser.ValueMessage.parseFrom(message.toByteArray());
            //1)	Сервер считывает из заголовка сообщения источник полученного сообщения (SourceID),
            // временной штамп полученного сообщения (Т), а также тип сообщения (MsgType).
            long sourceId = valueMessage.getSourceId();
            long timestamp = valueMessage.getTimeStamp();
            long typeId = valueMessage.getMessageType();
            int blockId = valueMessage.getMessageBlock();

            //2)	Сервер запрашивает у сервиса настроек все настройки, для которых
            // ключ значений CALIBR_MSG_SETTINGS-<SourceId>-<MsgType>, заданный в заголовке входящего сообщения,
            // соответственно равен ключу CALIBR_MSG_SETTINGS-<SourceId>-<InMsgType>, хранящемуся в сервисе настроек
            //  Value = <OutMsgType><InDataType><OutDataType>
            String[] calibrMessageSettings = getSettings(blockId, "CALIBR_MSG_SETTINGS-"+sourceId+"-"+typeId);
            if(calibrMessageSettings==null)
                return;

            //3)	Сервер считывает из сервиса настроек все настройки с ключом CALIBR_VALUE_SETTINGS-<SourceId>-<MsgType>
            //	Value = <MaxValue>
            String[] calibrValueSettings = getSettings(blockId, "CALIBR_VALUE_SETTINGS-"+sourceId+"-"+typeId);
            //4)	Если настройки для данного ключа отсутствуют, то сообщение не обрабатывается, выполнение данной операции прерывается.
            if(calibrValueSettings==null)
                return;
            double maxValue = Double.parseDouble(calibrValueSettings[0]);

            //5)	Сервер определяет поле в сообщении, коды из которого необходимо преобразовать в значения
            // на основе считанного параметра InDataType и считывает значение этого поля.
            for (String calibrMessageSetting : calibrMessageSettings){
                String[] values = calibrMessageSetting.split("-");
                if(values == null || values.length<3){
                    LOGGER.log(Level.ERROR, "Can't parse CALIBR_MSG_SETTINGS settings. Value= "+calibrMessageSetting);
                    continue;
                }
                int outMsgType = Integer.parseInt(values[0]);
                String inDataType = values[1];
                String outDataType = values[2];

                List valueList = null;
                if(inDataType.equals("int32"))
                    valueList = valueMessage.getIntValueList();
                if(inDataType.equals("int64"))
                    valueList = valueMessage.getLongValueList();
                if(inDataType.equals("double"))
                    valueList = valueMessage.getDoubleValueList();
                //6)	Создаем пустой набор значений S.
                ArrayList s = new ArrayList();

                //Далее последовательно по каждому из элементов набора, полученного на предыдущем шаге
                for(int i = 0; i < valueList.size(); i++){
                    //a.	Сервер формирует ключ для поиска соответствующей настройки
                    //	Key = CALIBR_SETTINGS-<SourceId>-<InMsgType>-<SensorNumber>-<TableKey>
                    //	Value = <TableValue>
                    String key = "CALIBR_SETTINGS-"+sourceId+"-"+typeId+"-"+i+"-"+valueList.get(i);
                    String[] tableValue = getSettings(blockId, key);

                    //c.	Если на предыдущем шаге настройка не найдена
                    MessageLite settingsErrorMessage = getSettingsErrorMessage(sourceId, timestamp, 1);
                    sendToServices(router, dataStorageService, settingsErrorMessage);

                    //d.	Если выбранное на шаге 6c значение параметра TableValue>MaxValue, указанного в настройках,
                    // то значение данного параметра заменяется на MaxValue
                    Double tableValueL = Double.parseDouble(tableValue[0]);
                    if(tableValueL>maxValue){
                        tableValue[0] = calibrValueSettings[0];
                    }
                    //e.	Добавляем полученное на шаге «d» или «b» значение в набор значений S
                    addToS(s, outDataType, tableValue[0]);
                }
                //7)	Сервер определяет тип поля в исходящем сообщении на основе параметра OutDataType,
                // считывает тип исходящего сообщения из параметра OutMsgType
                MessageLite messageLite = getValueMessage(sourceId, outMsgType, blockId, timestamp, s, outDataType);
                sendToServices(router, dataStorageService, messageLite);
            }


        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, "Can't read data from settings", e);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
    }

    private void addToS(ArrayList s, String dataType, String value){
        if(dataType.equals("int32"))
            s.add(Integer.parseInt(value));
        if(dataType.equals("int64"))
            s.add(Long.parseLong(value));
        if(dataType.equals("double"))
            s.add(Double.parseDouble(value));
    }

    private void processMessage(MessageLite message) {

        GroupRawData groupRawData;
        try {
            groupRawData = GroupRawData.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
        
        long sourceId = groupRawData.getSourceId();
        
        if(groupRawData.hasValid() && !groupRawData.getValid()){
            LOGGER.log(Level.INFO, new StringBuilder().append("Not valid GroupRawData message (sourceId=").append(sourceId).append(", messageTime=").append(groupRawData.getMessageTime()).append(")").toString());
            return;
        }
        
        List<FullCalibrationTable> calibrationTables = this.getCalibrationTableSet((int)sourceId);
        if (calibrationTables == null) {            
            return;
        }
        List<Integer> sensorsValues = groupRawData.getIntValueList();

        List<Integer> values = new ArrayList<Integer>();
        int i;
        for (i = 0; i < sensorsValues.size(); i++) {

            if (i >= calibrationTables.size()) {
                //Обработка ситуации, при которой количество данных от датчиков превышает количество калибровочных таблиц
                LOGGER.log(Level.INFO, "Not enough calibration tables for data source " + sourceId+" (tables "+calibrationTables.size()+", data item(s) "+sensorsValues.size()+")");
                return;
            }

            FullCalibrationTable calibrationTable = calibrationTables.get(i);

            int sensorValue = sensorsValues.get(i);
            if (sensorValue >= calibrationTable.getValueCount()) {
                LOGGER.log(Level.WARN, String.format("Wrong sensor value (%d) or calibration table size (%d). Source id = %d", sensorValue, calibrationTable.getValueCount(), sourceId));
                break;
            }

            long value = calibrationTable.getValue(sensorValue);

            values.add((int)value);
        }

        GroupUnsmoothedData groupUnsmoothedData =
                getGroupUnsmoothedMessage(sourceId, groupRawData.getTimeStamp(), values);

        if (i < calibrationTables.size()) {
            //Обработка ситуации, при которой количество данных от датчиков меньше количества калибровочных таблиц
            LOGGER.log(Level.INFO, "Not enough raw data for data source " + sourceId+" (tables "+calibrationTables.size()+", data item(s) "+sensorsValues.size()+")");
            return;
        }

        sendToServices(router,dataStorageService,groupUnsmoothedData);
    }

    private void sendToServices(Router router,  DataStorageService dataStorageService, MessageLite message){
        router.send(message);

        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getValueMessage(long sourceId, long messageType, int blockId, long timeStamp, List valueList, String dataType) {
        ValueMessageParser.ValueMessage.Builder builder = ValueMessageParser.ValueMessage.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(messageType)
                .setMessageBlock(blockId)
                .setTimeStamp(timeStamp);
        if(dataType.equals("int32"))
            return builder.addAllIntValue(valueList).build();
        if(dataType.equals("int64"))
            return builder.addAllLongValue(valueList).build();
        if(dataType.equals("double"))
            return builder.addAllDoubleValue(valueList).build();
        else
            return null;
    }

    private MessageLite getSettingsErrorMessage(long sourceId, long timeStamp, int value) {
        return SettingsErrorMessage.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(SettingsErrorMessage.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setBody(value)
                .build();
    }

    private GroupUnsmoothedData getGroupUnsmoothedMessage(long vehicleId, long timeStamp, List<Integer> values) {
        return GroupUnsmoothedData.newBuilder()
                .setSourceId(vehicleId).setMessageType(GroupUnsmoothedData.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp).addAllIntValue(values)
                .build();
    }

    private String getFullCalibrationTablesSetName(int sourceId) {

        return String.format("src%d:fullCalibrationTablesSet", sourceId);

    }


    public List<FullCalibrationTable> getCalibrationTableSet(int sourceId) {

        FullCalibrationTablesSet fullCalibrationTablesSet = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getFullCalibrationTablesSetName(sourceId));
            if(settingsObject!=null){
                fullCalibrationTablesSet = FullCalibrationTablesSet.parseFrom(settingsObject.toByteArray());
            } else {
                return null;
            }
            
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load calibration tables set for source %d", sourceId), ex);
            return null;
        }

        if (fullCalibrationTablesSet == null) {
            LOGGER.log(Level.INFO, String.format("Calibration tables set for the source %d wasn't found", sourceId));
            return null;
        }

        return fullCalibrationTablesSet.getFullCalibrationTableList();

    }
}
