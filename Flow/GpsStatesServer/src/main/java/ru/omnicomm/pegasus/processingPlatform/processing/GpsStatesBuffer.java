/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser;
import ru.omnicomm.pegasus.messaging.data.location.GpsStatesParser.BadGpsEND;
import ru.omnicomm.pegasus.messaging.data.location.GpsStatesParser.BadGpsSTART;
import ru.omnicomm.pegasus.messaging.settings.location.GpsStateDefinitionSettingsParser.GpsStateDefinitionBuffer;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * Буфер алгоритма определения состояний по данным GPS 
 * @author alexander
 */
public class GpsStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private boolean currentGpsStatus = false;
    private final long sourceId;

    /**
     * Конструктор (используется при создании нового буфера)
     * @param sourceId id источника сообщений 
     */
    public GpsStatesBuffer(long sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * Конструктор (используется при создании буфера на основе накопленных ранее данных)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public GpsStatesBuffer(GpsStateDefinitionBuffer buffer) throws IllegalArgumentException {
        this(buffer.getSourceId());
        this.currentGpsStatus = buffer.getGpsStatus();
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){
        GpsStateDefinitionBuffer.Builder builder =
                GpsStateDefinitionBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(GpsStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setTimeStamp(0)
                .setMessageTime(0)
                .setGpsStatus(currentGpsStatus);
        return builder.build();
    }

    /**
     * Производит обработку данных согласно алгоритму определения состояний по GPS данным
     * @param router сервис маршрутизатора сообщений
     * @param jmsBridgeService  сервис jms-bridge
     * @param dataStorageService сервис хранения данных
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, CoordinateParser.Coordinate dataMessage) {
        boolean gpsStatus = dataMessage.getValid();
        long sourceId = dataMessage.getSourceId();
        long timestamp = dataMessage.getTimeStamp();
        int blockId = dataMessage.getMessageBlock();

        if(gpsStatus && !this.currentGpsStatus){
            this.sendToServices(router, jmsBridgeService, dataStorageService, getBadGpsENDMessage(sourceId,timestamp,blockId));
        }
        if(!gpsStatus && this.currentGpsStatus){
            this.sendToServices(router, jmsBridgeService, dataStorageService, getBadGpsSTARTMessage(sourceId,timestamp,blockId));
        }
        this.currentGpsStatus = gpsStatus;
    }

    /**
     * Отправляет сообщение на сервисы Платформы
     * @param router сервис маршрутизатора
     * @param jmsBridgeService сервис брокера сообщений
     * @param dataStorageService сервис хранилища данных
     * @param message сообщение
     */
    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    /**
     * Возвращает сообщение BadGpsSTART
     * @param sourceId id источника данных
     * @param timestamp временная метка сообщения
     * @return сообщение в формате protobuf
     */
    private MessageLite getBadGpsSTARTMessage(long sourceId, long timestamp, int blockId) {
        return BadGpsSTART.newBuilder()
                .setSourceId(sourceId).setMessageType(BadGpsSTART.getDefaultInstance().getMessageType())
                .setTimeStamp(timestamp)
                .setMessageBlock(blockId)
                .build();
    }
    
    /**
     * Возвращает сообщение BadGpsEND
     * @param sourceId id источника данных
     * @param timestamp временная метка сообщения
     * @return сообщение в формате protobuf
     */
    private MessageLite getBadGpsENDMessage(long sourceId, long timestamp, int blockId) {
        return BadGpsEND.newBuilder()
                .setSourceId(sourceId).setMessageType(BadGpsEND.getDefaultInstance().getMessageType())
                .setTimeStamp(timestamp)
                .setMessageBlock(blockId)
                .build();
    }



}
