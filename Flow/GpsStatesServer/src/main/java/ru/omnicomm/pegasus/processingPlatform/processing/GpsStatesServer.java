/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.settings.location.GpsStateDefinitionSettingsParser.GpsStateDefinitionBuffer;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер определения состояний по GPS данным
 * PP-PSS-03-(GPS States)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("GpsStatesServerImplementation")
public class GpsStatesServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private GpsStatesBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public GpsStatesServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    /**
     * Метод передает на исполнение очередное сообщение от платформы.
     *
     * @param source  отправитель сообщения.
     * @param message сообщение, полученное от платформы.
     */
    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header;
        try {
            header = MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
            return;
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            //Уведомляем сервис маршрутизатора сообщений о смене источника данных
            //(Данная процедура необходима для корректного функционирования механизма масштабирования сервера; 
            //в случае ее игнорирования масштабирования сервера производиться не будет)
            long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(GpsStatesServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    /**
     * Метод передает на исполнение сигнал, необходимый для изменения состояния сервера, включая завершение его работы,
     * инициалиация работы сервера, сигнал для перезагрузки настроек и т.п.
     *
     * @param signal полученный от платформы сигнал.
     */
    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            router.registerServer(GpsStatesServer.class, self, (int)Coordinate.getDefaultInstance().getMessageType());
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(GpsStatesServer.class, self);
        }
    }

    /**
     * Обработка сигнала о приостановке сервера
     * @param pause сигнал
     */
    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }
                
            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    /**
     * Обработка очередного сообщения, принятого от Платформы
     * @param message сообщение
     * @throws InvalidProtocolBufferException в случае некорректного формата сообщения
     */
    private void processMessage(MessageLite message) throws InvalidProtocolBufferException {

        Coordinate dataMessage = Coordinate.parseFrom(message.toByteArray());
        long sourceId = dataMessage.getSourceId();

        //При смене источника данных обрабатываемых сообщений:
        // 1. сохраним в сервис настроек буфер накопленных данных "старого" источника данных
        // 2. загрузим из сервиса настроек буфер для "нового" источника данных
        if (sourceId != this.currentSourceId) {
            if (this.currentBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getBufferName((int)this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentBuffer = this.getBuffer((int)sourceId);
            if (currentBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the GpsAggregatingServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }
        }

        if (currentBuffer == null) {
            return;
        }

        this.currentBuffer.processData(router, jmsBridgeService, dataStorageService, dataMessage);
    }

    
    /**
     * Возвращает ключ к буферу накопленной информации по заданному источнику данных в сервисе настроек
     * @param sourceId источник данных
     * @return ключ к буферу накопленной информации по заданному источнику данных в сервисе настроек
     */
    private String getBufferName(int sourceId) {
        return String.format("src%d:gpsStates:buffer", sourceId);
    }

    /**
     * Возвращает буфер накопленной информации по заданному источнику данных.
     * @param sourceId источник данных
     * @return буфер накопленной информации по заданному источнику данных
     */
    private GpsStatesBuffer getBuffer(int sourceId) {

        GpsStateDefinitionBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getBufferName(sourceId));
            if(settingsObject!=null){
                buffer = GpsStateDefinitionBuffer.parseFrom(settingsObject.toByteArray());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        //Если буфер не был найден в сервисе настроек, то создаем его
        if (buffer == null) {
            return new GpsStatesBuffer(sourceId);
        } else {
            return new GpsStatesBuffer(buffer);
        }
    }


}
