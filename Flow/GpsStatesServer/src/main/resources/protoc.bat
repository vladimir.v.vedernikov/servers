@echo off

set "DEST_PATH=../java"

rem ##############################################
rem # Create common parser (for parsing headers) #
rem ##############################################
set "FILE_NAME=MessageHeader.proto"
set "SRC_PATH=..\..\..\..\..\..\MessageStorage\src\main\resources\protobuf\common"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"


rem ########################################
rem # Create parsers for required messages #
rem ########################################
set "SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/location"
for %%A in (Coordinate.proto GpsStates.proto) do (
 	copy "%SRC_PATH%\%%A" ".\%%A"
	call protoc.exe ".\%%A"  --java_out "%DEST_PATH%"
	del ".\%%A"
)

rem ########################################
rem # Create parser for required settings  #
rem ########################################
set "FILE_NAME=GpsStateDefinition.proto"
set "SRC_PATH=..\..\..\..\..\..\MessageStorage\src\main\resources\protobuf\settings\servers\location"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"


echo "done"
