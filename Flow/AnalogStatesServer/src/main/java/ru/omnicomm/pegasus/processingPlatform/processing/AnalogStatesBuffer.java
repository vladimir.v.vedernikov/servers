/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogStatesParser.AnalogValueDOWN;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogStatesParser.AnalogValueNormal;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogStatesParser.AnalogValueUP;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogStateDefinitionSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogStateDefinitionSettingsParser.AnalogStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;


/**
 * Буфер алгоритма определения состояний аналогового датчика
 * @author alexander
 */
public class AnalogStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int valueMIN;
    private final int valueMAX;
    private final int idleMAX;
    private final int timeInterval;

    private final List<AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data> buffer;

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний аналогового датчика для заданного источника данных
     *                 (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public AnalogStatesBuffer(AnalogStateDefinitionSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.valueMIN = settings.getValueMIN();
        this.valueMAX = settings.getValueMAX();
        this.idleMAX = settings.getIdleMAX();
        this.timeInterval = settings.getTimeInterval();
        this.buffer = new ArrayList<AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data>();
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний аналогового датчика для заданного источника данных
     *                 (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public AnalogStatesBuffer(AnalogStateDefinitionSettings settings,
            AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.getValueMIN() == buffer.getValueMIN() &&
           settings.getValueMAX() == buffer.getValueMAX() &&
           settings.getIdleMAX() == buffer.getIdleMAX() &&
           settings.getTimeInterval() == buffer.getTimeInterval()){
           
            this.buffer.addAll(buffer.getBufferList());

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        return AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setValueMIN(this.valueMIN)
                .setValueMAX(this.valueMAX)
                .setIdleMAX(this.idleMAX)
                .setTimeInterval(this.timeInterval)
                .addAllBuffer(this.buffer)
                .build();

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и 
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, AnalogValueCorrectedParser.AnalogValueCorrected dataMessage) {
        int messageTime = dataMessage.getMessageTime();
        long timeStamp = dataMessage.getTimeStamp();
        int blockId = dataMessage.getMessageBlock();
        int value = dataMessage.getIntValueList().get(0).intValue();
        int t = (int)timeStamp/1000;

        int bufferSize = this.buffer.size();
        if(bufferSize>0){

            boolean overMin = startThreshold(this.valueMIN);
            boolean notOverMin  = endThreshold(t,value,this.valueMIN);
            boolean overMax = startThreshold(this.valueMAX);
            boolean notOverMax  = endThreshold(t,value,this.valueMAX);

            long t_res;
            int v_res;
            if(overMax){
                t_res = this.buffer.get(bufferSize-1).getDataId()*1000;
                v_res = this.buffer.get(bufferSize-1).getValue();

//                if(bufferSize>1 && this.buffer.get(bufferSize-1).getDataId()-this.buffer.get(bufferSize-2).getDataId()<this.idleMAX){
//                    t_res = this.buffer.get(bufferSize-2).getDataId();
//                } else {
//                    t_res = this.buffer.get(bufferSize-1).getDataId() - this.timeInterval;
//                }
//
//                v_res = v;

                sendToServices(router, jmsBridgeService, dataStorageService, getAnalogValueUPMessage(this.sourceId, t_res, v_res, blockId));
                
            }

            if(notOverMax){
//                v_res = v;
//                t_res = this.buffer.get(bufferSize-1).getDataId();
                if(timeStamp-this.buffer.get(bufferSize-1).getDataId()*1000<this.idleMAX){
                    t_res = timeStamp;
                    v_res = value;
                } else {
                    t_res = (this.buffer.get(bufferSize-1).getDataId()+1)*1000;
                    v_res = 0;
                }

                if(notOverMin){
                    sendToServices(router, jmsBridgeService, dataStorageService, getAnalogValueDOWNMessage(this.sourceId, t_res, v_res,blockId));
                } else {
                    sendToServices(router, jmsBridgeService, dataStorageService, getAnalogValueNormalMessage(this.sourceId, t_res, v_res,blockId));
                }
            }

            //10)	Если OverMAX OR NotOverMax = True, Сервер переходит на шаг 13)
            if(!(overMax || notOverMax)){

                if(overMin){
//                    if(bufferSize>1 && this.buffer.get(bufferSize-1).getDataId()-this.buffer.get(bufferSize-2).getDataId()<this.idleMAX){
//                        t_res = this.buffer.get(bufferSize-2).getDataId();
//                    } else {
//                        t_res = this.buffer.get(bufferSize-1).getDataId() - this.timeInterval;
//                    }
//                    v_res = v;

                    t_res = this.buffer.get(bufferSize-1).getDataId()*1000;
                    v_res = this.buffer.get(bufferSize-1).getValue();

                    sendToServices(router, jmsBridgeService, dataStorageService, getAnalogValueNormalMessage(this.sourceId, t_res, v_res,blockId));
                }

                if(notOverMin){
//                    v_res = v;
//                    t_res = this.buffer.get(bufferSize-1).getDataId();
                    if(timeStamp-this.buffer.get(bufferSize-1).getDataId()*1000<this.idleMAX){
                        t_res = timeStamp;
                        v_res = value;
                    } else {
                        t_res = (this.buffer.get(bufferSize-1).getDataId()+1)*1000;
                        v_res = 0;
                    }

                    sendToServices(router, jmsBridgeService, dataStorageService, getAnalogValueDOWNMessage(this.sourceId, t_res, v_res,blockId));

                }

            }
            
        }

        //"шаг 13"
        this.buffer.add(AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data.newBuilder().setDataId(t).setValue(value).build());
        //в случае непоследовательных данных - сортируем буфер
        if(this.buffer.size()>1 && (this.buffer.get(this.buffer.size()-1).getDataId() <  this.buffer.get(this.buffer.size()-2).getDataId())){
            Collections.sort(buffer, new Comparator<AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data>() {
                @Override
                public int compare(AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data obj1, AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data obj2) {
                    return ((Integer) obj1.getDataId()).compareTo(obj2.getDataId());
                }
            });
        }

        cleanupBuffer();
        


    }

    private AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data getMaxData(){
        return Collections.max(buffer, new Comparator<AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data>() {
            @Override
            public int compare(AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data obj1, AnalogStateDefinitionSettingsParser.AnalogStateDefinitionBuffer.Data obj2) {
                return ((Integer) obj1.getDataId()).compareTo(obj2.getDataId());
            }
        });
    }

    private boolean startThreshold(int threshold){

        int bufferSize = this.buffer.size();
        int t_max = this.buffer.get(bufferSize-1).getDataId();
        int v_1 =   this.buffer.get(bufferSize-1).getValue();

        if(bufferSize == 1) {
            return v_1 > threshold;
        }

        int t_min = this.buffer.get(bufferSize-2).getDataId();
        int v_0 =   this.buffer.get(bufferSize-2).getValue();

        if(t_max - t_min >= this.idleMAX){
            return v_1 > threshold;
        } else {
            return v_1 > threshold && v_0 <= threshold;
        }


    }


    private boolean endThreshold(int t, int v, int threshold){

        int bufferSize = this.buffer.size();
        int t_max = this.buffer.get(bufferSize-1).getDataId();
        int v_1 =   this.buffer.get(bufferSize-1).getValue();

        if(v_1 <= threshold){
            return false;
        }

        if(t - t_max >= this.idleMAX ){
            return true;
        }

        return v <= threshold;


    }


    private void cleanupBuffer(){
        while(this.buffer.size()>2){
            this.buffer.remove(0);
        }
    }


    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getAnalogValueUPMessage(int sourceId, long timestamp, int value, int blockId) {
        return AnalogValueUP.newBuilder()
                .setSourceId(sourceId).setMessageType(AnalogValueUP.getDefaultInstance().getMessageType())
                .setMessageBlock(blockId)
                .setTimeStamp(timestamp)
                .setValue(value)
                .build();
    }

    private MessageLite getAnalogValueDOWNMessage(int sourceId, long timestamp, int value, int blockId) {
        return AnalogValueDOWN.newBuilder()
                .setSourceId(sourceId).setMessageType(AnalogValueDOWN.getDefaultInstance().getMessageType())
                .setMessageBlock(blockId)
                .setTimeStamp(timestamp)
                .setValue(value)
                .build();
    }

    private MessageLite getAnalogValueNormalMessage(int sourceId, long timestamp, int value, int blockId) {
        return AnalogValueNormal.newBuilder()
                .setSourceId(sourceId).setMessageType(AnalogValueNormal.getDefaultInstance().getMessageType())
                .setMessageBlock(blockId)
                .setTimeStamp(timestamp)
                .setValue(value)
                .build();
    }


}
