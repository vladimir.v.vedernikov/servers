/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogAggregatedDataParser.AGRAnalogAvr;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogAggregatedDataParser.AGRAnalogMax;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogAggregatedDataParser.AGRAnalogWork;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogAggregatingSettingsParser;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;


/**
 * Буфер алгоритма агрегации данных аналогового датчика
 * @author alexander
 */
public class AnalogAggregatingBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final long sourceId;
    private final int idleMax;
    private final int aggrInterval;
    private final List<AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData> buffer;


    /**
     * Конструктор
     * @param settings настройки алгоритма агрегации данных аналогового датчика для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public AnalogAggregatingBuffer(AnalogAggregatingSettingsParser.AnalogAggregatingSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.idleMax = settings.getIdleMax();
        this.aggrInterval = settings.getAggrInterval();
        this.buffer = new ArrayList<AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData>();

        if(this.aggrInterval<=0){
            throw new IllegalArgumentException("aggrInterval should be a positive value");
        }
    }


    /**
     * Конструктор
     * @param settings настройки алгоритма агрегации данных аналогового датчика для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public AnalogAggregatingBuffer(AnalogAggregatingSettingsParser.AnalogAggregatingSettings settings,
            AnalogAggregatingSettingsParser.AnalogAggregatingBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.getIdleMax() == buffer.getIdleMax() &&
           settings.getAggrInterval() == buffer.getAggrInterval()){
           
            this.buffer.addAll(buffer.getBufferList());

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        return AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setTimeStamp(0)
                .setIdleMax(this.idleMax)
                .setAggrInterval(this.aggrInterval)
                .addAllBuffer(this.buffer)
                .build();

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и 
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param t время
     * @param v скорость
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, int t, int v) {

        int t_0, t_1;

       /*
         "Сервер определяет для данного буфера минимальный временной штамп
         содержащихся в нём сообщений (Тмин). Сервер определяет минимальное 
         число T0, большее чем Тмин, представляющееся в виде T0 = k*AgrInterval, 
         где k – целое неотрицательное число. Если буфер пустой, T0 определяется 
         как максимальное число, такое что 
         T0 = k*AgrInterval И T0<T – AgrInterval (k - неотрицательное целое, в 
         случае, если T<AgrInterval, устанавливаем T0=0)."        
       */
        if(this.buffer.size()>0){
            int t_min = (int)this.buffer.get(0).getDataId()/1000;
            t_0 = (t_min/this.aggrInterval +1)*this.aggrInterval;
        } else {
            if(t < this.aggrInterval){
                t_0 = 0;
            } else {
                int k = t / this.aggrInterval;
                t_0 = t+1;
                while(t_0 >= t){
                    t_0 = (k--) * this.aggrInterval;
                }
            }
            
        }

        this.buffer.add(AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData.newBuilder().setDataId(t).setValue(v).build());
        //в случае непоследовательных данных - сортируем буфер
        if(this.buffer.size()>1 && (this.buffer.get(this.buffer.size()-1).getDataId() <  this.buffer.get(this.buffer.size()-2).getDataId())){
            Collections.sort(buffer, new Comparator<AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData>() {
                @Override
                public int compare(AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData obj1, AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData obj2) {
                    return ((Long) obj1.getDataId()).compareTo(obj2.getDataId());
                }
            });
        }   

        t_1 = t_0 + this.aggrInterval;

        while(t_1<t){

            int work = workCalculation(t_0,t_1);
            int avgValue = work/this.aggrInterval;
            int maxValue = maxValueCalculation(t_0, t_1);

            sendToServices(router, jmsBridgeService, dataStorageService, getAGRAnalogWorkMessage(sourceId, t_1, work));
            sendToServices(router, jmsBridgeService, dataStorageService, getAGRAnalogAvrMessage(sourceId, t_1, avgValue));
            sendToServices(router, jmsBridgeService, dataStorageService, getAGRAnalogMaxMessage(sourceId, t_1, maxValue));

            cleanupBuffer(t_1);
            
            t_0 = t_1;
            t_1 = t_0 + this.aggrInterval;
        }

    }

    private int workCalculation(int t_0, int t_1){
        int m = 0;

        //Находим в буфере индексы сообщений, соответствующих началу и концу периода
        int startIndex = -1, endIndex = -1;
        for (int i = 0; i < this.buffer.size(); i++) {
            if(startIndex < 0 && this.buffer.get(i).getDataId() >= t_0){
                startIndex = i;
            }
            if(this.buffer.get(i).getDataId() <= t_1){
                endIndex = i;
            }
        }
        
        if(startIndex<0 || endIndex<0){
            return m;
        }

        if(endIndex<startIndex){
            LOGGER.warn("Work calculation error; buffer: "+this.getMessageLite());
            return m;
        }

        //Считаем пробег
        int t_bgn = (int)this.buffer.get(startIndex).getDataId()/1000;
        int t_end = (int)this.buffer.get(endIndex).getDataId()/1000;
        for (int i = startIndex; i <= endIndex; i++) {

            int v = this.buffer.get(i).getValue();
            int tau = 0;
            int tau_max = this.idleMax;
            if(i>0){

                int t_i = (int)this.buffer.get(i).getDataId()/1000;
                int t_i_minus_1 = (int)this.buffer.get(i-1).getDataId()/1000;

                if(t_i - t_i_minus_1 < tau_max) {
                    tau = min(t_i,t_end)-max(t_i_minus_1,t_bgn);
                }

            } 
            m+=(v*tau);

        }

        return m;

    }

    private int maxValueCalculation(int t_0, int t_1){
        int v_max = 0;

        //Находим в буфере индексы сообщений, соответствующих началу и концу периода
        int startIndex = -1, endIndex = -1;
        for (int i = 0; i < this.buffer.size(); i++) {
            if(startIndex < 0 && this.buffer.get(i).getDataId() > t_0){
                startIndex = i;
            }
            if(this.buffer.get(i).getDataId() <= t_1){
                endIndex = i;
            }
        }

        if(startIndex<0 || endIndex<0){
            return v_max;
        }

        if(endIndex<startIndex){
            LOGGER.warn("Max value calculation error; buffer: "+this.getMessageLite());
            return v_max;
        }
        
        for (int i = startIndex; i < endIndex; i++) {
            int tau = 0;
            int tau_max = this.idleMax;
            if(i>0){

                int t_i = (int)this.buffer.get(i).getDataId()/1000;
                int t_i_minus_1 = (int)this.buffer.get(i-1).getDataId()/1000;

                if(t_i - t_i_minus_1 < tau_max) {
                    tau = 1;
                }

            }

            int v = this.buffer.get(i).getValue() * tau;
            if(v>v_max){
                v_max = v;
            }
        }



        return v_max;
    }

    private void cleanupBuffer(int t_1){

        int t_next = -1;
        for(AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.BasicData data : this.buffer){
            int t = (int)data.getDataId()/1000;
            if(t < t_1){
                t_next = t;
            } else {
                break;
            }
        }

        while(this.buffer.size()>0){
            int t = (int)this.buffer.get(0).getDataId()/1000;
            if(t < t_next){
                this.buffer.remove(0);
            } else {
                break;
            }
        }


    }

    private int min(int a, int b){
        return a < b ? a : b;
    }

    private int max(int a, int b){
        return a > b ? a : b;
    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getAGRAnalogWorkMessage(long sourceId, int messageTime, int value) {
        return AGRAnalogWork.newBuilder()
                .setSourceId(sourceId).setMessageType(AGRAnalogWork.getDefaultInstance().getMessageType())
                .setTimeStamp(messageTime*1000)
                //.setMessageTime(messageTime)
                .setValue(value)
                .build();
    }

    private MessageLite getAGRAnalogAvrMessage(long sourceId, int messageTime, int value) {
        return AGRAnalogAvr.newBuilder()
                .setSourceId(sourceId).setMessageType(AGRAnalogAvr.getDefaultInstance().getMessageType())
//                .setMessageTime(messageTime)
                .setTimeStamp(messageTime*1000)
                .setValue(value)
                .build();
    }

    private MessageLite getAGRAnalogMaxMessage(long sourceId, int messageTime, int value) {
        return AGRAnalogMax.newBuilder()
                .setSourceId(sourceId).setMessageType(AGRAnalogMax.getDefaultInstance().getMessageType())
                //.setMessageTime(messageTime)
                .setTimeStamp(messageTime*1000)
                .setValue(value)
                .build();
    }








}
