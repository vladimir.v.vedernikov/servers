/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser.AnalogValueCorrected;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogAggregatingSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogAggregatingSettingsParser.AnalogAggregatingSettings;


import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.messages.Pause;
import ru.omnicomm.pegasus.processingPlatform.messages.Paused;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер агрегации данных аналогового датчика;
 * реализация согласно требованиям PP-PSS-09-(Analog Aggregating)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("AnalogAggregatingServerImplementation")
public class AnalogAggregatingServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private AnalogAggregatingBuffer currentAnalogAggregatingBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public AnalogAggregatingServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = (int)header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(AnalogAggregatingServer.class, self, (int)currentSourceId, sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            router.registerServer(AnalogAggregatingServer.class, self, (int)AnalogValueCorrected.getDefaultInstance().getMessageType());
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(AnalogAggregatingServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException, IllegalArgumentException {

        AnalogValueCorrected dataMessage = AnalogValueCorrected.parseFrom(message.toByteArray());
        long sourceId = dataMessage.getSourceId();
        if (sourceId != this.currentSourceId) {
            if (this.currentAnalogAggregatingBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentAnalogAggregatingBuffer.getMessageLite(), this.getAnalogAggregatingBufferName((int)this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentAnalogAggregatingBuffer = this.getAnalogAggregatingBuffer((int)sourceId);
            if (currentAnalogAggregatingBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the AnalogAggregatingServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentAnalogAggregatingBuffer == null) {
            return;
        }

        int messageTime = dataMessage.getMessageTime();
        if(dataMessage.getIntValueList().size()!=1){
            throw new IllegalArgumentException("Wrong message: "+dataMessage);
        }

        int value = dataMessage.getIntValueList().get(0).intValue();

        this.currentAnalogAggregatingBuffer.processData(router,jmsBridgeService,dataStorageService,messageTime,value);
    }

    private String getAnalogAggregatingBufferName(int sourceId) {

        return String.format("src%d:analogAggregating:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:analogAggregating:settings", sourceId);

    }

    private AnalogAggregatingBuffer getAnalogAggregatingBuffer(int sourceId) {

        AnalogAggregatingSettingsParser.AnalogAggregatingSettings settings;
        AnalogAggregatingSettingsParser.AnalogAggregatingBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                
                AnalogAggregatingSettings defaultInstance = AnalogAggregatingSettings.getDefaultInstance();
                settings = AnalogAggregatingSettings.newBuilder()

                        .setSourceId(sourceId)
                        .setMessageType(defaultInstance.getMessageTime())
                        .setMessageTime(0)

                        .setIdleMax(defaultInstance.getIdleMax())
                        .setAggrInterval(defaultInstance.getAggrInterval())

                        .build();
            } else {
                settings = AnalogAggregatingSettingsParser.AnalogAggregatingSettings.parseFrom(settingsObject.toByteArray());
            }
            
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getAnalogAggregatingBufferName(sourceId));
            if(settingsObject!=null){
                buffer = AnalogAggregatingSettingsParser.AnalogAggregatingBuffer.parseFrom(settingsObject.toByteArray());
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new AnalogAggregatingBuffer(settings);
        } else {
            return new AnalogAggregatingBuffer(settings, buffer);
        }
    }
    






}
