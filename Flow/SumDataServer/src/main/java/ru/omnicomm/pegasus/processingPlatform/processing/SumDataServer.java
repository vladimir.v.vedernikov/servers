/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.data.fuel.GroupUnsmoothedDataParser.GroupUnsmoothedData;
import ru.omnicomm.pegasus.messaging.data.fuel.SumUnsmoothedDataParser.SumUnsmoothedData;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.messages.Pause;
import ru.omnicomm.pegasus.processingPlatform.messages.Paused;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;

/**
 * Сервер суммирования.
 * Основным функционалом сервера суммирования является определение с помощью суммирования объема топлива/жидкости.
 * Входной информацией для сервера суммирования является объем топлива в литрах, рассчитанный сервером калибровки
 * для каждого отдельного датчика.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("SumDataServerImplementation")
public class SumDataServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    /**
     * Ссылка на сервис маршрутизатора сообщений
     */
    private Router router;
    private DataStorageService dataStorageService;
    private Server self;

    private int currentSourceId;

    /**
     * Конструктор.
     *
     * @param router сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     */
    @ServerConstructor
    public SumDataServer(Router router, DataStorageService dataStorageService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
            return;
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(SumDataServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }
            
        }


    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            currentSourceId = -1;
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();

            router.registerServer(SumDataServer.class, self, GroupUnsmoothedData.getDefaultInstance().getMessageType());
        } else if (signal instanceof Pause) { 
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) { 
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(SumDataServer.class, self);
        }

    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }
            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message) {

        GroupUnsmoothedData groupUnsmoothedData;
        try {
            groupUnsmoothedData = GroupUnsmoothedData.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }



        int sumValue = 0;
        for (int value : groupUnsmoothedData.getIntValueList()) {
            sumValue += value;
        }

        SumUnsmoothedData sumUnsmoothedData =
                getSumUnsmoothedMessage(groupUnsmoothedData.getSourceId(), groupUnsmoothedData.getMessageTime(), sumValue);

        sendToServices(router,dataStorageService,sumUnsmoothedData);
    }

    private void sendToServices(Router router,  DataStorageService dataStorageService, MessageLite message){
        router.send(message);

        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private SumUnsmoothedData getSumUnsmoothedMessage(int sourceId, int messageTime, int sumValue) {
        return SumUnsmoothedData.newBuilder()
                .setSourceId(sourceId).setMessageType(SumUnsmoothedData.getDefaultInstance().getMessageType())
                .setMessageTime(messageTime).setValue(sumValue)
                .build();
    }
}
