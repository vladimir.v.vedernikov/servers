/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.binarySensor.BinaryStateParser.BinaryStateOFF;
import ru.omnicomm.pegasus.messaging.data.binarySensor.BinaryStateParser.BinaryStateON;
import ru.omnicomm.pegasus.messaging.data.binarySensor.BinaryValueParser.BinaryState;
import ru.omnicomm.pegasus.messaging.settings.binarySensor.BinaryStateDefinitionSettingsParser.BinaryStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.binarySensor.BinaryStateDefinitionSettingsParser.BinaryStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;


/**
 * Буфер алгоритма определения состояний бинарного датчика
 * @author alexander
 */
public class BinaryStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int idleMAX;

    private final List<BinaryStateDefinitionBuffer.Data> buffer;

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний бинарного датчика для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public BinaryStatesBuffer(BinaryStateDefinitionSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.idleMAX = settings.getIdleMAX();
        this.buffer = new ArrayList<BinaryStateDefinitionBuffer.Data>();

     
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний бинарного датчика для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public BinaryStatesBuffer(BinaryStateDefinitionSettings settings,
            BinaryStateDefinitionBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(
           settings.getIdleMAX() == buffer.getIdleMAX()){
           
            this.buffer.addAll(buffer.getBufferList());

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        return BinaryStateDefinitionBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(BinaryStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setIdleMAX(this.idleMAX)
                .addAllBuffer(this.buffer)
                .build();

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и 
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, BinaryState dataMessage) {
        long t = dataMessage.getTimeStamp();
        boolean v = dataMessage.getValue();
        int blockId = dataMessage.getMessageBlock();

        int time = (int)t/1000;
        ResultSet rs = isON(time,v);
        boolean isOn = rs.result;        
        long t_on = rs.timeStamp;
        rs = isOFF(time,v);
        boolean isOff = rs.result;
        long t_off = rs.timeStamp;
        
        if(!buffer.isEmpty() && (isOn || isOff)){
            long t_buf = this.buffer.get(this.buffer.size()-1).getDataId()*1000;
            
            if(t-t_buf>1000){
                sendToServices(jmsBridgeService,dataStorageService,getBinaryStateMessage(sourceId,t_buf+999,false, blockId));
            }
            if(t-t_buf>2000){
                sendToServices(jmsBridgeService,dataStorageService,getBinaryStateMessage(sourceId,t-999,false, blockId));
            }
            
        }
        
        if(isOff){
            sendToServices(router, jmsBridgeService,dataStorageService,getBinaryStateOFFMessage(sourceId,t_off, blockId));
        }
        
        if(isOn){
            sendToServices(router, jmsBridgeService,dataStorageService,getBinaryStateONMessage(sourceId,t_on, blockId));
        }

        this.buffer.add(BinaryStateDefinitionBuffer.Data.newBuilder().setDataId(time).setValue(v).build());
        
        cleanupBuffer();
       
    }


    private void cleanupBuffer(){
        Collections.sort(buffer, new Comparator<BinaryStateDefinitionBuffer.Data>() {
            @Override
            public int compare(BinaryStateDefinitionBuffer.Data obj1, BinaryStateDefinitionBuffer.Data obj2) {
                return ((Integer) obj1.getDataId()).compareTo(obj2.getDataId());
            }
        });
        
        while(this.buffer.size()>1){
            this.buffer.remove(0);
        }
    }

    private ResultSet isON(long t, boolean v){
        if(this.buffer.isEmpty()){
            return new ResultSet(v,t);
        }
        
        long t_buf = this.buffer.get(this.buffer.size()-1).getDataId()*1000;
        boolean v_buf = this.buffer.get(this.buffer.size()-1).getValue();

        if(t-t_buf>=idleMAX){
            return new ResultSet(v,t);          
        }
        
        if(v && !v_buf){
            return new ResultSet(true,t); 
        }
        
        return new ResultSet(false,t); 

    }

    private ResultSet isOFF(long t, boolean v){
        
        if(buffer.isEmpty()){
            return new ResultSet(!v,t); 
        }
        
        int t_buf = this.buffer.get(this.buffer.size()-1).getDataId()*1000;
        boolean v_buf = this.buffer.get(this.buffer.size()-1).getValue();
        
        if(t-t_buf>=idleMAX && v_buf){
            return new ResultSet(true,t_buf);          
        }
        
        if(!v && v_buf){
            return new ResultSet(true,t_buf); 
        }
        
        return new ResultSet(false,t_buf); 


    }


    private void sendToServices(JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getBinaryStateMessage(int sourceId, long timeStamp, boolean value, int blockId) {
        return BinaryState.newBuilder()
                .setSourceId(sourceId).setMessageType(BinaryState.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setMessageBlock(blockId)
                .setValue(value)
                .build();
    }

    private MessageLite getBinaryStateONMessage(int sourceId, long timeStamp, int blockId) {
        return BinaryStateON.newBuilder()
                .setSourceId(sourceId).setMessageType(BinaryStateON.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setMessageBlock(blockId)
                .build();
    }

    private MessageLite getBinaryStateOFFMessage(int sourceId, long timeStamp, int blockId) {
        return BinaryStateOFF.newBuilder()
                .setSourceId(sourceId).setMessageType(BinaryStateOFF.getDefaultInstance().getMessageType())
                .setTimeStamp(timeStamp)
                .setMessageBlock(blockId)
                .build();
    }
    
    private class ResultSet {
        
        private final boolean result;
        private final long timeStamp;

        public ResultSet(boolean result, long timeStamp) {
            this.result = result;
            this.timeStamp = timeStamp;
        }
        
        


    }


}
