/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingORRefuelingBegin;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingORRefuelingEnd;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingSTART;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingSTART;
import ru.omnicomm.pegasus.messaging.data.fuel.UnsmoothedDataParser.UnsmoothedData;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser.SearchingBuffer.FuelData;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser.SearchingBuffer.OutputData;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser.SearchingBuffer.ProcessId;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;


/**
 * Буфер данных для поиска сливов и заправок.
 *
 * @author alexander
 */
public class SearchingBuffer implements Serializable {

    private static final long serialVersionUID = 8767179953303658414L;
    private static final Logger LOGGER = LoggerFactory.getLogger();

    private SearchingBufferState state;
    private final List<FuelData> basicBuffer;
    private final List<FuelData> accumulationBuffer;
    private final List<OutputData> outputBuffer;
    private final Comparator<OutputData> outputDataComparator;

    private final int sourceId;

    private final int drainingTreshold;
    private final int refuelingTreshold;
    private final int dutyConsumption;
    private final int filterLength;

    private int startIndex;
    private int endIndex;

    private int prevDataId = -1;

    private ProcessId lastProcess;

    /**
     * Конструктор
     *
     * @param settings параметры алгоритма поиска сливов и заправок
     */
    public SearchingBuffer(DrainingRefuelingSearchingParser.DrainingRefuelingSearchingSettings settings) {
        
        this.state = SearchingBufferState.ORDINAL;
        this.basicBuffer = new LinkedList<FuelData>();
        this.accumulationBuffer = new LinkedList<FuelData>();
        this.outputBuffer = new LinkedList<OutputData>();
        this.outputDataComparator = new Comparator<OutputData>() {

            @Override
            public int compare(OutputData o1, OutputData o2) {
                return ((Integer) o1.getDataId()).compareTo(o2.getDataId());
            }


        };

        this.sourceId = settings.getSourceId();
        this.drainingTreshold = settings.getDrainingTreshold();
        this.refuelingTreshold = settings.getRefuelingTreshold();
        this.filterLength = settings.getFilterLength();
        this.dutyConsumption = settings.getDutyConsumption();

        this.lastProcess = ProcessId.ORDINAL;
              
    }

    /**
     * Конструктор
     * 
     * @param settings параметры алгоритма поиска сливов и заправок
     * @param buffer накопленные данные
     */
    public SearchingBuffer(DrainingRefuelingSearchingParser.DrainingRefuelingSearchingSettings settings,
            DrainingRefuelingSearchingParser.SearchingBuffer buffer) {

        this(settings);
        if(settings.getDrainingTreshold()==buffer.getDrainingTreshold() &&
           settings.getRefuelingTreshold()==buffer.getRefuelingTreshold()&&
           settings.getFilterLength()==buffer.getFilterLength()&&
           settings.getDutyConsumption()==buffer.getDutyConsumption()){

            this.state = SearchingBufferState.lookup(buffer.getState());

            this.basicBuffer.addAll(buffer.getBasicBufferList());
            this.accumulationBuffer.addAll(buffer.getAccumulationBufferList());
            this.outputBuffer.addAll(buffer.getOutputBufferList());

            this.startIndex = buffer.getStartIndex();
            this.endIndex = buffer.getEndIndex();
            this.prevDataId = buffer.getPrevDataId();

            this.lastProcess = buffer.getLastProcess();
        }

    }

    /**
     * Возвращает представление объекта в формате google protobuffer
     * @return представление объекта в формате google protobuffer
     */
    public MessageLite getMessageLite(){

        return DrainingRefuelingSearchingParser.SearchingBuffer.newBuilder().setSourceId(this.sourceId).
                setMessageType(DrainingRefuelingSearchingParser.SearchingBuffer.getDefaultInstance().getMessageType()). //newBuilder().getDefaultInstanceForType().getMessageType()
                setMessageTime(0).
                setState(this.state.getCode()).
                addAllBasicBuffer(this.basicBuffer).
                addAllAccumulationBuffer(this.accumulationBuffer).
                addAllOutputBuffer(this.outputBuffer).
                
                setDrainingTreshold(this.drainingTreshold).
                setRefuelingTreshold(this.refuelingTreshold).
                setDutyConsumption(this.dutyConsumption).
                setFilterLength(this.filterLength).
                setStartIndex(this.startIndex).
                setEndIndex(this.endIndex).
                setPrevDataId(this.prevDataId).
                setLastProcess(this.lastProcess).
                setPrevValue(0).
                build();

    }




    /**
     * Обрабатывает новые данные.
     *
     * @param router роутер (для отправки сообщений с зафиксированными заправками или сливами, а также для отправки сообщений на сервер сглаживания данных)
     * @param dataId временная метка сообщения
     * @param value  значение уровня топлива
     */
    public void processData(Router router, DataStorageService dataStorageService, int dataId, int value) {

        //проверка на последовательность данных
        if(dataId<=prevDataId){
            //Если нарушена последовательность данных, то считаем, что начался пересчет
            this.accumulationBuffer.clear();
            this.basicBuffer.clear();
            this.outputBuffer.clear();
            this.state = SearchingBufferState.ORDINAL;

            LOGGER.info("Current message time ("+dataId+" "+getDate(dataId) +") less then previous one ("+prevDataId+" "+getDate(prevDataId) +"). Reset searching algorithm for data source "+this.sourceId);

        }

        this.prevDataId = dataId;

        this.outputBuffer.add(makeOutputData(dataId, value, true));
        this.addToBuffer(makeFuelData(dataId, value));
        

        SearchingBufferState prevState = this.state;
        SearchingBufferState currentState = this.analyzeBuffer();

        


        switch (currentState) {
            case DRAINING_DETECTED:
            case REFUELING_DETECTED:
                sendToServices(router,dataStorageService,getDrainingOrRefuelingBeginsMessage(sourceId,getStartDataId()));
                sendToServices(router,dataStorageService,getDrainingOrRefuelingEndsMessage(sourceId,getEndDataId(), getEndValue() - getStartValue()));

                if(currentState == SearchingBufferState.DRAINING_DETECTED){
                    sendToServices(router,dataStorageService,getDrainingBeginMessage(sourceId,getStartDataId()));
                    sendToServices(router,dataStorageService,getDrainingEndMessage(sourceId,getEndDataId(), getStartValue() - getEndValue() ));
                    this.lastProcess = ProcessId.DRAINING;
                } else {
                    sendToServices(router,dataStorageService,getRefuelingBeginMessage(sourceId,getStartDataId()));
                    sendToServices(router,dataStorageService,getRefuelingEndMessage(sourceId,getEndDataId(), getEndValue() - getStartValue() ));
                    this.lastProcess = ProcessId.REFUELING;
                }

                int index = Collections.binarySearch(this.outputBuffer, makeOutputData(getStartDataId(), 0, true), outputDataComparator);
                if(index<0) {
                    LOGGER.warn("Output buffer error");
                    this.outputBuffer.clear();
                }
                int drLength = getEndDataId() - getStartDataId();
                for (int i = index; i < index + drLength; i++) {
                    if(i>=this.outputBuffer.size()){
                        break;
                    }
                    OutputData oldItem = this.outputBuffer.get(i);
                    OutputData newItem = makeOutputData(oldItem.getDataId(), oldItem.getValue(), false);
                    this.outputBuffer.set(i, newItem);
                }
                
                
                
                break;
            case ORDINAL:
                //Нормальный расход топлива: отправляем данные на сглаживание
                flushOutputBuffer(router);
                
                break;


        }

        if (prevState != currentState && (prevState == SearchingBufferState.MAYBE_DRAINING_DATA_ACCUMULATION || prevState == SearchingBufferState.MAYBE_REFUELING_DATA_ACCUMULATION)) {
            //Зафиксирован переход из состояния накопления данных для определения факта заправки/слива
            //Анализировать данные, по которым считалось среднее
            //Т.к. буфер будет использован уже для нового анализа, копируем данные из буфера в массив
            FuelData[] accumulatedData = this.accumulationBuffer.toArray(new FuelData[this.accumulationBuffer.size()]);

            for (int i = 1; i < accumulatedData.length; i++) {

                currentState = analyzeBuffer();
                this.addToBuffer(accumulatedData[i]);

            }

        }

        //Удаляем неактуальные данные
        while (currentState == SearchingBufferState.ORDINAL && this.basicBuffer.size() > this.filterLength + 2) {
            this.basicBuffer.remove(0);
        }
    }

    private UnsmoothedData getUnsmoothedMessage(int sourceId, int messageTime, int value, boolean smoothable) {
        return UnsmoothedData.newBuilder()
                .setSourceId(sourceId).setMessageType(UnsmoothedData.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(value).setSmoothable(smoothable)
                .build();
    }

//    private MessageLite getRefuelingMessage(int sourceId, int messageTime, int startDataId, int endDataId, int volume) {
//        return MessageParser.RefuelingData.newBuilder()
//                .setSourceId(sourceId).setMessageType(MessageType.REFUELING_DATA.getCode()).setMessageTime(messageTime)
//                .setStartDataId(startDataId).setEndDataId(endDataId).setVolume(volume)
//                .build();
//    }
//
//    private MessageLite getDrainingMessage(int sourceId, int messageTime, int startDataId, int endDataId, int volume) {
//        return MessageParser.DrainingData.newBuilder()
//                .setSourceId(sourceId).setMessageType(MessageType.DRAINING_DATA.getCode()).setMessageTime(messageTime)
//                .setStartDataId(startDataId).setEndDataId(endDataId).setVolume(volume)
//                .build();
//    }

    private MessageLite getDrainingOrRefuelingBeginsMessage(int sourceId, int messageTime) {
        return DrainingORRefuelingBegin.newBuilder()
                .setSourceId(sourceId).setMessageType(DrainingORRefuelingBegin.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .build();
    }

    private MessageLite getDrainingOrRefuelingEndsMessage(int sourceId, int messageTime, int volume) {
        return DrainingORRefuelingEnd.newBuilder()
                .setSourceId(sourceId).setMessageType(DrainingORRefuelingEnd.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(volume)
                .build();
    }

    private MessageLite getDrainingBeginMessage(int sourceId, int messageTime){
        return DrainingSTART.newBuilder()
                .setSourceId(sourceId).setMessageType(DrainingSTART.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .build();
    }

    private MessageLite getRefuelingBeginMessage(int sourceId, int messageTime){
        return RefuelingSTART.newBuilder()
                .setSourceId(sourceId).setMessageType(RefuelingSTART.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .build();
    }

    private MessageLite getDrainingEndMessage(int sourceId, int messageTime, int volume) {
        return DrainingEND.newBuilder()
                .setSourceId(sourceId).setMessageType(DrainingEND.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(volume)
                .build();
    }

    private MessageLite getRefuelingEndMessage(int sourceId, int messageTime, int volume) {
        return RefuelingEND.newBuilder()
                .setSourceId(sourceId).setMessageType(RefuelingEND.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(volume)
                .build();
    }

    private void addToBuffer(FuelData data) {
        switch (this.state) {
            case MAYBE_DRAINING_DATA_ACCUMULATION:
            case MAYBE_REFUELING_DATA_ACCUMULATION:
                this.accumulationBuffer.add(data);
                break;
            default:
                this.basicBuffer.add(data);
        }
    }


    private SearchingBufferState analyzeBuffer() {


        int prevPrevValue;
        switch (this.state) {
            case DRAINING_DETECTED:
            case REFUELING_DETECTED:
                this.state = SearchingBufferState.ORDINAL;
                //      break;

            case ORDINAL:
                //Для определения потенциального слива или заправки необходим накопленный буфер размером не менее searchingBufferLength+2
                if (basicBuffer.size() < filterLength + 2) {
                    break;
                }

                int currentValue = basicBuffer.get(basicBuffer.size() - 1).getValue();
                int previousValue = basicBuffer.get(basicBuffer.size() - 2).getValue();

                //Проверяем, не является ли имеющаяся последовательность данных началом потенциального слива
                //Потенциальным сливом считаются монотонно убывающие точки при условии что скорость убывания между ними строго больше нормального расхода топлива.
                //Для этого текущее значение уровня должно быть меньше предудущего и их разница должна превышать нормальный расход топлива

                if ((currentValue < previousValue) && (previousValue - currentValue > this.dutyConsumption)) {
                    //запоминаем точку начала потенциального слива
                    startIndex = basicBuffer.size() - 2;

                    //Переключаем состояние буфера
                    this.state = SearchingBufferState.MAYBE_DRAINING_IN_PROGRESS;
                    break;
                }

                //Проверяем, не является ли имеющаяся последовательность данных началом потенциальной заправки
                //Потенциальной заправкой считается последовательность точек, которые:
                //1. монотонно возрастают, т.е. каждая последующая имеет более высокий уровень топлива, чем предыдущая
                //2. 2 точки, находящиеся рядом, равны друг другу, точка перед ними меньше их, точка после них больше.

                //В начале заправки может выполняться только первое условие
                if (currentValue > previousValue) {
                    //запоминаем точку начала потенциальной заправки
                    startIndex = basicBuffer.size() - 2;

                    //Переключаем состояние буфера
                    this.state = SearchingBufferState.MAYBE_REFUELING_IN_PROGRESS;
                    break;

                }


                break;

            case MAYBE_DRAINING_IN_PROGRESS:
                currentValue = basicBuffer.get(basicBuffer.size() - 1).getValue();
                previousValue = basicBuffer.get(basicBuffer.size() - 2).getValue();
                prevPrevValue = basicBuffer.get(basicBuffer.size() - 3).getValue();

                if (
                        !(

                        (currentValue==previousValue && prevPrevValue-previousValue>this.dutyConsumption) ||
                        (previousValue-currentValue>this.dutyConsumption)

                        )

                        ) {
                //if ((currentValue >= previousValue) || (previousValue - currentValue <= this.dutyConsumption)) {

                    if (prevPrevValue == previousValue) {
                        endIndex = basicBuffer.size() - 3;//offset=3;
                    } else {
                        endIndex = basicBuffer.size() - 2;
                    }

                    //Потенциальный слив завершен
                    //Запоминаем точку окончания потенциального слива
                    endIndex = basicBuffer.size() - 2;
                    this.accumulationBuffer.clear();
                    for (int i = endIndex + 1; i < basicBuffer.size(); i++) {
                        this.accumulationBuffer.add(basicBuffer.get(i));
                    }

                    this.state = SearchingBufferState.MAYBE_DRAINING_DATA_ACCUMULATION;
                }
                break;

            case MAYBE_REFUELING_IN_PROGRESS:
                currentValue = basicBuffer.get(basicBuffer.size() - 1).getValue();
                previousValue = basicBuffer.get(basicBuffer.size() - 2).getValue();
                prevPrevValue = basicBuffer.get(basicBuffer.size() - 3).getValue();

                //Если не выполняются условия потенциальной заправки
                if (!((currentValue == previousValue && previousValue > prevPrevValue) || (currentValue > previousValue))) {

                    if (prevPrevValue == previousValue) {
                        endIndex = basicBuffer.size() - 3;//offset=3;
                    } else {
                        endIndex = basicBuffer.size() - 2;
                    }

                    this.state = SearchingBufferState.MAYBE_REFUELING_DATA_ACCUMULATION;

                    this.accumulationBuffer.clear();
                    for (int i = endIndex + 1; i < basicBuffer.size(); i++) {
                        this.accumulationBuffer.add(basicBuffer.get(i));
                    }

                }


                break;

            case MAYBE_DRAINING_DATA_ACCUMULATION:

                if(basicBuffer.get(startIndex).getValue() - basicBuffer.get(endIndex).getValue() < drainingTreshold){
                    this.state = SearchingBufferState.ORDINAL;
                    break;

                }

                if (this.accumulationBuffer.size() >= filterLength) {
                    //check if it is real draining

                    //Берем участок буфера до начала пот. слива
                    FuelData[] beforeDrainingData = this.basicBuffer.subList(startIndex - filterLength, startIndex).toArray(new FuelData[this.basicBuffer.subList(startIndex - filterLength, startIndex).size()]);
                    FuelData[] afterDrainingData = this.accumulationBuffer.toArray(new FuelData[this.accumulationBuffer.size()]);//this.basicBuffer.subList(endIndex + 1, endIndex + filterLength).toArray(new FuelData[this.basicBuffer.subList(endIndex + 1, endIndex + filterLength).size()]);

                    if (this.quartile(beforeDrainingData) - this.quartile(afterDrainingData) > this.drainingTreshold) {
                        this.state = SearchingBufferState.DRAINING_DETECTED;
                    } else {
                        this.state = SearchingBufferState.ORDINAL;
                    }

                }

                break;

            case MAYBE_REFUELING_DATA_ACCUMULATION:

                if(basicBuffer.get(endIndex).getValue() - basicBuffer.get(startIndex).getValue() < refuelingTreshold){
                    this.state = SearchingBufferState.ORDINAL;
                    break;

                }

                if (this.accumulationBuffer.size() >= filterLength) {
                    //check if it is real refueling

                    //Берем участок буфера до начала пот. слива
                    FuelData[] beforeRefuelingData = this.basicBuffer.subList(startIndex - filterLength, startIndex).toArray(new FuelData[this.basicBuffer.subList(startIndex - filterLength, startIndex).size()]);
                    FuelData[] afterRefuelingData = this.accumulationBuffer.toArray(new FuelData[this.accumulationBuffer.size()]);//this.basicBuffer.subList(endIndex + 1, endIndex + filterLength).toArray(new FuelData[this.basicBuffer.subList(endIndex + 1, endIndex + filterLength).size()]);

                    if (this.quartile(afterRefuelingData) - this.quartile(beforeRefuelingData) > this.refuelingTreshold) {
                        this.state = SearchingBufferState.REFUELING_DETECTED;

                    } else {
                        this.state = SearchingBufferState.ORDINAL;
                    }
                }

                break;
        }

        return this.state;
    }


    private int getEndDataId() {
        return this.basicBuffer.get(endIndex).getDataId();
    }

    private int getEndValue() {
        return this.basicBuffer.get(endIndex).getValue();
    }

    private int getStartDataId() {
        return this.basicBuffer.get(startIndex).getDataId();
    }

    private int getStartValue() {
        return this.basicBuffer.get(startIndex).getValue();
    }

    private int quartile(FuelData[] list) {

        Arrays.sort(list, new Comparator<FuelData>() {

            @Override
            public int compare(FuelData o1, FuelData o2) {
                return ((Integer) o1.getDataId()).compareTo(o2.getDataId());
            }
        });
        
        int quarter = list.length >>> 2;
        int sum = 0;
        for (int index = quarter; index < list.length - quarter; index++) {
            sum += list[index].getValue();
        }
        return sum / (list.length - (quarter << 1));
    }

    private void flushOutputBuffer(Router router){
        while(this.outputBuffer.size()>(filterLength+3)){
            OutputData data = this.outputBuffer.remove(0);
            router.send(getUnsmoothedMessage(sourceId, data.getDataId(), data.getValue(), data.getSmoothable()));
        }
    }

    private static FuelData makeFuelData(int dataId, int value){
        return FuelData.newBuilder().setDataId(dataId).setValue(value).build();
    }

    private static OutputData makeOutputData(int dataId, int value, boolean smoothable){
        return OutputData.newBuilder().setDataId(dataId).setValue(value).setSmoothable(smoothable).build();
    }

    private static Date getDate(int timeStamp) {

        return new Date(1293840000000L + timeStamp * 1000L);
    }

    private void sendToServices(Router router,  DataStorageService dataStorageService, MessageLite message){
        router.send(message);
  
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

//    private class OutputData{
//
//        private final int dataId;
//        private final int value;
//        private boolean smoothable;
//
//        public OutputData(int dataId, int value) {
//            this.dataId = dataId;
//            this.value = value;
//            this.smoothable = true;
//        }
//
//        public int getDataId() {
//            return dataId;
//        }
//
//        public boolean isSmoothable() {
//            return smoothable;
//        }
//
//        public int getValue() {
//            return value;
//        }
//
//        public void setSmoothable(boolean smoothable) {
//            this.smoothable = smoothable;
//        }
//
//        @Override
//        public String toString() {
//            return "OutputData{" + "dataId=" + dataId + " value=" + value + " smoothable=" + smoothable + '}';
//        }
//
//
//
//
//
//
//
//    }

}