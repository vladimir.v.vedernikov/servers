/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.data.fuel.SumUnsmoothedDataParser;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер поиска сливов и заправок.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DrainingsRefuelingsServerImplementation")
public class DrainingsRefuelingsServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private DataStorageService dataStorageService;

    private SettingsStorageService settingsStorageService;

    private int currentSourceId = -1;
    private SearchingBuffer currentSearchingBuffer;

    private Server self;
    private Handler handler;

    /**
     * Конструктор.
     *
     * @param router                 сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     * @param settingsStorageService сервис хранения настроек.
     */
    @ServerConstructor
    public DrainingsRefuelingsServer(Router router, DataStorageService dataStorageService, SettingsStorageService settingsStorageService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.settingsStorageService = settingsStorageService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {

        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(DrainingsRefuelingsServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }

        }

       


    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException, SettingsStorageServiceException {
        SumUnsmoothedDataParser.SumUnsmoothedData dataMessage = SumUnsmoothedDataParser.SumUnsmoothedData.parseFrom(message.toByteArray());
        int sourceId = dataMessage.getSourceId();
        if (sourceId != this.currentSourceId) {
            if (this.currentSearchingBuffer != null) {
                try {
                    this.settingsStorageService.bindTemporary(this.currentSearchingBuffer.getMessageLite(), this.getSearchingBufferName(this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }
            
            this.currentSearchingBuffer = this.getSearchingBuffer(sourceId);
            if (currentSearchingBuffer == null) {
                final String msg = new StringBuilder().append("The Searching Buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the DrainingsRefuelingsServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }
            
        }

        if (currentSearchingBuffer == null) {
            return;
        }

        int messageTime = dataMessage.getMessageTime();
        int value = dataMessage.getValue();

        this.currentSearchingBuffer.processData(router, dataStorageService,messageTime, value);
        //this.settingsStorageService.bindTemporary(this.currentSearchingBuffer.getMessageLite(), this.getSearchingBufferName(sourceId));

    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) { // Инициализация.
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            handler = initSignal.getHandler();

            router.registerServer(DrainingsRefuelingsServer.class, self, SumUnsmoothedDataParser.SumUnsmoothedData.newBuilder().getDefaultInstanceForType().getMessageType());
        } else if (signal instanceof Terminate) { // Завершение работы.
            router.unregisterServer(DrainingsRefuelingsServer.class, self);
        } else if (signal instanceof CaughtException) { // Ошибка.
            Throwable exception = ((CaughtException) signal).getException();
            LOGGER.log(Level.ERROR, exception.getMessage(), exception);
        }

    }


    private String getSearchingBufferName(int sourceId) {

        return String.format("src%d:drainingRefuelingSearching:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:drainingRefuelingSearching:settings", sourceId);

    }

    private SearchingBuffer getSearchingBuffer(int sourceId) {

        DrainingRefuelingSearchingParser.DrainingRefuelingSearchingSettings settings = null;
        DrainingRefuelingSearchingParser.SearchingBuffer buffer = null;

        try {

            MessageLite settingsObject = this.settingsStorageService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                return null;
            }

            settings = DrainingRefuelingSearchingParser.DrainingRefuelingSearchingSettings.parseFrom(settingsObject.toByteArray());
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        

        try {
            MessageLite settingsObject = this.settingsStorageService.lookupInMemory(this.getSearchingBufferName(sourceId));
            if(settingsObject!=null){
                buffer = DrainingRefuelingSearchingParser.SearchingBuffer.parseFrom(settingsObject.toByteArray());
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new SearchingBuffer(settings);
        } else {
            return new SearchingBuffer(settings, buffer);
        }


    }

}