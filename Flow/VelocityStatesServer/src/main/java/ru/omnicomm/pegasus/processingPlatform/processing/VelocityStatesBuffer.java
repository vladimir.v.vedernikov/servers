/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.movement.VelocityStatesParser.VelocityDOWN;
import ru.omnicomm.pegasus.messaging.data.movement.VelocityStatesParser.VelocityNORMAL;
import ru.omnicomm.pegasus.messaging.data.movement.VelocityStatesParser.VelocityUP;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityStateDefinitionSettingsParser.VelocityStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityStateDefinitionSettingsParser.VelocityStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;


/**
 * Буфер алгоритма определения состояний по скорости
 * @author alexander
 */
public class VelocityStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int valueMIN;
    private final int valueMAX;
    private final int idleMAX;
    private final int timeInterval;

    private final List<VelocityStateDefinitionBuffer.Data> buffer;

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по скорости для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public VelocityStatesBuffer(VelocityStateDefinitionSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.valueMIN = settings.getValueMIN();
        this.valueMAX = settings.getValueMAX();
        this.idleMAX = settings.getIdleMAX();
        this.timeInterval = settings.getTimeInterval();
        this.buffer = new ArrayList<VelocityStateDefinitionBuffer.Data>();

     
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по скорости для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public VelocityStatesBuffer(VelocityStateDefinitionSettings settings,
            VelocityStateDefinitionBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.getValueMIN() == buffer.getValueMIN() &&
           settings.getValueMAX() == buffer.getValueMAX() &&
           settings.getIdleMAX() == buffer.getIdleMAX() &&
           settings.getTimeInterval() == buffer.getTimeInterval()){
           
            this.buffer.addAll(buffer.getBufferList());

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        return VelocityStateDefinitionBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(VelocityStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setValueMIN(this.valueMIN)
                .setValueMAX(this.valueMAX)
                .setIdleMAX(this.idleMAX)
                .setTimeInterval(this.timeInterval)
                .addAllBuffer(this.buffer)
                .build();

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и 
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param t время
     * @param v значение
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, int t, int v) {

        int bufferSize = this.buffer.size();
        if(bufferSize>0){

            boolean overMin = startThreshold(this.valueMIN);
            boolean notOverMin  = endThreshold(t,v,this.valueMIN);
            boolean overMax = startThreshold(this.valueMAX);
            boolean notOverMax  = endThreshold(t,v,this.valueMAX);

            int t_res, v_res;
            if(overMax){

                if(bufferSize>1 && this.buffer.get(bufferSize-1).getDataId()-this.buffer.get(bufferSize-2).getDataId()<this.idleMAX){
                    t_res = this.buffer.get(bufferSize-2).getDataId();
                } else {
                    t_res = this.buffer.get(bufferSize-1).getDataId() - this.timeInterval;
                }

                v_res = v;

                sendToServices(router, jmsBridgeService, dataStorageService, getVelocityUPMessage(this.sourceId, t_res, v_res));
                
            }

            if(notOverMax){
                v_res = v;
                t_res = this.buffer.get(bufferSize-1).getDataId();

                if(notOverMin){
                    sendToServices(router, jmsBridgeService, dataStorageService, getVelocityDOWNMessage(this.sourceId, t_res, v_res));
                } else {
                    sendToServices(router, jmsBridgeService, dataStorageService, getVelocityNORMALMessage(this.sourceId, t_res, v_res));
                }
            }

            //10)	Если OverMAX OR NotOverMax = True, Сервер переходит на шаг 13)
            if(!(overMax || notOverMax)){

                if(overMin){
                    if(bufferSize>1 && this.buffer.get(bufferSize-1).getDataId()-this.buffer.get(bufferSize-2).getDataId()<this.idleMAX){
                        t_res = this.buffer.get(bufferSize-2).getDataId();
                    } else {
                        t_res = this.buffer.get(bufferSize-1).getDataId() - this.timeInterval;
                    }

                    v_res = v;
                    sendToServices(router, jmsBridgeService, dataStorageService, getVelocityNORMALMessage(this.sourceId, t_res, v_res));
                }

                if(notOverMin){
                    v_res = v;
                    t_res = this.buffer.get(bufferSize-1).getDataId();
                    sendToServices(router, jmsBridgeService, dataStorageService, getVelocityDOWNMessage(this.sourceId, t_res, v_res));

                }

            }
            
        }

        //"шаг 13"
        this.buffer.add(VelocityStateDefinitionBuffer.Data.newBuilder().setDataId(t).setValue(v).build());
        //в случае непоследовательных данных - сортируем буфер
        if(this.buffer.size()>1 && (this.buffer.get(this.buffer.size()-1).getDataId() <  this.buffer.get(this.buffer.size()-2).getDataId())){
            Collections.sort(buffer, new Comparator<VelocityStateDefinitionBuffer.Data>() {
                @Override
                public int compare(VelocityStateDefinitionBuffer.Data obj1, VelocityStateDefinitionBuffer.Data obj2) {
                    return ((Integer) obj1.getDataId()).compareTo(obj2.getDataId());
                }
            });
        }

        cleanupBuffer();
        


    }

    private boolean startThreshold(int threshold){

        int bufferSize = this.buffer.size();
        int t_max = this.buffer.get(bufferSize-1).getDataId();
        int v_1 =   this.buffer.get(bufferSize-1).getValue();

        if(bufferSize == 1) {
            return v_1 > threshold;
        }

        int t_min = this.buffer.get(bufferSize-2).getDataId();
        int v_0 =   this.buffer.get(bufferSize-2).getValue();

        if(t_max - t_min >= this.idleMAX){
            return v_1 > threshold;
        } else {
            return v_1 > threshold && v_0 <= threshold;
        }


    }


    private boolean endThreshold(int t, int v, int threshold){

        int bufferSize = this.buffer.size();
        int t_max = this.buffer.get(bufferSize-1).getDataId();
        int v_1 =   this.buffer.get(bufferSize-1).getValue();

        if(v_1 <= threshold){
            return false;
        }

        if(t - t_max >= this.idleMAX ){
            return true;
        }

        return v <= threshold;


    }


    private void cleanupBuffer(){
        while(this.buffer.size()>2){
            this.buffer.remove(0);
        }
    }


    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getVelocityUPMessage(int sourceId, int messageTime, int value) {
        return VelocityUP.newBuilder()
                .setSourceId(sourceId).setMessageType(VelocityUP.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(value)
                .build();
    }

    private MessageLite getVelocityDOWNMessage(int sourceId, int messageTime, int value) {
        return VelocityDOWN.newBuilder()
                .setSourceId(sourceId).setMessageType(VelocityDOWN.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(value)
                .build();
    }

    private MessageLite getVelocityNORMALMessage(int sourceId, int messageTime, int value) {
        return VelocityNORMAL.newBuilder()
                .setSourceId(sourceId).setMessageType(VelocityNORMAL.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .setValue(value)
                .build();
    }


}
