/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityStateDefinitionSettingsParser.VelocityStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityStateDefinitionSettingsParser.VelocityStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Сервер определения состояний по скорости;
 * реализация согласно требованиям PP-PSS-06-(Velocity States)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("VelocityStatesServerImplementation")
public class VelocityStatesServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private int currentSourceId;
    private VelocityStatesBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public VelocityStatesServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;
        try {
            header = MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(VelocityStatesServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            router.registerServer(VelocityStatesServer.class, self, ValueMultiplySpeed.getDefaultInstance().getMessageType());
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(VelocityStatesServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException, IllegalArgumentException, SettingsStorageServiceException {

        ValueMultiplySpeed dataMessage = ValueMultiplySpeed.parseFrom(message.toByteArray());
        int sourceId = dataMessage.getSourceId();
        if (sourceId != this.currentSourceId) {
            if (this.currentBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getBufferName(this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentBuffer = this.getBuffer(sourceId);
            if (currentBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the VelocityStatesServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentBuffer == null) {
            return;
        }

        int messageTime = dataMessage.getMessageTime();
        if(dataMessage.getDoubleValueList().size()!=1){
            throw new IllegalArgumentException("Wrong message: "+dataMessage);
        }

        int value = dataMessage.getDoubleValueList().get(0).intValue();

        this.currentBuffer.processData(router,jmsBridgeService,dataStorageService,messageTime,value);
       // this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getBufferName(sourceId));
    }

    private String getBufferName(int sourceId) {

        return String.format("src%d:velocityStates:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:velocityStates:settings", sourceId);

    }

    private VelocityStatesBuffer getBuffer(int sourceId) {

        VelocityStateDefinitionSettings settings;
        VelocityStateDefinitionBuffer buffer = null;

        try {

            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                return null;
            }
            settings = VelocityStateDefinitionSettings.parseFrom(settingsObject.toByteArray());

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getBufferName(sourceId));
            if(settingsObject!=null){
                buffer = VelocityStateDefinitionBuffer.parseFrom(settingsObject.toByteArray());
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new VelocityStatesBuffer(settings);
        } else {
            return new VelocityStatesBuffer(settings, buffer);
        }
    }
    






}
