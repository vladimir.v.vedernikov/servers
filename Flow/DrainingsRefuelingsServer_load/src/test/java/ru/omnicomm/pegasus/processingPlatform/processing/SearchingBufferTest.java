/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 08.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import junit.framework.TestCase;


import java.util.logging.Logger;

/**
 * @author alexander
 */
public class SearchingBufferTest extends TestCase {

    private static final Logger LOGGER = Logger.getLogger(SearchingBufferTest.class.getName());

    /**
     * Проверка реализации алгоритма поиска сливов и заправок
     */
    public void testDrainingsAndRefuelings() {

//        int drainingTreshold = 10;
//        int refuelingTreshold = 10;
//        int dutyConsumption = 5;
//        int searchingBufferLength = 7;
//
//        DrainingRefuelingSearchingSettings settings = MessageParser.DrainingRefuelingSearchingSettings.newBuilder().setSourceId(33554444).setMessageType(MessageType.DRAINING_REFUELING_SEARCHING_SETTINGS.getCode()).setMessageTime(0).setFilterLength(searchingBufferLength).setDrainingTreshold(drainingTreshold).setRefuelingTreshold(refuelingTreshold).setDutyConsumption(dutyConsumption).build();
//        SearchingBuffer buffer = new SearchingBuffer(settings);
//
//        int[] rawData = {
//
//                40, 45, 45, 45, 45, 45, 40,   // [0 - 6]
//
//                //Заправка 1
//                40, 45, 50, 50, 55, 65, 80, 80, 85, // [7 -15]
//
//                //Нормальный расход
//                85, 85, 80, //[16 - 18]
//
//                //Слив 1
//                75, 65, 50, 30, //[19 - 22]
//
//                //Нормальный расход
//                25, 20, 20, 15, 15, 10, 10, 10, //[23 - 30]
////
////            //Заправка 2
////            10,15,20,35,55,55,65, //[31 - 37]
////
////            //Слив 2
////            50,40,25, // 38 - 40
////
////            //Полка
////            25,25,25,25,25,25,25
////
////            //Слив 2
////            55,40,25,15,5,
////
////            //Заправка 3
////            10,20,30,45,60,80,
////
////            //Норм. расход
////            80,80,80,80,
////
////            //Слив 4
////            80,65,
////
////            //Норм. расход
////            65,65,65,65,
////
////            //Заправка 4
////            65,80,
////
////            //Норм.расход
////            80,80,80,80,80,80,80,80,80,80,80,80,80,80
//
//        };
//
//        // DrainingData messages
//        final List<MessageLite> drainings = new ArrayList<MessageLite>();
//        // RefuelingData messages
//        final List<MessageLite> refuelings = new ArrayList<MessageLite>();
//        // UnsmoothedData messages
//        final List<MessageLite> unsmoothedData = new ArrayList<MessageLite>();
//
//
//        // SearchingBufferState state = null;
//
//
//        Router router = new Router() {
//
//            @Override
//            public void registerServer(Class serverType, Server server, int[] types) {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void unregisterServer(Class serverType, Server server) {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void send(MessageLite message) {
//                MessageType messageType = null;
//                try {
//                    MessageParser.MessageHeader header = MessageParser.MessageHeader.parseFrom(message.toByteArray());
//                    messageType = MessageType.lookup(header.getMessageType());
//                } catch (InvalidProtocolBufferException e) {
//                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
//                }
//
//                if (messageType == null) {
//                    throw new IllegalArgumentException("Invalid message.");
//                }
//
//                switch (messageType) {
//                    case REFUELING_DATA:
//                        refuelings.add(message);
//                        break;
//                    case DRAINING_DATA:
//                        drainings.add(message);
//                        break;
//                    case UNSMOOTHED_DATA:
//                        unsmoothedData.add(message);
//                        break;
//                    default:
//                        throw new IllegalArgumentException("Wrong message type");
//                }
//            }
//
//            @Override
//            public void init() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void pause() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void resume() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void stop() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void dataSourceChanged(Class serverType, Server server, int prevSourceId, int currentSourceId) {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//        };
//
//        for (int dataId = 0; dataId < rawData.length; dataId++) {
//            int value = rawData[dataId];
//            buffer.processData(router, dataId, value);
//
//        }
//
////        for(RefuelingDataMessage refueling : refuelings){
////            System.out.println("REFUELING: "+refueling);
////        }
////
////        for(DrainingDataMessage draining : drainings){
////            System.out.println("DRAINING: "+draining);
////        }
//
////        System.out.println("OUTPUT: ");
////        for(MessageLite data : unsmoothedData){
////            System.out.println(data);
////        }
//
//        assertTrue(refuelings.size() == 1);
//        for (int i = 0; i < refuelings.size(); i++) {
//            MessageParser.RefuelingData refueling = getRefuelingData(refuelings.get(i));
//
//            switch (i) {
//                case 0:
//                    assertTrue(refueling.getStartDataId() == 7);
//                    assertTrue(refueling.getEndDataId() == 15);
//                    assertTrue(refueling.getVolume() == 45);
//                    break;
//            }
//        }
//
//        assertTrue(drainings.size() == 1);
//        for (int i = 0; i < drainings.size(); i++) {
//            MessageParser.DrainingData draining = getDrainingData(drainings.get(i));
//
//            switch (i) {
//                case 0:
//                    assertTrue(draining.getStartDataId() == 19);
//                    assertTrue(draining.getEndDataId() == 22);
//                    assertTrue(draining.getVolume() == 45);
//                    break;
//            }
//        }
    }

    public void testOutputMessagesOrder(){


//        int drainingTreshold = 10;
//        int refuelingTreshold = 10;
//        int dutyConsumption = 5;
//        int searchingBufferLength = 7;
//
//        DrainingRefuelingSearchingSettings settings =
//                MessageParser.DrainingRefuelingSearchingSettings.newBuilder().
//                setSourceId(33554444).
//                setMessageType(MessageType.DRAINING_REFUELING_SEARCHING_SETTINGS.getCode()).
//                setMessageTime(0).
//                setFilterLength(searchingBufferLength).
//                setDrainingTreshold(drainingTreshold).
//                setRefuelingTreshold(refuelingTreshold).
//                setDutyConsumption(dutyConsumption).build();
//
//        SearchingBuffer buffer = new SearchingBuffer(settings);
//
//        int[] rawData = {
//
//            115,115,110,115,115,115,115,115,115,
//
//            110, // нормальный расход
//
//            105, 95, 85, 75, 65, // слив , точки: [10,14], объем 40
//
//            60, 55, // Нормальный расход
//
//            55, 45, 35, 25, // слив, точки: [17, 20], объем 30
//
//            20, 15, // Нормальный расход
//
//            15, 25, 35, 40, // Заправка, [23,26], объем 25
//
//            40, 40, 40, 40, 40, 40, 40, 40,      // буфер
//
//            45,20,100, 76, 43,76, 34, 429, 457, 43, 56, 205, 9865, 3, 3468, 34, 3,245, 190, 34, 476, 346, 34, 235, 345 // шум
//
//
//
//        };
//
//        // DrainingData messages
//        final List<MessageLite> drainings = new ArrayList<MessageLite>();
//        // RefuelingData messages
//        final List<MessageLite> refuelings = new ArrayList<MessageLite>();
//        // UnsmoothedData messages
//        final List<MessageLite> unsmoothedData = new ArrayList<MessageLite>();
//
//        Router router = new Router() {
//
//            @Override
//            public void registerServer(Class serverType, Server server, int[] types) {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void unregisterServer(Class serverType, Server server) {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void send(MessageLite message) {
//                MessageType messageType = null;
//                try {
//                    MessageParser.MessageHeader header = MessageParser.MessageHeader.parseFrom(message.toByteArray());
//                    messageType = MessageType.lookup(header.getMessageType());
//                } catch (InvalidProtocolBufferException e) {
//                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
//                }
//
//                if (messageType == null) {
//                    throw new IllegalArgumentException("Invalid message.");
//                }
//
//                switch (messageType) {
//                    case REFUELING_DATA:
//                        refuelings.add(message);
//                        break;
//                    case DRAINING_DATA:
//                        drainings.add(message);
//                        break;
//                    case UNSMOOTHED_DATA:
//                        unsmoothedData.add(message);
//                        break;
//                    default:
//                        throw new IllegalArgumentException("Wrong message type");
//                }
//            }
//
//            @Override
//            public void init() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void pause() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void resume() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void stop() throws ServiceException {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//
//            @Override
//            public void dataSourceChanged(Class serverType, Server server, int prevSourceId, int currentSourceId) {
//                throw new UnsupportedOperationException("Not supported yet.");
//            }
//        };
//
//
//        for (int dataId = 0; dataId < rawData.length; dataId++) {
//            int value = rawData[dataId];
//            buffer.processData(router, dataId, value);
//
//        }
//
////
////        for(MessageLite refueling : refuelings){
////            System.out.println("REFUELING: "+refueling);
////        }
////
////        for(MessageLite draining : drainings){
////            System.out.println("DRAINING: "+draining);
////        }
//
////        System.out.println("OUTPUT: ");
//        int prevDataId = -1;
//        for(MessageLite data : unsmoothedData){
//            MessageParser.UnsmoothedData dataItem = this.getUnsmoothedData(data);
////            System.out.println(dataItem);
////            if(dataItem.getMessageTime() <= prevDataId){
////                fail();
////            }
//            if(dataItem.getMessageTime() != prevDataId+1){
//                fail();
//            }
//            prevDataId = dataItem.getMessageTime();
//        }
//
//
//


    }

//    private MessageParser.DrainingData getDrainingData(MessageLite messageLite) {
//        try {
//            MessageParser.DrainingData drainingData = MessageParser.DrainingData.parseFrom(messageLite.toByteArray());
//            if (MessageType.DRAINING_DATA.getCode() != drainingData.getMessageType()) {
//                throw new IllegalArgumentException("Wrong message type.");
//            }
//            return drainingData;
//        } catch (InvalidProtocolBufferException e) {
//            throw new IllegalArgumentException("Ivalid message.");
//        }
//    }
//
//    private MessageParser.RefuelingData getRefuelingData(MessageLite messageLite) {
//        try {
//            MessageParser.RefuelingData refuelingData = MessageParser.RefuelingData.parseFrom(messageLite.toByteArray());
//            if (MessageType.REFUELING_DATA.getCode() != refuelingData.getMessageType()) {
//                throw new IllegalArgumentException("Wrong message type.");
//            }
//            return refuelingData;
//        } catch (InvalidProtocolBufferException e) {
//            throw new IllegalArgumentException("Ivalid message.");
//        }
//    }
//
//
//    private MessageParser.UnsmoothedData getUnsmoothedData(MessageLite messageLite) {
//        try {
//            MessageParser.UnsmoothedData unsmoothed = MessageParser.UnsmoothedData.parseFrom(messageLite.toByteArray());
//            if (MessageType.UNSMOOTHED_DATA.getCode() != unsmoothed.getMessageType()) {
//                throw new IllegalArgumentException("Wrong message type.");
//            }
//            return unsmoothed;
//        } catch (InvalidProtocolBufferException e) {
//            throw new IllegalArgumentException("Ivalid message.");
//        }
//    }


}