/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Состояние буфера данных поиска сливов и заправок.
 *
 * @author alexander
 */
public enum SearchingBufferState {

    /**
     * Состояние ожидания потенциальных сливов или заправок.
     */
    ORDINAL(0),

    /**
     * Накопление монотонно убывающей последовательности данных.
     */
    MAYBE_DRAINING_IN_PROGRESS(1),

    /**
     * Накопление монотонно возростающей последовательности данных.
     */
    MAYBE_REFUELING_IN_PROGRESS(2),

    /**
     * Накопление данных, необходимых для однозначного определения наличия.
     * факта слива
     */
    MAYBE_DRAINING_DATA_ACCUMULATION(3),

    /**
     * Накопление данных, необходимых для однозначного определения наличия
     * факта заправки.
     */
    MAYBE_REFUELING_DATA_ACCUMULATION(4),

    /**
     * Факт слива зафиксирован.
     */
    DRAINING_DETECTED(5),

    /**
     * Факт заправки зафиксирован.
     */
    REFUELING_DETECTED(6);


    private final int code;
    private static final Map<Integer, SearchingBufferState> map = new HashMap<Integer, SearchingBufferState>();

    static {
        for (SearchingBufferState type : EnumSet.allOf(SearchingBufferState.class)) {
            map.put(type.getCode(), type);
        }
    }

    private SearchingBufferState(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static SearchingBufferState lookup(int code) {
        return map.get(code);
    }
}
