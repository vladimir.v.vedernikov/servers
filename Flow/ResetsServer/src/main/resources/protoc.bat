@echo off

set "DEST_PATH=../java"

rem ##############################################
rem # Create common parser (for parsing headers) #
rem ##############################################
set "FILE_NAME=MessageHeader.proto"
set "SRC_PATH=..\..\..\..\..\..\MessageStorage\src\main\resources\protobuf\common"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

rem ########################################
rem # Create parsers for required messages #
rem ########################################
set "FILE_NAME=ValueMultiplySpeed.proto"
set "SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/movement"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

set "FILE_NAME=AnalogValueCorrected.proto"
set "SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/analogSensor"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

rem #########################################
rem # Create parsers for required settings  #
rem #########################################
set "FILE_NAME=VelocityResets.proto"
set "SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/movement"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

set "FILE_NAME=AnalogResets.proto"
set "SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/analogSensor"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

echo "done"
