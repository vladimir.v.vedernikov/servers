#!/bin/sh

DEST_PATH=../java

##############################################
# Create common parser (for parsing headers) #
##############################################
FILE_NAME=MessageHeader.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/common
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


########################################
# Create parsers for required messages #
########################################
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/movement
FILE_NAME=ValueMultiplySpeed.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/analogSensor
FILE_NAME=AnalogValueCorrected.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}



#########################################
# Create parsers for required settings  #
#########################################
FILE_NAME=VelocityResets.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/movement
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

FILE_NAME=AnalogResets.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/analogSensor
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


echo "done"
