/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser.AnalogValueCorrected;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogResetsSettingsParser;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

/**
 * Буфер алгоритма обнуления аналогового значения
 * @author alexander
 */
public class AnalogValueResetsBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final long sourceId;
    private final int idleMAX;

    private final List<AnalogResetsSettingsParser.AnalogResetsBuffer.Data> buffer;

    private final Comparator<AnalogResetsSettingsParser.AnalogResetsBuffer.Data> comparator;

    /**
     * Конструктор
     * @param settings настройки алгоритма обнуления аналогового значения для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public AnalogValueResetsBuffer(AnalogResetsSettingsParser.AnalogResetsSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.idleMAX = settings.getIdleMAX();
        this.buffer = new ArrayList<AnalogResetsSettingsParser.AnalogResetsBuffer.Data>();

        this.comparator=  new Comparator<AnalogResetsSettingsParser.AnalogResetsBuffer.Data>() {
            @Override
            public int compare(AnalogResetsSettingsParser.AnalogResetsBuffer.Data obj1, AnalogResetsSettingsParser.AnalogResetsBuffer.Data obj2) {
                return ((Long) obj1.getDataId()).compareTo(obj2.getDataId());
            }
        };


    }

    /**
     * Конструктор
     * @param settings настройки алгоритма обнуления аналогового значения для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public AnalogValueResetsBuffer(AnalogResetsSettingsParser.AnalogResetsSettings settings,
            AnalogResetsSettingsParser.AnalogResetsBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.getIdleMAX() == buffer.getIdleMAX()){

            this.buffer.addAll(buffer.getBufferList());

        }

    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        return AnalogResetsSettingsParser.AnalogResetsBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(AnalogResetsSettingsParser.AnalogResetsBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setTimeStamp(0)
                .setIdleMAX(this.idleMAX)
                .addAllBuffer(this.buffer)
                .build();

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     */
    public void processData(JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, AnalogValueCorrected dataMessage) throws IllegalArgumentException{
        int v = dataMessage.getIntValueList().get(0).intValue();
        int t = dataMessage.getMessageTime();
        int messageBlockId = dataMessage.getMessageBlock();

        if(v<0){
            throw new IllegalArgumentException("Wrong input value: "+v);
        }

        if(this.buffer.size() > 0){
            if(isON()){

                long t_x = this.buffer.get(this.buffer.size()-1).getDataId() - 999;
                if(Collections.binarySearch(buffer, AnalogResetsSettingsParser.AnalogResetsBuffer.Data.newBuilder().setDataId(t_x).setValue(0).build(), comparator)<0){
                    sendToServices(jmsBridgeService,dataStorageService,getValueMultiplySpeedMessage(t_x, messageBlockId));
                }

            }

            if(isOFF(t,v)){

                int v_x = this.buffer.get(this.buffer.size()-1).getValue();
                if(v_x>0 && this.buffer.size()>1){
                    long t_x = this.buffer.get(this.buffer.size()-1).getDataId() + 999;
                    if(t_x != t) {
                        sendToServices(jmsBridgeService,dataStorageService,getValueMultiplySpeedMessage(t_x, messageBlockId));
                    }
                }

            }
        }

        this.buffer.add(AnalogResetsSettingsParser.AnalogResetsBuffer.Data.newBuilder().setDataId(t).setValue(v).build());
        if(this.buffer.size()>1 && (this.buffer.get(this.buffer.size()-1).getDataId() <  this.buffer.get(this.buffer.size()-2).getDataId())){
            Collections.sort(this.buffer, this.comparator);
        }
        cleanupBuffer();

    }

    private boolean isON(){

        long t_max = this.buffer.get(this.buffer.size()-1).getDataId();
        int v_1 = this.buffer.get(this.buffer.size()-1).getValue();

        if(this.buffer.size() == 1){
            return v_1 > 0;
        } else {
            long t_min = this.buffer.get(0).getDataId();
            int v_0 = this.buffer.get(0).getValue();
            if(t_max - t_min >= this.idleMAX*1000){
                return v_1 > 0;
            } else {
                return v_1 > 0 && v_0 == 0;
            }
        }


    }

    private boolean isOFF(int t, int v){
        long t_max = this.buffer.get(this.buffer.size()-1).getDataId();
        int v_1 = this.buffer.get(this.buffer.size()-1).getValue();

        if(v_1 > 0){
            return t - t_max >= this.idleMAX;
        } else {
            if (this.buffer.size() == 1) {
                return true;
            } else {
                long t_min = this.buffer.get(0).getDataId();
                int v_0 = this.buffer.get(0).getValue();
                return t_max - t_min < this.idleMAX*1000 && v_0 > 0;
            }
        }


    }

    private void cleanupBuffer(){
        while(this.buffer.size()>2){
            this.buffer.remove(0);
        }
    }


    private void sendToServices(JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){       
        String str = message.toString().replaceAll(" ", "");
        LOGGER.log(Level.INFO, new StringBuilder().append("ResetsServer output: ").append(str.replaceAll("\n", " ")).toString());      
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getValueMultiplySpeedMessage(long timeStamp, int blockId) {
        return AnalogValueCorrected.newBuilder()
                .setSourceId(this.sourceId).setMessageType(AnalogValueCorrected.getDefaultInstance().getMessageType()).setTimeStamp(timeStamp)
                .addIntValue(0)
                .setMessageBlock(blockId)
                .build();
    }



}
