/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.common.ResetsServerMessageType;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser.AnalogValueCorrected;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.settings.analogSensor.AnalogResetsSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityResetsSettingsParser;


import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.messages.Pause;
import ru.omnicomm.pegasus.processingPlatform.messages.Paused;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер обнуления;
 * реализация согласно требованиям PP-PSS-10-(Velocity Resets)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ResetsServerImplementation")
public class ResetsServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private VelocityResetsBuffer currentVelocityResetsBuffer;
    private AnalogValueResetsBuffer currentAnalogValueResetsBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public ResetsServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;
        try {
            header = MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
            return;
        }
        ResetsServerMessageType messageType = ResetsServerMessageType.lookup((int)header.getMessageType());
        try{

            switch(messageType) {

                case VALUE_MULTIPLY_SPEED:
                    processMessage(ValueMultiplySpeed.parseFrom(message.toByteArray()));
                    break;
                case ANALOG_VALUE_CORRECTED:
                    processMessage(AnalogValueCorrected.parseFrom(message.toByteArray()));
                    break;

            }

            
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(ResetsServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            final int[] types = {ResetsServerMessageType.VALUE_MULTIPLY_SPEED.getCode(), ResetsServerMessageType.ANALOG_VALUE_CORRECTED.getCode()};
            router.registerServer(ResetsServer.class, self, types);
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(ResetsServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(ValueMultiplySpeed dataMessage) throws InvalidProtocolBufferException, IllegalArgumentException {

        long sourceId = dataMessage.getSourceId();
        if (sourceId != this.currentSourceId) {
            if (this.currentVelocityResetsBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentVelocityResetsBuffer.getMessageLite(), this.getVelocityResetsBufferName((int)this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentVelocityResetsBuffer = this.getVelocityResetsBuffer((int)sourceId);
            if (currentVelocityResetsBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. Data, received from this source, will be ignored by the ResetsServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentVelocityResetsBuffer == null) {
            return;
        }

        if(dataMessage.getDoubleValueList().size()!=1){
            throw new IllegalArgumentException("Wrong message: "+dataMessage);
        }

        this.currentVelocityResetsBuffer.processData(jmsBridgeService,dataStorageService, dataMessage);
    }

    private void processMessage(AnalogValueCorrected dataMessage) throws InvalidProtocolBufferException, IllegalArgumentException {
        long sourceId = dataMessage.getSourceId();
        if (sourceId != this.currentSourceId) {
            if (this.currentAnalogValueResetsBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentAnalogValueResetsBuffer.getMessageLite(), this.getAnalogValueResetsBufferName((int)this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentAnalogValueResetsBuffer = this.getAnalogValueResetsBuffer((int)sourceId);
            if (currentAnalogValueResetsBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. Data, received from this source, will be ignored by the ResetsServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentAnalogValueResetsBuffer == null) {
            return;
        }

        if(dataMessage.getIntValueList().size()!=1){
            throw new IllegalArgumentException("Wrong message: "+dataMessage);
        }

        this.currentAnalogValueResetsBuffer.processData(jmsBridgeService,dataStorageService, dataMessage);

    }

    private String getVelocityResetsBufferName(int sourceId) {

        return String.format("src%d:velocityResets:buffer", sourceId);

    }

    private String getVelocityResetsSettingsName(int sourceId) {

        return String.format("src%d:velocityResets:settings", sourceId);

    }

    private VelocityResetsBuffer getVelocityResetsBuffer(int sourceId) {

        VelocityResetsSettingsParser.VelocityResetsSettings settings = null;
        VelocityResetsSettingsParser.VelocityResetsBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getVelocityResetsSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                return null;
            }
            settings = VelocityResetsSettingsParser.VelocityResetsSettings.parseFrom(settingsObject.toByteArray());
           
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getVelocityResetsBufferName(sourceId));
            if(settingsObject!=null){
                buffer = VelocityResetsSettingsParser.VelocityResetsBuffer.parseFrom(settingsObject.toByteArray());
            }
          
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new VelocityResetsBuffer(settings);
        } else {
            return new VelocityResetsBuffer(settings, buffer);
        }
    }


    private String getAnalogValueResetsBufferName(int sourceId) {

        return String.format("src%d:analogValueResets:buffer", sourceId);

    }

    private String getAnalogValueResetsSettingsName(int sourceId) {

        return String.format("src%d:analogValueResets:settings", sourceId);

    }

    private AnalogValueResetsBuffer getAnalogValueResetsBuffer(int sourceId) {

        AnalogResetsSettingsParser.AnalogResetsSettings settings = null;
        AnalogResetsSettingsParser.AnalogResetsBuffer buffer = null;

        try {

            MessageLite settingsObject = this.settingsService.lookup(this.getAnalogValueResetsSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                return null;
            }
            settings = AnalogResetsSettingsParser.AnalogResetsSettings.parseFrom(settingsObject.toByteArray());

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getAnalogValueResetsBufferName(sourceId));
            if(settingsObject!=null){
                buffer = AnalogResetsSettingsParser.AnalogResetsBuffer.parseFrom(settingsObject.toByteArray());
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new AnalogValueResetsBuffer(settings);
        } else {
            return new AnalogValueResetsBuffer(settings, buffer);
        }
    }
    






}
