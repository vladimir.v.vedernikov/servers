/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser.AnalogValueCorrected;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum ResetsServerMessageType {

    VALUE_MULTIPLY_SPEED(ValueMultiplySpeed.getDefaultInstance().getMessageType()),
    ANALOG_VALUE_CORRECTED(AnalogValueCorrected.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, ResetsServerMessageType> map = new HashMap<Integer, ResetsServerMessageType>();

    static {
        for (ResetsServerMessageType type : EnumSet.allOf(ResetsServerMessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private ResetsServerMessageType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static ResetsServerMessageType lookup(int code) {
        return map.get(code);
    }

}
