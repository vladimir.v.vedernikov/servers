/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.data.fuel.UnsmoothedDataParser.UnsmoothedData;
import ru.omnicomm.pegasus.messaging.settings.fuel.ApproximationSettingsParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер сглаживания данных по топливу.
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ApproximationServerImplementation")
public class ApproximationServer implements ServerImplementation {

    static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsStorageService;

    private int currentSourceId;
    private ApproximationBuffer currentApproximationBuffer;

    private Server self;


    /**
     * Конструктор.
     *
     * @param router                 сервис маршрутизатор сообщений.
     * @param dataStorageService сервис хранения данных.
     * @param settingsStorageService сервис хранения настроек.
     */
    @ServerConstructor
    public ApproximationServer(Router router, DataStorageService dataStorageService, SettingsStorageService settingsStorageService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.settingsStorageService = settingsStorageService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(ApproximationServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }

        }
    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException {

        int sourceId = CommonParser.MessageHeader.parseFrom(message.toByteArray()).getSourceId();
        if (sourceId != this.currentSourceId) {

            if (this.currentApproximationBuffer != null) {
                try {
                    this.settingsStorageService.bindTemporary(this.currentApproximationBuffer.getMessageLite(), this.getApproximationBufferName(sourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentApproximationBuffer = this.getApproximationBuffer(sourceId);

        }

        if (this.currentApproximationBuffer != null) {
            this.currentApproximationBuffer.processData(router,dataStorageService, message);
        } else {
            LOGGER.log(Level.INFO, "Can't process data for sourceId="+sourceId);
        }

    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;
            router.registerServer(ApproximationServer.class, self, UnsmoothedData.getDefaultInstance().getMessageType());
        } else if (signal instanceof Terminate) { // Завершение работы.
            router.unregisterServer(ApproximationServer.class, self);
        } else if (signal instanceof CaughtException) { // Ошибка.
            Throwable exception = ((CaughtException) signal).getException();
            LOGGER.log(Level.ERROR, exception.getMessage(), exception);
        }
    }

    private String getApproximationBufferName(int sourceId) {

        return String.format("src%d:approximation:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:approximation:settings", sourceId);

    }

    private ApproximationBuffer getApproximationBuffer(int sourceId) {

        ApproximationSettingsParser.ApproximationSettings settings = null;
        ApproximationSettingsParser.ApproximationBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsStorageService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                return null;
            }
            settings = ApproximationSettingsParser.ApproximationSettings.parseFrom(settingsObject.toByteArray());
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsStorageService.lookupInMemory(this.getApproximationBufferName(sourceId));
            if(settingsObject!=null){
                buffer = ApproximationSettingsParser.ApproximationBuffer.parseFrom(settingsObject.toByteArray());
            }
          
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new ApproximationBuffer(settings);
        } else {
            return new ApproximationBuffer(settings, buffer);
        }
    }

}
