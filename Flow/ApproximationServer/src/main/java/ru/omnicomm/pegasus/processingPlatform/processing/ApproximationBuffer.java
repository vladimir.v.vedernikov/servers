/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import ru.omnicomm.pegasus.messaging.data.fuel.ApproximatedDataParser.ApproximatedData;
import ru.omnicomm.pegasus.messaging.data.fuel.UnsmoothedDataParser.UnsmoothedData;
import ru.omnicomm.pegasus.messaging.settings.fuel.ApproximationSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.fuel.ApproximationSettingsParser.ApproximationBuffer.FuelData;
import ru.omnicomm.pegasus.messaging.settings.fuel.ApproximationSettingsParser.ApproximationSettings;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;

/**
 * Буфер сглаживания данных по топливу.
 *
 * @author alexander
 */
public class ApproximationBuffer implements Serializable {
    
    private static final long serialVersionUID = -2219342226618821644L;

    private final int sourceId;
    private final int filterLength;
    private final LinkedList<FuelData> buffer;

    private static final Logger LOGGER = LoggerFactory.getLogger();


    /**
     * Конструктор
     * @param settings параметры работы алгоритма сглаживания
     */
    public ApproximationBuffer(ApproximationSettings settings) {
        this.sourceId = settings.getSourceId();
        this.filterLength = settings.getFilterLength();
        this.buffer = new LinkedList<FuelData>();
    }

    /**
     * Конструктор
     * @param settings параметры работы алгоритма сглаживания
     * @param buffer накопленные данные
     */
    public ApproximationBuffer(ApproximationSettings settings,
            ApproximationSettingsParser.ApproximationBuffer buffer) {

        this(settings);
        if(buffer.getFilterLength() == settings.getFilterLength()){
            this.buffer.addAll(buffer.getBufferList());

        }
    }

    /**
     * Возвращает представление объекта в формате google protobuffer
     * @return представление объекта в формате google protobuffer
     */
    public MessageLite getMessageLite(){

        return ApproximationSettingsParser.ApproximationBuffer.newBuilder().setSourceId(this.sourceId).
                setMessageType(ApproximationSettingsParser.ApproximationBuffer.getDefaultInstance().getMessageType()).
                setMessageTime(0).
                addAllBuffer(this.buffer).
                setFilterLength(this.filterLength).
                build();

    }


    /**
     * Обрабатывает сырые данные.
     *
     * @param router  роутер (для отправки сообщений со сглаженными данными)
     * @param message сообщение с сырыми данными
     */
    public void processData(Router router, DataStorageService dataStorageService, MessageLite message) {

        UnsmoothedData unsmoothedData = null;
        try {
            unsmoothedData = UnsmoothedData.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        if (unsmoothedData == null) {
            return;
        }

        final int messageTime = unsmoothedData.getMessageTime();
     //   final int sourceId = unsmoothedData.getSourceId();
        final int value = unsmoothedData.getValue();
        if (unsmoothedData.getSmoothable()) {
            this.buffer.add(makeFuelData(messageTime, value));

            while (this.buffer.size() > this.filterLength) {
                this.approximateFirstPoint(router,dataStorageService,sourceId);
            }
        } else {
            //Сглаживаем накопленные данные до начала слива/заправки
            if (this.buffer.size() > 0) {
                this.buffer.add(makeFuelData(messageTime, value));
                while (this.buffer.size() > 1) {
                    this.approximateFirstPoint(router,dataStorageService,sourceId);
                }
                this.buffer.clear();
            }

            MessageLite approximatedData = ApproximatedData.newBuilder()
                    .setSourceId(sourceId)
                    .setMessageType(ApproximatedData.getDefaultInstance().getMessageType())
                    .setMessageTime(messageTime)
                    .setValue(value).build();
            //Отправляем точки, находящиеся внутри слива/заправки
            sendToServices(router,dataStorageService,approximatedData);
        }
    }

    private void approximateFirstPoint(Router router, DataStorageService dataStorageService, int sourceId) {
        //Отправляем первую точку буфера как сглаженную
        FuelData firstPoint = this.buffer.getFirst();

        MessageLite firstMessage = ApproximatedData.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(ApproximatedData.getDefaultInstance().getMessageType())
                .setMessageTime(firstPoint.getDataId())
                .setValue(firstPoint.getValue()).build();
        sendToServices(router,dataStorageService,firstMessage);

        //Считаем медиану относительно первой точки
        long[] W = new long[this.buffer.size() - 1];
        long W0 = this.buffer.getFirst().getValue() * 1000;
        for (int i = 0; i < W.length - 1; i++) {
            W[i] = ((long) this.buffer.get(i + 1).getValue() * 1000 - W0) / (long) (i + 1);
        }

        W0 += this.median(W); ///100;
        int approximatedValue = (int) (W0 / 1000);

        //Сглаженное значение получено
        //Удаляем первую точку буфера
        this.buffer.removeFirst();

        //Изменяем значение новой начальной точки буфера на сглаженное
        FuelData rawDataItem = this.buffer.getFirst();
        FuelData approximatedDataItem = makeFuelData(rawDataItem.getDataId(), approximatedValue);
        this.buffer.set(0, approximatedDataItem);
    }

    private long median(long[] W) {
        Arrays.sort(W);
        int middle = W.length / 2;
        if (W.length % 2 == 1) {
            return W[middle];
        } else {
            return ((W[middle - 1] + W[middle]) / 2L);
        }
    }

    private static FuelData makeFuelData(int dataId, int value){

        return FuelData.newBuilder().setDataId(dataId).setValue(value).build();

    }

    private void sendToServices(Router router,  DataStorageService dataStorageService, MessageLite message){
        router.send(message);

        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

}
