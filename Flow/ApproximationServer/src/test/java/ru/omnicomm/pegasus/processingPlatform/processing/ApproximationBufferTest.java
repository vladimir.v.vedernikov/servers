/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.Iterator;
import junit.framework.TestCase;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;

import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.data.fuel.ApproximatedDataParser.ApproximatedData;
import ru.omnicomm.pegasus.messaging.data.fuel.UnsmoothedDataParser.UnsmoothedData;
import ru.omnicomm.pegasus.messaging.settings.fuel.ApproximationSettingsParser.ApproximationSettings;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;

/**
 * @author alexander
 */
public class ApproximationBufferTest extends TestCase {

    /**
     * Проверка реализации алгоритма сглаживания данных по топливу.
     */
    public void testApproximation() {

        //UnsmoothedData
        final List<MessageLite> inputData = new ArrayList<MessageLite>();

        //ApproximatedData
        final List<MessageLite> outputData = new ArrayList<MessageLite>();


        int sourceId = 33554444;
        int dataId = 0;

        MessageLite test = getUnsmoothedDataMessage(sourceId, dataId, 1400, true);
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1400, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1300, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1800, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1600, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1500, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1600, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1300, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1500, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1500, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1700, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 2100, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1400, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 2100, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1900, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1800, false)); //Первая точка слива
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1400, false)); //Слив в процессе
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1000, false)); //Слив в процессе
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 800, true));  //Последняя точка слива
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 1000, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 600, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 500, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));

        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 700, true));
        inputData.add(getUnsmoothedDataMessage(sourceId, dataId++, 900, true));

        Router router = new Router() {

            @Override
            public void registerServer(Class serverType, Server server, int[] types) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void unregisterServer(Class serverType, Server server) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void send(MessageLite message) {
                try {
                    CommonParser.MessageHeader header = CommonParser.MessageHeader.parseFrom(message.toByteArray());

                } catch (InvalidProtocolBufferException e) {
                    fail(e.getMessage());
                }
            }

            @Override
            public void dataSourceChanged(Class serverType, Server server, int prevSourceId, int currentSourceId) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        DataStorageService dataStorageService = new DataStorageService() {

            @Override
            public void add(MessageLite... messages) throws DataStorageServiceException {
            }

            @Override
            public Iterator<MessageChunk> query(int sourceId, int messageType, int from, int till) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public Iterator<MessageChunk> querySourceId(int sourceId, int from, int till) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public Iterator<MessageChunk> queryMessageType(int messageType, int from, int till) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public int getMaxMessageTime(int sourceId, int messageType) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public int getMinMessageTime(int sourceId, int messageType) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void flush(int sourceId, int messageType) throws DataStorageServiceException {
            }

            @Override
            public void flush() throws DataStorageServiceException {
            }

            @Override
            public int getMaxMessageTimeBefore(int sourceId, int messageType, int messageTime) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public int getMinMessageTimeAfter(int sourceId, int messageType, int messageTime) throws DataStorageServiceException {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        ApproximationSettings settings =
                ApproximationSettings.newBuilder().
                setSourceId(33554444).
                setMessageType(ApproximationSettings.getDefaultInstance().getMessageType()).
                setMessageTime(0).setFilterLength(5).build();

        ApproximationBuffer buffer = new ApproximationBuffer(settings);

        for (MessageLite message : inputData) {
            buffer.processData(router, dataStorageService, message);
        }

//        System.out.println("OUTPUT: ");
//        for(ApproximatedDataMessage message : outputData){
//            System.out.printf("data=%2d\tvalue=%3d\n",message.getMessageTime(),message.getValue());
//        }

        //Первая точка должна совпадать с сырыми данными
        assertTrue(getValue(outputData.get(0)) == getValue(inputData.get(0)));

        //Выборочно проверяем несколько сглаженных точек до слива
        assertTrue(getValue(outputData.get(3)) == 1496);
        assertTrue(getValue(outputData.get(5)) == 1500);
        assertTrue(getValue(outputData.get(10)) == 1650);
        assertTrue(getValue(outputData.get(12)) == 1674);

        //Точки внутри слива (включая границы) не должны сгладиться
        for (int i = 20; i <= 23; i++) {
            assertTrue(getValue(outputData.get(i)) == getValue(inputData.get(i)));
        }

        //Выборочно проверяем несколько сглаженных точек до слива
        assertTrue(getValue(outputData.get(24)) == 775);
        assertTrue(getValue(outputData.get(25)) == 750);
        assertTrue(getValue(outputData.get(26)) == 737);
    }


    private int getValue(MessageLite message) {

        int messageType = -1;
        try {
            CommonParser.MessageHeader header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
            messageType = header.getMessageType();
        } catch (InvalidProtocolBufferException e) {
            fail(e.getMessage());
        }

//        final int APPROXIMATED_DATA = ApproximatedData.getDefaultInstance().getMessageType();
//        final int UNSMOOTHED_DATA = UnsmoothedData.getDefaultInstance().getMessageType();
        switch (messageType) {
            case 55: {
                try {
                    return ApproximatedData.parseFrom(message.toByteArray()).getValue();
                } catch (InvalidProtocolBufferException e) {
                    fail(e.getMessage());
                }
            }
            break;
            case 54: {
                try {
                    return UnsmoothedData.parseFrom(message.toByteArray()).getValue();
                } catch (InvalidProtocolBufferException e) {
                    fail(e.getMessage());
                }
            }
            break;
            default:
                fail("Wrong message");
        }

        fail("Wrong way.");
        return -1;
    }

    private MessageLite getUnsmoothedDataMessage(int sourceId, int dataId, int value, boolean smoothable) {
        return UnsmoothedData.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(UnsmoothedData.getDefaultInstance().getMessageType())
                .setMessageTime(dataId)
                .setValue(value)
                .setSmoothable(smoothable).build();
    }

}
