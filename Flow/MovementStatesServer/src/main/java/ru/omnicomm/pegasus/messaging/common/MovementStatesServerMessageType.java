/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum MovementStatesServerMessageType {

    VALUE_MULTIPLY_SPEED(ValueMultiplySpeed.getDefaultInstance().getMessageType()),
    COORDINATE(Coordinate.getDefaultInstance().getMessageType());

    private final long code;

    private static final Map<Long, MovementStatesServerMessageType> map = new HashMap<Long, MovementStatesServerMessageType>();

    static {
        for (MovementStatesServerMessageType type : EnumSet.allOf(MovementStatesServerMessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private MovementStatesServerMessageType(long cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public long getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static MovementStatesServerMessageType lookup(int code) {
        return map.get(code);
    }

}
