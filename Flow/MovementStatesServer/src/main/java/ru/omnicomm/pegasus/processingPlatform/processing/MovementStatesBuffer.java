/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.common.movementStatesServer.MessageType;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser.ParkingEND;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser.ParkingSTART;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer.GpsData;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer.VelocityData;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;


/**
 * Буфер алгоритма определения состояний по движению
 * @author alexander
 */
public class MovementStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int distanceMIN;
    private final int velocityMIN;
    private final int idleMAX;
    private final int decisionInterval;

    private final List<VelocityData> velocityBuffer;
    private final List<GpsData> gpsBuffer;

    private MovementStateDefinitionBuffer.State currentState;

    private final Comparator<VelocityData> velocityDataByMessageTimeComparator;
    private final Comparator<GpsData> gpsDataByMessageTimeComparator;



    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public MovementStatesBuffer(MovementStateDefinitionSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.distanceMIN = settings.getDistanceMIN();
        this.velocityMIN = settings.getVelocityMIN();
        this.idleMAX = settings.getIdleMAX();
        this.decisionInterval = settings.getDecisionInterval();

        this.velocityBuffer = new ArrayList<VelocityData>();
        this.gpsBuffer = new ArrayList<GpsData>();

        this.currentState = MovementStateDefinitionBuffer.State.NOT_DEFINED;

        this.velocityDataByMessageTimeComparator = new Comparator<VelocityData>() {

            @Override
            public int compare(VelocityData obj1, VelocityData obj2) {
                return ((Integer)obj1.getDataId()).compareTo((Integer)obj2.getDataId());
            }
        };

        this.gpsDataByMessageTimeComparator = new Comparator<GpsData>() {

            @Override
            public int compare(GpsData obj1, GpsData obj2) {
                return ((Integer)obj1.getDataId()).compareTo((Integer)obj2.getDataId());
            }
        };

     
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public MovementStatesBuffer(MovementStateDefinitionSettings settings,
            MovementStateDefinitionBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.hasDistanceMIN() == buffer.hasDistanceMIN() &&
           settings.getVelocityMIN() == buffer.getVelocityMIN() &&
           settings.getIdleMAX() == buffer.getIdleMAX() &&
           settings.getDecisionInterval() == buffer.getDecisionInterval()){

           
            this.velocityBuffer.addAll(buffer.getVelocityBufferList());
            this.gpsBuffer.addAll(buffer.getGpsBufferList());


            this.currentState = buffer.getCurrentState();

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        MovementStateDefinitionBuffer.Builder builder =
                MovementStateDefinitionBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(MovementStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setDistanceMIN(this.distanceMIN)
                .setVelocityMIN(this.velocityMIN)
                .setIdleMAX(this.idleMAX)
                .setDecisionInterval(this.decisionInterval)

                .addAllVelocityBuffer(this.velocityBuffer)
                .addAllGpsBuffer(this.gpsBuffer)

                .setCurrentState(this.currentState);

        
        return builder.build();

    }


    /**
     * Выполняет обработку входных данных согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param messageTypeId тип сообщения
     * @param messageBody тело сообщения
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, long messageTypeId, byte[] messageBody) throws InvalidProtocolBufferException {

        MessageType messageType = MessageType.lookup(messageTypeId);

        if(messageType == null){
            throw new IllegalArgumentException("Unknown message type id: "+messageTypeId);
        }

        //Получаем агрегированные данные по скорости и координатам
        switch(messageType) {

            case VALUE_MULTIPLY_SPEED:
                this.processVelocityData(ValueMultiplySpeed.parseFrom(messageBody),router,jmsBridgeService,dataStorageService);
                break;

            case COORDINATE:
                this.processGpsData(Coordinate.parseFrom(messageBody));
                break;

        }

    }

    //4.1	Обработка сообщения "ValueMultiplySpeed"
    private void processVelocityData(ValueMultiplySpeed valueMultiplySpeed,Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService) {
        //3)	Сервер помещает обрабатываемое сообщение в буфер
        this.velocityBuffer.add(VelocityData.newBuilder().setDataId(valueMultiplySpeed.getMessageTime()).setValue((int)(valueMultiplySpeed.getDoubleValue(0))).build());

        Collections.sort(this.gpsBuffer, this.gpsDataByMessageTimeComparator);
        Collections.sort(this.velocityBuffer, this.velocityDataByMessageTimeComparator);

        //4)	Если разность между максимальным и минимальным timestamp в буфере меньше DecisionInterval, то сервер прерывает данную ветвь.
        while(!this.velocityBuffer.isEmpty() && this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() - this.velocityBuffer.get(0).getDataId() >= this.decisionInterval){
            //5)	Сервер выполняет алгоритм 5.1 Подсчёт средней скорости и запоминает его результат VAvr.
            int v_avr = this.getAverageSpeed();

            //6)	Сервер выполняет алгоритм 5.2 Подсчёт пробега и запоминает его результат M.

            int m = this.getMileage();

            //7)	Устанавливаем Tm = минимальный timestamp в буфере.
            int t_mim;
            if(this.gpsBuffer.isEmpty()){
                t_mim = this.velocityBuffer.get(0).getDataId();
            } else {
                t_mim = this.velocityBuffer.get(0).getDataId() > this.gpsBuffer.get(0).getDataId() ? this.velocityBuffer.get(0).getDataId() : this.gpsBuffer.get(0).getDataId();
            }

            //8)	Если М < DistanceMin И VAvr < VelocityMIN, И текущее состояние для SourceID НЕ равно Стоянка, то:
            if(m < this.distanceMIN && v_avr < this.velocityMIN && this.currentState != MovementStateDefinitionBuffer.State.PARKING){
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingSTART со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = минимальный timestamp среди всех сообщений в буфере, для которых выполняется условие Vm < VelocityMIN, где Vm – скорость из тела сообщения. (Устанавливаем Tm = данный timestamp)
                //iii.	Тело сообщения пустое
                int minTimestamp = Integer.MAX_VALUE;
                for(VelocityData velocityData : this.velocityBuffer){
                    if(velocityData.getValue() < this.velocityMIN && velocityData.getDataId() < minTimestamp){
                        minTimestamp = velocityData.getDataId();
                    }
                }
                t_mim = minTimestamp;
                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingSTARTMessage(sourceId, minTimestamp));
                //b.	Текущее состояние для SourceID устанавливается равным Стоянка
                this.currentState = MovementStateDefinitionBuffer.State.PARKING;
            }

            //9)	Если НЕ (М < DistanceMin И VAvr < VelocityMIN), И текущее состояние для SourceID НЕ равно Движение, то:
            if(!(m < this.distanceMIN && v_avr < this.velocityMIN) && (this.currentState != MovementStateDefinitionBuffer.State.MOVEMENT) ){
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingEND со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = максимальный timestamp сообщения в буфере
                //iii.	Тело сообщения пустое
                int maxTimestamp;
                if(this.gpsBuffer.isEmpty()){
                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId();
                } else {
                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() > this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId() ? this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() : this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId();
                }
                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingENDMessage(sourceId, maxTimestamp));
                //b.	Текущее состояние для SourceID устанавливается равным Движение
                this.currentState = MovementStateDefinitionBuffer.State.MOVEMENT;
            }

            //10)	Сервер удаляет из буфера все сообщения с timestamp, не большим, чем Tm и переходит на шаг 4)
            while(this.gpsBuffer.size()>0){
                if(this.gpsBuffer.get(0).getDataId()<=t_mim){
                    this.gpsBuffer.remove(0);
                } else {
                    break;
                }
            }

            while(this.velocityBuffer.size()>0){
                if(this.velocityBuffer.get(0).getDataId()<=t_mim){
                    this.velocityBuffer.remove(0);
                } else {
                    break;
                }
            }          
        }
    }

    private void processGpsData(Coordinate coordinate) {

        if(coordinate.getValid()){
            this.gpsBuffer.add(GpsData.newBuilder()
                    
                    .setDataId(coordinate.getMessageTime())
                    .setLatitude(coordinate.getLatitude())
                    .setLongitude(coordinate.getLongitude())

                    .build());
        }
        
    }

    private int getAverageSpeed(){

        int S = 0;
        for (int i = 1; i < this.velocityBuffer.size(); i++) {

            int deltaT = this.velocityBuffer.get(i).getDataId() - this.velocityBuffer.get(i-1).getDataId();
            int v_i;
            if(deltaT < this.idleMAX){
                v_i = this.velocityBuffer.get(i).getValue();
            } else {
                v_i = 0;
            }
            S += (v_i * deltaT);
        }
        return S/(this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId()-this.velocityBuffer.get(0).getDataId());

    }

    private int getMileage(){

        
        int m = 0;

        for (int i = 1; i < gpsBuffer.size(); i++) {
            m+=getDistance(gpsBuffer.get(i-1),gpsBuffer.get(i));
        }
        
        return m;       
    }

    private double sin(double deg){
        return Math.sin(Math.toRadians(deg));
    }

    private double cos(double deg){
        return Math.cos(Math.toRadians(deg));
    }


    private int getDistance(GpsData point1, GpsData point2){

        double earth_radius = 6372.0;

        double dist =
                2 * earth_radius * 1000 * Math.asin(
                    Math.sqrt(
                        Math.pow(sin((point1.getLatitude() - point2.getLatitude()) / 2.0), 2) +
                        cos(point1.getLatitude()) * cos(point2.getLatitude()) *
                        Math.pow(sin((point1.getLongitude() - point2.getLongitude()) / 2.0), 2)
                    )
                );

        return (int)dist;


    }



    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getParkingSTARTMessage(int sourceId, int messageTime) {
        return ParkingSTART.newBuilder()
                .setSourceId(sourceId).setMessageType(ParkingSTART.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .build();
    }

    private MessageLite getParkingENDMessage(int sourceId, int messageTime) {
        return ParkingEND.newBuilder()
                .setSourceId(sourceId).setMessageType(ParkingEND.getDefaultInstance().getMessageType()).setMessageTime(messageTime)
                .build();
    }






}
