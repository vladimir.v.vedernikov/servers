/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.common.MovementStatesServerMessageType;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер определения состояний по движению;
 * реализация согласно требованиям PP-PSS-07-(Movement States)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("MovementStatesServerImplementation")
public class MovementStatesServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private MovementStatesBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public MovementStatesServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;

        byte[] messageBody = message.toByteArray();
        try {
            header = MessageHeader.parseFrom(messageBody);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(header.getSourceId(), header.getMessageType(), messageBody);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
           long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(MovementStatesServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            final int[] types = {(int)MovementStatesServerMessageType.VALUE_MULTIPLY_SPEED.getCode(),
                                 (int)MovementStatesServerMessageType.COORDINATE.getCode()};

            router.registerServer(MovementStatesServer.class, self, types);
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(MovementStatesServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(long sourceId, long messageType, byte[] messageBody) throws Exception {


        if (sourceId != this.currentSourceId) {
            if (this.currentBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getBufferName((int)this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentBuffer = this.getBuffer((int)sourceId);
            if (currentBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the MovementStatesServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentBuffer == null) {
            return;
        }

        this.currentBuffer.processData(router,jmsBridgeService,dataStorageService,messageType,messageBody);
     //   this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getBufferName(sourceId));
    }

    private String getBufferName(int sourceId) {

        return String.format("src%d:movementStates:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:movementStates:settings", sourceId);

    }

    private MovementStatesBuffer getBuffer(int sourceId) {

        MovementStateDefinitionSettings settings;
        MovementStateDefinitionBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                return null;
            }
            settings = MovementStateDefinitionSettings.parseFrom(settingsObject.toByteArray());

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getBufferName(sourceId));
            if(settingsObject!=null){
                buffer = MovementStateDefinitionBuffer.parseFrom(settingsObject.toByteArray());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new MovementStatesBuffer(settings);
        } else {
            return new MovementStatesBuffer(settings, buffer);
        }
    }
    






}
