/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementAggregatingSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementAggregatingSettingsParser.MovementAggregatingSettings;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер агрегации движения;
 * реализация согласно требованиям PP-PSS-01-(Movement Aggregating)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("MovementAggregatingServerImplementation")
public class MovementAggregatingServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private static final AggregationType AGGR_TYPE = AggregationType.DEFAULT;

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private int currentSourceId;
    private MovementAggregatingBuffer currentMovementAggregatingBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public MovementAggregatingServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;
        try {
            header = MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(MovementAggregatingServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;

            router.registerServer(MovementAggregatingServer.class, self, ValueMultiplySpeed.getDefaultInstance().getMessageType());
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(MovementAggregatingServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException, IllegalArgumentException {

        ValueMultiplySpeed dataMessage = ValueMultiplySpeed.parseFrom(message.toByteArray());
        int sourceId = dataMessage.getSourceId();
        if (sourceId != this.currentSourceId) {
            if (this.currentMovementAggregatingBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentMovementAggregatingBuffer.getMessageLite(), this.getMovementAggregatingBufferName(this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentMovementAggregatingBuffer = this.getMovementAggregatingBuffer(sourceId);
            if (currentMovementAggregatingBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the MovementAggregatingServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentMovementAggregatingBuffer == null) {
            return;
        }

        int messageTime = dataMessage.getMessageTime();
        if(dataMessage.getDoubleValueList().size()!=1){
            throw new IllegalArgumentException("Wrong message: "+dataMessage);
        }

        int value = dataMessage.getDoubleValueList().get(0).intValue();

        this.currentMovementAggregatingBuffer.processData(router,jmsBridgeService,dataStorageService,messageTime,value);
    }

    private String getMovementAggregatingBufferName(int sourceId) {

        return String.format("src%d:movementAggregating:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:movementAggregating:settings", sourceId);

    }

    private MovementAggregatingBuffer getMovementAggregatingBuffer(int sourceId) {

        MovementAggregatingSettingsParser.MovementAggregatingSettings settings;
        MovementAggregatingSettingsParser.MovementAggregatingBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                MovementAggregatingSettings defaultInstance = MovementAggregatingSettings.getDefaultInstance();
                settings = MovementAggregatingSettings.newBuilder()

                        .setSourceId(sourceId)
                        .setMessageType(defaultInstance.getMessageTime())
                        .setMessageTime(0)

                        .setIdleMax(defaultInstance.getIdleMax())
                        .setAggrInterval(defaultInstance.getAggrInterval())

                        .build();
            } else {
                settings = MovementAggregatingSettingsParser.MovementAggregatingSettings.parseFrom(settingsObject.toByteArray());
            }
            

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getMovementAggregatingBufferName(sourceId));
            if(settingsObject!=null){
                buffer = MovementAggregatingSettingsParser.MovementAggregatingBuffer.parseFrom(settingsObject.toByteArray());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new MovementAggregatingBuffer(AGGR_TYPE, settings);
        } else {
            return new MovementAggregatingBuffer(AGGR_TYPE, settings, buffer);
        }
    }
    






}
