/**
 * Class COMMServerTest
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 *
 * It is a part of Asymbix SDK.
 * Licensed under the Asymbix SDK License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.asymbix.com/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the full volume of permissions and
 * limitations under the License.
 *
 *
 * @author Rustam Yusubahmedov jusubahmedov@asctel.com
 * @version 1.0, 4/29/13 12:50 PM
 */

/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import ru.omnicomm.pegasus.messaging.online.special.SpecialMessagesParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;

import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * @author alexander
 */


public class TestCOMMServer /*extends TestCase*/ {

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;
    private BlockService blockService;

    COMMServer server;

    @Before
    public void onCreate() {
        createServices();
        server = new COMMServer(blockService, router, jmsBridgeService, dataStorageService, settingsService);
    }


    @Test
    public void onMessage() {
        MessageLite statusNotif = getRecalcStatusNotification(1, 234);
        CommonParser.MessageHeader header = getMessageHeader(1, 234);
        COMMServerBuffer.getInstance().add(header, statusNotif);
        server.onMessage(null, statusNotif);
        System.out.println("onMessage OK");
        //  MessageLite test = getOutOfSequenceDataMessage(sourceId, dataId, 502);
    }

    @Test
    public void testBuffer() {

        MessageLite statusNotif = getRecalcStatusNotification(1, 231);
        CommonParser.MessageHeader header = getMessageHeader(1, 231);
        COMMServerBuffer.getInstance().add(header, statusNotif);
        System.out.println("add to buffer OK");

        SortedMap<Long, MessageLite> messages = COMMServerBuffer.getInstance().get(header);
        if (null != messages)
            System.out.println("message for header -OK");
        else
            System.out.println("message for header empty");
        COMMServerBuffer.getInstance().remove(header);
        System.out.println("remove from buffer - OK");

        messages = COMMServerBuffer.getInstance().get(header);
        if (null != messages)
            System.out.println("message for header -OK");
        else
            System.out.println("message for header empty");
    }

    private void createServices() {
        router = new Router() {

            @Override
            public void registerServer(Class serverType, Server server, int[] types) {
                // throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void unregisterServer(Class serverType, Server server) {
                // throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void send(MessageLite message) {
                try {
                    CommonParser.MessageHeader header = CommonParser.MessageHeader.parseFrom(message.toByteArray());

                } catch (InvalidProtocolBufferException e) {
                    // fail(e.getMessage());
                }
            }

            @Override
            public void dataSourceChanged(Class serverType, Server server, int prevSourceId, int currentSourceId) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        dataStorageService = new DataStorageService() {
            @Override
            public void add(MessageLite... messages) throws DataStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public Iterator<MessageChunk> query(int blockId, int sourceId, int messageType, int from, int till) throws DataStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String queryAsync(Server server, int sourceId, int blockId, int messageType, int from, int till) throws DataStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public Iterator<MessageChunk> querySourceId(int sourceId, int from, int till) throws DataStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public Iterator<MessageChunk> queryMessageType(int messageType, int from, int till) throws DataStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public int getMaxMessageTime(int blockId, int sourceId, int messageType) throws DataStorageServiceException {
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String getMaxMessageTimeAsync(Server server, int blockId, int sourceId, int messageType) throws DataStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public int getMinMessageTime(int blockId, int sourceId, int messageType) throws DataStorageServiceException {
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String getMinMessageTimeAsync(Server server, int blockId, int sourceId, int messageType) throws DataStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public int getMaxMessageTimeBefore(int sourceId, int messageType, int messageTime) throws DataStorageServiceException {
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public int getMinMessageTimeAfter(int sourceId, int messageType, int messageTime) throws DataStorageServiceException {
                return 0;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public void flush(int sourceId, int messageType) throws DataStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public void flush() throws DataStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };

        settingsService = new SettingsStorageService() {
            @Override
            @Deprecated
            public void bind(MessageLite settings, String name) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public void bindTemporary(MessageLite settings, String name) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public MessageLite lookup(String name) throws SettingsStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public MessageLite lookupInMemory(String name) throws SettingsStorageServiceException {
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            @Deprecated
            public void unbind(String name) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void add(int blockId, String key, String... values) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String[] get(int blockId, String key) throws SettingsStorageServiceException {
                return new String[0];  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void delete(int blockId, String key) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void delete(int blockId, String key, String... values) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void addBinaryData(String key, byte[] value) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public byte[] getBinaryData(String key) throws SettingsStorageServiceException {
                return new byte[0];  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void deleteBinaryData(String key) throws SettingsStorageServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };

        jmsBridgeService = new JmsBridgeService() {
            @Override
            public void send(MessageLite message) throws JmsBridgeServiceException {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void registerServer(Class serverType, Server server, int... types) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void unregisterServer(Class serverType, Server server) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };

        blockService = new BlockService() {
            @Override
            public void send(MessageLite message) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void registerServer(Class serverType, Server server, int... types) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void unregisterServer(Class serverType, Server server) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };
    }


    private MessageLite getOutOfSequenceDataMessage(int sourceId, int dataId, int messType) {
        SpecialMessagesParser.OutOfSequenceData.Builder bld = SpecialMessagesParser.OutOfSequenceData.newBuilder();
        bld.setSourceId(sourceId);
        bld.setMessageType(messType);
        bld.setTimeStamp((int) (System.currentTimeMillis() / 1000L));
        bld.setMessageId(dataId);

        return bld.build();
    }

    private MessageLite getRecalcStatusNotification(int sourceId, int dataId) {
        SpecialMessagesParser.RecalcStatusNotification.Builder bld = SpecialMessagesParser.RecalcStatusNotification.newBuilder();
        bld.setSourceId(sourceId);
        bld.setMessageType(534);
        bld.setTimeStamp((int) (System.currentTimeMillis() / 1000L));
        bld.setMessageId(dataId);

        return bld.build();
    }

    private CommonParser.MessageHeader getMessageHeader(int sourceId, int dataId) {
        CommonParser.MessageHeader.Builder bld = CommonParser.MessageHeader.newBuilder();
        bld.setSourceId(sourceId);
        bld.setMessageType(534);
        bld.setTimeStamp((int) (System.currentTimeMillis() / 1000L));
        return bld.build();
    }
}
