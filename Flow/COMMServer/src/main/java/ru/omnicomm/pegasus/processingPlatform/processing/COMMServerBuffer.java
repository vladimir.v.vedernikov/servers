package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created with IntelliJ IDEA.
 * User: rustam
 * Date: 4/26/13
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class COMMServerBuffer {
    private static COMMServerBuffer ourInstance = new COMMServerBuffer();

    private ConcurrentMap<String, SortedMap<Long, MessageLite>> buffer;

    public static COMMServerBuffer getInstance() {
        return ourInstance;
    }

    private COMMServerBuffer() {
        buffer = new ConcurrentHashMap<>();
    }

    void add(MessageHeader header, MessageLite message) {
        String key = getKey((int) header.getSourceId(), header.getMessageType());
        SortedMap<Long, MessageLite> list = buffer.get(key);
        if (null == list) {
            list = new TreeMap<>();
            buffer.put(key, list);
        }
        synchronized (list) {
            list.put(header.getTimeStamp(), message);
        }
    }

    SortedMap<Long, MessageLite> get(MessageHeader header) {
        String key = getKey((int) header.getSourceId(), header.getMessageType());
        return buffer.get(key);
    }

    void remove (MessageHeader header){
        String key = getKey((int) header.getSourceId(), header.getMessageType());
        buffer.remove(key);
    }


    private String getKey(int sourceId, long messageId) {
        return String.valueOf(sourceId) + "-" + String.valueOf(messageId);
    }
}
