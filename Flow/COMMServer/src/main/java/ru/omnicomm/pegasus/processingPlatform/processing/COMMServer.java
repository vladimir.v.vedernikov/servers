/**
 * Class TransitionalServer
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 *
 * It is a part of Asymbix SDK.
 * Licensed under the Asymbix SDK License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.asymbix.com/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the full volume of permissions and
 * limitations under the License.
 *
 *
 * @author Rustam Yusubahmedov jusubahmedov@asctel.com
 * @version 1.0, 4/26/13 11:02 AM
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.online.special.SpecialMessagesParser.OutOfSequenceData;
import ru.omnicomm.pegasus.messaging.online.special.SpecialMessagesParser.RecalcStatusNotification;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

import java.util.Map;
import java.util.SortedMap;

//import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;

/**
 * ...TelematicsPlatformDocs/Processing/TMP-PSS-01-(COMM Server).docx
 * Сервер предназначен для получения данных от сервиса
 * межслойной коммуникации и последующего направления их на обработку.
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("COMMServerImplementation")
public class COMMServer implements ServerImplementation {
    private final int blockId = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final Router router;
    private final JmsBridgeService jmsBridgeService;
    private final DataStorageService dataStorageService;
    private final SettingsStorageService settingsService;
    private final BlockService blockService;


    private Server self;

    private final static int OUT_OF_SEQUENCE_DATA = 533;
    private final static int RECALC_STATUS_NOTIFICATION = 534;

    private long currentSourceId;

    @ServerConstructor
    public COMMServer(BlockService block, Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
        this.blockService = block;
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;
            router.registerServer(COMMServer.class, self, RECALC_STATUS_NOTIFICATION);
            blockService.registerServer(COMMServer.class, self, 0);

        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(COMMServer.class, self);
            blockService.unregisterServer(COMMServer.class, self);
        }
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        CommonParser.MessageHeader header = null;
        try {
            header = CommonParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.error(e.getMessage(), e);
            return;
        }
        try {
            if (RECALC_STATUS_NOTIFICATION == header.getMessageType()) {
                processRecalcStatusNotification(header,message);
            } else {

                String set_value = "";
                set_value = getSetting(blockId, keySRC(header));

                if (null == set_value || set_value.isEmpty())
                    return;

                if ("2".equals(set_value)) {
                    isSRCEq2(header);
                } else if ("1".equals(set_value)) {
                    isSRCEq1(header, message);
                }
            }
        } catch (Exception ex) {
            LOGGER.info("Can't process data", ex);
        } finally {
            int sourceId = (int) header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(COMMServer.class, self, (int) currentSourceId, sourceId);
                currentSourceId = sourceId;
            }
        }
    }


    private void processRecalcStatusNotification(MessageHeader header,MessageLite message) throws InvalidProtocolBufferException, IllegalArgumentException,
            DataStorageServiceException, JmsBridgeServiceException, SettingsStorageServiceException {
        RecalcStatusNotification dataMessage = null;
        try {
            dataMessage = RecalcStatusNotification.parseFrom(message.toByteArray());
        }
        catch(Exception ex){
            LOGGER.error("On parse RecalcStatusNotification. ", ex);
            return;
        }
        if(null==dataMessage)
            return;

        if (0 != dataMessage.getRecalcStatus())
            return;

        SortedMap<Long, MessageLite> messages = COMMServerBuffer.getInstance().get(header);
        if (null == messages)
            return;
        for (Map.Entry<Long, MessageLite> item : messages.entrySet()) {
            dataStorageService.add(header, item.getValue());
            router.send(item.getValue());
            jmsBridgeService.send(item.getValue());
        }
    }

    /**
     * Если SRC-<SourceId> == 2
     *
     * @param header
     * @throws DataStorageServiceException
     * @throws SettingsStorageServiceException
     *
     * @throws JmsBridgeServiceException
     */
    private void isSRCEq2(MessageHeader header) throws DataStorageServiceException,
            SettingsStorageServiceException, JmsBridgeServiceException, InvalidProtocolBufferException {
        int timestamp = header.getMessageTime();
        Integer recalcFrom = getRECALCFROM(header);

        dataStorageService.add(header);
        jmsBridgeService.send(header);

        if (null == recalcFrom || recalcFrom > timestamp)
            settingsService.add(blockId, keyRECALCFROM(header), String.valueOf(timestamp));
        sendOutOfSequenceData(header);
    }

    /**
     * Если SRC-<SourceId> == 1
     *
     * @throws DataStorageServiceException
     * @throws SettingsStorageServiceException
     *
     * @throws JmsBridgeServiceException
     */
    private void isSRCEq1(MessageHeader header, MessageLite message) throws DataStorageServiceException,
            SettingsStorageServiceException, JmsBridgeServiceException {
        int timestamp = header.getMessageTime();
        int blockId = header.getMessageBlock();
        Integer maxtime = getMAXTIME(header);
        Integer recalc = getRECALC(header);
        Integer recalcFrom = getRECALCFROM(header);

        if (null == maxtime || maxtime < timestamp) {
            settingsService.add(blockId, keyMAXTIME(header), String.valueOf(timestamp));
            if (null == recalc || recalc == 0) {// невыполнение пересчёта (=0) или настройка отсутствует:
                flushBuffer(header, message);
            } else {  //если пересчет
                COMMServerBuffer.getInstance().add(header, message);
            }

        } else {  //данные идут не последовательно
            if (null == recalc || recalc == 0) {// невыполнение пересчёта (=0) или настройка отсутствует:
                if (null == recalcFrom || recalcFrom > timestamp)
                    settingsService.add(blockId, keyRECALCFROM(header), String.valueOf(timestamp));
                sendOutOfSequenceData(header);
                flushBuffer(header, message);
            } else { //iii. данный момент производится пересчёт
                COMMServerBuffer.getInstance().add(header, message);
                if (timestamp < recalc) {
                    if (null == recalcFrom || recalcFrom > timestamp)
                        settingsService.add(blockId, keyRECALCFROM(header), String.valueOf(timestamp));
                    sendOutOfSequenceData(header);
                }
            }
        }
    }

    /**
     * Отправка данных из буфера в сервисы
     * и очистка буфера
     */
    private void flushBuffer(final MessageHeader header, final MessageLite message) throws DataStorageServiceException, JmsBridgeServiceException {
        SortedMap<Long, MessageLite> msges = COMMServerBuffer.getInstance().get(header);
        if (null == msges) {
            dataStorageService.add(message);
            router.send(message);
            jmsBridgeService.send(message);
        } else {
            synchronized (msges) {
                msges.put(header.getTimeStamp(), message);
                for (SortedMap.Entry<Long, MessageLite> item : msges.entrySet()) {
                    dataStorageService.add(message);
                    router.send(message);
                    jmsBridgeService.send(message);
                }
            }
            COMMServerBuffer.getInstance().remove(header);
        }
    }


    private void sendOutOfSequenceData(final MessageHeader dataMessage) throws DataStorageServiceException, JmsBridgeServiceException {
        OutOfSequenceData.Builder bld = OutOfSequenceData.newBuilder();
        bld.setSourceId(dataMessage.getSourceId());
        bld.setMessageType(OUT_OF_SEQUENCE_DATA);
        bld.setTimeStamp(dataMessage.getMessageTime());
        bld.setMessageId((int) dataMessage.getMessageType());  //FIXME вроде так, но тип параметров не совпадает

        OutOfSequenceData data = bld.build();
        dataStorageService.add(data);
        router.send(data);
        jmsBridgeService.send(data);
    }


    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.warn("Can't send signal", ex);
        }
    }


    private String getSetting(int blockId, String key) throws SettingsStorageServiceException {
        String[] settings = settingsService.get(blockId, key);
        if (null == settings || 0 == settings.length) {
            LOGGER.error("Can't read " + key + " from settings");
            return null;
        }
        LOGGER.debug(key + " settings: " + settings[0]);
        return settings[0].trim();
    }

    private Integer getRECALCFROM(MessageHeader header) throws SettingsStorageServiceException {
        Integer result = null;
        String key = keyRECALCFROM(header);
        String setValue = getSetting(blockId, key);
        if (null == setValue || setValue.isEmpty())
            return null;
        try {
            result = Integer.parseInt(setValue);
        } catch (NumberFormatException ex) {
            LOGGER.error(key + " from settings not integer.");
        }
        return result;
    }

    private Integer getMAXTIME(MessageHeader header) throws SettingsStorageServiceException {
        Integer result = null;
        String key = keyMAXTIME(header);
        String setValue = getSetting(blockId, key);
        if (null == setValue || setValue.isEmpty())
            return null;
        try {
            result = Integer.parseInt(getSetting(blockId, key));
        } catch (NumberFormatException ex) {
            LOGGER.error(key + " from settings not integer.");
        }
        return result;
    }

    private Integer getRECALC(MessageHeader header) throws SettingsStorageServiceException {
        Integer result = null;
        String key = keyRECALC(header);
        String setValue = getSetting(blockId, key);
        if (null == setValue || setValue.isEmpty())
            return null;
        try {
            result = Integer.parseInt(getSetting(blockId, key));
        } catch (NumberFormatException ex) {
            LOGGER.error(key + " from settings not integer.");
        }
        return result;
    }

    private String keySRC(MessageHeader data) {
        return "SRC-" + String.valueOf(data.getSourceId());
    }


    private String keyRECALCFROM(MessageHeader data) {
        return "RECALCFROM-" + String.valueOf(data.getSourceId()) +
                "-" + String.valueOf(data.getMessageType());
    }

    private String keyMAXTIME(MessageHeader data) {
        return "MAXTIME-" + String.valueOf(data.getSourceId()) +
                "-" + String.valueOf(data.getMessageType());
    }

    private String keyRECALC(MessageHeader data) {
        return "RECALC-" + String.valueOf(data.getSourceId()) +
                "-" + String.valueOf(data.getMessageType());
    }
}
