/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.util.*;

import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.messaging.common.ParkingServerMessageType;
import ru.omnicomm.pegasus.messaging.common.movementStatesServer.MessageType;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser.ParkingEND;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser.ParkingSTART;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.online.location.LocationDataParser;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser.AnalogValueCorrected;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser.ParkingStateDefinitionSettings;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser.ParkingStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser.ParkingStateDefinitionBuffer.AnalogValueCorrectedData;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser.ParkingStateDefinitionBuffer.GpsCorrectData;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;


/**
 * Буфер алгоритма определения состояний по движению
 * @author alexander
 */
public class ParkingBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private static final long STARTING_POINT = 1293840000000L;
    //примем интервал получения данных из файлов равным двум суткам
    private static final int DATA_INTERVAL = (2*24*60*60);

    private final long sourceId;
    private final int distanceMIN;
    private final int velocityMIN;
    private final int idleMAX;
    private final int decisionInterval;

    private final List<AnalogValueCorrectedData> analogValueBuffer;
    private final List<GpsCorrectData> gpsBuffer;

    private final List<AnalogValueCorrectedData> analogValueDelayBuffer;
    private final List<GpsCorrectData> gpsDelayBuffer;

    private ParkingStateDefinitionBuffer.State currentState;

    private final Comparator<AnalogValueCorrectedData> analogValueByTimeAddComparator;
    private final Comparator<AnalogValueCorrectedData> analogValueByTimestampComparator;
    private final Comparator<AnalogValueCorrected> avByTimestampComparator;
    private final Comparator<GpsCorrectData> gpsDataByTimestampComparator;
    private final Comparator<GpsCorrectData> gpsDataByTimeAddComparator;


    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public ParkingBuffer(ParkingStateDefinitionSettings settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.distanceMIN = settings.getDistanceMIN();
        this.velocityMIN = settings.getVelocityMIN();
        this.idleMAX = settings.getIdleMAX();
        this.decisionInterval = settings.getDecisionInterval();

        this.analogValueBuffer = new ArrayList<AnalogValueCorrectedData>();
        this.gpsBuffer = new ArrayList<GpsCorrectData>();

        this.analogValueDelayBuffer = new ArrayList<AnalogValueCorrectedData>();
        this.gpsDelayBuffer = new ArrayList<GpsCorrectData>();

        this.currentState = ParkingStateDefinitionBuffer.State.NOT_DEFINED;

        this.analogValueByTimeAddComparator = new Comparator<AnalogValueCorrectedData>() {

            @Override
            public int compare(AnalogValueCorrectedData obj1, AnalogValueCorrectedData obj2) {
                return ((Long)obj1.getTimeAdd()).compareTo((Long)obj2.getTimeAdd());
            }
        };

        this.analogValueByTimestampComparator = new Comparator<AnalogValueCorrectedData>() {

            @Override
            public int compare(AnalogValueCorrectedData obj1, AnalogValueCorrectedData obj2) {
                return ((Long)obj1.getTimeStamp()).compareTo((Long)obj2.getTimeStamp());
            }
        };

        this.avByTimestampComparator = new Comparator<AnalogValueCorrected>() {

            @Override
            public int compare(AnalogValueCorrected obj1, AnalogValueCorrected obj2) {
                return ((Long)obj1.getTimeStamp()).compareTo((Long)obj2.getTimeStamp());
            }
        };

        this.gpsDataByTimestampComparator = new Comparator<GpsCorrectData>() {

            @Override
            public int compare(GpsCorrectData obj1, GpsCorrectData obj2) {
                return ((Long)obj1.getTimeStamp()).compareTo((Long)obj2.getTimeStamp());
            }
        };

        this.gpsDataByTimeAddComparator = new Comparator<GpsCorrectData>() {

            @Override
            public int compare(GpsCorrectData obj1, GpsCorrectData obj2) {
                return ((Long)obj1.getTimeAdd()).compareTo((Long)obj2.getTimeAdd());
            }
        };
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public ParkingBuffer(ParkingStateDefinitionSettings settings,
                         ParkingStateDefinitionBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.hasDistanceMIN() == buffer.hasDistanceMIN() &&
           settings.getVelocityMIN() == buffer.getVelocityMIN() &&
           settings.getIdleMAX() == buffer.getIdleMAX() &&
           settings.getDecisionInterval() == buffer.getDecisionInterval()){

           
            this.analogValueBuffer.addAll(buffer.getAnalogValueBufferList());
            this.gpsBuffer.addAll(buffer.getGpsBufferList());


            this.currentState = buffer.getCurrentState();

        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        ParkingStateDefinitionBuffer.Builder builder =
                ParkingStateDefinitionBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(ParkingStateDefinitionBuffer.getDefaultInstance().getMessageType())
                .setDistanceMIN(this.distanceMIN)
                .setVelocityMIN(this.velocityMIN)
                .setIdleMAX(this.idleMAX)
                .setDecisionInterval(this.decisionInterval)

                .addAllAnalogValueBuffer(this.analogValueBuffer)
                .addAllGpsBuffer(this.gpsBuffer)

                .setCurrentState(this.currentState);

        
        return builder.build();

    }


    /**
     * Выполняет обработку входных данных согласно алгоритму и
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param messageTypeId тип сообщения
     * @param messageBody тело сообщения
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,
                            long messageTypeId, long processId, long timeInterval, byte[] messageBody) throws InvalidProtocolBufferException {

        MessageType messageType = MessageType.lookup(messageTypeId);

        if(messageType == null){
            throw new IllegalArgumentException("Unknown message type id: "+messageTypeId);
        }

        //4)	Сервер ищет в массиве служебных данных (см. п. 3.33.2 Начальное состояние) буфер сообщений для данного ProcessID
        // и текущее состояние для данного ProcessID. Если эти величины не найдены, они инициализируются, для текущего ProcessId и
        // временного штампа T, как указано в п. Инициализация буфера сообщений и Инициализация состояния
        initBuffer(dataStorageService);

        //5)	Сервер ищет в массиве служебных данных (см. п. 3.33.2 Начальное состояние) буфер задержки, для данного ProcessId.
        // Если эта величина не найдена, она инициализируется, для текущего ProcessId и временного штампа T, как указано в п. инициализация
        // буфера задержки.
        initDelayBuffer();

        //Получаем агрегированные данные по скорости и координатам
        switch(messageType) {

            case ANALOG_VALUE_CORRECTED:
                this.processAnalogData(AnalogValueCorrectedParser.AnalogValueCorrected.parseFrom(messageBody),
                        processId, timeInterval, router, jmsBridgeService, dataStorageService);
                break;

            case GPS_CORRECT:
                this.processGpsData(LocationDataParser.GPSCorrect.parseFrom(messageBody), timeInterval);
                break;

        }

    }

    private void initBuffer(DataStorageService dataStorageService) {
        if(this.analogValueBuffer!=null)
            return;

        long T = Long.MAX_VALUE;
        long T1 = Long.MAX_VALUE;
        long Tmax = Long.MAX_VALUE;
        int till = (int)(getMessageTime(System.currentTimeMillis())/1000L) ;
        int from = till - DATA_INTERVAL;
        int psMessageTypeId = (int) MovementStatesParser.ParkingSTART.getDefaultInstance().getMessageType();
        int peMessageTypeId = (int) MovementStatesParser.ParkingSTART.getDefaultInstance().getMessageType();
        ArrayList<AnalogValueCorrected> analogValueTimes = getAnalogValueMessagesFromDataStorage(dataStorageService, till, from);
        if(analogValueTimes.isEmpty())
            return;
        else
            T1 = analogValueTimes.get(analogValueTimes.size()-1).getTimeStamp();

        ArrayList<Long> parkingStartTimes = getPSTimeStampsFromDataStorage(dataStorageService, till, from, psMessageTypeId);
        Collections.sort(parkingStartTimes);
        if(!parkingStartTimes.isEmpty())
            Tmax = parkingStartTimes.get(analogValueTimes.size()-1);

        ArrayList<Long> parkingEndTimes = getPETimeStampsFromDataStorage(dataStorageService, till, from, peMessageTypeId);
        Collections.sort(parkingEndTimes);
        if(!parkingStartTimes.isEmpty()&&parkingStartTimes.get(analogValueTimes.size()-1)>Tmax)
            Tmax = parkingStartTimes.get(analogValueTimes.size()-1);

        //3.	Выбираем из хранилища все сообщения типа AnalogValueCorrected с timestamp, не меньшим, чем T1
        // И меньшим, чем T И большим, чем TMax. Все найденные сообщения помещаем в буфер
        while(!analogValueTimes.isEmpty()
                && analogValueTimes.get(analogValueTimes.size()-1).getTimeStamp()>=T1 && analogValueTimes.get(analogValueTimes.size()-1).getTimeStamp()>Tmax
                && analogValueTimes.get(analogValueTimes.size()-1).getTimeStamp()<T) {
            this.analogValueBuffer.add(AnalogValueCorrectedData.newBuilder().setTimeStamp(analogValueTimes.get(analogValueTimes.size()-1).getTimeStamp()).build());
            analogValueTimes.remove(analogValueTimes.size()-1);
        }
    }


    private void initDelayBuffer(){

    }

    /**
     * 1.	Ищем в хранилище для SourceID равного ProcessId, указанному во входных данных, сообщение типа AGRValueCorrected с максимальным timestamp
     * за интервал from-till
     * @param dataStorageService
     * @param till
     * @param from
     * @return
     */
    private ArrayList<AnalogValueCorrected> getAnalogValueMessagesFromDataStorage(DataStorageService dataStorageService, int till, int from) {
        ArrayList<AnalogValueCorrected> timesList = new ArrayList<AnalogValueCorrected>();
        int avMessageTypeId = (int) AnalogValueCorrected.getDefaultInstance().getMessageType();
        try {
            Iterator<MessageChunk> iterator = dataStorageService.query((int) this.sourceId, avMessageTypeId, from, till);
            while(iterator.hasNext()) {
                MessageChunk messageChunk = iterator.next();
                MessageLite message = messageChunk.messages[0];
                byte[] messageBody = message.toByteArray();
                try {
                    timesList.add( AnalogValueCorrected.parseFrom(messageBody) );
                } catch (InvalidProtocolBufferException e) {
                    LOGGER.log(Level.ERROR, e.getMessage(), e);
                }
            }
        } catch (DataStorageServiceException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
        Collections.sort(timesList, this.avByTimestampComparator);
        return timesList;
    }

    /**
     * 2.	Находим максимальный timestamp TMax cреди всех сообщений ParkingEND для данного SourceId.
     * @param dataStorageService
     * @param till
     * @param from
     * @param avMessageTypeId
     * @return
     */
    private ArrayList<Long> getPETimeStampsFromDataStorage(DataStorageService dataStorageService, int till, int from, int avMessageTypeId) {
        ArrayList<Long> timesList = new ArrayList<Long>();
        try {
            Iterator<MessageChunk> iterator = dataStorageService.query((int) this.sourceId, avMessageTypeId, from, till);
            while(iterator.hasNext()) {
                MessageChunk messageChunk = iterator.next();
                MessageLite message = messageChunk.messages[0];
                byte[] messageBody = message.toByteArray();
                try {
                    timesList.add( ParkingEND.parseFrom(messageBody).getTimeStamp() );
                } catch (InvalidProtocolBufferException e) {
                    LOGGER.log(Level.ERROR, e.getMessage(), e);
                }
            }
        } catch (DataStorageServiceException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
        return timesList;
    }

    /**
     * 2.	Находим максимальный timestamp TMax cреди всех сообщений ParkingSTART для данного SourceId.
     * @param dataStorageService
     * @param till
     * @param from
     * @param avMessageTypeId
     * @return
     */
    private ArrayList<Long> getPSTimeStampsFromDataStorage(DataStorageService dataStorageService, int till, int from, int avMessageTypeId) {
        ArrayList<Long> timesList = new ArrayList<Long>();
        try {
            Iterator<MessageChunk> iterator = dataStorageService.query((int) this.sourceId, avMessageTypeId, from, till);
            while(iterator.hasNext()) {
                MessageChunk messageChunk = iterator.next();
                MessageLite message = messageChunk.messages[0];
                byte[] messageBody = message.toByteArray();
                try {
                    timesList.add( ParkingSTART.parseFrom(messageBody).getTimeStamp() );
                } catch (InvalidProtocolBufferException e) {
                    LOGGER.log(Level.ERROR, e.getMessage(), e);
                }
            }
        } catch (DataStorageServiceException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }
        return timesList;
    }

    //4.1	Обработка сообщения "AnalogValueCorrected"
    private void processAnalogData(AnalogValueCorrectedParser.AnalogValueCorrected analogValueCorrected,
                    long processId, long timeInterval, Router router, JmsBridgeService jmsBridgeService,
                    DataStorageService dataStorageService) {

        //6)	Помещаем сообщение в буфер задержки. При помещении сообщения в буфер задержки ему присваивается дополнительный параметр Tadd – время добавления в буфер.
        long tadd = getMessageTime(System.currentTimeMillis());
        this.analogValueDelayBuffer.add(AnalogValueCorrectedData.newBuilder().setTimeStamp(analogValueCorrected.getTimeStamp()).setTimeAdd(tadd).build());

        Collections.sort(this.analogValueDelayBuffer, this.analogValueByTimeAddComparator);
        Collections.sort(this.gpsBuffer, this.gpsDataByTimestampComparator);
        //7)	Сервер отправляет в буфер сообщений все сообщения из буфера задержки, для которых Tadd’ < Tadd – Tint, где Tadd – время добавления обрабатываемого сообщения
        //8)	Сервер удаляет из буфера задержки сообщения, отправленные в буфер сообщений на предыдущем шаге.
        while(!this.analogValueDelayBuffer.isEmpty()
                && this.analogValueDelayBuffer.get(0).getTimeAdd()<(tadd-timeInterval) )
        {
                this.analogValueBuffer.add(this.analogValueDelayBuffer.get(0));
                analogValueDelayBuffer.remove(0);
        }

        //9)	Если разность между максимальным и минимальным timestamp в буфере сообщений меньше DecisionInterval, то сервер прерывает данную ветвь.
        Collections.sort(this.analogValueDelayBuffer, this.analogValueByTimestampComparator);
        while(!this.analogValueBuffer.isEmpty() && this.analogValueBuffer.get(this.analogValueBuffer.size()-1).getTimeStamp() - this.analogValueBuffer.get(0).getTimeStamp() >= this.decisionInterval){
            //10)	Сервер выполняет алгоритм 5.1 Подсчёт средней скорости и запоминает его результат VAvr.
            int v_avr = this.getAverageSpeed();

            //11)	Сервер выполняет алгоритм 5.2 Подсчёт пробега и запоминает его результат M.
            int m = this.getMileage();

            //12)	Устанавливаем Tm = минимальный timestamp в буфере.
            long t_mim;
            if(this.gpsBuffer.isEmpty()){
                t_mim = this.analogValueBuffer.get(0).getTimeStamp();
            } else {
                t_mim = this.analogValueBuffer.get(0).getTimeStamp() > this.gpsBuffer.get(0).getTimeStamp() ? this.analogValueBuffer.get(0).getTimeStamp() : this.gpsBuffer.get(0).getTimeStamp();
            }

            //13)	Если М < DistanceMin И VAvr < VelocityMIN, И текущее состояние для SourceID НЕ равно Стоянка, то:
            if(m < this.distanceMIN && v_avr < this.velocityMIN && this.currentState != ParkingStateDefinitionBuffer.State.PARKING){
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingSTART со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = минимальный timestamp среди всех сообщений в буфере, для которых выполняется условие Vm < VelocityMIN, где Vm – скорость из тела сообщения. (Устанавливаем Tm = данный timestamp)
                //iii.	Тело сообщения пустое
                int minTimestamp = Integer.MAX_VALUE;
                long minTimestampL = Long.MAX_VALUE;
                for(AnalogValueCorrectedData analogData : this.analogValueBuffer){
                    if(analogData.getValue() < this.velocityMIN && analogData.getTimeStamp() < minTimestampL){
                        minTimestampL = analogData.getTimeStamp();
                    }
                }
                t_mim = minTimestampL;
                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingSTARTMessage(sourceId, minTimestampL));
                //b.	Текущее состояние для SourceID устанавливается равным Стоянка
                this.currentState = ParkingStateDefinitionBuffer.State.PARKING;
            }

            //14)	Если НЕ (М < DistanceMin И VAvr < VelocityMIN), И текущее состояние для SourceID НЕ равно Движение, то:
            if(!(m < this.distanceMIN && v_avr < this.velocityMIN) && (this.currentState != ParkingStateDefinitionBuffer.State.MOVEMENT) ){
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingEND со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = максимальный timestamp сообщения в буфере
                //iii.	Тело сообщения пустое
                long maxTimestamp;
                if(this.gpsBuffer.isEmpty()){
                    maxTimestamp = this.analogValueBuffer.get(this.analogValueBuffer.size()-1).getTimeStamp();
                } else {
                    maxTimestamp = this.analogValueBuffer.get(this.analogValueBuffer.size()-1).getTimeStamp() >
                            this.gpsBuffer.get(this.gpsBuffer.size()-1).getTimeStamp() ?
                            this.analogValueBuffer.get(this.analogValueBuffer.size()-1).getTimeStamp() : this.gpsBuffer.get(this.gpsBuffer.size()-1).getTimeStamp();
                }
                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingENDMessage(sourceId, maxTimestamp));
                //b.	Текущее состояние для SourceID устанавливается равным Движение
                this.currentState = ParkingStateDefinitionBuffer.State.MOVEMENT;
            }

            //10)	Сервер удаляет из буфера все сообщения с timestamp, не большим, чем Tm и переходит на шаг 4)
            while(this.gpsBuffer.size()>0){
                if(this.gpsBuffer.get(0).getTimeStamp()<=t_mim){
                    this.gpsBuffer.remove(0);
                } else {
                    break;
                }
            }

            while(this.analogValueBuffer.size()>0){
                if(this.analogValueBuffer.get(0).getTimeStamp()<=t_mim){
                    this.analogValueBuffer.remove(0);
                } else {
                    break;
                }
            }
        }

    }

//    //4.1	Обработка сообщения "ValueMultiplySpeed"
//    private void processVelocityData(ValueMultiplySpeed valueMultiplySpeed,Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService) {
//        //3)	Сервер помещает обрабатываемое сообщение в буфер
//        this.velocityBuffer.add(VelocityData.newBuilder().setDataId(valueMultiplySpeed.getMessageTime()).setValue((int)(valueMultiplySpeed.getDoubleValue(0))).build());
//
//        Collections.sort(this.gpsBuffer, this.gpsDataByMessageTimeComparator);
//        Collections.sort(this.velocityBuffer, this.analogValueByTimeAddComparator);
//
//        //4)	Если разность между максимальным и минимальным timestamp в буфере меньше DecisionInterval, то сервер прерывает данную ветвь.
//        while(!this.velocityBuffer.isEmpty() && this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() - this.velocityBuffer.get(0).getDataId() >= this.decisionInterval){
//            //5)	Сервер выполняет алгоритм 5.1 Подсчёт средней скорости и запоминает его результат VAvr.
//            int v_avr = this.getAverageSpeed();
//
//            //6)	Сервер выполняет алгоритм 5.2 Подсчёт пробега и запоминает его результат M.
//
//            int m = this.getMileage();
//
//            //7)	Устанавливаем Tm = минимальный timestamp в буфере.
//            int t_mim;
//            if(this.gpsBuffer.isEmpty()){
//                t_mim = this.velocityBuffer.get(0).getDataId();
//            } else {
//                t_mim = this.velocityBuffer.get(0).getDataId() > this.gpsBuffer.get(0).getDataId() ? this.velocityBuffer.get(0).getDataId() : this.gpsBuffer.get(0).getDataId();
//            }
//
//            //8)	Если М < DistanceMin И VAvr < VelocityMIN, И текущее состояние для SourceID НЕ равно Стоянка, то:
//            if(m < this.distanceMIN && v_avr < this.velocityMIN && this.currentState != MovementStateDefinitionBuffer.State.PARKING){
//                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingSTART со следующими параметрами:
//                //i.	Источник сообщения = обрабатываемый SourceID
//                //ii.	Временной штамп = минимальный timestamp среди всех сообщений в буфере, для которых выполняется условие Vm < VelocityMIN, где Vm – скорость из тела сообщения. (Устанавливаем Tm = данный timestamp)
//                //iii.	Тело сообщения пустое
//                int minTimestamp = Integer.MAX_VALUE;
//                for(VelocityData velocityData : this.velocityBuffer){
//                    if(velocityData.getValue() < this.velocityMIN && velocityData.getDataId() < minTimestamp){
//                        minTimestamp = velocityData.getDataId();
//                    }
//                }
//                t_mim = minTimestamp;
//                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingSTARTMessage(sourceId, minTimestamp));
//                //b.	Текущее состояние для SourceID устанавливается равным Стоянка
//                this.currentState = MovementStateDefinitionBuffer.State.PARKING;
//            }
//
//            //9)	Если НЕ (М < DistanceMin И VAvr < VelocityMIN), И текущее состояние для SourceID НЕ равно Движение, то:
//            if(!(m < this.distanceMIN && v_avr < this.velocityMIN) && (this.currentState != MovementStateDefinitionBuffer.State.MOVEMENT) ){
//                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingEND со следующими параметрами:
//                //i.	Источник сообщения = обрабатываемый SourceID
//                //ii.	Временной штамп = максимальный timestamp сообщения в буфере
//                //iii.	Тело сообщения пустое
//                int maxTimestamp;
//                if(this.gpsBuffer.isEmpty()){
//                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId();
//                } else {
//                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() > this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId() ? this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() : this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId();
//                }
//                this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingENDMessage(sourceId, maxTimestamp));
//                //b.	Текущее состояние для SourceID устанавливается равным Движение
//                this.currentState = MovementStateDefinitionBuffer.State.MOVEMENT;
//            }
//
//            //10)	Сервер удаляет из буфера все сообщения с timestamp, не большим, чем Tm и переходит на шаг 4)
//            while(this.gpsBuffer.size()>0){
//                if(this.gpsBuffer.get(0).getDataId()<=t_mim){
//                    this.gpsBuffer.remove(0);
//                } else {
//                    break;
//                }
//            }
//
//            while(this.velocityBuffer.size()>0){
//                if(this.velocityBuffer.get(0).getDataId()<=t_mim){
//                    this.velocityBuffer.remove(0);
//                } else {
//                    break;
//                }
//            }
//        }
//    }

    private void processGpsData(LocationDataParser.GPSCorrect coordinate, long timeInterval) {
        long tadd = getMessageTime(System.currentTimeMillis());
//        if(coordinate.getValid()){
            this.gpsBuffer.add(GpsCorrectData.newBuilder()
                    .setTimeAdd(tadd)
                    .setTimeStamp(coordinate.getTimeStamp())
                    .setLatitude(coordinate.getLatitude())
                    .setLongitude(coordinate.getLongitude())

                    .build());
//        }
        Collections.sort(this.gpsBuffer, this.gpsDataByTimeAddComparator);
        //7)	Сервер отправляет в буфер сообщений все сообщения из буфера задержки, для которых Tadd’ < Tadd – Tint, где Tadd – время добавления обрабатываемого сообщения
        //8)	Сервер удаляет из буфера задержки сообщения, отправленные в буфер сообщений на предыдущем шаге.
        while(!this.gpsDelayBuffer.isEmpty()
                && this.gpsDelayBuffer.get(0).getTimeAdd()<(tadd-timeInterval) )
        {
            this.gpsBuffer.add(this.gpsDelayBuffer.get(0));
            gpsDelayBuffer.remove(0);
        }
    }

    private int getAverageSpeed(){
        int S = 0;
        for (int i = 1; i < this.analogValueBuffer.size(); i++) {

            long deltaT = this.analogValueBuffer.get(i).getTimeStamp() - this.analogValueBuffer.get(i-1).getTimeStamp();
            int v_i;
            if(deltaT/1000L < this.idleMAX){
                v_i = this.analogValueBuffer.get(i).getValue();
            } else {
                v_i = 0;
            }
            S += (v_i * deltaT);
        }
        return (int)(S/(this.analogValueBuffer.get(this.analogValueBuffer.size()-1).getTimeStamp()-this.analogValueBuffer.get(0).getTimeStamp())/1000);
    }

    private int getMileage(){

        
        int m = 0;

        for (int i = 1; i < gpsBuffer.size(); i++) {
            m+=getDistance(gpsBuffer.get(i-1),gpsBuffer.get(i));
        }
        
        return m;       
    }

    private double sin(double deg){
        return Math.sin(Math.toRadians(deg));
    }

    private double cos(double deg){
        return Math.cos(Math.toRadians(deg));
    }


    private int getDistance(GpsCorrectData point1, GpsCorrectData point2){

        double earth_radius = 6372.0;

        double dist =
                2 * earth_radius * 1000 * Math.asin(
                    Math.sqrt(
                        Math.pow(sin((point1.getLatitude() - point2.getLatitude()) / 2.0), 2) +
                        cos(point1.getLatitude()) * cos(point2.getLatitude()) *
                        Math.pow(sin((point1.getLongitude() - point2.getLongitude()) / 2.0), 2)
                    )
                );

        return (int)dist;


    }



    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getParkingSTARTMessage(long sourceId, long timeStamp) {
        return ParkingSTART.newBuilder()
                .setSourceId(sourceId).setMessageType(ParkingSTART.getDefaultInstance().getMessageType()).setTimeStamp(timeStamp)
                .build();
    }

    private MessageLite getParkingENDMessage(long sourceId, long timeStamp) {
        return ParkingEND.newBuilder()
                .setSourceId(sourceId).setMessageType(ParkingEND.getDefaultInstance().getMessageType()).setTimeStamp(timeStamp)
                .build();
    }

    private static long getMessageTime(long unixTimeStamp){
        return (unixTimeStamp-STARTING_POINT);
    }

}
