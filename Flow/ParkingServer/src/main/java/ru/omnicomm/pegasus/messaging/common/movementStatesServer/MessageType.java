/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.movementStatesServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.online.location.LocationDataParser;


/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum MessageType {

    GPS_CORRECT(LocationDataParser.GPSCorrect.getDefaultInstance().getMessageType()),
    ANALOG_VALUE_CORRECTED(AnalogValueCorrectedParser.AnalogValueCorrected.getDefaultInstance().getMessageType());

    private final long code;

    private static final Map<Long, MessageType> map = new HashMap<Long, MessageType>();

    static {
        for (MessageType type : EnumSet.allOf(MessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private MessageType(long cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public long getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static MessageType lookup(long code) {
        return map.get(code);
    }

}
