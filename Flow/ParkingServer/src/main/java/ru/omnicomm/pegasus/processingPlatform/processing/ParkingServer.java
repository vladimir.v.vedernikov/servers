/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.common.ParkingServerMessageType;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser.ParkingStateDefinitionSettings;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser.ParkingStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.ParkingStateDefinitionSettingsParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

import static ru.omnicomm.pegasus.messaging.common.ParkingServerMessageType.ANALOG_VALUE_CORRECTED;

/**
 * Сервер определения парковкм;
 * реализация согласно требованиям TTP-PSS-01-(Parking Server)
 * @author khotyan
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ParkingServerImplementation")
public class ParkingServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private long currentSourceId;
    private long currentProcessId;
    private ParkingBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public ParkingServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;
        long processId = 0L;
        long timeInterval = 0L;
        String[] settingsProcessId = null;
        String[] settingsTimeInterval = null;

        byte[] messageBody = message.toByteArray();
        try {
            header = MessageHeader.parseFrom(messageBody);


        switch(ParkingServerMessageType.lookup(header.getMessageType())){
            case ANALOG_VALUE_CORRECTED:
                processId = header.getSourceId();
                break;
            case GPS_CORRECT:
                settingsProcessId = this.settingsService.get(new StringBuffer("CO-COORDINATE-DIR").append(header.getSourceId()).toString());
                if(settingsProcessId.length>0){
                    processId = Long.getLong(settingsProcessId[0]);
                }
                break;
        }

            settingsTimeInterval = this.settingsService.get("SRV_PARK_TIMEOUT");
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        } catch (SettingsStorageServiceException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        if(settingsTimeInterval.length>0){
            timeInterval = Long.getLong(settingsTimeInterval[0]);
        }

        try{
            processMessage(header.getSourceId(), processId, timeInterval, header.getMessageType(), messageBody);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
           long sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(ParkingServer.class, self, (int)currentSourceId, (int)sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;
            currentProcessId = -1;

            final int[] types = {(int) ANALOG_VALUE_CORRECTED.getCode(),
                                 (int) ParkingServerMessageType.GPS_CORRECT.getCode()};

            router.registerServer(ParkingServer.class, self, types);
     


        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(ParkingServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }


            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    /**
     * Выполняется после загрузки настроек при получении сообщения
     * @param sourceId
     * @param processId
     * @param timeInterval
     * @param messageType
     * @param messageBody
     * @throws Exception
     */
    private void processMessage(long sourceId, long processId, long timeInterval, long messageType, byte[] messageBody) throws Exception {

        if (sourceId != this.currentSourceId) {
            if (this.currentBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getBufferName((int)processId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentBuffer = this.getBuffer((int)processId);
            if (currentBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(processId)
                        .append(" wasn't created. All data, received from this process, will be ignored by the ParkingServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }
            if(processId==0L||timeInterval==0){
                final String msg = new StringBuilder().append("ProcessId is null || timeInterval is null")
                        .append("The buffer for process ").append(processId)
                        .append(" wasn't created. All data, received from this process, will be ignored by the ParkingServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentBuffer == null) {
            return;
        }

        this.currentBuffer.processData(router,jmsBridgeService,dataStorageService,processId,timeInterval,messageType,messageBody);
    }

    private String getBufferName(int processId) {
        return String.format("src%d:movementStates:buffer", processId);
    }

    private String getSettingsName(int processId) {
        return String.format("src%d:movementStates:settings", processId);
    }

    /**
     * Получает нужный буффер по processId
     * @param processId
     * @return
     */
    private ParkingBuffer getBuffer(int processId) {
        ParkingStateDefinitionSettings settings;
        ParkingStateDefinitionBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(processId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for process %d wasn't found", processId));
                return null;
            }
            settings = ParkingStateDefinitionSettings.parseFrom(settingsObject.toByteArray());

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for process %d", processId), ex);
            return null;
        }

        if (settings == null) {
            LOGGER.log(Level.INFO, String.format("Settings for process %d wasn't found", processId));
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getBufferName(processId));
            if(settingsObject!=null){
                buffer = ParkingStateDefinitionBuffer.parseFrom(settingsObject.toByteArray());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for process %d", processId), ex);
        }

        if (buffer == null) {
            return new ParkingBuffer(settings);
        } else {
            return new ParkingBuffer(settings, buffer);
        }
    }
}
