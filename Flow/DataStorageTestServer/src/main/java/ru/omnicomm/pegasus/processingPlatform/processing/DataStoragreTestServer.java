/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 07.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;

/**
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DataStorageTestServerImplementation")
public class DataStoragreTestServer implements ServerImplementation {

    private DataStorageService dataStorageService;

    @ServerConstructor
    public DataStoragreTestServer(DataStorageService dataStorageService) {
        this.dataStorageService = dataStorageService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
   
    }

    @Override
    public void onSignal(Signal signal) {

    }

}
