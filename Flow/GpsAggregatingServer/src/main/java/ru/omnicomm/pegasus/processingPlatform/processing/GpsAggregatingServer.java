/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.settings.location.GpsAggregatingSettingsParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер агрегации данных GPS;
 * реализация согласно требованиям PP-PSS-04-(GPS Aggregating), Included/REG-FRS-03-(Mileage Calculation)
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("GpsAggregatingServerImplementation")
public class GpsAggregatingServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private static final GpsAggregatingSettingsParser.GpsAggregatingSettings defaultSettings = 
            GpsAggregatingSettingsParser.GpsAggregatingSettings.getDefaultInstance();

    private Router router;
    private JmsBridgeService jmsBridgeService;
    private DataStorageService dataStorageService;
    private SettingsStorageService settingsService;

    private Server self;
    private int currentSourceId;
    private GpsAggregatingBuffer currentBuffer;

    /**
     * Конструктор
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param settingsService ссылка на сервис настроек
     */
    @ServerConstructor
    public GpsAggregatingServer(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService,SettingsStorageService settingsService) {
        this.router = router;
        this.jmsBridgeService = jmsBridgeService;
        this.dataStorageService = dataStorageService;
        this.settingsService = settingsService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageHeader header = null;
        try {
            header = MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
        }

        try{
            processMessage(message);
        } catch (Exception ex) {
            LOGGER.info("Can't process data",ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(GpsAggregatingServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        if (signal instanceof Init) {
            //ИНИЦИАЛИЗАЦИЯ СЕРВЕРА
            Init initSignal = (Init) signal;
            self = initSignal.getSelf();
            currentSourceId = -1;
            router.registerServer(GpsAggregatingServer.class, self, Coordinate.getDefaultInstance().getMessageType());
        } else if (signal instanceof Pause) { // Запрос о приостановке работы сервера
            handlePauseSignal((Pause) signal);
        } else if (signal instanceof Terminate) {
            LOGGER.info(((Terminate) signal).cause());
            router.unregisterServer(GpsAggregatingServer.class, self);
        }
    }

    private void handlePauseSignal(Pause pause) {
        try {
            ((Pause) pause).source().send(new Paused() {

                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }
                
            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void processMessage(MessageLite message) throws InvalidProtocolBufferException, IllegalArgumentException {

        Coordinate dataMessage = Coordinate.parseFrom(message.toByteArray());
        int sourceId = dataMessage.getSourceId();
        int messageTime = dataMessage.getMessageTime();
        if (sourceId != this.currentSourceId) {
            if (this.currentBuffer != null) {
                try {
                    this.settingsService.bindTemporary(this.currentBuffer.getMessageLite(), this.getGpsAggregatingBufferName(this.currentSourceId));
                } catch (Exception ex) {
                    LOGGER.log(Level.WARN, "Can't save the buffer", ex);
                }
            }

            this.currentBuffer = this.getBuffer(sourceId);
            if (currentBuffer == null) {
                final String msg = new StringBuilder().append("The buffer for source ").append(sourceId)
                        .append(" wasn't created. All data, received from this source, will be ignored by the GpsAggregatingServer.").toString();
                LOGGER.log(Level.WARN, msg);
                return;
            }

        }

        if (currentBuffer == null) {
            return;
        }
        
        if(dataMessage.getSatellite()>3 && dataMessage.getValid()){
            this.currentBuffer.processData(router,jmsBridgeService,dataStorageService,dataMessage.getLatitude(),dataMessage.getLongitude(),messageTime);
        }
        
    }

    
    private String getGpsAggregatingBufferName(int sourceId) {

        return String.format("src%d:gpsAggregating:buffer", sourceId);

    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:gpsAggregating:settings", sourceId);

    }

    private GpsAggregatingBuffer getBuffer(int sourceId) {

        GpsAggregatingSettingsParser.GpsAggregatingSettings settings;
        GpsAggregatingSettingsParser.GpsAggregatingBuffer buffer = null;

        try {
            MessageLite settingsObject = this.settingsService.lookup(this.getSettingsName(sourceId));

            if (settingsObject == null) {
                LOGGER.log(Level.INFO, String.format("Settings for source %d wasn't found", sourceId));
                settings =  GpsAggregatingSettingsParser.GpsAggregatingSettings.newBuilder()
                        .setSourceId(sourceId)
                        .setMessageType(defaultSettings.getMessageType())
                        .setMessageTime(0)
                        .setIdleMax(defaultSettings.getIdleMax())
                        .setAggrInterval(defaultSettings.getAggrInterval())
                        .build();
            } else {
                settings = GpsAggregatingSettingsParser.GpsAggregatingSettings.parseFrom(settingsObject.toByteArray());
            }
            

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load settings for source %d", sourceId), ex);
            return null;
        }

        try {
            MessageLite settingsObject = this.settingsService.lookupInMemory(this.getGpsAggregatingBufferName(sourceId));
            if(settingsObject!=null){
                buffer = GpsAggregatingSettingsParser.GpsAggregatingBuffer.parseFrom(settingsObject.toByteArray());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.WARN, String.format("Can't load buffer for source %d", sourceId), ex);
        }

        if (buffer == null) {
            return new GpsAggregatingBuffer(settings);
        } else {
            return new GpsAggregatingBuffer(settings, buffer);
        }
    }


}
