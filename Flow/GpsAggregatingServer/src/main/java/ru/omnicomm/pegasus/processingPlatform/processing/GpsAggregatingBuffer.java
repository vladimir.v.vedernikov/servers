/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.*;
import ru.omnicomm.pegasus.messaging.data.location.GpsAggregatedDataParser.AGRMileageGPS;
import ru.omnicomm.pegasus.messaging.settings.location.GpsAggregatingSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.location.GpsAggregatingSettingsParser.GpsAggregatingBuffer.GpsData;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * Буфер алгоритма агрегации данных по GPS
 * @author alexander
 */
public class GpsAggregatingBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int idleMax;
    private final int aggrInterval;
    private final List<GpsData> buffer;
    private GpsData prevConcentratedMassCenter;

    private final Comparator bufferComparator = new Comparator<GpsData>() {
            @Override
            public int compare(GpsData point1, GpsData point2) {
                return ((Integer)point1.getDataId()).compareTo((Integer)point2.getDataId());
            }
        };

    /**
     * Конструктор
     * @param settings настройки алгоритма агрегации движения для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public GpsAggregatingBuffer(GpsAggregatingSettingsParser.GpsAggregatingSettings  settings) throws IllegalArgumentException {
        this.sourceId = settings.getSourceId();
        this.idleMax = settings.getIdleMax();
        this.aggrInterval = settings.getAggrInterval();
        this.buffer = new ArrayList<GpsData>();
        this.prevConcentratedMassCenter = null;

        if(this.aggrInterval<=0){
            throw new IllegalArgumentException("aggrInterval should be a positive value");
        }
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма агрегации движения для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public GpsAggregatingBuffer(GpsAggregatingSettingsParser.GpsAggregatingSettings settings,
            GpsAggregatingSettingsParser.GpsAggregatingBuffer buffer) throws IllegalArgumentException {

        this(settings);
        if(settings.getIdleMax() == buffer.getIdleMax() &&
           settings.getAggrInterval() == buffer.getAggrInterval()){

            this.buffer.addAll(buffer.getBufferList());
            if(buffer.hasConcentratedMassCenter()){
                this.prevConcentratedMassCenter = buffer.getConcentratedMassCenter();
            }

        }

    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        GpsAggregatingSettingsParser.GpsAggregatingBuffer.Builder builder =
                GpsAggregatingSettingsParser.GpsAggregatingBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(GpsAggregatingSettingsParser.GpsAggregatingBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setIdleMax(this.idleMax)
                .setAggrInterval(this.aggrInterval)
                .addAllBuffer(this.buffer);

        if(this.prevConcentratedMassCenter!=null){
            builder.setConcentratedMassCenter(prevConcentratedMassCenter);
        }

        return builder.build();

    }

    private void addToBuffer(int dataId, double latitude, double longitude){

        this.buffer.add(GpsData.newBuilder().setDataId(dataId).setLatitude(latitude).setLongitude(longitude).build());
        if(this.buffer.size()>1){
            int n = this.buffer.size();
            if(this.buffer.get(n-1).getDataId()>=this.buffer.get(n-2).getDataId()){
                Collections.sort(buffer, bufferComparator);
            }
        }

    }

    /**
     * Производит обработку данных согласно алгоритму расчета пробега по координатам
     * @param router сервис маршрутизатора сообщений
     * @param jmsBridgeService  сервис jms-bridge
     * @param dataStorageService сервис хранения данных
     * @param latitude широта
     * @param longitude долгота
     * @param messageTime временная метка сообщения
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, double latitude, double longitude, int messageTime) {

//        if(this.buffer.size()<15){
//            this.buffer.add(GpsData.newBuilder().setLatitude(latitude).setLongitude(longitude).build());
//            return;
//        }
//
//        if(this.buffer.size()>15){
//            LOGGER.warn("Odd data was found in the GpsAggregatingBuffer");
//            while(this.buffer.size()>15){
//                this.buffer.remove(0);
//            }
//        }

        if(this.buffer.size()<2){
            addToBuffer(messageTime,latitude,longitude);
            return;
        }

        int t_first = this.buffer.get(0).getDataId();
        int t_last = this.buffer.get(this.buffer.size()-1).getDataId();

        if(t_last-t_first<this.aggrInterval){
            addToBuffer(messageTime,latitude,longitude);
            return;
        }

        //Находим сконцентрированный центр масс
        GpsData[] dataArray = this.buffer.toArray(new GpsData[this.buffer.size()]);
        GpsData concentratedMassCenter = this.getConcentratedMassCenter(dataArray);

        if(this.prevConcentratedMassCenter!=null){
            this.sendToServices(router, jmsBridgeService, dataStorageService, getAGRMileageVMessage(sourceId, messageTime, this.getDistance(this.prevConcentratedMassCenter, concentratedMassCenter)));
        }
        this.prevConcentratedMassCenter=concentratedMassCenter;

//        while(this.buffer.size()>1){
//            this.buffer.remove(0);
//        }
        this.buffer.clear();

        addToBuffer(messageTime,latitude,longitude);

    }

    private double sin(double deg){
        return Math.sin(Math.toRadians(deg));
    }

    private double cos(double deg){
        return Math.cos(Math.toRadians(deg));
    }

    private GpsData getConcentratedMassCenter(GpsData[] points){

//        if(points.length!=15){
//            throw new IllegalArgumentException("Wrong length of coordinate points array: "+points.length);
//        }

        final GpsData point_mc = getMassCenter(points);

        //Сортируем входной массив таким образом, чтобы точки с наименьшим расстоянием до найденного центра масс оказались в начале
        Arrays.sort(points, new Comparator<GpsData>() {
            @Override
            public int compare(GpsData point1, GpsData point2) {
                return ((Long)getDistance(point1,point_mc)).compareTo((Long)getDistance(point1,point_mc));
            }
        });

        int arrayLength = points.length*2/3; //Для FAS было 10 (т.к. points.length всегда была равна 15)
     //   GpsData[] points_ = new GpsData[10];
        GpsData[] points_ = new GpsData[arrayLength];
        System.arraycopy(points, 0, points_, 0, points_.length);

        return getMassCenter(points_);


    }

    private GpsData getMassCenter(GpsData[] points){
        if(points.length<=0){
            throw new IllegalArgumentException("Coordinate points array shouldn't be empty");
        }

        double l_mc = 0.0;
        double g_mc = 0.0;
        for (int i = 0; i < points.length; i++) {
            l_mc+=points[i].getLatitude();
            g_mc+=points[i].getLongitude();
        }
        return GpsData.newBuilder().setLatitude(l_mc/points.length).setLongitude(g_mc/points.length).build();
        
    }

    /**
     * Возвращает расстояние между двумя точками
     *
     * @param latitude1  широта  первой точки
     * @param longitude1 долгота первой точки
     * @param latitude2  широта  второй точки
     * @param longitude2 долгота второй точки
     * @return расстояние в мм (точность согласно требованиям PP-SRS-01 (Format Message), AgrMileageGPS)
     */
    private long getDistance(GpsData point1, GpsData point2){

        double earth_radius = 6372.0;

        double dist =
                2 * earth_radius * 1000000 * Math.asin(
                    Math.sqrt(
                        Math.pow(sin((point1.getLatitude() - point2.getLatitude()) / 2.0), 2) +
                        cos(point1.getLatitude()) * cos(point2.getLatitude()) *
                        Math.pow(sin((point1.getLongitude() - point2.getLongitude()) / 2.0), 2)
                    )
                );

        return (long)dist;


    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getAGRMileageVMessage(int sourceId, int messageTime, long value) {
        
        int newMessageTime = messageTime - (messageTime % this.aggrInterval);
        return AGRMileageGPS.newBuilder()
                .setSourceId(sourceId).setMessageType(AGRMileageGPS.getDefaultInstance().getMessageType()).setMessageTime(newMessageTime)
                .setValue(value)
                .build();
    }


}
