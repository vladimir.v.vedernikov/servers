/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 26.08.11
 */
package ru.omnicomm.pegasus.processing.generator;

import java.util.Iterator;

/**
 * Используется в качестве генератора сообщений.
 * Расширяет интерфейс {@link Iterator} который итерируется по {@link MessageInfo}.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public interface DataSource extends Iterator<MessageInfo> {
}
