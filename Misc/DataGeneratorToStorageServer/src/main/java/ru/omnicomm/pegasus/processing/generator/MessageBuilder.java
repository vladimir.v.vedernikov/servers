/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 01.09.11
 */
package ru.omnicomm.pegasus.processing.generator;

import java.util.Iterator;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
interface MessageBuilder extends Iterator<MessageInfo> {
}
