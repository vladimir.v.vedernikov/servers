/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 30.08.11
 */
package ru.omnicomm.pegasus.processing.generator;

import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Тестовый сервер генерации пакетов в хранилище
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DataGeneratorToStorageServerImplementation")
public class DataGeneratorToStorageServer extends AbstractGenerator implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private static final String DEFAULT_LOG_FILE_NAME = "DataGeneratorToStorageServer.log";

    private SettingsStorageService settingsService;

    private String inputFileName;
    private String logFileName;
    private boolean logging = true;

    /**
     * Конструктор.
     *
     * @param dataStorageService сервис хранилища.
     * @param settingsService сервис хранения настроек.
     */
    @ServerConstructor
    public DataGeneratorToStorageServer(DataStorageService dataStorageService, SettingsStorageService settingsService) {
        super(dataStorageService); //KIA: Изменил router на dataStorageService
        this.settingsService = settingsService;
    }

    @Override
    protected void loadSettings() {
//        try {
//            final String settingsName = getDataGeneratorServerSettingsName();
//            DataGeneratorServerSettings settings = (DataGeneratorServerSettings) settingsService.lookup(settingsName);
//            if (settings != null) {
//                inputFileName = settings.getInputFile();
//                logging = settings.getLogging();
//                logFileName = settings.getLogFile();
//            } else {
//                LOGGER.log(Level.INFO, "Can''t find settings " + settingsName);
//            }
//        } catch (Exception ex) {
//            LOGGER.log(Level.INFO, "Can''t load settings", ex);
//        }
    }

    /**
     * Создается {@link FileDataSource}, который загружает данные из файла.
     *
     * @return источник данных.
     */
    @Override
    protected DataSource createDataSource() {
        String fileName = getInputFileName();
        FileDataSource dataSource = new FileDataSource(fileName);
        if (dataSource.getResourceFile(fileName) == null) {
            String message = new StringBuilder().append("File ").append(fileName).append(" wasn't found").toString();
            throw new RuntimeException(message);
        }
        return dataSource;
    }

    @Override
    protected ResultWriter createResultWriter() {
        if (isLogging()) {
            return super.createResultWriter();
        } else {
            return null;
        }
    }

    @Override
    protected String getResultWriterFileName() {
        return getLogFileName();
    }

    private String getDataGeneratorServerSettingsName() {
        return "src0:dataGeneratorToStorageServer:settings";//KIA исправил и тут
    }

    public String getInputFileName() {
        if (inputFileName == null) {
            return DEFAULT_DATA_FILE_NAME;
        }
        return inputFileName;
    }

    public String getLogFileName() {
        if (logFileName == null) {
            return DEFAULT_LOG_FILE_NAME;
        }
        return logFileName;
    }

    public boolean isLogging() {
        return logging;
    }
}
