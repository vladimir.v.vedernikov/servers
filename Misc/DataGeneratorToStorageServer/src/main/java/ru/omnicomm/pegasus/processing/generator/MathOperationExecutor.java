/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 02.09.11
 */
package ru.omnicomm.pegasus.processing.generator;

import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
class MathOperationExecutor {

    private static Map<Class<? extends Number>, Method> addMethods = new HashMap<Class<? extends Number>, Method>();
    private static Map<Class<? extends Number>, Method> subtractMethods = new HashMap<Class<? extends Number>, Method>();
    private static Map<Class<? extends Number>, Method> lessThanMethods = new HashMap<Class<? extends Number>, Method>();
    private static Map<Class<? extends Number>, Method> greaterThanMethods = new HashMap<Class<? extends Number>, Method>();
    private static Map<Class<? extends Number>, Method> absMethods = new HashMap<Class<? extends Number>, Method>();

    static {
        try {
            addMethods.put(Integer.class, MathOperationExecutor.class.getDeclaredMethod("add", Integer.class, Integer.class));
            addMethods.put(Long.class, MathOperationExecutor.class.getDeclaredMethod("add", Long.class, Long.class));
            addMethods.put(Double.class, MathOperationExecutor.class.getDeclaredMethod("add", Double.class, Double.class));
            addMethods.put(Float.class, MathOperationExecutor.class.getDeclaredMethod("add", Float.class, Float.class));

            subtractMethods.put(Integer.class, MathOperationExecutor.class.getDeclaredMethod("subtract", Integer.class, Integer.class));
            subtractMethods.put(Long.class, MathOperationExecutor.class.getDeclaredMethod("subtract", Long.class, Long.class));
            subtractMethods.put(Double.class, MathOperationExecutor.class.getDeclaredMethod("subtract", Double.class, Double.class));
            subtractMethods.put(Float.class, MathOperationExecutor.class.getDeclaredMethod("subtract", Float.class, Float.class));

            lessThanMethods.put(Integer.class, MathOperationExecutor.class.getDeclaredMethod("lessThan", Integer.class, Integer.class));
            lessThanMethods.put(Long.class, MathOperationExecutor.class.getDeclaredMethod("lessThan", Long.class, Long.class));
            lessThanMethods.put(Double.class, MathOperationExecutor.class.getDeclaredMethod("lessThan", Double.class, Double.class));
            lessThanMethods.put(Float.class, MathOperationExecutor.class.getDeclaredMethod("lessThan", Float.class, Float.class));

            greaterThanMethods.put(Integer.class, MathOperationExecutor.class.getDeclaredMethod("greaterThan", Integer.class, Integer.class));
            greaterThanMethods.put(Long.class, MathOperationExecutor.class.getDeclaredMethod("greaterThan", Long.class, Long.class));
            greaterThanMethods.put(Double.class, MathOperationExecutor.class.getDeclaredMethod("greaterThan", Double.class, Double.class));
            greaterThanMethods.put(Float.class, MathOperationExecutor.class.getDeclaredMethod("greaterThan", Float.class, Float.class));

            absMethods.put(Integer.class, MathOperationExecutor.class.getDeclaredMethod("abs", Integer.class));
            absMethods.put(Long.class, MathOperationExecutor.class.getDeclaredMethod("abs", Long.class));
            absMethods.put(Float.class, MathOperationExecutor.class.getDeclaredMethod("abs", Float.class));
            absMethods.put(Double.class, MathOperationExecutor.class.getDeclaredMethod("abs", Double.class));
        } catch (NoSuchMethodException e) {
            LoggerFactory.getLogger().log(Level.ERROR, null, e);
        }
    }

    public MathOperationExecutor() {
    }

    /**
     * @param a первое значение.
     * @param b второе значение.
     * @return результат выражения {@code a + b}
     */
    public <T extends Number> T add(T a, T b) {
        return (T) invokeMethod(addMethods, a, b);
    }

    /**
     * @param a первое значение.
     * @param b второе значение.
     * @return результат выражения {@code a - b}
     */
    public <T extends Number> T subtract(T a, T b) {
        return (T) invokeMethod(subtractMethods, a, b);
    }

    /**
     * @param a первое значение.
     * @param b второе значение.
     * @return результат сравнения {@code a < b}
     */
    public <T extends Number> boolean lessThan(T a, T b) {
        return (Boolean) invokeMethod(lessThanMethods, a, b);
    }

    /**
     * @param a первое значение.
     * @param b второе значение.
     * @return результат сравнения {@code a > b}
     */
    public <T extends Number> boolean greaterThan(T a, T b) {
        return (Boolean) invokeMethod(greaterThanMethods, a, b);
    }

    /**
     * @param value значение.
     * @return абсолютное значение.
     */
    public <T extends Number> T abs(T value) {
        return (T) invokeMethod(absMethods, value);
    }

    private Object invokeMethod(Map<Class<? extends Number>, Method> methods, Number... args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Empty argument's array");
        }
        Class<? extends Number> argumentType = args[0].getClass();
        Method method = methods.get(argumentType);
        if (method == null) {
            throw new IllegalArgumentException("Unsupported argument type " + argumentType);
        }

        try {
            return method.invoke(this, args);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e); //todo
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e); // todo
        }
    }

    private Integer add(Integer a, Integer b) {
        return a + b;
    }

    private Long add(Long a, Long b) {
        return a + b;
    }

    private Float add(Float a, Float b) {
        return a + b;
    }

    private Double add(Double a, Double b) {
        return a + b;
    }

    private Integer subtract(Integer a, Integer b) {
        return a - b;
    }

    private Long subtract(Long a, Long b) {
        return a - b;
    }

    private Float subtract(Float a, Float b) {
        return a - b;
    }

    private Double subtract(Double a, Double b) {
        return a - b;
    }

    private boolean lessThan(Integer a, Integer b) {
        return a < b;
    }

    private boolean lessThan(Long a, Long b) {
        return a < b;
    }

    private boolean lessThan(Float a, Float b) {
        return a < b;
    }

    private boolean lessThan(Double a, Double b) {
        return a < b;
    }

    private boolean greaterThan(Integer a, Integer b) {
        return a > b;
    }

    private boolean greaterThan(Long a, Long b) {
        return a > b;
    }

    private boolean greaterThan(Float a, Float b) {
        return a > b;
    }

    private boolean greaterThan(Double a, Double b) {
        return a > b;
    }

    private Integer abs(Integer value) {
        return Math.abs(value);
    }

    private Long abs(Long value) {
        return Math.abs(value);
    }

    private Float abs(Float value) {
        return Math.abs(value);
    }

    private Double abs(Double value) {
        return Math.abs(value);
    }

}
