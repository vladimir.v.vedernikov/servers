/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 18.11.11
 */
package ru.omnicomm.pegasus.processing.generator;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Используется в качестве обертки списка {@code MessageBuilder}'ов.
 * Для компановки нескольких {@code MessageBuilder}'ов в один.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
class MessageBuilderCompound implements MessageBuilder {

    private Iterator<MessageBuilder> iterator;

    /**
     * Текущий построитель сообщений.
     */
    private MessageBuilder currentBuilder;

    private int timeout;

    MessageBuilderCompound(int timeout, MessageBuilder... builders) {
        this.timeout = timeout;
        iterator = Arrays.asList(builders).iterator();
    }

    @Override
    public boolean hasNext() {
        if (currentBuilder != null && currentBuilder.hasNext()) {
            // Текущий построитель сообщений определен и пожет построить еще сообщения.
            // Случай когда используются сообщения с диапазоном значений.
            return true;
        } else {
            // Либо это первый вызов hasNext у FileDataSource, либо текущий построитель сообщений исчерпал свои ресурсы.
            // В этом определим следующий.
            while (iterator.hasNext()) {
                // Следующий построитель.
                MessageBuilder builder = iterator.next();
                if (builder.hasNext()) { // Способен построить сообщение/я.
                    currentBuilder = builder;
                    return true;
                }
            }
            // Не найден ни один билдер для создания сообщений.
            return false;
        }
    }

    @Override
    public MessageInfo next() {
        MessageInfo info = currentBuilder.next();
        if (timeout != 0) {
            info.setTimeout(timeout);
            timeout = 0;
        }
        return info;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
