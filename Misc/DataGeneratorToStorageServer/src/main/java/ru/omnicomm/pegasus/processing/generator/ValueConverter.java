/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 01.09.11
 */
package ru.omnicomm.pegasus.processing.generator;

import com.google.protobuf.Descriptors;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * Вспомогательный класс для конвертирования строкового значения в занчение типа параметра сообщения.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
class ValueConverter {

    private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private static final String DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";
    private DateFormat dateFormatter;//yyyy.mm.dd HH:mm:ss

    /**
     * Используется для проверки нужного количества цифер в строке с датой. Количество цафер в году, месяце, дне и т.д.
     * должно строго соответствовать yyyy.mm.dd HH:mm:ss. DP-119.
     */
    private Pattern datePattern;

    ValueConverter() {
        dateFormatter = new SimpleDateFormat(DATE_FORMAT);//yyyy.mm.dd HH:mm:ss
        dateFormatter.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("UTC")));
        dateFormatter.setLenient(false);

        datePattern = Pattern.compile("[\\d]{4}.[\\d]{2}.[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}");
    }

    public Object convertValue(Descriptors.FieldDescriptor fieldDescriptor, String strValue)
            throws MessageBuildException {
        Descriptors.FieldDescriptor.JavaType javaType = fieldDescriptor.getJavaType();
        return convertValue(javaType, strValue);
    }

    private Object convertValue(Descriptors.FieldDescriptor.JavaType javaType, String value)
            throws MessageBuildException {
        try {
            switch (javaType) {
                case INT:
                    return Integer.valueOf(value);
                case STRING:
                    return value;
                case DOUBLE:
                    return Double.valueOf(value);
                case FLOAT:
                    return Float.valueOf(value);
                case LONG:
                    return Long.valueOf(value);
                case BOOLEAN:
                    return Boolean.valueOf(value);
                case BYTE_STRING:
                    throwUnsupportedException(Descriptors.FieldDescriptor.JavaType.BYTE_STRING);
                case ENUM:
                    throwUnsupportedException(Descriptors.FieldDescriptor.JavaType.ENUM);
                case MESSAGE:
                    throwUnsupportedException(Descriptors.FieldDescriptor.JavaType.MESSAGE);
                default:
                    throw new MessageBuildException(String.format("Unknown proto java type=%s", javaType)); //development error
            }
        } catch (NumberFormatException e) {
            throw new MessageBuildException(new StringBuilder().append("Cannot convert value=").append(value).toString());
        }
    }

    public int convertMesasgeTime(String stringTime)
            throws MessageBuildException {
        try {
            if (!datePattern.matcher(stringTime).matches()) {
                throw new MessageBuildException("Invalid message time values format. Usage: yyyy.MM.dd HH:mm:ss");
            }
            Date date = dateFormatter.parse(stringTime);
            return getMessageTimeStamp(date.getTime());
        } catch (ParseException e) {
            throw new MessageBuildException("Invalid message time values format. Usage: yyyy.MM.dd HH:mm:ss", e);
        } catch (IllegalArgumentException e) {
            final String error = new StringBuilder().append("Invalid message time value ").append(stringTime).toString();
            throw new MessageBuildException(error, e);
        }
    }

    public String convertMesasgeTime(int time) {
        Date date = getDate(time);
        return dateFormatter.format(date);
    }

    private void throwUnsupportedException(Descriptors.FieldDescriptor.JavaType javaType) {
        throw new UnsupportedOperationException(String.format("Unsupported proto java type=%s", javaType));
    }

    public int getMessageTimeStamp(long milliseconds) {
        if (milliseconds < STARTING_POINT) {
            final String error = new StringBuilder().append("Value ").append(milliseconds)
                    .append(" have to be equal to or greater than ").append(STARTING_POINT).toString();
            throw new IllegalArgumentException(error);
        }
        return (int) ((milliseconds - STARTING_POINT) / 1000);
    }

    public Date getDate(int timeStamp) {
        return new Date(STARTING_POINT + timeStamp * 1000L);
    }
}
