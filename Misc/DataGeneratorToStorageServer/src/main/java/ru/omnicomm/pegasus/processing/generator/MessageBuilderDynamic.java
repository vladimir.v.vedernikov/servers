/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 01.09.11
 */
package ru.omnicomm.pegasus.processing.generator;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.messages.DataGeneratorServerMessageType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
class MessageBuilderDynamic implements MessageBuilder {

    /**
     * Количество сообщений генерируемых данным билдером.
     */
    private int quantity;

    /**
     * Задержка перед первым сообщением генерируемым построителем.
     */
    private int timeout;

    /**
     * Итерация.
     */
    private int iteration;

    /**
     * Используется для неоднократного получения proto-builder'а.
     */
    private DataGeneratorServerMessageType messageType;

    /**
     * Идентификатор источника данных.
     */
    private int sourceId;

    /**
     * Строковое представления времени сообщения. Передается в строке, т.к. может быть сложным и задаваться диапазоном.
     */
    private String messageTime;

    /**
     * Метаинформация сообщения.
     */
    private Descriptors.Descriptor messageDescriptor;

    private ValueConverter converter;

    private List<Value> fieldValues;

    void initialize(int quantity, int timeout, int sourceId, DataGeneratorServerMessageType messageType, String messageTime, List<String> values)
            throws MessageBuildException {
        this.quantity = quantity;
        this.timeout = timeout;
        this.sourceId = sourceId;
        this.messageType = messageType;
        this.messageTime = messageTime;

        this.fieldValues = new ArrayList<Value>();
        this.iteration = 0;

        initialize(values);
    }

    /**
     * Проверяет на валидность значения сообщения. Строит wrapper'ы для значений.
     *
     * @param values список значений.
     * @throws MessageBuildException при некорректных данных в строке.
     */
    private void initialize(List<String> values)
            throws MessageBuildException {

        try {
            messageDescriptor = DataGeneratorServerMessageType.getDescriptor(messageType);
        } catch (IllegalArgumentException e) {
            throw new MessageBuildException(e);
        }

        List<Descriptors.FieldDescriptor> fieldDescriptors = messageDescriptor.getFields();
        int valueIndex = 0;
        for (Descriptors.FieldDescriptor fieldDescriptor : fieldDescriptors) {

            if (valueIndex >= values.size() + 3) {
                // Проверяем в этом случае оставшиеся значения на их обязательность.
                if (fieldDescriptor.isRequired()) {
                    throw notEnoughDataException(fieldDescriptor);
                } else {
                    fieldValues.add(new NoValue());
                }
            } else {
                switch (valueIndex) {
                    case 0:
                        fieldValues.add(new SimpleValue(sourceId));
                        break; // sourceId
                    case 1:
                        fieldValues.add(new SimpleValue(messageType.getCode()));
                        break; // messageType
                    case 2:
                        fieldValues.add(getMessageTimeValue());
                        break; // messageTime
                    default: {
                        String value = values.get(valueIndex - 3);
                        fieldValues.add(getValue(fieldDescriptor, value));
                    }
                }
            }
            valueIndex++;
        }
    }

    private MessageBuildException notEnoughDataException(Descriptors.FieldDescriptor fieldDescriptor)
            throws MessageBuildException {
        final String errorMesage = new StringBuilder().append("Not enough data for message ")
                .append(messageType).append(". Field ").append(fieldDescriptor.getName())
                .append(" is required.").toString();
        return new MessageBuildException(errorMesage);
    }

    private Value getValue(Descriptors.FieldDescriptor descriptor, String value)
            throws MessageBuildException {
        if (value.isEmpty()) {
            if (descriptor.isRequired()) {
                // Случай, когда незадано значение и поле является обязательным.
                throw notEnoughDataException(descriptor);
            } else {
                // Значение не задано, не будем ничего сетить в прото-билдер.
                return new NoValue();
            }
        } else if (descriptor.isRepeated()) {
            return getRepeatedValue(descriptor, value);
        } else if (ParserUtil.isCompoundValue(value)) { // сложное изменяемое значение
            return getCompoundValue(descriptor, value);
        } else {
            return getSimpleValue(descriptor, value);
        }
    }

    private Value getSimpleValue(Descriptors.FieldDescriptor descriptor, String value)
            throws MessageBuildException {
        Object converted = converter.convertValue(descriptor, value);
        return new SimpleValue(converted);
    }

    private Value getCompoundValue(Descriptors.FieldDescriptor descriptor, String value)
            throws MessageBuildException {
        // Сложным может быть только числовое поле.
        validateCompoundValueType(descriptor);

        List<String> strValues = ParserUtil.getCompaundValueList(value);
        // должно быть 4 значения в формате: {<начальное значение>, <инкремент>, <левая граница>, <правая граница>}
        if (strValues.size() < 4) {
            final String errorMessage = new StringBuilder().append("Not enough data in compound startValue:{")
                    .append(value).append("}. Usage: {startValue, increment, left_limit, right_limit}").toString();
            throw new MessageBuildException(errorMessage);
        }
        List<Object> values = new ArrayList<Object>(strValues.size());
        for (String strValue : strValues) {
            Object converted = converter.convertValue(descriptor, strValue);
            values.add(converted);
        }

        Number startValue = (Number) values.get(0);
        Number increment = (Number) values.get(1);
        Number leftLimit = (Number) values.get(2);
        Number rightLimit = (Number) values.get(3);
        return new CompoundValue<Number>(startValue, increment, leftLimit, rightLimit);
    }

    private void validateCompoundValueType(Descriptors.FieldDescriptor descriptor)
            throws MessageBuildException {
        Descriptors.FieldDescriptor.JavaType javaType = descriptor.getJavaType();
        switch (javaType) {
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
                return;
            default:
                throw new MessageBuildException("Value with type " + javaType + " cannot be compound.");
        }
    }

    private Value getRepeatedValue(Descriptors.FieldDescriptor descriptor, String repeatedValue)
            throws MessageBuildException {
        List<Value> values = new ArrayList<Value>();
        if (ParserUtil.isRepeatedValue(repeatedValue)) {
            List<String> strValues = ParserUtil.getRepeatedValueList(repeatedValue);
            for (String strValue : strValues) {
                if (ParserUtil.isCompoundValue(strValue)) {
                    values.add(getCompoundValue(descriptor, strValue));
                } else {
                    values.add(getSimpleValue(descriptor, strValue));
                }
            }
        } else {
            if (ParserUtil.isCompoundValue(repeatedValue)) {
                values.add(getCompoundValue(descriptor, repeatedValue));
            } else {
                values.add(getSimpleValue(descriptor, repeatedValue));
            }
        }

        return new RepeatedValue(values);
    }

    public Value getMessageTimeValue()
            throws MessageBuildException {
        if (ParserUtil.isCompoundValue(messageTime)) {
            List<String> strValues = ParserUtil.getCompaundValueList(messageTime);
            // должно быть 4 значения в формате: {<начальное значение>, <инкремент>, <левая граница>, <правая граница>}
            if (strValues.size() < 4) {
                final String errorMessage = new StringBuilder().append("Not enough data in compound startValue:{")
                        .append(messageTime).append("}. Usage: {startValue, increment, left_limit, right_limit}").toString();
                throw new MessageBuildException(errorMessage);
            }

            Integer startDate = converter.convertMesasgeTime(strValues.get(0));
            Integer increment;
            try {
                increment = Integer.parseInt(strValues.get(1));
            } catch (NumberFormatException e) {
                final String error = new StringBuilder().append("Illegal incremet value=")
                        .append(strValues.get(1)).append(" for message type").toString();
                throw new MessageBuildException(error);
            }
            Integer leftLimit = converter.convertMesasgeTime(strValues.get(2));
            Integer rightLimit = converter.convertMesasgeTime(strValues.get(3));

            return new CompoundValue<Integer>(startDate, increment, leftLimit, rightLimit);
        } else {
            int value = converter.convertMesasgeTime(messageTime);
            return new SimpleValue(value);
        }
    }

    @Override
    public boolean hasNext() {
        return iteration < quantity;
    }

    @Override
    public MessageInfo next() {
        MessageInfo messageInfo = buildMessageInfo();
        iteration++;

        return messageInfo;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    private MessageInfo buildMessageInfo() {
        GeneratedMessage.Builder protoBuilder = DataGeneratorServerMessageType.getBuilder(messageType);
        List<Descriptors.FieldDescriptor> fieldDescriptors = messageDescriptor.getFields();
        int valueIndex = 0;
        for (Descriptors.FieldDescriptor fieldDescriptor : fieldDescriptors) {
            Value fieldValue = fieldValues.get(valueIndex);
            fieldValue.setValue(fieldDescriptor, protoBuilder);

            valueIndex++;
        }
        MessageLite message = protoBuilder.build();
        return new MessageInfo(message, timeout);
    }

    public void setConverter(ValueConverter converter) {
        this.converter = converter;
    }

    private interface Value {
        void setValue(Descriptors.FieldDescriptor fieldDescriptor, GeneratedMessage.Builder protoBuilder);

        Object getValue();
    }

    private abstract class AbstractValue implements Value {
        @Override
        public void setValue(Descriptors.FieldDescriptor fieldDescriptor, GeneratedMessage.Builder protoBuilder) {
            protoBuilder.setField(fieldDescriptor, getValue());
        }

        public abstract Object getValue();
    }

    private class SimpleValue extends AbstractValue {

        private Object value;

        private SimpleValue(Object value) {
            this.value = value;
        }

        @Override
        public Object getValue() {
            return value;
        }
    }

    private class CompoundValue<T extends Number> extends AbstractValue {

        private MathOperationExecutor math = new MathOperationExecutor();

        private T startValue;

        private T left;

        private T right;

        private T increment;

        private T nextValue;

        private CompoundValue(T startValue, T increment, T leftLimit, T rightLimit)
                throws MessageBuildException {
            validateValues(startValue, increment, leftLimit, rightLimit);

            this.startValue = startValue;
            this.increment = increment;
            this.left = leftLimit;
            this.right = rightLimit;
        }

        private void validateValues(T startValue, T increment, T leftLimit, T rightLimit)
                throws MessageBuildException {
            // Проверка: B >= A
            if (math.lessThan(rightLimit, leftLimit)) {
                final String message = new StringBuilder().append("In compound value right limit(B)=").append(rightLimit)
                        .append(" cannot be less than left limit(A)=").append(leftLimit).toString();
                throw new MessageBuildException(message);
            }

            // Проверка: |S| =< B-A
            T abDifference = math.subtract(rightLimit, leftLimit);
            T incrementMod = math.abs(increment);
            if (math.greaterThan(incrementMod, abDifference)) {
                throw new MessageBuildException("Wrong compound values. Condition (|S| =< B-A) is faild.");
            }

            // Проверка на то, что стартовое значение попадает в диапазон.
            if (math.lessThan(startValue, leftLimit) || math.greaterThan(startValue, rightLimit)) {
                final String error = new StringBuilder().append("Start value ").append(startValue)
                        .append(" is out of range [").append(leftLimit).append(", ")
                        .append(rightLimit).append("]").toString();
                throw new MessageBuildException(error);
            }
        }

        public Object getValue() {
            if (nextValue == null) {
                // первый вызов метода.
                nextValue = startValue;
            } else {
                nextValue = getNextValue();
            }

            return nextValue;
        }

        private T getNextValue() {
            T result = math.add(nextValue, increment);
            // Проверяем не вышло ли новое значение за границы допустимых значений.
            if (math.lessThan(right, result)) {
                // Вышло за правую границу, скорректируем значение как описано в DP-PSS-01-(Test Server).docx
                // A + (Ai-1 + S) - B, Ai-1 + S > B, где
                // A - начало диапазона (левая граница), B - окончание диапазона (правая граница),
                // S - шаг изменения параметра, Ai-1 - значение параметра для предыдущей строки
                // (для самого первого сообщения равно заданному в строке значению параметра)
                result = math.add(result, left);
                result = math.subtract(result, right);
            } else if (math.greaterThan(left, result)) {
                // Вышло за левую границу, скорректируем значение как описано в DP-PSS-01-(Test Server).docx
                // B + (Ai-1 + S) - A, Ai-1 + S < A, где
                // A - начало диапазона (левая граница), B - окончание диапазона (правая граница),
                // S - шаг изменения параметра, Ai-1 - значение параметра для предыдущей строки
                // (для самого первого сообщения равно заданному в строке значению параметра)
                result = math.add(result, right);
                result = math.subtract(result, left);
            }
            return result;
        }
    }

    private class RepeatedValue extends AbstractValue {

        private List<Value> values;

        private RepeatedValue(List<Value> values) {
            this.values = values;
        }

        @Override
        public Object getValue() {
            List<Object> result = new ArrayList<Object>();
            for (Value value : values) {
                result.add(value.getValue());
            }
            return result;
        }
    }

    private class NoValue implements Value {
        @Override
        public void setValue(Descriptors.FieldDescriptor fieldDescriptor, GeneratedMessage.Builder protoBuilder) {
            // do nothing;
        }

        @Override
        public Object getValue() {
            throw new UnsupportedOperationException();
        }
    }
}
