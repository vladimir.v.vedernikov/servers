/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 30.08.11
 */
package ru.omnicomm.pegasus.processing.generator;

import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Генератор сообщений.
 * Входящими данными является текстовый файл, имя которого по умолчанию определено как
 * {@link AbstractGenerator#DEFAULT_DATA_FILE_NAME}. Генератор считывает данные из файла построчно и для каждой строки
 * формирует одно или несколько сообщений.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class FileDataSource implements DataSource {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private DataLineParser dataLineParser;

    private RandomAccessFile inputDataFile;

    private Iterator<MessageBuilder> dataIterator;

    private String dataFileName;

    /**
     * Текущий построитель сообщений.
     */
    private MessageBuilder currentBuilder;

    /**
     * Создает экземпляр файлового источника данных.
     *
     * @param dataFileName имя файла с данными.
     * @throws NullPointerException если {@code dataFileName null}.
     */
    public FileDataSource(String dataFileName) {
        if (dataFileName == null) {
            throw new NullPointerException("Parameter dataFileName is null.");
        }
        this.dataFileName = dataFileName;
    }

    @Override
    public boolean hasNext() {
        if (currentBuilder != null && currentBuilder.hasNext()) {
            // Текущий построитель сообщений определен и пожет построить еще сообщения.
            // Случай когда используются сообщения с диапазоном значений.
            return true;
        } else {
            // Либо это первый вызов hasNext у FileDataSource, либо текущий построитель сообщений исчерпал свои ресурсы.
            // В этом определим следующий.
            while (getDataIterator().hasNext()) {
                // Следующий построитель.
                MessageBuilder builder = getDataIterator().next();
                if (builder.hasNext()) { // Способен построить сообщение/я.
                    currentBuilder = builder;
                    return true;
                }
            }
            // Не найден ни один билдер для создания сообщений.
            return false;
        }
    }

    @Override
    public MessageInfo next() {
        return currentBuilder.next();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    private Iterator<MessageBuilder> getDataIterator() {
        // Первое обращение. Загрузим данные из файла.
        if (dataIterator == null) {
            long t1 = System.currentTimeMillis();
            List<MessageBuilder> messageInfos = readData();
            long t2 = System.currentTimeMillis();
            if (LOGGER.isDebugEnabled()) {
                final String debug = new StringBuilder().append(getFileName()).append(" has been read for ")
                        .append(t2 - t1).append(" ms. MessageBuilder list size = ")
                        .append(messageInfos.size()).toString();
                LOGGER.info(debug);
            }


            dataIterator = messageInfos.iterator();
        }
        return dataIterator;
    }

    /**
     * @return возвращает список билдеров полученных по данным из файла. Никогда не возвращает {@code null}.
     */
    private List<MessageBuilder> readData() {
        List<MessageBuilder> result = new ArrayList<MessageBuilder>();
        String dataLine = null;
        try {
            dataLine = getInputDataFile().readLine();
        } catch (IOException e) {
            LOGGER.log(Level.WARN, null, e);
        }
        while (dataLine != null) {
            if (dataLine.startsWith(ParserUtil.COMMENT)) {
                // skip this line
            } else if (!dataLine.isEmpty()) {
                try {
//                    final MessageBuilder builder = messageBuilderFactory.getBuilder(dataLine);
                    final MessageBuilder builder = getDataLineParser().parse(dataLine);
                    result.add(builder);
                } catch (MessageBuildException e) {
                    final String errorMessage
                            = new StringBuilder().append("Cannot create message from line=").append(dataLine)
                            .toString();
                    LOGGER.log(Level.WARN, errorMessage, e);
                }
            }

            dataLine = null;
            try {
                dataLine = getInputDataFile().readLine();
            } catch (IOException e) {
                LOGGER.log(Level.WARN, null, e);
            }
        }
        return result;
    }

    private RandomAccessFile getInputDataFile()
            throws FileNotFoundException {
        if (inputDataFile == null) {
            inputDataFile = createInputDataFile();
        }

        return inputDataFile;
    }

    private RandomAccessFile createInputDataFile()
            throws FileNotFoundException {
        String fileName = getFileName();
        File file = getResourceFile(fileName);
        if (file == null) {
            final String message = new StringBuilder().append("File ").append(fileName).append(" wasn't found").toString();
            throw new FileNotFoundException(message);
        }

        return new RandomAccessFile(file, "rw");
    }

    private String getFileName() {
        if (dataFileName == null) {
            return AbstractGenerator.DEFAULT_DATA_FILE_NAME;
        } else {
            return dataFileName;
        }
    }

    public File getResourceFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            return file;
        }

        // пробуем достать из ресурсов.
        URL url = getClass().getResource("/" + fileName);
        if (url == null) {
            return null;
        }

        try {
            file = new File(url.toURI());
            if (file.exists()) {
                return file;
            }
        } catch (URISyntaxException e) {
            LOGGER.log(Level.WARN, e.getMessage(), e);
        }

        return null;
    }

    public DataLineParser getDataLineParser() {
        if (dataLineParser == null) {
            dataLineParser = new CommonDataLineParser();
        }
        return dataLineParser;
    }

    public void setDataLineParser(DataLineParser dataLineParser) {
        this.dataLineParser = dataLineParser;
    }
}
