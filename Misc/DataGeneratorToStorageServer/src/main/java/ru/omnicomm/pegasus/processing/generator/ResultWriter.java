/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 08.09.11
 */
package ru.omnicomm.pegasus.processing.generator;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.MessageParser;
import ru.omnicomm.pegasus.processingPlatform.messages.DataGeneratorServerMessageType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Пишет все сгенерированные данные в файлик.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
class ResultWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Writer writer;

    //Используется для получения даты при генерациия сообщения в результирующий файл.
    private Date dat = new Date();
    private final static String format = "{0,date} {0,time}";
    private MessageFormat formatter;
    private ValueConverter valueConverter;
    private Object args[] = new Object[1];

    ResultWriter(String fileName)
            throws IOException {
        FileOutputStream ostream = null;
        try {
            //
            //   attempt to create file
            //
            ostream = new FileOutputStream(fileName);
        } catch (FileNotFoundException ex) {
            String parentName = new File(fileName).getParent();
            if (parentName != null) {
                File parentDir = new File(parentName);
                if (!parentDir.exists() && parentDir.mkdirs()) {
                    ostream = new FileOutputStream(fileName);
                } else {
                    throw ex;
                }
            } else {
                throw ex;
            }
        }

        writer = new OutputStreamWriter(ostream);
    }

    public void closeWriter() {

        try {
            writer.close();
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, null, e);
        }

    }

    public void write(MessageLite message) {
        try {
            final String log = getLogMessage(message);
            writer.write(log);
            writer.flush();
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, null, e);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.ERROR, null, e);
        }
    }

    private String getLogMessage(MessageLite message) {

        // Minimize memory allocations here.
        dat.setTime(System.currentTimeMillis());
        args[0] = dat;
        StringBuffer text = new StringBuffer();
        if (formatter == null) {
            formatter = new MessageFormat(format);
        }
        formatter.format(args, text, null);

        if (message instanceof GeneratedMessage) {
            List<Descriptors.FieldDescriptor> fields = getMessageFields(message);
            Map<Descriptors.FieldDescriptor, Object> values = ((GeneratedMessage) message).getAllFields();
            for (Descriptors.FieldDescriptor field : fields) {
                text.append(", ");
                String fieldName = field.getName();
                Object value = values.get(field);
                if (value == null) {
                    // Это поле optional с неопределенным значением.
//                    if (field.hasDefaultValue()) {
//                        text.append("(").append(field.getDefaultValue()).append(")");
//                    }
                } else if ("messageType".equals(fieldName)) {
                    // Получи имя типа сообщения по его численному коду.
                    int messageType = (Integer) value;
                    String messageTypeName = DataGeneratorServerMessageType.lookup(messageType).getName();
                    text.append(messageTypeName);
                } else if ("messageTime".equals(fieldName)) {
                    // Получим строковое представление даты.
                    int messageTime = (Integer) value;
                    if (valueConverter == null) {
                        valueConverter = new ValueConverter();
                    }
                    text.append(valueConverter.convertMesasgeTime(messageTime));
                } else {
                    if (value instanceof List) {
                        boolean first = true;
                        for (Object element : ((List) value)) {
                            if (!first) {
                                text.append(", ");
                            }
                            text.append(element);
                            first = false;
                        }
                    } else {
                        text.append(value);
                    }
                }
            }

        } else {
            text.append(" [message is not GeneratedMessage type.]");
        }

        text.append("\n");
        return text.toString();
    }

    /**
     * Возвращает все дескрипторы полей сообщения.
     *
     * @param message сообщение.
     * @return список дескрипторов.
     * @throws IllegalArgumentException в случае ошибки при выполнении операции.
     */
    private List<Descriptors.FieldDescriptor> getMessageFields(MessageLite message) {
        MessageParser.MessageHeader header;
        try {
            header = MessageParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e);
        }

        int code = header.getMessageType();
        DataGeneratorServerMessageType messageType = DataGeneratorServerMessageType.lookup(code);
        if (messageType == null) {
            final String error = new StringBuilder().append("Invalid message type code=").append(code).toString();
            throw new IllegalArgumentException(error);
        }
        Descriptors.Descriptor descriptor = DataGeneratorServerMessageType.getDescriptor(messageType);
        return descriptor.getFields();
    }

}
