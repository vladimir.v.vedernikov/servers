/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 30.08.11
 */
package ru.omnicomm.pegasus.processing.generator;

import com.google.protobuf.MessageLite;

/**
 * Обертка для сообщения и времени, которое необходимо выждать перед отправлением этого сообщения.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class MessageInfo {
    private MessageLite message;
    private int timeout;

    public MessageInfo() {
    }

    public MessageInfo(MessageLite message) {
        this.message = message;
    }

    public MessageInfo(MessageLite message, int timeout) {
        this.message = message;
        this.timeout = timeout;
    }

    public MessageLite getMessage() {
        return message;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setMessage(MessageLite message) {
        this.message = message;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
}
