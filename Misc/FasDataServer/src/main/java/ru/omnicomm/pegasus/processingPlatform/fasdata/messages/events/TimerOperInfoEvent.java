/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.exceptions.NotDecodableDataException;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;


/**
 * Событие "Сбор данных по таймеру"
 * @author Nelly
 */
public class TimerOperInfoEvent extends EventMessage{
    
    private static final Logger logger = LoggerFactory.getLogger();
  
   /**
    * Конструктор
    */
   public TimerOperInfoEvent(FasRegistratorProfile registrator){
       super(MessageType.EV_TMR_OPER_INFO,registrator);
   }
   
   

    @Override
    public List<MessageLite> getMessages() throws NotDecodableDataException{
        List<MessageLite> messages = new ArrayList<MessageLite>();

        List<Integer> sensorValues = new ArrayList<Integer>();

        byte[] body = this.getData();
        
        logger.log(Level.INFO, "TIMER_INFO length: "+body.length);

        short flags = makeShort(body[1], body[0]);
        boolean gpsValid = ((flags >> 2) & 1)>0;
        boolean ignitionKey = (flags & 1)>0;
        double latitude = makeInt(body[8], body[7], body[6], body[5])*0.0000001;
        double longitude = makeInt(body[12], body[11], body[10], body[9])*0.0000001;
        double altitude = makeShort(body[17], body[16]) * 1000.0;
        
        int gpsdata = makeInt((byte)0, body[15], body[14], body[13]);
        int satelilite = (gpsdata >> 19) & 0xF;
        double gpsSpeed = (gpsdata & 0x3FF)*100; //от регистратора приходит в дм/с, соответственно, чтобы получить мм/с умножаем на 100
        double azimuth = (gpsdata >> 10) & 0x1FF;
        
        int pos = 18;
        try{
            for (int i = 0; i < 4; i++) {
//                SensorData sensorData = new SensorData(vehicleID, dateID);
//                llsData.add(sensorData);
//                sensorData.setSensorNumber(i + 1);
//                int temperature = 0;
//                if (i == 3)
//                    temperature = 0xFF & body[pos];
//                else
//                    temperature = body[pos];
//                sensorData.setTemperature(temperature);

                short packedData = makeShort(body[pos + 2], body[pos + 1]);
//                sensorData.setLevelCode(packedData & 0xfff);
//                sensorData.setError(((packedData>>12)&1)>0);
//                sensorData.setExist(((packedData>>13)&1)>0);
//                sensorData.setSensorReadiness(((packedData>>14)&1)>0);

                if(((packedData>>12)&1)==0 && //флаг ошибки
                   ((packedData>>13)&1)>0 // && //флаг присутствия
                  // ((packedData>>14)&1)>0     //флаг готовности
                   ){
                    
                    sensorValues.add(packedData & 0xfff);
                }


                pos += 3;
            }
            
        } catch (Exception ex) {
            throw new NotDecodableDataException("Can't parse data",ex);
        }

        if(this.getFuelSourceId()!=null){
            messages.add(makeGroupRawData(this.getFuelSourceId(),sensorValues));
        }
        if(this.getSpeedSourceId()!=null){
            messages.add(makeMultiplySpeed(this.getSpeedSourceId(),gpsValid,(int)gpsSpeed));
        }
        if(this.getLocationSourceId()!=null){
            messages.add(makeCoordinate(this.getLocationSourceId(),gpsValid,latitude,longitude,altitude,satelilite));
        }
        if(this.getLocationSourceId()!=null){
            messages.add(makeSpeedAndAzimuth(this.getLocationSourceId(),gpsValid,gpsSpeed,azimuth));
        }
        if(this.getAnalogSourceId()!=null){
            messages.add(makeAnalogValueRaw(this.getAnalogSourceId(),gpsValid,(int)gpsSpeed));
        }     
//        if(this.getIgnitionSourceId()!=null){
//            messages.add(makeBinaryState(this.getIgnitionSourceId(),ignitionKey));
//        }
      
//        
//        addToResponse(this.getFuelSourceId(),messages,makeGroupRawData(this.getFuelSourceId(),sensorValues));
//        addToResponse(this.getSpeedSourceId(),messages,makeMultiplySpeed(this.getSpeedSourceId(),gpsValid,(int)gpsSpeed));
//        addToResponse(this.getLocationSourceId(),messages,makeCoordinate(this.getLocationSourceId(),gpsValid,latitude,longitude,altitude,satelilite));
//        addToResponse(this.getLocationSourceId(),messages,makeSpeedAndAzimuth(this.getLocationSourceId(),gpsValid,gpsSpeed,azimuth));
//        addToResponse(this.getAnalogSourceId(),messages,makeAnalogValueRaw(this.getAnalogSourceId(),gpsValid,(int)gpsSpeed));
//        addToResponse(this.getBinarySourceId(),messages,makeBinaryState(this.getBinarySourceId(),ignitionKey));
//        
        return messages;
    }
    
    
    
    
    
    

}
