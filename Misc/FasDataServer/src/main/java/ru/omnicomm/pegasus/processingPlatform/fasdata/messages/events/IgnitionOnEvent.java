/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;


/**
 * Событие включения зажигания
 * @author Nelly
 */
public class IgnitionOnEvent extends EventMessage{
    
    /**
     * Конструктор
     */
    public IgnitionOnEvent(FasRegistratorProfile registrator){
        super(MessageType.EV_IGT_ON,registrator);
    }

    @Override
    public List<MessageLite> getMessages() {
        List<MessageLite> messages  = new ArrayList<MessageLite>();
//        if(this.getIgnitionSourceId()!=null){
//            messages.add(makeBinaryState(this.getIgnitionSourceId(),true));
//        }
        return messages;
    }

}
