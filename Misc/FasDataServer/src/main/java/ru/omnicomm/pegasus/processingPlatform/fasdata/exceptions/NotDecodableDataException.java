/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.exceptions;

/**
 * Исключение, возникающее при декодирование данных, пришедших от регистратора FAS
 * @author alexander
 */
public class NotDecodableDataException extends Exception{

    private static final long serialVersionUID = 3408829734862400178L;

    public NotDecodableDataException(String message) {
        super(message);
    }

    public NotDecodableDataException() {
        super();
    }

    public NotDecodableDataException(Throwable cause) {
        super(cause);
    }

    public NotDecodableDataException(String message, Throwable cause) {
        super(message,cause);
    }





}
