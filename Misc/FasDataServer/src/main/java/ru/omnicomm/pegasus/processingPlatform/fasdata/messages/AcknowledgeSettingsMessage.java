/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages;


/**
 * Сообщение с подтверждением приема настроек
 * @author Nelly
 */
public class AcknowledgeSettingsMessage extends BasicMessage{

    private byte error;
    private byte[] body;

    /**
     * Конструктор
     * @param body тело сообщения
     */
    public AcknowledgeSettingsMessage(byte[] body){
        super(MessageType.CMD_ACKN_SETTINGS);
        this.body = body;
        if (body != null && body.length == 1){
            this.error = body[0];
        } else {
            throw new IllegalArgumentException("Wrong body length for AcknowledgeFirmwareMessage");
        }
    }

    @Override
    public int getLength() {
        return 1;
    }

    /**
     * Возвращает код ошибки
     * @return код ошибки
     */
    public byte getError() {
        return error;
    }

    @Override
    public String toString() {
        return (super.toString() + " [ error=" + this.error + " ] ");
    }

    

    @Override
    public byte[] getData() {
        return this.body;
    }
}
