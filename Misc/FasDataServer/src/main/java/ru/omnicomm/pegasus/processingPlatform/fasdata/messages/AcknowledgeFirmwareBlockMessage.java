/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages;


/**
 * Сообщение с подтверждением блока прошивки
 * @author akarpov
 */
public class AcknowledgeFirmwareBlockMessage extends BasicMessage{

    private int blockIndex;
    private byte[] body;
    
    /**
     * Конструктор
     * @param body тело сообщения
     */
    public AcknowledgeFirmwareBlockMessage(byte[] body){
        super(MessageType.CMD_ACKN_FWBLOCK);
        this.body = body;
        if (body != null && body.length == 2){
            this.blockIndex = (((body[1]& 0xFF) << 8) | (body[0]& 0xFF)) ;
        } else {
            throw new IllegalArgumentException("Wrong body length for AcknowledgeFirmwareBlockMessage");
        }
    }
    
    @Override
    public int getLength() {
        return 2;
    }
    
    /**
     * Возвращает номер блока прошивки
     * @return номер блока прошивки
     */
    public int getBlockIndex(){
        return this.blockIndex;
    }
    
    @Override
    public String toString() {
        return (super.toString()+" [ blockIndex = "+this.blockIndex + " ]");
    }

    @Override
    public byte[] getData() {
        return this.body;
    }

}
