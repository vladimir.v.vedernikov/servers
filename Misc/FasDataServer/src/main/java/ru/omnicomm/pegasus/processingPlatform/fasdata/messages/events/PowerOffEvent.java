/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;


/**
 * Событие выключения питания
 * @author Nelly
 */
public class PowerOffEvent extends EventMessage{

    /**
     * Конструктор
     */
    public PowerOffEvent(FasRegistratorProfile registrator){
        super(MessageType.EV_EXT_PWR_OFF,registrator);
    }

    @Override
    public List<MessageLite> getMessages() {
        return new ArrayList<MessageLite>();
    }

}
