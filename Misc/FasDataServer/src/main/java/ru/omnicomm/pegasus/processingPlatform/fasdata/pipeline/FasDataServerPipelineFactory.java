/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.pipeline;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import static org.jboss.netty.channel.Channels.pipeline;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import ru.omnicomm.pegasus.processingPlatform.fasdata.FasDataServer;
import ru.omnicomm.pegasus.processingPlatform.fasdata.filter.FasDataProtocolDecoder;
import ru.omnicomm.pegasus.processingPlatform.fasdata.filter.FasDataProtocolEncoder;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasDataServerHandler;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Channel Pipeline Factory for the FAS Data Server 
 * @author alexander
 */
public class FasDataServerPipelineFactory implements ChannelPipelineFactory {

    private final Router router;
    private final JmsBridgeService messageBrokerService;
    private final SettingsStorageService settingsStorageService;
    private final BusinessLogicService blService;
    private final FasDataServer parent;

    private ChannelGroup channels;

    /**
     * Конструктор
     * @param router роутер
     * @param settingsStorageService сервис настроек
     */
    public FasDataServerPipelineFactory(Router router, JmsBridgeService messageBrokerService, SettingsStorageService settingsStorageService, 
            BusinessLogicService blService, FasDataServer parent, ChannelGroup channels) {
        this.router = router;
        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.parent = parent;
        this.channels = channels;
        this.blService = blService;
    }

    

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = pipeline();
        // Enable stream compression (you can remove these two if unnecessary)
//        pipeline.addLast("deflater", new ZlibEncoder(ZlibWrapper.GZIP));
//        pipeline.addLast("inflater", new ZlibDecoder(ZlibWrapper.GZIP));

        // Add the number codec first,
        pipeline.addLast("decoder", new FasDataProtocolDecoder());
        pipeline.addLast("encoder", new FasDataProtocolEncoder());

        // and then business logic.
        // Please note we create a handler for every new channel
        // because it has stateful properties.
        HashedWheelTimer timer = new HashedWheelTimer();
        pipeline.addLast("idleHandler", new IdleStateHandler(timer, 60, 60, 0));
        pipeline.addLast("handler", new FasDataServerHandler(router, messageBrokerService, settingsStorageService, blService, parent, channels));

        return pipeline;

    }

  }
