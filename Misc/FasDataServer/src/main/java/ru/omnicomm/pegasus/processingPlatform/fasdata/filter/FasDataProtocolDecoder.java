/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.filter;

import java.util.ArrayList;
import java.util.List;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.CorruptedFrameException;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.AcknowledgeFirmwareBlockMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.AcknowledgeFirmwareMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.AcknowledgeSettingsMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.AcknowledgeStartFirmwareUpdating;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.BasicMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.DataClearedMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.HelloMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageBuffer;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.SendDataMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.SendSettingsMessage;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

/**
 * Декодер протокола регистратора FAS
 * @author alexander
 */
public class FasDataProtocolDecoder extends FrameDecoder {

    private static final Logger logger = LoggerFactory.getLogger();

    @Override
    protected List<BasicMessage> decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {

        MessageBuffer messageBuffer = this.getMessageBuffer(ctx);
        List<BasicMessage> res = new ArrayList<BasicMessage>();

        while(buffer.readable()){
            byte b = buffer.readByte();

            messageBuffer.next(b);
            if (messageBuffer.getStatus() == MessageBuffer.Status.READY){
                BasicMessage message = createMessage(messageBuffer);
                if(message==null){
                    throw new CorruptedFrameException("Wrong message");
                }
                res.add(message);
                messageBuffer.reset();
                
            } else if (messageBuffer.getStatus() == MessageBuffer.Status.FAILED) {
                messageBuffer.reset();
                throw new CorruptedFrameException("Wrong byte sequence");
            }

        }

        return res;
    }

        private BasicMessage createMessage(MessageBuffer messageBuffer) {
        int messageType = messageBuffer.getCommand();
        MessageType cmd = MessageType.lookup(messageType);
        if (cmd == null) {
            logger.log(Level.INFO, new StringBuilder().append("Unknown command received ").append(messageType).toString());
            return null;
        }
        final byte[] body = messageBuffer.getBody();
       // int length = body != null ? body.length : 0;
      //  logger.info("Message received: command " + cmd.getDescription() + ", body length = " + length+", deviceId = "+(Integer)ioSession.getAttribute(DEVICE_ID));
        try{
            switch (cmd) {
                case CMD_HELLO:
                    return new HelloMessage(body);
                case CMD_SEND_DATA:
                    return new SendDataMessage(body);
                case CMD_DATA_CLEARED:
                    return new DataClearedMessage(body);
                case CMD_ACKN_FWBLOCK:
                    AcknowledgeFirmwareBlockMessage acknowledgeFirmwareBlockMessage = new AcknowledgeFirmwareBlockMessage(body);
                   // logger.info("FIRMWARE BLOCK ACKNOWLEDGED: blockIndex="+acknowledgeFirmwareBlockMessage+", deviceId = "+(Integer)ioSession.getAttribute(DEVICE_ID));
                    return acknowledgeFirmwareBlockMessage;
                case CMD_ACKN_FW:
                    return new AcknowledgeFirmwareMessage(body);
                case CMD_SEND_SETTINGS:
                    return new SendSettingsMessage(body);
                case CMD_ACKN_SETTINGS:
                    return new AcknowledgeSettingsMessage(body);
                case CMD_ACKN_START_FW_UPDTG:
                    return new AcknowledgeStartFirmwareUpdating(body);
                default:
                    logger.log(Level.INFO, new StringBuilder().append("Unknown or unacceptable type of message : ").append(messageType).toString());
                    return null;
            }
        } catch (IllegalArgumentException ex){
            logger.log(Level.INFO, "Can't parse the message",ex);
            return null;
        }

    }

    private MessageBuffer getMessageBuffer(ChannelHandlerContext ctx){

        if(ctx.getAttachment()==null){
            ctx.setAttachment(new MessageBuffer());
        }

        return (MessageBuffer)ctx.getAttachment();

    }


}
