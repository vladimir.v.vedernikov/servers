/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;

/**
 * Событие "Ошибка"
 * @author akarpov
 */
public class ErrorEvent extends EventMessage{


    /**
     * Конструктор
     */
    public ErrorEvent(FasRegistratorProfile registrator) {
        super(MessageType.EV_ERROR,registrator);
    }

    @Override
    public int getLength() {
        return 9;
    }

    @Override
    public List<MessageLite> getMessages() {
        return new ArrayList<MessageLite>();
    }

}
