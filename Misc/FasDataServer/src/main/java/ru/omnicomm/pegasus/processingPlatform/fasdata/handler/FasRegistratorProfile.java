/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.handler;

import com.google.protobuf.MessageLite;
import java.util.HashMap;
import ru.omnicomm.pegasus.messaging.settings.fas.RegistratorProfileParser.RegistratorProfile;

/**
 * Профиль регистратора FAS
 * @author alexander
 */
public class FasRegistratorProfile {

    private final int sourceId;
    private long lastDataId;
    
    private final HashMap<String, Integer> sourceIds = new HashMap<String, Integer>();

    /**
     * Конструктор
     * @param profile профиль в формате MessageLite
     */
    public FasRegistratorProfile(RegistratorProfile profile) {
        this.sourceId = profile.getSourceId();
        if(profile.hasLastDataId()){
            this.lastDataId = profile.getLastDataId() & 0xFFFFFFFFL;
        } else {
            this.lastDataId = 0L;
        }
    }
    
    public void putSourceId(String key, int value){       
        this.sourceIds.put(key, value);
    }
    
    public Integer getSourceId(String key){
        return this.sourceIds.get(key);
    }

    /**
     * Возвращает профиль регистратора FAS в формате MessageLite
     * @return
     */
    public MessageLite getMessageLite(){

        return RegistratorProfile.newBuilder().setSourceId(this.sourceId).
                setMessageType(RegistratorProfile.getDefaultInstance().getMessageType()).
                setMessageTime(0).setLastDataId((int)this.lastDataId)
                .build();

    }

    /**
     * Возвращает id источника данных (id регистратора FAS)
     * @return id источника данных (id регистратора FAS)
     */
    public int getSourceId() {
        return sourceId;
    }
 
    /**
     * Возвращает id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     * @return id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     */
    public long getLastDataId() {
        return lastDataId;
    }

    /**
     * Устанавливает id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     * @param lastDataId id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     */
    public void setLastDataId(long lastDataId) {
        this.lastDataId = lastDataId;
    }

    @Override
    public String toString() {
        return "RegistratorProfile{" + "sourceId=" + sourceId + "; lastDataId=" + lastDataId + '}';
    }

    

    



}
