/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;


/**
 * Событие включения зажигания
 * @author Nelly
 */
public class PowerOnEvent extends EventMessage{

    /**
     * Конструктор
     */
    public PowerOnEvent(FasRegistratorProfile registrator){
        super(MessageType.EV_PWR_ON,registrator);
    }

    @Override
    public List<MessageLite> getMessages() {
        return new ArrayList<MessageLite>();
    }
}
