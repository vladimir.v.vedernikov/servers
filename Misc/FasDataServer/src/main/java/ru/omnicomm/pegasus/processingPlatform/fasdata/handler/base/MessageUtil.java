/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 28.09.12
 */
package ru.omnicomm.pegasus.processingPlatform.fasdata.handler.base;

import com.google.protobuf.ByteString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.CreateRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.CreateTemporaryRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.UpdateRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ReadRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.DeleteRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.MUIString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;

import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class MessageUtil {

    public static CreateRequest buildCreateRequest(long entityType) {
        return CreateRequest.newBuilder()
                .setMessageType(CreateRequest.getDefaultInstance().getMessageType())
                .setEntityType(entityType).build();
    }

    public static CreateTemporaryRequest buildCreateTemporaryRequest(long entityType) {
        return CreateTemporaryRequest.newBuilder()
                .setMessageType(CreateTemporaryRequest.getDefaultInstance().getMessageType())
                .setEntityType(entityType).build();
    }

    public static UpdateRequest buildUpdateRequest(long entityType, long entityId, long parameterId, ParameterValue value) {
        return UpdateRequest.newBuilder()
                .setMessageType(UpdateRequest.getDefaultInstance().getMessageType())
                .setEntityType(entityType)
                .setEntityId(entityId)
                .setParameterId(parameterId)
                .setValue(value)
                .build();
    }

    public static ReadRequest buildReadRequest(long entityType, long entityId, long parameterId) {
        return buildReadRequest(entityType, entityId, parameterId, null, null);
    }

    public static ReadRequest buildReadRequest(long entityType, long entityId, long parameterId, Integer version, Integer timestamp) {
        ReadRequest.Builder builder = ReadRequest.newBuilder()
                .setMessageType(ReadRequest.getDefaultInstance().getMessageType())
                .setEntityType(entityType)
                .setEntityId(entityId)
                .setParameterId(parameterId);

        if (version != null) {
            builder.setVersion(version);
        }

        if (timestamp != null) {
            builder.setTimestamp(timestamp);
        }

        return builder.build();
    }

    public static DeleteRequest buildDeleteRequest(long entityType, long entityId) {
        return DeleteRequest.newBuilder()
                .setMessageType(DeleteRequest.getDefaultInstance().getMessageType())
                .setEntityType(entityType)
                .setEntityId(entityId).build();
    }


    public static SelectRequest buildSelectRequest(long entityType, List<QueryItem> filtes) {
        return buildSelectRequest(null, entityType, filtes, null, null);
    }

    public static SelectRequest buildSelectRequest(int locale, long entityType, List<QueryItem> filtes) {
        return buildSelectRequest(locale, entityType, filtes, null, null);
    }

    public static SelectRequest buildSelectRequest(Integer locale, long entityType, List<QueryItem> filtes, Integer startTime, Integer endTime) {
        SelectRequest.Builder builder = SelectRequest.newBuilder();

        builder.setMessageType(SelectRequest.getDefaultInstance().getMessageType())
                .setEntityType(entityType).addAllFilter(filtes);

        if (locale != null) {
            builder.setLocale(locale);
        }

        if (startTime != null) {
            builder.setStartTime(startTime);
        }

        if (endTime != null) {
            builder.setEndTime(endTime);
        }

        return builder.build();
    }

    public static QueryItem buildQueryItem(long attributeId, Operation op, ParameterValue value) {
        return QueryItem.newBuilder().setAttributeId(attributeId).setOperation(op).setValue(value).build();
    }

    public static ParameterValue buildParameterValue(ParameterValueType type, Object value) {
        ParameterValue.Builder builder = ParameterValue.newBuilder();

        switch (type) {
            case STRING:
                builder.setStringValue((String) value);
                break;
            case NUMBER:
                builder.setDoubleValue((Double) value);
                break;
            case DATETIME:
                builder.setDatetimeValue((Long) value);
                break;
            case MUISTRING:
                builder.setMuistringValue((MUIString) value);
                break;
            case OBJECT_LINK:
                builder.setObjectLinkValue((ObjectLink) value);
                break;
            case FILE:
                builder.setFileValue((ByteString) value);
                break;
            case INTEGER:
                builder.setIntValue((Integer) value);
                break;
            case LONG:
                builder.setLongValue((Long) value);
                break;
            case BOOLEAN:
                builder.setBooleanValue((Boolean) value);
                break;
            case ARRAY:
                builder.setValueArray((ValueArray) value);
                break;
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }

        return builder.build();
    }

    public static ValueArray buildValueArray(List<ParameterValue> values) {
        return ValueArray.newBuilder()
                .addAllValues(values).build();
    }

    public static ByteString buildByteString(byte[] bytes) {
        return ByteString.copyFrom(bytes);
    }

    public static ObjectLink buildObjectLink(List<Long> links) {
        return ObjectLink.newBuilder()
                .addAllLinks(links).build();
    }

    public static MUIString buildMUIString(Map<Integer, String> muiValues) {
        MUIString.Builder builder = MUIString.newBuilder();

        for (Integer langId : muiValues.keySet()) {
            String muiString = muiValues.get(langId);
            MUIString.MUIValue muiValue = MUIString.MUIValue.newBuilder().setLangID(langId).setValue(muiString).build();
            builder.addStrValues(muiValue);
        }

        return builder.build();
    }
}
