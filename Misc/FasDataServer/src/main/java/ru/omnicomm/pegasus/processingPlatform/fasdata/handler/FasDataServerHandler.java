/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.handler;

import com.google.protobuf.MessageLite;
import java.util.List;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import ru.omnicomm.pegasus.messaging.settings.fas.RegistratorProfileParser.RegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.FasDataServer;
import ru.omnicomm.pegasus.processingPlatform.fasdata.exceptions.NotDecodableDataException;
import ru.omnicomm.pegasus.processingPlatform.fasdata.exceptions.ProfileLoadingException;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.base.ControlEntityHelper;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.*;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events.*;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Обработчик сообщений регистратора FAS
 * @author alexander
 */
public class FasDataServerHandler extends IdleStateAwareChannelHandler{

    private static final Logger logger = LoggerFactory.getLogger();

    private final Router router;
    private final JmsBridgeService messageBrokerService;
    private final SettingsStorageService settingsStorageService;
    private final FasDataServer parent;

    private final ChannelGroup channels;
    
    private ControlEntityHelper baseHelper;


    /**
     * Конструктор
     * @param router роутер
     * @param settingsStorageService сервис настроек
     * @param channels
     */
    public FasDataServerHandler(Router router, JmsBridgeService messageBrokerService, SettingsStorageService settingsStorageService, BusinessLogicService blService,FasDataServer parent, ChannelGroup channels) {
        super();
        this.router = router;
        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.parent = parent;
        this.channels = channels;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {

        if (e instanceof ChannelStateEvent) {
            logger.info(e.toString());
        }
        super.handleUpstream(ctx, e);

    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {

        List<BasicMessage> messages = (List<BasicMessage>) e.getMessage();


        for(BasicMessage message : messages){
            this.processMessage(ctx, message);
            
        }

    }

    private void processMessage(final ChannelHandlerContext ctx, BasicMessage message){
        
        logger.info("MESSAGE RECEIVED: "+message);
        
        
        if(!this.parent.isActive()){
            ctx.getChannel().close();
        }

        if(ctx.getAttachment()==null && message.getCommand()!=MessageType.CMD_HELLO){
            ctx.getChannel().close();
            return;
        }

        final FasRegistratorProfile profile = (FasRegistratorProfile)ctx.getAttachment();
        
        
        logger.info("COMMAND: "+message.getCommand());
        switch(message.getCommand()){

            case CMD_HELLO:          
                final FasRegistratorProfile p = this.getRegistratorProfile(((HelloMessage)message).getVehicleID());
                
                logger.log(Level.INFO, "FAS Registrator with id "+p.getSourceId()+" has been connected");
                
                addSourceIdToProfile(p,"Speed");
                addSourceIdToProfile(p,"Fuel");
                addSourceIdToProfile(p,"Coordinates");
                addSourceIdToProfile(p,"AnalogSensor");
                addSourceIdToProfile(p,"BinarySensor");
                addSourceIdToProfile(p,"Ignition");
                
                ctx.setAttachment( p);
                if(p.getLastDataId()>0){
                    logger.log(Level.INFO,"CLEAR DATA TILL: "+p.getLastDataId());
                    ctx.getChannel().write(new ClearDataMessage(p.getLastDataId())).addListener(new ChannelFutureListener() {
                       
                        
                        @Override
                        public void operationComplete(ChannelFuture future) throws Exception {
                            p.setLastDataId(0L);
                            ctx.getChannel().write(new GetDataMessage());
                        }
                    });
                } else {
                    ctx.getChannel().write(new GetDataMessage());
                }
                
                break;

            case CMD_SEND_DATA:
                SendDataMessage dataMessage = (SendDataMessage)message;
                int length = dataMessage.getLength();
                long lastDataId = profile.getLastDataId();
                if (length == 0){
                    logger.log(Level.INFO,"EMPTY DATA CONTAINER");
                    //Empty data container
                    if(lastDataId>=0){
                        logger.log(Level.INFO,"CLEAR DATA TILL: "+lastDataId);
                        ctx.getChannel().write(new ClearDataMessage(lastDataId)).addListener(new ChannelFutureListener() {

                            @Override
                            public void operationComplete(ChannelFuture future) throws Exception {
                                profile.setLastDataId(0L);
                                future.getChannel().close();
                            }

                        });

                    } else {
                        ctx.getChannel().close();
                    }


                    break;
                }


                byte[] body = dataMessage.getBody();
                int pos = 0;
                EventMessage event = null;

                while (pos < length) {

                    try{
                        event = parseEvent(body, pos, profile);
                        logger.log(Level.INFO,"EVENT: "+event);
                        List<MessageLite> msgs = event.getMessages();

                        for(MessageLite messageLite : msgs){
                            logger.log(Level.INFO,"MESSAGE: "+messageLite.toString().replaceAll("\n", " "));
                            messageBrokerService.send(messageLite);
                            router.send(messageLite);
                        }

                        

                    } catch (IllegalArgumentException e){
                        logger.log(Level.INFO, "Archive error", e);
                        profile.setLastDataId(0xFFFFFFFFl);
                        throw e;
                    } catch (NotDecodableDataException e){
                        logger.log(Level.INFO, "Can't parse message container", e);
                    } catch (JmsBridgeServiceException e){
                        logger.log(Level.INFO, "Can't send message to the message broker", e);
                    }

                    long tmpDataId = event.getFasFormatDataId();
                    pos += event.getLength();

                    if(tmpDataId>lastDataId){
                        lastDataId  = tmpDataId;
                        profile.setLastDataId(lastDataId);
                    }

                }

                
                break; //from case CMD_SEND_DATA

        }



    }
    
    private void addSourceIdToProfile(FasRegistratorProfile profile, String key){
        Integer sourceId = null;
        try{
            sourceId = baseHelper.selectFirstSoureceId(key, profile.getSourceId());
        } catch (Exception ex) {
            logger.log(Level.WARN, ex.getMessage());
        }
                
        if(sourceId!=null){
            profile.putSourceId(key, sourceId);
        } else {
            logger.log(Level.INFO, "sourceId of '"+key+"' wasn't found for FAS Registrator "+profile.getSourceId());
        }
    }

    private FasRegistratorProfile getRegistratorProfile(int sourceId) throws ProfileLoadingException{
        MessageLite profileObject = null;
        RegistratorProfile profileSettings = null;
        
        try{
            profileObject = this.settingsStorageService.lookup(getProfileSettingsName(sourceId));
          //  profileSettings = (RegistratorProfile)this.settingsStorageService.lookup(getProfileSettingsName(sourceId));
        } catch (Exception ex) {
            throw new ProfileLoadingException(String.format("Can't load FAS Registrator profile %d", sourceId),ex);
        }
        
        if(profileObject!=null){
            try{
                profileSettings = RegistratorProfile.parseFrom(profileObject.toByteString());
            } catch (Exception ex) {
                logger.log(Level.INFO, String.format("Can't parse FAS Registrator profile %d", sourceId),ex);
            }
        }

        if(profileSettings == null) {
            
            profileSettings = RegistratorProfile.newBuilder().setSourceId(sourceId).setMessageType(RegistratorProfile.getDefaultInstance().getMessageType()).setMessageTime(0).setLastDataId(0).build();
            
           // throw new ProfileLoadingException(String.format("Profile of the FAS Registrator %d wasn't found", sourceId));
        } 
        return new FasRegistratorProfile(profileSettings);
    }

    private String getProfileSettingsName(int sourceId){
        return String.format("src%d:profile", sourceId);
    }

    private EventMessage parseEvent(byte[] body, int pos, FasRegistratorProfile registrator) {


        if(body.length<5) {
            throw new IllegalArgumentException("Too short event body length: "+body.length);
        }

        int type = body[pos];//[pos+4];
        logger.log(Level.INFO, "EVENT TYPE CODE: "+type);
        MessageType eventType = MessageType.lookup(type);
        logger.log(Level.INFO, "EVENT TYPE: "+eventType);
        if (eventType == null) {
            throw new IllegalArgumentException("Unknown event type received 0x" + Integer.toHexString(type).toUpperCase());
        }


        EventMessage event;

        switch (eventType) {
            case EV_TMR_OPER_INFO:
                event = new TimerOperInfoEvent(registrator);
                break;
            case EV_IGT_ON:
                event = new IgnitionOnEvent(registrator);
                break;
            case EV_IGT_OFF:
                event = new IgnitionOffEvent(registrator);
                break;
            case EV_PWR_ON:
                event = new PowerOnEvent(registrator);
                break;
            case EV_EXT_PWR_OFF:
                event = new PowerOffEvent(registrator);
                break;
            case EV_DRIVER_REG:
                event = new DriverRegEvent(registrator);
                break;
            case EV_INCOMING_CALL:
                event = new IncomingCallEvent(registrator);
                break;
            case EV_SETTINGS:
                event = new SettingsEvent(registrator);
                break;
            case EV_ACCESS_POINT:
                event = new AccessPointEvent(registrator);
                break;
            case EV_ERROR:
                event = new ErrorEvent(registrator);
                break;
            case EV_ALARM:
                event = new AlarmEvent(registrator);
                break;
            default:
                throw new IllegalArgumentException("Unknown event type " + type);
        }

        int length = event.getLength();
        byte[] eventBody = new byte[length];
        System.arraycopy(body, pos, eventBody, 0, length);



        event.parseBody(eventBody);
        return event;
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        if(!this.parent.isActive()){
            ctx.getChannel().close();
        } else {
            channels.add(e.getChannel());
        }
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        logger.log(Level.INFO, new StringBuilder().append("Session closed: ").append(ctx.getAttachment()).toString());

        if(ctx.getAttachment() != null){
            FasRegistratorProfile profile = (FasRegistratorProfile)ctx.getAttachment();
            this.settingsStorageService.bind(profile.getMessageLite(), getProfileSettingsName(profile.getSourceId()));
        }

    }

    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) throws Exception {
        
        if(e.getState() == IdleState.READER_IDLE){
            logger.log(Level.WARN, "Channel is closing because of idle. IDLE State is " + e.getState());
            e.getChannel().close();
        } else {
            logger.log(Level.INFO, "IDLE State is " + e.getState());
        }
    }



    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        logger.log(Level.WARN, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        super.channelClosed(ctx, e);
//
//        Channel closedChannel = e.getChannel();
//        ChannelPipeline pipeline = closedChannel.getPipeline();
//        ChannelHandler channelHandler = pipeline.get("idleHandler");
//        if (channelHandler instanceof ExternalResourceReleasable) {            
//            ((ExternalResourceReleasable) channelHandler).releaseExternalResources();
//        }
    }
}
