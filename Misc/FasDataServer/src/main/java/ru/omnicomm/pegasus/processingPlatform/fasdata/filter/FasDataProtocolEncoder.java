/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.filter;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.BasicMessage;
import static ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageBuffer.*;

/**
 * Кодер протокола регистратора FAS
 * @author alexander
 */
public class FasDataProtocolEncoder extends OneToOneEncoder{


    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {

        if (!(message instanceof BasicMessage)) {
            throw new IllegalArgumentException("Wrong message");
        }

        ChannelBuffer buffer = ChannelBuffers.dynamicBuffer();
        
        byte[] tmpArray = new byte[257]; // body length 0 ... 255, 1 - command, 1 length
        int ind = 0;
        int length = ((BasicMessage) message).getLength();

        buffer.writeByte(SYN);

        byte cmd = (byte)((BasicMessage) message).getCommand().getCode();
        buffer.writeByte(cmd);
        tmpArray[ind++] = cmd;

        tmpArray[ind++] = (byte)length;
        if (length == SYN) {
            buffer.writeByte(ESC);
            buffer.writeByte(ESC_SYN);
        } else if (length == ESC) {
            buffer.writeByte(ESC);
            buffer.writeByte(ESC_ESC);
        } else {
            buffer.writeByte((byte)length);
        }

        if (length > 0) {
          //  ByteBuffer tmpbuf = ByteBuffer.allocate(length);
            byte[] tmpbuf = ((BasicMessage) message).write();

            for(byte b : tmpbuf){

                tmpArray[ind++] = b;

                if (b == SYN) {
                    buffer.writeByte(ESC);
                    buffer.writeByte(ESC_SYN);
                } else if (b == ESC) {
                    buffer.writeByte(ESC);
                    buffer.writeByte(ESC_ESC);
                } else {
                    buffer.writeByte(b);
                }

            }

        }

        byte[] crcArray = new byte[ind];
        System.arraycopy(tmpArray, 0, crcArray, 0, ind);

        short crc = getCrc(crcArray);

        for(int i=8; i>=0; i-=8){
            byte b;
            b = (byte)((crc >> i) & 0xff);

            if (b == SYN) {
                buffer.writeByte(ESC);
                buffer.writeByte(ESC_SYN);
            } else if (b == ESC) {
                buffer.writeByte(ESC);
                buffer.writeByte(ESC_ESC);
            } else {
                buffer.writeByte((byte)b);
            }
        }

        return buffer;
    }


    private short getCrc(byte[] array) {
        int crc = 0xFFFF;
        int length = array.length;
        int arrayIndex = 0;
        while (length-- > 0) {
            crc ^= array[arrayIndex++] << 8;
            for (int forIndex = 0; forIndex < 8; forIndex++) {
                crc = (crc & 0x8000) != 0 ? (crc << 1) ^ 0x1021 : crc << 1;
            }
        }
        return (short)crc;
    }

}
