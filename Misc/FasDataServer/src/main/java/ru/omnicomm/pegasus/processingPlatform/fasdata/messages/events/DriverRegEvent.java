/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;

/**
 * Событие регистрации водителя
 * @author Nelly
 */
public class DriverRegEvent extends EventMessage{

    /**
     * Конструктор
     */
    public DriverRegEvent(FasRegistratorProfile registrator){
        super(MessageType.EV_DRIVER_REG, registrator);
    }
    
    @Override
    public int getLength() {
        return super.getLength()+6;
    }

    @Override
    public List<MessageLite> getMessages() {
        return new ArrayList<MessageLite>();
    }
}
