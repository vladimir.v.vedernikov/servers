/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages;

/**
 *
 * @author alexander
 */
public class GetDataMessage extends BasicMessage{

    /**
     * Конструктор
     */
    public GetDataMessage(){
        super(MessageType.CMD_GET_DATA);
    }

    @Override
    public int getLength(){
        return 4;
    }



    @Override
    public byte[] write() {
        byte[] bytes = {(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};

        return bytes;
    }

    @Override
    public byte[] getData() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
