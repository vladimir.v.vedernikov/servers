/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processingPlatform.fasdata.handler.base;

/**
 *
 * @author alexander
 */
import com.google.protobuf.InvalidProtocolBufferException;

import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;

import java.util.*;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class ControlEntityHelper {

    public static final long CONTROLLED_UNIT_TYPE_ENTITY_TYPE = 1240;

    public static final long CONTROLLED_UNIT_TYPE_NAME_PARAMETER_ID = 1241;

    public static final long CONTROLLED_UNIT_TYPE_DISPLAY_NAME_PARAMETER_ID = 1242;


    public static final long CONTROLLED_UNIT_ENTITY_TYPE = 1250;

    public static final long CONTROLLED_UNIT_CUT_PARAMETER_ID = 1251;

    public static final long CONTROLLED_UNIT_SOURCE_PARAMETER_ID = 1252;

    public static final long CONTROLLED_UNIT_CO_PARAMETER_ID = 1253;


    public static final long SOURCE_ID_ENTITY_TYPE = 1300;

    public static final long SOURCE_ID_VALUE_PARAMETER_ID = 1301;


    private BusinessLogicService blService;

    public ControlEntityHelper(BusinessLogicService blService) {
        this.blService = blService;
    }

    /**
     * Осуществляет поиск {@code источников данных (sourceId)} по переданным параметрам {@code cutName}
     * и {@code coId} и возвращает первый найденный или {@code null}.
     *
     * @param cutName внутреннее имя агрегата.
     * @param coId    идентификатор контролируемого объекта.
     * @return первый найденный по параметрам {@code sourceId} или {@code null} если ничего не найдено.
     * @throws RequestProcessingException при ошибках во время выполнения запросов к {@code BasePlatform}.
     */
    public Integer selectFirstSoureceId(String cutName, long coId) {
        Collection<Integer> sources = selectSources(cutName, coId);
        if (sources.isEmpty()) {
            return null;
        } else {
            return sources.iterator().next();
        }
    }

    /**
     * Осуществляет поиск {@code источников данных (sourceId)} по переданным параметрам {@code cutName}
     * и {@code coId}.
     *
     * @param cutName внутреннее имя агрегата.
     * @param coId    идентификатор контролируемого объекта.
     * @return список найденных по параметрам {@code sourceId}. Никогда не возвращает {@code null}.
     * @throws RequestProcessingException при ошибках во время выполнения запросов к {@code BasePlatform}.
     */
    public Collection<Integer> selectSources(String cutName, long coId) {
        Long cutId = selectCUTIdByName(cutName);
        if (cutId == null) {
            throw new FasDataServerException("Cannot find 'ControlledUnitType' with name cutName");
        }

        Set<Long> sourceIdsByCU = selectSourceIdsByCU(coId, cutId);

        //todo реализовать фильтрацию sourceId по ControlledParameter

        return selectSourcesByIdentifiers(sourceIdsByCU);
    }

    /**
     * Выполняет запрос по объектам сущности {@code 'SourceId'} у которых {@code entityId} присутствует в списке
     * идентификаторов {@code sourceIdsByCU}.
     *
     * @param sourceIdsByCU список идентификаторов сущности {@code 'SourceId'}.
     * @return список {@code 'sourceId'}.
     */
    public Set<Integer> selectSourcesByIdentifiers(Set<Long> sourceIdsByCU) {
        Set<Integer> sources = new HashSet<Integer>(sourceIdsByCU.size());
        if (sourceIdsByCU.isEmpty()) {
            return sources;
        }

        List<ParameterValue> sourcesIdList = new ArrayList<ParameterValue>();
        for (Long sourceId : sourceIdsByCU) {
            ParameterValue sourceIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, sourceId);
            sourcesIdList.add(sourceIdValue);
        }
        ValueArray valueArray = MessageUtil.buildValueArray(sourcesIdList);
        ParameterValue filterValue = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, valueArray);

        QueryItem sourceIdItem = MessageUtil.buildQueryItem(BusinessLogicService.ENTITY_ID_ATTRIBUTE, Operation.IN, filterValue);
        SelectRequest sourcesQuery = MessageUtil.buildSelectRequest(SOURCE_ID_ENTITY_TYPE, Arrays.asList(sourceIdItem));
        try {
            byte[] resultBytes = blService.selectByParams(sourcesQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();

            for (Parameter parameter : parameters) {
                if (SOURCE_ID_VALUE_PARAMETER_ID == parameter.getAttributeId()) {
                    Long source = parameter.getValue().getLongValue();
                    sources.add(source.intValue());
                }
            }
        } catch (BusinessLogicServiceException e) {
            throw new FasDataServerException("Cannot execute 'selectSourcesByIdentifiers' query", e);
        } catch (InvalidProtocolBufferException e) {
            throw new FasDataServerException("Cannot parse query result", e);
        }

        return sources;
    }

    public Long selectCUTIdByName(String cutName) {
        if (cutName == null) {
            throw new NullPointerException("Parameter cutName is null");
        }

        final ParameterValue cutNameValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, cutName);
        final QueryItem cutNameItem = MessageUtil.buildQueryItem(CONTROLLED_UNIT_TYPE_NAME_PARAMETER_ID, Operation.EQ, cutNameValue);
        final SelectRequest cutQuery = MessageUtil.buildSelectRequest(CONTROLLED_UNIT_TYPE_ENTITY_TYPE, Arrays.asList(cutNameItem));

        try {
            byte[] resultBytes = blService.selectByParams(cutQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();
            if (parameters.isEmpty()) {
                return null;
            } else {
                return parameters.get(0).getEntityId();
            }
        } catch (BusinessLogicServiceException e) {
            throw new FasDataServerException("Cannot execute 'controlled unit type by name' query", e);
        } catch (InvalidProtocolBufferException e) {
            throw new FasDataServerException("Cannot parse query result", e);
        }
    }

    public Set<Long> selectSourceIdsByCU(long coId, long cutId) {
        final ParameterValue coIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, coId);
        final QueryItem coBindItem = MessageUtil.buildQueryItem(CONTROLLED_UNIT_CO_PARAMETER_ID, Operation.EQ, coIdValue);

        final ParameterValue cutIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, cutId);
        final QueryItem cutBindItem = MessageUtil.buildQueryItem(CONTROLLED_UNIT_CUT_PARAMETER_ID, Operation.EQ, cutIdValue);

        final SelectRequest cutQuery = MessageUtil.buildSelectRequest(CONTROLLED_UNIT_ENTITY_TYPE, Arrays.asList(coBindItem, cutBindItem));

        Set<Long> result = new HashSet<Long>();
        try {
            byte[] resultBytes = blService.selectByParams(cutQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();
            Map<Long, Map<Long, List<Parameter>>> cuEntities = groupParameters(parameters);
            for (Long cuId : cuEntities.keySet()) {
                Map<Long, List<Parameter>> attributes = cuEntities.get(cuId);
                List<Parameter> values = attributes.get(CONTROLLED_UNIT_SOURCE_PARAMETER_ID);
                for (Parameter value : values) {
                    final ParameterValue parameterValue = value.getValue();
                    try {
                        ObjectLink links = parameterValue.getObjectLinkValue();
                        for (Long sourceId : links.getLinksList()) {
                            result.add(sourceId);
                        }
                    } catch (ClassCastException e) {
                        final String errMsg = "Controlled unit bind source parameter must be type of ObjectLink but " + parameterValue;
                        throw new FasDataServerException(errMsg, e);
                    }
                }

            }

        } catch (BusinessLogicServiceException e) {
            throw new FasDataServerException("Cannot execute 'controlled unit type by name' query", e);
        } catch (InvalidProtocolBufferException e) {
            throw new FasDataServerException("Cannot parse query result", e);
        }

        return result;
    }
    
    public static Map<Long, Map<Long, List<Parameter>>> groupParameters(List<Parameter> parameters) {
        Map<Long, Map<Long, List<Parameter>>> result = new HashMap<Long, Map<Long, List<Parameter>>>();

        for (Parameter parameter : parameters) {
            long entityId = parameter.getEntityId();
            long attributeId = parameter.getAttributeId();

            // Коллекция отношений идентификаторов атрибутов к списку самих атрибутов.
            // Может быть несколько атрибутов с одинаковым attrId т.к. в ответе на запрос могут передаваться
            // исторические данные.
            Map<Long, List<Parameter>> entity = result.get(entityId);
            if (entity == null) {
                entity = new HashMap<Long, List<Parameter>>();
                result.put(entityId, entity);
            }

            // Список параметров.
            List<Parameter> sameAttrIdParams = entity.get(attributeId);
            if (sameAttrIdParams == null) {
                sameAttrIdParams = new ArrayList<Parameter>();
                entity.put(attributeId, sameAttrIdParams);
            }

            sameAttrIdParams.add(parameter);
        }

        return result;
    }
}
