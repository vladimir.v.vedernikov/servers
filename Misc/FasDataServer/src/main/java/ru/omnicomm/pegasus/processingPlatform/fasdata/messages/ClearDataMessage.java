/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages;

import java.nio.ByteBuffer;


/**
 * Запрос на стирание данных с регистратора
 * @author Nelly
 */
public class ClearDataMessage extends BasicMessage{

    private long dataID;

    /**
     * Конструктор
     * @param value значение id сообщения, до которого включительно должны
     * быть удалены данные из архива регистратора 
     */
    public ClearDataMessage(long value){
        super(MessageType.CMD_CLEAR_DATA);
        this.dataID = value;
    }

    @Override
    public int getLength(){
        return 4;
    }

    /**
     * Возвращает id сообщения, до которого включительно должны
     * быть удалены данные из архива регистратора
     * @return id сообщения
     */
    public long getDataID(){
        return this.dataID;
    }

    @Override
    public byte[] write() {

        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(Integer.reverseBytes((int)this.dataID));
        bb.flip();

//        byte[] bytes = new byte[4];
//
//        int j = 0;
//        for (int i = 0; i < bytes.length; i++) {
//            long b = (dataID >> j) & 0xFF;
//            bytes[i] = (byte) b;
//            j+=8;
//
//        }

        return bb.array();

    }



//    @Override
//    public void write(ByteBuffer buf){
//
//        long b;
//
//        b = dataID & 0xFF;
//        buf.put((byte)b);
//
//        b = (dataID >> 8) & 0xFF;
//        buf.put((byte)b);
//
//        b = (dataID >> 16) & 0xFF;
//        buf.put((byte)b);
//
//        b = (dataID >> 24) & 0xFF;
//        buf.put((byte)b);
//    }


    @Override
    public String toString() {
        return (super.toString() + " [ dataID = " + dataID + " ]");
    }
   

    @Override
    public byte[] getData() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
