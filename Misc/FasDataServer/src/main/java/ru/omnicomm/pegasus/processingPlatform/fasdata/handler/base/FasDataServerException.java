package ru.omnicomm.pegasus.processingPlatform.fasdata.handler.base;


public class FasDataServerException extends RuntimeException {
    private static final long serialVersionUID = 5011825329351734965L;

    public FasDataServerException() {
    }

    public FasDataServerException(String message) {
        super(message);
    }

    public FasDataServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public FasDataServerException(Throwable cause) {
        super(cause);
    }

}
