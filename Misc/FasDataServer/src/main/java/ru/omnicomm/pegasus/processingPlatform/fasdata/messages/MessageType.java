/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processingPlatform.fasdata.messages;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Тип сообщений, приходящих от регистраторов
 * @author alexander
 */
public enum MessageType {

    //Messages of communication protocol (between server and client)
    CMD_HELLO(0x00, "I AM HERE (00)"),
    CMD_GET_DATA(0x01, "GET DATA (01)"),
    CMD_SEND_DATA(0x02, "SEND DATA (02)"),
    CMD_CLEAR_DATA(0x03, "CLEAR DATA (03)"),
    CMD_DATA_CLEARED(0x04, "DATA CLEARED (04)"),
    CMD_SEND_FWBLOCK(0x05, "SEND FIRMWARE BLOCK (05)"),
    CMD_ACKN_FWBLOCK(0x06, "ACKNOWLEDGE FIRMWARE BLOCK (06)"),
    CMD_FW_TRANSMITTED(0x07, "FIRMWARE TRANSMITTED (07)"),

    CMD_ACKN_FW(0x08, "ACKNOWLEDGE FIRMWARE TRANSMITTED (08)"),
    CMD_GET_SETTINGS(0x09, "GET CONFIG (09)"),
    CMD_SEND_SETTINGS(0x0A, "SEND CONFIG (0A)"),
    CMD_MODIFY_SETTINGS(0x0B, "SET CONFIG (0B)"),
    CMD_ACKN_SETTINGS(0x0C, "ACKNOWLEDGE SET CONFIG (0C)"),
    CMD_START_FW_UPDTG(0x0D, "START FIRMWARE UPDATING (0D)"),
    CMD_ACKN_START_FW_UPDTG(0x0E, "ACKNOWLEDGE START FIRMWARE UPDATING (0E)"),
    
    
    //Events from data container
    EV_TMR_OPER_INFO (0x11, "TIMER INFO"),
    EV_IGT_ON (0x12, "IGNITION ON"),
    EV_IGT_OFF (0x13, "IGNITION OFF "),
    EV_PWR_ON (0x14,"POWER ON"),
    EV_EXT_PWR_OFF(0x15,"POWER OFF"),
    EV_DRIVER_REG(0x16,"DRIVER REGISTRATION"),
    EV_INCOMING_CALL(0x17,"INCOMING CALL"),
    EV_ALARM(0x19, "ALARM"),
    EV_SETTINGS(0x20,"DEVICE SETTINGS"),
    EV_ACCESS_POINT(0x21,"ACCESS POINT"),
    EV_ERROR(0x22,"ERROR"),
    
    //Service messages
    SERV_END_OF_DATA_CONTAINER(0x51,"End of data container (Service message)"),
    SERV_EMPTY_DATA_CANTAINER(0x52,"Empty data container (Service message)"),
    SERV_CLOSE_SESSION(0x53,"Close registrator session");

    private final byte code;
    private final String description;
    private static final Map<Integer, MessageType> map = new HashMap<Integer, MessageType>();


    static {
        for (MessageType type : EnumSet.allOf(MessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private MessageType(int cmd, String name) {
        this.code = (byte) cmd;
        this.description = name;
    }

    /**
     * Возвращает код команды
     * @return код команды
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает описание команды
     * @return описание команды
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Возвращает тип сообщения по коду команды
     * @param code код команды
     * @return тип сообщения
     */
    public static MessageType lookup(int code) {
        return map.get(code);
    }
}
