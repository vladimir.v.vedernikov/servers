/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;


/**
 * Событие "настройки регистратора"
 * @author akarpov
 */
public class SettingsEvent extends EventMessage{

    /**
     * Конструктор
     */
    public SettingsEvent(FasRegistratorProfile registrator){
        super(MessageType.EV_SETTINGS,registrator);
    }
    
    @Override
    public int getLength() {
        return 30;
    }

    @Override
    public List<MessageLite> getMessages() {
        return new ArrayList<MessageLite>();
    }
    
   

}
