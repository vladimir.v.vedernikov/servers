/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueRawParser;
import ru.omnicomm.pegasus.messaging.data.binarySensor.BinaryStateParser;
import ru.omnicomm.pegasus.messaging.data.fuel.GroupRawDataParser;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser;
import ru.omnicomm.pegasus.messaging.data.location.SpeedAndAzimuthParser;
import ru.omnicomm.pegasus.messaging.data.movement.MultiplySpeedParser;
import ru.omnicomm.pegasus.processingPlatform.fasdata.exceptions.NotDecodableDataException;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.BasicMessage;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;



/**
 * Базовый класс для сообщений, содержащихся в контейнере данных
 * SendDataMessage
 * @author alexander
 */
public abstract class EventMessage extends BasicMessage {
    
    //private final int sourceId;
    private final FasRegistratorProfile registrator;
    private long dataID;
    private byte[] data;
    
    protected static final int GROUP_RAW_DATA_TYPE = GroupRawDataParser.GroupRawData.getDefaultInstance().getMessageType();
    protected static final int MULTIPLY_SPEED_TYPE = MultiplySpeedParser.MultiplySpeed.getDefaultInstance().getMessageType();
    protected static final int COORDINATE_TYPE     = CoordinateParser.Coordinate.getDefaultInstance().getMessageType();
    protected static final int SPEED_AND_AZMT_TYPE = SpeedAndAzimuthParser.SpeedAndAzimuth.getDefaultInstance().getMessageType();
    protected static final int ANALOG_VALUE_TYPE   = AnalogValueRawParser.AnalogValueRaw.getDefaultInstance().getMessageType();
    protected static final int BINARY_STATE_ON_TYPE   = BinaryStateParser.BinaryStateON.getDefaultInstance().getMessageType();


    /**
     *  Конструктор
     * @param type тип сообщения по протоколу взаимодействия с регистратором
     */
    public EventMessage(MessageType type, FasRegistratorProfile registrator) {
        super(type);
        this.registrator = registrator;

    }
        

    @Override
        public byte[] getData(){
            return this.data;
        }


    /**
     * Декодирует тело сообщения
     * @param body тело сообщения
     */
    public void parseBody(byte[] body) {
        if (body == null || body.length != getLength()) {
                throw new IllegalArgumentException("Wrong body length for message " +
                                getCommand().getDescription());
        }

        this.dataID = makeLong((byte)0,(byte)0,(byte)0,(byte)0, body[4], body[3], body[2], body[1]);
        this.data = new byte[body.length-5];
        System.arraycopy(body, 5, this.data, 0, body.length-5);


    }


    @Override
    public int getLength() {
        return 39;
    }

    /**
     * Возвращает id сообщения в формате FAS
     * @return id сообщения в формате FAS
     */
    public long getFasFormatDataId() {
        return this.dataID;
    }

    /**
     * Возвращает id сообщения в формате Pegasus
     * @return id сообщения в формате Pegasus
     */
    public int getPegasusFormatDataId(){
        return (int)(this.dataID - 63072000L);
        //63072000 сек. - разница между 01.01.2011 и 01.01.2009
    }


	@Override
	public String toString() {
            String ret = " Event type = " + getCommand().getDescription() + ", dataID = " +
                            dataID + ", body length = " + getLength();

            return ret;
	}

    public Integer getSourceId(String key) {
        return this.registrator.getSourceId(key);
    }
    
    protected Integer getFuelSourceId(){
        return this.getSourceId("Fuel");
    }
    
    protected Integer getLocationSourceId(){
        return this.getSourceId("Coordinates");
    }
    
    protected Integer getSpeedSourceId(){
        return this.getSourceId("Speed");
    }
    
    protected Integer getAnalogSourceId(){
        return this.getSourceId("AnalogSensor");
    }
//
//    protected Integer getIgnitionSourceId(){
//        return this.getSourceId("Ignition");
//    }
    
    protected Integer getBinarySourceId(){
        return this.getSourceId("BinarySensor");
    }
    
//    protected void addToResponse(Integer sourceId, List<MessageLite> messages, MessageLite message){
//        if(sourceId!=null){
//            messages.add(message);
//        }
//    }
    
    protected MessageLite makeGroupRawData(int sourceId, List<Integer> sensorValues){
        return GroupRawDataParser.GroupRawData.newBuilder()
                .setSourceId(sourceId)
                .setMessageTime(this.getPegasusFormatDataId())
                .setMessageType(GROUP_RAW_DATA_TYPE)
                
                .setValid(true)
                .addAllIntValue(sensorValues)
                
                .build();
    }
    
    protected MessageLite makeMultiplySpeed(int sourceId, boolean valid, int value){
        return MultiplySpeedParser.MultiplySpeed.newBuilder()
                .setSourceId(sourceId)
                .setMessageTime(this.getPegasusFormatDataId())
                .setMessageType(MULTIPLY_SPEED_TYPE)
                
                .setValid(valid)
                .addIntValue(value)
                
                .build();
    }
    
    protected MessageLite makeCoordinate(int sourceId, boolean valid, double latitude, double longitude, double altitude, int satellite){
        return CoordinateParser.Coordinate.newBuilder()
                .setSourceId(sourceId)
                .setMessageTime(this.getPegasusFormatDataId())
                .setMessageType(COORDINATE_TYPE)
                
                .setValid(valid)
                .setLatitude(latitude)
                .setLongitude(longitude)
                .setAltitude(altitude)
                .setSatellite(satellite)
                
                .build();
    }
    
    protected MessageLite makeSpeedAndAzimuth(int sourceId, boolean valid, double speed, double azimuth){
        return SpeedAndAzimuthParser.SpeedAndAzimuth.newBuilder()
                .setSourceId(sourceId)
                .setMessageTime(this.getPegasusFormatDataId())
                .setMessageType(SPEED_AND_AZMT_TYPE)
                
                .setValid(valid)
                .setSpeed(speed)
                .setAzimuth(azimuth)
                
                .build();
    }
    
    protected MessageLite makeAnalogValueRaw(int sourceId, boolean valid, int value){
        return AnalogValueRawParser.AnalogValueRaw.newBuilder()
                .setSourceId(sourceId)
                .setMessageTime(this.getPegasusFormatDataId())
                .setMessageType(ANALOG_VALUE_TYPE)
                
                .setValid(valid)
                .addIntValue(value)
                
                .build();
    }
    
    protected MessageLite makeBinaryStateON(int sourceId){
        return BinaryStateParser.BinaryStateON.newBuilder()
                .setSourceId(sourceId)
                .setMessageTime(this.getPegasusFormatDataId())
                .setMessageType(BINARY_STATE_ON_TYPE)
                .build();
    }
    
    



    //63072000


    /**
     * Возвращает список сообщений, содержащихся в пакете данных FAS
     * (данные по топливу, местоположение и т.д.)
     * @return список сообщений, содержащихся в пакете данных FAS
     * @throws NotDecodableDataException в случае ошибки декодирования
     */
    public abstract List<MessageLite> getMessages() throws NotDecodableDataException;
	

}
