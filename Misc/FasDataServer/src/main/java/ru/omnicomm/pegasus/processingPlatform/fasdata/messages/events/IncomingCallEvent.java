/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages.events;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.fasdata.handler.FasRegistratorProfile;
import ru.omnicomm.pegasus.processingPlatform.fasdata.messages.MessageType;


/**
 * Событие "Входящий звонок"
 * (на данный момент (10.2010) не реализовано на стороне регистратора)
 * @author akarpov
 */
public class IncomingCallEvent extends EventMessage {
    
    /**
     * Конструктор
     */
    public IncomingCallEvent(FasRegistratorProfile registrator){
        super(MessageType.EV_INCOMING_CALL,registrator);
    }

    @Override
    public List<MessageLite> getMessages() {
        return new ArrayList<MessageLite>();
    }
}
