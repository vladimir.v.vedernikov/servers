/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.fasdata.messages;


/**
 * Подтверждение на стирание данных
 * @author alexander
 */
public class DataClearedMessage extends BasicMessage{

    private long dataID;
    private byte[] body;

    /**
     * Конструктор
     * @param body тело сообщения
     */
    public DataClearedMessage(byte[] body){
        super(MessageType.CMD_DATA_CLEARED);
        this.body = body;
        if (body != null && body.length == 4){
            byte nb = 0;
            this.dataID = this.makeLong(nb,nb,nb,nb,body[3], body[2], body[1], body[0]);
        } else {
            throw new IllegalArgumentException("Wrong body of DataClearedMessage");
            
        }

    }

    /**
     * Возвращает id сообщения, до которого включительно 
     * очищен архив регистратора
     * @return id сообщения
     */
    public long getDataID(){
        return this.dataID;
    }

    public int getLength(){
        return body.length;
    }

    @Override
    public String toString() {
        return (super.toString() + " [ dataID = " + dataID + " ]");
    }

    

    @Override
    public byte[] getData() {
        return this.body;
    }

}

