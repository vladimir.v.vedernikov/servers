#!/bin/sh

DEST_PATH=../java

########################################
# Create parsers for required messages #
########################################

SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/data/fuel
FILE_NAME=GroupRawData.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/data/movement/
FILE_NAME=MultiplySpeed.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/data/location/
for FILE_NAME in Coordinate.proto SpeedAndAzimuth.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done

SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/data/analogSensor/
FILE_NAME=AnalogValueRaw.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/data/binarySensor/
FILE_NAME=BinaryStates.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

########################################
# Create parser for required settings  #
########################################
FILE_NAME=RegistratorProfile.proto
SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/fas
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


echo "done"
