/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 18.11.11
 */
package ru.omnicomm.pegasus.processing.generator;

import ru.omnicomm.pegasus.processingPlatform.messages.DataGeneratorServerMessageType;

import java.util.ArrayList;
import java.util.List;

/**
 * Парсит строки следующего формата:
 * <p/>
 * <code><i>quantity</i>;<i>timeout</i>;<i>sourceId</i>;<i>messageType</i>;<i>messageTime</i>[;<i>messageValue</i>]*</code>
 * , где
 * <ul>
 * <li><i>quantity</i> - количество повторений строки</li>
 * <li><i>timeout</i> - временная задержка перед отправкой сообщения, указывается в миллисекундах</li>
 * <li><i>sourceId</i> - идентификатор источника</li>
 * <li><i>messageType</i> - тип сообщения, задается именем сообщения, определенном в файле *.proto</li>
 * <li><i>messageTime</i> - время сообщения, задается в виде строки формата {@link ValueConverter#DATE_FORMAT}</li>
 * <li><i>messageValue</i> - значения полей тела сообщения</li>
 * </ul>
 * Для всех <b>числовых</b> параметров тела сообщения может быть указан диапазон значений и шаг изменения параметра
 * (положительный или отрицательный). В этом случае значение параметра сообщения является <i>'сложным'</i> значением,
 * которое меняется на шаг изменения в зависимости от повторения сообщения. Такие значения должны быть выделены в блок
 * {@link ParserUtil#COMPOUND_BRACE_START} и {@link ParserUtil#COMPOUND_BRACE_END}, в котором по порядку
 * указываются начальное значение, шаг изменения параметра, левая граница диапазона и правая граница диапазона.
 * <p/>
 * В случае если параметр сообщения <i>'повторяемого'</i> типа (определен как {@code repeated} в {@code *.proto} файле),
 * то список его значений можно задать в блоке {@link ParserUtil#REPEATED_BRACE_START}
 * и {@link ParserUtil#REPEATED_BRACE_END}.<br/><b>Важно</b>, параметер сообщения не может быть определен
 * одновременно как <i>'сложный'</i> и как <i>'повторяемый'</i>.
 * <p/>
 * В файле помимо данных о сообщениях могут быть комментарии - это строки которые начинаются с символов
 * {@link ParserUtil#COMMENT}. Комментарии никак не обрабатываются.
 * <p/>
 * Пример текстового файла, который содержит информацию для создания 10 сообщений типа
 * {@link ru.omnicomm.pegasus.processingPlatform.messages.MessageParser.ApproximatedData}, с временной задержкой в 0,5
 * секунды, от источника с идентификатором {@code 1001}, со значениями сообщения от {@code 0} до {@code 9}.
 * У всех 10 сообщений одна и таже временная метка = {@code 1 января 2011г. 01:00:00}.
 * <p/>
 * {@code //Создание 10 сообщений}<br/>
 * {@code 10; 500; 1001; ApproximatedData; 2011.01.01 04:00:00; {0,1,0,10};}
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class CommonDataLineParser implements DataLineParser {

    private ValueConverter converter;

    public CommonDataLineParser() {
        converter = new ValueConverter();
    }

    @Override
    public MessageBuilder parse(String dataLine) throws MessageBuildException {
        final Data data = new Data(dataLine);
        final int quantity = data.getQuantity();
        final int timeout = data.getTimeout();
        final int sourceId = data.getSourceId();
        final DataGeneratorServerMessageType messageType = data.getMessageType();
        final String messageTime = data.getMessageTime();
        final List<String> values = data.getValues();

        MessageBuilderDynamic builder = new MessageBuilderDynamic();
        builder.setConverter(converter);
        builder.initialize(quantity, timeout, sourceId, messageType, messageTime, values);
        return builder;
    }

    /**
     * Используется для разбиения строки с данными на составляющие, а также для ее первонячальной валидации -
     * проверки наличия минимальнного набора({@link #MIN_VALUES_AMOUNT}) значений в строке. После преобразования строки
     * получаем следующие значения:
     * <ul>
     * <li>колиество повторений для строки типа {@code int}</li>
     * <li>временная задержка перед отправкой сообщения в миллисекундах типа {@code int}</li>
     * <li>{@code sourceId} типа {@code int}</li>
     * <li>{@code messageType} типа {@link MessageType}</li>
     * <li>строковое представление даты сообщения {@code messageTime}</li>
     * </ul>
     *
     * @author Sergey.Sitishev (sitishev@omnicomm.ru)
     */
    private class Data {
        /**
         * Минимальное количество значений, необходимое для построения сообщения
         * (quantity, timeout, sourceId, messageType, messageTime).
         */
        public static final int MIN_VALUES_AMOUNT = 5;

        public static final int QUANTITY_VALUE_INDEX = 0;

        public static final int TIMEOUT_VALUE_INDEX = 1;

        public static final int SOURCE_ID_VALUE_INDEX = 2;

        public static final int MESSAGE_TYPE_VALUE_INDEX = 3;

        public static final int MESSAGE_TIME_VALUE_INDEX = 4;

        private int quantity;
        private int timeout;
        private int sourceId;
        private DataGeneratorServerMessageType messageType;
        private String messageTime;
        private List<String> values;

        private Data(String dataLine)
                throws MessageBuildException {
            initialize(dataLine);
        }

        private void initialize(String dataLine)
                throws MessageBuildException {
            List<String> allValues = new ArrayList<String>();

            for (String splited : dataLine.split(ParserUtil.LINE_VALUE_DELIMITER)) {
                allValues.add(splited.trim());
            }

            if (allValues.size() < MIN_VALUES_AMOUNT) {
                final String errorMessage
                        = new StringBuilder().append("Not enough data in line: \"").append(dataLine)
                        .append("\". Usage: quantity;timeout;sourceId;messageType;messageTime;[allValues]*").toString();
                throw new MessageBuildException(errorMessage);
            }

            this.quantity = getQuantityValue(allValues);
            this.timeout = getTimeoutValue(allValues);
            this.sourceId = getSourceIdValue(allValues);
            this.messageType = getMessageType(allValues);
            this.messageTime = getMessageTime(allValues);
            this.values = allValues.subList(MIN_VALUES_AMOUNT, allValues.size());
        }

        private String getMessageTime(List<String> values) {
            return values.get(MESSAGE_TIME_VALUE_INDEX).trim();
        }

        private int getQuantityValue(List<String> values)
                throws MessageBuildException {
            String strQuantity = values.get(QUANTITY_VALUE_INDEX);
            try {
                int quantity = getIntValue(strQuantity);
                if (quantity < 0) {
                    final String error = new StringBuilder().append("Quantity value=").append(quantity)
                            .append(" have to be equal to or greater than 0").toString();
                    throw new MessageBuildException(error);
                }
                return quantity;
            } catch (IllegalArgumentException e) {
                final String error = new StringBuilder().append("Invalid quantity value=").append(strQuantity).toString();
                throw new MessageBuildException(error, e);
            }
        }

        private int getTimeoutValue(List<String> values)
                throws MessageBuildException {
            String strTimeout = values.get(TIMEOUT_VALUE_INDEX);
            try {
                int timeout = getIntValue(strTimeout);
                if (timeout < 0) {
                    final String error = new StringBuilder().append("Timeout value=").append(timeout)
                            .append(" have to be equal to or greater than 0").toString();
                    throw new MessageBuildException(error);
                }
                return timeout;
            } catch (IllegalArgumentException e) {
                final String error = new StringBuilder().append("Invalid timeout value=").append(strTimeout).toString();
                throw new MessageBuildException(error, e);
            }
        }

        private int getSourceIdValue(List<String> values)
                throws MessageBuildException {
            String strSourceId = values.get(SOURCE_ID_VALUE_INDEX);
            try {
                return getIntValue(strSourceId);
            } catch (IllegalArgumentException e) {
                final String error = new StringBuilder().append("Invalid sourceId value=").append(strSourceId).toString();
                throw new MessageBuildException(error, e);
            }
        }

        private int getIntValue(String strValue) {
            try {
                return Integer.valueOf(strValue);
            } catch (NumberFormatException e) {
                final String message = new StringBuilder().append("Cannot conver strValue=").append(strValue)
                        .append(" to Integer.").toString();
                throw new IllegalArgumentException(message);
            }
        }

        private DataGeneratorServerMessageType getMessageType(List<String> values)
                throws MessageBuildException {
            String messageTypeName = values.get(MESSAGE_TYPE_VALUE_INDEX);
            DataGeneratorServerMessageType messageType = DataGeneratorServerMessageType.lookup(messageTypeName);
            if (messageType == null) {
                final String message = new StringBuilder().append("Unsupported message with name=")
                        .append(messageTypeName).toString();
                throw new MessageBuildException(message);
            }
            return messageType;
        }

        public int getQuantity() {
            return quantity;
        }

        public int getTimeout() {
            return timeout;
        }

        public int getSourceId() {
            return sourceId;
        }

        public DataGeneratorServerMessageType getMessageType() {
            return messageType;
        }

        public String getMessageTime() {
            return messageTime;
        }

        public List<String> getValues() {
            return values;
        }

    }

}
