/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 26.08.11
 */
package ru.omnicomm.pegasus.processing.generator;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Loading;
import ru.omnicomm.pegasus.processingPlatform.messages.Pause;
import ru.omnicomm.pegasus.processingPlatform.messages.Paused;
import ru.omnicomm.pegasus.processingPlatform.messages.Resume;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.messages.TimerEvent;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

import java.io.IOException;

/**
 * Абстрактная реализация сервера генерации сообщений.
 * <p/>
 * При получении сигнала {@link Init} вызывает абстрактный метод {@link AbstractGenerator#createDataSource()} для
 * получения экземпляра {@link DataSource}. Этот метод должен быть реализован в классах наследниках
 * (см. пример {@link DataGeneratorServer}).
 * Далее сервер получает по одному сгенерированные сообщения из {@link DataSource} и отправляет их на роутер через
 * время указанное в {@link MessageInfo#getTimeout()}.
 * <p/>
 * В сервере реализована реакция на сигнал {@link Pause} который может прийти от менеджера слоя. Если сервер остановлен
 * то генерация сообщений и их посылка на роутер не осуществляется.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public abstract class AbstractGenerator /*implements ServerImplementation*/ {

    private final static Logger LOGGER = LoggerFactory.getLogger();

    /**
     * ID таймера по которому происходит генерация следующего сообщения.
     */
    public static final String GENERATE_MESSAGE_TIMER_ID = "GENERATE_MESSAGE_TIMER_ID";

    /**
     * Файл с данными по умолчанию.
     */
    public static final String DEFAULT_DATA_FILE_NAME = "input.csv";

    /**
     * Генератор данных.
     */
    private DataSource dataSource;

    /**
     * Текущее сгенерированное сообщение.
     */
//    private MessageLite currentMessage;

    private MessageInfo currentInfo;

    private MessageInfo nextCurrentInfo;

    /**
     * Если сервер остановлен, то генерация сообщений и их посылка не осуществляется.
     */
    private boolean paused;

    /**
     * Индикатор того, что dataSource был опустошен, т.е. при вызове метода {@link DataSource#hasNext()}
     * получили {@code false}.
     */
    private boolean dataSourceEmptied;

    /**
     * Флаг сигнализирующий, что была пропущена посылка текущего сообщения из-за остановленности сервера.
     * Если этот флаг {@code true}, то при возобновлении работы (по приходу сигнала {@link Resume}) будет обработано
     * текущее сообщение и сгенерировано новое, иначе измениться только значение {@link #paused} на {@code false}.
     */
    private boolean timerSkipped;

    /**
     * Если {@code true}, то прекращается генерация сообщений, а также игнорируютс события от таймера.
     * Выставляется в {@code true} после прихода сигнала {@link Terminate}.
     */
    private boolean terminated;

    private ResultWriter resultWriter;

    private Handler handler;

    private Server self;

    private Router router;

    /**
     * Конструктор.
     *
     * @param router сервис маршрутизатор сообщений.
     * @throws NullPointerException если {@code router null}.
     */
    public AbstractGenerator(Router router) {
        if (router == null) {
            throw new NullPointerException("Router is null.");
        }
        this.router = router;
    }

//    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {

            @Override
            public void visit(CaughtException signal) throws RuntimeException {
                LOGGER.log(Level.INFO, null, signal.getException());
            }

            @Override
            public void visit(Init signal) throws RuntimeException {
                handler = signal.getHandler();
                self = signal.getSelf();

                loadSettings();
                dataSource = createDataSource();
                resultWriter = createResultWriter();

                // Генерация первого сообщения.
                LOGGER.info("DataSource initialized...");
                if (dataSource.hasNext()) {
                    nextCurrentInfo = dataSource.next();
                    generateTimerEvent();

                }
//                generateTimerEvent();
            }

            @Override
            public void visit(TimerEvent signal) throws RuntimeException {
                if (terminated) {
                    return;
                }
                if (GENERATE_MESSAGE_TIMER_ID.equals(signal.id())) {
                    processCurrentMessage();
                }
            }

            @Override
            public void visit(Pause signal) throws RuntimeException {
                if (terminated) {
                    return;
                }
                paused = true;
                Server source = signal.source();
                try {
                    source.send(new Paused() {
                        @Override
                        public Server source() {
                            return self;
                        }

                        @Override
                        public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                            visitor.visit(this);
                        }
                    });
                } catch (SendMessageException e) {
                    LOGGER.log(Level.WARN, "Cannot send answer Paused signal.", e);
                }
            }

            @Override
            public void visit(Resume signal) throws RuntimeException {
                if (terminated) {
                    return;
                }
                paused = false;
                if (timerSkipped) {
                    timerSkipped = false;
                    processCurrentMessage();
                }
            }

            @Override
            public void visit(Terminate signal) throws RuntimeException {
                String cause = signal.cause();
                LOGGER.info("[AbstractGenerator] Terminate signal with cause=" + cause);
                terminated = true;
                closeResultWriter();
            }

            @Override
            public void visit(Loading signal) throws RuntimeException {
                if (terminated) {
                    return;
                }
                Loading loading = new Loading() {
                    @Override
                    public Server source() {
                        return self;
                    }

                    @Override
                    public int level() {
                        return 10;
                    }

                    @Override
                    public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                        visitor.visit(this);
                    }
                };
                try {
                    signal.source().send(loading);
                } catch (SendMessageException e) {
                    LOGGER.log(Level.WARN, "Cannot send answer Loading signal.", e);
                }
            }
        });
    }

//    @Override
    public void onMessage(Server server, MessageLite messageLite) {
        // do nothing.
    }

    private void generateTimerEvent() {
        if (dataSourceEmptied) {
            return;
        }
//        if (dataSource.hasNext()) {
//            MessageInfo messageInfo = dataSource.next();
//            currentMessage = messageInfo.getMessage();
//            long delay = messageInfo.getTimeout();
//            handler.setTimer(GENERATE_MESSAGE_TIMER_ID, Handler.TimerType.DELAY_BASED, delay, 0);
//        } else {
//            dataSourceEmptied = true;
//            closeResultWriter();
//            LOGGER.log(Level.INFO, "DataSource emptied.");
//        }

        long delay = nextCurrentInfo.getTimeout();
        handler.setTimer(GENERATE_MESSAGE_TIMER_ID, Handler.TimerType.DELAY_BASED, delay, 0);
        currentInfo = nextCurrentInfo;
//        nextCurrentInfo = null;
        if (dataSource.hasNext()) {
            nextCurrentInfo = dataSource.next();
        } else {
            dataSourceEmptied = true;
            // баг - MP-1017 - не пишет последнее сообщение, появился после добавления nextCurrentInfo - для увеличения
            // скорости работы генератора.
//            closeResultWriter();
            LOGGER.log(Level.INFO, "DataSource emptied.");
        }

    }

    private void processCurrentMessage() {
//        if (!paused) {
//            if (currentMessage != null) {
//                logMessage(currentMessage);
//                router.send(currentMessage);
//            }
//            generateTimerEvent();
//        } else {
//            timerSkipped = true;
//        }

        if (!paused) {
            MessageLite message = currentInfo.getMessage();
            if (message != null) {
                logMessage(message);
                router.send(message);
            }
            if (!dataSourceEmptied) {
                generateTimerEvent();
            } else {
                // Закрываем здесь, после того как последнее обработанное сообщение было обработано.
                closeResultWriter();
            }
        } else {
            timerSkipped = true;
        }
    }

    private void closeResultWriter() {
        if (resultWriter != null) {
            resultWriter.closeWriter();
            resultWriter = null;
        }
    }

    private void logMessage(MessageLite message) {
        if (resultWriter != null) {
            resultWriter.write(message);
        }
    }

    /**
     * Читает настройки.
     */
    protected void loadSettings() {
        //Empty
    }

    /**
     * Создает логгер для записи сгенерированных сообщений в файл.
     *
     * @return логгер для записи сгенерированных сообщений в файл.
     */
    protected ResultWriter createResultWriter() {
        ResultWriter result = null;
        try {
            String fileName = getResultWriterFileName();
            result = new ResultWriter(fileName);
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, null, e);
        }
        return result;
    }

    /**
     * Возвращает имя файла в который пишутся сгенерированные сообщения.
     *
     * @return имя файла в который пишутся сгенерированные сообщения.
     */
    protected String getResultWriterFileName() {
        return handler.id().toLowerCase() + ".log";
    }


    /**
     * Создает осточник данных. Вызывается один раз при инициализации сервера.
     *
     * @return источник данных.
     */
    protected abstract DataSource createDataSource();

}
