/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 18.11.11
 */
package ru.omnicomm.pegasus.processing.generator;

/**
 * Интерфейс определяет метод преобразования строки в {@link MessageBuilder}.
 * Используется в {@link FileDataSource} для разбора файла с данными.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
interface DataLineParser {

    /**
     * Возвращает экземпляр {@link MessageBuilder} по переданной строке {@code dataLine}.
     *
     * @param dataLine строка данных.
     * @return разобранная строка.
     * @throws MessageBuildException при невозможности преобразовать строку {@code dataLine} в сообщение.
     */
    public MessageBuilder parse(String dataLine)
            throws MessageBuildException;

}
