/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 08.09.11
 */
package ru.omnicomm.pegasus.processing.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Предоставляет методы для разбиения сложных и повторяемых значений.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class ParserUtil {

    /**
     * Разделитель значений в строке с данными.
     */
    public static final String LINE_VALUE_DELIMITER = ";";

    /**
     * Разделитель простых значений в составе сложных.
     */
    public static final String COMPOUND_VALUE_DELIMITER = ",";

    /**
     * Коментарий.
     */
    public static final String COMMENT = "//";

    /**
     * Начало блока для сложного значения сообщения.
     */
    public static final String COMPOUND_BRACE_START = "{";

    /**
     * Конец блока для сложного значения сообщения.
     */
    public static final String COMPOUND_BRACE_END = "}";

    /**
     * Начало повторяемого блока.
     */
    public static final String REPEATED_BRACE_START = "[";

    /**
     * Конец повторяемого блока.
     */
    public static final String REPEATED_BRACE_END = "]";

    /**
     * Регулярное выражение для получения списка повторяемых значений. Используется взамен стандартного
     * {@link StringTokenizer} с разделителем {@link #COMPOUND_VALUE_DELIMITER}, потому что в списке значений
     * повторяемого поля могут быть сложные значения, у которых в качестве разделителя используются тотже
     * {@link #COMPOUND_VALUE_DELIMITER}.
     * <br/>Здесь \u007B = '{' и \u007D = '}'.
     */
   public static final Pattern repeatedPattern = Pattern.compile("[^\\u007B\\u007D\\s,]+|\\u007B(?:[^\\u007B\\u007D])*\\u007D");

    /**
     * Проверяет является ли сложным {@code value}, т.е. проверяет наличие {@link #COMPOUND_BRACE_START} и {@link #COMPOUND_BRACE_END}.
     *
     * @param value значение.
     * @return {@code true} если обромлено в {@link #COMPOUND_BRACE_START} и {@link #COMPOUND_BRACE_END}, иначе {@code false}.
     * @throws NullPointerException если {@code value null}.
     */
    public static boolean isCompoundValue(String value) {
        return isBracedValue(COMPOUND_BRACE_START, COMPOUND_BRACE_END, value);
    }

    /**
     * Разбивает сложное значение на простые. В качестве {@code compaundValue} ожидается строка вида:
     * <code>{@link #COMPOUND_BRACE_START} value {@link #COMPOUND_VALUE_DELIMITER} value ... {@link #COMPOUND_BRACE_START}</code>.
     *
     * @param compaundValue сложное значение.
     * @return список простых в составе сложного {@code compaundValue}.
     */
    public static List<String> getCompaundValueList(String compaundValue) {
        return getBracedValueList(COMPOUND_BRACE_START, COMPOUND_BRACE_END, compaundValue);
    }

    /**
     * Проверяет является ли значение {@code value} повторяемым, т.е. проверяет наличие {@link #REPEATED_BRACE_START}
     * и {@link #REPEATED_BRACE_END}.
     *
     * @param value значение.
     * @return {@code true} если обромлено в {@link #REPEATED_BRACE_START} и {@link #REPEATED_BRACE_END},
     *         иначе {@code false}.
     * @throws NullPointerException если {@code value null}.
     */
    public static boolean isRepeatedValue(String value) {
        return isBracedValue(REPEATED_BRACE_START, REPEATED_BRACE_END, value);
    }

    /**
     * Разбивает сложное значение на простые. В качестве {@code repeatedValue} ожидается строка вида:
     * <code>{@link #REPEATED_BRACE_START} value {@link #COMPOUND_VALUE_DELIMITER} value ... {@link #REPEATED_BRACE_END}</code>.
     * Значения могут быть определены как сложные, т.е. менять свои значения при каждом вызове.
     *
     * @param repeatedValue сложное значение.
     * @return список простых в составе сложного {@code repeatedValue}.
     */
    public static List<String> getRepeatedValueList(String repeatedValue) {
        final String value = getNoBraceValue(REPEATED_BRACE_START, REPEATED_BRACE_END, repeatedValue);
        Matcher matcher = repeatedPattern.matcher(value);
        List<String> repeatedValues = new ArrayList<String>();
        while (matcher.find()) {
            repeatedValues.add(matcher.group());
        }
        return repeatedValues;
    }

    /**
     * Вырезает {@code startBrace} и {@code endBrace} из {@code value}. Предполагается, что метод используется
     * после получения положительного вызова метода {@link #isBracedValue(String, String, String)}.
     *
     * @param startBrace начало блока.
     * @param endBrace   конец блока.
     * @param value      строка обрамленная в {@code startBrace} и {@code endBrace}.
     * @return строка без {@code startBrace} и {@code endBrace}.
     */
    private static String getNoBraceValue(String startBrace, String endBrace, String value) {
        value = value.trim();
        return value.substring(startBrace.length(), value.length() - endBrace.length());
    }

    private static boolean isBracedValue(String startBrace, String endBrace, String value) {
        if (value == null) {
            throw new NullPointerException("Value is null");
        }

        value = value.trim();
        return value.startsWith(startBrace) && value.endsWith(endBrace);
    }

    private static List<String> getBracedValueList(String startBrace, String endBrace, String value) {
        final String withOutBrace = getNoBraceValue(startBrace, endBrace, value);
        StringTokenizer stringTokenizer = new StringTokenizer(withOutBrace, COMPOUND_VALUE_DELIMITER);
        List<String> values = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            values.add(nextToken.trim());
        }
        return values;
    }
}
