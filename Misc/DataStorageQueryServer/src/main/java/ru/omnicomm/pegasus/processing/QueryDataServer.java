/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 28.09.11
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import java.util.Iterator;
import ru.omnicomm.pegasus.messaging.common.QueryDataServerMessageType;
import ru.omnicomm.pegasus.messaging.data.service.DataStorageQueryParser.DataStorageQuery;
import ru.omnicomm.pegasus.messaging.data.service.LayerMessagesParser;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
//import ru.omnicomm.pegasus.processingPlatform.messages.MessageParser;
//import ru.omnicomm.pegasus.processingPlatform.messages.MessageType;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("QueryServerImplementation")
public class QueryDataServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    /**
     * Значение шаблона запроса при котором поле может принимать любое значение.
     */
    public static final int UNDEFINED_QUERY_VALUE = -1;

    /**
     * Максимальное значение ответных сообщений.
     */
    public static final int QUERY_MESSAGE_LIMIT = 1000;

    private DataStorageService dataStorage;

    private JmsBridgeService jmsBridge;

    private Server self;
    private Handler handler;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     */
    @ServerConstructor
    public QueryDataServer(DataStorageService dataStorage, JmsBridgeService jmsBridge) {
        this.dataStorage = dataStorage;
        this.jmsBridge = jmsBridge;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        DataStorageQuery query = null;
        try {
            query = DataStorageQuery.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
        }

        if (query == null) {
            return;
        }

        if (QueryDataServerMessageType.DATA_STORAGE_QUERY.getCode() != query.getMessageType()) {
            LOGGER.log(Level.WARN, "Unexpected message type=" + query.getMessageType());
            return;
        }

        int sourceId = query.getQueryId();
        int messageType = query.getQueryType();
        int from = query.getQueryFrom();
        int till = query.getQueryTill();

        if (LOGGER.isDebugEnabled()) {
            final String msg = new StringBuilder().append("Query received: sourceId= ").append(sourceId)
                    .append(", messageType=").append(messageType)
                    .append(", from=").append(from)
                    .append(", till=").append(till).toString();
            LOGGER.log(Level.INFO, msg);
        }

        if (UNDEFINED_QUERY_VALUE == from) {
            from = 0;
        }

        if (UNDEFINED_QUERY_VALUE == till) {
            till = Integer.MAX_VALUE;
        }

        try {
            Iterator<MessageChunk> messages = queryMessages(sourceId, messageType, from, till);
            sendResultMessages(messages);
        } catch (DataStorageServiceException e) {
            handleDataStorageServiceException(e);
        } catch (JmsBridgeServiceException e) {
            handleJmsBridgeServiceException(e);
        } catch (IllegalArgumentException e) {
            // Посылаем сообщение ошибку.
            LOGGER.log(Level.ERROR, null, e);
            MessageLite errorMessage = getErrorMessage(e.getMessage());
            sendErrorMessage(errorMessage);
        }
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();
                handler = signal.getHandler();

                jmsBridge.registerServer(QueryDataServer.class, self, QueryDataServerMessageType.DATA_STORAGE_QUERY.getCode());
            }

            @Override
            public void visit(Terminate signal) {
                super.visit(signal);    //To change body of overridden methods use File | Settings | File Templates.
            }
        });
    }

    private Iterator<MessageChunk> queryMessages(int sourceId, int messageType, int from, int till)
            throws DataStorageServiceException {
        if (UNDEFINED_QUERY_VALUE == sourceId && UNDEFINED_QUERY_VALUE == messageType) {
            // Не определены сразу и идентификатор источника, и тип сообщения.
            throw new IllegalArgumentException("Invalid query message format. Query sourceId and query messageType are undefined.");
        } else if (UNDEFINED_QUERY_VALUE == sourceId) {
            return dataStorage.queryMessageType(messageType, from, till);
        } else if (UNDEFINED_QUERY_VALUE == messageType) {
            return dataStorage.querySourceId(sourceId, from, till);
        } else {
            return dataStorage.query(sourceId, messageType, from, till);
        }
    }

    private int getTimeStamp() {
        long milliseconds = System.currentTimeMillis();
        return (int) ((milliseconds - STARTING_POINT) / 1000);
    }

    private void sendResultMessages(Iterator<MessageChunk> messages)
            throws JmsBridgeServiceException {
        int counter = 0;
        while (messages.hasNext()) {
            MessageChunk chunk = messages.next();
            for (MessageLite message : chunk.messages) {
                jmsBridge.send(message);
                counter++;
                if (counter > QUERY_MESSAGE_LIMIT) {
                    return;
                }
            }
        }
    }

    private MessageLite getErrorMessage(String cause) {
        return LayerMessagesParser.Error.newBuilder()
                .setSourceId(1)
                .setMessageType(QueryDataServerMessageType.ERROR.getCode())
                .setMessageTime(getTimeStamp())
                .setErrorId(1)
                .setCause(cause).build();
    }

    private void sendErrorMessage(MessageLite error) {
        try {
            jmsBridge.send(error);
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.ERROR, null, e);
        }
    }

    private void handleDataStorageServiceException(DataStorageServiceException e) {
        LOGGER.log(Level.ERROR, null, e);
        String cause = "[DataStorageServiceException] " + e.getMessage();
        MessageLite errorMessage = getErrorMessage(cause);
        sendErrorMessage(errorMessage);
    }

    private void handleJmsBridgeServiceException(JmsBridgeServiceException e) {
        LOGGER.log(Level.ERROR, null, e);
        String cause = "[JmsBridgeServiceException] " + e.getMessage();
        MessageLite errorMessage = getErrorMessage(cause);
        sendErrorMessage(errorMessage);
    }
}

