/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.service.DataStorageQueryParser.DataStorageQuery;
import ru.omnicomm.pegasus.messaging.data.service.LayerMessagesParser;


/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum QueryDataServerMessageType {

    DATA_STORAGE_QUERY(DataStorageQuery.getDefaultInstance().getMessageType()),
    ERROR(LayerMessagesParser.Error.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, QueryDataServerMessageType> map = new HashMap<Integer, QueryDataServerMessageType>();

    static {
        for (QueryDataServerMessageType type : EnumSet.allOf(QueryDataServerMessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private QueryDataServerMessageType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static QueryDataServerMessageType lookup(int code) {
        return map.get(code);
    }

}
