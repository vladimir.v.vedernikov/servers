#!/bin/sh

DEST_PATH=../java

##############################################
# Create common parser (for parsing headers) #
##############################################
FILE_NAME=MessageHeader.proto
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/common
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


########################################
# Create parsers for required messages #
########################################
SRC_PATH=../../../../../../MessageStorage/src/main/resources/protobuf/data/service
for FILE_NAME in DataStorageQuery.proto  LayerMessages.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done




echo "done"
