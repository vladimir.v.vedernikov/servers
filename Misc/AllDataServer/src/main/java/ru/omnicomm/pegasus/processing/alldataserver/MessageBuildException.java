/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 30.08.11
 */
package ru.omnicomm.pegasus.processing.alldataserver;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class MessageBuildException extends Exception {
    private static final long serialVersionUID = -1723052430629053623L;

    public MessageBuildException() {
    }

    public MessageBuildException(String message) {
        super(message);
    }

    public MessageBuildException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageBuildException(Throwable cause) {
        super(cause);
    }
}
