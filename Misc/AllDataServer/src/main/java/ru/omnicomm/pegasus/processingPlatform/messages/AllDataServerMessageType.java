/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 02.06.2011
 */
package ru.omnicomm.pegasus.processingPlatform.messages;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.MessageLite;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Перечисление типов сообщений, использующихся в платформе
 *
 * @author alexander
 */
public enum AllDataServerMessageType {

    // LAYER MESSAGES
    ERROR("Error"),

    GROUP_RAW_DATA("GroupRawData"),
    GROUP_UNSMOOTHED_DATA("GroupUnsmoothedData"),
    SUM_UNSMOOTHED_DATA("SumUnsmoothedData"),

    UNSMOOTHED_DATA("UnsmoothedData"),
    APPROXIMATED_DATA("ApproximatedData"),

    DRAINING_OR_REFUELING_BEGINS("DrainingORRefuelingBegin"),
    DRAINING_OR_REFUELING_ENDS("DrainingORRefuelingEnd"),

    SPEED_AND_AZIMUTH("SpeedAndAzimuth"),
    MULTIPLY_SPEED("MultiplySpeed"),
    VALUE_MULTIPLY_SPEED("ValueMultiplySpeed"),
    ANALOG_VALUE_CORRECTED("AnalogValueCorrected"),
    COORDINATE("Coordinate"),

    VELOCITY_UP("VelocityUP"),
    VELOCITY_DOWN("VelocityDOWN"),
    VELOCITY_NORMAL("VelocityNORMAL"),

    ANALOG_VALUE_UP("AnalogValueUP"),
    ANALOG_VALUE_DOWN("AnalogValueDOWN"),
    ANALOG_VALUE_NORMAL("AnalogValueNormal"),
    ANALOG_VALUE_RAW("AnalogValueRaw"),

    BINARY_STATE("BinaryState"),
    BINARY_STATE_ON("BinaryStateON"),
    BINARY_STATE_OFF("BinaryStateOFF"),

    AGR_MILEAGE_V("AGRMileageV"),
    AGR_VELOCITY_MAX("AGRVelocityMax"),
    AGR_VELOCITY_AVR("AGRVelocityAvr"),

    AGR_ANALOG_WORK("AGRAnalogWork"),
    AGR_ANALOG_MAX("AGRAnalogMax"),
    AGR_ANALOG_AVR("AGRAnalogAvr"),

    AGR_MILEAGE_GPS("AGRMileageGPS"),

    PARKING_START("ParkingSTART"),
    PARKING_END("ParkingEND"),
    
    DRAINING_BEGIN("DrainingSTART"),
    DRAINING_END("DrainingEND"),
    REFUELING_BEGIN("RefuelingSTART"),
    REFUELING_END("RefuelingEND"),
    
    GeoZoneSTART("GeoZoneSTART"),
    GeoZoneEND("GeoZoneEND"),
    GeoZones("GeoZones"),
    
    //INPUT MESSAGES
    DATA_STORAGE_QUERY("DataStorageQuery"),

    //KIA_20121228 Added GpsStateServer messages. Start >>>
    BAD_GPS_START("BadGpsSTART"),
    BAD_GPS_END("BadGpsEND");
    //<<< End KIA_20121228

    private static final String MESSAGE_TYPE_FIELD_NAME = "messageType";
    private static final String DESCRIPTOR_METHOD_NAME = "getDescriptor";
    private static final String BUILDER_METHOD_NAME = "newBuilder";
    private static final String PARSE_METHOD_NAME = "parseFrom";

    private final int code;
    private final String name;
    private final Class<? extends GeneratedMessage> javaType;

    private static final Map<Integer, AllDataServerMessageType> codeToType = new HashMap<Integer, AllDataServerMessageType>();
    private static final Map<String, AllDataServerMessageType> nameToType = new HashMap<String, AllDataServerMessageType>();
    private static Map<AllDataServerMessageType, Class<? extends GeneratedMessage>> messageClasses
            = new HashMap<AllDataServerMessageType, Class<? extends GeneratedMessage>>();

    static {
        for (AllDataServerMessageType type : EnumSet.allOf(AllDataServerMessageType.class)) {
            // Проверка уникальности значения типа сообщения, который задается в файле MessageParser.proto в качестве
            // значения по умолчанию для поля MESSAGE_TYPE_FIELD_NAME
            final int typeCode = type.getCode();
            final String typeName = type.getName();
            final Class<? extends GeneratedMessage> javaType = type.getJavaType();
            if (codeToType.containsKey(typeCode)) {
                final String error = new StringBuilder().append("Both messages ").append(codeToType.get(typeCode))
                        .append(" and ").append(type).append(" have the same message type value=").append(typeCode)
                        .toString();
                throw new RuntimeException(error);
            }
            if (nameToType.containsKey(typeName)) {
                final String error = new StringBuilder().append("Both messages ").append(nameToType.get(typeName))
                        .append(" and ").append(type).append(" have the same message name=").append(typeName)
                        .toString();
                throw new RuntimeException(error);
            }
            codeToType.put(typeCode, type);
            nameToType.put(typeName, type);
            messageClasses.put(type, javaType);
        }
    }

    private AllDataServerMessageType(String messageName) {
        Descriptors.Descriptor descriptor = MessageParser.getDescriptor().findMessageTypeByName(messageName);

        String typeString = MessageParser.class.getName() + "$" + descriptor.getName();
        try {
            this.javaType = (Class<? extends GeneratedMessage>) Class.forName(typeString);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        if (descriptor == null) {
            final String error = new StringBuilder().append("Cannot find descriptor for message with name=")
                    .append(messageName).toString();
            throw new IllegalArgumentException(error);
        }

        Descriptors.FieldDescriptor typeField = descriptor.findFieldByName(MESSAGE_TYPE_FIELD_NAME);
        if (typeField == null) {
            final String error = new StringBuilder().append("Cannot find field ").append(MESSAGE_TYPE_FIELD_NAME)
                    .append(" in message ").append(messageName).toString();
            throw new IllegalArgumentException(error);
        }

        int messageType;
        try {
            messageType = (Integer) typeField.getDefaultValue();
        } catch (ClassCastException e) {
            final String error = new StringBuilder().append("Wrong ").append(MESSAGE_TYPE_FIELD_NAME)
                    .append(" field type=").append(typeField.getJavaType().name())
                    .append(". Expecting ").append(Descriptors.FieldDescriptor.JavaType.INT).append(" type.").toString();
            throw new IllegalArgumentException(error, e);
        } catch (UnsupportedOperationException e) {
            throw new IllegalArgumentException(e);
        }

        if (messageType == 0) {
            final String error = new StringBuilder().append("Undefined default value for field '")
                    .append(MESSAGE_TYPE_FIELD_NAME).append("' of ").append(messageName).append(" message.").toString();
            throw new IllegalArgumentException(error);
        }

        this.name = messageName;
        this.code = messageType;
    }

    /**
     * Возвращает идентификатор типа сообщения (целое число).
     *
     * @return идентификатор типа сообщения.
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает имя (строковое обозначение) типа сообщения.
     *
     * @return имя типа сообщения.
     */
    public String getName() {
        return name;
    }

    /**
     * Возвращает {@code java class} сообщения, сгенерированного protoc.exe.
     *
     * @return {@code java class} сообщения.
     */
    public Class<? extends GeneratedMessage> getJavaType() {
        return javaType;
    }

    /**
     * Возвращает тип сообщения по заданному идентификатору.
     *
     * @param code идентификатор типа сообщения.
     * @return тип сообщения.
     */
    public static AllDataServerMessageType lookup(int code) {
        return codeToType.get(code);
    }

    /**
     * Возвращает тип сообщения по заданному имени.
     *
     * @param name имя типа сообщения.
     * @return тип сообщения.
     */
    public static AllDataServerMessageType lookup(String name) {
        return nameToType.get(name);
    }


    public static MessageLite parseMessage(AllDataServerMessageType type, MessageLite messageLite) {
        return parseMessage(type, messageLite.toByteArray());
    }

    public static MessageLite parseMessage(AllDataServerMessageType type, byte[] messageBytes) {
        Class<? extends GeneratedMessage> messageClass = getMessageClass(type);
        return (MessageLite) invokeStaticMethod(messageClass, PARSE_METHOD_NAME, messageBytes);
    }

    public static Descriptors.Descriptor getDescriptor(AllDataServerMessageType type) {
        Class<? extends GeneratedMessage> messageClass = getMessageClass(type);
        return (Descriptors.Descriptor) invokeStaticMethod(messageClass, DESCRIPTOR_METHOD_NAME);
    }

    public static GeneratedMessage.Builder getBuilder(AllDataServerMessageType type) {
        Class<? extends GeneratedMessage> messageClass = getMessageClass(type);
        return (GeneratedMessage.Builder) invokeStaticMethod(messageClass, BUILDER_METHOD_NAME);
    }

    private static Object invokeStaticMethod(Class type, String methodName, Object... args) {
        Class<?>[] argsTypes = null;
        if (args != null) {
            argsTypes = new Class<?>[args.length];
            for (int i = 0; i < args.length; i++) {
                argsTypes[i] = args[i].getClass();
            }
        }

        try {
            Method method = type.getMethod(methodName, argsTypes);
            return method.invoke(type, args);
        } catch (NoSuchMethodException e) {
            final String error = new StringBuilder().append("No such method with name ").append(methodName)
                    .append(" in ").append(type.getName()).toString();
            throw new IllegalArgumentException(error, e);
        } catch (InvocationTargetException e) {
            final String error = new StringBuilder().append("Invocation target exception. Method ").append(methodName)
                    .append(" in ").append(type.getName()).toString();
            throw new IllegalArgumentException(error, e);
        } catch (IllegalAccessException e) {
            final String error = new StringBuilder().append("Illegal access for method ").append(methodName)
                    .append(" in ").append(type.getName()).toString();
            throw new IllegalArgumentException(error, e);
        }
    }

    private static Class<? extends GeneratedMessage> getMessageClass(AllDataServerMessageType type) {
        final Class<? extends GeneratedMessage> clazz = messageClasses.get(type);
        if (clazz == null) {
            throw new IllegalArgumentException(new StringBuilder().append("There is no message class for type=")
                    .append(type).toString());
        }
        return clazz;
    }

}