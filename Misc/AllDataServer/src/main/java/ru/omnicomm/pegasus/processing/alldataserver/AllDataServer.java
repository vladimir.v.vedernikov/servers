/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processing.alldataserver;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("AllDataServerImplementation")
public class AllDataServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private Router router;
    private RandomAccessFile allDataFile = null;
    private Server self;
    private int currentSourceId;

    private boolean showDateTime = true;

    private ValueConverter valueConverter;
    private Date dat = new Date();


    /**
     * Конструктор.
     *
     * @param router сервис маршрутизатор сообщений.
     */
    @ServerConstructor
    public AllDataServer(Router router) {
        this.router = router;

    }

    @Override
    public void onMessage(Server source, MessageLite message) {

        MessageParser.MessageHeader header = null;
        final byte[] bytes = message.toByteArray();
        try {
            header = MessageParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, e.getMessage(), e);
            return;
        }

        try {
            processMessage(header, bytes);
        } catch (Exception ex) {
            LOGGER.warn("Can't process message", ex);
        } finally {
            int sourceId = header.getSourceId();
            if (sourceId != currentSourceId) {
                router.dataSourceChanged(AllDataServer.class, self, currentSourceId, sourceId);
                currentSourceId = sourceId;
            }

        }


    }

    private void processMessage(MessageParser.MessageHeader header, byte[] bytes) throws Exception {

        if (allDataFile == null) {
            //LOGGER.log(Level.INFO, String.format("Message received: %s\n%s", message.getClass().getName(), message.toString()));
            LOGGER.log(Level.WARN, "Output file is not ready");
        } else {

            AllDataServerMessageType messageType = AllDataServerMessageType.lookup(header.getMessageType());
            MessageLite specificMessage = AllDataServerMessageType.parseMessage(messageType, bytes);

//            StringBuilder sb = new StringBuilder();
//            sb
//                .append((new Date()).toString())
//                .append("\n")
//
//                .append(specificMessage.toString())
//                .append("\n")
//                ;
//
//            allDataFile.writeBytes(sb.toString());

            allDataFile.writeBytes(this.getLogMessage(specificMessage));


        }

    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(Init signal) {
                handlerInitSignal(signal);
            }

            @Override
            public void visit(CaughtException signal) {
                handleCaughtException(signal);
            }

            @Override
            public void visit(Pause signal) {
                handlePauseSignal(signal);
            }
        });
    }

    private void handlerInitSignal(Init init) {
        currentSourceId = -1;
        self = init.getSelf();

        Properties pr = new Properties();
        try {
            FileInputStream fs = new FileInputStream("config/alldataserver.properties");
            pr.load(fs);
            this.showDateTime = Boolean.parseBoolean(pr.getProperty("showDateTime", "true"));
            fs.close();

        } catch (Exception ex) {
            LOGGER.log(Level.INFO, "Can''t load properties", ex);
        }

        if (self.getId().equals("ALLDATASERVERIMPLEMENTATIONINSTANCEID")) {

            //Первый экземпляр сервера
            File outputDir = new File("allDataServerOutput");
            if (outputDir.exists()) {

                if (outputDir.isDirectory()) {
                    File[] files = outputDir.listFiles();
                    for (File file : files) {
                        if (file.isFile())
                            file.delete();
                    }
                } else {
                    outputDir.delete();
                    outputDir.mkdir();
                }
            } else {
                outputDir.mkdir();
            }
        }


        final String directory = "allDataServerOutput";
        File file = new File(directory + File.separator + self.getId() + ".txt");
        try {
            if (!file.exists()) {
                File dir = new File(directory);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                file.createNewFile();
            }

            allDataFile = new RandomAccessFile(file, "rw");
            allDataFile.setLength(0);
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, "File operation error", ex);
        }


        int[] types = new int[100];
        for (int i = 0; i < types.length; i++) {
            types[i] = i;

        }

        router.registerServer(AllDataServer.class, self, types);
    }

    private void handlePauseSignal(Pause pause) {
        try {
            pause.source().send(new Paused() {
                private static final long serialVersionUID = 1941887840224135544L;

                @Override
                public Server source() {
                    return self;
                }

                @Override
                public <E extends Throwable> void accept(Visitor<E> visitor) throws E {
                    visitor.visit(this);
                }
            });
        } catch (SendMessageException ex) {
            LOGGER.log(Level.WARN, "Can't send signal", ex);
        }
    }

    private void handleCaughtException(CaughtException signal) {
        LOGGER.log(Level.WARN, "AllData server error", signal.getException());
    }

    private String getLogMessage(MessageLite message) {

        StringBuilder text = new StringBuilder();
        boolean firstField = true;

        if (valueConverter == null) {
            valueConverter = new ValueConverter();
        }

        if (this.showDateTime) {
            // Minimize memory allocations here.
            dat.setTime(System.currentTimeMillis());
            text.append(valueConverter.convertUnixTimestamp(dat));
            firstField = false;
        }


        if (message instanceof GeneratedMessage) {
            List<Descriptors.FieldDescriptor> fields = getMessageFields(message);
            Map<Descriptors.FieldDescriptor, Object> values = ((GeneratedMessage) message).getAllFields();
            for (Descriptors.FieldDescriptor field : fields) {
                if (firstField) {
                    firstField = false;
                } else {
                    text.append(", ");
                }

                String fieldName = field.getName();
                Object value = values.get(field);
                if (value == null) {
                    // Это поле optional с неопределенным значением.
//                    if (field.hasDefaultValue()) {
//                        text.append("(").append(field.getDefaultValue()).append(")");
//                    }
                } else if ("messageType".equals(fieldName)) {
                    // Получи имя типа сообщения по его численному коду.
                    int messageType = (Integer) value;
                    String messageTypeName = AllDataServerMessageType.lookup(messageType).getName();
                    text.append(messageTypeName);
                } else if ("messageTime".equals(fieldName)) {
                    // Получим строковое представление даты.
                    int messageTime = (Integer) value;

                    text.append(valueConverter.convertMesasgeTime(messageTime));
                } else {
                    if (value instanceof List) {
                        boolean first = true;
                        for (Object element : ((List) value)) {
                            if (!first) {
                                text.append(", ");
                            }
                            text.append(element);
                            first = false;
                        }
                    } else {
                        text.append(value);
                    }
                }
            }

        } else {
            text.append(" [message is not GeneratedMessage type.]");
        }

        text.append("\n");
        return text.toString();
    }

    private List<Descriptors.FieldDescriptor> getMessageFields(MessageLite message) {
        MessageParser.MessageHeader header;
        try {
            header = MessageParser.MessageHeader.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e);
        }

        int code = header.getMessageType();
        AllDataServerMessageType messageType = AllDataServerMessageType.lookup(code);
        if (messageType == null) {
            final String error = new StringBuilder().append("Invalid message type code=").append(code).toString();
            throw new IllegalArgumentException(error);
        }
        Descriptors.Descriptor descriptor = AllDataServerMessageType.getDescriptor(messageType);
        return descriptor.getFields();
    }


}