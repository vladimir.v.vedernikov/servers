/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 14.12.11
 */
package ru.omnicomm.pegasus.processing.fasdata;

import com.google.protobuf.MessageLite;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.Executors;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.ChannelGroupFutureListener;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import ru.omnicomm.pegasus.processing.fasdata.netty.ServerPipelineFactory;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("FasDataServer_protoImplementation")
public class FasDataServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private DataStorageService dataStorageService;
    private JmsBridgeService jmsBridgeService;

    private NettyServer nettyServer = new NettyServer();

    /**
     * Конструктор.
     *
     * @param router сервис маршрутизатор сообщений.
     */
    @ServerConstructor
    public FasDataServer(Router router, DataStorageService dataStorageService, JmsBridgeService jmsBridgeService) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.jmsBridgeService = jmsBridgeService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(Init signal) {
                nettyServer.start();
            }

            @Override
            public void visit(Terminate signal) {
                nettyServer.stop();
            }

            @Override
            public void visit(CaughtException signal) {
                Throwable exception = signal.getException();
                LOGGER.log(Level.ERROR, exception.getMessage(), exception);
            }
        });
    }

    private class NettyServer {

        private ChannelGroup channels;

        private ChannelFactory channelFactory;

        private NettyServer() {
            channels = new DefaultChannelGroup("FasDataServer_proto_channel_group");
            channelFactory = new NioServerSocketChannelFactory(
                    Executors.newCachedThreadPool(), Executors.newCachedThreadPool());
        }

        public void start() {
            
            Properties pr = new Properties();
            int port = 9978;
            try {
                FileInputStream fs = new FileInputStream("config/fasdataserver.properties");
                pr.load(fs);
                port = Integer.parseInt(pr.getProperty("protobuf.protocol.port", "9978"));
                fs.close();

            } catch (Exception ex) {
                LOGGER.log(Level.INFO, "Can''t load properties", ex);
            }
            
            ServerBootstrap bootstrap = new ServerBootstrap(channelFactory);
            // Set up the event pipeline factory.
            bootstrap.setPipelineFactory(new ServerPipelineFactory(router, dataStorageService, jmsBridgeService, channels));
            // Bind and start to accept incoming connections.
            Channel serverChannel = bootstrap.bind(new InetSocketAddress(port));
            channels.add(serverChannel);
        }

        public void stop() {
            channels.close().addListener(new ChannelGroupFutureListener() {
                @Override
                public void operationComplete(ChannelGroupFuture future) throws Exception {
                    channelFactory.releaseExternalResources();
                }
            });
        }
    }

}
