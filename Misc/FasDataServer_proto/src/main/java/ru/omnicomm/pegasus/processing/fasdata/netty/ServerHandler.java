/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 14.12.11
 */
package ru.omnicomm.pegasus.processing.fasdata.netty;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import org.jboss.netty.util.ExternalResourceReleasable;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.location.SpeedAndAzimuthParser.SpeedAndAzimuth;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class ServerHandler extends IdleStateAwareChannelHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private Router router;
    private DataStorageService dataStorageService;
    private JmsBridgeService jmsBridgeService;
    private final ChannelGroup channels;

    public ServerHandler(Router router, DataStorageService dataStorageService, JmsBridgeService jmsBridgeService, ChannelGroup channels) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.jmsBridgeService = jmsBridgeService;
        this.channels = channels;
    }

    @Override
    public void handleUpstream(
            ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
        if (e instanceof ChannelStateEvent) {

            ChannelStateEvent stateEvent = (ChannelStateEvent)e;
            if(stateEvent.getValue() instanceof Boolean){
                if(!(Boolean)stateEvent.getValue()){
                    LOGGER.info("Closing connection "+e.getChannel());
//                    try{
//                        this.dataStorageService.flush();
//                    } catch (Exception ex) {
//                        LOGGER.log(Level.WARN, "",ex);
//                    }

                }
            }
            LOGGER.info("Channel Event: "+e.toString());

        }
        super.handleUpstream(ctx, e);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        MessageLite message = (MessageLite) e.getMessage();
       // LOGGER.log(Level.INFO, "INCOMING MESSAGE:\n"+message);
        router.send(message);
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.log(Level.WARN, "",ex);
        }

        try{
            MessageHeader header = MessageHeader.parseFrom(message.toByteArray());
            if(header.getMessageType()==Coordinate.getDefaultInstance().getMessageType() ||
               header.getMessageType()==SpeedAndAzimuth.getDefaultInstance().getMessageType()){
                jmsBridgeService.send(message);
            }
        } catch (JmsBridgeServiceException ex) {
            LOGGER.log(Level.WARN, "",ex);
        } catch (InvalidProtocolBufferException ex) {
            LOGGER.log(Level.WARN, "",ex);
        }
        
    }

    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) throws Exception {
        if(e.getState() == IdleState.READER_IDLE){
            LOGGER.log(Level.WARN, "Channel is closing because of idle. IDLE State is " + e.getState());
            e.getChannel().close();
        } else {
            LOGGER.log(Level.INFO, "IDLE State is " + e.getState());
        }
        
    }

    @Override
    public void exceptionCaught(
            ChannelHandlerContext ctx, ExceptionEvent e) {
        // Close the connection when an exception is raised.
        LOGGER.log(Level.WARN, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelOpen(ctx, e);

        channels.add(e.getChannel());
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelClosed(ctx, e);

        Channel closedChannel = e.getChannel();
        ChannelPipeline pipeline = closedChannel.getPipeline();
        ChannelHandler channelHandler = pipeline.get("idleHandler");
        if (channelHandler instanceof ExternalResourceReleasable) {
            ((ExternalResourceReleasable) channelHandler).releaseExternalResources();
        }
    }
}
