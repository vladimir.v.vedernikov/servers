/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 14.12.11
 */
package ru.omnicomm.pegasus.processing.fasdata.netty;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.codec.protobuf.ProtobufDecoder;
import org.jboss.netty.handler.codec.protobuf.ProtobufEncoder;
import org.jboss.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import org.jboss.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import ru.omnicomm.pegasus.messaging.common.CommonParser;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class ServerPipelineFactory implements ChannelPipelineFactory {

    private Router router;
    private DataStorageService dataStorageService;
    private JmsBridgeService jmsBridgeService;

    private ChannelGroup channels;

    public ServerPipelineFactory(Router router, DataStorageService dataStorageService, JmsBridgeService jmsBridgeService, ChannelGroup channels) {
        this.router = router;
        this.dataStorageService = dataStorageService;
        this.jmsBridgeService = jmsBridgeService;
        this.channels = channels;
    }

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = Channels.pipeline();

        pipeline.addLast("frameDecoder", new ProtobufVarint32FrameDecoder());
        pipeline.addLast("protobufDecoder", new ProtobufDecoder(CommonParser.MessageHeader.getDefaultInstance()));

        pipeline.addLast("frameEncoder", new ProtobufVarint32LengthFieldPrepender());
        pipeline.addLast("protobufEncoder", new ProtobufEncoder());

        // and then business logic.
        // Please note we create a handler for every new channel
        // because it has stateful properties.
        HashedWheelTimer timer = new HashedWheelTimer();
        pipeline.addLast("idleHandler", new IdleStateHandler(timer, 60, 60, 0));

        pipeline.addLast("handler", new ServerHandler(router,dataStorageService,jmsBridgeService,channels));

        return pipeline;
    }

}

