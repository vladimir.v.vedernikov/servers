/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 10.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import java.util.ArrayList;
import java.util.List;

import static ru.omnicomm.pegasus.base.util.MessageUtil.buildParameterValue;
import static ru.omnicomm.pegasus.base.util.MessageUtil.buildQueryItem;

/**
 * Запросный сервер для проверки пароля пользователя. Работает следующим образом:
 * 1. Пописывается в сервисе JmsBridgeService на получение запросных сообщений с типом 413.
 * 2. По приходу сообщения формирует из него запрос (SelectRequest) к платформе биснес-логики.
 * 3. Выполняет запрос к платформе биснес-логики и из ответа формирует ответное сообщение с типом 213.
 * 4. Отправляет через сервис JmsBridgeService созданное на шаге 3 сообщение.
 *
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("AutentificationRequestServer_Implementation")
public class AutentificationRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    /**
     * Тип ответного сообщения.
     */
    public static final int RESPONSE_MESSAGE_TYPE = 213;

    /**
     * Тип запросного сообщения, на который подписывается данный сервер.
     */
    public static final int REQUEST_MESSAGE_TYPE = 413;

    /**
     * Идентификатор типа сущности 'Пользователь' в объектной модели платформы бизнес-логики.
     */
    public static final long USER_ENTITY_TYPE = 1310;

    /**
     * Идентификатор атрибута 'логин' сущности 'Пользователь'.
     */
    public static final long USER_LOGIN_PARAMETER_ID = 1311;

    /**
     * Идентификатор атрибута 'пароль' сущности 'Пользователь'.
     */
    public static final long USER_PASS_PARAMETER_ID = 1312;

    /**
     * Идентификатор атрибута 'идентификатор пользователя' сущности 'Пользователь'.
     */
    public static final long USER_ID_PARAMETER_ID = 1313;

    /**
     * Имя параметра/фильтра в запросе в котором указан логин для проверки.
     */
    public static final String LOGIN_FILTER = "login";

    /**
     * Имя параметра/фильтра в запросе в котором указан пароль для проверки.
     */
    public static final String PASSWORD_FILTER = "password";

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public AutentificationRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    protected void processMessage(SendRequestParser.SendRequest request) {
        try {
            logAcceptedMessage(request);
            try {
                final String login = getLogin(request);
                final String pass = getPass(request);

                final BLSMessageParser.SelectRequest queryRequest = buildQueryRequest(login, pass);
                byte[] responseBytes = getBlService().selectByParams(queryRequest.toByteArray());
                BLSMessageParser.ResultSet responseParams = BLSMessageParser.ResultSet.parseFrom(responseBytes);

                MessageLite responseMessage = buildResponseMessage(request, login, responseParams);
                getJmsBridge().send(responseMessage);

                logProcessedMessage(request);
            } catch (BusinessLogicServiceException e) {
                logErrorMessage(request, e.getMessage(), e);
                getJmsBridge().send(getDefectiveResponse(request));
            } catch (RequestParameterException e) {
                logErrorMessage(request, e.getMessage(), e);
                getJmsBridge().send(getDefectiveResponse(request));
            } catch (InvalidProtocolBufferException e) {
                logErrorMessage(request, e.getMessage(), e);
                getJmsBridge().send(getDefectiveResponse(request));
            }
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    private String getPass(SendRequestParser.SendRequest request) {
        List<String> passwords = CommonUtil.getRequestFilterValues(PASSWORD_FILTER, request);
        if (passwords.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(PASSWORD_FILTER).append(" is required").toString();
            throw new RequestParameterException(message);
        }

        return passwords.get(0);
    }

    private String getLogin(SendRequestParser.SendRequest request) {
        List<String> logins = CommonUtil.getRequestFilterValues(LOGIN_FILTER, request);
        if (logins.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(LOGIN_FILTER).append(" is required").toString();
            throw new RequestParameterException(message);
        }
        return logins.get(0);
    }


    @Override
    protected BLSMessageParser.SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        throw new UnsupportedOperationException();
    }

    /**
     * Строит запрос к платформе бизнес-логики. Данный запрос проверяет существование переданного логина и
     * поставленного ему в соответствие хеш пароля. В ответ ожидаем получить объект класса 'Пользователь' в виде
     * списка атрибутов из которых он состоит.
     *
     * @param login логин.
     * @param pass  хеш пароля.
     * @return запрос к платформе бизнес-логики.
     */
    protected BLSMessageParser.SelectRequest buildQueryRequest(String login, String pass) {
        List<BLSMessageParser.QueryItem> items = new ArrayList<BLSMessageParser.QueryItem>();

        BLSMessageParser.ParameterValue loginValue = buildParameterValue(BLSMessageParser.ParameterValueType.STRING, login);
        BLSMessageParser.QueryItem loginItem = buildQueryItem(USER_LOGIN_PARAMETER_ID, BLSMessageParser.QueryItem.Operation.EQ, loginValue);
        items.add(loginItem);

        BLSMessageParser.ParameterValue passValue = buildParameterValue(BLSMessageParser.ParameterValueType.STRING, pass);
        BLSMessageParser.QueryItem passItem = buildQueryItem(USER_PASS_PARAMETER_ID, BLSMessageParser.QueryItem.Operation.EQ, passValue);
        items.add(passItem);

        return MessageUtil.buildSelectRequest(USER_ENTITY_TYPE, items);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        throw new UnsupportedOperationException();
    }

    protected MessageLite buildResponseMessage(SendRequestParser.SendRequest request, String login, BLSMessageParser.ResultSet response) {
        MessageParser.AutentificationResponse.Builder builder = getResponseBuilder(request);

        builder.setLogin(login);

        for (BLSMessageParser.Parameter parameter : response.getParametersList()) {
            final String attributeId = parameter.getAttributeId();
            if (Long.parseLong(attributeId) == USER_ID_PARAMETER_ID) {
                Integer userId = (Integer) extractValue(BLSMessageParser.ParameterValueType.INTEGER, parameter);
                builder.setUserId(userId.toString());
            }
        }

        return builder.build();
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.AutentificationResponse.Builder builder = getResponseBuilder(request);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.AutentificationResponse.Builder builder = getResponseBuilder(request);
        return builder.build();
    }

    private MessageParser.AutentificationResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request) {
        MessageParser.AutentificationResponse.Builder builder = MessageParser.AutentificationResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }
}
