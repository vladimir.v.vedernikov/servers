/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 06.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("GeozoneCoordinateRequestServer_Implementation")
public class GeozoneCoordinateRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 211;

    public static final int REQUEST_MESSAGE_TYPE = 411;

    public static final long GEOZONE_COORD_ENTITY_TYPE = 1110;

    public static final long GEOZONE_BIND_PARAMETER_ID = 1111;

    public static final long GEOZONE_COORD_LATITUDE_PARAMETER_ID = 1112;

    public static final long GEOZONE_COORD_LONGITUDE_PARAMETER_ID = 1113;

    public static final long GEOZONE_COORD_INDEX_PARAMETER_ID = 1114;

    public static final String GEOZONE_ID_FILTER = "geozoneId";


    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public GeozoneCoordinateRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();

        List<String> geozoneIds = CommonUtil.getRequestFilterValues(GEOZONE_ID_FILTER, request);
        if (geozoneIds.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ").append(GEOZONE_ID_FILTER)
                    .append(" is required in DriverIdRequest").toString();
            throw new RequestParameterException(message);
        }

        List<ParameterValue> filterIds = new ArrayList<ParameterValue>(geozoneIds.size());
        try {
            for (String geozoneId : geozoneIds) {
                final long idLong = Long.parseLong(geozoneId);
                final ParameterValue geozoneIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, idLong);
                filterIds.add(geozoneIdValue);
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Invalid parameter ").append(GEOZONE_ID_FILTER)
                    .append(" value").toString();
            throw new RequestParameterException(errMsg);
        }

        ValueArray filter = MessageUtil.buildValueArray(filterIds);
        ParameterValue filterValue = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, filter);
        QueryItem entityIdItem = MessageUtil.buildQueryItem(GEOZONE_BIND_PARAMETER_ID, Operation.IN, filterValue);

        items.add(entityIdItem);

        return MessageUtil.buildSelectRequest(GEOZONE_COORD_ENTITY_TYPE, items);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            MessageParser.GeozoneCoordinateResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<MessageParser.GeozoneCoordinateResponse.Row> rows = new ArrayList<MessageParser.GeozoneCoordinateResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(MessageParser.State.LAST);
                } else {
                    responseBuilder.setBlockState(MessageParser.State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(MessageParser.State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.GeozoneCoordinateResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.GeozoneCoordinateResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    private MessageParser.GeozoneCoordinateResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();
        String geozoneCoordId = Long.toString(entity.getEntityId());

        ObjectLink geozoneIdBind = getParameterValue(GEOZONE_BIND_PARAMETER_ID, ParameterValueType.OBJECT_LINK, attributes);
        String geozoneId = getObjectLinkValue(geozoneIdBind);

        Double latitude = getParameterValue(GEOZONE_COORD_LATITUDE_PARAMETER_ID, ParameterValueType.NUMBER, attributes);
        Double longitude = getParameterValue(GEOZONE_COORD_LONGITUDE_PARAMETER_ID, ParameterValueType.NUMBER, attributes);
        Integer index = getParameterValue(GEOZONE_COORD_INDEX_PARAMETER_ID, ParameterValueType.INTEGER, attributes);

        MessageParser.GeozoneCoordinateResponse.Row.Builder builder = MessageParser.GeozoneCoordinateResponse.Row.newBuilder();
        builder.setCoordinateId(geozoneCoordId);
        builder.setGeozoneId(geozoneId);
        builder.setLatitude(latitude == null ? 0.0 : latitude);
        builder.setLongitude(longitude == null ? 0.0 : longitude);
        builder.setIndex(index);

        return builder.build();
    }

    private MessageParser.GeozoneCoordinateResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.GeozoneCoordinateResponse.Builder builder = MessageParser.GeozoneCoordinateResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

    private String getObjectLinkValue(ObjectLink geozoneGroupBind) {
        StringBuilder sb = new StringBuilder();

        boolean isFirst = true;
        for (String groupId : geozoneGroupBind.getLinksList()) {
            if (isFirst) {
                isFirst = false;
            } else {
                sb.append(", ");
            }

            sb.append(groupId);
        }

        return sb.toString();
    }
}
