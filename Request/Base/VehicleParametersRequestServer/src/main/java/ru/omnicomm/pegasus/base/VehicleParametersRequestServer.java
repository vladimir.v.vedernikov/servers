/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 09.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;

import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.STRING;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.OBJECT_LINK;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.MUISTRING;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("VehicleParametersRequestServer_Implementation")
public class VehicleParametersRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 205;

    public static final int REQUEST_MESSAGE_TYPE = 405;

    public static final String VEHICLE_ID_FILTER = "vehicleId";

    public static final String START_PERIOD_FILTER = "startPeriod";

    public static final String FINISH_PERIOD_FILTER = "finishPeriod";

    public static final long VEHICLE_ENTITY_TYPE = 1000;

    public static final long VEHICLE_GARAGE_NUMBER_PARAMETER_ID = 1208;

    public static final long VEHICLE_STATE_NUMBER_PARAMETER_ID = 1207;

    public static final long VEHICLE_COLOR_PARAMETER_ID = 1206;

    public static final long VEHICLE_MODEL_PARAMETER_ID = 1204;

    public static final long VEHICLE_BRAND_PARAMETER_ID = 1205;

    public static final long VEHICLE_DRIVER_BIND_PARAMETER_ID = 1201;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public VehicleParametersRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {      
        List<QueryItem> items = new ArrayList<QueryItem>();

        QueryItem vehicleIdItem = CommonUtil.buildEntityIdQueryItem(VEHICLE_ID_FILTER, request);
        if (vehicleIdItem == null) {
            final String message = new StringBuilder().append("Parameter ").append(VEHICLE_ID_FILTER)
                    .append(" is required in VehicleParametersRequestServer").toString();
            throw new RequestParameterException(message);
        }
        items.add(vehicleIdItem);

        Double startTimeFilter = CommonUtil.getRequestFilterValue(START_PERIOD_FILTER, request);
        if (startTimeFilter == null) {
            final String message = new StringBuilder().append("Parameter ").append(START_PERIOD_FILTER)
                    .append(" is required in VehicleParametersRequestServer").toString();
            throw new RequestParameterException(message);
        }

        Double endTimeFilter = CommonUtil.getRequestFilterValue(FINISH_PERIOD_FILTER, request);
        if (endTimeFilter == null) {
            final String message = new StringBuilder().append("Parameter ").append(START_PERIOD_FILTER)
                    .append(" is required in VehicleParametersRequestServer").toString();
            throw new RequestParameterException(message);
        }

        final int startTime = startTimeFilter.intValue();
        final int endTime = endTimeFilter.intValue();
        final String localeCode = getCurrentLocale();
        return MessageUtil.buildSelectRequest(localeCode, VEHICLE_ENTITY_TYPE, items, startTime, endTime);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            MessageParser.VehicleParametersOverPeriodResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<MessageParser.VehicleParametersOverPeriodResponse.Row> rows = new ArrayList<MessageParser.VehicleParametersOverPeriodResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(MessageParser.State.LAST);
                } else {
                    responseBuilder.setBlockState(MessageParser.State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(MessageParser.State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.VehicleParametersOverPeriodResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.VehicleParametersOverPeriodResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    private MessageParser.VehicleParametersOverPeriodResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();

        String vehicleId = Long.toString(entity.getEntityId());

        String garageNumber = getParameterValue(VEHICLE_GARAGE_NUMBER_PARAMETER_ID, STRING, attributes);
        String stateNumber = getParameterValue(VEHICLE_STATE_NUMBER_PARAMETER_ID, STRING, attributes);
        String vehicleModel = getParameterValue(VEHICLE_MODEL_PARAMETER_ID, STRING, attributes);
        String vehicleBrand = getParameterValue(VEHICLE_BRAND_PARAMETER_ID, MUISTRING, attributes);
        String vehicleColor = getParameterValue(VEHICLE_COLOR_PARAMETER_ID, MUISTRING, attributes);

        Set<String> driverIds = new HashSet<String>();
        try {
            List<ObjectLink> driverIdBinds = getParameterValues(VEHICLE_DRIVER_BIND_PARAMETER_ID, OBJECT_LINK, attributes);
            for (ObjectLink driverIdBind : driverIdBinds) {
                for (String driverId : driverIdBind.getLinksList()) {
                    driverIds.add(driverId);
                }
            }
        } catch (RequestProcessingException e) {
            // возможно если не назначен ни один водитель
        }

        MessageParser.VehicleParametersOverPeriodResponse.Row.Builder builder = MessageParser.VehicleParametersOverPeriodResponse.Row.newBuilder();

        builder.setVehicleId(vehicleId);
        builder.setVehicleGarageNumber(garageNumber);
        builder.setVehicleStateNumber(stateNumber);
        builder.setVehicleModel(vehicleBrand + " " + vehicleModel);
        builder.setVehicleColor(vehicleColor);

        builder.addAllDriverId(driverIds);

        return builder.build();
    }

    private MessageParser.VehicleParametersOverPeriodResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.VehicleParametersOverPeriodResponse.Builder builder = MessageParser.VehicleParametersOverPeriodResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

}
