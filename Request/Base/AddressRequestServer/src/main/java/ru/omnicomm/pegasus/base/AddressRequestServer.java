/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 10.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("AddressRequestServer_Implementation")
public class AddressRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 208;

    public static final int REQUEST_MESSAGE_TYPE = 408;

    public static final String VEHICLE_ID_FILTER = "vehicleId";

    public static final String START_POINT_COORDINATES_FILTER = "startPointCoordinates";

    public static final String FINISH_POINT_COORDINATES_FILTER = "finishPointCoordinates";

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public AddressRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    protected void processMessage(SendRequestParser.SendRequest request) {
        try {
            try {
                setCurrentLocale(request);
                List<String> startPointCoordinates = CommonUtil.getRequestFilterValues(START_POINT_COORDINATES_FILTER, request);
                if (startPointCoordinates.isEmpty()) {
                    final String message = new StringBuilder().append("Parameter ")
                            .append(START_POINT_COORDINATES_FILTER).append(" is required").toString();
                    throw new RequestParameterException(message);
                }

                List<String> finishPointCoordinates = CommonUtil.getRequestFilterValues(FINISH_POINT_COORDINATES_FILTER, request);
                if (finishPointCoordinates.isEmpty()) {
                    final String message = new StringBuilder().append("Parameter ")
                            .append(FINISH_POINT_COORDINATES_FILTER).append(" is required").toString();
                    throw new RequestParameterException(message);
                }

                List<String> vehicleIds = CommonUtil.getRequestFilterValues(VEHICLE_ID_FILTER, request);
                if (vehicleIds.isEmpty()) {
                    final String message = new StringBuilder().append("Parameter ")
                            .append(VEHICLE_ID_FILTER).append(" is required").toString();
                    throw new RequestParameterException(message);
                }

                MessageLite responseMessage = buildResponseMessages(startPointCoordinates, finishPointCoordinates, vehicleIds.get(0), request);
                getJmsBridge().send(responseMessage);
            } catch (RequestParameterException e) {
                getJmsBridge().send(getDefectiveResponse(request));
            }
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    private MessageLite buildResponseMessages(List<String> startPointCoordinates,
                                              List<String> finishPointCoordinates,
                                              String vehicleId,
                                              SendRequestParser.SendRequest request) {
        MessageParser.AddressesResponse.Builder builder = getResponseBuilder(request, 0);

        List<MessageParser.AddressesResponse.Row> rows = new ArrayList<MessageParser.AddressesResponse.Row>();
        String address = getAddress();

        int coordCount = Math.min(startPointCoordinates.size(), finishPointCoordinates.size());
        for (int i = 0; i < coordCount; i++) {
            MessageParser.AddressesResponse.Row.Builder rowBuilder = MessageParser.AddressesResponse.Row.newBuilder();

            String startValue = startPointCoordinates.get(i);
            String finishValue = finishPointCoordinates.get(i);

            rowBuilder.setVehicleId(vehicleId);
            rowBuilder.setStartPointAddress(address);
            rowBuilder.setFinishPointAddress(address);
            rowBuilder.setStartPointCoordinates(startValue);
            rowBuilder.setFinishPointCoordinates(finishValue);
            rows.add(rowBuilder.build());
        }

        builder.setBlockState(MessageParser.State.LAST);
        builder.addAllRows(rows);
        return builder.build();
    }


    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.AddressesResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.AddressesResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        throw new UnsupportedOperationException();
    }

    private MessageParser.AddressesResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.AddressesResponse.Builder builder = MessageParser.AddressesResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

    private String getValueString(List<String> values) {
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            sb.append(value).append(",");
        }
        return sb.toString();
    }

    public String getAddress() {
        if ("ru".equalsIgnoreCase(getCurrentLocale())) {
            return "Нет данных";
        } else {
            return "No data";
        }
    }
}
