/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 06.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;

import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.ARRAY;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.INTEGER;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.LONG;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.NUMBER;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.STRING;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.MUISTRING;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.OBJECT_LINK;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("GeozoneRequestServer_Implementation")
public class GeozoneRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 210;

    public static final int REQUEST_MESSAGE_TYPE = 410;

    public static final long GEOZONE_ENTITY_TYPE = 1090;

    public static final long GEOZONE_NAME_PARAMETER_ID = 1091;

    public static final long GEOZONE_TYPE_PARAMETER_ID = 1092;

    public static final long GEOZONE_COLOR_PARAMETER_ID = 1093;

    public static final long GEOZONE_STATUS_PARAMETER_ID = 1094;

    public static final long GEOZONE_RADIUS_PARAMETER_ID = 1095;

    public static final long GEOZONE_GROUP_BIND_PARAMETER_ID = 1096;

    public static final String GEOZONE_GROUP_ID_FILTER = "geozoneGroupId";

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public GeozoneRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();

        List<String> geozoneGroupIds = CommonUtil.getRequestFilterValues(GEOZONE_GROUP_ID_FILTER, request);
        if (geozoneGroupIds.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ").append(GEOZONE_GROUP_ID_FILTER)
                    .append(" is required in DriverIdRequest").toString();
            throw new RequestParameterException(message);
        }

        List<ParameterValue> filterIds = new ArrayList<ParameterValue>(geozoneGroupIds.size());
        try {
            for (String geozoneGroupId : geozoneGroupIds) {
                final long idLong = Long.parseLong(geozoneGroupId);
                final ParameterValue idValue = MessageUtil.buildParameterValue(LONG, idLong);
                filterIds.add(idValue);
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Invalid parameter ").append(GEOZONE_GROUP_ID_FILTER)
                    .append(" value").toString();
            throw new RequestParameterException(errMsg);
        }

        ValueArray filterValue = MessageUtil.buildValueArray(filterIds);
        ParameterValue filter = MessageUtil.buildParameterValue(ARRAY, filterValue);
        QueryItem entityIdItem = MessageUtil.buildQueryItem(GEOZONE_GROUP_BIND_PARAMETER_ID, Operation.IN, filter);
        items.add(entityIdItem);

        return MessageUtil.buildSelectRequest(getCurrentLocale(), GEOZONE_ENTITY_TYPE, items);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            MessageParser.GeozoneResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<MessageParser.GeozoneResponse.Row> rows = new ArrayList<MessageParser.GeozoneResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(MessageParser.State.LAST);
                } else {
                    responseBuilder.setBlockState(MessageParser.State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(MessageParser.State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.GeozoneResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.GeozoneResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    private MessageParser.GeozoneResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();
        String geozoneId = Long.toString(entity.getEntityId());
        String geozoneName = getParameterValue(GEOZONE_NAME_PARAMETER_ID, MUISTRING, attributes);

        ObjectLink geozoneGroupBind = getParameterValue(GEOZONE_GROUP_BIND_PARAMETER_ID, OBJECT_LINK, attributes);
        String geozoneGroupId = getObjectLinkValue(geozoneGroupBind);

        String geozoneType = getParameterValue(GEOZONE_TYPE_PARAMETER_ID, STRING, attributes);

        Integer geozoneStatusInt = getParameterValue(GEOZONE_STATUS_PARAMETER_ID, INTEGER, attributes);
        String geozoneStatus = geozoneStatusInt == null ? " " : geozoneStatusInt.toString();

        String geozoneColor = getParameterValue(GEOZONE_COLOR_PARAMETER_ID, MUISTRING, attributes);

        Double geozoneRadius = getParameterValue(GEOZONE_RADIUS_PARAMETER_ID, NUMBER, attributes);

        MessageParser.GeozoneResponse.Row.Builder builder = MessageParser.GeozoneResponse.Row.newBuilder();
        builder.setGeozoneId(geozoneId);
        builder.setGeozoneName(geozoneName);
        builder.setGeozoneGroupId(geozoneGroupId);
        builder.setGeozoneType(geozoneType);
        builder.setGeozoneStatus(geozoneStatus);
        builder.setGeozoneColor(geozoneColor);
        builder.setGeozoneRadius(geozoneRadius == null ? 0.0 : geozoneRadius);

        return builder.build();
    }

    private String getObjectLinkValue(ObjectLink geozoneGroupBind) {
        StringBuilder sb = new StringBuilder();

        boolean isFirst = true;
        for (String groupId : geozoneGroupBind.getLinksList()) {
            if (isFirst) {
                isFirst = false;
            } else {
                sb.append(", ");
            }

            sb.append(groupId);
        }

        return sb.toString();
    }

    private MessageParser.GeozoneResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.GeozoneResponse.Builder builder = MessageParser.GeozoneResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

}
