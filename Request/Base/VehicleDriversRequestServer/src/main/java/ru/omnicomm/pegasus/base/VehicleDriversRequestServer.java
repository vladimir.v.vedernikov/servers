/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 11.09.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser.DriversOverPeriodResponse;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser.State;

import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("VehicleDriversRequestServer_Implementation")
public class VehicleDriversRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 214;

    public static final int REQUEST_MESSAGE_TYPE = 414;

    public static final String VEHICLE_ID_FILTER = "vehicleId";

    public static final String START_PERIOD_FILTER = "startPeriod";

    public static final String FINISH_PERIOD_FILTER = "finishPeriod";

    public static final long VEHICLE_ENTITY_TYPE = 1000;

    public static final long VEHICLE_DRIVER_BIND_PARAMETER_ID = 1201;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public VehicleDriversRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();

        QueryItem vehicleIdItem = CommonUtil.buildEntityIdQueryItem(VEHICLE_ID_FILTER, request);
        if (vehicleIdItem == null) {
            final String message = new StringBuilder().append("Parameter ").append(VEHICLE_ID_FILTER)
                    .append(" is required in VehicleDriversRequestServer").toString();
            throw new RequestParameterException(message);
        }
        items.add(vehicleIdItem);

        Double startTimeFilter = CommonUtil.getRequestFilterValue(START_PERIOD_FILTER, request);
        if (startTimeFilter == null) {
            final String message = new StringBuilder().append("Parameter ").append(START_PERIOD_FILTER)
                    .append(" is required in VehicleDriversRequestServer").toString();
            throw new RequestParameterException(message);
        }

        Double endTimeFilter = CommonUtil.getRequestFilterValue(FINISH_PERIOD_FILTER, request);
        if (endTimeFilter == null) {
            final String message = new StringBuilder().append("Parameter ").append(START_PERIOD_FILTER)
                    .append(" is required in VehicleDriversRequestServer").toString();
            throw new RequestParameterException(message);
        }

        final String localeCode = getCurrentLocale();
        final int startTime = startTimeFilter.intValue();
        final int endTime = endTimeFilter.intValue();
        return MessageUtil.buildSelectRequest(localeCode, VEHICLE_ENTITY_TYPE, items, startTime, endTime);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            DriversOverPeriodResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<DriversOverPeriodResponse.Row> rows = new ArrayList<DriversOverPeriodResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(State.LAST);
                } else {
                    responseBuilder.setBlockState(State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        DriversOverPeriodResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        DriversOverPeriodResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(State.LAST);
        return builder.build();
    }

    private DriversOverPeriodResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();

        DriversOverPeriodResponse.Row.Builder builder = DriversOverPeriodResponse.Row.newBuilder();

        String vehicleId = Long.toString(entity.getEntityId());
        builder.setVehicleId(vehicleId);

        List<Parameter> driverAttribute = attributes.get(VEHICLE_DRIVER_BIND_PARAMETER_ID);
        if (driverAttribute != null && !driverAttribute.isEmpty()) {
            List<DriversOverPeriodResponse.Driver> drivers
                    = new ArrayList<DriversOverPeriodResponse.Driver>();

            for (Parameter parameter : driverAttribute) {
                Integer startTime = parameter.hasStartTime() ? parameter.getStartTime() : null;
                Integer endTime = parameter.hasEndTime() ? parameter.getEndTime() : null;

                ObjectLink driverLiks = (ObjectLink) extractValue(ParameterValueType.OBJECT_LINK, parameter);
                for (String driverId : driverLiks.getLinksList()) {
                    DriversOverPeriodResponse.Driver.Builder driverBuilder  = DriversOverPeriodResponse.Driver.newBuilder();
                    driverBuilder.setDriverId(driverId);
                    if (startTime != null) {
                        driverBuilder.setStartDate(startTime.doubleValue());
                    }

                    if (endTime != null) {
                        driverBuilder.setFinishDate(endTime.doubleValue());
                    }

                    drivers.add(driverBuilder.build());
                }
            }

            builder.addAllDrivers(drivers);
        }

        return builder.build();
    }

    private DriversOverPeriodResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        DriversOverPeriodResponse.Builder builder = DriversOverPeriodResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

}
