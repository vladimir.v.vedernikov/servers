/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 02.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;

import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.MUISTRING;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("VehicleTypeRequestServer_Implementation")
public class VehicleTypeRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 201;

    public static final int REQUEST_MESSAGE_TYPE = 401;

    public static final long VEHICLE_TYPE_ENTITY_TYPE = 1210;

    public static final long VEHICLE_TYPE_NAME_PARAMETER_ID = 1211;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public VehicleTypeRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();
        final String localeCode = getCurrentLocale();
        return MessageUtil.buildSelectRequest(localeCode, VEHICLE_TYPE_ENTITY_TYPE, items);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {

        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            MessageParser.VehicleTypeResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<MessageParser.VehicleTypeResponse.Row> rows = new ArrayList<MessageParser.VehicleTypeResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(MessageParser.State.LAST);
                } else {
                    responseBuilder.setBlockState(MessageParser.State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(MessageParser.State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.VehicleTypeResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.VehicleTypeResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    private MessageParser.VehicleTypeResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();
        String vehicleTypeId = Long.toString(entity.getEntityId());
        String vehicleTypeName = getParameterValue(VEHICLE_TYPE_NAME_PARAMETER_ID, MUISTRING, attributes);

        MessageParser.VehicleTypeResponse.Row.Builder builder = MessageParser.VehicleTypeResponse.Row.newBuilder();
        builder.setVehicleTypeId(vehicleTypeId);
        builder.setVehicleTypeName(vehicleTypeName);
        return builder.build();
    }

    private MessageParser.VehicleTypeResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.VehicleTypeResponse.Builder builder = MessageParser.VehicleTypeResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }
}
