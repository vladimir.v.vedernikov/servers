/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 19.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.CreateTemporaryRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ObjectIdentificatorsRequestServer_Implementation")
public class ObjectIdentificatorsRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 227;

    public static final int REQUEST_MESSAGE_TYPE = 427;

    public static final String ENTITY_TYPE_FILTER = "entityType";

    public static final String ID_COUNT_FILTER = "idCount";

    public static final int MAX_ID_COUNT_VALUE = 20;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public ObjectIdentificatorsRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected void processMessage(SendRequestParser.SendRequest request) {
        try {
            try {
                long entityType = getEntityType(request);
                int idCount = getIdCount(request);

                if (idCount > MAX_ID_COUNT_VALUE) {
                    final String message = new StringBuilder().append("Invalid  ")
                            .append(ID_COUNT_FILTER).append(" parameter value=").append(idCount).toString();
                    throw new RequestParameterException(message);
                }

                List<String> identifiers = new ArrayList<String>();
                CreateTemporaryRequest createRequest = MessageUtil.buildCreateTemporaryRequest(entityType);
                for (int i = 0; i < idCount; i++) {
                    final String strId = getBlService().createEntityTemporary(createRequest.toByteArray());
                    long id = Long.parseLong(strId);
                    identifiers.add(Long.toString(id));
                }

                MessageLite response = buildResponseMessages(identifiers, request);
                getJmsBridge().send(response);
            } catch (RequestParameterException e) {
                logErrorMessage(request, e.getMessage(), e);
                getJmsBridge().send(getDefectiveResponse(request));
            } catch (BusinessLogicServiceException e) {
                logErrorMessage(request, e.getMessage(), e);
                getJmsBridge().send(getDefectiveResponse(request));
            }
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.ObjectIdentificatorsResponse.Builder builder = getResponseBuilder(request);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.ObjectIdentificatorsResponse.Builder builder = getResponseBuilder(request);
        return builder.build();
    }

    private MessageParser.ObjectIdentificatorsResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request) {
        MessageParser.ObjectIdentificatorsResponse.Builder builder = MessageParser.ObjectIdentificatorsResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

    private MessageLite buildResponseMessages(List<String> identifiers, SendRequestParser.SendRequest request) {
        MessageParser.ObjectIdentificatorsResponse.Builder builder = getResponseBuilder(request);
        builder.addAllEntityId(identifiers);
        return builder.build();
    }

    private long getEntityType(SendRequestParser.SendRequest request) {
        String entityType = CommonUtil.getRequestFilterValue(ENTITY_TYPE_FILTER, request);
        if (entityType == null) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(ENTITY_TYPE_FILTER).append(" is required").toString();
            throw new RequestParameterException(message);
        }
        try {
            return Long.parseLong(entityType);
        } catch (NumberFormatException e) {
            throw new RequestParameterException("Invalid entityType value=" + entityType, e);
        }
    }

    private int getIdCount(SendRequestParser.SendRequest request) {
        Integer idCount = CommonUtil.getRequestFilterValue(ID_COUNT_FILTER, request);
        if (idCount == null) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(ID_COUNT_FILTER).append(" is required").toString();
            throw new RequestParameterException(message);
        }

        return idCount;
    }
}
