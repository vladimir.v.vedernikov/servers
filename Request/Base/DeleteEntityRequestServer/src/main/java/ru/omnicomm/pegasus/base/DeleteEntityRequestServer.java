/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 13.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.request.MessageParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DeleteEntityRequestServer_Implementation")
public class DeleteEntityRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    private JmsBridgeService jmsBridge;

    private BusinessLogicService blService;

    private Server self;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public DeleteEntityRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.jmsBridge = jmsBridge;
        this.blService = blService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        MessageParser.DeleteActionRequest deleteRequest;
        try {
            deleteRequest = MessageParser.DeleteActionRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }

        try {
            blService.startTransaction();
            try {
                MessageLite response = processRequest(deleteRequest);
                blService.commitTransaction();
                jmsBridge.send(response);
            } catch (RequestParameterException e) {
                LOGGER.log(Level.WARN, "Invalid request parameters", e);
                blService.abortTransaction();
            } catch (RequestProcessingException e) {
                final String errMsg = "Error while processing request.";
                LOGGER.log(Level.INFO, errMsg, e);
                blService.abortTransaction();

                final long entityId = getEntityId(deleteRequest);
                final MessageLite notification = buildNotification(entityId, errMsg + " " + e.getMessage());
                jmsBridge.send(notification);
            }
        } catch (BusinessLogicServiceException e) {
            LOGGER.log(Level.INFO, null, e);
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                final int messageType = MessageParser.DeleteActionRequest.getDefaultInstance().getMessageType();
                jmsBridge.registerServer(DeleteEntityRequestServer.class, self, messageType);
            }

            @Override
            public void visit(Terminate signal) {
                jmsBridge.unregisterServer(DeleteEntityRequestServer.class, self);
            }
        });
    }

    private MessageParser.ObjectUpdateActionsResultNotification buildNotification(Long entityId, String errorMessage) {
        MessageParser.ObjectUpdateActionsResultNotification.Builder builder
                = MessageParser.ObjectUpdateActionsResultNotification.newBuilder();
        builder.setMessageType(MessageParser.ObjectUpdateActionsResultNotification.getDefaultInstance().getMessageType());

        if (entityId != null) {
            builder.setEntityId(Long.toString(entityId));
        }

        if (errorMessage != null) {
            builder.setErrorMessage(errorMessage);
        }

        return builder.build();
    }

    private MessageLite processRequest(MessageParser.DeleteActionRequest deleteRequest) {
        long entityType = getEntityType(deleteRequest);
        long entityId = getEntityId(deleteRequest);

        try {
            BLSMessageParser.DeleteRequest request = MessageUtil.buildDeleteRequest(entityType, entityId);
            boolean deleted = blService.deleteEntity(request.toByteArray());
            if (deleted) {
                return buildNotification(entityId, null);
            } else {
                return buildNotification(entityId, "Cannot delete object with entityId=" + entityId);
            }
        } catch (BusinessLogicServiceException e) {
            throw new RequestProcessingException(e);
        }
    }

    private long getEntityId(MessageParser.DeleteActionRequest deleteRequest) {
        String strValue = deleteRequest.getEntityId();
        if (strValue == null) {
            throw new RequestParameterException("Entity id isn't defined");
        }

        try {
            return Long.parseLong(strValue);
        } catch (NumberFormatException e) {
            throw new RequestParameterException("Invalid entity type value=" + strValue, e);
        }
    }

    private long getEntityType(MessageParser.DeleteActionRequest deleteRequest) {
        String strValue = deleteRequest.getEntityType();
        if (strValue == null) {
            throw new RequestParameterException("Entity type isn't defined");
        }
        try {
            return Long.parseLong(strValue);
        } catch (NumberFormatException e) {
            throw new RequestParameterException("Invalid entity type value=" + strValue, e);
        }
    }


}
