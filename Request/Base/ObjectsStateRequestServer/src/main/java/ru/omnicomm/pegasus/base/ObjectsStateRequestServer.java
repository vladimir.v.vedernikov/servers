/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 10.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;

import java.util.List;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ObjectsStateRequestServer_Implementation")
public class ObjectsStateRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 212;

    public static final int REQUEST_MESSAGE_TYPE = 412;

    public static final String STATE_ID_FILTER = "stateId";


    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public ObjectsStateRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    protected void processMessage(SendRequestParser.SendRequest request) {
        try {
            try {
                List<String> stateIds = CommonUtil.getRequestFilterValues(STATE_ID_FILTER, request);
                if (stateIds.isEmpty()) {
                    final String message = new StringBuilder().append("Parameter ")
                            .append(STATE_ID_FILTER).append(" is required").toString();
                    throw new RequestParameterException(message);
                }

                MessageLite responseMessage = buildResponseMessages(request);
                getJmsBridge().send(responseMessage);
            } catch (RequestParameterException e) {
                getJmsBridge().send(getDefectiveResponse(request));
            }
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.ObjectsStateResponse.Builder builder = getResponseBuilder(request);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.ObjectsStateResponse.Builder builder = getResponseBuilder(request);
        return builder.build();
    }

    private MessageParser.ObjectsStateResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request) {
        MessageParser.ObjectsStateResponse.Builder builder = MessageParser.ObjectsStateResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

    private MessageLite buildResponseMessages(SendRequestParser.SendRequest request) {
        MessageParser.ObjectsStateResponse.Builder builder = getResponseBuilder(request);
        builder.setPercent(50.0);
        return builder.build();
    }
}
