/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 10.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;

import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ru.omnicomm.pegasus.base.util.MessageUtil.buildParameterValue;
import static ru.omnicomm.pegasus.base.util.MessageUtil.buildQueryItem;
import static ru.omnicomm.pegasus.base.util.MessageUtil.buildValueArray;

import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.STRING;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.MUISTRING;
import static ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType.OBJECT_LINK;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("VehicleRequestServer_Implementation")
public class VehicleRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 204;

    public static final int REQUEST_MESSAGE_TYPE = 404;

    public static final String VEHICLE_ID_FILTER = "vehicleId";

    public static final String VEHICLE_TYPE_ID_FILTER = "vehicleTypeId";

    public static final String VEHICLE_GROUP_ID_FILTER = "vehicleGroupId";

    public static final String VEHICLE_STATE_NUMBER_MASK_FILTER = "vehicleStateNumberMask";

    public static final String VEHICLE_GARAGE_NUMBER_FILTER = "vehicleGarageNumber";

    public static final String VEHICLE_REGION_ID_FILTER = "regionId";

    public static final String DRIVER_MASK_FILTER = "driverMask"; //todo temporary


    public static final long VEHICLE_ENTITY_TYPE = 1000;

    public static final long DRIVER_ENTITY_TYPE = 1080; //todo temporary

    public static final long VEHICLE_GARAGE_NUMBER_PARAMETER_ID = 1208;

    public static final long VEHICLE_STATE_NUMBER_PARAMETER_ID = 1207;

    public static final long VEHICLE_COLOR_PARAMETER_ID = 1206;

    public static final long VEHICLE_MODEL_PARAMETER_ID = 1204;

    public static final long VEHICLE_DRIVER_BIND_PARAMETER_ID = 1201;

    public static final long VEHICLE_REGION_BIND_PARAMETER_ID = 1203;

    public static final long VEHICLE_TYPE_BIND_PARAMETER_ID = 1202;

    public static final long VEHICLE_GROUP_BIND_PARAMETER_ID = 1209;

    public static final long DRIVER_NAME_PARAMETER_ID = 1082; //todo temporary


    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public VehicleRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();

        QueryItem vehicleIdItem = CommonUtil.buildEntityIdQueryItem(VEHICLE_ID_FILTER, request);
        if (vehicleIdItem != null) {
            items.add(vehicleIdItem);
        }

        addVehicleTypeIdItem(request, items);
        addVehicleGroupIdItem(request, items);
        addDriverMaskItem(request, items);
        addVehicleStateNumberMaskItem(request, items);
        addVehicleGarageNumberItem(request, items);
        addRegionIdItem(request, items);

        return MessageUtil.buildSelectRequest(getCurrentLocale(), VEHICLE_ENTITY_TYPE, items);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            MessageParser.FilteredVehicleResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<MessageParser.FilteredVehicleResponse.Row> rows = new ArrayList<MessageParser.FilteredVehicleResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(MessageParser.State.LAST);
                } else {
                    responseBuilder.setBlockState(MessageParser.State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(MessageParser.State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.FilteredVehicleResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.FilteredVehicleResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    private MessageParser.FilteredVehicleResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();

        String vehicleId = Long.toString(entity.getEntityId());

//        VEHICLE_GARAGE_NUMBER_PARAMETER_ID
        String garageNumber = getParameterValue(VEHICLE_GARAGE_NUMBER_PARAMETER_ID, STRING, attributes);
        String stateNumber = getParameterValue(VEHICLE_STATE_NUMBER_PARAMETER_ID, STRING, attributes);
        String vehicleModel = getParameterValue(VEHICLE_MODEL_PARAMETER_ID, STRING, attributes);
        String vehicleColor = getParameterValue(VEHICLE_COLOR_PARAMETER_ID, MUISTRING, attributes);

        // Тип ТС
        ObjectLink typeId = getParameterValue(VEHICLE_TYPE_BIND_PARAMETER_ID, OBJECT_LINK, attributes);
        // Регион
        ObjectLink regionId = getParameterValue(VEHICLE_REGION_BIND_PARAMETER_ID, OBJECT_LINK, attributes);

        ObjectLink driver = null;
        try {
            driver = getParameterValue(VEHICLE_DRIVER_BIND_PARAMETER_ID, OBJECT_LINK, attributes);
        } catch (RequestProcessingException e) {
            // возможно если не назначен ни один водитель
        }

        ObjectLink groupId = null;
        try {
            groupId = getParameterValue(VEHICLE_GROUP_BIND_PARAMETER_ID, OBJECT_LINK, attributes);
        } catch (RequestProcessingException e) {
            // возможно не принадлежит ни одной группе
        }

        MessageParser.FilteredVehicleResponse.Row.Builder builder = MessageParser.FilteredVehicleResponse.Row.newBuilder();

        builder.setVehicleId(vehicleId);
        builder.setVehicleGarageNumber(garageNumber);
        builder.setVehicleStateNumber(stateNumber);
        builder.setVehicleModel(vehicleModel);
        builder.setVehicleColor(vehicleColor);

        List<String> typeIds = typeId.getLinksList();
        if (!typeIds.isEmpty()) {
            builder.setVehicleTypeId(typeIds.get(0));
        } else {
            builder.setVehicleTypeId("<undefined>");
        }

        List<String> regionIds = regionId.getLinksList();
        if (!regionIds.isEmpty()) {
            builder.setRegionId(regionIds.get(0));
        } else {
            builder.setRegionId("<undefined>");
        }

        if (driver != null) {
            List<String> driverIds = driver.getLinksList();
            if (!driverIds.isEmpty()) {
                builder.setDriverId(driverIds.get(0));
            }
        }

        if (groupId != null) {
            List<String> groupIds = groupId.getLinksList();
            List<String> groupIdStrings = new ArrayList<String>();
            for (String id : groupIds) {
                groupIdStrings.add(id);
            }

            builder.addAllVehicleGroupId(groupIdStrings);
        }

        return builder.build();
    }

    private MessageParser.FilteredVehicleResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.FilteredVehicleResponse.Builder builder = MessageParser.FilteredVehicleResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }


    private void addVehicleTypeIdItem(SendRequestParser.SendRequest request, List<QueryItem> items) {
        String filterValue = CommonUtil.getRequestFilterValue(VEHICLE_TYPE_ID_FILTER, request);
        if (filterValue == null) {
            return;
        }

        try {
            Long vehicleTypeId = Long.parseLong(filterValue);
            ParameterValue vehicleTypeValue = buildParameterValue(ParameterValueType.LONG, vehicleTypeId);
            QueryItem entityIdItem = buildQueryItem(VEHICLE_TYPE_BIND_PARAMETER_ID, Operation.EQ, vehicleTypeValue);
            items.add(entityIdItem);
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Invalid parameter ").append(VEHICLE_TYPE_ID_FILTER)
                    .append(" value=").append(filterValue).toString();
            throw new RequestParameterException(errMsg);
        }
    }

    private void addVehicleGroupIdItem(SendRequestParser.SendRequest request, List<QueryItem> items) {
        List<String> vehicleGroupsIds = CommonUtil.getRequestFilterValues(VEHICLE_GROUP_ID_FILTER, request);
        if (vehicleGroupsIds.isEmpty()) {
            return;
        }

        List<ParameterValue> parameterValues = new ArrayList<ParameterValue>(vehicleGroupsIds.size());
        try {
            for (String vehicleGroupsId : vehicleGroupsIds) {
                final long longFilterValue = Long.parseLong(vehicleGroupsId);
                parameterValues.add(buildParameterValue(ParameterValueType.LONG, longFilterValue));
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Invalid parameter ").append(VEHICLE_GROUP_ID_FILTER)
                    .append(" value").toString();
            throw new RequestParameterException(errMsg);
        }

        ValueArray valueArray = MessageUtil.buildValueArray(parameterValues);
        ParameterValue filter = buildParameterValue(ParameterValueType.ARRAY, valueArray);

        QueryItem entityIdItem = buildQueryItem(VEHICLE_GROUP_BIND_PARAMETER_ID, Operation.IN, filter);
        items.add(entityIdItem);
    }

    private void addVehicleStateNumberMaskItem(SendRequestParser.SendRequest request, List<QueryItem> items) {
        String mask = CommonUtil.getRequestFilterValue(VEHICLE_STATE_NUMBER_MASK_FILTER, request);
        if (mask == null) {
            return;
        }

        final ParameterValue filterValue = buildParameterValue(ParameterValueType.STRING, mask);
        QueryItem nameItem = buildQueryItem(VEHICLE_STATE_NUMBER_PARAMETER_ID, Operation.LIKE, filterValue);
        items.add(nameItem);
    }

    private void addVehicleGarageNumberItem(SendRequestParser.SendRequest request, List<QueryItem> items) {
        String garageNumber = CommonUtil.getRequestFilterValue(VEHICLE_GARAGE_NUMBER_FILTER, request);
        if (garageNumber == null) {
            return;
        }

        ParameterValue filterValue = buildParameterValue(ParameterValueType.STRING, garageNumber);
        QueryItem entityIdItem = buildQueryItem(VEHICLE_GARAGE_NUMBER_PARAMETER_ID, Operation.EQ, filterValue);
        items.add(entityIdItem);
    }

    private void addRegionIdItem(SendRequestParser.SendRequest request, List<QueryItem> items) {
        List<String> vehicleRegionIds = CommonUtil.getRequestFilterValues(VEHICLE_REGION_ID_FILTER, request);
        if (vehicleRegionIds.isEmpty()) {
            return;
        }

        List<ParameterValue> parameterValues = new ArrayList<ParameterValue>(vehicleRegionIds.size());
        try {
            for (String vehicleRegionId : vehicleRegionIds) {
                final long longFilterValue = Long.parseLong(vehicleRegionId);
                parameterValues.add(buildParameterValue(ParameterValueType.LONG, longFilterValue));
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Invalid parameter ").append(VEHICLE_REGION_ID_FILTER)
                    .append(" value").toString();
            throw new RequestParameterException(errMsg);
        }
        ValueArray valueArray = buildValueArray(parameterValues);
        ParameterValue filter = buildParameterValue(ParameterValueType.ARRAY, valueArray);

        QueryItem entityIdItem = buildQueryItem(VEHICLE_REGION_BIND_PARAMETER_ID, Operation.IN, filter);
        items.add(entityIdItem);
    }

    private void addDriverMaskItem(SendRequestParser.SendRequest request, List<QueryItem> items) {
        SelectRequest queryRequest = buildDriverByMaskRequest(request);
        if (queryRequest == null) {
            return;
        }

        try {

            byte[] resultSetBytes = getBlService().selectByParams(queryRequest.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultSetBytes);
//            ResultSet resultSet = getBlService().selectByParams(queryRequest.toByteArray());
            List<Parameter> parameters = resultSet.getParametersList();

            Set<Long> driverIds = getFilteredDriverIds(parameters);
            if (!driverIds.isEmpty()) {
                List<ParameterValue> parameterValues = new ArrayList<ParameterValue>(driverIds.size());
                for (Long driverId : driverIds) {
                    parameterValues.add(buildParameterValue(ParameterValueType.LONG, driverId));
                }

                ValueArray valueArray = buildValueArray(parameterValues);
                ParameterValue filter = buildParameterValue(ParameterValueType.ARRAY, valueArray);
                QueryItem driverIdItem = buildQueryItem(VEHICLE_DRIVER_BIND_PARAMETER_ID, Operation.IN, filter);
                items.add(driverIdItem);
            } else {
                // todo Очень вредная заглушка, делается для того чтобы при отсутствии водителей по маске не выдать все
                // todo тс. Нужно убрать в след версиях ОБЯЗАТЕЛЬНО!
                ParameterValue filterValue = buildParameterValue(ParameterValueType.LONG, -1L);
                QueryItem driverIdItem = buildQueryItem(VEHICLE_DRIVER_BIND_PARAMETER_ID, Operation.EQ, filterValue);
                items.add(driverIdItem);
            }
        } catch (BusinessLogicServiceException e) {
            throw new RequestProcessingException("Cannot select driver id by mask", e);
        } catch (InvalidProtocolBufferException e) {
            throw new RequestProcessingException("Cannot parse result set from BasePlatform", e);
        }
    }

    private Set<Long> getFilteredDriverIds(List<Parameter> parameters) {
        Set<Long> driverIds = new HashSet<Long>();
        for (Parameter parameter : parameters) {
            driverIds.add(Long.parseLong(parameter.getEntityId()));
        }
        return driverIds;
    }

    private SelectRequest buildDriverByMaskRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();

        String mask = CommonUtil.getRequestFilterValue(DRIVER_MASK_FILTER, request);
        if (mask == null) {
            return null;
        }

        ParameterValue maskValue = buildParameterValue(ParameterValueType.STRING, mask);
        QueryItem nameMaskItem = buildQueryItem(DRIVER_NAME_PARAMETER_ID, Operation.LIKE, maskValue);
        items.add(nameMaskItem);

        return MessageUtil.buildSelectRequest(getCurrentLocale(), DRIVER_ENTITY_TYPE, items);
    }
}
