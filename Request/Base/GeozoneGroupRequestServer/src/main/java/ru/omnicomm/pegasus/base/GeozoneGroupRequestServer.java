/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 06.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.MessageUtil;
import ru.omnicomm.pegasus.messaging.base.response.MessageParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("GeozoneGroupRequestServer_Implementation")
public class GeozoneGroupRequestServer extends AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 209;

    public static final int REQUEST_MESSAGE_TYPE = 409;

    public static final long GEOZONE_GROUP_ENTITY_TYPE = 1100;

    public static final long GEOZONE_GROUP_NAME_PARAMETER_ID = 1101;

    public static final long GEOZONE_GROUP_PARENT_ID_PARAMETER_ID = 1102;

    public static final String GEOZONE_PARENT_ID_FILTER = "geozoneParentId";

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public GeozoneGroupRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }


    @Override
    protected SelectRequest buildQueryRequest(SendRequestParser.SendRequest request) {
        List<QueryItem> items = new ArrayList<QueryItem>();

        String groupParentId = CommonUtil.getRequestFilterValue(GEOZONE_PARENT_ID_FILTER, request);
        if (groupParentId == null) {
            final String message = new StringBuilder().append("Parameter ").append(GEOZONE_PARENT_ID_FILTER)
                    .append(" is required in DriverIdRequest").toString();
            throw new RequestParameterException(message);
        }

        Long parentId;
        try {
            parentId = Long.parseLong(groupParentId);
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Invalid parameter ").append(GEOZONE_PARENT_ID_FILTER)
                    .append(" value=").append(groupParentId).toString();
            throw new RequestParameterException(errMsg);
        }

        ParameterValue filter = MessageUtil.buildParameterValue(ParameterValueType.LONG, parentId);
        QueryItem entityIdItem = MessageUtil.buildQueryItem(GEOZONE_GROUP_PARENT_ID_PARAMETER_ID, Operation.EQ, filter);
        items.add(entityIdItem);

        return MessageUtil.buildSelectRequest(getCurrentLocale(), GEOZONE_GROUP_ENTITY_TYPE, items);
    }

    @Override
    protected int getResponseMessageType() {
        return RESPONSE_MESSAGE_TYPE;
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    @Override
    protected List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities) {
        int blockNumber = 0;
        List<MessageLite> responses = new ArrayList<MessageLite>();
        int entitySize = responseEntities.size(); // количество блоков.
        for (List<ResponseEntry> responseEntity : responseEntities) {
            MessageParser.GeozoneGroupResponse.Builder responseBuilder = getResponseBuilder(request, blockNumber);

            List<MessageParser.GeozoneGroupResponse.Row> rows = new ArrayList<MessageParser.GeozoneGroupResponse.Row>();
            try {
                for (ResponseEntry entry : responseEntity) {
                    rows.add(getRow(entry));
                }

                if ((blockNumber + 1) == entitySize) {
                    responseBuilder.setBlockState(MessageParser.State.LAST);
                } else {
                    responseBuilder.setBlockState(MessageParser.State.NORMAL);
                }
            } catch (RequestProcessingException e) {
                LOGGER.log(Level.INFO, "Error while processing request (instanceId=" + request.getInstanceId() + ")", e);
                responseBuilder.setBlockState(MessageParser.State.DEFECTIVE);
            }
            responseBuilder.addAllRows(rows);
            responses.add(responseBuilder.build());

            blockNumber++;
        }

        return responses;
    }

    @Override
    protected MessageLite getDefectiveResponse(SendRequestParser.SendRequest request) {
        MessageParser.GeozoneGroupResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.DEFECTIVE);
        return builder.build();
    }

    @Override
    protected MessageLite getEmptyResponse(SendRequestParser.SendRequest request) {
        MessageParser.GeozoneGroupResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(MessageParser.State.LAST);
        return builder.build();
    }

    private MessageParser.GeozoneGroupResponse.Row getRow(ResponseEntry entity) {
        Map<Long, List<Parameter>> attributes = entity.getAttributes();
        String geozoneGroupId = Long.toString(entity.getEntityId());
        String geozoneGroupName = getParameterValue(GEOZONE_GROUP_NAME_PARAMETER_ID, ParameterValueType.MUISTRING, attributes);

        MessageParser.GeozoneGroupResponse.Row.Builder builder = MessageParser.GeozoneGroupResponse.Row.newBuilder();
        builder.setGeozoneGroupId(geozoneGroupId);
        builder.setGeozoneGroupName(geozoneGroupName);
        return builder.build();
    }

    private MessageParser.GeozoneGroupResponse.Builder getResponseBuilder(SendRequestParser.SendRequest request, int blockNumber) {
        MessageParser.GeozoneGroupResponse.Builder builder = MessageParser.GeozoneGroupResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setTimeStamp(System.currentTimeMillis());
        builder.setBlockNumber(blockNumber);
        builder.setSourceId(0);
        builder.setMessageTime(0);
        return builder;
    }

}
