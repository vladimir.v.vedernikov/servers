/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev Date: 19.06.12
 */
package ru.omnicomm.pegasus.processing;

import com.ascatel.asymbix.rendering.platform.messages.dao.EmptyMessageParser;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.UnknownFieldSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public final class CommonUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    /**
     * Группирует параметры по их entityId. Возвращает карту где ключ - идентификатор сущности, значение - набор
     * параметров сущности.
     *
     * @param parameters список параметров.
     * @return карта сгруппированных по сущностям параметров.
     */
    public static Map<Long, Map<Long, List<Parameter>>> groupParameters(List<Parameter> parameters) {
        Map<Long, Map<Long, List<Parameter>>> result = new HashMap<>();
        for (Parameter parameter : parameters) {
            Long entityId = Long.valueOf(parameter.getEntityId());
            Long attributeId = Long.valueOf(parameter.getAttributeId());
            // Коллекция отношений идентификаторов атрибутов к списку самих атрибутов.
            // Может быть несколько атрибутов с одинаковым attrId т.к. в ответе на запрос могут передаваться
            // исторические данные.
            Map<Long, List<Parameter>> entity = result.get(entityId);
            if (entity == null) {
                entity = new HashMap<>();
                result.put(Long.valueOf(entityId), entity);
            }
            // Список параметров.
            List<Parameter> sameAttrIdParams = entity.get(attributeId);
            if (sameAttrIdParams == null) {
                sameAttrIdParams = new ArrayList<>();
                entity.put(Long.valueOf(attributeId), sameAttrIdParams);
            }
            sameAttrIdParams.add(parameter);
        }
        return result;
    }

    /**
     * Обрабатывает данные, полученные от сервиса бизнеслогики, парсером пустых сообщений. Выводит в лог содержимое.
     *
     * @param byteArray - массив данных
     * @throws InvalidProtocolBufferException - ошибка обработки
     */
    public static void logResponseMessage(byte[] byteArray) throws InvalidProtocolBufferException {
        EmptyMessageParser.EmptyMessage inputMessage = EmptyMessageParser.EmptyMessage.parseFrom(byteArray);
        //Читаем все поля сообщения
        Map<Integer, UnknownFieldSet.Field> currentFields = inputMessage.getUnknownFields().asMap();
        for (int index : currentFields.keySet()) {
            LOGGER.info("COServer: UnknownFieldIndex - " + index);
            UnknownFieldSet.Field field = currentFields.get(index);
            if (field != null) {
                for (Long id : field.getFixed64List()) {
                    LOGGER.info("COServer: UnknownField Fixed64List - " + id);
                }
                for (Integer id : field.getFixed32List()) {
                    LOGGER.info("COServer: UnknownField Fixed32List - " + id);
                }
                for (Long id : field.getVarintList()) {
                    LOGGER.info("COServer: UnknownField VarintList - " + id);
                }
            }
            else {
                LOGGER.info("COServer: field null for - " + index);
            }
            LOGGER.info("COServer: UnknownFieldIndex ended");
        }
    }

    private CommonUtil() {
    }
}
