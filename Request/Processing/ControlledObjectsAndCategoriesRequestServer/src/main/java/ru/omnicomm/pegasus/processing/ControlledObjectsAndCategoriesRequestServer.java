/*
 * ru.omnicomm.pegasus.processing.ControlledObjectsAndCategoriesRequestServer
 * 
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * For internal use only.
 * 
 * Author: Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 * Version: 
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.*;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

/**
 *
 * @author Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ControlledObjectsAndCategoriesRequestServerImplementation")
public class ControlledObjectsAndCategoriesRequestServer implements ServerImplementation {

    private static final int REQUEST_MESSAGE_TYPE = 449;
    private static final int RESPONSE_MESSAGE_TYPE = 450;
    private static final int IN_BLOCK_MESSAGES_LIMIT = 100;
    private static final long RANGE_PARAMETER_ID = 72;
    private static final long CO_CLASS_ID = 1800;
    private static final long NAME_ATTRIBUTE_ID = 1801;
    private static final long BIND_CAT_PARAMETER_ID = 1802;
    private static final long CATEGORY_CLASS_ID = 1870;
    private static final long BIND_CO_ATTRIBUTE_ID = 1872;
    private static final Logger logger = LoggerFactory.getLogger();
    private Server self;
    private JmsBridgeService jmsBridge;
    private BusinessLogicService blService;

    @ServerConstructor
    public ControlledObjectsAndCategoriesRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.jmsBridge = jmsBridge;
        this.blService = blService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        BaseResponseParser.GetCOandCATRequest request;
        try {
            request = BaseResponseParser.GetCOandCATRequest.parseFrom(message.toByteArray());
        }
        catch (InvalidProtocolBufferException ex) {
            logger.error("Invalid message.", ex);
            return;
        }
        processMessage(request);
    }

    @Override
    public void onSignal(Signal signal) {
        final Class<?> cls = this.getClass();
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) throws RuntimeException {
                logger.error("error in " + cls.getName(), signal.getException());
            }

            @Override
            public void visit(Init signal) throws RuntimeException {
                self = signal.getSelf();
                jmsBridge.registerServer(cls, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) throws RuntimeException {
                jmsBridge.unregisterServer(cls, self);
            }
        });
    }

    @SuppressWarnings("UseSpecificCatch")
    private void processMessage(BaseResponseParser.GetCOandCATRequest request) {
        try {
            SelectRequest selectRequest = buildCOQueryRequest(request);
            logger.info("CO&CATRequestServer get selectCORequest" + selectRequest);
            byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
            logger.info("CO&CATRequestServer get responseBytes - " + (responseBytes != null));
            CommonUtil.logResponseMessage(responseBytes);
            ResultSet response = ResultSet.parseFrom(responseBytes);
            logger.info("CO&CATRequestServer get resultSet" + response);
            Map<Long, Map<Long, List<Parameter>>> coResponse = CommonUtil.groupParameters(response.getParametersList());
            Map<Long, Map<Long, List<Parameter>>> catResponse = null;
            if (!coResponse.isEmpty()) {
                Set<Long> coList = coResponse.keySet();
                selectRequest = buildCATQueryRequest(coList);
                logger.info("CO&CATRequestServer get selectCATRequest" + selectRequest);
                responseBytes = blService.selectByParams(selectRequest.toByteArray());
                logger.info("CO&CATRequestServer get responseBytes - " + (responseBytes != null));
                CommonUtil.logResponseMessage(responseBytes);
                response = ResultSet.parseFrom(responseBytes);
                logger.info("CO&CATRequestServer get resultSet" + response);
                catResponse = CommonUtil.groupParameters(response.getParametersList());
            }
            List<MessageLite> responseMessages = buildResponseMessages(request, coResponse, catResponse);
            if (responseMessages.isEmpty()) {
                sendJmsMessage(getEmptyResponse(request));
            }
            else {
                for (MessageLite responseMessage : responseMessages) {
                    sendJmsMessage(responseMessage);
                }
            }
        }
        catch (Exception ex) {
            logger.error("error", ex);
            try {
                sendJmsMessage(getDefectiveResponse(request));
            }
            catch (JmsBridgeServiceException ex1) {
                logger.error("Cannot send response message to jmsBridge", ex1);
            }
        }
    }

    private void sendJmsMessage(MessageLite message) throws JmsBridgeServiceException {
        logger.info("send jms message" + message);
        jmsBridge.send(message);
    }

    private SelectRequest buildCOQueryRequest(BaseResponseParser.GetCOandCATRequest request) throws
            BusinessLogicServiceException, InvalidProtocolBufferException {
        List<QueryItem> items = new ArrayList<>();
        for (BaseResponseParser.GetCOandCATRequest.QueryItem item : request.getCriteriaListList()) {
            ClassAttribute classAttr = getQueryClassAttribute(item.getAttributeId());
            if (classAttr != null) {
                processClassAttribute(classAttr, items, item);
            }
        }
        SelectRequest.Builder builder = SelectRequest.newBuilder();
        SelectRequest selectRequest = builder.setMessageType(SELECT_REQUEST_MESSAGE_TYPE).setEntityType(String.valueOf(CO_CLASS_ID)).addAllFilter(items).build();
        return selectRequest;
    }

    private SelectRequest buildCATQueryRequest(Set<Long> coList) {
        List<QueryItem> items = new ArrayList<>();
        if (!coList.isEmpty()) {
            ValueArray.Builder ids = ValueArray.newBuilder();
            for (Long id : coList) {
                ids.addValues(ParameterValue.newBuilder().setLongValue(id).build());
            }
            ParameterValue value = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, ids.build());
            QueryItem coIdListItem = MessageUtil.buildQueryItem(Long.toString(BIND_CO_ATTRIBUTE_ID), Operation.IN, value);
            items.add(coIdListItem);
        }
        SelectRequest.Builder builder = SelectRequest.newBuilder().setEntityType(String.valueOf(CATEGORY_CLASS_ID)).addAllFilter(items);
        return builder.build();
    }

    private void addQueryItem(ClassAttribute classAttr, BaseResponseParser.GetCOandCATRequest.QueryItem item, List<QueryItem> items) {
        String attributeId = String.valueOf(item.getAttributeId());
        String value = item.getValue();
        String locale = item.getLocale();
        switch (classAttr.getAttrType()) {
            case 1:
                ParameterValue parameterValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, value + '%');
                QueryItem queryItem = MessageUtil.buildQueryItem(attributeId, Operation.LIKE, parameterValue);
                items.add(queryItem);
                break;
            case 2:
                if (item.getValue() != null) {
                    try {
                        Double doubleValue = Double.parseDouble(value);
                        ParameterValue doubleQueryValue = MessageUtil.buildParameterValue(ParameterValueType.NUMBER, doubleValue);
                        QueryItem intQueryItem = MessageUtil.buildQueryItem(attributeId, Operation.EQ, doubleQueryValue);
                        items.add(intQueryItem);
                    }
                    catch (NumberFormatException ex) {
                        logger.error("Invalid numeric value for attribute - " + item.getAttributeId());
                    }
                }
                break;
            case 3:
                if (value != null) {
                    logger.info("CO&CATRequestServer addQueryItem: DatetimeValue  - " + value);
                    String[] values = value.split(",");
                    Long startDate = null, endDate = null;
                    for (String v : values) {
                        if (v.startsWith("Start:")) {
                            startDate = parseDate(v.substring(6));
                        }
                        else if (v.startsWith("End:")) {
                            endDate = parseDate(v.substring(4));
                        }
                    }
                    if (startDate != null) {
                        parameterValue = MessageUtil.buildParameterValue(ParameterValueType.DATETIME, startDate);
                        queryItem = MessageUtil.buildQueryItem(attributeId, Operation.GE, parameterValue);
                        items.add(queryItem);
                    }
                    if (endDate != null) {
                        parameterValue = MessageUtil.buildParameterValue(ParameterValueType.DATETIME, endDate);
                        queryItem = MessageUtil.buildQueryItem(attributeId, Operation.LE, parameterValue);
                        items.add(queryItem);
                    }
                }
                break;
            case 4:
                BLSMessageParser.MUIString.Builder muiVal = BLSMessageParser.MUIString.newBuilder();
                BLSMessageParser.MUIString.MUIValue.Builder valBuilder = BLSMessageParser.MUIString.MUIValue.newBuilder();
                valBuilder.setValue(item.getValue() + '%');
                valBuilder.setLangID(locale);
                muiVal.addStrValues(valBuilder);
                ParameterValue muiQueryValue = MessageUtil.buildParameterValue(ParameterValueType.MUISTRING, muiVal.build());
                QueryItem muiQqueryItem = MessageUtil.buildQueryItem(attributeId, Operation.LIKE, muiQueryValue);
                items.add(muiQqueryItem);
                break;
            default:
        }
    }

    private ClassAttribute getQueryClassAttribute(Long queryAttr) throws BusinessLogicServiceException,
                                                                         InvalidProtocolBufferException {
        SelectRequest selectClassAttributesRequest = buildClassAttributesQueryRequest(CO_CLASS_ID);
        byte[] responseBytes = blService.selectByParams(selectClassAttributesRequest.toByteArray());
        CommonUtil.logResponseMessage(responseBytes);
        ResultSet response = ResultSet.parseFrom(responseBytes);
        Map<Long, Map<Long, List<Parameter>>> responseEntities = CommonUtil.groupParameters(response.getParametersList());
        for (Map.Entry<Long, Map<Long, List<Parameter>>> entry : responseEntities.entrySet()) {
            Long entityId = entry.getKey();
            Map<Long, List<Parameter>> attributes = entry.getValue();
            Integer type = getParameterValue(Long.parseLong(TYPE_ATTRIBUTE_PARAMETER), ParameterValueType.INTEGER, attributes);
            if (type == OBJECT_ATTRIBUTE_TYPE) {
                String range = getParameterValue(RANGE_PARAMETER_ID, ParameterValueType.STRING, attributes);
                String[] splittedRange = range.split(";");
                if (splittedRange.length > 0) {
                    Long classId = Long.parseLong(splittedRange[0]);
                    ClassAttribute linkedAttr = getLinkedQueryClassAttribute(queryAttr, classId);
                    if (linkedAttr != null) {
                        linkedAttr.setParentAttributeId(entityId);
                        return linkedAttr;
                    }
                }
            }
            else if (entityId == queryAttr.longValue()) {
                Long classId = getParameterValue(Long.parseLong(CLASS_ID_ATTRIBUTE_PARAMETER), ParameterValueType.LONG, attributes);
                ClassAttribute attr = new ClassAttribute(entityId, type, true, classId);
                return attr;
            }
        }
        return null;
    }

    private ClassAttribute getLinkedQueryClassAttribute(Long queryAttr, Long classId) throws
            BusinessLogicServiceException, InvalidProtocolBufferException {
        List<QueryItem> items = new ArrayList<>();
        ParameterValue classIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, classId);
        QueryItem classListItem = MessageUtil.buildQueryItem(CLASS_ID_ATTRIBUTE_PARAMETER, Operation.EQ, classIdValue);
        items.add(classListItem);
        SelectRequest.Builder builder = SelectRequest.newBuilder();
        SelectRequest selectRequest = builder.setMessageType(SELECT_REQUEST_MESSAGE_TYPE).setEntityType(ATTRIBUTE_ENTITY_TYPE).addAllFilter(items).build();
        byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
        ResultSet response = ResultSet.parseFrom(responseBytes);
        Map<Long, Map<Long, List<Parameter>>> responseEntities = CommonUtil.groupParameters(response.getParametersList());
        for (Map.Entry<Long, Map<Long, List<Parameter>>> entry : responseEntities.entrySet()) {
            Integer type = getParameterValue(Long.parseLong(TYPE_ATTRIBUTE_PARAMETER), ParameterValueType.INTEGER, entry.getValue());
            if (type != OBJECT_ATTRIBUTE_TYPE && (entry.getKey() == queryAttr.longValue())) {
                ClassAttribute attr = new ClassAttribute(entry.getKey(), type, false, classId);
                return attr;
            }
        }
        return null;
    }

    private SelectRequest buildLinkedObjectsQueryRequest(ClassAttribute classAttr, BaseResponseParser.GetCOandCATRequest.QueryItem item) {
        List<QueryItem> items = new ArrayList<>();
        SelectRequest.Builder builder = SelectRequest.newBuilder();
        addQueryItem(classAttr, item, items);
        SelectRequest selectRequest = builder.setMessageType(SELECT_REQUEST_MESSAGE_TYPE).setLocale(item.getLocale())
                .setEntityType(String.valueOf(classAttr.getClassId())).addAllFilter(items).build();
        return selectRequest;
    }

    private SelectRequest buildClassAttributesQueryRequest(Long attrId) {
        List<QueryItem> items = new ArrayList<>();
        SelectRequest.Builder builder = SelectRequest.newBuilder();
        ParameterValue classIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, attrId);
        QueryItem classListItem = MessageUtil.buildQueryItem(CLASS_ID_ATTRIBUTE_PARAMETER, Operation.EQ, classIdValue);
        items.add(classListItem);
        SelectRequest selectRequest = builder.setMessageType(SELECT_REQUEST_MESSAGE_TYPE)
                .setEntityType(ATTRIBUTE_ENTITY_TYPE).addAllFilter(items).build();
        return selectRequest;
    }

    private void processClassAttribute(ClassAttribute classAttr, List<QueryItem> items, BaseResponseParser.GetCOandCATRequest.QueryItem item) throws
            BusinessLogicServiceException, InvalidProtocolBufferException {
        if (classAttr.isAttrFromCOClass()) {
            addQueryItem(classAttr, item, items);
        }
        else {
            SelectRequest getLinkedObjectsRequest = buildLinkedObjectsQueryRequest(classAttr, item);
            byte[] responseBytes = blService.selectByParams(getLinkedObjectsRequest.toByteArray());
            ResultSet response = ResultSet.parseFrom(responseBytes);
            Map<Long, Map<Long, List<Parameter>>> responseEntities = CommonUtil.groupParameters(response.getParametersList());
            List<ParameterValue> parameterValues = new ArrayList<>();
            for (Map.Entry<Long, Map<Long, List<Parameter>>> entry : responseEntities.entrySet()) {
                parameterValues.add(MessageUtil.buildParameterValue(ParameterValueType.LONG, entry.getKey()));
            }
            ValueArray valueArray = MessageUtil.buildValueArray(parameterValues);
            ParameterValue filter = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, valueArray);
            QueryItem entityIdItem = MessageUtil.buildQueryItem(String.valueOf(classAttr.getParentAttributeId()), Operation.IN, filter);
            items.add(entityIdItem);
        }
    }

    private <T> T getParameterValue(Long attrId, ParameterValueType type, Map<Long, List<BLSMessageParser.Parameter>> attributes) {
        return getParameterValue(attrId, type, attributes, null);
    }

    @SuppressWarnings("unchecked")
    private <T> T getParameterValue(Long attrId, ParameterValueType type, Map<Long, List<BLSMessageParser.Parameter>> attributes, String locale) {
        List<BLSMessageParser.Parameter> parameters = attributes.get(attrId);
        if (parameters == null) {
            throw new RequestProcessingException("Cannot find entity parameter " + attrId);
        }
        if (parameters.isEmpty()) {
            throw new RequestProcessingException("Cannot find entity parameter " + attrId);
        }
        BLSMessageParser.Parameter parameter = parameters.get(0);
        return (T) extractValue(type, parameter, locale);
    }

    private Object extractValue(ParameterValueType type, Parameter parameter, String locale) {
        BLSMessageParser.ParameterValue value = parameter.getValue();
        switch (type) {
            case STRING:
                return value.getStringValue();
            case NUMBER:
                return value.getDoubleValue();
            case DATETIME:
                return value.getDatetimeValue();
            case MUISTRING:
                String muiStr = null;
                for (BLSMessageParser.MUIString.MUIValue muiValue : value.getMuistringValue().getStrValuesList()) {
                    if (muiValue.getLangID() != null && muiValue.getLangID().equals(locale)) {
                        muiStr = muiValue.getValue();
                        break;
                    }
                }
                return muiStr;
            case OBJECT_LINK:
                return value.getObjectLinkValue();
            case FILE:
                return value.getFileValue();
            case INTEGER:
                return value.getIntValue();
            case LONG:
                return value.getLongValue();
            case BOOLEAN:
                return value.getBooleanValue();
            case ARRAY:
                return value.getValueArray().getValuesList();
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }
    }

    private Long parseDate(String dateTimeString) {
        Long result = null;
        SimpleDateFormat sfd = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ROOT);
        try {
            Date date = sfd.parse(dateTimeString);
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.ROOT);
            c.clear();
            c.set(1900, Calendar.JANUARY, 1);
            result = date.getTime() - c.getTimeInMillis();
        }
        catch (ParseException parseException) {
            logger.warn("Cannot parse date string: " + dateTimeString, parseException);
        }
        return result;
    }

    private MessageLite getDefectiveResponse(BaseResponseParser.GetCOandCATRequest request) {
        BaseResponseParser.GetCOandCATResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(BaseResponseParser.State.DEFECTIVE);
        return builder.build();
    }

    private MessageLite getEmptyResponse(BaseResponseParser.GetCOandCATRequest request) {
        BaseResponseParser.GetCOandCATResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(BaseResponseParser.State.LAST);
        return builder.build();
    }

    private BaseResponseParser.GetCOandCATResponse.Builder getResponseBuilder(BaseResponseParser.GetCOandCATRequest request, int blockNumber) {
        BaseResponseParser.GetCOandCATResponse.Builder builder = BaseResponseParser.GetCOandCATResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setBlockNumber(blockNumber);
        return builder;
    }

    private List<MessageLite> buildResponseMessages(BaseResponseParser.GetCOandCATRequest request,
                                                    Map<Long, Map<Long, List<Parameter>>> coResponse,
                                                    Map<Long, Map<Long, List<Parameter>>> catResponse) {
        List<MessageLite> result = new ArrayList<>();
        if (coResponse.isEmpty()) {
            return result;
        }
        Iterator<Map.Entry<Long, Map<Long, List<Parameter>>>> coIterator = coResponse.entrySet().iterator();
        Iterator<Map.Entry<Long, Map<Long, List<Parameter>>>> catIterator = catResponse != null && !catResponse.isEmpty() ? catResponse.entrySet().iterator() : null;
        int blockNumber = 0, responseCounter = 0;
        Map<Long, String> attrLocales = new HashMap<>();
        List<BaseResponseParser.GetCOandCATRequest.QueryItem> qItems = request.getCriteriaListList();
        for (BaseResponseParser.GetCOandCATRequest.QueryItem qItem : qItems) {
            String locale = qItem.getLocale();
            if (locale != null && !locale.isEmpty()) {
                attrLocales.put(qItem.getAttributeId(), locale);
            }
        }
        boolean hasNext;
        BaseResponseParser.GetCOandCATResponse.Builder responseBuilder = null;
        do {
            if (responseBuilder == null) {
                responseBuilder = getResponseBuilder(request, blockNumber++);
            }
            try {
                if (coIterator.hasNext()) {
                    BaseResponseParser.GetCOandCATResponse.COEntry coEntry = getCOEntry(coIterator.next());
                    responseBuilder.addCoEntries(coEntry);
                    responseCounter++;
                }
                if (catIterator != null && catIterator.hasNext()) {
                    BaseResponseParser.GetCOandCATResponse.CATEntry catEntry = getCATEntry(catIterator.next(), attrLocales);
                    responseBuilder.addCatEntries(catEntry);
                    responseCounter++;
                }
            }
            catch (RequestProcessingException ex) {
                logger.info("Error while processing request (instanceId=" + request.getInstanceId() + ")", ex);
                responseBuilder.setBlockState(BaseResponseParser.State.DEFECTIVE);
            }
            hasNext = coIterator.hasNext() || catIterator != null && catIterator.hasNext();
            if (responseCounter >= IN_BLOCK_MESSAGES_LIMIT || responseBuilder.getBlockState() == BaseResponseParser.State.DEFECTIVE) {
                if (responseBuilder.getBlockState() != BaseResponseParser.State.DEFECTIVE) {
                    responseBuilder.setBlockState(hasNext ? BaseResponseParser.State.NORMAL : BaseResponseParser.State.LAST);
                }
                result.add(responseBuilder.build());
                responseCounter = 0;
                responseBuilder = null;
            }
        } while (hasNext);
        if (responseCounter > 0 && responseBuilder != null) {
            responseBuilder.setBlockState(BaseResponseParser.State.LAST);
            result.add(responseBuilder.build());
        }
        return result;
    }

    private BaseResponseParser.GetCOandCATResponse.COEntry getCOEntry(Map.Entry<Long, Map<Long, List<Parameter>>> entry) {
        BaseResponseParser.GetCOandCATResponse.COEntry.Builder builder = BaseResponseParser.GetCOandCATResponse.COEntry.newBuilder();
        builder.setCoId(entry.getKey());
        Map<Long, List<Parameter>> attributes = entry.getValue();
        if (attributes.containsKey(NAME_ATTRIBUTE_ID)) {
            String coName = getParameterValue(NAME_ATTRIBUTE_ID, ParameterValueType.STRING, attributes);
            builder.setCoName(coName);
        }
        if (attributes.containsKey(BIND_CAT_PARAMETER_ID)) {
            BLSMessageParser.ObjectLink categoryId = getParameterValue(BIND_CAT_PARAMETER_ID, ParameterValueType.OBJECT_LINK, attributes);
            List<Long> categoryIds = MessageUtil.convertToLongList(categoryId.getLinksList());
            builder.addAllBindCat(categoryIds);
        }
        return builder.build();
    }

    private BaseResponseParser.GetCOandCATResponse.CATEntry getCATEntry(Map.Entry<Long, Map<Long, List<Parameter>>> entry, Map<Long, String> attrLocales) {
        BaseResponseParser.GetCOandCATResponse.CATEntry.Builder builder = BaseResponseParser.GetCOandCATResponse.CATEntry.newBuilder();
        builder.setCatId(entry.getKey());
        Map<Long, List<Parameter>> attributes = entry.getValue();
        String coName = getParameterValue(NAME_ATTRIBUTE_ID, ParameterValueType.MUISTRING, attributes, attrLocales.get(NAME_ATTRIBUTE_ID));
        builder.setCoName(coName);
        if (attributes.containsKey(BIND_CO_ATTRIBUTE_ID)) {
            BLSMessageParser.ObjectLink coIdList = getParameterValue(BIND_CO_ATTRIBUTE_ID, ParameterValueType.OBJECT_LINK, attributes);
            builder.addAllCoIdList(MessageUtil.convertToLongList(coIdList.getLinksList()));
        }
        return builder.build();
    }

    private class ResponseEntry {

        private long entityId;
        private Map<Long, List<BLSMessageParser.Parameter>> attributes;

        ResponseEntry(long entityId, Map<Long, List<BLSMessageParser.Parameter>> attributes) {
            this.entityId = entityId;
            this.attributes = attributes;
        }

        long getEntityId() {
            return entityId;
        }

        void setEntityId(long entityId) {
            this.entityId = entityId;
        }

        Map<Long, List<BLSMessageParser.Parameter>> getAttributes() {
            return attributes;
        }

        void setAttributes(Map<Long, List<BLSMessageParser.Parameter>> attributes) {
            this.attributes = attributes;
        }
    }

    private class ClassAttribute {

        private long entityId;
        private boolean attrFromCOClass;
        private Integer attrType;
        private Long classId;
        private Long parentAttributeId;

        ClassAttribute(long entityId, Integer attrType, boolean attrFromCOClass, Long classId) {
            this.entityId = entityId;
            this.attrType = attrType;
            this.attrFromCOClass = attrFromCOClass;
            this.classId = classId;
        }

        long getEntityId() {
            return entityId;
        }

        void setEntityId(long entityId) {
            this.entityId = entityId;
        }

        Integer getAttrType() {
            return attrType;
        }

        void setAttrType(Integer attrType) {
            this.attrType = attrType;
        }

        boolean isAttrFromCOClass() {
            return attrFromCOClass;
        }

        void setAttrFromCOClass(boolean attrFromCOClass) {
            this.attrFromCOClass = attrFromCOClass;
        }

        Long getClassId() {
            return classId;
        }

        void setClassId(Long classId) {
            this.classId = classId;
        }

        Long getParentAttributeId() {
            return parentAttributeId;
        }

        void setParentAttributeId(Long parentAttributeId) {
            this.parentAttributeId = parentAttributeId;
        }
    }
}
