/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev Date: 28.09.12
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.ByteString;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.MUIString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public final class MessageUtil {

    public static SelectRequest buildSelectRequest(long entityType, List<QueryItem> filtes) {
        return buildSelectRequest(null, entityType, filtes, null, null);
    }

    public static SelectRequest buildSelectRequest(String locale, long entityType, List<QueryItem> filtes, Integer startTime, Integer endTime) {
        SelectRequest.Builder builder = SelectRequest.newBuilder();
        builder.setMessageType(Constants.SELECT_REQUEST_MESSAGE_TYPE).
                setEntityType(String.valueOf(entityType)).addAllFilter(filtes);
        if (locale != null) {
            builder.setLocale(locale);
        }
        if (startTime != null) {
            builder.setStartTime(startTime);
        }
        if (endTime != null) {
            builder.setEndTime(endTime);
        }
        return builder.build();
    }

    public static QueryItem buildQueryItem(String attributeId, Operation op, ParameterValue value) {
        return QueryItem.newBuilder().setAttributeId(attributeId).setOperation(op).setValue(value).build();
    }

    public static ParameterValue buildParameterValue(ParameterValueType type, Object value) {
        ParameterValue.Builder builder = ParameterValue.newBuilder();
        switch (type) {
            case STRING:
                builder.setStringValue((String) value);
                break;
            case NUMBER:
                builder.setDoubleValue((Double) value);
                break;
            case DATETIME:
                builder.setDatetimeValue((Long) value);
                break;
            case MUISTRING:
                builder.setMuistringValue((MUIString) value);
                break;
            case OBJECT_LINK:
                builder.setObjectLinkValue((ObjectLink) value);
                break;
            case FILE:
                builder.setFileValue((ByteString) value);
                break;
            case INTEGER:
                builder.setIntValue((Integer) value);
                break;
            case LONG:
                builder.setLongValue((Long) value);
                break;
            case BOOLEAN:
                builder.setBooleanValue((Boolean) value);
                break;
            case ARRAY:
                builder.setValueArray((ValueArray) value);
                break;
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }
        return builder.build();
    }

    public static ValueArray buildValueArray(List<ParameterValue> values) {
        return ValueArray.newBuilder().addAllValues(values).build();
    }

    public static ParameterValue buildParameterValue(List<Long> values) {
        List<ParameterValue> pvs = new ArrayList<>();
        for (Long value : values) {
            pvs.add(buildParameterValue(ParameterValueType.LONG, value));
        }
        return buildParameterValue(ParameterValueType.ARRAY, buildValueArray(pvs));
    }

    public static List<String> convertToStringList(List<Long> data) {
        List<String> result = null;
        if (data != null) {
            result = new ArrayList<>();
            for (Long value : data) {
                result.add(String.valueOf(value));
            }
        }
        return result;
    }

    public static List<Long> convertToLongList(List<String> data) {
        List<Long> result = null;
        if (data != null) {
            result = new ArrayList<>();
            for (String value : data) {
                result.add(Long.valueOf(value));
            }
        }
        return result;
    }

    private MessageUtil() {
    }
}
