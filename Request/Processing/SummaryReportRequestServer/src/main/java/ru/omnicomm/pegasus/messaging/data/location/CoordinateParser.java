// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Coordinate.proto

package ru.omnicomm.pegasus.messaging.data.location;

public final class CoordinateParser {
  private CoordinateParser() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public static final class Coordinate extends
      com.google.protobuf.GeneratedMessage {
    // Use Coordinate.newBuilder() to construct.
    private Coordinate() {
      initFields();
    }
    private Coordinate(boolean noInit) {}
    
    private static final Coordinate defaultInstance;
    public static Coordinate getDefaultInstance() {
      return defaultInstance;
    }
    
    public Coordinate getDefaultInstanceForType() {
      return defaultInstance;
    }
    
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_descriptor;
    }
    
    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_fieldAccessorTable;
    }
    
    // required int32 sourceId = 1;
    public static final int SOURCEID_FIELD_NUMBER = 1;
    private boolean hasSourceId;
    private int sourceId_ = 0;
    public boolean hasSourceId() { return hasSourceId; }
    public int getSourceId() { return sourceId_; }
    
    // optional int32 messageType = 2 [default = 61];
    public static final int MESSAGETYPE_FIELD_NUMBER = 2;
    private boolean hasMessageType;
    private int messageType_ = 61;
    public boolean hasMessageType() { return hasMessageType; }
    public int getMessageType() { return messageType_; }
    
    // required int32 messageTime = 3;
    public static final int MESSAGETIME_FIELD_NUMBER = 3;
    private boolean hasMessageTime;
    private int messageTime_ = 0;
    public boolean hasMessageTime() { return hasMessageTime; }
    public int getMessageTime() { return messageTime_; }
    
    // optional bool valid = 100 [default = true];
    public static final int VALID_FIELD_NUMBER = 100;
    private boolean hasValid;
    private boolean valid_ = true;
    public boolean hasValid() { return hasValid; }
    public boolean getValid() { return valid_; }
    
    // optional double latitude = 101;
    public static final int LATITUDE_FIELD_NUMBER = 101;
    private boolean hasLatitude;
    private double latitude_ = 0D;
    public boolean hasLatitude() { return hasLatitude; }
    public double getLatitude() { return latitude_; }
    
    // optional double longitude = 102;
    public static final int LONGITUDE_FIELD_NUMBER = 102;
    private boolean hasLongitude;
    private double longitude_ = 0D;
    public boolean hasLongitude() { return hasLongitude; }
    public double getLongitude() { return longitude_; }
    
    // optional double altitude = 103;
    public static final int ALTITUDE_FIELD_NUMBER = 103;
    private boolean hasAltitude;
    private double altitude_ = 0D;
    public boolean hasAltitude() { return hasAltitude; }
    public double getAltitude() { return altitude_; }
    
    // optional int32 satellite = 104;
    public static final int SATELLITE_FIELD_NUMBER = 104;
    private boolean hasSatellite;
    private int satellite_ = 0;
    public boolean hasSatellite() { return hasSatellite; }
    public int getSatellite() { return satellite_; }
    
    private void initFields() {
    }
    public final boolean isInitialized() {
      if (!hasSourceId) return false;
      if (!hasMessageTime) return false;
      return true;
    }
    
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (hasSourceId()) {
        output.writeInt32(1, getSourceId());
      }
      if (hasMessageType()) {
        output.writeInt32(2, getMessageType());
      }
      if (hasMessageTime()) {
        output.writeInt32(3, getMessageTime());
      }
      if (hasValid()) {
        output.writeBool(100, getValid());
      }
      if (hasLatitude()) {
        output.writeDouble(101, getLatitude());
      }
      if (hasLongitude()) {
        output.writeDouble(102, getLongitude());
      }
      if (hasAltitude()) {
        output.writeDouble(103, getAltitude());
      }
      if (hasSatellite()) {
        output.writeInt32(104, getSatellite());
      }
      getUnknownFields().writeTo(output);
    }
    
    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;
    
      size = 0;
      if (hasSourceId()) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(1, getSourceId());
      }
      if (hasMessageType()) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(2, getMessageType());
      }
      if (hasMessageTime()) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(3, getMessageTime());
      }
      if (hasValid()) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(100, getValid());
      }
      if (hasLatitude()) {
        size += com.google.protobuf.CodedOutputStream
          .computeDoubleSize(101, getLatitude());
      }
      if (hasLongitude()) {
        size += com.google.protobuf.CodedOutputStream
          .computeDoubleSize(102, getLongitude());
      }
      if (hasAltitude()) {
        size += com.google.protobuf.CodedOutputStream
          .computeDoubleSize(103, getAltitude());
      }
      if (hasSatellite()) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(104, getSatellite());
      }
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }
    
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data).buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data, extensionRegistry)
               .buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data).buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data, extensionRegistry)
               .buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input).buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input, extensionRegistry)
               .buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      Builder builder = newBuilder();
      if (builder.mergeDelimitedFrom(input)) {
        return builder.buildParsed();
      } else {
        return null;
      }
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      Builder builder = newBuilder();
      if (builder.mergeDelimitedFrom(input, extensionRegistry)) {
        return builder.buildParsed();
      } else {
        return null;
      }
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input).buildParsed();
    }
    public static ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input, extensionRegistry)
               .buildParsed();
    }
    
    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }
    
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> {
      private ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate result;
      
      // Construct using ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate.newBuilder()
      private Builder() {}
      
      private static Builder create() {
        Builder builder = new Builder();
        builder.result = new ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate();
        return builder;
      }
      
      protected ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate internalGetResult() {
        return result;
      }
      
      public Builder clear() {
        if (result == null) {
          throw new IllegalStateException(
            "Cannot call clear() after build().");
        }
        result = new ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate();
        return this;
      }
      
      public Builder clone() {
        return create().mergeFrom(result);
      }
      
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate.getDescriptor();
      }
      
      public ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate getDefaultInstanceForType() {
        return ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate.getDefaultInstance();
      }
      
      public boolean isInitialized() {
        return result.isInitialized();
      }
      public ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate build() {
        if (result != null && !isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return buildPartial();
      }
      
      private ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate buildParsed()
          throws com.google.protobuf.InvalidProtocolBufferException {
        if (!isInitialized()) {
          throw newUninitializedMessageException(
            result).asInvalidProtocolBufferException();
        }
        return buildPartial();
      }
      
      public ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate buildPartial() {
        if (result == null) {
          throw new IllegalStateException(
            "build() has already been called on this Builder.");
        }
        ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate returnMe = result;
        result = null;
        return returnMe;
      }
      
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate) {
          return mergeFrom((ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }
      
      public Builder mergeFrom(ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate other) {
        if (other == ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate.getDefaultInstance()) return this;
        if (other.hasSourceId()) {
          setSourceId(other.getSourceId());
        }
        if (other.hasMessageType()) {
          setMessageType(other.getMessageType());
        }
        if (other.hasMessageTime()) {
          setMessageTime(other.getMessageTime());
        }
        if (other.hasValid()) {
          setValid(other.getValid());
        }
        if (other.hasLatitude()) {
          setLatitude(other.getLatitude());
        }
        if (other.hasLongitude()) {
          setLongitude(other.getLongitude());
        }
        if (other.hasAltitude()) {
          setAltitude(other.getAltitude());
        }
        if (other.hasSatellite()) {
          setSatellite(other.getSatellite());
        }
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }
      
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder(
            this.getUnknownFields());
        while (true) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              this.setUnknownFields(unknownFields.build());
              return this;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                this.setUnknownFields(unknownFields.build());
                return this;
              }
              break;
            }
            case 8: {
              setSourceId(input.readInt32());
              break;
            }
            case 16: {
              setMessageType(input.readInt32());
              break;
            }
            case 24: {
              setMessageTime(input.readInt32());
              break;
            }
            case 800: {
              setValid(input.readBool());
              break;
            }
            case 809: {
              setLatitude(input.readDouble());
              break;
            }
            case 817: {
              setLongitude(input.readDouble());
              break;
            }
            case 825: {
              setAltitude(input.readDouble());
              break;
            }
            case 832: {
              setSatellite(input.readInt32());
              break;
            }
          }
        }
      }
      
      
      // required int32 sourceId = 1;
      public boolean hasSourceId() {
        return result.hasSourceId();
      }
      public int getSourceId() {
        return result.getSourceId();
      }
      public Builder setSourceId(int value) {
        result.hasSourceId = true;
        result.sourceId_ = value;
        return this;
      }
      public Builder clearSourceId() {
        result.hasSourceId = false;
        result.sourceId_ = 0;
        return this;
      }
      
      // optional int32 messageType = 2 [default = 61];
      public boolean hasMessageType() {
        return result.hasMessageType();
      }
      public int getMessageType() {
        return result.getMessageType();
      }
      public Builder setMessageType(int value) {
        result.hasMessageType = true;
        result.messageType_ = value;
        return this;
      }
      public Builder clearMessageType() {
        result.hasMessageType = false;
        result.messageType_ = 61;
        return this;
      }
      
      // required int32 messageTime = 3;
      public boolean hasMessageTime() {
        return result.hasMessageTime();
      }
      public int getMessageTime() {
        return result.getMessageTime();
      }
      public Builder setMessageTime(int value) {
        result.hasMessageTime = true;
        result.messageTime_ = value;
        return this;
      }
      public Builder clearMessageTime() {
        result.hasMessageTime = false;
        result.messageTime_ = 0;
        return this;
      }
      
      // optional bool valid = 100 [default = true];
      public boolean hasValid() {
        return result.hasValid();
      }
      public boolean getValid() {
        return result.getValid();
      }
      public Builder setValid(boolean value) {
        result.hasValid = true;
        result.valid_ = value;
        return this;
      }
      public Builder clearValid() {
        result.hasValid = false;
        result.valid_ = true;
        return this;
      }
      
      // optional double latitude = 101;
      public boolean hasLatitude() {
        return result.hasLatitude();
      }
      public double getLatitude() {
        return result.getLatitude();
      }
      public Builder setLatitude(double value) {
        result.hasLatitude = true;
        result.latitude_ = value;
        return this;
      }
      public Builder clearLatitude() {
        result.hasLatitude = false;
        result.latitude_ = 0D;
        return this;
      }
      
      // optional double longitude = 102;
      public boolean hasLongitude() {
        return result.hasLongitude();
      }
      public double getLongitude() {
        return result.getLongitude();
      }
      public Builder setLongitude(double value) {
        result.hasLongitude = true;
        result.longitude_ = value;
        return this;
      }
      public Builder clearLongitude() {
        result.hasLongitude = false;
        result.longitude_ = 0D;
        return this;
      }
      
      // optional double altitude = 103;
      public boolean hasAltitude() {
        return result.hasAltitude();
      }
      public double getAltitude() {
        return result.getAltitude();
      }
      public Builder setAltitude(double value) {
        result.hasAltitude = true;
        result.altitude_ = value;
        return this;
      }
      public Builder clearAltitude() {
        result.hasAltitude = false;
        result.altitude_ = 0D;
        return this;
      }
      
      // optional int32 satellite = 104;
      public boolean hasSatellite() {
        return result.hasSatellite();
      }
      public int getSatellite() {
        return result.getSatellite();
      }
      public Builder setSatellite(int value) {
        result.hasSatellite = true;
        result.satellite_ = value;
        return this;
      }
      public Builder clearSatellite() {
        result.hasSatellite = false;
        result.satellite_ = 0;
        return this;
      }
      
      // @@protoc_insertion_point(builder_scope:ru.omnicomm.pegasus.messaging.data.location.Coordinate)
    }
    
    static {
      defaultInstance = new Coordinate(true);
      ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.internalForceInit();
      defaultInstance.initFields();
    }
    
    // @@protoc_insertion_point(class_scope:ru.omnicomm.pegasus.messaging.data.location.Coordinate)
  }
  
  private static com.google.protobuf.Descriptors.Descriptor
    internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_fieldAccessorTable;
  
  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\020Coordinate.proto\022+ru.omnicomm.pegasus." +
      "messaging.data.location\"\253\001\n\nCoordinate\022\020" +
      "\n\010sourceId\030\001 \002(\005\022\027\n\013messageType\030\002 \001(\005:\0026" +
      "1\022\023\n\013messageTime\030\003 \002(\005\022\023\n\005valid\030d \001(\010:\004t" +
      "rue\022\020\n\010latitude\030e \001(\001\022\021\n\tlongitude\030f \001(\001" +
      "\022\020\n\010altitude\030g \001(\001\022\021\n\tsatellite\030h \001(\005B?\n" +
      "+ru.omnicomm.pegasus.messaging.data.loca" +
      "tionB\020CoordinateParser"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
      new com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner() {
        public com.google.protobuf.ExtensionRegistry assignDescriptors(
            com.google.protobuf.Descriptors.FileDescriptor root) {
          descriptor = root;
          internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_descriptor =
            getDescriptor().getMessageTypes().get(0);
          internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_fieldAccessorTable = new
            com.google.protobuf.GeneratedMessage.FieldAccessorTable(
              internal_static_ru_omnicomm_pegasus_messaging_data_location_Coordinate_descriptor,
              new java.lang.String[] { "SourceId", "MessageType", "MessageTime", "Valid", "Latitude", "Longitude", "Altitude", "Satellite", },
              ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate.class,
              ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate.Builder.class);
          return null;
        }
      };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }
  
  public static void internalForceInit() {}
  
  // @@protoc_insertion_point(outer_class_scope)
}
