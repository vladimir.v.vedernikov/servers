/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.request217;

import com.google.protobuf.MessageLite;
import java.util.Iterator;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;

/**
 *
 * @author alexander
 */
public class MessageLiteIterator implements Iterator<MessageLite> {
    
    private final Iterator<MessageChunk> iterator;
    private MessageLite[] messages = new MessageLite[0];
    private int index = 0;
    
    public MessageLiteIterator(int sourceId, int messageType, int from, int till, DataStorageService dataStorage) throws DataStorageServiceException {      
        this.iterator = dataStorage.query(sourceId, messageType, from, till);
        this.makeNext();
    }
    
    private void makeNext(){
        if(++this.index>=this.messages.length){
            while(this.iterator.hasNext()){
                this.messages = this.iterator.next().messages;
                this.index = 0;
                if(this.messages.length>0){              
                    break;
                }
            }
        }
        
//        if(++this.index>=this.messages.length && this.iterator.hasNext()){
//            this.messages = this.iterator.next().messages;
//            this.index = 0;
//        }
    }

    @Override
    public boolean hasNext() {
        return this.index<this.messages.length;
    }

    @Override
    public MessageLite next() {
        MessageLite message = this.messages[index];
        this.makeNext();
        return message;
        
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
