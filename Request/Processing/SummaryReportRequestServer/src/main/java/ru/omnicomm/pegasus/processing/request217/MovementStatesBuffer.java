/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processing.request217;

import com.google.protobuf.InvalidProtocolBufferException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer.GpsData;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer.VelocityData;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;



/**
 * Буфер алгоритма определения состояний по движению
 * @author alexander
 */
public class MovementStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int distanceMIN;
    private final int velocityMIN;
    private final int idleMAX;
    private final int decisionInterval;

    private final List<VelocityData> periodDataBuffer;
    private final List<VelocityData> velocityBuffer;
    private final List<GpsData> gpsBuffer;

    private MovementStateDefinitionBuffer.State currentState;

    private final Comparator<VelocityData> velocityDataByMessageTimeComparator;
    private final Comparator<GpsData> gpsDataByMessageTimeComparator;
    
    
    private long mileageValue;
    private long maxVelocityValue;
    private int movementTimeValue;
    private int parkingTimeValue;
    private int nodataTimeValue;
    
  //  private int currentNodataTimeValue; //Время отсутствия данных за период парковки/движения
    private int startPeriodTime;// = Integer.MIN_VALUE; //Время начала периода парковки/движения
    private final int initMessageTime; 
    

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public MovementStatesBuffer(MovementStateDefinitionSettings settings, int from) throws IllegalArgumentException {
        this.startPeriodTime=from-settings.getDecisionInterval()*2;
        this.initMessageTime=from;
        this.sourceId = settings.getSourceId();
        this.distanceMIN = settings.getDistanceMIN();
        this.velocityMIN = settings.getVelocityMIN();
        this.idleMAX = settings.getIdleMAX();
        this.decisionInterval = settings.getDecisionInterval();
        this.periodDataBuffer = new ArrayList<VelocityData>();
        this.velocityBuffer = new ArrayList<VelocityData>();
        this.gpsBuffer = new ArrayList<GpsData>();

        this.currentState = MovementStateDefinitionBuffer.State.NOT_DEFINED;

        this.velocityDataByMessageTimeComparator = new Comparator<VelocityData>() {

            @Override
            public int compare(VelocityData obj1, VelocityData obj2) {
                return ((Integer)obj1.getDataId()).compareTo((Integer)obj2.getDataId());
            }
        };

        this.gpsDataByMessageTimeComparator = new Comparator<GpsData>() {

            @Override
            public int compare(GpsData obj1, GpsData obj2) {
                return ((Integer)obj1.getDataId()).compareTo((Integer)obj2.getDataId());
            }
        };

     
    }


    public void processData(int messageTypeId, byte[] messageBody) throws InvalidProtocolBufferException {

        MessageType messageType = MessageType.lookup(messageTypeId);

        if(messageType == null){
            throw new IllegalArgumentException("Unknown message type id: "+messageTypeId);
        }

        //Получаем агрегированные данные по скорости и координатам
        switch(messageType) {

            case VALUE_MULTIPLY_SPEED:
                this.processVelocityData(ValueMultiplySpeed.parseFrom(messageBody));
                break;

            case COORDINATE:
                this.processGpsData(Coordinate.parseFrom(messageBody));
                break;

        }

    }

    //4.1	Обработка сообщения "ValueMultiplySpeed"
    private void processVelocityData(ValueMultiplySpeed valueMultiplySpeed) {
        
        LOGGER.debug("PROCESSING SPEED: "+valueMultiplySpeed.toString().replaceAll("\n", " "));
        
//        if(this.startPeriodTime==Integer.MIN_VALUE){
//            this.startPeriodTime=valueMultiplySpeed.getMessageTime();
//        }
        if(valueMultiplySpeed.getMessageTime()>=this.initMessageTime){
            this.periodDataBuffer.add(VelocityData.newBuilder().setDataId(valueMultiplySpeed.getMessageTime()).setValue((int)(valueMultiplySpeed.getDoubleValue(0))).build());
        }
        
        
        //3)	Сервер помещает обрабатываемое сообщение в буфер
        this.velocityBuffer.add(VelocityData.newBuilder().setDataId(valueMultiplySpeed.getMessageTime()).setValue((int)(valueMultiplySpeed.getDoubleValue(0))).build());

        Collections.sort(this.gpsBuffer, this.gpsDataByMessageTimeComparator);
        Collections.sort(this.velocityBuffer, this.velocityDataByMessageTimeComparator);

        //4)	Если разность между максимальным и минимальным timestamp в буфере меньше DecisionInterval, то сервер прерывает данную ветвь.
        while(!this.velocityBuffer.isEmpty() && this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() - this.velocityBuffer.get(0).getDataId() >= this.decisionInterval){
            //5)	Сервер выполняет алгоритм 5.1 Подсчёт средней скорости и запоминает его результат VAvr.
            int v_avr = this.getAverageSpeed();
           // LOGGER.debug("v_avr="+v_avr);
            //6)	Сервер выполняет алгоритм 5.2 Подсчёт пробега и запоминает его результат M.

            int m = this.getMileage();
      //      LOGGER.debug("m="+m);

            //7)	Устанавливаем Tm = минимальный timestamp в буфере.
            int t_min;
            if(this.gpsBuffer.isEmpty()){
                t_min = this.velocityBuffer.get(0).getDataId();
            } else {
                t_min = this.velocityBuffer.get(0).getDataId() > this.gpsBuffer.get(0).getDataId() ? this.velocityBuffer.get(0).getDataId() : this.gpsBuffer.get(0).getDataId();
            }
            
       //     LOGGER.debug("m="+m+" distanceMIN="+distanceMIN+" v_avr="+v_avr+" velocityMIN="+velocityMIN);
            
            //8)	Если М < DistanceMin И VAvr < VelocityMIN, И текущее состояние для SourceID НЕ равно Стоянка, то:
            if(m < this.distanceMIN && v_avr < this.velocityMIN && this.currentState != MovementStateDefinitionBuffer.State.PARKING){
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingSTART со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = минимальный timestamp среди всех сообщений в буфере, для которых выполняется условие Vm < VelocityMIN, где Vm – скорость из тела сообщения. (Устанавливаем Tm = данный timestamp)
                //iii.	Тело сообщения пустое
                int minTimestamp = Integer.MAX_VALUE;
                for(VelocityData velocityData : this.velocityBuffer){
                    if(velocityData.getValue() < this.velocityMIN && velocityData.getDataId() < minTimestamp){
                        minTimestamp = velocityData.getDataId();
                    }
                }
                if(minTimestamp<Integer.MAX_VALUE){
                    t_min = minTimestamp;
              //      if(this.startPeriodTime>minTimestamp){
                        int nodata = getNodataPeriodTime(this.startPeriodTime,minTimestamp);
                        int t = minTimestamp - this.startPeriodTime - nodata;
                        if(this.startPeriodTime<this.initMessageTime){
                            t=minTimestamp - this.initMessageTime - nodata;
                        }
                        long mileage = getMileage(this.startPeriodTime,minTimestamp);
                        this.mileageValue+=mileage;
                        
                        
                        LOGGER.debug(String.format("UNKNOWN PERIOD: from %8d till %8d, total=%4d, nodata=%4d",this.startPeriodTime,minTimestamp,t,nodata));
                        
                        
                        if(t>0){
                            if(mileage>0){
                                LOGGER.debug(String.format("MOVEMENT: from %8d till %8d, total=%4d, nodata=%4d",this.startPeriodTime,minTimestamp,t,nodata));
                                this.movementTimeValue += t;
                            } else {
                                LOGGER.debug(String.format("PARKING: from %8d till %8d, total=%4d, nodata=%4d **",this.startPeriodTime,minTimestamp,t,nodata));
                                this.parkingTimeValue += t;
                            }
                            
                        }
                        
                        this.nodataTimeValue  +=nodata; 
                        this.startPeriodTime = minTimestamp;
             //       }
                    //фиксируем период движения      
                    
               //   LOGGER.info("PARKING START: "+minTimestamp);
                //   this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingSTARTMessage(sourceId, minTimestamp));
                    //b.	Текущее состояние для SourceID устанавливается равным Стоянка
                    this.currentState = MovementStateDefinitionBuffer.State.PARKING;
                    
                    while(this.periodDataBuffer.size()>5){
                        this.periodDataBuffer.remove(0);
                    }
                    
                }
                
            }

            //9)	Если НЕ (М < DistanceMin И VAvr < VelocityMIN), И текущее состояние для SourceID НЕ равно Движение, то:
            if(!(m < this.distanceMIN && v_avr < this.velocityMIN) && (this.currentState != MovementStateDefinitionBuffer.State.MOVEMENT) ){
                
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingEND со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = максимальный timestamp сообщения в буфере
                //iii.	Тело сообщения пустое
                int maxTimestamp;
                if(this.gpsBuffer.isEmpty()){
                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId();
                } else {
                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() > this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId() ? this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() : this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId();
                }
                //фиксируем период парковки
            //    if(this.startPeriodTime>maxTimestamp){
                    int nodata = getNodataPeriodTime(this.startPeriodTime,maxTimestamp);
                    int t = maxTimestamp - this.startPeriodTime - nodata;// - this.currentNodataTimeValue;
                    if(this.startPeriodTime<this.initMessageTime){
                        t=maxTimestamp - this.initMessageTime - nodata;
                    }
                    if(t>0){
                        this.parkingTimeValue += t;
                    }
                    

                    LOGGER.debug(String.format("PARKING : from %8d till %8d, total=%4d, nodata=%4d",this.startPeriodTime,maxTimestamp,t,nodata));
                    this.nodataTimeValue   +=nodata; //TODO: get no data period
                    this.startPeriodTime = maxTimestamp;
            //    }
                
     //           LOGGER.debug("PARKING END: "+maxTimestamp);
                //this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingENDMessage(sourceId, maxTimestamp));
                //b.	Текущее состояние для SourceID устанавливается равным Движение
                this.currentState = MovementStateDefinitionBuffer.State.MOVEMENT;
                
                while(this.periodDataBuffer.size()>5){
                    this.periodDataBuffer.remove(0);
                }
            }

            //10)	Сервер удаляет из буфера все сообщения с timestamp, не большим, чем Tm и переходит на шаг 4)
            while(this.gpsBuffer.size()>0){
                if(this.gpsBuffer.get(0).getDataId()<=t_min){
                    this.gpsBuffer.remove(0);
                } else {
                    break;
                }
            }

            while(this.velocityBuffer.size()>0){
                if(this.velocityBuffer.get(0).getDataId()<=t_min){
                    this.velocityBuffer.remove(0);
                } else {
                    break;
                }
            }          
        }
    }
    
    public void commit(int till){
        int nodata = getNodataPeriodTime(this.startPeriodTime,till);
        int t = till - this.startPeriodTime - nodata;// - this.currentNodataTimeValue;
        if(this.startPeriodTime<this.initMessageTime){
            t=till - this.initMessageTime - nodata;
        }
        this.nodataTimeValue   +=nodata;
        if(t>0){
            
//            boolean hasData=false;
//            for (VelocityData v : this.periodDataBuffer) {
//                if(v.getDataId()>=this.initMessageTime && v.getDataId()>=this.startPeriodTime && v.getDataId()<=till){
//                    hasData=true;
//                    break;
//                }
//                
//            }
            
          //  if(hasData){
                switch(this.currentState){
                    case PARKING: 
                        this.parkingTimeValue += t;
                        LOGGER.debug(String.format("PARKING : from %8d till %8d, total=%4d, nodata=%4d *",this.startPeriodTime,till,t,nodata));
                        break;
                    case MOVEMENT: 
                        this.movementTimeValue += t;
                        LOGGER.debug(String.format("MOVEMENT: from %8d till %8d, total=%4d, nodata=%4d *",this.startPeriodTime,till,t,nodata));
                        this.mileageValue+=getMileage(this.startPeriodTime+1,till);
                        break;

        //            default:
        //                this.nodataTimeValue = till - this.initMessageTime;
        //                if(t!=0){
        //                    this.mileageValue+=getMileage(this.startPeriodTime,till);
        //                    if(this.mileageValue>0){
        //                        this.movementTimeValue += t;
        //                        this.mileageValue+=getMileage(this.startPeriodTime,till);
        //                    } else {
        //                        this.parkingTimeValue += t;
        //                    }
        //                }
                }
//            } else {
//                this.nodataTimeValue+=t;
//            }
            
            
        }
        
    }
    
    private int getNodataPeriodTime(int from, int till){
        if(this.periodDataBuffer.isEmpty()){
            return 0;
        }
        int res = 0;
        int t = from;
        for(VelocityData vd : this.periodDataBuffer){
            if(vd.getDataId()>=from && vd.getDataId()<=till){
                int delta_t = vd.getDataId() - t;
                if(delta_t >= this.idleMAX){
                    res+=delta_t;
                }
                t=vd.getDataId();
            }
            
        }
        int delta_t = till - t;
        if(delta_t >= this.idleMAX){
            res+=delta_t;
        }
        
        return res;
    }

    private void processGpsData(Coordinate coordinate) {
        
        LOGGER.debug("PROCESSING COORDINATES: "+coordinate.toString().replaceAll("\n", " "));

        if(coordinate.getValid()){
            this.gpsBuffer.add(GpsData.newBuilder()
                    
                    .setDataId(coordinate.getMessageTime())
                    .setLatitude(coordinate.getLatitude())
                    .setLongitude(coordinate.getLongitude())

                    .build());
        }
        
    }
    
    private long getMileage(int from, int till){
        long S = 0;
        for (int i = 1; i < this.periodDataBuffer.size(); i++) {
            
            if(this.periodDataBuffer.get(i).getDataId()>=from && this.periodDataBuffer.get(i).getDataId()<=till ){
                int deltaT = this.periodDataBuffer.get(i).getDataId() - this.periodDataBuffer.get(i-1).getDataId();
                int v_i;
                if(deltaT < this.idleMAX){
                    v_i = this.periodDataBuffer.get(i).getValue();
                } else {
                    v_i = 0;
                }
                S += (long)(v_i * deltaT);
                
            }

            
        }

        
        return S;
        
    }

    private int getAverageSpeed(){

        int S = 0;
        for (int i = 1; i < this.velocityBuffer.size(); i++) {

            int deltaT = this.velocityBuffer.get(i).getDataId() - this.velocityBuffer.get(i-1).getDataId();
            int v_i;
            if(deltaT < this.idleMAX){
                v_i = this.velocityBuffer.get(i).getValue();
                if(v_i>this.maxVelocityValue){
                    this.maxVelocityValue = v_i;
                }
            } else {
  //              this.currentNodataTimeValue+=deltaT;
                v_i = 0;
            }
            S += (v_i * deltaT);     
        }
       // if(this.currentState==MovementStateDefinitionBuffer.State.MOVEMENT){
         //   this.mileageValue+=S;
            
            
        //}
        
        return S/(this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId()-this.velocityBuffer.get(0).getDataId());

    }

    private int getMileage(){

        
        int m = 0;

        for (int i = 1; i < gpsBuffer.size(); i++) {
            m+=getDistance(gpsBuffer.get(i-1),gpsBuffer.get(i));
        }
        
        return m;       
    }

    private double sin(double deg){
        return Math.sin(Math.toRadians(deg));
    }

    private double cos(double deg){
        return Math.cos(Math.toRadians(deg));
    }


    private int getDistance(GpsData point1, GpsData point2){

        double earth_radius = 6372.0;

        double dist =
                2 * earth_radius * 1000 * Math.asin(
                    Math.sqrt(
                        Math.pow(sin((point1.getLatitude() - point2.getLatitude()) / 2.0), 2) +
                        cos(point1.getLatitude()) * cos(point2.getLatitude()) *
                        Math.pow(sin((point1.getLongitude() - point2.getLongitude()) / 2.0), 2)
                    )
                );

        return (int)dist;


    }
    
    

    public long getMaxVelocityValue() {
        return maxVelocityValue;
    }

    public long getMileageValue() {
        return mileageValue;
    }
    
    private int getLastPeriodValue(){
        if(this.velocityBuffer.isEmpty()){
            return 0;
        }
        int maxTimestamp;
        if(this.gpsBuffer.isEmpty()){
            maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId();
        } else {
            maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() > this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId() ? this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() : this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId();
        }
        return maxTimestamp;
    }

    public int getMovementTimeValue() {   
            return movementTimeValue;
    
    }

    public int getNodataTimeValue() {

            return nodataTimeValue;
      
        
    }

    public int getParkingTimeValue() {
  
            return parkingTimeValue;
      
        
    }






}
