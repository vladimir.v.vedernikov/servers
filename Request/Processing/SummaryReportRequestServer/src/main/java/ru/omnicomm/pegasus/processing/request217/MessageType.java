/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processing.request217;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum MessageType {

    COORDINATE(Coordinate.getDefaultInstance().getMessageType()),

//    AGR_MILEAGE_V(AGRMileageV.getDefaultInstance().getMessageType()),
//    AGR_VELOCITY_MAX(AGRVelocityMax.getDefaultInstance().getMessageType()),
//    AGR_VELOCITY_AVR(AGRVelocityAvr.getDefaultInstance().getMessageType()),
//
//    PARKING_START(ParkingSTART.getDefaultInstance().getMessageType()),
//    PARKING_END(ParkingEND.getDefaultInstance().getMessageType()),

    VALUE_MULTIPLY_SPEED(ValueMultiplySpeed.getDefaultInstance().getMessageType());
    

    private final int code;

    private static final Map<Integer, MessageType> map = new HashMap<Integer, MessageType>();

    static {
        for (MessageType type : EnumSet.allOf(MessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private MessageType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static MessageType lookup(int code) {
        return map.get(code);
    }

}
