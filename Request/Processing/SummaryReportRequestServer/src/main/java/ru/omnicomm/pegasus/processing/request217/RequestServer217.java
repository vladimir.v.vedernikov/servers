/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.request217;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.omnicomm.pegasus.base.Constants;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser.SendRequest;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.data.responses.VelocityTimeParametersResponseParser.VelocityTimeParametersResponse;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;


/**
 * @author alexander
 */

@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("SummaryReportRequestServerImplementation")
public class RequestServer217 implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 217;
    public static final int REQUEST_MESSAGE_TYPE = 417;

//    private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private DataStorageService dataStorage;
    private SettingsStorageService settingsStorage;
    private JmsBridgeService jmsBridge;

    public static final String CONTROLLED_UNIT_TYPE_NAME = "Speed";
    private ControlEntityHelper baseHelper;

    private Server self;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     */
    @ServerConstructor
    public RequestServer217(DataStorageService dataStorage, SettingsStorageService settingsStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.dataStorage = dataStorage;
        this.settingsStorage = settingsStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }


    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequest request;
        try {
            request = SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }


        try {
            long start = System.currentTimeMillis();
            processMessage(request);
            long total = System.currentTimeMillis() - start;

            LOGGER.info("TOTAL REQUEST PROCESSING TIME: " + total + " ms");
        } catch (Exception ex) {
            LOGGER.info("Can't process data", ex);
        }
    }

    private void processMessage(SendRequest request) throws DataStorageServiceException, InvalidProtocolBufferException, SettingsStorageServiceException, JmsBridgeServiceException {
        List<SendRequest.Parameter> filter = request.getFilterList();
        long startPeriod = Long.MIN_VALUE;
        long finishPeriod = Long.MIN_VALUE;
        List<Integer> vehiclesIds = new ArrayList<Integer>();
        String locale = "en";
        String requestId = request.getInstanceId();

        for (SendRequest.Parameter parameter : filter) {

            String key = parameter.getKey();

            if (key.equalsIgnoreCase("startPeriod")) {
                startPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("finishPeriod")) {
                finishPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("vehicleId")) {
                if (vehiclesIds.size() < Constants.TOTAL_MESSAGES_LIMIT) {
                    vehiclesIds.add(Integer.parseInt(parameter.getStringValue()));
                }
            } else if (key.equalsIgnoreCase("locale")) {
                locale = parameter.getStringValue();
            }

        }

        if (startPeriod < Integer.MIN_VALUE || finishPeriod < Integer.MIN_VALUE || vehiclesIds.isEmpty() || finishPeriod < startPeriod){
            throw new IllegalArgumentException("There are not enough or wrong parameters in the request: " + request);
        }

        int blockNmb = 0;
        
        int t2 = (int) finishPeriod;

        VelocityTimeParametersResponse.Builder responseBuilder = makeResponseBuilder(blockNmb++, requestId);

        int messageCounter = 0;
        for (int i = 0; i < vehiclesIds.size(); i++) {
            
            
            
            int vehicleId = vehiclesIds.get(i);
            LOGGER.debug("VEHICLE_ID = "+vehicleId);
            
            //1. Разбиваем период t1 - t2 на интервалы длиной в сутки (86400 сек.): таким образом получаем список ключей к кэшу
            //2. Бежим по полученному списку
            //  2.1 Загружаем файл с данными (ReportCache)
            //  2.2 Если отсутствует, то создаем новый ReportCache
            //  2.3 Находим в ReportCache нужные нам DataItems
            //  2.4 Если отсутствуют, то создаем и помещаем их в ReportCache
            //  2.5 Если в конец или начало периода не кратны 3600, то для конца или начала периода считаем по сырым данным
            //  2.6 Прибавляем данные к имеющимся результатам 
            //  2.6 Отправляем обновленные ReportCache в хранилище

            long mileage = 0;
            long maxVelocity = 0;
            int movementTime = 0;
            int parkingTime = 0;
            int nodataTime = 0;
            
            int t1 = (int) startPeriod;
         
            int dayCacheMessageTime = t1 - t1%86400;
            int messageTime = t1 - t1%3600;  
            
            Integer sourceId = baseHelper.selectFirstSoureceId(CONTROLLED_UNIT_TYPE_NAME, vehicleId);
            if(sourceId==null){
                continue;
            }
            
            String settingsKey = getSettingsName(sourceId);
            MessageLite settingsObject = this.settingsStorage.lookup(settingsKey);
            if (settingsObject == null) {
                LOGGER.info("Movement States Definition Settings wasn't found for sourceId="+sourceId);
                continue;
            }
            MovementStateDefinitionSettings settings = null;
            try{
                settings = MovementStateDefinitionSettings.parseFrom(settingsObject.toByteString());
            } catch (Exception ex) {
                LOGGER.info("Wrong settings object: "+settingsObject,ex);
                
            }
            if(settings==null){
                continue;
            }
            
        //    t1 = t1-settings.getDecisionInterval();
            
            MovementStatesBuffer buf = new MovementStatesBuffer(settings,t1);
            
            LOGGER.info("Decision interval: "+settings.getDecisionInterval());
            
            Iterator<MessageLite> coordinatesIterator = new MessageLiteIterator(sourceId, MessageType.COORDINATE.getCode(), t1-settings.getDecisionInterval()*2, t2, this.dataStorage);
            Iterator<MessageLite> speedIterator = new MessageLiteIterator(sourceId, MessageType.VALUE_MULTIPLY_SPEED.getCode(), t1-settings.getDecisionInterval()*2, t2, this.dataStorage);
            
            try{
                ValueMultiplySpeed speed = getNextSpeedData(speedIterator);
                while(coordinatesIterator.hasNext() && speed!=null){
                    Coordinate coordinate = Coordinate.parseFrom(coordinatesIterator.next().toByteString()); 
                    buf.processData(MessageType.COORDINATE.getCode(), coordinate.toByteArray());
                    while(speed!=null && speed.getMessageTime()<=coordinate.getMessageTime()){
                        buf.processData(MessageType.VALUE_MULTIPLY_SPEED.getCode(), speed.toByteArray());
                        speed = getNextSpeedData(speedIterator);
                    }
                }
            } catch (Exception ex) {
                responseBuilder.setBlockState(VelocityTimeParametersResponse.State.DEFECTIVE);
                jmsBridge.send(responseBuilder.build());
                responseBuilder = makeResponseBuilder(blockNmb++, requestId);
                LOGGER.info("Summary report error",ex);
                continue; // for (int i = 0; i < vehiclesIds.size(); i++) 
            }
            
            buf.commit(t2);
            
            mileage = buf.getMileageValue();
            maxVelocity = buf.getMaxVelocityValue();
            movementTime = buf.getMovementTimeValue();
            parkingTime = buf.getParkingTimeValue();
            nodataTime = buf.getNodataTimeValue();
            
            int delta = (t2-t1) - (nodataTime+parkingTime+movementTime);
            nodataTime+=delta;

            
            VelocityTimeParametersResponse.Row.Builder parameterRowBuilder = VelocityTimeParametersResponse.Row.newBuilder();
            parameterRowBuilder.setVehicleId(Integer.toString(vehicleId));

            long avgVelocity;
            if(movementTime>0){
                avgVelocity = mileage/movementTime;
            } else {
                avgVelocity = 0;
            }
            if(avgVelocity>maxVelocity){
                avgVelocity=maxVelocity;
            }
            parameterRowBuilder.setMileage(mileage); 
            parameterRowBuilder.setAverageVelocity(avgVelocity); 
            parameterRowBuilder.setMaxVelocity(maxVelocity); 
            parameterRowBuilder.setMotionTime(movementTime); 
            parameterRowBuilder.setStoppingTime(parkingTime); 
            parameterRowBuilder.setAbsenceTime(nodataTime); 

            responseBuilder.addRows(parameterRowBuilder);

            if (++messageCounter % Constants.IN_BLOCK_MESSAGES_LIMIT == 0) {
                responseBuilder.setBlockState(VelocityTimeParametersResponse.State.NORMAL);
                jmsBridge.send(responseBuilder.build());
                responseBuilder = makeResponseBuilder(blockNmb++, requestId);
            }

        }
        responseBuilder.setBlockState(VelocityTimeParametersResponse.State.LAST);
        jmsBridge.send(responseBuilder.build());

    }
    
    private ValueMultiplySpeed getNextSpeedData(Iterator<MessageLite> speedIterator) throws InvalidProtocolBufferException{
        ValueMultiplySpeed speed = null;
        if(speedIterator.hasNext()){
            speed = ValueMultiplySpeed.parseFrom(speedIterator.next().toByteString());
        }
  
        return speed;
    }
    
    private int getChunckId(int messageTime){
        return messageTime - (messageTime % 3600);
    }
    
    private String getCacheKey(int vehicleId, int messageTime){
        return String.format("src%d:mtp%d:summaryReport:cache", vehicleId,messageTime);
    }
    


    private VelocityTimeParametersResponse.Builder makeResponseBuilder(int blockNum, String requestId) {

        VelocityTimeParametersResponse.Builder responseBuilder = VelocityTimeParametersResponse.newBuilder()

                .setMessageType(RESPONSE_MESSAGE_TYPE)
                .setInstanceId(requestId)
                .setBlockNumber(blockNum);

        return responseBuilder;
    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:movementStates:settings", sourceId);

    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                jmsBridge.registerServer(RequestServer217.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                super.visit(signal);    //To change body of overridden methods use File | Settings | File Templates.
            }
        });
    }

    private class MovementState implements Comparable {

        private final Integer messageTime;
        private final Boolean startParking;

        public MovementState(int messageTime, boolean startParking) {
            this.messageTime = messageTime;
            this.startParking = startParking;
        }

        @Override
        public int compareTo(Object o) {

            MovementState obj = (MovementState) o;

            int result = messageTime.compareTo(obj.messageTime);
            if (result != 0) {
                return result;
            }
            return startParking.compareTo(obj.startParking);
        }

    }

}