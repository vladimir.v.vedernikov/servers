#!/bin/sh

DEST_PATH=../java

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/responses/reportcache
FILE_NAME=MovementDataCache.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

echo "done"
