/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.movementdatacache;

import ru.omnicomm.pegasus.messaging.data.responses.movementdatacache.MovementDataCacheParser.LocationDataItem;


/**
 *
 * @author alexander
 */
public class LocationData extends MovementReportDataItem{
    
    public static final int COEF = 10000000;
    
    private final double latitude;
    private final double longitude;

    public LocationData(double latitude, double longitude, int messageTime) {
        super(messageTime);
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public LocationDataItem getProtobuf() {
        return LocationDataItem.newBuilder()
                .setDataItemTime(this.getMessageTime())
                .setLatitude((int)(this.latitude*COEF))
                .setLongitude((int)(this.longitude*COEF))
                .build();
    }
    
}
