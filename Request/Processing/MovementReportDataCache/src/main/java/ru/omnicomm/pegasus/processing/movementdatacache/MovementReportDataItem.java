/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.movementdatacache;

import com.google.protobuf.MessageLite;

/**
 *
 * @author alexander
 */
public abstract class MovementReportDataItem implements Comparable{
    
    private final int messageTime;

    public MovementReportDataItem(int messageTime) {
        this.messageTime = messageTime;
    }

    public int getMessageTime() {
        return messageTime;
    }
    
    public abstract MessageLite getProtobuf();
    
    @Override
    public int compareTo(Object obj) {
        if (obj instanceof MovementReportDataItem) {
            return Integer.valueOf(this.messageTime).compareTo(((MovementReportDataItem) obj).messageTime);
        } else {
            throw new IllegalArgumentException("Argument should be instance of "+this.getClass().getName());
        }
    }
    
    
}
