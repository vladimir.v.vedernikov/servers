/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.movementdatacache;

import ru.omnicomm.pegasus.messaging.data.responses.movementdatacache.MovementDataCacheParser.SpeedDataItem;


/**
 *
 * @author alexander
 */
public class SpeedData extends MovementReportDataItem {
    
    private final int value;

    public SpeedData(int messageTime, int value) {
        super(messageTime);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public SpeedDataItem getProtobuf() {
        
        return SpeedDataItem.newBuilder()
                .setDataItemTime(this.getMessageTime())
                .setValue(value)
                .build();
        
    }

    
    
    

}
