/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.movementdatacache;

import com.google.protobuf.MessageLite;
import java.util.*;
import ru.omnicomm.pegasus.messaging.data.responses.movementdatacache.MovementDataCacheParser.MovementDataCache;

/**
 *
 * @author alexander
 */
public class MovementReportDataCache extends MovementReportDataItem{
    
    private final int sourceId;
    private static final int MESSAGE_TYPE = MovementDataCache.getDefaultInstance().getMessageType();
    
    private final Map<Integer,List<SpeedData>> speedCache = new TreeMap<Integer,List<SpeedData>>();
    private final Map<Integer,List<LocationData>> locationCache = new TreeMap<Integer,List<LocationData>>();

    public MovementReportDataCache(int sourceId, int messageTime) {
        super(messageTime);
        this.sourceId = sourceId;
    }

    public int getSourceId() {
        return sourceId;
    }
    
    public List<SpeedData> getSpeedData(int messageTime){
        return this.speedCache.get(messageTime);
    }
    
    public List<LocationData> getLocationData(int messageTime){
        return this.locationCache.get(messageTime);
    }
    
    public void setSpeedData(int messageTime, List<SpeedData> speedData){
        this.speedCache.put(messageTime, speedData);
    }
    
    public void setLocationData(int messageTime, List<LocationData> locationData){
        this.locationCache.put(messageTime, locationData);
    }

    @Override
    public MessageLite getProtobuf() {
        
        MovementDataCache.Builder builder = MovementDataCache.newBuilder()
                
                .setSourceId(this.sourceId)
                .setMessageType(MESSAGE_TYPE)
                .setMessageTime(this.getMessageTime());
                
        Iterator<Integer> speedKeys = this.speedCache.keySet().iterator();
        Iterator<Integer> locationKeys = this.locationCache.keySet().iterator();
        
        while(speedKeys.hasNext()){
            List<SpeedData> speedList = this.speedCache.get(speedKeys.next());
            Collections.sort(speedList);
            for(SpeedData s : speedList){
                builder.addSpeed(s.getProtobuf());
            }
        }
        
        while(locationKeys.hasNext()){
            List<LocationData> locationList = this.locationCache.get(locationKeys.next());
            Collections.sort(locationList);
            for(LocationData l : locationList){
                builder.addLocation(l.getProtobuf());
            }
        }
        
        
        return builder.build();
    }
    
    
    
    
}
