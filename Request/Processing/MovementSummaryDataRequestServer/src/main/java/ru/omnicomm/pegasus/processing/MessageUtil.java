/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev Date: 28.09.12
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.ByteString;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.MUIString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class MessageUtil {

    public static SelectRequest buildSelectRequest(long entityType, List<QueryItem> filters) {
        return buildSelectRequest(null, null, entityType, filters, null, null);
    }

    public static SelectRequest buildSelectRequest(String locale, Integer blockId, long entityType, List<QueryItem> filters, Integer startTime, Integer endTime) {
        SelectRequest.Builder builder = SelectRequest.newBuilder();
        builder.setMessageType(6).setEntityType(String.valueOf(entityType)).addAllFilter(filters);
        if (locale != null) {
            builder.setLocale(locale);
        }
        if (blockId != null) {
            builder.setMessageBlock(blockId);
        }
        else {
            builder.setMessageBlock(1);
        }
        if (startTime != null) {
            builder.setStartTime(startTime);
        }
        if (endTime != null) {
            builder.setEndTime(endTime);
        }
        return builder.build();
    }

    public static QueryItem buildQueryItem(long attributeId, Operation op, ParameterValue value) {
        return QueryItem.newBuilder().setAttributeId(String.valueOf(attributeId)).setOperation(op).setValue(value).build();
    }

    public static ParameterValue buildParameterValue(ParameterValueType type, Object value) {
        ParameterValue.Builder builder = ParameterValue.newBuilder();
        switch (type) {
            case STRING:
                builder.setStringValue((String) value);
                break;
            case MUISTRING:
                builder.setMuistringValue((MUIString) value);
                break;
            case OBJECT_LINK:
                builder.setObjectLinkValue((ObjectLink) value);
                break;
            case LONG:
                builder.setLongValue((Long) value);
                break;
            case ARRAY:
                builder.setValueArray((ValueArray) value);
                break;
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }
        return builder.build();
    }

    public static ValueArray buildValueArray(List<ParameterValue> values) {
        return ValueArray.newBuilder()
                .addAllValues(values).build();
    }

    public static ByteString buildByteString(byte[] bytes) {
        return ByteString.copyFrom(bytes);
    }

    public static Map<Long, Map<Long, List<Parameter>>> groupParameters(List<Parameter> parameters) {
        Map<Long, Map<Long, List<Parameter>>> result = new HashMap<>();
        for (Parameter parameter : parameters) {
            long entityId = Long.parseLong(parameter.getEntityId());
            long attributeId = Long.parseLong(parameter.getAttributeId());
            Map<Long, List<Parameter>> entity = result.get(entityId);
            if (entity == null) {
                entity = new HashMap<>();
                result.put(entityId, entity);
            }
            List<Parameter> sameAttrIdParams = entity.get(attributeId);
            if (sameAttrIdParams == null) {
                sameAttrIdParams = new ArrayList<>();
                entity.put(attributeId, sameAttrIdParams);
            }
            sameAttrIdParams.add(parameter);
        }
        return result;
    }

    private MessageUtil() {
    }
}
