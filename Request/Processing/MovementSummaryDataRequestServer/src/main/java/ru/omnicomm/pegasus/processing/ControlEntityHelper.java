/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev Date: 01.08.12
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class ControlEntityHelper {

    private static final long SOURCE_ID_ENTITY_TYPE = 1200;
    private static final long MESSAGE_TYPE_ENTITY_TYPE = 1300;
    private static final long MESSAGETYPE_NAME_ID = 1304;
    private static final long CONTROLLED_PARAMETER_ENTITY_TYPE = 1500;
    private static final long CONTROLLED_PARAMETER_NAME_ID = 1501;
    private static final long CONTROLLED_UNIT_ENTITY_TYPE = 1550;
    private static final long CONTROLLED_UNIT_CUT_PARAMETER_ID = 1551;
    private static final long CONTROLLED_UNIT_SOURCE_PARAMETER_ID = 1552;
    private static final long CONTROLLED_UNIT_CO_PARAMETER_ID = 1553;
    private static final long CONTROLLED_UNIT_TYPE_ENTITY_TYPE = 1600;
    private static final long CONTROLLED_UNIT_TYPE_NAME_PARAMETER_ID = 1601;
    private static final long ATOMIC_SENSOR_ENTITY_ID = 1750;
    private static final long ATOMIC_SENSOR_SOURCE_ID = 1753;
    private static final long ATOMIC_SENSOR_CP_ID = 1754;
    private BusinessLogicService blService;

    public ControlEntityHelper(BusinessLogicService blService) {
        this.blService = blService;
    }

    /**
     * Осуществляет поиск {@code источников данных (sourceId)} по переданным параметрам {@code cutName} и {@code coId} и
     * возвращает первый найденный или {@code null}.
     *
     * @param cutName внутреннее имя агрегата.
     * @param cpName  внутреннее имя параметра.
     * @param coId    идентификатор контролируемого объекта.
     * @return первый найденный по параметрам {@code sourceId} или {@code null} если ничего не найдено.
     * @throws RequestProcessingException при ошибках во время выполнения запросов к {@code BasePlatform}.
     */
    public Integer selectFirstSourceId(String cutName, String cpName, long coId) throws RequestProcessingException {
        Integer result = null;
        Collection<Long> sources = selectSources(cutName, coId);
        if (!sources.isEmpty()) {
            Long cpId = selectCPIdByName(cpName);
            if (cpId == null) {
                throw new RequestProcessingException("Cannot find 'ControlledParameter' with name " + cpName);
            }
            List<ParameterValue> sourceIDs = new ArrayList<>(sources.size());
            for (Long sourceId : sources) {
                ParameterValue parameterValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, sourceId.toString());
                sourceIDs.add(parameterValue);
            }
            ValueArray valueArray = MessageUtil.buildValueArray(sourceIDs);
            ParameterValue parameterValue = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, valueArray);
            QueryItem sourceIdsItem = MessageUtil.buildQueryItem(ATOMIC_SENSOR_SOURCE_ID, Operation.IN, parameterValue);
            ParameterValue cpIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, cpId.toString());
            QueryItem cpIdItem = MessageUtil.buildQueryItem(ATOMIC_SENSOR_CP_ID, Operation.EQ, cpIdValue);
            SelectRequest selectRequest = MessageUtil.buildSelectRequest(ATOMIC_SENSOR_ENTITY_ID, Arrays.asList(sourceIdsItem, cpIdItem));
            try {
                byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
                ResultSet resultSet = ResultSet.parseFrom(responseBytes);
                List<Parameter> parameters = resultSet.getParametersList();
                if (!parameters.isEmpty()) {
                    result = Integer.parseInt(parameters.get(0).getEntityId());
                }
            }
            catch (BusinessLogicServiceException | InvalidProtocolBufferException | NumberFormatException ex) {
                throw new RequestProcessingException("Cannot get SourceId: cutName=" + cutName + "; cpName=" + cpName + "; coId=" + coId, ex);
            }
        }
        return result;
    }

    /**
     * Осуществляет поиск {@code источников данных (sourceId)} по переданным параметрам {@code cutName} и {@code coId}.
     *
     * @param cutName внутреннее имя агрегата.
     * @param coId    идентификатор контролируемого объекта.
     * @return список найденных по параметрам {@code sourceId}. Никогда не возвращает {@code null}.
     * @throws RequestProcessingException при ошибках во время выполнения запросов к {@code BasePlatform}.
     */
    public Collection<Long> selectSources(String cutName, long coId) throws RequestProcessingException {
        Long cutId = selectCUTIdByName(cutName);
        if (cutId == null) {
            throw new RequestProcessingException("Cannot find 'ControlledUnitType' with name " + cutName);
        }
        Set<Long> sourceIdsByCU = selectSourceIdsByCU(coId, cutId);
        return selectSourcesByIdentifiers(sourceIdsByCU);
    }

    /**
     * Выполняет запрос по объектам сущности {@code 'SourceId'} у которых {@code entityId} присутствует в списке
     * идентификаторов {@code sourceIdsByCU}.
     *
     * @param sourceIdsByCU список идентификаторов сущности {@code 'SourceId'}.
     * @return список {@code 'sourceId'}.
     * @throws RequestProcessingException
     */
    public Set<Long> selectSourcesByIdentifiers(Set<Long> sourceIdsByCU) throws RequestProcessingException {
        Set<Long> sources = new HashSet<>(sourceIdsByCU.size());
        if (sourceIdsByCU.isEmpty()) {
            return sources;
        }

        List<ParameterValue> sourcesIdList = new ArrayList<>();
        for (Long sourceId : sourceIdsByCU) {
            ParameterValue sourceIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, sourceId.toString());
            sourcesIdList.add(sourceIdValue);
        }
        ValueArray valueArray = MessageUtil.buildValueArray(sourcesIdList);
        ParameterValue filterValue = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, valueArray);

        QueryItem sourceIdItem = MessageUtil.buildQueryItem(Long.parseLong(BusinessLogicService.ENTITY_ID_ATTRIBUTE), Operation.IN, filterValue);
        SelectRequest sourcesQuery = MessageUtil.buildSelectRequest(SOURCE_ID_ENTITY_TYPE, Arrays.asList(sourceIdItem));
        try {
            byte[] resultBytes = blService.selectByParams(sourcesQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();

            for (Parameter parameter : parameters) {
                sources.add(Long.parseLong(parameter.getEntityId()));
            }
        }
        catch (BusinessLogicServiceException e) {
            throw new RequestProcessingException("Cannot execute 'selectSourcesByIdentifiers' query", e);
        }
        catch (InvalidProtocolBufferException e) {
            throw new RequestProcessingException("Cannot parse query result", e);
        }

        return sources;
    }

    public Long selectCUTIdByName(String cutName) throws RequestProcessingException {
        if (cutName == null) {
            throw new NullPointerException("Parameter cutName is null");
        }

        final ParameterValue cutNameValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, cutName);
        final QueryItem cutNameItem = MessageUtil.buildQueryItem(CONTROLLED_UNIT_TYPE_NAME_PARAMETER_ID, Operation.EQ, cutNameValue);
        final SelectRequest cutQuery = MessageUtil.buildSelectRequest(CONTROLLED_UNIT_TYPE_ENTITY_TYPE, Arrays.asList(cutNameItem));

        try {
            byte[] resultBytes = blService.selectByParams(cutQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();
            if (parameters.isEmpty()) {
                return null;
            }
            else {
                return Long.valueOf(parameters.get(0).getEntityId());
            }
        }
        catch (BusinessLogicServiceException e) {
            throw new RequestProcessingException("Cannot execute 'controlled unit type by name' query", e);
        }
        catch (InvalidProtocolBufferException e) {
            throw new RequestProcessingException("Cannot parse query result", e);
        }
    }

    public Set<Long> selectSourceIdsByCU(long coId, long cutId) throws RequestProcessingException {
        final ParameterValue coIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, String.valueOf(coId));
        final QueryItem coBindItem = MessageUtil.buildQueryItem(CONTROLLED_UNIT_CO_PARAMETER_ID, Operation.EQ, coIdValue);
        final ParameterValue cutIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, String.valueOf(cutId));
        final QueryItem cutBindItem = MessageUtil.buildQueryItem(CONTROLLED_UNIT_CUT_PARAMETER_ID, Operation.EQ, cutIdValue);
        final SelectRequest cutQuery = MessageUtil.buildSelectRequest(CONTROLLED_UNIT_ENTITY_TYPE, Arrays.asList(coBindItem, cutBindItem));
        Set<Long> result = new HashSet<>();
        try {
            byte[] resultBytes = blService.selectByParams(cutQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();
            Map<Long, Map<Long, List<Parameter>>> cuEntities = MessageUtil.groupParameters(parameters);
            for (Long cuId : cuEntities.keySet()) {
                Map<Long, List<Parameter>> attributes = cuEntities.get(cuId);
                List<Parameter> values = attributes.get(CONTROLLED_UNIT_SOURCE_PARAMETER_ID);
                for (Parameter value : values) {
                    final ParameterValue parameterValue = value.getValue();
                    try {
                        ObjectLink links = parameterValue.getObjectLinkValue();
                        for (String sourceId : links.getLinksList()) {
                            result.add(Long.parseLong(sourceId));
                        }
                    }
                    catch (ClassCastException e) {
                        final String errMsg = "Controlled unit bind source parameter must be type of ObjectLink but " + parameterValue;
                        throw new RequestProcessingException(errMsg, e);
                    }
                }

            }
        }
        catch (BusinessLogicServiceException e) {
            throw new RequestProcessingException("Cannot execute 'controlled unit type by name' query", e);
        }
        catch (InvalidProtocolBufferException e) {
            throw new RequestProcessingException("Cannot parse query result", e);
        }
        return result;
    }

    public String selectMessageTypeIdByName(String name) throws RequestProcessingException {
        String result = null;
        ParameterValue parameterValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, name);
        QueryItem queryItem = MessageUtil.buildQueryItem(MESSAGETYPE_NAME_ID, Operation.EQ, parameterValue);
        SelectRequest selectRequest = MessageUtil.buildSelectRequest(MESSAGE_TYPE_ENTITY_TYPE, Arrays.asList(queryItem));
        try {
            byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(responseBytes);
            List<Parameter> parameters = resultSet.getParametersList();
            if (!parameters.isEmpty()) {
                result = parameters.get(0).getEntityId();
            }
        }
        catch (BusinessLogicServiceException | InvalidProtocolBufferException ex) {
            throw new RequestProcessingException("Cannot get 'MessageTypeId': name=" + name, ex);
        }
        return result;
    }

    private Long selectCPIdByName(String cpName) throws RequestProcessingException {
        if (cpName == null) {
            throw new NullPointerException("Parameter cpName is null");
        }

        final ParameterValue cpNameValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, cpName);
        final QueryItem cpNameItem = MessageUtil.buildQueryItem(CONTROLLED_PARAMETER_NAME_ID, Operation.EQ, cpNameValue);
        final SelectRequest cpQuery = MessageUtil.buildSelectRequest(CONTROLLED_PARAMETER_ENTITY_TYPE, Arrays.asList(cpNameItem));

        try {
            byte[] resultBytes = blService.selectByParams(cpQuery.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(resultBytes);
            List<Parameter> parameters = resultSet.getParametersList();
            if (parameters.isEmpty()) {
                return null;
            }
            else {
                return Long.valueOf(parameters.get(0).getEntityId());
            }
        }
        catch (BusinessLogicServiceException e) {
            throw new RequestProcessingException("Cannot execute 'controlled unit type by name' query", e);
        }
        catch (InvalidProtocolBufferException e) {
            throw new RequestProcessingException("Cannot parse query result", e);
        }
    }
}
