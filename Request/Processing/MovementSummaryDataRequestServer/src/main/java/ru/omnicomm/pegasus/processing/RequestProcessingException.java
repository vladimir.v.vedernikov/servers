/*
 * ru.omnicomm.pegasus.processing.RequestProcessingException
 * 
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * For internal use only.
 * 
 * Author: Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 * Version: 
 */
package ru.omnicomm.pegasus.processing;

/**
 *
 * @author Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 */
public class RequestProcessingException extends Exception {

    public RequestProcessingException(String message) {
        super(message);
    }

    RequestProcessingException(String message, Throwable t) {
        super(message, t);
    }
}
