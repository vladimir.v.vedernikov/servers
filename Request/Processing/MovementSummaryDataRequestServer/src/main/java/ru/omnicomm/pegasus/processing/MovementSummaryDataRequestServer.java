/*
 * ru.omnicomm.pegasus.processing.MovementSummaryDataRequestServer
 * 
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * For internal use only.
 * 
 * Author: Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 * Version: 
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser.GetMovSumDataRequest;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser.GetMovSumDataResponse;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser.State;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.MUIString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ValueArray;
import ru.omnicomm.pegasus.messaging.online.analogSensor.AGRAnalogParser;
import ru.omnicomm.pegasus.messaging.online.analogSensor.AnalogValueParser;
import ru.omnicomm.pegasus.messaging.online.location.LocationDataParser;
import ru.omnicomm.pegasus.messaging.online.movement.MovementStatesParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.TimeConverter;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 *
 * @author Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation(value = "MovementSummaryDataRequestServerImplementation")
public class MovementSummaryDataRequestServer implements ServerImplementation {

    private static final int REQUEST_MESSAGE_TYPE = 447;
    private static final int RESPONSE_MESSAGE_TYPE = 448;
    private static final int ANALOG_VALUE_CORRECTED_MESSAGE_TYPE = 505;
    private static final int AGR_ANALOG_WORK_MESSAGE_TYPE = 509;
    private static final int AGR_ANALOG_MAX_MESSAGE_TYPE = 510;
    private static final int PARKING_START_MESSAGE_TYPE = 515;
    private static final int PARKING_END_MESSAGE_TYPE = 516;
    private static final int GPS_RAW_MESSAGE_TYPE = 525;
    private static final int GPS_CORRECT_MESSAGE_TYPE = 526;
    private static final long DRIVER_ENTITY_TYPE = 1650;
    private static final long DRIVER_FULL_NAME = 1651;
    private static final long VEHICLE_ENTITY_TYPE = 1800;
    private static final long VEHICLE_STATE_NUMBER = 1801;
    private static final long VEHICLE_DRIVER_LIST = 1804;
    private static final long VEHICLE_GARAGE_NUMBER = 1809;
    private static final String CONTROLLED_PARAMETER_NAME_1 = "Coordinates";
    private static final String CONTROLLED_PARAMETER_NAME_2 = "AnalogSensorVelocity";
    private static final String CONTROLLED_UNIT_TYPE_NAME = "CO";
    private static final int IN_BLOCK_MESSAGES_LIMIT = 100;
    private static final Logger logger = LoggerFactory.getLogger();
    private Server self;
    private JmsBridgeService jmsBridge;
    private BusinessLogicService blService;
    private ControlEntityHelper baseHelper;
    private DataStorageService dataStorage;
    private SettingsStorageService settingsStorage;

    @ServerConstructor
    public MovementSummaryDataRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService,
                                            DataStorageService dataStorage, SettingsStorageService settingsStorage) {
        this.jmsBridge = jmsBridge;
        this.blService = blService;
        this.baseHelper = new ControlEntityHelper(blService);
        this.dataStorage = dataStorage;
        this.settingsStorage = settingsStorage;
    }

    @Override
    @SuppressWarnings("UseSpecificCatch")
    public void onMessage(Server source, MessageLite message) {
        GetMovSumDataRequest request;
        try {
            request = GetMovSumDataRequest.parseFrom(message.toByteArray());
        }
        catch (InvalidProtocolBufferException ex) {
            logger.error("Invalid message.", ex);
            return;
        }
        try {
            processMessage(request);
        }
        catch (Exception ex) {
            logger.error("Can't process data", ex);
            try {
                sendJmsMessage(getDefectiveResponse(request));
            }
            catch (JmsBridgeServiceException ex1) {
                logger.error("Cannot send response message to jmsBridge", ex1);
            }
        }
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) throws RuntimeException {
                logger.error(null, signal.getException());
            }

            @Override
            public void visit(Init signal) throws RuntimeException {
                self = signal.getSelf();
                jmsBridge.registerServer(MovementSummaryDataRequestServer.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) throws RuntimeException {
                jmsBridge.unregisterServer(MovementSummaryDataRequestServer.class, self);
            }
        });
    }

    @SuppressWarnings({"null", "ConstantConditions"})
    private void processMessage(GetMovSumDataRequest request) throws RequestParameterException,
                                                                     BusinessLogicServiceException,
                                                                     InvalidProtocolBufferException,
                                                                     JmsBridgeServiceException {
        String requestId = request.getInstanceId();
        int blockId = request.getMessageBlock();
        String locale = request.getLocale();
        int startTime = request.getStartTime();
        int endTime = request.getEndTime();
        int mileageType = request.getMileageCalculationMethod();
        QueryItem vehicleIdItem = buildVehicleIdQueryItem(request);
        if (vehicleIdItem == null) {
            jmsBridge.send(getEmptyResponse(request));
            return;
        }
        SelectRequest queryRequest = MessageUtil.buildSelectRequest(locale, blockId, VEHICLE_ENTITY_TYPE, Arrays.asList(vehicleIdItem), startTime, endTime);
        byte[] responseBytes = blService.selectByParams(queryRequest.toByteArray());
        ResultSet response = ResultSet.parseFrom(responseBytes);
        // Map<vehicleID, Map<attributeID, List<Parameter>>>
        Map<Long, Map<Long, List<Parameter>>> vehicleParameterMap = MessageUtil.groupParameters(response.getParametersList());
        int blockNum = 0, messageCounter = 0;
        GetMovSumDataResponse.Builder responseBuilder = makeResponseBuilder(blockNum++, requestId);
        Iterator<Long> vehicleIdIterator = vehicleParameterMap.keySet().iterator();
        while (vehicleIdIterator.hasNext()) {
            Long vehicleId = vehicleIdIterator.next();
            GetMovSumDataResponse.SumData.Builder sumDataBuilder = GetMovSumDataResponse.SumData.newBuilder();
            sumDataBuilder.setCoId(vehicleId);
            Map<Long, List<Parameter>> parameterMap = vehicleParameterMap.get(vehicleId);
            String stateNumber = getParameterValue(VEHICLE_STATE_NUMBER, ParameterValueType.STRING, parameterMap, locale);
            if (stateNumber != null) {
                sumDataBuilder.setStateNumber(stateNumber);
            }
            String garageNumber = getParameterValue(VEHICLE_GARAGE_NUMBER, ParameterValueType.STRING, parameterMap, locale);
            if (garageNumber != null) {
                sumDataBuilder.setGarageNumber(garageNumber);
            }
            List<ObjectLink> objectLinks = getParameterValues(VEHICLE_DRIVER_LIST, ParameterValueType.OBJECT_LINK, parameterMap, locale);
            List<Long> driverIDs = new ArrayList<>();
            if (objectLinks != null) {
                for (ObjectLink objectLink : objectLinks) {
                    List<String> links = objectLink.getLinksList();
                    for (String link : links) {
                        try {
                            long driverID = Long.parseLong(link);
                            if (!driverIDs.contains(driverID)) {
                                driverIDs.add(driverID);
                            }
                        }
                        catch (NumberFormatException numberFormatException) {
                            logger.warn("Cannot parse link into driverID: link=" + link + ": " + numberFormatException.getMessage());
                        }
                    }
                }
            }
            if (!driverIDs.isEmpty()) {
                QueryItem driversQueryItem = buildIdListQueryItem(driverIDs);
                queryRequest = MessageUtil.buildSelectRequest(locale, blockId, DRIVER_ENTITY_TYPE, Arrays.asList(driversQueryItem), startTime, endTime);
                responseBytes = blService.selectByParams(queryRequest.toByteArray());
                response = ResultSet.parseFrom(responseBytes);
                // Map<driverID, Map<attributeID, List<Parameter>>>
                Map<Long, Map<Long, List<Parameter>>> driversParameterMap = MessageUtil.groupParameters(response.getParametersList());
                for (Long driverID : driversParameterMap.keySet()) {
                    parameterMap = driversParameterMap.get(driverID);
                    String driverName = getParameterValue(DRIVER_FULL_NAME, ParameterValueType.MUISTRING, parameterMap, locale);
                    if (driverName != null) {
                        sumDataBuilder.addDrivers(driverName);
                    }
                }
            }
            try {
                Integer coordinatesSourceId = baseHelper.selectFirstSourceId(CONTROLLED_UNIT_TYPE_NAME, CONTROLLED_PARAMETER_NAME_1, vehicleId);
                Integer speedSourceId = baseHelper.selectFirstSourceId(CONTROLLED_UNIT_TYPE_NAME, CONTROLLED_PARAMETER_NAME_2, vehicleId);
                int[] movementParkingTimes = null;
                if (coordinatesSourceId == null) {
                    logger.info("'Coordinates' sourceId not found: coId=" + vehicleId);
                }
                else {
                    int idleTime = getIdleTime(blockId, coordinatesSourceId, startTime, endTime);
                    sumDataBuilder.setDataAbsenceTime(idleTime);
                    movementParkingTimes = getMovementParkingTimes(blockId, mileageType == 0
                                                                            ? speedSourceId
                                                                            : coordinatesSourceId,
                                                                   startTime,
                                                                   endTime);
                    if (movementParkingTimes != null) {
                        sumDataBuilder.setMovementTime(movementParkingTimes[0]);
                        sumDataBuilder.setParkingTime(movementParkingTimes[1]);
                    }
                }
                int parkingTime = movementParkingTimes != null ? movementParkingTimes[1] : 0;
                double[] mileageAvgSpeed;
                if (mileageType == 0) {
                    mileageAvgSpeed = getMileageAvgSpeedBySpeed(blockId, speedSourceId, startTime, endTime, parkingTime);
                }
                else {
                    mileageAvgSpeed = getMileageAvgSpeedByCoord(blockId, coordinatesSourceId, startTime, endTime, parkingTime);
                }
                sumDataBuilder.setMileage(mileageAvgSpeed[0]);
                sumDataBuilder.setAverageVelocity(mileageAvgSpeed[1]);
                double maxSpeed = getMaxSpeed(blockId, speedSourceId, startTime, endTime);
                sumDataBuilder.setMaxVelocity(maxSpeed);
                responseBuilder.addSumData(sumDataBuilder);
                if (++messageCounter % IN_BLOCK_MESSAGES_LIMIT == 0) {
                    responseBuilder.setBlockState(vehicleIdIterator.hasNext() ? State.NORMAL : State.LAST);
                    jmsBridge.send(responseBuilder.build());
                    responseBuilder = vehicleIdIterator.hasNext() ? makeResponseBuilder(blockNum++, requestId) : null;
                }
            }
            catch (RequestProcessingException | DataStorageServiceException | InvalidProtocolBufferException |
                   SettingsStorageServiceException ex) {
                responseBuilder.setBlockState(State.DEFECTIVE);
                responseBuilder.setErrorMessage(ex.getMessage());
                jmsBridge.send(responseBuilder.build());
                responseBuilder = vehicleIdIterator.hasNext() ? makeResponseBuilder(blockNum++, requestId) : null;
                logger.info("Summary report error", ex);
            }
        }
        if (responseBuilder != null) {
            responseBuilder.setBlockState(State.LAST);
            jmsBridge.send(responseBuilder.build());
        }
    }

    private void sendJmsMessage(MessageLite message) throws JmsBridgeServiceException {
        logger.info("send jms message" + message);
        jmsBridge.send(message);
    }

    private MessageLite getDefectiveResponse(GetMovSumDataRequest request) {
        GetMovSumDataResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(State.DEFECTIVE);
        return builder.build();
    }

    private MessageLite getEmptyResponse(GetMovSumDataRequest request) {
        GetMovSumDataResponse.Builder builder = getResponseBuilder(request, 0);
        builder.setBlockState(State.LAST);
        return builder.build();
    }

    private GetMovSumDataResponse.Builder getResponseBuilder(GetMovSumDataRequest request, int blockNumber) {
        GetMovSumDataResponse.Builder builder = GetMovSumDataResponse.newBuilder();
        builder.setInstanceId(request.getInstanceId());
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setBlockNumber(blockNumber);
        return builder;
    }

    private QueryItem buildVehicleIdQueryItem(GetMovSumDataRequest request) {
        List<Long> vehicleIds = request.getCoListList();
        return buildIdListQueryItem(vehicleIds);
    }

    private QueryItem buildIdListQueryItem(List<Long> ids) {
        QueryItem queryItem = null;
        if (!ids.isEmpty()) {
            QueryItem.Operation op;
            ParameterValue parameterValue;
            if (ids.size() == 1) {
                op = QueryItem.Operation.EQ;
                parameterValue = buildParameterValue(ParameterValueType.STRING, ids.get(0).toString());
            }
            else {
                op = QueryItem.Operation.IN;
                List<ParameterValue> values = new ArrayList<>();
                for (Long vehicleId : ids) {
                    values.add(buildParameterValue(ParameterValueType.STRING, vehicleId.toString()));
                }
                parameterValue = buildParameterValue(ParameterValueType.ARRAY, buildValueArray(values));
            }
            queryItem = buildQueryItem(BusinessLogicService.ENTITY_ID_ATTRIBUTE, op, parameterValue);
        }
        return queryItem;
    }

    private ParameterValue buildParameterValue(ParameterValueType parameterValueType, Object value) {
        ParameterValue.Builder builder = ParameterValue.newBuilder();
        switch (parameterValueType) {
            case STRING:
                builder.setStringValue((String) value);
                break;
            case LONG:
                builder.setLongValue((long) value);
                break;
            case ARRAY:
                builder.setValueArray((ValueArray) value);
                break;
            default:
                throw new IllegalArgumentException("Unsupported parameter value type");
        }
        return builder.build();
    }

    private ValueArray buildValueArray(List<ParameterValue> values) {
        return ValueArray.newBuilder().addAllValues(values).build();
    }

    private QueryItem buildQueryItem(String attributeId, QueryItem.Operation op, ParameterValue value) {
        return QueryItem.newBuilder().setAttributeId(attributeId).setOperation(op).setValue(value).build();
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> getParameterValues(long attrId, ParameterValueType type, Map<Long, List<Parameter>> attributes, String locale) {
        List<T> result = null;
        List<Parameter> parameters = attributes.get(attrId);
        if (parameters == null) {
            return result;
        }
        result = new ArrayList<>(parameters.size());
        for (Parameter parameter : parameters) {
            result.add((T) extractValue(type, parameter, locale));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private <T> T getParameterValue(long attrId, ParameterValueType type, Map<Long, List<Parameter>> attributes, String locale) {
        T result = null;
        List<Parameter> parameters = attributes.get(attrId);
        if (parameters == null || parameters.isEmpty()) {
            return result;
        }
        Parameter parameter = parameters.get(0);
        return (T) extractValue(type, parameter, locale);
    }

    private Object extractValue(ParameterValueType type, Parameter parameter, String locale) {
        ParameterValue value = parameter.getValue();
        switch (type) {
            case STRING:
                return value.getStringValue();
            case MUISTRING:
                String muiStr = null;
                for (MUIString.MUIValue muiValue : value.getMuistringValue().getStrValuesList()) {
                    if (locale.toLowerCase().equals(muiValue.getLangID())) {
                        muiStr = muiValue.getValue();
                        break;
                    }
                }
                return muiStr;
            case OBJECT_LINK:
                return value.getObjectLinkValue();
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }
    }

    private int getIdleTime(int blockId, Integer sourceId, int startTime, int endTime) throws DataStorageServiceException,
                                                                                              InvalidProtocolBufferException {
        int idleMax = 480;
        try {
            String[] parameters = settingsStorage.get(blockId, "IDLEMAX-" + sourceId);
            if (parameters != null && parameters.length > 0) {
                idleMax = Integer.parseInt(parameters[0]);
            }
        }
        catch (SettingsStorageServiceException | NumberFormatException settingsStorageServiceException) {
        }
        Iterator<MessageLite> gpsRawIterator = new MessageLiteIterator(blockId, sourceId, GPS_RAW_MESSAGE_TYPE, startTime, endTime, dataStorage);
        List<Integer> idleTimes = new ArrayList<>();
        while (gpsRawIterator.hasNext()) {
            MessageLite message = gpsRawIterator.next();
            LocationDataParser.GPSRaw gpsRaw = LocationDataParser.GPSRaw.parseFrom(message.toByteArray());
            idleTimes.add((int) (gpsRaw.getTimeStamp() / 1000));
        }
        if (idleTimes.isEmpty()) {

            int idleStartTime = 0, idleEndTime = TimeConverter.getTimeStamp();
            MessageLite ml = getPreviousItem(blockId, sourceId, GPS_RAW_MESSAGE_TYPE, startTime);
            if (ml != null) {
                LocationDataParser.GPSRaw gpsRaw = LocationDataParser.GPSRaw.parseFrom(ml.toByteArray());
                idleStartTime = (int) (gpsRaw.getTimeStamp() / 1000);
            }
            ml = getNextItem(blockId, sourceId, GPS_RAW_MESSAGE_TYPE, endTime);
            if (ml != null) {
                LocationDataParser.GPSRaw gpsRaw = LocationDataParser.GPSRaw.parseFrom(ml.toByteArray());
                idleEndTime = (int) (gpsRaw.getTimeStamp() / 1000);
            }
            idleTimes.add(0, idleStartTime);
            idleTimes.add(idleEndTime);
        }
        Collections.sort(idleTimes);
        int result = 0;
        for (int i = 1; i < idleTimes.size(); i++) {
            int curr = idleTimes.get(i);
            int prev = idleTimes.get(i - 1);
            if (curr - prev < idleMax) {
                continue;
            }
            result += Math.min(curr, endTime) - Math.max(prev, startTime);
        }
        return result;
    }

    private int[] getMovementParkingTimes(int blockId, Integer coordinateSourceId, int startTime, int endTime) throws
            DataStorageServiceException, InvalidProtocolBufferException {
        Iterator<MessageLite> parkingStartIterator = new MessageLiteIterator(blockId, coordinateSourceId, PARKING_START_MESSAGE_TYPE, startTime, endTime, dataStorage);
        Iterator<MessageLite> parkingEndIterator = new MessageLiteIterator(blockId, coordinateSourceId, PARKING_END_MESSAGE_TYPE, startTime, endTime, dataStorage);
        List<TimeEntity> parkingTimes = new ArrayList<>();
        while (parkingStartIterator.hasNext()) {
            MessageLite message = parkingStartIterator.next();
            MovementStatesParser.ParkingSTART parkingStart = MovementStatesParser.ParkingSTART.parseFrom(message.toByteArray());
            parkingTimes.add(new TimeEntity(PARKING_START_MESSAGE_TYPE, parkingStart.getMessageTime()));
        }
        while (parkingEndIterator.hasNext()) {
            MessageLite message = parkingStartIterator.next();
            MovementStatesParser.ParkingEND parkingEnd = MovementStatesParser.ParkingEND.parseFrom(message.toByteArray());
            parkingTimes.add(new TimeEntity(PARKING_END_MESSAGE_TYPE, parkingEnd.getMessageTime()));
        }
        int[] result = null;
        if (parkingTimes.isEmpty()) {
            int parkingStartTime = 0, parkingEndTime = 0;
            MessageLite ml = getPreviousItem(blockId, coordinateSourceId, PARKING_START_MESSAGE_TYPE, startTime);
            if (ml != null) {
                MovementStatesParser.ParkingSTART parkingStart = MovementStatesParser.ParkingSTART.parseFrom(ml.toByteArray());
                parkingStartTime = parkingStart.getMessageTime();
            }
            if (parkingStartTime > 0) {
                ml = getPreviousItem(blockId, coordinateSourceId, PARKING_END_MESSAGE_TYPE, startTime);
                if (ml != null) {
                    MovementStatesParser.ParkingEND parkingEnd = MovementStatesParser.ParkingEND.parseFrom(ml.toByteArray());
                    parkingEndTime = parkingEnd.getMessageTime();
                }
                if (parkingEndTime > parkingStartTime) {
                    result = new int[]{endTime - startTime, 0};
                }
                else {
                    result = new int[]{0, endTime - startTime};
                }
            }
        }
        else {
            Collections.sort(parkingTimes);
            TimeEntity firstEntity = parkingTimes.get(0);
            if (firstEntity.getTime() > startTime) {
                TimeEntity fakeEntity = new TimeEntity(firstEntity.getType() == PARKING_START_MESSAGE_TYPE
                                                       ? PARKING_END_MESSAGE_TYPE
                                                       : PARKING_START_MESSAGE_TYPE,
                                                       startTime);
                parkingTimes.add(0, fakeEntity);
            }
            TimeEntity lastEntity = parkingTimes.get(parkingTimes.size() - 1);
            if (lastEntity.getTime() < endTime) {
                TimeEntity fakeEntity = new TimeEntity(lastEntity.getType() == PARKING_START_MESSAGE_TYPE
                                                       ? PARKING_END_MESSAGE_TYPE
                                                       : PARKING_START_MESSAGE_TYPE,
                                                       endTime);
                parkingTimes.add(fakeEntity);
            }
            int s1 = 0, s2 = 0;
            for (int i = 1; i < parkingTimes.size() - 1; i += 2) {
                s1 += parkingTimes.get(i).getTime() - parkingTimes.get(i - 1).getTime();
                s2 += parkingTimes.get(i + 1).getTime() - parkingTimes.get(i).getTime();
            }
            if (parkingTimes.get(0).getType() == PARKING_START_MESSAGE_TYPE) {
                result = new int[]{s2, s1};
            }
            else {
                result = new int[]{s1, s2};
            }
        }
        return result;
    }

    private MessageLite getPreviousItem(int blockId, Integer sourceId, int messageType, int startTime) throws
            DataStorageServiceException {
        MessageLite result = null;
        int minStartTime = dataStorage.getMinMessageTime(blockId, sourceId, messageType);
        if (minStartTime > 0 && minStartTime < startTime) {
            int t1, t2 = startTime;
            do {
                t1 = t2 - 86400;
                MessageLiteIterator iterator = new MessageLiteIterator(blockId, sourceId, messageType, t1, t2, dataStorage);
                while (iterator.hasNext()) {
                    result = iterator.next();
                }
                if (result != null) {
                    break;
                }
                t2 = t1;
            } while (t2 > minStartTime);
        }
        return result;
    }

    private MessageLite getNextItem(int blockId, Integer sourceId, int messageType, int time) throws
            DataStorageServiceException {
        MessageLite result = null;
        int maxEndTime = dataStorage.getMaxMessageTime(blockId, sourceId, messageType);
        if (maxEndTime > 0 && maxEndTime > time) {
            int t1 = time, t2;
            do {
                t2 = t1 + 86400;
                MessageLiteIterator iterator = new MessageLiteIterator(blockId, sourceId, messageType, t1, t2, dataStorage);
                if (iterator.hasNext()) {
                    result = iterator.next();
                    break;
                }
                t1 = t2;
            } while (t2 < maxEndTime);
        }
        return result;
    }

    private double[] getMileageAvgSpeedBySpeed(int blockId, Integer speedSourceId, int startTime, int endTime, int parkingTime) throws
            DataStorageServiceException, InvalidProtocolBufferException, SettingsStorageServiceException {
        int agrInterval = 600;
        try {
            String[] settings = settingsStorage.get(blockId, "AGRINTERVAL");
            if (settings != null && settings.length > 0) {
                agrInterval = Integer.parseInt(settings[0]);
            }
        }
        catch (SettingsStorageServiceException | NumberFormatException settingsStorageServiceException) {
            logger.warn("Cannot get value of AGRINTERVAL", settingsStorageServiceException);
        }
        Iterator<MessageLite> agrAnalogWorkIterator = new MessageLiteIterator(blockId, speedSourceId, AGR_ANALOG_WORK_MESSAGE_TYPE, startTime + 1, endTime, dataStorage);
        List<AnalogValueEntity> analogValues = new ArrayList<>();
        while (agrAnalogWorkIterator.hasNext()) {
            AGRAnalogParser.AGRAnalogWork agrAnalogWork = AGRAnalogParser.AGRAnalogWork.parseFrom(agrAnalogWorkIterator.next().toByteArray());
            analogValues.add(new AnalogValueEntity((int) (agrAnalogWork.getTimeStamp() / 1000), agrAnalogWork.getValue()));
        }
        int tMin = endTime, tMax = endTime;
        double d0 = 0, d1 = 0, s = 0;
        if (!analogValues.isEmpty()) {
            Collections.sort(analogValues);
            tMin = analogValues.get(0).getTime();
            tMax = analogValues.get(analogValues.size() - 1).getTime();
            for (int i = 1; i < analogValues.size(); i++) {
                s += analogValues.get(i).getValue();
            }
        }
        if (tMin - startTime < agrInterval) {
            d0 = getMileage(blockId, speedSourceId, startTime, tMin);
        }
        else if (!analogValues.isEmpty()) {
            d0 = analogValues.get(0).getValue();
        }
        if (endTime - tMax > 0) {
            d1 = getMileage(blockId, speedSourceId, tMax, endTime);
        }
        double mileage = d0 + s + d1;
        int time = endTime - startTime - parkingTime;
        double avgSpeed = 0;
        if (time != 0) {
            avgSpeed = mileage / time;
        }
        return new double[]{mileage, avgSpeed};
    }

    private double[] getMileageAvgSpeedByCoord(int blockId, Integer speedSourceId, int startTime, int endTime, int parkingTime) throws
            DataStorageServiceException, InvalidProtocolBufferException {
        Iterator<MessageLite> gpsCorrectIterator = new MessageLiteIterator(blockId, speedSourceId, GPS_CORRECT_MESSAGE_TYPE, startTime, endTime, dataStorage);
        List<GPSEntity> gpsValues = new ArrayList<>();
        while (gpsCorrectIterator.hasNext()) {
            LocationDataParser.GPSCorrect gpsCorrect = LocationDataParser.GPSCorrect.parseFrom(gpsCorrectIterator.next().toByteArray());
            gpsValues.add(new GPSEntity((int) (gpsCorrect.getTimeStamp() / 1000), gpsCorrect.getLatitude() / 100000.0, gpsCorrect.getLongitude() / 100000.0));
        }
        boolean addFirst = gpsValues.isEmpty() || (gpsValues.get(0).getTime() > startTime);
        boolean addLast = gpsValues.isEmpty() || (gpsValues.get(gpsValues.size() - 1).getTime() < endTime);
        if (addFirst) {
            MessageLite ml = getPreviousItem(blockId, speedSourceId, GPS_CORRECT_MESSAGE_TYPE, startTime);
            if (ml != null) {
                LocationDataParser.GPSCorrect gpsc = LocationDataParser.GPSCorrect.parseFrom(ml.toByteArray());
                gpsValues.add(0, new GPSEntity((int) (gpsc.getTimeStamp() / 1000), gpsc.getLatitude() / 100000.0, gpsc.getLongitude() / 100000.0));
            }
        }
        if (addLast) {
            MessageLite ml = getNextItem(blockId, speedSourceId, GPS_CORRECT_MESSAGE_TYPE, endTime);
            if (ml != null) {
                LocationDataParser.GPSCorrect gpsc = LocationDataParser.GPSCorrect.parseFrom(ml.toByteArray());
                gpsValues.add(new GPSEntity((int) (gpsc.getTimeStamp() / 1000), gpsc.getLatitude() / 100000.0, gpsc.getLongitude() / 100000.0));
            }
        }
        if (gpsValues.isEmpty()) {
            return new double[]{0, 0};
        }
        Collections.sort(gpsValues);
        double mileage = 0, la1, la2, lo1, lo2, a, b;
        int r2 = 2 * 6372000;
        for (int i = 1; i < gpsValues.size(); i++) {
            GPSEntity e1 = gpsValues.get(i - 1);
            GPSEntity e2 = gpsValues.get(i);
            la1 = Math.toRadians(e1.getLat());
            lo1 = Math.toRadians(e1.getLon());
            la2 = Math.toRadians(e2.getLat());
            lo2 = Math.toRadians(e2.getLon());
            a = Math.sin((la1 - la2) / 2);
            b = Math.sin((lo1 - lo2) / 2);
            mileage += r2 * Math.asin(Math.sqrt(a * a + Math.cos(la1) * Math.cos(la2) * b * b));
        }
        int time = endTime - startTime - parkingTime;
        double avgSpeed = 0;
        if (time != 0) {
            avgSpeed = mileage / time;
        }
        return new double[]{mileage, avgSpeed};
    }

    private double getMileage(int blockId, Integer sourceId, int startTime, int endTime) throws
            InvalidProtocolBufferException, DataStorageServiceException {
        double result = 0;
        Iterator<MessageLite> avcIterator = new MessageLiteIterator(blockId, sourceId, ANALOG_VALUE_CORRECTED_MESSAGE_TYPE, startTime, endTime, dataStorage);
        List<AnalogValueEntity> analogValues = new ArrayList<>();
        while (avcIterator.hasNext()) {
            AnalogValueParser.AnalogValueCorrected avc = AnalogValueParser.AnalogValueCorrected.parseFrom(avcIterator.next().toByteArray());
            analogValues.add(new AnalogValueEntity((int) (avc.getTimeStamp() / 1000), avc.getValue()));
        }
        boolean addFirst = analogValues.isEmpty() || (analogValues.get(0).getTime() > startTime);
        boolean addLast = analogValues.isEmpty() || (analogValues.get(analogValues.size() - 1).getTime() < endTime);
        if (addFirst) {
            MessageLite ml = getPreviousItem(blockId, sourceId, ANALOG_VALUE_CORRECTED_MESSAGE_TYPE, startTime);
            if (ml != null) {
                AnalogValueParser.AnalogValueCorrected avc = AnalogValueParser.AnalogValueCorrected.parseFrom(ml.toByteArray());
                analogValues.add(0, new AnalogValueEntity((int) (avc.getTimeStamp() / 1000), avc.getValue()));
            }
        }
        if (addLast) {
            MessageLite ml = getNextItem(blockId, sourceId, ANALOG_VALUE_CORRECTED_MESSAGE_TYPE, endTime);
            if (ml != null) {
                AnalogValueParser.AnalogValueCorrected avc = AnalogValueParser.AnalogValueCorrected.parseFrom(ml.toByteArray());
                analogValues.add(new AnalogValueEntity((int) (avc.getTimeStamp() / 1000), avc.getValue()));
            }
        }
        if (analogValues.isEmpty()) {
            return result;
        }
        int idleMax = 480;
        try {
            String[] parameters = settingsStorage.get(blockId, "IDLEMAX-" + sourceId);
            if (parameters != null && parameters.length > 0) {
                idleMax = Integer.parseInt(parameters[0]);
            }
        }
        catch (SettingsStorageServiceException | NumberFormatException settingsStorageServiceException) {
        }
        Collections.sort(analogValues);
        for (int i = 1; i < analogValues.size(); i++) {
            AnalogValueEntity curr = analogValues.get(i);
            AnalogValueEntity prev = analogValues.get(i - 1);
            int t = 0;
            if (curr.getTime() - prev.getTime() < idleMax) {
                t = Math.min(curr.getTime(), endTime) - Math.max(prev.getTime(), startTime);
            }
            result += curr.getValue() * t;
        }
        return result;
    }

    private double getMaxSpeed(int blockId, Integer speedSourceId, int startTime, int endTime) throws
            DataStorageServiceException, InvalidProtocolBufferException {
        int agrInterval = 600;
        try {
            String[] settings = settingsStorage.get(blockId, "AGRINTERVAL");
            if (settings != null && settings.length > 0) {
                agrInterval = Integer.parseInt(settings[0]);
            }
        }
        catch (SettingsStorageServiceException | NumberFormatException settingsStorageServiceException) {
            logger.warn("Cannot get value of AGRINTERVAL", settingsStorageServiceException);
        }
        Iterator<MessageLite> agrAnalogMaxIterator = new MessageLiteIterator(blockId, speedSourceId, AGR_ANALOG_MAX_MESSAGE_TYPE, startTime + 1, endTime, dataStorage);
        List<AnalogValueEntity> analogValues = new ArrayList<>();
        while (agrAnalogMaxIterator.hasNext()) {
            AGRAnalogParser.AGRAnalogMax agrAnalogMax = AGRAnalogParser.AGRAnalogMax.parseFrom(agrAnalogMaxIterator.next().toByteArray());
            analogValues.add(new AnalogValueEntity((int) (agrAnalogMax.getTimeStamp() / 1000), agrAnalogMax.getValue()));
        }
        int tMin = endTime, tMax = endTime;
        double d0 = 0, d1 = 0, s = 0;
        if (!analogValues.isEmpty()) {
            Collections.sort(analogValues);
            tMin = analogValues.get(0).getTime();
            tMax = analogValues.get(analogValues.size() - 1).getTime();
            for (int i = 1; i < analogValues.size(); i++) {
                s = Math.max(s, analogValues.get(i).getValue());
            }
        }
        if (tMin - startTime < agrInterval) {
            d0 = getMaxSpeed0(blockId, speedSourceId, startTime, tMin);
        }
        else if (!analogValues.isEmpty()) {
            d0 = analogValues.get(0).getValue();
        }
        if (endTime - tMax > 0) {
            d1 = getMaxSpeed0(blockId, speedSourceId, tMax, endTime);
        }
        return Math.max(s, Math.max(d0, d1));
    }

    private double getMaxSpeed0(int blockId, Integer sourceId, int startTime, int endTime) throws
            DataStorageServiceException, InvalidProtocolBufferException {
        double result = 0;
        Iterator<MessageLite> avcIterator = new MessageLiteIterator(blockId, sourceId, ANALOG_VALUE_CORRECTED_MESSAGE_TYPE, startTime, endTime, dataStorage);
        List<AnalogValueEntity> analogValues = new ArrayList<>();
        while (avcIterator.hasNext()) {
            AnalogValueParser.AnalogValueCorrected avc = AnalogValueParser.AnalogValueCorrected.parseFrom(avcIterator.next().toByteArray());
            analogValues.add(new AnalogValueEntity((int) (avc.getTimeStamp() / 1000), avc.getValue()));
        }
        boolean addFirst = analogValues.isEmpty() || (analogValues.get(0).getTime() > startTime);
        boolean addLast = analogValues.isEmpty() || (analogValues.get(analogValues.size() - 1).getTime() < endTime);
        if (addFirst) {
            MessageLite ml = getPreviousItem(blockId, sourceId, ANALOG_VALUE_CORRECTED_MESSAGE_TYPE, startTime);
            if (ml != null) {
                AnalogValueParser.AnalogValueCorrected avc = AnalogValueParser.AnalogValueCorrected.parseFrom(ml.toByteArray());
                analogValues.add(0, new AnalogValueEntity((int) (avc.getTimeStamp() / 1000), avc.getValue()));
            }
        }
        if (addLast) {
            MessageLite ml = getNextItem(blockId, sourceId, ANALOG_VALUE_CORRECTED_MESSAGE_TYPE, endTime);
            if (ml != null) {
                AnalogValueParser.AnalogValueCorrected avc = AnalogValueParser.AnalogValueCorrected.parseFrom(ml.toByteArray());
                analogValues.add(new AnalogValueEntity((int) (avc.getTimeStamp() / 1000), avc.getValue()));
            }
        }
        if (analogValues.isEmpty()) {
            return result;
        }
        int idleMax = 480;
        try {
            String[] parameters = settingsStorage.get(blockId, "IDLEMAX-" + sourceId);
            if (parameters != null && parameters.length > 0) {
                idleMax = Integer.parseInt(parameters[0]);
            }
        }
        catch (SettingsStorageServiceException | NumberFormatException settingsStorageServiceException) {
        }
        Collections.sort(analogValues);
        for (int i = 1; i < analogValues.size(); i++) {
            AnalogValueEntity curr = analogValues.get(i);
            AnalogValueEntity prev = analogValues.get(i - 1);
            int t = (curr.getTime() > endTime && curr.getTime() - prev.getTime() >= idleMax) ? 0 : 1;
            result = Math.max(result, curr.getValue() * t);
        }
        return result;
    }

    private GetMovSumDataResponse.Builder makeResponseBuilder(int blockNum, String instanceId) {
        return GetMovSumDataResponse.newBuilder().
                setMessageType(RESPONSE_MESSAGE_TYPE).
                setBlockNumber(blockNum).
                setInstanceId(instanceId);
    }

    private class TimeEntity implements Comparable<TimeEntity> {

        private int type;
        private int time;

        TimeEntity(int type, int time) {
            this.type = type;
            this.time = time;
        }

        int getType() {
            return type;
        }

        int getTime() {
            return time;
        }

        @Override
        public int compareTo(TimeEntity o) {
            int diff = this.time - o.getTime();
            if (diff != 0) {
                return diff;
            }
            return this.type - o.getType();
        }
    }

    private class AnalogValueEntity implements Comparable<AnalogValueEntity> {

        private int time;
        private double value;

        AnalogValueEntity(int time, double value) {
            this.time = time;
            this.value = value;
        }

        int getTime() {
            return time;
        }

        double getValue() {
            return value;
        }

        @Override
        public int compareTo(AnalogValueEntity o) {
            return this.time - o.getTime();
        }
    }

    private class GPSEntity implements Comparable<GPSEntity> {

        private int time;
        private double lat, lon;

        GPSEntity(int time, double lat, double lon) {
            this.time = time;
            this.lat = lat;
            this.lon = lon;
        }

        int getTime() {
            return time;
        }

        double getLat() {
            return lat;
        }

        double getLon() {
            return lon;
        }

        @Override
        public int compareTo(GPSEntity o) {
            return this.time - o.getTime();
        }
    }
}
