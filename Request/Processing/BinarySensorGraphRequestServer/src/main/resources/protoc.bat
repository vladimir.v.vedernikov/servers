@echo off

set "DEST_PATH=../java"

rem ##############################################
rem # Create common parser (for parsing headers) #
rem ##############################################
set "FILE_NAME=MessageHeader.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

rem ########################################
rem # Create parsers for required messages #
rem ########################################
rem set "FILE_NAME=Interaction.proto"
rem set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/service"
rem copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
rem call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
rem del ".\%FILE_NAME%"

set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/binarySensor"
for %%A in (BinaryStates.proto BinaryValue.proto) do (
 	copy "%SRC_PATH%\%%A" ".\%%A"
	call protoc.exe ".\%%A"  --java_out "%DEST_PATH%"
	del ".\%%A"
)

set "FILE_NAME=BinaryDataResponse.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/responses"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

set "FILE_NAME=SendRequestMessage.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

echo "done"
