#!/bin/sh

DEST_PATH=../java

##############################################
# Create common parser (for parsing headers) #
##############################################
FILE_NAME=MessageHeader.proto
SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

########################################
# Create parsers for required messages #
########################################
#SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/service
#FILE_NAME=Interaction.proto
#ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
#protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
#rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/binarySensor
for FILE_NAME in BinaryStates.proto BinaryValue.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/responses
FILE_NAME=BinaryDataResponse.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common
FILE_NAME=SendRequestMessage.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


echo "done"
