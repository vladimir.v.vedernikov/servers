/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.binarySensorGraphRequestServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.binarySensor.BinaryStateParser.BinaryStateOFF;
import ru.omnicomm.pegasus.messaging.data.binarySensor.BinaryStateParser.BinaryStateON;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum EventType {

    BINARY_STATE_ON(BinaryStateON.getDefaultInstance().getMessageType()),
    BINARY_STATE_OFF(BinaryStateOFF.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, EventType> map = new HashMap<Integer, EventType>();

    static {
        for (EventType type : EnumSet.allOf(EventType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private EventType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static EventType lookup(int code) {
        return map.get(code);
    }

}
