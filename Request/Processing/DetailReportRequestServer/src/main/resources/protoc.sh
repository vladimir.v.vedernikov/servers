#!/bin/sh

DEST_PATH=../java

FILE_NAME=SendRequestMessage.proto
SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/settings/servers/movement
FILE_NAME=MovementStateDefinition.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/location
FILE_NAME=Coordinate.proto 
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/movement
FILE_NAME=ValueMultiplySpeed.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/responses/reportcache
FILE_NAME=DetailReportDayCache.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/responses
FILE_NAME=VehicleParametersOverIntervalsResponse.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}


echo "done"
