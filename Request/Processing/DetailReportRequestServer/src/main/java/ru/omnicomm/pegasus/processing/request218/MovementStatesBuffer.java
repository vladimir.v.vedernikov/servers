/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processing.request218;

import com.google.protobuf.InvalidProtocolBufferException;
import java.util.*;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer.GpsData;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer.VelocityData;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings;
import ru.omnicomm.pegasus.processing.request218.cache.IntervalData;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;



/**
 * Буфер алгоритма определения состояний по движению
 * @author alexander
 */
public class MovementStatesBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final int sourceId;
    private final int distanceMIN;
    private final int velocityMIN;
    private final int idleMAX;
    private final int decisionInterval;

    private final List<VelocityData> periodVelocityDataBuffer;
    private final TreeSet<CoordinatesData> periodCoordinatesDataBuffer;
    
    private final List<VelocityData> velocityBuffer;
    private final List<GpsData> gpsBuffer;

    private MovementStateDefinitionBuffer.State currentState;

    private final Comparator<VelocityData> velocityDataByMessageTimeComparator;
    private final Comparator<GpsData> gpsDataByMessageTimeComparator;
    
    private int startPeriodTime;// = Integer.MIN_VALUE; //Время начала периода парковки/движения
    private final int initMessageTime; 
    
    private final List<IntervalData> intervals = new ArrayList<IntervalData>();
    

    /**
     * Конструктор
     * @param settings настройки алгоритма определения состояний по движению для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public MovementStatesBuffer(MovementStateDefinitionSettings settings, int from) throws IllegalArgumentException {
        this.startPeriodTime=from-settings.getDecisionInterval()*2;
        this.initMessageTime=from;
        this.sourceId = settings.getSourceId();
        this.distanceMIN = settings.getDistanceMIN();
        this.velocityMIN = settings.getVelocityMIN();
        this.idleMAX = settings.getIdleMAX();
        this.decisionInterval = settings.getDecisionInterval();
        this.periodVelocityDataBuffer = new ArrayList<VelocityData>();
        this.periodCoordinatesDataBuffer = new TreeSet<CoordinatesData>();
        this.velocityBuffer = new ArrayList<VelocityData>();
        this.gpsBuffer = new ArrayList<GpsData>();

        this.currentState = MovementStateDefinitionBuffer.State.NOT_DEFINED;

        this.velocityDataByMessageTimeComparator = new Comparator<VelocityData>() {

            @Override
            public int compare(VelocityData obj1, VelocityData obj2) {
                return ((Integer)obj1.getDataId()).compareTo((Integer)obj2.getDataId());
            }
        };

        this.gpsDataByMessageTimeComparator = new Comparator<GpsData>() {

            @Override
            public int compare(GpsData obj1, GpsData obj2) {
                return ((Integer)obj1.getDataId()).compareTo((Integer)obj2.getDataId());
            }
        };

     
    }


    public void processData(int messageTypeId, byte[] messageBody) throws InvalidProtocolBufferException {

        MessageType messageType = MessageType.lookup(messageTypeId);

        if(messageType == null){
            throw new IllegalArgumentException("Unknown message type id: "+messageTypeId);
        }

        //Получаем агрегированные данные по скорости и координатам
        switch(messageType) {

            case VALUE_MULTIPLY_SPEED:
                this.processVelocityData(ValueMultiplySpeed.parseFrom(messageBody));
                break;

            case COORDINATE:
                this.processGpsData(Coordinate.parseFrom(messageBody));
                break;

        }

    }

    //4.1	Обработка сообщения "ValueMultiplySpeed"
    private void processVelocityData(ValueMultiplySpeed valueMultiplySpeed) {
        
        LOGGER.debug("PROCESSING SPEED: "+valueMultiplySpeed.toString().replaceAll("\n", " "));
        
//        if(this.startPeriodTime==Integer.MIN_VALUE){
//            this.startPeriodTime=valueMultiplySpeed.getMessageTime();
//        }
        if(valueMultiplySpeed.getMessageTime()>=this.initMessageTime){
            this.periodVelocityDataBuffer.add(VelocityData.newBuilder().setDataId(valueMultiplySpeed.getMessageTime()).setValue((int)(valueMultiplySpeed.getDoubleValue(0))).build());
        }
        
        
        //3)	Сервер помещает обрабатываемое сообщение в буфер
        this.velocityBuffer.add(VelocityData.newBuilder().setDataId(valueMultiplySpeed.getMessageTime()).setValue((int)(valueMultiplySpeed.getDoubleValue(0))).build());

        Collections.sort(this.gpsBuffer, this.gpsDataByMessageTimeComparator);
        Collections.sort(this.velocityBuffer, this.velocityDataByMessageTimeComparator);

        //4)	Если разность между максимальным и минимальным timestamp в буфере меньше DecisionInterval, то сервер прерывает данную ветвь.
        while(!this.velocityBuffer.isEmpty() && this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() - this.velocityBuffer.get(0).getDataId() >= this.decisionInterval){
            //5)	Сервер выполняет алгоритм 5.1 Подсчёт средней скорости и запоминает его результат VAvr.
            int v_avr = this.getAverageSpeed();
           // LOGGER.debug("v_avr="+v_avr);
            //6)	Сервер выполняет алгоритм 5.2 Подсчёт пробега и запоминает его результат M.

            int m = this.getMileage();
      //      LOGGER.debug("m="+m);

            //7)	Устанавливаем Tm = минимальный timestamp в буфере.
            int t_min;
            if(this.gpsBuffer.isEmpty()){
                t_min = this.velocityBuffer.get(0).getDataId();
            } else {
                t_min = this.velocityBuffer.get(0).getDataId() > this.gpsBuffer.get(0).getDataId() ? this.velocityBuffer.get(0).getDataId() : this.gpsBuffer.get(0).getDataId();
            }
            
       //     LOGGER.debug("m="+m+" distanceMIN="+distanceMIN+" v_avr="+v_avr+" velocityMIN="+velocityMIN);
            
            //8)	Если М < DistanceMin И VAvr < VelocityMIN, И текущее состояние для SourceID НЕ равно Стоянка, то:
            if(m < this.distanceMIN && v_avr < this.velocityMIN && this.currentState != MovementStateDefinitionBuffer.State.PARKING){
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingSTART со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = минимальный timestamp среди всех сообщений в буфере, для которых выполняется условие Vm < VelocityMIN, где Vm – скорость из тела сообщения. (Устанавливаем Tm = данный timestamp)
                //iii.	Тело сообщения пустое
                int minTimestamp = Integer.MAX_VALUE;
                for(VelocityData velocityData : this.velocityBuffer){
                    if(velocityData.getValue() < this.velocityMIN && velocityData.getDataId() < minTimestamp){
                        minTimestamp = velocityData.getDataId();
                    }
                }
                if(minTimestamp<Integer.MAX_VALUE){
                    t_min = minTimestamp;

                        int t = minTimestamp - this.startPeriodTime;
                        if(this.startPeriodTime<this.initMessageTime){
                            t=minTimestamp - this.initMessageTime;
                        }
                        long mileage = getMileage(this.startPeriodTime,minTimestamp);
                       
                        LOGGER.debug(String.format("UNKNOWN PERIOD: from %8d till %8d, total=%4d",this.startPeriodTime,minTimestamp,t));
                        
                        
                        if(t>0){
                            if(mileage>0){
                                LOGGER.debug(String.format("MOVEMENT: from %8d till %8d, total=%4d",this.startPeriodTime,minTimestamp,t));                       
                                this.intervals.addAll(this.getIntervals(this.startPeriodTime, minTimestamp, IntervalType.MOVEMENT));
                            } else {
                                LOGGER.debug(String.format("PARKING: from %8d till %8d, total=%4d",this.startPeriodTime,minTimestamp,t));
                                this.intervals.addAll(this.getIntervals(this.startPeriodTime, minTimestamp, IntervalType.PARKING));
                            }
                            
                        }

                        this.startPeriodTime = minTimestamp;
             //       }
                    //фиксируем период движения      
                    
               //   LOGGER.info("PARKING START: "+minTimestamp);
                //   this.sendToServices(router, jmsBridgeService, dataStorageService, this.getParkingSTARTMessage(sourceId, minTimestamp));
                    //b.	Текущее состояние для SourceID устанавливается равным Стоянка
                    this.currentState = MovementStateDefinitionBuffer.State.PARKING;
                    
                    while(this.periodVelocityDataBuffer.size()>5){
                        this.periodVelocityDataBuffer.remove(0);
                    }
                    
                }
                
            }

            //9)	Если НЕ (М < DistanceMin И VAvr < VelocityMIN), И текущее состояние для SourceID НЕ равно Движение, то:
            if(!(m < this.distanceMIN && v_avr < this.velocityMIN) && (this.currentState != MovementStateDefinitionBuffer.State.MOVEMENT) ){
                
                //a.	Сервер формирует, сохраняет в хранилище, и оправляет на сервис Router и сервис JMS сообщение типа ParkingEND со следующими параметрами:
                //i.	Источник сообщения = обрабатываемый SourceID
                //ii.	Временной штамп = максимальный timestamp сообщения в буфере
                //iii.	Тело сообщения пустое
                int maxTimestamp;
                if(this.gpsBuffer.isEmpty()){
                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId();
                } else {
                    maxTimestamp = this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() > this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId() ? this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId() : this.gpsBuffer.get(this.gpsBuffer.size()-1).getDataId();
                }
                //фиксируем период парковки
            //    if(this.startPeriodTime>maxTimestamp){
                    int t = maxTimestamp - this.startPeriodTime;// - this.currentNodataTimeValue;
                    if(this.startPeriodTime<this.initMessageTime){
                        t=maxTimestamp - this.initMessageTime;
                    }
                    if(t>0){
                        //this.parkingTimeValue += t;
                    }
                    
                    //TODO: add to intervals

                    LOGGER.debug(String.format("PARKING : from %8d till %8d, total=%4d",this.startPeriodTime,maxTimestamp,t));
                    this.intervals.addAll(this.getIntervals(this.startPeriodTime, maxTimestamp, IntervalType.PARKING));
                    this.startPeriodTime = maxTimestamp;

                //b.	Текущее состояние для SourceID устанавливается равным Движение
                this.currentState = MovementStateDefinitionBuffer.State.MOVEMENT;
                
                while(this.periodVelocityDataBuffer.size()>10){
                    this.periodVelocityDataBuffer.remove(0);
                }
                while(this.periodCoordinatesDataBuffer.size()>10){
                    this.periodCoordinatesDataBuffer.pollFirst();
                }
            }

            //10)	Сервер удаляет из буфера все сообщения с timestamp, не большим, чем Tm и переходит на шаг 4)
            while(this.gpsBuffer.size()>0){
                if(this.gpsBuffer.get(0).getDataId()<=t_min){
                    this.gpsBuffer.remove(0);
                } else {
                    break;
                }
            }

            while(this.velocityBuffer.size()>0){
                if(this.velocityBuffer.get(0).getDataId()<=t_min){
                    this.velocityBuffer.remove(0);
                } else {
                    break;
                }
            }          
        }
    }
    
    public void commit(int till){

        int t = till - this.startPeriodTime;// - this.currentNodataTimeValue;
        if(this.startPeriodTime<this.initMessageTime){
            t=till - this.initMessageTime;
        }

        if(t>0){

            switch(this.currentState){
                case PARKING: 
                    this.intervals.addAll(this.getIntervals(this.startPeriodTime, till, IntervalType.PARKING));
                    LOGGER.debug(String.format("PARKING : from %8d till %8d, total=%4d *",this.startPeriodTime,till,t));
                    break;
                case MOVEMENT: 
                    this.intervals.addAll(this.getIntervals(this.startPeriodTime, till, IntervalType.MOVEMENT));
                    LOGGER.debug(String.format("MOVEMENT: from %8d till %8d, total=%4d *",this.startPeriodTime,till,t));
                    break;

            }

            
        }
        
    }

    public List<IntervalData> getIntervals() {
        List<IntervalData> res = new ArrayList<IntervalData>();
        res.addAll(this.intervals);
        return res;
    }


    private void processGpsData(Coordinate coordinate) {
        
        LOGGER.debug("PROCESSING COORDINATES: "+coordinate.toString().replaceAll("\n", " "));
        
        GpsData gpsData = GpsData.newBuilder()
                    
                    .setDataId(coordinate.getMessageTime())
                    .setLatitude(coordinate.getLatitude())
                    .setLongitude(coordinate.getLongitude())

                    .build();
        
        if(coordinate.getMessageTime()>=this.initMessageTime && coordinate.getValid()){
            this.periodCoordinatesDataBuffer.add(new CoordinatesData(gpsData.getDataId(),gpsData.getLatitude(),gpsData.getLongitude()));
        }

        if(coordinate.getValid()){
            this.gpsBuffer.add(gpsData);
        }
        
    }
    
    private void applyCoordinates (IntervalData intervalData){
        
        CoordinatesData first = this.periodCoordinatesDataBuffer.floor(new CoordinatesData(intervalData.getIntervalStart(),0,0));
        CoordinatesData last = this.periodCoordinatesDataBuffer.ceiling(new CoordinatesData(intervalData.getIntervalEnd(),0,0));
        
        if(first!=null){
            intervalData.setLatitudeFrom(first.getLatitude());
            intervalData.setLongitudeFrom(first.getLongitude());
        }
        
        if(last!=null){
            intervalData.setLatitudeTill(last.getLatitude());
            intervalData.setLongitudeTill(last.getLongitude());
        }
    }
    
    private List<IntervalData> getIntervals(int from, int till, IntervalType intervalType){
        
        int t1; //initMessageTime
        t1 = from < this.initMessageTime ?  this.initMessageTime : from;
        List<IntervalData> res = new ArrayList<IntervalData>();

        IntervalData interval = new IntervalData(t1,0,intervalType,0,0,0,0,0,0);
        for (int i = 1; i < this.periodVelocityDataBuffer.size(); i++) {
            
            if(this.periodVelocityDataBuffer.get(i-1).getDataId()>=t1 && this.periodVelocityDataBuffer.get(i).getDataId()<=till ){
                int deltaT = this.periodVelocityDataBuffer.get(i).getDataId() - this.periodVelocityDataBuffer.get(i-1).getDataId();
                int v_i;
                if(deltaT < this.idleMAX){
                    v_i = this.periodVelocityDataBuffer.get(i).getValue();
                    if(interval.getType()  == IntervalType.NO_DATA){
                        interval.setIntervalEnd(this.periodVelocityDataBuffer.get(i).getDataId());
                        if(interval.getLength()>0){
                            res.add(interval);
                        }
                        interval = new IntervalData(this.periodVelocityDataBuffer.get(i).getDataId(),0,intervalType,0,0,0,0,0,0);
                    }
                } else {
                    v_i = 0;
                    interval.setIntervalEnd(this.periodVelocityDataBuffer.get(i-1).getDataId());
                    if(interval.getLength()>0){
                        res.add(interval);
                    }
                    interval = new IntervalData(this.periodVelocityDataBuffer.get(i-1).getDataId(),0,IntervalType.NO_DATA,0,0,0,0,0,0);
                }
                
                long S = interval.getMileage();
                S += (long)(v_i * deltaT);
                if(intervalType  == IntervalType.MOVEMENT){
                    if(v_i>interval.getMaxVelocity()){
                        interval.setMaxVelocity(v_i);
                    }
                    interval.setMileage(S);
                }
                
            }

            
        }
        interval.setIntervalEnd(till);
        if(interval.getLength()>0){
            res.add(interval);
        }
        
        for(IntervalData intervalData : res) {
            this.applyCoordinates(intervalData);
        }
        
        
        return res;

        
     
    }
    
    private long getMileage(int from, int till){
        long S = 0;
        for (int i = 1; i < this.periodVelocityDataBuffer.size(); i++) {
            
            if(this.periodVelocityDataBuffer.get(i).getDataId()>=from && this.periodVelocityDataBuffer.get(i).getDataId()<=till ){
                int deltaT = this.periodVelocityDataBuffer.get(i).getDataId() - this.periodVelocityDataBuffer.get(i-1).getDataId();
                int v_i;
                if(deltaT < this.idleMAX){
                    v_i = this.periodVelocityDataBuffer.get(i).getValue();
                } else {
                    v_i = 0;
                }
                S += (long)(v_i * deltaT);
                
            }

            
        }

        
        return S;
        
    }

    private int getAverageSpeed(){

        int S = 0;
        for (int i = 1; i < this.velocityBuffer.size(); i++) {

            int deltaT = this.velocityBuffer.get(i).getDataId() - this.velocityBuffer.get(i-1).getDataId();
            int v_i;
            if(deltaT < this.idleMAX){
                v_i = this.velocityBuffer.get(i).getValue();
            } else {
  //              this.currentNodataTimeValue+=deltaT;
                v_i = 0;
            }
            S += (v_i * deltaT);     
        }
       // if(this.currentState==MovementStateDefinitionBuffer.State.MOVEMENT){
         //   this.mileageValue+=S;
            
            
        //}
        
        return S/(this.velocityBuffer.get(this.velocityBuffer.size()-1).getDataId()-this.velocityBuffer.get(0).getDataId());

    }

    private int getMileage(){

        
        int m = 0;

        for (int i = 1; i < gpsBuffer.size(); i++) {
            m+=getDistance(gpsBuffer.get(i-1),gpsBuffer.get(i));
        }
        
        return m;       
    }

    private double sin(double deg){
        return Math.sin(Math.toRadians(deg));
    }

    private double cos(double deg){
        return Math.cos(Math.toRadians(deg));
    }


    private int getDistance(GpsData point1, GpsData point2){

        double earth_radius = 6372.0;

        double dist =
                2 * earth_radius * 1000 * Math.asin(
                    Math.sqrt(
                        Math.pow(sin((point1.getLatitude() - point2.getLatitude()) / 2.0), 2) +
                        cos(point1.getLatitude()) * cos(point2.getLatitude()) *
                        Math.pow(sin((point1.getLongitude() - point2.getLongitude()) / 2.0), 2)
                    )
                );

        return (int)dist;


    }
    
    private class CoordinatesData implements Comparable {
        
        private final int messageTime;
        private final double latitude;
        private final double longitude;

        public CoordinatesData(int messageTime, double latitude, double longitude) {
            this.messageTime = messageTime;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        public int compareTo(Object obj) {
            if (obj instanceof CoordinatesData) {
                return Integer.valueOf(this.messageTime).compareTo(((CoordinatesData) obj).messageTime);
            } else {
                throw new IllegalArgumentException("Argument should be instance of "+this.getClass().getName());
            }
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public int getMessageTime() {
            return messageTime;
        }
        
        

    }

}
