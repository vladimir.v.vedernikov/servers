/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.request218;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alexander
 */
public enum IntervalType {

    MOVEMENT(1),
    PARKING(2),
    NO_DATA(3);

    private final int code;

    private static final Map<Integer, IntervalType> map = new HashMap<Integer, IntervalType>();

    static {
        for (IntervalType type : EnumSet.allOf(IntervalType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private IntervalType(int cmd) {
        this.code = cmd;
    }

    public int getCode() {
        return this.code;
    }

    public static IntervalType lookup(int code) {
        return map.get(code);
    }
}