/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.request218;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.Iterator;
import java.util.List;
import ru.omnicomm.pegasus.base.Constants;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser.SendRequest;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.movement.ValueMultiplySpeedParser.ValueMultiplySpeed;
import ru.omnicomm.pegasus.messaging.data.responses.VehicleParametersOverIntervalsResponseParser.VehicleParametersOverIntervalsResponse;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser;
import ru.omnicomm.pegasus.processing.request218.cache.IntervalData;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DetailReportRequestServerImplementation")
public class RequestServer218 implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();
    private static int SPEED_MESSAGE_TYPE = ValueMultiplySpeed.getDefaultInstance().getMessageType();
    private static int COORDINATES_MESSAGE_TYPE = Coordinate.getDefaultInstance().getMessageType();

    public static final int RESPONSE_MESSAGE_TYPE = 218;
    public static final int REQUEST_MESSAGE_TYPE = 418;

    public static final String CONTROLLED_UNIT_TYPE_NAME = "Speed";

    // private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private DataStorageService dataStorage;
    private SettingsStorageService settingsStorage;
    private JmsBridgeService jmsBridge;

    private ControlEntityHelper baseHelper;

    private Server self;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     */
    @ServerConstructor
    public RequestServer218(DataStorageService dataStorage, SettingsStorageService settingsStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.dataStorage = dataStorage;
        this.settingsStorage = settingsStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }

        try {
            long start = System.currentTimeMillis();
            processMessage(request);
            long total = System.currentTimeMillis() - start;

            LOGGER.info("TOTAL REQUEST PROCESSING TIME: " + total + " ms");
        } catch (Exception ex) {
            LOGGER.info("Can't process data", ex);
        }
    }


    private void processMessage(SendRequest request) throws IllegalArgumentException, DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException, SettingsStorageServiceException {
        List<SendRequestParser.SendRequest.Parameter> filter = request.getFilterList();
        long startPeriod = -1;
        long finishPeriod = -1;
        Integer sourceId;
        int vehicleId = -1;
        int limitTime = 0;
        IntervalType intervalTypeId = null;
        String locale = "en";
        String requestId = request.getInstanceId();

        for (SendRequest.Parameter parameter : filter) {

            String key = parameter.getKey();

            if (key.equalsIgnoreCase("startPeriod")) {
                startPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("finishPeriod")) {
                finishPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("limitTime")) {
                limitTime = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("vehicleId")) {
                vehicleId = Integer.parseInt(parameter.getStringValue());
            } else if (key.equalsIgnoreCase("intervalTypeId")) {
                
                intervalTypeId = IntervalType.lookup(parameter.getIntValue());
            } else if (key.equalsIgnoreCase("locale")) {
                locale = parameter.getStringValue();
            }

        }

        if (startPeriod < 0 || finishPeriod < 0 || vehicleId < 0 || (intervalTypeId == null)) {
            throw new IllegalArgumentException("There are not enough or wrong parameters in the request: " + request);
        }

        String intervalType = getIntervalType(intervalTypeId, locale);

        int blockNmb = 0;
        int rowsInBlock = 1;
//        int averageVelocityGlobalCounter = 0;
//        double maxVelocity = 0;
        VehicleParametersOverIntervalsResponse.Builder responseBuilder = makeResponseBuilder(blockNmb++, requestId);
        
        sourceId = baseHelper.selectFirstSoureceId(CONTROLLED_UNIT_TYPE_NAME, vehicleId);
        if (sourceId == null) {
            responseBuilder.setBlockState(VehicleParametersOverIntervalsResponse.State.LAST);
            jmsBridge.send(responseBuilder.build());
            return;
        }
        
        //1. Разбиваем период t1 - t2 на интервалы длиной в сутки (86400 сек.): таким образом получаем список ключей к кэшу
        //2. Бежим по полученному списку
        //2.1 Загружаем reportCache
        //2.2 Если отсутствует, что формируем и помещаем в БД
        //2.3 Добавляем данные из reportCache в отчет
        int t1 = (int)startPeriod;
        int t2 = (int)finishPeriod;

        String settingsKey = getSettingsName(sourceId);
      
        MessageLite settingsObject = this.settingsStorage.lookup(settingsKey);
        if (settingsObject == null) {
            LOGGER.info("Movement States Definition Settings wasn't found for sourceId="+sourceId);
            return;
        }
        MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings settings = null;
        try{
            settings = MovementStateDefinitionSettingsParser.MovementStateDefinitionSettings.parseFrom(settingsObject.toByteString());
        } catch (Exception ex) {
            LOGGER.info("Wrong settings object: "+settingsObject,ex);

        }
        if(settings==null){
            LOGGER.info("Movement States Definition Settings wasn't found for sourceId="+sourceId);
            return;
        }


        MovementStatesBuffer buf = new MovementStatesBuffer(settings,t1);

        LOGGER.info("Decision interval: "+settings.getDecisionInterval());

        Iterator<MessageLite> coordinatesIterator = new MessageLiteIterator(sourceId, MessageType.COORDINATE.getCode(), t1-settings.getDecisionInterval()*2, t2, this.dataStorage);
        Iterator<MessageLite> speedIterator = new MessageLiteIterator(sourceId, MessageType.VALUE_MULTIPLY_SPEED.getCode(), t1-settings.getDecisionInterval()*2, t2, this.dataStorage);

        try{
            ValueMultiplySpeed speed = getNextSpeedData(speedIterator);
            while(coordinatesIterator.hasNext() && speed!=null){
                Coordinate coordinate = Coordinate.parseFrom(coordinatesIterator.next().toByteString()); 
                buf.processData(MessageType.COORDINATE.getCode(), coordinate.toByteArray());
                while(speed!=null && speed.getMessageTime()<=coordinate.getMessageTime()){
                    buf.processData(MessageType.VALUE_MULTIPLY_SPEED.getCode(), speed.toByteArray());
                    speed = getNextSpeedData(speedIterator);
                }
            }
        } catch (Exception ex) {
            responseBuilder.setBlockState(VehicleParametersOverIntervalsResponse.State.DEFECTIVE);
            jmsBridge.send(responseBuilder.build());
            LOGGER.info("Detail report error",ex);
            return; // for (int i = 0; i < vehiclesIds.size(); i++) 
        }

        buf.commit(t2);

        Iterator<IntervalData> res = buf.getIntervals().iterator();

 
        while(res.hasNext()){
            IntervalData intervalData = res.next();

               LOGGER.info(intervalData.toString());

            // LOGGER.info("FROM "+intervalData.getIntervalStart()+" - TILL "+intervalData.getIntervalEnd()+ " t1="+t1+" t2="+t2+" "+(intervalData.getIntervalStart() >= t1) + " "+(intervalData.getIntervalEnd() <= t2));

            if(intervalTypeId==intervalData.getType() && intervalData.getLength()>=limitTime  && intervalData.getIntervalStart() >= t1  && intervalData.getIntervalEnd() <= t2  ){
                VehicleParametersOverIntervalsResponse.Row.Builder parameterRowBuilder = VehicleParametersOverIntervalsResponse.Row.newBuilder();
                parameterRowBuilder.setIntervalType(intervalType);// .addParameters(getStringValueParameter("intervalType", intervalType));
                parameterRowBuilder.setIntervalStart(intervalData.getIntervalStart()); //.addParameters(getLongValueParameter("intervalStart", startInterval));
                parameterRowBuilder.setIntervalFinish(intervalData.getIntervalStart()+intervalData.getLength());  //.addParameters(getLongValueParameter("intervalFinish", state.messageTime));
                parameterRowBuilder.setIntervalDuration(intervalData.getLength()); //.addParameters(getStringValueParameter("intervalDuration", getTimeDiff(intervalDuration)));

                parameterRowBuilder.setMileage(intervalData.getMileage()); //.addParameters(getDoubleValueParameter("mileage", mileageLocal));
                double avrVelocity;
                if(intervalData.getLength()>0){
                    avrVelocity = (double)intervalData.getMileage()/intervalData.getLength();
                } else {
                    avrVelocity = 0;
                }
                parameterRowBuilder.setAverageVelocity(avrVelocity);  //.addParameters(getDoubleValueParameter("averageVelocity", avrVelocityLocal));
                parameterRowBuilder.setMaxVelocity(intervalData.getMaxVelocity()); //.addParameters(getDoubleValueParameter("maxVelocity", maxVelocityLocal));

                parameterRowBuilder.setStartPointCoordinates(getCoordinatesString(intervalData.getLatitudeFrom(),intervalData.getLongitudeFrom())); //.addParameters(getStringValueParameter("startPointCoordinates", getCoordinatesString(coordinates[0])));
                parameterRowBuilder.setFinishPointCoordinates(getCoordinatesString(intervalData.getLatitudeTill(),intervalData.getLongitudeTill())); //.addParameters(getStringValueParameter("finishPointCoordinates", getCoordinatesString(coordinates[1])));

                responseBuilder.addRows(parameterRowBuilder);

                if (++rowsInBlock > Constants.IN_BLOCK_MESSAGES_LIMIT) {
                    responseBuilder.setBlockState(VehicleParametersOverIntervalsResponse.State.NORMAL);
                    //LOGGER.info("ROW "+rowsInBlock+ "FROM "+IN_BLOCK_MESSAGES_LIMIT);
                    jmsBridge.send(responseBuilder.build());

                    rowsInBlock = 1;
                    responseBuilder = makeResponseBuilder(blockNmb++, requestId);
                }

            }

        }

     

        responseBuilder.setBlockState(VehicleParametersOverIntervalsResponse.State.LAST);
        jmsBridge.send(responseBuilder.build());


    }
    
    private ValueMultiplySpeed getNextSpeedData(Iterator<MessageLite> speedIterator) throws InvalidProtocolBufferException{
        ValueMultiplySpeed speed = null;
        if(speedIterator.hasNext()){
            speed = ValueMultiplySpeed.parseFrom(speedIterator.next().toByteString());
        }
  
        return speed;
    }

    
    private String getCoordinatesString(double latitude, double longitude) {

        StringBuilder sb = new StringBuilder()

                .append("(")
                .append(latitude)
                .append(", ")
                .append(longitude)
                .append(")");

        return sb.toString();


    }

    private String getSettingsName(int sourceId) {

        return String.format("src%d:movementStates:settings", sourceId);

    }
    
    private String getCacheKey(int sourceId, int messageTime){
        return String.format("src%d:mtm%d:detailReport:cache", sourceId,messageTime);
    }

    private VehicleParametersOverIntervalsResponse.Builder makeResponseBuilder(int blockNum, String requestId) {

        VehicleParametersOverIntervalsResponse.Builder responseBuilder = VehicleParametersOverIntervalsResponse.newBuilder()

                .setMessageType(RESPONSE_MESSAGE_TYPE)
                .setInstanceId(requestId)
                .setBlockNumber(blockNum);

        return responseBuilder;
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                jmsBridge.registerServer(RequestServer218.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                super.visit(signal);    //To change body of overridden methods use File | Settings | File Templates.
            }
        });
    }

    private String getIntervalType(IntervalType intervalTypeId, String locale) {

        //TODO: Load localization from the cloud DB
        if (locale.equalsIgnoreCase("ru")) {

            switch (intervalTypeId) {
                case MOVEMENT:
                    return "Движение";
                case PARKING:
                    return "Стоянка";
                case NO_DATA:
                    return "Нет данных";

                default:
                    throw new IllegalArgumentException("Unknown or unsupported parameter type: " + intervalTypeId);
            }

        } else { //Default locale is "en"
            switch (intervalTypeId) {
                case MOVEMENT:
                    return "Movement";
                case PARKING:
                    return "Parking";
                case NO_DATA:
                    return "No data";

                default:
                    throw new IllegalArgumentException("Unknown or unsupported parameter type: " + intervalTypeId);
            }
        }
    }
}
