/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.request218.cache;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Iterator;
import java.util.TreeSet;
import ru.omnicomm.pegasus.messaging.data.responses.reportcache.DetailReportDayCacheParser.DetailReportDataItem;
import ru.omnicomm.pegasus.messaging.data.responses.reportcache.DetailReportDayCacheParser.DetailReportDayCache;
import ru.omnicomm.pegasus.processing.request218.IntervalType;

/**
 *
 * @author alexander
 */
public class ReportCache {
    
    private final static int messageType = DetailReportDayCache.getDefaultInstance().getMessageType();
    
    private final int vehicleId;
    private final int messageTime;
    
    private final TreeSet<IntervalData> intervals = new TreeSet<IntervalData>();

    public ReportCache(int vehicleId, int messageTime) {
        if(messageTime % 86400 != 0) {
            throw new IllegalArgumentException("messageTime value must be a multiple of 86400");
        }
        this.vehicleId = vehicleId;
        this.messageTime = messageTime;
    }
    
    public ReportCache(ByteString byteString) throws InvalidProtocolBufferException{
        this(DetailReportDayCache.parseFrom(byteString));
    }
    
    public ReportCache(DetailReportDayCache cache){     
        this(cache.getSourceId(),cache.getMessageTime());  
        for(DetailReportDataItem item : cache.getItemsList()){         
            this.intervals.add(new IntervalData(item));                  
        }
    }

    public int getMessageTime() {
        return messageTime;
    }

    public int getVehicleId() {
        return vehicleId;
    }
    
    public Iterator<IntervalData> getIntervals(){
        return this.intervals.iterator();
    }
    
//    public Iterator<IntervalData> getIntervals(int from, int till){
//        this.intervals.subSet(new IntervalData(from,0,null,0,0,0,0,0,0), new IntervalData(till,0,null,0,0,0,0,0,0));
//        return this.intervals.iterator();
//    }
    
    public void removeInterval(IntervalData interval){
        this.intervals.remove(interval);
    }
    
    public boolean isEmpty(){
        return this.intervals.isEmpty();
    }
    
    public IntervalData getInterval(int messageTime){
        return this.intervals.floor(new IntervalData(messageTime,0,null,0,0,0,0,0,0));
    }
    
    public IntervalData getNextInterval(int messageTime){
        return this.intervals.ceiling(new IntervalData(messageTime,0,null,0,0,0,0,0,0));
    }
    
    
    public void merge(ReportCache reportCache){
        this.intervals.addAll(reportCache.intervals);
    }
     
    public int getLastMessageTime(){
        
        if(!this.intervals.isEmpty()){
            IntervalData last = this.intervals.last();
            return last.getIntervalStart()+last.getLength();
        } else {
            return messageTime;
        }
  
    }

    
    public void addIntrerval(IntervalData item){
        
        
        //TODO: merge interval
        this.intervals.add(item);
    }
    
    public void trim(){
        while(this.intervals.last().getType() == IntervalType.NO_DATA){
            this.intervals.pollLast();
        }
    }
    
    
    public DetailReportDayCache getProtobuf(){
        DetailReportDayCache.Builder builder = DetailReportDayCache.newBuilder();
        
        builder
                .setSourceId(this.vehicleId)
                .setMessageType(messageType)
                .setMessageTime(this.messageTime);
        
        Iterator<IntervalData> iter = this.intervals.iterator();
        while(iter.hasNext()){
            builder.addItems(iter.next().getProtobuf());
        }                      
        
        return builder.build();
    }

    
}
