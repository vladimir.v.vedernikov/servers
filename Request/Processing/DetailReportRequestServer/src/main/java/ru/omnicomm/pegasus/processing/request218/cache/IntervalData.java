/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.request218.cache;

import ru.omnicomm.pegasus.messaging.data.responses.reportcache.DetailReportDayCacheParser.DetailReportDataItem;
import ru.omnicomm.pegasus.processing.request218.IntervalType;

/**
 *
 * @author alexander
 */
public class IntervalData implements Comparable {
    
    private final int intervaStart;
    private int intervalEnd;
    private final IntervalType type;
    private long mileage;
    private long maxVelocity;
    private double latitudeFrom;
    private double longitudeFrom;
    private double latitudeTill;
    private double longitudeTill;
    
    private final static double coef = 10000000.0;

    public IntervalData(int intervalTime, int intervalEnd, IntervalType type, long mileage, long maxVelocity, double latitudeFrom, double longitudeFrom, double latitudeTill, double longitudeTill) {
        this.intervaStart = intervalTime;
        this.intervalEnd = intervalEnd;
        this.type = type;
        this.mileage = mileage;
        this.maxVelocity = maxVelocity;
        this.latitudeFrom = latitudeFrom;
        this.longitudeFrom = longitudeFrom;
        this.latitudeTill = latitudeTill;
        this.longitudeTill = longitudeTill;
    }

    public IntervalData(DetailReportDataItem dataItem) {
        this(dataItem.getIntervalTime(), dataItem.getIntervalTime()+dataItem.getIntervalLength(),IntervalType.lookup(dataItem.getIntervalType()),dataItem.getMileage(),dataItem.getMaxVelocity(),(double)dataItem.getLatitudeFrom()/coef,(double)dataItem.getLongitudeFrom()/coef,(double)dataItem.getLatitudeTill()/coef,(double)dataItem.getLongitudeTill()/coef);
    }
    
    
    
    DetailReportDataItem getProtobuf(){
        
        DetailReportDataItem.Builder builder = DetailReportDataItem.newBuilder();
        
        builder.setIntervalTime(this.intervaStart);
        builder.setIntervalLength(this.intervalEnd-this.intervaStart);
        builder.setIntervalType(this.type.getCode());
        builder.setMileage(this.mileage);
        builder.setMaxVelocity(this.maxVelocity);
        builder.setLatitudeFrom( (long)(this.latitudeFrom  * coef));
        builder.setLongitudeFrom((long)(this.longitudeFrom * coef));
        builder.setLatitudeTill( (long)(this.latitudeTill  * coef));
        builder.setLongitudeTill((long)(this.longitudeTill * coef));
        
        return builder.build();
        
    }

    public double getCoordinatesTill() {
        return latitudeTill;
    }

    public int getIntervalStart() {
        return intervaStart;
    }
    
    public int getIntervalEnd() {
        return intervalEnd;
    }

    public void setIntervalEnd(int intervalEnd) {
        this.intervalEnd = intervalEnd;
    }

    public int getLength() {
        return intervalEnd-intervaStart;
    }

    public long getMaxVelocity() {
        return maxVelocity;
    }

    public long getMileage() {
        return mileage;
    }

    public IntervalType getType() {
        return type;
    }

    public double getLatitudeFrom() {
        return latitudeFrom;
    }

    public void setLatitudeFrom(double latitudeFrom) {
        this.latitudeFrom = latitudeFrom;
    }

    public double getLatitudeTill() {
        return latitudeTill;
    }

    public void setLatitudeTill(double latitudeTill) {
        this.latitudeTill = latitudeTill;
    }

    public double getLongitudeFrom() {
        return longitudeFrom;
    }

    public void setLongitudeFrom(double longitudeFrom) {
        this.longitudeFrom = longitudeFrom;
    }

    public double getLongitudeTill() {
        return longitudeTill;
    }

    public void setLongitudeTill(double longitudeTill) {
        this.longitudeTill = longitudeTill;
    } 

    public void setMaxVelocity(long maxVelocity) {
        this.maxVelocity = maxVelocity;
    }

    public void setMileage(long mileage) {
        this.mileage = mileage;
    }
    
    

    @Override
    public int compareTo(Object obj) {
        if (obj instanceof IntervalData) {
            return Integer.valueOf(this.intervaStart).compareTo(((IntervalData) obj).intervaStart);
        } else {
            throw new IllegalArgumentException("Argument should be instance of "+this.getClass().getName());
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IntervalData other = (IntervalData) obj;
        if (this.intervaStart != other.intervaStart) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.intervaStart;
        return hash;
    }

    @Override
    public String toString() {
        return "IntervalData{" + "from=" + intervaStart + ", till=" + intervalEnd + ", type=" + type + ", mileage=" + mileage + ", maxVelocity=" + maxVelocity + ", ("+latitudeFrom + "," + longitudeFrom + ")-(" + latitudeTill + "," + longitudeTill + ")}";
    }
    
}
