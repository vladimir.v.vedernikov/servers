/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.analogSensorGraphRequestServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogStatesParser.AnalogValueDOWN;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogStatesParser.AnalogValueNormal;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogStatesParser.AnalogValueUP;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum EventType {

    ANALOG_VALUE_UP(AnalogValueUP.getDefaultInstance().getMessageType()),
    ANALOG_VALUE_DOWN(AnalogValueDOWN.getDefaultInstance().getMessageType()),
    ANALOG_VALUE_NORMAL(AnalogValueNormal.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, EventType> map = new HashMap<Integer, EventType>();

    static {
        for (EventType type : EnumSet.allOf(EventType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private EventType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static EventType lookup(int code) {
        return map.get(code);
    }

}
