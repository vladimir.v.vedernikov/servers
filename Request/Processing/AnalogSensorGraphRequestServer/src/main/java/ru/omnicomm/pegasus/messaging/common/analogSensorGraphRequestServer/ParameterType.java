/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.analogSensorGraphRequestServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.analogSensor.AnalogValueCorrectedParser.AnalogValueCorrected;

/**
 *
 * @author alexander
 */
public enum ParameterType {

    ANALOG_VALUE_CORRECTED(AnalogValueCorrected.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, ParameterType> map = new HashMap<Integer, ParameterType>();

    static {
        for (ParameterType type : EnumSet.allOf(ParameterType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private ParameterType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static ParameterType lookup(int code) {
        return map.get(code);
    }

}
