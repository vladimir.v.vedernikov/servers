/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.refuelingsreport;

/**
 *
 * @author alexander
 */
public interface FuelEvent {
    
    int getVehicleId();
    
    int getStartStateDate();
    
    int getFinishStateDate();

    int getValue();
    
    String getFuelEventType();
    
    FuelEventType getFuelEventId();
    
}
