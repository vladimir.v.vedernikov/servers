/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.refuelingsreport;

/**
 *
 * @author alexander
 */
public abstract class DrainingOrRefueling implements FuelEvent{

    private final String fuelProcess;
    private final FuelEventType processType;

    public DrainingOrRefueling(FuelEventType processType, String locale) {
        this.fuelProcess = makeEventName(locale,processType);
        this.processType = processType;
    }

    @Override
    public String getFuelEventType() {
        return this.fuelProcess;
    }
    
    @Override
    public FuelEventType getFuelEventId(){
        return this.processType;
    }
    
    
    private static String makeEventName(String locale, FuelEventType processType) {

        //TODO: Load localization from the cloud DB
        if (locale.equalsIgnoreCase("ru")) {

            switch (processType) {
                case DRAINING:
                    return "Слив";
                case REFUELING:
                    return "Заправка";
                default:
                    throw new IllegalArgumentException("Unknown or unsupported event type: " + processType);
            }

        } else { //Default locale is "en"
            switch (processType) {
                case DRAINING:
                    return "Draining";
                case REFUELING:
                    return "Refueling";
                default:
                    throw new IllegalArgumentException("Unknown or unsupported event type: " + processType);
            }
        }

    }
    
    


    
}
