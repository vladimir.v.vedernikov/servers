/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.refuelingsreport;

import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Iterator;
import java.util.LinkedList;
import ru.omnicomm.pegasus.messaging.data.fuel.ApproximatedDataParser.ApproximatedData;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingSTART;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingSTART;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;

/**
 *
 * @author alexander
 */
public class FuelEventsIterator implements Iterator<FuelEvent>{
    
    private static Logger LOGGER = LoggerFactory.getLogger();
    
    private final int vehicleId;
    
    private final MessageLiteIterator drainingsStartsIterator;
    private final MessageLiteIterator drainingsEndsIterator;
    private final MessageLiteIterator refuelingsStartsIterator;
    private final MessageLiteIterator refuelingsEndsIterator;
    
    private final MessageLiteIterator approximatedDataIterator;
    
    private final LinkedList<DrainingState> nextDrainings  = new LinkedList<DrainingState>();
    private final LinkedList<RefuelingState> nextRefuelings = new LinkedList<RefuelingState>();
    
    private final String locale;
    
    private final int from;
    private final int till;
    
    private ApproximatedData firstApproximatedData = null;
    private ApproximatedData lastApproximatedData = null;



    public FuelEventsIterator(int sourceId, int vehicleId, int from, int till, DataStorageService dataStorageService, String locale) throws DataStorageServiceException, InvalidProtocolBufferException {
        this.vehicleId = vehicleId;
        this.locale = locale;
        
        this.from = from;
        this.till = till;
        
        
        this.drainingsStartsIterator = new MessageLiteIterator(sourceId,DrainingSTART.getDefaultInstance().getMessageType(),from,till,dataStorageService);
        this.drainingsEndsIterator = new MessageLiteIterator(sourceId,DrainingEND.getDefaultInstance().getMessageType(),from,till,dataStorageService);
        
        this.refuelingsStartsIterator = new MessageLiteIterator(sourceId,RefuelingSTART.getDefaultInstance().getMessageType(),from,till,dataStorageService);
        this.refuelingsEndsIterator = new MessageLiteIterator(sourceId,RefuelingEND.getDefaultInstance().getMessageType(),from,till,dataStorageService);
        
        this.approximatedDataIterator = new MessageLiteIterator(sourceId,ApproximatedData.getDefaultInstance().getMessageType(),from,till,dataStorageService);

        while(this.approximatedDataIterator.hasNext()){
            ApproximatedData data = ApproximatedData.parseFrom(this.approximatedDataIterator.next().toByteString());
            if(firstApproximatedData==null){
                firstApproximatedData = data;
            }
            lastApproximatedData = data;
        }
        
        this.makeNextDraining();
        this.makeNextRefueling();
    }

    public ApproximatedData getFirstApproximatedData() {
        return firstApproximatedData;
    }

    public ApproximatedData getLastApproximatedData() {
        return lastApproximatedData;
    }
    
    private void makeNextDraining(){
        try{
            int start=Integer.MIN_VALUE, end=Integer.MIN_VALUE, value = 0;
            
            if(this.drainingsStartsIterator.hasNext()){
                start = DrainingSTART.parseFrom(this.drainingsStartsIterator.next().toByteString()).getMessageTime();
            }
            if(this.drainingsEndsIterator.hasNext()){
                DrainingEND drainingEnd = DrainingEND.parseFrom(this.drainingsEndsIterator.next().toByteString());
                end = drainingEnd.getMessageTime();
                value = drainingEnd.getValue();
            }
            
            if(start==Integer.MIN_VALUE && end==Integer.MIN_VALUE){
            } else
            if(end==Integer.MIN_VALUE){
      //          this.nextDrainings.offer(new DrainingState(start,Integer.MIN_VALUE,value));
            }            
            else {
                while(end<start){
                    if(this.drainingsEndsIterator.hasNext()){
                        this.nextDrainings.offer(new DrainingState(Integer.MIN_VALUE,end,value));
                        end = DrainingEND.parseFrom(this.drainingsEndsIterator.next().toByteString()).getMessageTime();
                    } else {
                        return;
                    }
                }

                this.nextDrainings.offer(new DrainingState(start,end,value));
            }
            
            
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, "", ex);
        }
    }
    
    private void makeNextRefueling(){
        try{
            int start=Integer.MIN_VALUE, end=Integer.MIN_VALUE, value = 0;
            
            if(this.refuelingsStartsIterator.hasNext()){
                start = RefuelingSTART.parseFrom(this.refuelingsStartsIterator.next().toByteString()).getMessageTime();
            }
            if(this.refuelingsEndsIterator.hasNext()){
                RefuelingEND refuelingEnd = RefuelingEND.parseFrom(this.refuelingsEndsIterator.next().toByteString());
                end = refuelingEnd.getMessageTime();
                value = refuelingEnd.getValue();
            }
            
            if(start==Integer.MIN_VALUE && end==Integer.MIN_VALUE){
            } else
            if(end==Integer.MIN_VALUE){
          //      this.nextRefuelings.offer(new RefuelingState(start,Integer.MIN_VALUE,value));
            }            
            else {
                while(end<start){
                    if(this.refuelingsEndsIterator.hasNext()){                   
                        this.nextRefuelings.offer(new RefuelingState(Integer.MIN_VALUE,end,value));
                        end = RefuelingEND.parseFrom(this.refuelingsEndsIterator.next().toByteString()).getMessageTime();
                    } else {
                        return;
                    }
                }

                this.nextRefuelings.offer(new RefuelingState(start,end,value));
            }
            
            
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, "", ex);
        }
    }
    
    

    @Override
    public boolean hasNext() {
        return !this.nextDrainings.isEmpty() || !this.nextRefuelings.isEmpty();
        //return this.nextDraining!=null || this.nextRefueling!=null;
    }

    @Override
    public FuelEvent next() {
        if(!this.nextDrainings.isEmpty() && !this.nextRefuelings.isEmpty()){
            
            FuelEvent nextDraining = this.nextDrainings.peek();
            FuelEvent nextRefueling = this.nextRefuelings.peek();
            
            if(nextDraining.getStartStateDate()<nextRefueling.getStartStateDate()){              
                this.makeNextDraining();
                return makeOutput(this.nextDrainings.poll()); 
            } else {
                this.makeNextRefueling();
                return makeOutput(this.nextRefuelings.poll());                  
            }
            
        } else
        if(!this.nextDrainings.isEmpty()) {
            this.makeNextDraining();
            return makeOutput(this.nextDrainings.poll());            
        } else
        if(!this.nextRefuelings.isEmpty()) {
            this.makeNextRefueling();
            return makeOutput(this.nextRefuelings.poll());
        }    
        
        return null;
    }
    
    private RefuelingState makeOutput(RefuelingState state) {
        
        //LOGGER.log(Level.INFO, this.currentApproximatedData.toString());
        
        RefuelingState output = new RefuelingState(state);
        
        if(output.getStartStateDate()==Integer.MIN_VALUE){
            output.setStartStateDate(this.from);
        }
        
        if(output.getFinishStateDate()==Integer.MIN_VALUE){
            output.setFinishStateDate(this.till);
        }
        
        
        return output;
        
    }
    
    private DrainingState makeOutput(DrainingState state) {
        
        //LOGGER.log(Level.INFO, this.currentApproximatedData.toString());
        
        DrainingState output = new DrainingState(state);
        
        if(output.getStartStateDate()==Integer.MIN_VALUE){
            output.setStartStateDate(this.from);
        }
        
        if(output.getFinishStateDate()==Integer.MIN_VALUE){
            output.setFinishStateDate(this.till);
        }

        
        return output;
        
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    

    
    private class DrainingState extends DrainingOrRefueling {
        
        
        private int startStateDate;
        private int finishStateDate;
        private int value;

        public DrainingState(int startStateDate, int finishStateDate, int value) {
            super(FuelEventType.DRAINING,locale);
            this.startStateDate = startStateDate;
            this.finishStateDate = finishStateDate;
            this.value = value;
        }    
        
        public DrainingState(FuelEvent state){
            this(state.getStartStateDate(),state.getFinishStateDate(),state.getValue());
        }

        @Override
        public int getVehicleId() {
            return vehicleId;
        }

        @Override
        public int getStartStateDate() {
            return this.startStateDate;
        }

        @Override
        public int getFinishStateDate() {
            return this.finishStateDate;
        }

        @Override
        public int getValue() {
            return this.value;
        }
        
        public void setFinishStateDate(int finishStateDate) {
            this.finishStateDate = finishStateDate;
        }

        public void setStartStateDate(int startStateDate) {
            this.startStateDate = startStateDate;
        }

        public void setValue(int value) {
            this.value = value;
        }

        
    }
    
    private class RefuelingState extends DrainingOrRefueling {
 
        private int startStateDate;
        private int finishStateDate;
        private int value;

        public RefuelingState(int startStateDate, int finishStateDate, int value) {
            super(FuelEventType.REFUELING,locale);
            this.startStateDate = startStateDate;
            this.finishStateDate = finishStateDate;
            this.value = value;
        }        
        
        public RefuelingState(FuelEvent state){
            this(state.getStartStateDate(),state.getFinishStateDate(),state.getValue());
        }

        @Override
        public int getVehicleId() {
            return vehicleId;
        }

        @Override
        public int getStartStateDate() {
            return this.startStateDate;
        }

        @Override
        public int getFinishStateDate() {
            return this.finishStateDate;
        }

        @Override
        public int getValue() {
            return this.value;
        }

        public void setFinishStateDate(int finishStateDate) {
            this.finishStateDate = finishStateDate;
        }

        public void setStartStateDate(int startStateDate) {
            this.startStateDate = startStateDate;
        }

        public void setValue(int value) {
            this.value = value;
        }
        
        

        
    }
    

    
}
