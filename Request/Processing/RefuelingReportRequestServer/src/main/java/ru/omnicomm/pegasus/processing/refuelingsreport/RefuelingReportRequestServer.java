/**
 * Copyright © 2012 ООО "Омникомм Технологии"
 */
package ru.omnicomm.pegasus.processing.refuelingsreport;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.base.Constants;
import ru.omnicomm.pegasus.base.RequestParameterException;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.messaging.data.responses.RefuelingResponseParser;
import ru.omnicomm.pegasus.messaging.data.responses.RefuelingResponseParser.DischargeAndFuelingDataResponse;
import ru.omnicomm.pegasus.messaging.data.responses.SummaryRefuelingResponseParser;
import ru.omnicomm.pegasus.messaging.data.responses.SummaryRefuelingResponseParser.SummaryDischargeAndFuelingDataResponse;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("RefuelingReportRequestServerImplementation")
public class RefuelingReportRequestServer implements ServerImplementation {

    public static final int RESPONSE_MESSAGE_TYPE = 221;
    public static final int REQUEST_MESSAGE_TYPE = 421;
    public static final int SUMMARY_RESPONSE_MESSAGE_TYPE = 226;
    public static final int SUMMARY_REQUEST_MESSAGE_TYPE = 426;
    public static final String START_PERIOD_FILTER = "startPeriod";
    public static final String FINISH_PERIOD_FILTER = "finishPeriod";
    public static final String VEHICLE_ID_FILTER = "vehicleId";
    public static final String CONTROLLED_UNIT_TYPE_NAME = "Fuel";

    private static Logger LOGGER = LoggerFactory.getLogger();

    private DataStorageService dataStorage;
    private JmsBridgeService jmsBridge;

    private Server self;

    private ControlEntityHelper baseHelper;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     * @param blService   сервис бизнес платформы.
     */
    @ServerConstructor
    public RefuelingReportRequestServer(DataStorageService dataStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.dataStorage = dataStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();
                jmsBridge.registerServer(RefuelingReportRequestServer.class, self, REQUEST_MESSAGE_TYPE, SUMMARY_REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                jmsBridge.unregisterServer(RefuelingReportRequestServer.class, self);
            }
        });
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }

        try {
            processMessage(request);
        } catch (Exception ex) {
            LOGGER.info("Can't process data", ex);
        }
    }


    private void processMessage(SendRequestParser.SendRequest request)
            throws DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException {

        int startPeriod = getStartPeriodFilter(request);
        int finishPeriod = getFinishPeriodFilter(request);
        String locale = CommonUtil.getLocale(request);
        List<Long> vehicleIds = getVehicleIds(request);

        int totalMessagesCounter = 0;
        int inBlockMessagesCounter = 0;
        int blockNum = 0;
        String instanceId = request.getInstanceId();
        int requestType = request.getMessageType();
//        Response.Builder responseBuilder = makeResponseBuilder(blockNum, requestId);
        DischargeAndFuelingDataResponse.Builder responseBuilder = makeResponseBuilder(blockNum, instanceId);
        int totalValue = 0;
        int totalDischargeValue = 0;
        int totalFillingValue = 0;
        int totalDrainValue = 0;
        //Формируем список значений заданного параметра
        responseBuilderLoop:
        for (long vehicleId : vehicleIds) {
            Integer sourceId = baseHelper.selectFirstSoureceId(CONTROLLED_UNIT_TYPE_NAME, vehicleId);
            if (sourceId == null) {
                continue;
            }
            
            FuelEventsIterator fuelEvents = new FuelEventsIterator(sourceId, (int)vehicleId,(int)startPeriod,(int)finishPeriod,this.dataStorage,locale);
            int drainValue = 0;
            int fillingValue = 0;
            while(fuelEvents.hasNext()){
                
                FuelEvent data = fuelEvents.next();
                
                switch(data.getFuelEventId()) {
                    case DRAINING:
                        drainValue+=data.getValue();
                        break;
                    case REFUELING:
                        fillingValue+=data.getValue();
                        break;
                }
                
                DischargeAndFuelingDataResponse.Row.Builder rowBuilder = DischargeAndFuelingDataResponse.Row.newBuilder();

                rowBuilder.setVehicleId(Long.toString(vehicleId));
                rowBuilder.setEventName(data.getFuelEventType());
                rowBuilder.setStartInterval(data.getStartStateDate());
                rowBuilder.setFinishInterval(data.getFinishStateDate());
                rowBuilder.setFuelValue(data.getValue());

                responseBuilder.addRows(rowBuilder);

                if (++inBlockMessagesCounter >= Constants.IN_BLOCK_MESSAGES_LIMIT) {
                    responseBuilder.setBlockState(RefuelingResponseParser.State.NORMAL);
                    if(requestType==REQUEST_MESSAGE_TYPE)
                        jmsBridge.send(responseBuilder.build());
                    responseBuilder = makeResponseBuilder(++blockNum, instanceId);
                    inBlockMessagesCounter = 0;
                }

                if (++totalMessagesCounter >= Constants.TOTAL_MESSAGES_LIMIT) {
                    break responseBuilderLoop;
                }                
            }     
            
            int dischargeValue;
            if(fuelEvents.getFirstApproximatedData()!=null && fuelEvents.getLastApproximatedData()!=null ){
                //расход топлива без учета сливов
                dischargeValue = fuelEvents.getFirstApproximatedData().getValue() - fuelEvents.getLastApproximatedData().getValue() + fillingValue;
                totalValue+=fuelEvents.getLastApproximatedData().getValue();
            } else {
                dischargeValue = 0;
            }
            
            totalDischargeValue+=dischargeValue;
            totalFillingValue+=fillingValue;
            totalDrainValue+=drainValue;
            
            
        } //for(int vehicleId : vehicleIds)

        responseBuilder.setBlockState(RefuelingResponseParser.State.LAST);
        if(requestType==REQUEST_MESSAGE_TYPE) {
            jmsBridge.send(responseBuilder.build());
        } else {
            SummaryDischargeAndFuelingDataResponse.Builder builder = SummaryDischargeAndFuelingDataResponse.newBuilder();
            builder.setInstanceId(instanceId);
            builder.setBlockNumber(0);
            builder.setBlockState(SummaryRefuelingResponseParser.State.LAST);
            builder.setMessageType(SUMMARY_RESPONSE_MESSAGE_TYPE);  
            builder.addRows(
                    
                    SummaryDischargeAndFuelingDataResponse.Row.newBuilder()
                    .setTotalValue(totalValue)
                    .setTotalDischargeValue(totalDischargeValue)
                    .setTotalFillingValue(totalFillingValue)
                    .setTotalDrainValue(totalDrainValue)
                    );
            
            jmsBridge.send(builder.build());
        }
            
        
        
        
    }

    private List<Long> getVehicleIds(SendRequestParser.SendRequest request) {
        List<String> strValues = CommonUtil.getRequestFilterValues(VEHICLE_ID_FILTER, request);
        checkIfEmpty(strValues, VEHICLE_ID_FILTER);

        List<Long> vehicleIds = new ArrayList<Long>(strValues.size());
        try {
            for (String strValue : strValues) {
                vehicleIds.add(Long.parseLong(strValue));
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Value of request parameter ")
                    .append(VEHICLE_ID_FILTER).append(" is not Long").toString();
            throw new RequestParameterException(errMsg, e);
        }

        return vehicleIds;
    }

    private int getStartPeriodFilter(SendRequestParser.SendRequest request) {
        List<Double> startPeriods = CommonUtil.getRequestFilterValues(START_PERIOD_FILTER, request);
        checkIfEmpty(startPeriods, START_PERIOD_FILTER);

        return startPeriods.get(0).intValue();
    }

    private int getFinishPeriodFilter(SendRequestParser.SendRequest request) {
        List<Double> finishPeriods = CommonUtil.getRequestFilterValues(FINISH_PERIOD_FILTER, request);
        checkIfEmpty(finishPeriods, FINISH_PERIOD_FILTER);

        return finishPeriods.get(0).intValue();
    }

    private void checkIfEmpty(List<?> filters, String filterName) {
        if (filters.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(filterName).append(" is required").toString();
            throw new RequestParameterException(message);
        }
    }

    private DischargeAndFuelingDataResponse.Builder makeResponseBuilder(int blockNum, String instanceId) {
        DischargeAndFuelingDataResponse.Builder builder = DischargeAndFuelingDataResponse.newBuilder();
        builder.setInstanceId(instanceId);
        builder.setBlockNumber(blockNum);
        builder.setMessageType(RESPONSE_MESSAGE_TYPE);  

        return builder;
    }

    

}
