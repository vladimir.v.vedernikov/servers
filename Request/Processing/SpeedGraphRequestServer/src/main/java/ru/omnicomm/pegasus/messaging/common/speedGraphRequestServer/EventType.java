/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.speedGraphRequestServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.movement.VelocityStatesParser.VelocityDOWN;
import ru.omnicomm.pegasus.messaging.data.movement.VelocityStatesParser.VelocityNORMAL;
import ru.omnicomm.pegasus.messaging.data.movement.VelocityStatesParser.VelocityUP;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum EventType {

    VELOCITY_UP(VelocityUP.getDefaultInstance().getMessageType()),
    VELOCITY_DOWN(VelocityDOWN.getDefaultInstance().getMessageType()),
    VELOCITY_NORMAL(VelocityNORMAL.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, EventType> map = new HashMap<Integer, EventType>();

    static {
        for (EventType type : EnumSet.allOf(EventType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private EventType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static EventType lookup(int code) {
        return map.get(code);
    }

}
