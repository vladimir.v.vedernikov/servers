/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.vehicleStates;

/**
 *
 * @author alexander
 */
public interface VehicleState {
    
    int getVehicleId();
    
    int getStartStateDate();
    
    int getFinishStateDate();
    
    int getStateId();
    
}
