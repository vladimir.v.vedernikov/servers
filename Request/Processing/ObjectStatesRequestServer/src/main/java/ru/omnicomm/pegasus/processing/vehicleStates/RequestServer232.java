/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.vehicleStates;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.omnicomm.pegasus.base.Constants;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.messaging.data.responses.ObjectStatesResponseParser.ObjectStatesResponse;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ObjectStatesRequestServerImplementation")
public class RequestServer232 implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 232;
    public static final int REQUEST_MESSAGE_TYPE = 432;

    public static final String SPEED_UNIT_TYPE_NAME = "Speed";
    public static final String FUEL_UNIT_TYPE_NAME = "Fuel";

    //private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private DataStorageService dataStorage;
    private SettingsStorageService settingsStorage;
    private JmsBridgeService jmsBridge;

    private ControlEntityHelper baseHelper;
    private Server self;

    static final int ALARM = 3300;
    static final int DRAINING = 3301;
    static final int CONTROL_ZONE_ALARM = 3302;
    static final int REFUELING = 3303;
    static final int SPEEDING = 3304;
    static final int PARKING = 3305;
    static final int MOVEMENT = 3306;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     */
    @ServerConstructor
    public RequestServer232(DataStorageService dataStorage, SettingsStorageService settingsStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.dataStorage = dataStorage;
        this.settingsStorage = settingsStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }


        try {
            long start = System.currentTimeMillis();
            processMessage(request);
            long total = System.currentTimeMillis() - start;

            LOGGER.info("TOTAL REQUEST PROCESSING TIME: " + total + " ms");
        } catch (Exception ex) {
            LOGGER.info("Can't process data", ex);
        }
    }

    private void processMessage(SendRequestParser.SendRequest request) throws IllegalArgumentException, DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException {
        List<SendRequestParser.SendRequest.Parameter> filter = request.getFilterList();
        long startPeriod = -1;
        long finishPeriod = -1;
        int vehicleId = -1;
        List<Integer> stateIds = new ArrayList<Integer>();

        String locale = "en";
        String requestId = request.getInstanceId();

        for (SendRequestParser.SendRequest.Parameter parameter : filter) {

            String key = parameter.getKey();

            if (key.equalsIgnoreCase("startPeriod")) {
                startPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("finishPeriod")) {
                finishPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("vehicleId")) {
                vehicleId = Integer.parseInt(parameter.getStringValue());
            } else if (key.equalsIgnoreCase("stateId")) {
                stateIds.add(Integer.parseInt(parameter.getStringValue()));
            } else if (key.equalsIgnoreCase("locale")) {
                locale = parameter.getStringValue();
            }

        }

        if (startPeriod < 0 || finishPeriod < 0 || vehicleId < 0 || stateIds.isEmpty()) {
            throw new IllegalArgumentException("There are not enough or wrong parameters in the request: " + request);
        }


        int totalMessagesCounter = 0;
        int inBlockMessagesCounter = 0;
        int blockNum = 0;
        ObjectStatesResponse.Builder responseBuilder = makeResponseBuilder(blockNum, requestId);

        responseBuilderLoop:
        for (int stateId : stateIds) {

            Integer sourceId = null;

            switch (stateId) {
                case DRAINING:
                case REFUELING:
                    sourceId = baseHelper.selectFirstSoureceId(FUEL_UNIT_TYPE_NAME, vehicleId);
                    break;
                case SPEEDING:
                case PARKING:
                case MOVEMENT:
                    sourceId = baseHelper.selectFirstSoureceId(SPEED_UNIT_TYPE_NAME, vehicleId);
            }

            if (sourceId == null) {
                continue;
            }

            Iterator<VehicleState> statesIterator;
            switch (stateId) {
                case DRAINING:
                    statesIterator = new DrainingIterator(sourceId, vehicleId, (int) startPeriod, (int) finishPeriod, this.dataStorage);
                    break;
                case REFUELING:
                    statesIterator = new RefuelingsIterator(sourceId, vehicleId, (int) startPeriod, (int) finishPeriod, this.dataStorage);
                    break;
                case SPEEDING:
                    statesIterator = new SpeedingIterator(sourceId, vehicleId, (int) startPeriod, (int) finishPeriod, this.dataStorage);
                    break;
                case PARKING:
                    statesIterator = new ParkingIterator(sourceId, vehicleId, (int) startPeriod, (int) finishPeriod, this.dataStorage);
                    break;
                case MOVEMENT:
                    statesIterator = new MovementIterator(sourceId, vehicleId, (int) startPeriod, (int) finishPeriod, this.dataStorage);
                    break;
                default:
                    continue;

            }

            while (statesIterator.hasNext()) {
                VehicleState state = statesIterator.next();
                responseBuilder.addRows(makeResponseRow(state));
                if (++inBlockMessagesCounter >= Constants.IN_BLOCK_MESSAGES_LIMIT) {
                    responseBuilder.setBlockState(ObjectStatesResponse.State.NORMAL);
                    MessageLite response = responseBuilder.build();
                    jmsBridge.send(response);
                    responseBuilder = makeResponseBuilder(++blockNum, requestId);
                    inBlockMessagesCounter = 0;
                }

                if (++totalMessagesCounter >= Constants.TOTAL_MESSAGES_LIMIT) {
                    break responseBuilderLoop;
                }
            }

        }

        responseBuilder.setBlockState(ObjectStatesResponse.State.LAST);
        MessageLite response = responseBuilder.build();
        jmsBridge.send(response);

    }

    private ObjectStatesResponse.Row makeResponseRow(VehicleState vehicleState) {
        ObjectStatesResponse.Row.Builder rowBuilder = ObjectStatesResponse.Row.newBuilder();

        rowBuilder.setVehicleId(Integer.toString(vehicleState.getVehicleId()));
        rowBuilder.setStartStateDate(vehicleState.getStartStateDate());
        rowBuilder.setFinishStateDate(vehicleState.getFinishStateDate());
        rowBuilder.setStateId(Integer.toString(vehicleState.getStateId()));

        return rowBuilder.build();
    }

    private ObjectStatesResponse.Builder makeResponseBuilder(int blockNum, String requestId) {

        ObjectStatesResponse.Builder responseBuilder = ObjectStatesResponse.newBuilder()

                .setMessageType(RESPONSE_MESSAGE_TYPE)
                .setInstanceId(requestId)
                .setBlockNumber(blockNum);

        return responseBuilder;
    }


    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                jmsBridge.registerServer(RequestServer232.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                super.visit(signal);    //To change body of overridden methods use File | Settings | File Templates.
            }
        });
    }


}
