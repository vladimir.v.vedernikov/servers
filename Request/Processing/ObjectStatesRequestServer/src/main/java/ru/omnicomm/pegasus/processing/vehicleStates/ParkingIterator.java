/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.vehicleStates;

import java.util.Iterator;
import ru.omnicomm.pegasus.messaging.data.movement.MovementStatesParser;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;

/**
 *
 * @author alexander
 */
public class ParkingIterator implements Iterator<VehicleState>{
    
    private static Logger LOGGER = LoggerFactory.getLogger();
    
    private final int vehicleId;
    private final int from;
    private final int till;
    
    private final EventsIterator startsIterator;
    private final EventsIterator endsIterator;
    
    private VehicleState state = null;

    public ParkingIterator(int sourceId, int vehicleId, int from, int till, DataStorageService dataStorageService) throws DataStorageServiceException {
        this.vehicleId = vehicleId;
        this.from = from;
        this.till = till;
        this.startsIterator = new EventsIterator(sourceId,MovementStatesParser.ParkingSTART.getDefaultInstance().getMessageType(),from,till,dataStorageService);
        this.endsIterator = new EventsIterator(sourceId,MovementStatesParser.ParkingEND.getDefaultInstance().getMessageType(),from,till,dataStorageService);
        
        this.makeNext();
    }
    
    

    private void makeNext(){
        
        try{
            int start=Integer.MIN_VALUE, end=Integer.MIN_VALUE;
            
            if(this.startsIterator.hasNext()){
                start = MovementStatesParser.ParkingSTART.parseFrom(this.startsIterator.next().toByteString()).getMessageTime();
            }
            if(this.endsIterator.hasNext()){
                end = MovementStatesParser.ParkingEND.parseFrom(this.endsIterator.next().toByteString()).getMessageTime();
            }
            
            if(start==Integer.MIN_VALUE && end==Integer.MIN_VALUE){
                this.state = null;
            } else
            if(start==Integer.MIN_VALUE) {
                this.state = new ParkingIterator.ParkingState(this.from,end);
            } else
            if(end==Integer.MIN_VALUE) {
                this.state = new ParkingIterator.ParkingState(start,this.till);
            } else {
                while(end<start){
                    if(this.endsIterator.hasNext()){
                        end = MovementStatesParser.ParkingEND.parseFrom(this.endsIterator.next().toByteString()).getMessageTime();
                    } else {
                        this.state = new ParkingIterator.ParkingState(start,this.till);
                        return;
                    }
                }
                this.state = new ParkingIterator.ParkingState(start,end);
            }
            
            
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, "", ex);
            this.state = null;
        }
        
        
    }
    
    

    @Override
    public boolean hasNext() {
        return this.state!=null;
    }

    @Override
    public VehicleState next() {
        VehicleState next = new ParkingIterator.ParkingState(this.state.getStartStateDate(), this.state.getFinishStateDate());
        makeNext();
        return next;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    private class ParkingState implements VehicleState {
        
        
        private final int startStateDate;
        private final int finishStateDate;

        public ParkingState(int startStateDate, int finishStateDate) {
            this.startStateDate = startStateDate;
            this.finishStateDate = finishStateDate;
        }              

        @Override
        public int getVehicleId() {
            return vehicleId;
        }

        @Override
        public int getStartStateDate() {
            return this.startStateDate;
        }

        @Override
        public int getFinishStateDate() {
            return this.finishStateDate;
        }

        @Override
        public int getStateId() {
            return RequestServer232.PARKING;
        }
        
    }
    
}
