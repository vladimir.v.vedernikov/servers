/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing.vehicleStates;

import com.google.protobuf.MessageLite;
import java.util.Iterator;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;

/**
 *
 * @author alexander
 */
public class EventsIterator implements Iterator<MessageLite> {
    
    private static Logger LOGGER = LoggerFactory.getLogger();
    
    private final Iterator<MessageChunk> iterator;
    private MessageLite[] messages = new MessageLite[0];
    private int index = 0;
    
    public EventsIterator(int sourceId, int messageType, int from, int till, DataStorageService dataStorage) throws DataStorageServiceException {      
        
        int t1 = dataStorage.getMaxMessageTimeBefore(sourceId, messageType, from);
        int t2 = dataStorage.getMinMessageTimeAfter(sourceId, messageType, till);
        
        if(t1 < 0){
            t1 = 0;
        }
        
        LOGGER.log(Level.INFO, "sourceId="+sourceId+", messageType="+messageType+", from="+from+", till="+till+", t1="+t1+", t2="+t2);
        
        this.iterator = dataStorage.query(sourceId, messageType, t1, t2);
        this.makeNext();
    }
    
    private void makeNext(){
        if(++this.index>=this.messages.length){
            while(this.iterator.hasNext()){
                this.messages = this.iterator.next().messages;
                this.index = 0;
                if(this.messages.length>0){              
                    break;
                }
            }
        }
    }

    @Override
    public boolean hasNext() {
        return this.index<this.messages.length;
    }

    @Override
    public MessageLite next() {
        MessageLite message = this.messages[index];
        this.makeNext();
        return message;
        
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
