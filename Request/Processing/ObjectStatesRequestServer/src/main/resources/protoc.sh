#!/bin/sh

DEST_PATH=../java

##############################################
# Create common parser (for parsing headers) #
##############################################

 FILE_NAME=SendRequestMessage.proto
 SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common
 ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
 protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
 rm ${FILE_NAME}



########################################
# Create parsers for required messages #
########################################

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/responses
FILE_NAME=ObjectStatesResponse.proto
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/fuel
for FILE_NAME in DrainingsAndRefuelingsData.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done

SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/movement
for FILE_NAME in VelocityStates.proto MovementStates.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done

#
# SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/data/location
# for FILE_NAME in SpeedAndAzimuth.proto Coordinate.proto; do
#     ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
#     protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
#     rm ${FILE_NAME}
# done


echo "done"
