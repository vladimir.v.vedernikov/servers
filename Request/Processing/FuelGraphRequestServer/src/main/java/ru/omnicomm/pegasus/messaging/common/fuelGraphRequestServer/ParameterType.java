/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.fuelGraphRequestServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.fuel.ApproximatedDataParser.ApproximatedData;
import ru.omnicomm.pegasus.messaging.data.fuel.SumUnsmoothedDataParser.SumUnsmoothedData;

/**
 *
 * @author alexander
 */
public enum ParameterType {

    SUM_UNSMOOTHED_DATA(SumUnsmoothedData.getDefaultInstance().getMessageType()),
    APPROXIMATED_DATA(ApproximatedData.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, ParameterType> map = new HashMap<Integer, ParameterType>();

    static {
        for (ParameterType type : EnumSet.allOf(ParameterType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private ParameterType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static ParameterType lookup(int code) {
        return map.get(code);
    }

}
