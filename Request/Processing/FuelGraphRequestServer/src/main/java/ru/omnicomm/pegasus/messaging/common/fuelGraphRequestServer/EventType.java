/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.messaging.common.fuelGraphRequestServer;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingSTART;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingSTART;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum EventType {


//    SUM_UNSMOOTHED_DATA(SumUnsmoothedData.getDefaultInstance().getMessageType()),
//    APPROXIMATED_DATA(ApproximatedData.getDefaultInstance().getMessageType()),
    DRAINING_BEGIN(DrainingSTART.getDefaultInstance().getMessageType()),
    DRAINING_END(DrainingEND.getDefaultInstance().getMessageType()),
    REFUELING_BEGIN(RefuelingSTART.getDefaultInstance().getMessageType()),
    REFUELING_END(RefuelingEND.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, EventType> map = new HashMap<Integer, EventType>();

    static {
        for (EventType type : EnumSet.allOf(EventType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private EventType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static EventType lookup(int code) {
        return map.get(code);
    }

}
