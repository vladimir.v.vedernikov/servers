/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ru.omnicomm.pegasus.base.AbstractRequestServer;
import ru.omnicomm.pegasus.base.Constants;
import ru.omnicomm.pegasus.base.RequestParameterException;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.messaging.data.responses.FuelDataResponseParser;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.common.fuelGraphRequestServer.EventType;
import ru.omnicomm.pegasus.messaging.common.fuelGraphRequestServer.ParameterType;
import ru.omnicomm.pegasus.messaging.data.fuel.ApproximatedDataParser.ApproximatedData;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.DrainingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.DrainingsAndRefuelingsDataParser.RefuelingEND;
import ru.omnicomm.pegasus.messaging.data.fuel.SumUnsmoothedDataParser.SumUnsmoothedData;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;

@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("FuelGraphRequestServerImplementation")
public class FuelGraphRequestServer implements ServerImplementation {

    public static final int RESPONSE_MESSAGE_TYPE = 222;

    public static final int REQUEST_MESSAGE_TYPE = 422;

    public static final String START_PERIOD_FILTER = "startPeriod";

    public static final String FINISH_PERIOD_FILTER = "finishPeriod";

    public static final String VEHICLE_ID_FILTER = "vehicleId";

    public static final String EVENT_ID_FILTER = "eventId";

    public static final String PARAMETER_ID_FILTER = "parameterId";

    public static final String CONTROLLED_UNIT_TYPE_NAME = "Fuel";

    private static Logger LOGGER = LoggerFactory.getLogger();

//    private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private DataStorageService dataStorage;

    private JmsBridgeService jmsBridge;

    private Server self;

    private ControlEntityHelper baseHelper;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     */
    @ServerConstructor
    public FuelGraphRequestServer(DataStorageService dataStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.dataStorage = dataStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                jmsBridge.registerServer(FuelGraphRequestServer.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                jmsBridge.unregisterServer(FuelGraphRequestServer.class, self);
            }
        });
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }

//        if (RESPONSE_MESSAGE_TYPE == request.getRequestedMessageType()) {
            try {
                processMessage(request);
            } catch (Exception ex) {
                LOGGER.info("Can't process data", ex);
            }
//        }
    }

//    private void processMessage(Request request) throws IllegalArgumentException, DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException{
//
//        List<Request.Parameter> filter =  request.getFilterList();
//
//        long startPeriod = -1;
//        long finishPeriod = -1;
//        List<Integer> vehicleIds = new ArrayList<Integer>();
//        List<EventType> eventTypes = new ArrayList<EventType>();
//        ParameterType parameterType = null;
//        String locale = "en";
//        String requestId = request.getRequestId();
//
//        for(Request.Parameter parameter : filter){
//
//            String key = parameter.getKey();
//
//            if(key.equalsIgnoreCase("startPeriod")){
//                startPeriod = parameter.getLongValue();
//            } else
//            if(key.equalsIgnoreCase("finishPeriod")){
//                finishPeriod = parameter.getLongValue();
//            } else
//            if(key.equalsIgnoreCase("vehicleId")){
//                vehicleIds.add((int)parameter.getLongValue());
//            } else
//            if(key.equalsIgnoreCase("eventId")){
//                int eventId = (int)parameter.getLongValue();
//                EventType eventType = EventType.lookup(eventId);
//                if(eventType==null){
//                    throw new IllegalArgumentException("Unknown event id: "+eventId);
//                }
//                eventTypes.add(eventType);
//            } else
//            if(key.equalsIgnoreCase("parameterId")){
//                parameterType = ParameterType.lookup((int)parameter.getLongValue());
//                if(parameterType==null){
//                    throw new IllegalArgumentException("Unknown parameter id: "+parameterType);
//                }
//            } else
//            if(key.equalsIgnoreCase("locale")){
//                locale = parameter.getStringValue();
//            }
//
//        }
//
//        if(startPeriod<0 || finishPeriod<0 || vehicleIds.isEmpty() || parameterType==null){
//            throw new IllegalArgumentException("There are not enough or wrong parameters in the request: "+request);
//        }
//
//
//
//
//
//        //Формируем список событий для каждого vehicle
//        HashMap<EventKey,String> eventsMap = new HashMap<EventKey,String>();
//        for(int vehicleId : vehicleIds) {
//            for(EventType eventType : eventTypes) {
//                Iterator<MessageChunk> iterator = dataStorage.query(vehicleId, eventType.getCode(), (int)startPeriod, (int)finishPeriod);
//                while(iterator.hasNext()){
//                    MessageChunk messageChunck = iterator.next();
//                    for(MessageLite message : messageChunck.messages) {
//                        byte[] bytes = message.toByteArray();
//                        MessageHeader messageHeader = MessageHeader.parseFrom(bytes);
//                        eventsMap.put(new EventKey(vehicleId,eventType.getCode(),messageHeader.getMessageTime()), this.getEventName(eventType, locale, bytes));
//                    }  //for(MessageLite message : messageChunck.messages)
//                }  //while(iterator.hasNext())
//            }  //for(EventType eventType : eventTypes)
//        }  //for(int vehicleId : vehicleIds)
//
//        int totalMessagesCounter = 0;
//        int inBlockMessagesCounter = 0;
//        int blockNum = 0;
//        Response.Builder responseBuilder = makeResponseBuilder(parameterType, blockNum, requestId, locale);
//        //Формируем список значений заданного параметра
//        for(int vehicleId : vehicleIds) {
//
//            Iterator<MessageChunk> iterator = dataStorage.query(vehicleId, parameterType.getCode(), (int)startPeriod, (int)finishPeriod);
//            while(iterator.hasNext()){
//                MessageChunk messageChunck = iterator.next();
//                for(MessageLite message : messageChunck.messages) {
//                    byte[] bytes = message.toByteArray();
//                    MessageHeader messageHeader = MessageHeader.parseFrom(bytes);
//                    long pointDate = (long)messageHeader.getMessageTime();
//                    Response.ParameterRow.Builder parameterRowBuilder = Response.ParameterRow.newBuilder();
//                    parameterRowBuilder.addParameters(getIntValueParameter("vehicleId", vehicleId));
//                    parameterRowBuilder.addParameters(getLongValueParameter("pointDate", pointDate));
//                    double parameterValue = 0.0;
//                    switch(parameterType) {
//                        case SUM_UNSMOOTHED_DATA:
//                            parameterValue = SumUnsmoothedData.parseFrom(bytes).getValue() / 10.0;
//                            break;
//                        case APPROXIMATED_DATA:
//                            parameterValue = ApproximatedData.parseFrom(bytes).getValue() / 10.0;
//                            break;
//                    }
//                    parameterRowBuilder.addParameters(getDoubleValueParameter("parameterValue", parameterValue));
//                    //Добавляем события, соответствующие этой точке
//                    for (EventType type : EnumSet.allOf(EventType.class)) {
//                        String eventName = eventsMap.get(new EventKey(vehicleId,type.getCode(),pointDate));
//                        if(eventName!=null){
//                            parameterRowBuilder.addParameters(getIntValueParameter("eventId", type.getCode()));
//                            parameterRowBuilder.addParameters(getStringValueParameter("eventName", eventName));
//                        }
//                    }
//
//                    responseBuilder.addRows(parameterRowBuilder);
//                    if(++inBlockMessagesCounter>=IN_BLOCK_MESSAGES_LIMIT){
//                        jmsBridge.send(responseBuilder.build());
//                        responseBuilder = makeResponseBuilder(parameterType,
//                                ++blockNum, requestId, locale);
//                        inBlockMessagesCounter=0;
//                    }
//
//                    if(++totalMessagesCounter>=TOTAL_MESSAGES_LIMIT){
//                        break;
//                    }
//
//
//                } //for(MessageLite message : messageChunck.messages)
//            } //while(iterator.hasNext())
//
//            if(totalMessagesCounter>=TOTAL_MESSAGES_LIMIT){
//                break;
//            }
//
//        } //for(int vehicleId : vehicleIds)
//
//
//     //   if(inBlockMessagesCounter>0){
//            jmsBridge.send(responseBuilder.build());
//     //   }
//
//
//
//    }

    private void processMessage(SendRequestParser.SendRequest request)
            throws DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException {

        int startPeriod = getStartPeriodFilter(request);
        int finishPeriod = getFinishPeriodFilter(request);

        String locale = CommonUtil.getLocale(request);

        List<Long> vehicleIds = getVehicleIds(request);
        List<EventType> eventTypes = getEventTypes(request);
        ParameterType parameterType = getParameterType(request);
        String parameterName = getParameterName(parameterType, locale);

        //Формируем список событий для каждого vehicle
        HashMap<EventKey, String> eventsMap = new HashMap<EventKey, String>();
        for (long vehicleId : vehicleIds) {
            Integer sourceId = baseHelper.selectFirstSoureceId(CONTROLLED_UNIT_TYPE_NAME, vehicleId);
            if (sourceId == null) {
                continue;
            }
            for (EventType eventType : eventTypes) {
                Iterator<MessageChunk> iterator = dataStorage.query(sourceId, eventType.getCode(), startPeriod, finishPeriod);
                while (iterator.hasNext()) {
                    MessageChunk messageChunck = iterator.next();
                    for (MessageLite message : messageChunck.messages) {
                        byte[] bytes = message.toByteArray();
                        MessageHeader messageHeader = MessageHeader.parseFrom(bytes);
                        eventsMap.put(new EventKey(sourceId, eventType.getCode(), messageHeader.getMessageTime()), this.getEventName(eventType, locale, bytes));
                    }  //for(MessageLite message : messageChunck.messages)
                }  //while(iterator.hasNext())
            }  //for(EventType eventType : eventTypes)
        }  //for(int vehicleId : vehicleIds)

        int totalMessagesCounter = 0;
        int inBlockMessagesCounter = 0;
        int blockNum = 0;
        String instanceId = request.getInstanceId();
        FuelDataResponseParser.FuelResponse.Builder responseBuilder = makeResponseBuilder(blockNum, instanceId);

        //Формируем список значений заданного параметра
        for (long vehicleId : vehicleIds) {
            Integer sourceId = baseHelper.selectFirstSoureceId(CONTROLLED_UNIT_TYPE_NAME, vehicleId);
            if (sourceId == null) {
                continue;
            }

            Iterator<MessageChunk> iterator = dataStorage.query(sourceId, parameterType.getCode(), startPeriod, finishPeriod);
            while (iterator.hasNext()) {
                MessageChunk messageChunck = iterator.next();
                for (MessageLite message : messageChunck.messages) {
                    byte[] bytes = message.toByteArray();
                    MessageHeader messageHeader = MessageHeader.parseFrom(bytes);
                    long pointDate = (long) messageHeader.getMessageTime();
                    double parameterValue = parseParameterValue(parameterType, bytes);

                    FuelDataResponseParser.FuelResponse.Row.Builder rowBuilder
                            = FuelDataResponseParser.FuelResponse.Row.newBuilder();

                    rowBuilder.setVehicleId(Integer.toString(sourceId));
                    rowBuilder.setPointDate(new Long(pointDate).doubleValue());
                    rowBuilder.setParameterValue(parameterValue);
                    rowBuilder.setParameterName(parameterName);

                    //Добавляем события, соответствующие этой точке
                    for (EventType type : EnumSet.allOf(EventType.class)) {
                        String eventName = eventsMap.get(new EventKey(sourceId, type.getCode(), pointDate));
                        if (eventName != null) {
                            rowBuilder.setEventId(type.getCode());
                            rowBuilder.setEventName(eventName);
                        }
                    }

                    responseBuilder.addRows(rowBuilder);
                    if (++inBlockMessagesCounter >= Constants.IN_BLOCK_MESSAGES_LIMIT) {
                        responseBuilder.setBlockState(FuelDataResponseParser.State.NORMAL);
                        jmsBridge.send(responseBuilder.build());
                        responseBuilder = makeResponseBuilder(++blockNum, instanceId);
                        inBlockMessagesCounter = 0;
                    }

                    if (++totalMessagesCounter >= Constants.TOTAL_MESSAGES_LIMIT) {
                        break;
                    }
                } //for(MessageLite message : messageChunck.messages)
            } //while(iterator.hasNext())

            if (totalMessagesCounter >= Constants.TOTAL_MESSAGES_LIMIT) {
                break;
            }

        } //for(int vehicleId : vehicleIds)

        //   if(inBlockMessagesCounter>0){
        responseBuilder.setBlockState(FuelDataResponseParser.State.LAST);
        jmsBridge.send(responseBuilder.build());
        //   }
    }

    private ParameterType getParameterType(SendRequestParser.SendRequest request) {
        List<Integer> parameterCode = CommonUtil.getRequestFilterValues(PARAMETER_ID_FILTER, request);
        checkIfEmpty(parameterCode, PARAMETER_ID_FILTER);

        ParameterType parameterType = ParameterType.lookup(parameterCode.get(0));
        if (parameterType == null) {
            throw new RequestParameterException("Unknown parameter id: " + parameterType);
        }

        return parameterType;
    }

    private List<EventType> getEventTypes(SendRequestParser.SendRequest request) {
        List<Integer> eventCodes = CommonUtil.getRequestFilterValues(EVENT_ID_FILTER, request);
        checkIfEmpty(eventCodes, EVENT_ID_FILTER);

        List<EventType> result = new ArrayList<EventType>(eventCodes.size());

        for (Integer eventCode : eventCodes) {
            EventType eventType = EventType.lookup(eventCode);
            if (eventType == null) {
                throw new RequestParameterException("Unknown event id: " + eventCode);
            }
            result.add(eventType);
        }

        return result;
    }

    private List<Long> getVehicleIds(SendRequestParser.SendRequest request) {
        List<String> strValues = CommonUtil.getRequestFilterValues(VEHICLE_ID_FILTER, request);
        checkIfEmpty(strValues, VEHICLE_ID_FILTER);

        List<Long> vehicleIds = new ArrayList<Long>(strValues.size());
        try {
            for (String strValue : strValues) {
                vehicleIds.add(Long.parseLong(strValue));
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Value of request parameter ")
                    .append(VEHICLE_ID_FILTER).append(" is not Long").toString();
            throw new RequestParameterException(errMsg, e);
        }

        return vehicleIds;
    }

    private int getStartPeriodFilter(SendRequestParser.SendRequest request) {
        List<Double> startPeriods = CommonUtil.getRequestFilterValues(START_PERIOD_FILTER, request);
        checkIfEmpty(startPeriods, START_PERIOD_FILTER);

        return startPeriods.get(0).intValue();
    }

    private int getFinishPeriodFilter(SendRequestParser.SendRequest request) {
        List<Double> finishPeriods = CommonUtil.getRequestFilterValues(FINISH_PERIOD_FILTER, request);
        checkIfEmpty(finishPeriods, FINISH_PERIOD_FILTER);

        return finishPeriods.get(0).intValue();
    }

    private void checkIfEmpty(List<?> filters, String filterName) {
        if (filters.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(filterName).append(" is required").toString();
            throw new RequestParameterException(message);
        }
    }

    private double parseParameterValue(ParameterType parameterType, byte[] bytes)
            throws InvalidProtocolBufferException {
        switch (parameterType) {
            case SUM_UNSMOOTHED_DATA:
                return SumUnsmoothedData.parseFrom(bytes).getValue() / 10.0;
            case APPROXIMATED_DATA:
                return ApproximatedData.parseFrom(bytes).getValue() / 10.0;
            default:
                throw new IllegalArgumentException("Unsupported parameterType=" + parameterType);
        }
    }

    private FuelDataResponseParser.FuelResponse.Builder makeResponseBuilder(int blockNum, String instanceId) {
        FuelDataResponseParser.FuelResponse.Builder builder = FuelDataResponseParser.FuelResponse.newBuilder();

        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setInstanceId(instanceId);
        builder.setBlockNumber(blockNum);

        return builder;
    }

    private String getParameterName(ParameterType parameterType, String locale) {
        //TODO: Load localization from the cloud DB
        if (locale.equalsIgnoreCase("ru")) {

            switch (parameterType) {
                case SUM_UNSMOOTHED_DATA:
                    return "Сырые данные";
                case APPROXIMATED_DATA:
                    return "Сглаженные данные";

                default:
                    throw new IllegalArgumentException("Unknown or unsupported parameter type: " + parameterType);
            }

        } else { //Default locale is "en"
            switch (parameterType) {
                case SUM_UNSMOOTHED_DATA:
                    return "Raw data";
                case APPROXIMATED_DATA:
                    return "Approximated data";

                default:
                    throw new IllegalArgumentException("Unknown or unsupported parameter type: " + parameterType);
            }
        }
    }

    private String getEventName(EventType eventType, String locale, byte[] bytes) throws InvalidProtocolBufferException {
        //TODO: Load localization from the cloud DB
        if (locale.equalsIgnoreCase("ru")) {

            switch (eventType) {

                case DRAINING_BEGIN:
                    return "Начало слива";
                case DRAINING_END:
                    return "Конец слива (" + (DrainingEND.parseFrom(bytes).getValue() / 10.0) + " л.)";
                case REFUELING_BEGIN:
                    return "Начало заправки";
                case REFUELING_END:
                    return "Конец заправки (" + (RefuelingEND.parseFrom(bytes).getValue() / 10.0) + " л.)";
                default:
                    throw new IllegalArgumentException("Unknown or unsupported event type: " + eventType);
            }

        } else { //Default locale is "en"
            switch (eventType) {

                case DRAINING_BEGIN:
                    return "Draining begins";
                case DRAINING_END:
                    return "Draining ends (" + (DrainingEND.parseFrom(bytes).getValue() / 10.0) + " l.)";
                case REFUELING_BEGIN:
                    return "Refueling begins";
                case REFUELING_END:
                    return "Refuelings ends (" + (RefuelingEND.parseFrom(bytes).getValue() / 10.0) + " l.)";
                default:
                    throw new IllegalArgumentException("Unknown or unsupported event type: " + eventType);
            }
        }
    }

    private class EventKey {
        private final int vehicleId;
        private final int eventId;
        private final long pointDate;

        public EventKey(int vehicleId, int eventId, long pointDate) {
            this.vehicleId = vehicleId;
            this.eventId = eventId;
            this.pointDate = pointDate;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final EventKey other = (EventKey) obj;
            if (this.vehicleId != other.vehicleId) {
                return false;
            }
            if (this.eventId != other.eventId) {
                return false;
            }
            if (this.pointDate != other.pointDate) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + this.vehicleId;
            hash = 67 * hash + this.eventId;
            hash = 67 * hash + (int) (this.pointDate ^ (this.pointDate >>> 32));
            return hash;
        }
    }
}

