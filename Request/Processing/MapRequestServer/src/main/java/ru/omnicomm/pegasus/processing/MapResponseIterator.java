/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.location.SpeedAndAzimuthParser.SpeedAndAzimuth;
import ru.omnicomm.pegasus.messaging.data.responses.GeographyParametersResponseParser.GeographyParametersResponse.Row;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;

/**
 *
 * @author alexander
 */
public class MapResponseIterator implements Iterator<Row.Builder>{
    
    private static Logger LOGGER = LoggerFactory.getLogger();
    
    private final int vehicleId;
    private final MessageLiteIterator coordinatesIterator;
    private final MessageLiteIterator speedAndAzimuthIterator;
    private MapResponseItem currentItem;
    
    private static final int DrainingSTART = 84;
    private static final int DrainingEND = 86;
    private static final int GeoZoneSTART = 88;
    private static final int GeoZoneEND = 89;
    private static final int RefuelingSTART = 85;
    private static final int RefuelingEND = 87;
    private static final int VelocityUP = 65;
    private static final int VelocityDOWN = 66;
    private static final int VelocityNormal = 67;
    private static final int ParkingSTART = 82;
    private static final int ParkingEND = 83;
    
    private final EventsStorage drainingSTARTs;
    private final EventsStorage drainingENDs;
    private final EventsStorage geoZoneSTARTs;
    private final EventsStorage geoZoneENDs;
    private final EventsStorage refuelingSTARTs;
    private final EventsStorage refuelingENDs;
    private final EventsStorage velocityUPs;
    private final EventsStorage velocityDOWNs;
    private final EventsStorage velocityNormals;
    private final EventsStorage parkingSTARTs;
    private final EventsStorage parkingENDs;

    public MapResponseIterator(int vehicleId, int from, int till, DataStorageService dataStorage, ControlEntityHelper baseHelper) throws DataStorageServiceException {
        this.vehicleId = vehicleId;
        
        Integer sourceId = baseHelper.selectFirstSoureceId("Coordinates", vehicleId);
        if(sourceId==null){
            this.coordinatesIterator = null;
            this.speedAndAzimuthIterator = null;
            this.geoZoneSTARTs = null;
            this.geoZoneENDs = null;
            this.drainingSTARTs = null;
            this.drainingENDs = null;
            this.refuelingSTARTs = null;
            this.refuelingENDs = null;
            this.velocityUPs = null;
            this.velocityDOWNs = null;
            this.velocityNormals = null;
            this.parkingSTARTs = null;
            this.parkingENDs = null;
            
        } else {
            //dataStorage.query(sourceId, messageType, from, till);
            this.coordinatesIterator = 
                    new MessageLiteIterator(sourceId,MapRequestMessageType.COORDINATE.getCode(),from,till,dataStorage);
            this.speedAndAzimuthIterator = 
                    new MessageLiteIterator(sourceId,MapRequestMessageType.SPEED_AND_AZIMUTH.getCode(),from,till,dataStorage);
            this.geoZoneSTARTs = 
                    new EventsStorage(dataStorage.query(sourceId, GeoZoneSTART, from, till));
            this.geoZoneENDs = 
                    new EventsStorage(dataStorage.query(sourceId,GeoZoneEND,from,till));
            
            sourceId = baseHelper.selectFirstSoureceId("Fuel", vehicleId);
            if(sourceId==null){
                this.drainingSTARTs = null;
                this.drainingENDs = null;
                this.refuelingSTARTs = null;        
                this.refuelingENDs = null;
            } else {
                this.drainingSTARTs = 
                        new EventsStorage(dataStorage.query(sourceId,DrainingSTART,from,till));
                this.drainingENDs = 
                        new EventsStorage(dataStorage.query(sourceId,DrainingEND,from,till));
                this.refuelingSTARTs = 
                        new EventsStorage(dataStorage.query(sourceId,RefuelingSTART,from,till));
                this.refuelingENDs = 
                        new EventsStorage(dataStorage.query(sourceId,RefuelingEND,from,till));
            }
            
            sourceId = baseHelper.selectFirstSoureceId("Speed", vehicleId);
            if(sourceId==null){
                this.velocityUPs = null;
                this.velocityDOWNs = null;
                this.velocityNormals = null;        
                this.parkingSTARTs = null;
                this.parkingENDs = null;
            } else {
                this.velocityUPs = 
                        new EventsStorage(dataStorage.query(sourceId,VelocityUP,from,till));
                this.velocityDOWNs = 
                        new EventsStorage(dataStorage.query(sourceId,VelocityDOWN,from,till));
                this.velocityNormals = 
                        new EventsStorage(dataStorage.query(sourceId,VelocityNormal,from,till));
                this.parkingSTARTs = 
                        new EventsStorage(dataStorage.query(sourceId,ParkingSTART,from,till));
                this.parkingENDs = 
                        new EventsStorage(dataStorage.query(sourceId,ParkingEND,from,till));
            }
            
            this.makeNext();
        }
        
        
        
        
        
        
    }

    private void makeNext(){
        this.currentItem = null;
        if(this.coordinatesIterator.hasNext() && this.speedAndAzimuthIterator.hasNext()){
            try{
                Coordinate c = Coordinate.parseFrom(this.coordinatesIterator.next().toByteString());
                SpeedAndAzimuth s = SpeedAndAzimuth.parseFrom(this.speedAndAzimuthIterator.next().toByteString());
                
                this.currentItem = this.getMapResponseItem(c,s);
                
            } catch (Exception ex) {
                LOGGER.log(Level.WARN, "", ex);
            }
            
        }
        
    }
    
    private MapResponseItem getMapResponseItem(Coordinate c, SpeedAndAzimuth s) throws InvalidProtocolBufferException {
        int messageTime = c.getMessageTime() >= s.getMessageTime() ? c.getMessageTime() : s.getMessageTime();
        MapResponseItem m = new MapResponseItem(this.vehicleId,messageTime);
        if(c.getMessageTime()==s.getMessageTime()){  
            m.setCoordinates(c);
            m.setSpeedAndAzimuth(s);
            
            m.addEvents(this.getEvents(messageTime));
            
            return m;
        }
        else
        if(c.getMessageTime() > s.getMessageTime()){
            if(this.speedAndAzimuthIterator.hasNext()){
                SpeedAndAzimuth nexts = SpeedAndAzimuth.parseFrom(this.speedAndAzimuthIterator.next().toByteString());
                return getMapResponseItem(c,nexts);
            } else {
                return m;
            }          
        } else {
            if(this.coordinatesIterator.hasNext()){
                Coordinate nextc = Coordinate.parseFrom(this.coordinatesIterator.next().toByteString());
                return getMapResponseItem(nextc,s);
            } else {
                return m;
            }    
        }
        
        
        
    }
    
    private List<Integer> getEvents(int messageTime) throws InvalidProtocolBufferException{
        List<Integer> events = new ArrayList<Integer>();
        
        addEvent(messageTime,this.drainingSTARTs,events);
        addEvent(messageTime,this.drainingENDs,events);
        addEvent(messageTime,this.refuelingSTARTs,events);
        addEvent(messageTime,this.refuelingENDs,events);
        addEvent(messageTime,this.geoZoneSTARTs,events);
        addEvent(messageTime,this.geoZoneENDs,events);
        addEvent(messageTime,this.velocityDOWNs,events);
        addEvent(messageTime,this.velocityNormals,events);
        addEvent(messageTime,this.velocityUPs,events);
        addEvent(messageTime,this.parkingSTARTs,events);
        addEvent(messageTime,this.parkingENDs,events);
  
        return events;
    }
    
    private void addEvent(int messageTime, EventsStorage eventsStorage, List<Integer> events)  {
        
        while(this.getChunckId(messageTime)>eventsStorage.getChunkId() && eventsStorage.next()){}
        
        MessageHeader header = eventsStorage.getEvent(messageTime);
        if(header!=null){
            events.add(header.getMessageType());
        }
        
//        while(iterator!=null && iterator.hasNext()){
//            
//            
//            MessageHeader header = MessageHeader.parseFrom(iterator.next().toByteString());
//            LOGGER.info("EVENT: "+header.toString().replaceAll("\n", " ")+" (messageTime="+messageTime+")");
//            if(messageTime==header.getMessageTime()){
//                events.add(header.getMessageType());
//                break;
//            }
//        }
    }
    
    private int getChunckId(int messageTime){
        return messageTime - (messageTime % (3600*24) );
    }
    

    @Override
    public boolean hasNext() {
        return this.coordinatesIterator!=null && this.speedAndAzimuthIterator!=null && this.currentItem!=null && this.currentItem.isCompleted();
    }

    @Override
    public Row.Builder next() {
        Row.Builder r = this.currentItem.getResponseRow();
        this.makeNext();
        return r;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    private class MapResponseItem {
        
        private final int vehicleId;
        private final int messageTime;
        
        private double latitude;
        private double longitude;
        private double altitude;
        
        private double speed;
        private double azimuth;
        
        private boolean coordinatesFlag = false;
        private boolean speedAndAzimuthFlag = false;
        
        private final List<Integer> events = new ArrayList<Integer>();

        public MapResponseItem(int vehicleId, int messageTime) {
            this.vehicleId = vehicleId;
            this.messageTime = messageTime;
        }

        public int getMessageTime() {
            return messageTime;
        }
        
        public void setCoordinates(Coordinate c){
            if(c.getMessageTime()!=this.messageTime){
                throw new IllegalArgumentException("Wrong messageTime value");
            }
            this.latitude = c.getLatitude();
            this.longitude = c.getLongitude();
            this.altitude = c.getAltitude();
            this.coordinatesFlag = true;
        }
        
        public void setSpeedAndAzimuth(SpeedAndAzimuth s){
            if(s.getMessageTime()!=this.messageTime){
                throw new IllegalArgumentException("Wrong messageTime value");
            }
            this.speed = s.getSpeed();
            this.azimuth = s.getAzimuth();
            this.speedAndAzimuthFlag = true;
        }
        
        public boolean isCompleted(){
            return this.coordinatesFlag && this.speedAndAzimuthFlag;
        }
        
        public void addEvents(List<Integer> eventIds){
            this.events.addAll(eventIds);
        }
        
        public Row.Builder getResponseRow(){
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < this.events.size(); i++) {
                sb.append(this.events.get(i));
                if(i<this.events.size()-1){
                    sb.append(",");
                }
                
            }
            return Row.newBuilder()
                    .setVehicleId(Integer.toString(vehicleId))
                    .setPointDate(this.messageTime)
                    .setLatitude(this.latitude)
                    .setLongitude(this.longitude)
                    .setAltitude(this.altitude)
                    .setSpeed(this.speed)
                    .setAzimuth(this.azimuth)
                    .setStateId(sb.toString());              
        }
        
        
    }
    
}
