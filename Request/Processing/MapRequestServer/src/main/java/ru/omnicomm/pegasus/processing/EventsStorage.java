/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.common.CommonParser.MessageHeader;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.MessageChunk;


/**
 *
 * @author alexander
 */
public class EventsStorage {
    
    private int chunkId = Integer.MIN_VALUE;
    private final Map<Integer, MessageHeader> events = new HashMap<Integer, MessageHeader>();
    private final Iterator<MessageChunk> iterator;
    
    private static Logger LOGGER = LoggerFactory.getLogger();

    public EventsStorage(Iterator<MessageChunk> iterator) {
        this.iterator = iterator;
    }

    public int getChunkId() {
        return this.chunkId;
    }
    
    public MessageHeader getEvent(int messageTime){
        return this.events.get(messageTime);
    }
    
    public boolean next(){
        events.clear();
        chunkId = Integer.MIN_VALUE;
        while(this.iterator.hasNext()){
            MessageLite[] messages = this.iterator.next().messages;
            if(messages.length>0){
                try{
                    
                    for(MessageLite message : messages){
                        MessageHeader header = MessageHeader.parseFrom(message.toByteString());
                        if(chunkId<header.getMessageTime()){
                            chunkId = this.getChunckId(header.getMessageTime());
                        }
                        this.events.put(header.getMessageTime(), header);
                        
                    }
                    return true;
                    
                } catch (InvalidProtocolBufferException ex) {
                    LOGGER.info("Can't parse message",ex);
                }
            }
        }
        return false;
    }
    
    private int getChunckId(int messageTime){
        return messageTime - (messageTime % (3600*24));
    }
    
    
    
    
}
