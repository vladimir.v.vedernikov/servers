/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.data.location.CoordinateParser.Coordinate;
import ru.omnicomm.pegasus.messaging.data.location.SpeedAndAzimuthParser.SpeedAndAzimuth;

/**
 * Перечисление типов сообщений, с которыми работает сервер
 * @author alexander
 */
public enum MapRequestMessageType {

    COORDINATE(Coordinate.getDefaultInstance().getMessageType()),
    SPEED_AND_AZIMUTH(SpeedAndAzimuth.getDefaultInstance().getMessageType());

    private final int code;

    private static final Map<Integer, MapRequestMessageType> map = new HashMap<Integer, MapRequestMessageType>();

    static {
        for (MapRequestMessageType type : EnumSet.allOf(MapRequestMessageType.class)) {
            map.put(type.getCode(), type);
        }
    }

    private MapRequestMessageType(int cmd) {
        this.code = cmd;
    }

    /**
     * Возвращает код типа сообщения
     * @return код типа сообщения
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип сообщения по коду
     * @param code код типа сообщения
     * @return тип сообщения
     */
    public static MapRequestMessageType lookup(int code) {
        return map.get(code);
    }

}
