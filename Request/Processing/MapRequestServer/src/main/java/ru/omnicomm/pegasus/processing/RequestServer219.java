/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.util.ArrayList;
import java.util.List;

import ru.omnicomm.pegasus.base.Constants;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.messaging.data.responses.GeographyParametersResponseParser.GeographyParametersResponse;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

/**
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("MapRequestServerImplementation")
public class RequestServer219 implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    public static final int RESPONSE_MESSAGE_TYPE = 219;
    public static final int REQUEST_MESSAGE_TYPE = 419;
    public static final String CONTROLLED_UNIT_TYPE_NAME = "Coordinates";
    // private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private DataStorageService dataStorage;
    private JmsBridgeService jmsBridge;

    private ControlEntityHelper baseHelper;

    private Server self;

    /**
     * Конструктор.
     *
     * @param dataStorage сервис хранилища данных.
     * @param jmsBridge   сервис брокера сообщений.
     */
    @ServerConstructor
    public RequestServer219(DataStorageService dataStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.dataStorage = dataStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }


        try {
            long start = System.currentTimeMillis();
            processMessage(request);
            long total = System.currentTimeMillis() - start;

            LOGGER.info("TOTAL REQUEST PROCESSING TIME: " + total + " ms");
        } catch (Exception ex) {
            LOGGER.info("Can't process data", ex);
        }
    }

    private void processMessage(SendRequestParser.SendRequest request) throws IllegalArgumentException, DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException {
        List<SendRequestParser.SendRequest.Parameter> filter = request.getFilterList();
        long startPeriod = -1;
        long finishPeriod = -1;
        List<Integer> vehicleIds = new ArrayList<Integer>();
        String locale = "en";
        String requestId = request.getInstanceId();

        for (SendRequestParser.SendRequest.Parameter parameter : filter) {

            String key = parameter.getKey();

            if (key.equalsIgnoreCase("startPeriod")) {
                startPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("finishPeriod")) {
                finishPeriod = (int) parameter.getDoubleValue();
            } else if (key.equalsIgnoreCase("vehicleId")) {
                vehicleIds.add(Integer.parseInt(parameter.getStringValue()));
            } else if (key.equalsIgnoreCase("locale")) {
                locale = parameter.getStringValue();
            }

        }

        if (startPeriod < 0 || finishPeriod < 0 || vehicleIds.isEmpty()) {
            throw new IllegalArgumentException("There are not enough or wrong parameters in the request: " + request);
        }
        int totalMessagesCounter = 0;
        int inBlockMessagesCounter = 0;
        int blockNum = 0;
        GeographyParametersResponse.Builder responseBuilder = makeResponseBuilder(blockNum, requestId);
        responseBuilderLoop:
        for (int vehicleId : vehicleIds) {
            MapResponseIterator iterator = new MapResponseIterator(vehicleId, (int) startPeriod, (int) finishPeriod, this.dataStorage, this.baseHelper);
            while (iterator.hasNext()) {
                responseBuilder.addRows(iterator.next());
                if (++inBlockMessagesCounter >= Constants.IN_BLOCK_MESSAGES_LIMIT) {
                    responseBuilder.setBlockState(GeographyParametersResponse.State.NORMAL);
                    MessageLite response = responseBuilder.build();
                    LOGGER.info("PREPARED RESPONSE: " + response);
                    jmsBridge.send(response);
                    responseBuilder = makeResponseBuilder(++blockNum, requestId);
                    inBlockMessagesCounter = 0;
                }

                if (++totalMessagesCounter >= Constants.TOTAL_MESSAGES_LIMIT) {
                    break responseBuilderLoop;
                }
            }
        }
        responseBuilder.setBlockState(GeographyParametersResponse.State.LAST);
        MessageLite response = responseBuilder.build();
        LOGGER.info("PREPARED RESPONSE: " + response);
        jmsBridge.send(response);

    }

    private GeographyParametersResponse.Builder makeResponseBuilder(int blockNum, String requestId) {

        GeographyParametersResponse.Builder responseBuilder = GeographyParametersResponse.newBuilder()

                .setMessageType(RESPONSE_MESSAGE_TYPE)
                .setInstanceId(requestId)
                .setBlockNumber(blockNum);

        return responseBuilder;
    }


    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                jmsBridge.registerServer(RequestServer219.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                super.visit(signal);    //To change body of overridden methods use File | Settings | File Templates.
            }
        });
    }


}
