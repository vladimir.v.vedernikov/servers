/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 */
package ru.omnicomm.pegasus.processing;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.omnicomm.pegasus.base.AbstractRequestServer;
import ru.omnicomm.pegasus.base.RequestParameterException;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.base.util.ControlEntityHelper;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.messaging.data.responses.SpeedAndStatesResponseParser;
import ru.omnicomm.pegasus.messaging.data.responses.SpeedAndStatesResponseParser.SpeedAndStatesResponse;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser.SearchingBuffer;
import ru.omnicomm.pegasus.messaging.settings.fuel.DrainingRefuelingSearchingParser.SearchingBuffer.ProcessId;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementStateDefinitionSettingsParser.MovementStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityStateDefinitionSettingsParser.VelocityStateDefinitionBuffer;
import ru.omnicomm.pegasus.messaging.settings.movement.VelocityStateDefinitionSettingsParser.VelocityStateDefinitionSettings;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("VehiclesListRequestServerImplementation")
public class VehiclesListRequestServer implements ServerImplementation {

    public static final int RESPONSE_MESSAGE_TYPE = 220;

    public static final int REQUEST_MESSAGE_TYPE = 420;

    public static final String VEHICLE_ID_FILTER = "vehicleId";

    public static final String STATE_ID_FILTER = "stateId";

    public static final String CONTROLLED_UNIT_TYPE_NAME = "Speed";

    private static Logger LOGGER = LoggerFactory.getLogger();

//    private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    private SettingsStorageService settingsStorage;
    private JmsBridgeService jmsBridge;

    private Server self;

    private ControlEntityHelper baseHelper;

//        Коды событий:
//        10;Тревога;Alarm
//        20;Слив топлива;Draining
//        30;Нарушение границы зоны контроля;Control zone alarm
//        40;Заправка;Fuelling
//        50;Нарушение скоростного режима;Speeding
//        60;Стоянка;Parking
//        70;Движение;Motion


    private static final long ALARM = 3300L;
    private static final long DRAINING = 3301L;
    private static final long CONTROL_ZONE_ALARM = 3302L;
    private static final long REFUELING = 3303L;
    private static final long SPEEDING = 3304L;
    private static final long PARKING = 3305L;
    private static final long MOVEMENT = 3306L;


    /**
     * Конструктор.
     *
     * @param settingsStorage сервис хранилища настроек.
     * @param jmsBridge       сервис брокера сообщений.
     */
    @ServerConstructor
    public VehiclesListRequestServer(SettingsStorageService settingsStorage, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.settingsStorage = settingsStorage;
        this.jmsBridge = jmsBridge;
        this.baseHelper = new ControlEntityHelper(blService);
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                jmsBridge.registerServer(VehiclesListRequestServer.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) {
                jmsBridge.unregisterServer(VehiclesListRequestServer.class, self);
            }
        });
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }

//        if (RESPONSE_MESSAGE_TYPE == request.getRequestedMessageType()) {
            try {
                processMessage(request);
            } catch (Exception ex) {
                LOGGER.info("Can't process data", ex);
            }
//        }
    }

//    private void processMessage(Request request) throws IllegalArgumentException, DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException{
//        List<Request.Parameter> filter =  request.getFilterList();
//
//        List<Integer> vehicleIds = new ArrayList<Integer>();
//        List<Integer> stateIds = new ArrayList<Integer>();
//        String locale = "en";
//        String requestId = request.getRequestId();
//
//        for(Request.Parameter parameter : filter){
//
//            String key = parameter.getKey();
//
//            if(key.equalsIgnoreCase("vehicleId")){
//                vehicleIds.add((int)parameter.getLongValue());
//            } else
//            if(key.equalsIgnoreCase("stateId")){
//                stateIds.add((int)parameter.getLongValue());
//            } else
//            if(key.equalsIgnoreCase("locale")){
//                locale = parameter.getStringValue();
//            }
//
//        }
//
//        if(vehicleIds.isEmpty()){
//            throw new IllegalArgumentException("There are not enough or wrong parameters in the request: "+request);
//        }
//
//        int inBlockMessagesCounter = 0;
//        int blockNum = 0;
//
//        Response.Builder responseBuilder = makeResponseBuilder(blockNum, requestId);
//        //Формируем список значений заданного параметра
//        for(int vehicleId : vehicleIds) {
//
//            VelocityData velocityData = this.getCurrentSpeed(vehicleId);
//            Response.ParameterRow.Builder parameterRowBuilder = Response.ParameterRow.newBuilder();
//            parameterRowBuilder.addParameters(getIntValueParameter("vehicleId", vehicleId));
//            if(velocityData!=null){
//                parameterRowBuilder.addParameters(getLongValueParameter("pointDate", velocityData.dateId));
//                parameterRowBuilder.addParameters(getDoubleValueParameter("speed", velocityData.value));
//            }
//            StringBuilder states = new StringBuilder();
//            for(int stateId : stateIds){
//
//                switch(stateId) {
//
//                    case DRAINING:
//                    case REFUELING:
//
//                        ProcessId fuelState = this.getDrainingRefuelingState(vehicleId);
//                        int fuelStateId = 0;
//                        if(stateId == DRAINING && fuelState == ProcessId.DRAINING){
//                            fuelStateId = DRAINING;
//                        }
//                        if(stateId == REFUELING && fuelState == ProcessId.REFUELING){
//                            fuelStateId = REFUELING;
//                        }
//
//                        if(fuelStateId>0){
//                            if(states.length()>0){
//                                states.append(",");
//                            }
//                            states.append(fuelStateId);
//                        }
//
//                        break;
//
//                    case SPEEDING:
//                        if(velocityData!=null){
//                            int speedLimit = this.getSpeedLimit(vehicleId);
//                            if(speedLimit>=0 && velocityData.value>speedLimit){
//                                if(states.length()>0){
//                                    states.append(",");
//                                }
//                                states.append(SPEEDING);
//                            }
//                        }
//
//                        break;
//
//                    case PARKING:
//                    case MOVEMENT:
//
//                        MovementStateDefinitionBuffer.State movementState = this.getMovementState(vehicleId);
//                        int movementStateId = 0;
//                        if(stateId == MOVEMENT && movementState == MovementStateDefinitionBuffer.State.MOVEMENT){
//                            movementStateId = MOVEMENT;
//                        }
//                        if(stateId == PARKING && movementState == MovementStateDefinitionBuffer.State.PARKING){
//                            movementStateId = PARKING;
//                        }
//
//                        if(movementStateId>0){
//                            if(states.length()>0){
//                                states.append(",");
//                            }
//                            states.append(movementStateId);
//                        }
//
//                        break;
//
//
//                }
//
//            }
//            parameterRowBuilder.addParameters(getStringValueParameter("stateIdList", states.toString()));
//
//            responseBuilder.addRows(parameterRowBuilder);
//
//        } //for(int vehicleId : vehicleIds)
//
//        Response resp = responseBuilder.build();
//        LOGGER.log(Level.INFO, new StringBuilder().append("Output message: ").append(resp).toString());
//        jmsBridge.send(resp);
//
//    }


    private void processMessage(SendRequestParser.SendRequest request)
            throws DataStorageServiceException, InvalidProtocolBufferException, JmsBridgeServiceException {
//        String locale = CommonUtil.getLocale(request);

        List<Long> vehicleIds = getVehicleIds(request);
        List<Long> stateIds = getStateIds(request);

//        int inBlockMessagesCounter = 0;
        int blockNum = 0;
        String instanceId = request.getInstanceId();

        SpeedAndStatesResponse.Builder responseBuilder = makeResponseBuilder(blockNum, instanceId);
        //Формируем список значений заданного параметра
        for (long vehicleId : vehicleIds) {
            Integer sourceId = baseHelper.selectFirstSoureceId(CONTROLLED_UNIT_TYPE_NAME, vehicleId);
            if (sourceId == null) {
                continue;
            }

            VelocityData velocityData = this.getCurrentSpeed(sourceId);

            SpeedAndStatesResponse.Row.Builder rowBuilder = SpeedAndStatesResponse.Row.newBuilder();

            rowBuilder.setVehicleId(Integer.toString(sourceId));
            if (velocityData != null) {
                rowBuilder.setPointDate(new Integer(velocityData.dateId).doubleValue());
                rowBuilder.setSpeed(new Integer(velocityData.value).doubleValue());

            }

            List<String> states = getStates(sourceId, stateIds, velocityData);
            rowBuilder.addAllStateId(states);

            responseBuilder.addRows(rowBuilder);
        } //for(int vehicleId : vehicleIds)

        responseBuilder.setBlockState(SpeedAndStatesResponseParser.State.LAST);
        jmsBridge.send(responseBuilder.build());
    }

    private List<String> getStates(int sourceId, List<Long> stateIds, VelocityData velocityData) {
        List<String> states = new ArrayList<String>();
        for (long stateId : stateIds) {
            if (stateId == DRAINING) {
                ProcessId fuelState = this.getDrainingRefuelingState(sourceId);
                if (fuelState == ProcessId.DRAINING) {
                    states.add(Long.toString(DRAINING));
                }
            } else if (stateId == REFUELING) {
                ProcessId fuelState = this.getDrainingRefuelingState(sourceId);
                if (fuelState == ProcessId.REFUELING) {
                    states.add(Long.toString(REFUELING));
                }
            } else if (stateId == SPEEDING) {
                if (velocityData != null) {
                    int speedLimit = this.getSpeedLimit(sourceId);
                    if (speedLimit >= 0 && velocityData.value > speedLimit) {
                        states.add(Long.toString(SPEEDING));
                    }
                }
            } else if (stateId == PARKING) {
                MovementStateDefinitionBuffer.State movementState = this.getMovementState(sourceId);
                if (movementState == MovementStateDefinitionBuffer.State.PARKING) {
                    states.add(Long.toString(PARKING));
                }
            } else if (stateId == MOVEMENT) {
                MovementStateDefinitionBuffer.State movementState = this.getMovementState(sourceId);
                if (movementState == MovementStateDefinitionBuffer.State.MOVEMENT) {
                    states.add(Long.toString(MOVEMENT));
                }
            } else {
                LOGGER.log(Level.INFO, "Unsupported stateId=" + stateId);
            }
        }
        return states;
    }

    private List<Long> getVehicleIds(SendRequestParser.SendRequest request) {
        List<String> strValues = CommonUtil.getRequestFilterValues(VEHICLE_ID_FILTER, request);
        checkIfEmpty(strValues, VEHICLE_ID_FILTER);

        List<Long> vehicleIds = new ArrayList<Long>(strValues.size());
        try {
            for (String strValue : strValues) {
                vehicleIds.add(Long.parseLong(strValue));
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Value of request parameter ")
                    .append(VEHICLE_ID_FILTER).append(" is not Long").toString();
            throw new RequestParameterException(errMsg, e);
        }

        return vehicleIds;
    }

    private List<Long> getStateIds(SendRequestParser.SendRequest request) {
        List<String> strValues = CommonUtil.getRequestFilterValues(STATE_ID_FILTER, request);
        List<Long> stateIds = new ArrayList<Long>(strValues.size());
        try {
            for (String strValue : strValues) {
                stateIds.add(Long.parseLong(strValue));
            }
        } catch (NumberFormatException e) {
            final String errMsg = new StringBuilder().append("Value of request parameter ")
                    .append(VEHICLE_ID_FILTER).append(" is not Long").toString();
            throw new RequestParameterException(errMsg, e);
        }

        return stateIds;
    }

    private void checkIfEmpty(List<?> filters, String filterName) {
        if (filters.isEmpty()) {
            final String message = new StringBuilder().append("Parameter ")
                    .append(filterName).append(" is required").toString();
            throw new RequestParameterException(message);
        }
    }

    private VelocityData getCurrentSpeed(int sourceId) {
        try {
            MessageLite settingsObject = this.settingsStorage.lookupInMemory(this.getVelocityBufferName(sourceId));
            if (settingsObject != null) {
                VelocityStateDefinitionBuffer buffer = VelocityStateDefinitionBuffer.parseFrom(settingsObject.toByteArray());
                int bufferSize = buffer.getBufferList().size();
                if (bufferSize > 0) {
                    VelocityStateDefinitionBuffer.Data data = buffer.getBuffer(bufferSize - 1);
                    return new VelocityData(data.getDataId(), data.getValue());
                } else {
                    LOGGER.log(Level.INFO, new StringBuilder().append("VelocityStateDefinitionBuffer is empty for sourceId=").append(sourceId).toString());
                    return new VelocityData(getCurrentTime(), 0);
                }
            } else {
                LOGGER.log(Level.INFO, new StringBuilder().append("VelocityStateDefinitionBuffer wasn't found for sourceId=").append(sourceId).toString());
                return new VelocityData(getCurrentTime(), 0);
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, new StringBuilder().append("Can't load VelocityStateDefinitionBuffer for sourceId=").append(sourceId).toString(), ex);
            return new VelocityData(getCurrentTime(), 0);
        }

    }

    private int getCurrentTime() {
        return ((int) (System.currentTimeMillis() / 1000) - 63072000);
    }

    private int getSpeedLimit(int sourceId) {
        try {
            MessageLite settingsObject = this.settingsStorage.lookupInMemory(this.getVelocitySettingsName(sourceId));
            if (settingsObject != null) {
                VelocityStateDefinitionSettings settings = VelocityStateDefinitionSettings.parseFrom(settingsObject.toByteArray());
                return settings.getValueMAX();
            } else {
                LOGGER.log(Level.INFO, new StringBuilder().append("VelocityStateDefinitionSettings wasn't found for sourceId=").append(sourceId).toString());
                return -1;
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, new StringBuilder().append("Can't load VelocityStateDefinitionSettings for sourceId=").append(sourceId).toString(), ex);
            return -1;
        }
    }

    private MovementStateDefinitionBuffer.State getMovementState(int sourceId) {
        try {
            MessageLite settingsObject = this.settingsStorage.lookupInMemory(this.getMovementBufferName(sourceId));
            if (settingsObject != null) {
                MovementStateDefinitionBuffer buffer = MovementStateDefinitionBuffer.parseFrom(settingsObject.toByteArray());
                return buffer.getCurrentState();
            } else {
                LOGGER.log(Level.INFO, new StringBuilder().append("MovementStateDefinitionBuffer wasn't found for sourceId=").append(sourceId).toString());
                return MovementStateDefinitionBuffer.State.NOT_DEFINED;
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, new StringBuilder().append("Can't load MovementStateDefinitionBuffer for sourceId=").append(sourceId).toString(), ex);
            return MovementStateDefinitionBuffer.State.NOT_DEFINED;
        }

    }

    private ProcessId getDrainingRefuelingState(int sourceId) {
        try {
            MessageLite settingsObject = this.settingsStorage.lookupInMemory(this.getSearchingBufferName(sourceId));
            if (settingsObject != null) {
                SearchingBuffer buffer = SearchingBuffer.parseFrom(settingsObject.toByteArray());
                return buffer.getLastProcess();
            } else {
                LOGGER.log(Level.INFO, new StringBuilder().append("SearchingBuffer wasn't found for sourceId=").append(sourceId).toString());
                return ProcessId.ORDINAL;
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARN, new StringBuilder().append("Can't load SearchingBuffer for sourceId=").append(sourceId).toString(), ex);
            return ProcessId.ORDINAL;
        }

    }

    private String getVelocityBufferName(int sourceId) {
        return String.format("src%d:velocityStates:buffer", sourceId);
    }

    private String getVelocitySettingsName(int sourceId) {
        return String.format("src%d:velocityStates:settings", sourceId);
    }

    private String getMovementBufferName(int sourceId) {
        return String.format("src%d:movementStates:buffer", sourceId);
    }

    private String getSearchingBufferName(int sourceId) {
        return String.format("src%d:drainingRefuelingSearching:buffer", sourceId);
    }

    private SpeedAndStatesResponse.Builder makeResponseBuilder(int blockNum, String instanceId) {
        SpeedAndStatesResponse.Builder builder = SpeedAndStatesResponse.newBuilder();

        builder.setMessageType(RESPONSE_MESSAGE_TYPE);
        builder.setInstanceId(instanceId);
        builder.setBlockNumber(blockNum);

        return builder;
    }

    private class VelocityData {

        private final int dateId;
        private final int value;

        public VelocityData(int dateId, int value) {
            this.dateId = dateId;
            this.value = value;
        }
    }
}
