/*
 * ru.omnicomm.pegasus.processing.ControlledParameterDataRequestServer
 * 
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * For internal use only.
 * 
 * Author: Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 * Version: 
 */
package ru.omnicomm.pegasus.processing;

import com.ascatel.asymbix.utils.uniparser.BytesField;
import com.ascatel.asymbix.utils.uniparser.DoubleField;
import com.ascatel.asymbix.utils.uniparser.Field;
import com.ascatel.asymbix.utils.uniparser.IntField;
import com.ascatel.asymbix.utils.uniparser.LongField;
import com.ascatel.asymbix.utils.uniparser.Message;
import com.ascatel.asymbix.utils.uniparser.ParserException;
import com.ascatel.asymbix.utils.uniparser.StringField;
import com.ascatel.asymbix.utils.uniparser.WireFormat;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser.GetCPDataResponse.ResponseEntry;
import ru.omnicomm.pegasus.messaging.base.request.BaseResponseParser.ResponseHeader;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

/**
 *
 * @author Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ControlledParameterDataRequestServerImplementation")
public class ControlledParameterDataRequestServer implements ServerImplementation {

    private static final int TAG_TIMESTAMP = 5;
    private static final int REQUEST_MESSAGE_TYPE = 423;
    private static final int RESPONSE_MESSAGE_TYPE = 424;
    private static final int ATOMIC_SENSOR_ENTITY_TYPE = 1750;
    private static final int ATOMIC_SENSOR_SOURCE_ID = 1753;
    private static final int ATOMIC_SENSOR_CP_ID = 1754;
    private static final int IN_BLOCK_MESSAGES_LIMIT = 100;
    private static final Logger logger = LoggerFactory.getLogger();
    private Server self;
    private JmsBridgeService jmsBridge;
    private BusinessLogicService blService;
    private ControlEntityHelper baseHelper;
    private DataStorageService dataStorage;

    @ServerConstructor
    public ControlledParameterDataRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService, DataStorageService dataStorage) {
        this.jmsBridge = jmsBridge;
        this.blService = blService;
        this.baseHelper = new ControlEntityHelper(blService);
        this.dataStorage = dataStorage;
    }

    @Override
    @SuppressWarnings("UseSpecificCatch")
    public void onMessage(Server source, MessageLite message) {
        BaseResponseParser.GetCPDataRequest request;
        try {
            request = BaseResponseParser.GetCPDataRequest.parseFrom(message.toByteArray());
        }
        catch (InvalidProtocolBufferException ex) {
            logger.error("Invalid message.", ex);
            return;
        }
        try {
            processMessage(request);
        }
        catch (Exception ex) {
            logger.info("Can't process data", ex);
        }
    }

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) throws RuntimeException {
                logger.error(null, signal.getException());
            }

            @Override
            public void visit(Init signal) throws RuntimeException {
                self = signal.getSelf();
                jmsBridge.registerServer(ControlledParameterDataRequestServer.class, self, REQUEST_MESSAGE_TYPE);
            }

            @Override
            public void visit(Terminate signal) throws RuntimeException {
                jmsBridge.unregisterServer(ControlledParameterDataRequestServer.class, self);
            }
        });
    }

    private void processMessage(BaseResponseParser.GetCPDataRequest request) throws RequestProcessingException, JmsBridgeServiceException, DataStorageServiceException, ParserException, InvalidProtocolBufferException {
        String instanceId = request.getInstanceId();
        long coId = request.getCoId();
        long cuId = request.getCuId();
        long cpId = request.getCpId();
        long mtId = request.getMtId();
        int blockId = request.getMessageBlock();
        int startDate = request.getStartDate();
        int endDate = request.getEndDate();
        Collection<Long> sourceIds = baseHelper.selectSources(cuId);
        Map<Long, Set<Long>> sensors = getAtomicSensors(sourceIds, cpId);
        if (sensors.size() != 1) {
            BaseResponseParser.GetCPDataResponse.Builder responseBuilder = makeResponseBuilder(0, instanceId);
            if (!sensors.isEmpty()) {
                responseBuilder.setErrorMessage("More than one Atomic Sensor found");
            }
            responseBuilder.setBlockState(BaseResponseParser.State.LAST);
            jmsBridge.send(responseBuilder.build());
            return;
        }
        long sourceId = sensors.values().iterator().next().iterator().next();
        Map<Long, ResponseEntry.Builder> entries = new TreeMap<>();
        Iterator<MessageLite> iterator = new MessageLiteIterator(blockId, (int) sourceId, (int) mtId, startDate, endDate, dataStorage);
        while (iterator.hasNext()) {
            MessageLite ml = iterator.next();
            Message message = Message.parse(ml.toByteArray());
            LongField tsField = (LongField) message.getField(TAG_TIMESTAMP);
            ResponseEntry.Builder reBuilder = entries.get(tsField.getValue());
            if (reBuilder == null) {
                reBuilder = ResponseEntry.newBuilder();
                reBuilder.setTimestamp(tsField.getValue());
                entries.put(tsField.getValue(), reBuilder);
            }
            for (Field<?> field : message.getFields()) {
                if (field.getTag() < 128) {
                    continue;
                }
                ResponseEntry.Values.Builder vBuilder = ResponseEntry.Values.newBuilder();
                vBuilder.setTag(field.getTag());
                switch (field.getJSType()) {
                    case WireFormat.JS_BYTES:
                        vBuilder.setBytesValue(ByteString.copyFrom(((BytesField) field).getValue()));
                        break;
                    case WireFormat.JS_DOUBLE:
                        vBuilder.setDoubleValue(((DoubleField) field).getValue());
                        break;
                    case WireFormat.JS_INT:
                        vBuilder.setIntValue(((IntField) field).getValue());
                        break;
                    case WireFormat.JS_LONG:
                        vBuilder.setLongValue(((LongField) field).getValue());
                        break;
                    case WireFormat.JS_STRING:
                        vBuilder.setStringValue(((StringField) field).getValue());
                        break;
                    default:
                        logger.warn("Unknown wiretype: " + field);
                        continue;
                }
                reBuilder.addValues(vBuilder);
            }
        }
        Set<Long> cpmts = baseHelper.selectControlledParameterMessageTypes(cpId);
        for (Long cpmt : cpmts) {
            iterator = new MessageLiteIterator(blockId, (int) sourceId, cpmt.intValue(), startDate, endDate, dataStorage);
            while (iterator.hasNext()) {
                MessageLite ml = iterator.next();
                BaseResponseParser.ResponseHeader responseHeader = ResponseHeader.parseFrom(ml.toByteArray());
                long timestamp = responseHeader.getTimestamp();
                long messageType = responseHeader.getMessageType();
                ResponseEntry.Builder reBuilder = entries.get(timestamp);
                if (reBuilder == null) {
                    reBuilder = ResponseEntry.newBuilder();
                    reBuilder.setTimestamp(timestamp);
                    entries.put(timestamp, reBuilder);
                }
                reBuilder.addMtId(messageType);
            }
        }
        int blockNum = 0, messageCounter = 0;
        BaseResponseParser.GetCPDataResponse.Builder responseBuilder = makeResponseBuilder(blockNum++, instanceId);
        for (ResponseEntry.Builder reBuilder : entries.values()) {
            responseBuilder.addEntries(reBuilder);
            if (++messageCounter % IN_BLOCK_MESSAGES_LIMIT == 0) {
                responseBuilder.setBlockState(BaseResponseParser.State.NORMAL);
                jmsBridge.send(responseBuilder.build());
                responseBuilder = makeResponseBuilder(blockNum++, instanceId);
            }
        }
        responseBuilder.setBlockState(BaseResponseParser.State.LAST);
        jmsBridge.send(responseBuilder.build());
    }

    private Map<Long, Set<Long>> getAtomicSensors(Collection<Long> sourceIds, long cpId) throws RequestProcessingException {
        Map<Long, Set<Long>> result = new HashMap<>();
        ParameterValue cpIdValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, cpId);
        QueryItem cpIdItem = MessageUtil.buildQueryItem(ATOMIC_SENSOR_CP_ID, QueryItem.Operation.EQ, cpIdValue);
        QueryItem sourceIdsItem = MessageUtil.buildIdListQueryItem(ATOMIC_SENSOR_SOURCE_ID, sourceIds);
        SelectRequest selectRequest = MessageUtil.buildSelectRequest(ATOMIC_SENSOR_ENTITY_TYPE, Arrays.asList(cpIdItem, sourceIdsItem));
        ResultSet resultSet;
        try {
            byte[] resultBytes = blService.selectByParams(selectRequest.toByteArray());
            resultSet = ResultSet.parseFrom(resultBytes);
        }
        catch (BusinessLogicServiceException | InvalidProtocolBufferException ex) {
            throw new RequestProcessingException("Cannot get Atomic Sensors", ex);
        }
        for (Parameter parameter : resultSet.getParametersList()) {
            if (parameter.getAttributeId().equals(String.valueOf(ATOMIC_SENSOR_SOURCE_ID))) {
                long sensorId = Long.parseLong(parameter.getEntityId());
                Set<Long> sources = result.get(sensorId);
                if (sources == null) {
                    sources = new HashSet<>();
                    result.put(sensorId, sources);
                }
                long sourceId = parameter.getValue().getLongValue();
                sources.add(sourceId);
            }
        }
        return result;
    }

    private BaseResponseParser.GetCPDataResponse.Builder makeResponseBuilder(int blockNum, String instanceId) {
        return BaseResponseParser.GetCPDataResponse.newBuilder().
                setMessageType(RESPONSE_MESSAGE_TYPE).
                setBlockNumber(blockNum).
                setInstanceId(instanceId);
    }
}
