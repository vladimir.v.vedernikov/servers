@echo off

set "DEST_PATH=../../java"

set "FILE_NAME=BaseServiceMessages.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/base"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

set "FILE_NAME=BaseResponseMessages.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/base"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

set "FILE_NAME=BaseRequestMessages.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/base"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"

set "FILE_NAME=SendRequestMessage.proto"
set "SRC_PATH=../../../../../../../../MessageStorage/src/main/resources/protobuf/common"
copy "%SRC_PATH%\%FILE_NAME%" ".\%FILE_NAME%"
call protoc.exe ".\%FILE_NAME%"  --java_out "%DEST_PATH%"
del ".\%FILE_NAME%"


echo "done"
