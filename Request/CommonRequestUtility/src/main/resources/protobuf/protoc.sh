#!/bin/sh

DEST_PATH=../../java

FILE_NAME=BaseResponseMessages.proto
SRC_PATH=../../../../../../../../../MessageStorage/src/main/resources/protobuf/base
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

FILE_NAME=BaseRequestMessages.proto
SRC_PATH=../../../../../../../../../MessageStorage/src/main/resources/protobuf/base
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

FILE_NAME=SendRequestMessage.proto
SRC_PATH=../../../../../../../../../MessageStorage/src/main/resources/protobuf/common
ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
rm ${FILE_NAME}

echo "done"
