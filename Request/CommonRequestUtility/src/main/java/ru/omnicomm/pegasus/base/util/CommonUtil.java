/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 19.06.12
 */
package ru.omnicomm.pegasus.base.util;

import ru.omnicomm.pegasus.base.RequestParameterException;
import ru.omnicomm.pegasus.messaging.base.request.MessageParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.MUIString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.UpdateRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;

import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class CommonUtil {

    public static final String LOCALE_PARAM = "locale";

    public static String getLocale(SendRequestParser.SendRequest request) {
        List<String> locales = getRequestFilterValues(LOCALE_PARAM, request);

        if (locales.isEmpty()) {
            throw new RequestParameterException("Filter parameter 'locale' is requared.");
        }

        return locales.get(0).toLowerCase();
    }

    public static <T> List<T> getRequestFilterValues(String parameterKey, SendRequestParser.SendRequest request) {
        if (parameterKey == null) {
            throw new NullPointerException("Parameter parameterKey key is null");
        }

        List<T> result = new ArrayList<T>();
        List<SendRequestParser.SendRequest.Parameter> filter = request.getFilterList();
        for (SendRequestParser.SendRequest.Parameter parameter : filter) {
            if (parameterKey.equals(parameter.getKey())) {
                SendRequestParser.SendRequest.Parameter.Type parameterType = parameter.getType();
                Object value;
                switch (parameterType) {
                    case STRING:
                        value = parameter.getStringValue();
                        break;
                    case BOOLEAN:
                        value = parameter.getBooleanValue();
                        break;
                    case DOUBLE:
                        value = parameter.getDoubleValue();
                        break;
                    case INT:
                        value = parameter.getIntValue();
                        break;
                    default:
                        throw new RequestParameterException("Unsupported request parameter type" + parameterType);
                }
                try {
                    result.add((T) value);
                } catch (ClassCastException e) {
                    throw new RequestParameterException("Wrong parameter type", e);
                }
            }
        }
        return result;
    }

    /**
     * @param parameterKey имя фильтра в запросе.
     * @param request      запрос.
     * @param <T>          тип ожидаемого значения фильтра.
     * @return первое значение фильтра с ключем {@code parameterKey} в запросе или {@code null} если фильтр отсутствует.
     */
    public static <T> T getRequestFilterValue(String parameterKey, SendRequestParser.SendRequest request) {
        if (parameterKey == null) {
            throw new NullPointerException("Parameter parameterKey key is null");
        }

        T result = null;
        List<SendRequestParser.SendRequest.Parameter> filter = request.getFilterList();
        for (SendRequestParser.SendRequest.Parameter parameter : filter) {
            if (parameterKey.equals(parameter.getKey())) {
                SendRequestParser.SendRequest.Parameter.Type parameterType = parameter.getType();
                Object value;
                switch (parameterType) {
                    case STRING:
                        value = parameter.getStringValue();
                        break;
                    case BOOLEAN:
                        value = parameter.getBooleanValue();
                        break;
                    case DOUBLE:
                        value = parameter.getDoubleValue();
                        break;
                    case INT:
                        value = parameter.getIntValue();
                        break;
                    default:
                        throw new RequestParameterException("Unsupported request parameter type" + parameterType);
                }
                try {
                    result = (T) value;
                    // Значение найдено - прекращаем поиск.
                    break;
                } catch (ClassCastException e) {
                    throw new RequestParameterException("Wrong parameter type", e);
                }
            }
        }
        return result;
    }

    /**
     * Строит {@link QueryItem} для ключей сущностей передаваемых в запросе. Выделен в отдельный метод т.к. в запросе
     * идентификаторы приходят в виде строк а не long'ов и нужно парсить их здесь.
     *
     * @param parameterKey имя параметра в запросе.
     * @param request      запрос.
     * @return построенный {@link QueryItem}.
     */
    public static QueryItem buildEntityIdQueryItem(String parameterKey, SendRequestParser.SendRequest request) {
        QueryItem queryItem = null;
        List<String> entityIds = CommonUtil.getRequestFilterValues(parameterKey, request);
        if (entityIds.size() > 0) {
            Operation op;
            ParameterValue parameterValue;
            try {
                if (entityIds.size() > 1) {
                    op = Operation.IN;
                    List<ParameterValue> values = new ArrayList<ParameterValue>();
                    for (String entityIdStr : entityIds) {
                        Long entityId = Long.parseLong(entityIdStr);
                        values.add(MessageUtil.buildParameterValue(ParameterValueType.LONG, entityId));
                    }
                    parameterValue = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, MessageUtil.buildValueArray(values));
                } else {
                    op = Operation.EQ;
                    Long entityId = Long.parseLong(entityIds.get(0));
                    parameterValue = MessageUtil.buildParameterValue(ParameterValueType.LONG, entityId);
                }
            } catch (NumberFormatException e) {
                final String errMsg = new StringBuilder().append("Value of request parameter ")
                        .append(parameterKey).append(" is not Long").toString();
                throw new RequestParameterException(errMsg, e);
            }

            queryItem = MessageUtil.buildQueryItem(Long.parseLong(BusinessLogicService.ENTITY_ID_ATTRIBUTE), op, parameterValue);
        }

        return queryItem;
    }

    public static QueryItem buildQueryItem(long attributeId, Operation op, String parameterKey, SendRequestParser.SendRequest request) {
        List<SendRequestParser.SendRequest.Parameter> filter = request.getFilterList();
        List<ParameterValue> params = new ArrayList<ParameterValue>();
        for (SendRequestParser.SendRequest.Parameter parameter : filter) {
            if (parameterKey.equals(parameter.getKey())) {
                params.add(buildParameterValue(parameter));
            }
        }

        if (params.isEmpty()) {
            return null;
        }

        ParameterValue value;
        if (params.size() > 1) {
            value = MessageUtil.buildParameterValue(ParameterValueType.ARRAY, MessageUtil.buildValueArray(params));
        } else {
            value = params.get(0);
        }

        return MessageUtil.buildQueryItem(attributeId, op, value);
    }

    public static ParameterValue buildParameterValue(SendRequestParser.SendRequest.Parameter parameter) {
        SendRequestParser.SendRequest.Parameter.Type parameterType = parameter.getType();
        switch (parameterType) {
            case STRING:
                return MessageUtil.buildParameterValue(ParameterValueType.STRING, parameter.getStringValue());
            case BOOLEAN:
                return MessageUtil.buildParameterValue(ParameterValueType.BOOLEAN, parameter.getBooleanValue());
            case DOUBLE:
                return MessageUtil.buildParameterValue(ParameterValueType.NUMBER, parameter.getDoubleValue());
            case INT:
                return MessageUtil.buildParameterValue(ParameterValueType.INTEGER, parameter.getIntValue());
            default:
                throw new RequestParameterException("Unsupported request parameter type" + parameterType);
        }
    }

    /**
     * Группирует параметры по их entityId. Возвращает карту где ключ - идентификатор сущности, значение -
     * набор параметров сущности.
     *
     * @param parameters список параметров.
     * @return карта сгруппированных по сущностям параметров.
     */
    public static Map<Long, Map<Long, List<Parameter>>> groupParameters(List<Parameter> parameters) {
        Map<Long, Map<Long, List<Parameter>>> result = new HashMap<Long, Map<Long, List<Parameter>>>();

        for (Parameter parameter : parameters) {
            long entityId = Long.parseLong(parameter.getEntityId());
            long attributeId = Long.parseLong(parameter.getAttributeId());

            // Коллекция отношений идентификаторов атрибутов к списку самих атрибутов.
            // Может быть несколько атрибутов с одинаковым attrId т.к. в ответе на запрос могут передаваться
            // исторические данные.
            Map<Long, List<Parameter>> entity = result.get(entityId);
            if (entity == null) {
                entity = new HashMap<Long, List<Parameter>>();
                result.put(entityId, entity);
            }

            // Список параметров.
            List<Parameter> sameAttrIdParams = entity.get(attributeId);
            if (sameAttrIdParams == null) {
                sameAttrIdParams = new ArrayList<Parameter>();
                entity.put(attributeId, sameAttrIdParams);
            }

            sameAttrIdParams.add(parameter);
        }

        return result;
    }

    public static UpdateRequest buildUpdateRequest(long entityType, long entityId, MessageParser.Parameter param) {
        long parameterId = getParameterId(param);

        ParameterValue parameterValue = buildParameterValue(param);
        return MessageUtil.buildUpdateRequest(entityType, entityId, parameterId, parameterValue);
    }

    private static ParameterValue buildParameterValue(MessageParser.Parameter param) {
        MessageParser.Parameter.Type type = param.getType();
        switch (type) {
            case BOOLEAN:
                return MessageUtil.buildParameterValue(ParameterValueType.BOOLEAN, param.getBooleanValue());
            case DOUBLE:
                return MessageUtil.buildParameterValue(ParameterValueType.LONG, new Double(param.getDoubleValue()).longValue());
            case INT:
                return MessageUtil.buildParameterValue(ParameterValueType.INTEGER, param.getIntValue());
            case STRING:
                return MessageUtil.buildParameterValue(ParameterValueType.STRING, param.getStringValue());
            case MUISTRING:
                MUIString value = parseMUIString(param.getMuistringValueList());
                return MessageUtil.buildParameterValue(ParameterValueType.MUISTRING, value);
            case OBJECT:
                ObjectLink objectLink = parseObjectLink(param.getObjectValueList());
                return MessageUtil.buildParameterValue(ParameterValueType.OBJECT_LINK, objectLink);
            case FILE:
                return MessageUtil.buildParameterValue(ParameterValueType.FILE, param.getFileValue());
            default:
                throw new RequestParameterException("Unknown parameter type");
        }
    }

    private static MUIString parseMUIString(List<MessageParser.MUIstring> muistringValueList) {
        Map<String, String> muiValues = new HashMap<String, String>();
        for (MessageParser.MUIstring muiValue : muistringValueList) {
            muiValues.put(muiValue.getLangID(), muiValue.getValue());
        }

        return MessageUtil.buildMUIString(muiValues);
    }

    private static ObjectLink parseObjectLink(List<String> objectValueList) {
        final List<Long> links = new ArrayList<Long>(objectValueList.size());
        for (String  entityIdStr : objectValueList) {
            try {
                links.add(Long.parseLong(entityIdStr));
            } catch(NumberFormatException e) {
                throw new RequestParameterException("Invalid entity object link value=" + entityIdStr, e);
            }
        }

        return MessageUtil.buildObjectLink(links);
    }


    private static long getParameterId(MessageParser.Parameter param) {
        String strValue = param.getParameterId();
        if (strValue == null) {
            throw new RequestParameterException("Parameter identifier isn't defined");
        }
        try {
            return Long.parseLong(strValue);
        } catch (NumberFormatException e) {
            throw new RequestParameterException("Invalid parameter identifier value=" + strValue, e);
        }
    }

}
