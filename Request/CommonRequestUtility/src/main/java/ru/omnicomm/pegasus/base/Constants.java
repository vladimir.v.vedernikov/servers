/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 26.06.12
 */
package ru.omnicomm.pegasus.base;

import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class Constants {

    public static final String TOTAL_MESSAGES_LIMIT_PROPERTY = "total.messages.limit";
    public static final String IN_BLOCK_MESSAGES_LIMIT_PROPERTY = "block.message.limit";

    public static final int DEFAULT_TOTAL_MESSAGES_LIMIT = 1000; //Всего сообщений
    public static final int DEFAULT_IN_BLOCK_MESSAGES_LIMIT = 100; //Количество сообщений в блоке

    public static final int TOTAL_MESSAGES_LIMIT; //Всего сообщений
    public static final int IN_BLOCK_MESSAGES_LIMIT; //Количество сообщений в блоке

    static {
        Properties pr = new Properties();
        FileInputStream fs = null;
        try {
            fs = new FileInputStream("config/interaction.properties");
        } catch (FileNotFoundException e) {
            try {
                fs = new FileInputStream("/config/interaction.properties");
            } catch (FileNotFoundException e1) {
                // Для того, чтобы работало из ide
                try {
                    fs = new FileInputStream("src/main/resources/config/interaction.properties");
                } catch (FileNotFoundException e2) {
                    LoggerFactory.getLogger().log(Level.INFO, "Can''t find blssettings.properties file", e1);
                }
            }
        }

        if (fs != null) {
            try {
                pr.load(fs);
            } catch (IOException e) {
                LoggerFactory.getLogger().log(Level.INFO, "Can''t load properties", e);
            }
        }

        TOTAL_MESSAGES_LIMIT = parseInt(pr.getProperty(TOTAL_MESSAGES_LIMIT_PROPERTY), DEFAULT_TOTAL_MESSAGES_LIMIT);
        LoggerFactory.getLogger().log(Level.INFO, "Total messages per query limited to  " + TOTAL_MESSAGES_LIMIT);
        IN_BLOCK_MESSAGES_LIMIT = parseInt(pr.getProperty(IN_BLOCK_MESSAGES_LIMIT_PROPERTY), DEFAULT_IN_BLOCK_MESSAGES_LIMIT);
        LoggerFactory.getLogger().log(Level.INFO, "Number of messages in block limited to " + IN_BLOCK_MESSAGES_LIMIT);

        if (fs != null) {
            try {
                fs.close();
            } catch (IOException e) {
                LoggerFactory.getLogger().log(Level.INFO, null, e);
            }
        }
    }

    public static int parseInt(String value, int defaultValue) {
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                LoggerFactory.getLogger().log(Level.INFO, "Can''t parse int value " + value, e);
                return defaultValue;
            }
        }
    }


}
