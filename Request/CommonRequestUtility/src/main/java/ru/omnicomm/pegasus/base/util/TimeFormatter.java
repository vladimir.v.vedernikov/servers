/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 11.09.12
 */
package ru.omnicomm.pegasus.base.util;

import ru.omnicomm.pegasus.processingPlatform.TimeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class TimeFormatter {
    private static final TimeFormatter INSTANCE = new TimeFormatter();

    private DateFormat dateFormatter;//yyyy.mm.dd HH:mm:ss
    private Pattern datePattern;

    private TimeFormatter() {
        dateFormatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");//yyyy.mm.dd HH:mm:ss
        dateFormatter.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("UTC")));
        dateFormatter.setLenient(false);

        datePattern = Pattern.compile("[\\d]{4}.[\\d]{2}.[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}");
    }

    public static TimeFormatter getInstance() {
        return INSTANCE;
    }

    public long convertMesasgeTime(String stringTime) {
        try {
            if (!datePattern.matcher(stringTime).matches()) {
                throw new IllegalArgumentException("Invalid message time values format. Usage: yyyy.MM.dd HH:mm:ss");
            }
            Date date = dateFormatter.parse(stringTime);
            return TimeConverter.convertTimeStamp(date.getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid message time values format. Usage: yyyy.MM.dd HH:mm:ss", e);
        } catch (IllegalArgumentException e) {
            final String error = new StringBuilder().append("Invalid message time value ").append(stringTime).toString();
            throw new IllegalArgumentException(error, e);
        }
    }

    public String convertMesasgeTime(int seconds) {
        final long milsecs = TimeConverter.convertTimeStamp(seconds);
        return dateFormatter.format(new Date(milsecs));
    }

}
