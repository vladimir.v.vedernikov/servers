/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 19.06.12
 */
package ru.omnicomm.pegasus.base;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class RequestParameterException extends RuntimeException {

    public RequestParameterException() {
    }

    public RequestParameterException(String message) {
        super(message);
    }

    public RequestParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestParameterException(Throwable cause) {
        super(cause);
    }

}