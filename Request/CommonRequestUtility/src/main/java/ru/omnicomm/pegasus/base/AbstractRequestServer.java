/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 02.07.12
 */
package ru.omnicomm.pegasus.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.base.util.CommonUtil;
import ru.omnicomm.pegasus.messaging.common.SendRequestParser;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public abstract class AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

//    public static final int REQUEST_MESSAGE_TYPE = 301;

    private JmsBridgeService jmsBridge;

    private BusinessLogicService blService;

    private Server self;

    private Handler handler;

    private String currentLocale;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    public AbstractRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.jmsBridge = jmsBridge;
        this.blService = blService;
    }

    //    @Override
    public void onMessage(Server source, MessageLite message) {
        SendRequestParser.SendRequest request;
        try {
            request = SendRequestParser.SendRequest.parseFrom(message.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.ERROR, "Invalid message.", e);
            return;
        }

//        if (getResponseMessageType() == request.getRequestedMessageType()) {
        processMessage(request);
//        }

    }

    //    @Override
    public void onSignal(Signal signal) {
        final Class serverType = getClass();

        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();
                handler = signal.getHandler();

                jmsBridge.registerServer(serverType, self, getRequestMessageType());
            }

            @Override
            public void visit(Terminate signal) {
                jmsBridge.unregisterServer(serverType, self);
            }
        });
    }

    public String getCurrentLocale() {
        return currentLocale;
    }

    public void setCurrentLocale(SendRequestParser.SendRequest request) {
        currentLocale = getLocale(request);
    }

    private String getLocale(SendRequestParser.SendRequest request) {
//        final String localeStr;
        try {
            return CommonUtil.getLocale(request);
        } catch (RequestParameterException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            return "en";
        }

//        try {
//            return Locale.valueOf(localeStr.toUpperCase());
//        } catch (IllegalArgumentException e) {
//            throw new RequestParameterException("Invalid locale value=" + localeStr, e);
//        }
    }

    protected void processMessage(SendRequestParser.SendRequest request) {
        try {
            logAcceptedMessage(request);
            try {
                setCurrentLocale(request);
                final BLSMessageParser.SelectRequest queryRequest = buildQueryRequest(request);
                byte[] responseBytes = blService.selectByParams(queryRequest.toByteArray());
                BLSMessageParser.ResultSet response = BLSMessageParser.ResultSet.parseFrom(responseBytes);

                List<List<ResponseEntry>> responseEntities = buildResponseEntries(response.getParametersList());
                List<MessageLite> responseMessages = buildResponseMessages(request, responseEntities);

                if (responseMessages.isEmpty()) {
                    jmsBridge.send(getEmptyResponse(request));
                } else {
                    for (MessageLite responseMessage : responseMessages) {
                        jmsBridge.send(responseMessage);
                    }
                }
                logProcessedMessage(request);
            } catch (BusinessLogicServiceException e) {
                logErrorMessage(request, e.getMessage(), e);
                jmsBridge.send(getDefectiveResponse(request));
            } catch (RequestParameterException e) {
                logErrorMessage(request, e.getMessage(), e);
                jmsBridge.send(getDefectiveResponse(request));
            } catch (RequestProcessingException e) {
                logErrorMessage(request, e.getMessage(), e);
                jmsBridge.send(getDefectiveResponse(request));
            } catch (InvalidProtocolBufferException e) {
                logErrorMessage(request, e.getMessage(), e);
                jmsBridge.send(getDefectiveResponse(request));
            } catch (Exception e) {
                logErrorMessage(request, e.getMessage(), e);
                jmsBridge.send(getDefectiveResponse(request));
            }
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    private List<List<ResponseEntry>> buildResponseEntries(List<Parameter> parameters) {
        List<List<ResponseEntry>> result = new ArrayList<List<ResponseEntry>>();

        int responseCounter = 0;
        List<ResponseEntry> rows = new ArrayList<ResponseEntry>();

        Map<Long, Map<Long, List<Parameter>>> entities = CommonUtil.groupParameters(parameters);
        Set<Long> entityIds = entities.keySet();
        for (Long entityId : entityIds) {
            responseCounter++;

            if (responseCounter > Constants.TOTAL_MESSAGES_LIMIT) {
                // Превышен лимит сообщений.
                break;
            }

            Map<Long, List<Parameter>> attributes = entities.get(entityId);
            rows.add(new ResponseEntry(entityId, attributes));

            if (rows.size() >= Constants.IN_BLOCK_MESSAGES_LIMIT) {
                result.add(rows);
                rows = new ArrayList<ResponseEntry>();
            }
        }

        if (!rows.isEmpty()) {
            result.add(rows);
        }

        return result;
    }

    @SuppressWarnings("Unchecked")
    protected <T> List<T> getParameterValues(Long attrId, ParameterValueType type, Map<Long, List<Parameter>> attributes) {
        List<Parameter> parameters = attributes.get(attrId);
        if (parameters == null) {
            throw new RequestProcessingException("Cannot find entity parameter " + attrId);
        }

        List<T> result = new ArrayList<T>();
        for (Parameter parameter : parameters) {
            result.add((T) this.extractValue(type, parameter));
        }

        return result;
    }

    protected <T> T getParameterValue(Long attrId, ParameterValueType type, Map<Long, List<Parameter>> attributes) {
        List<Parameter> parameters = attributes.get(attrId);
        if (parameters == null) {
            throw new RequestProcessingException("Cannot find entity parameter " + attrId);
        }

        if (parameters.isEmpty()) {
            throw new RequestProcessingException("Cannot find entity parameter " + attrId);
        }

        Parameter parameter = parameters.get(0);
        return (T) extractValue(type, parameter);
    }

    @SuppressWarnings("unchecked")
    protected Object extractValue(ParameterValueType type, Parameter parameter) {
        BLSMessageParser.ParameterValue value = parameter.getValue();
        switch (type) {
            case STRING:
                return value.getStringValue();
            case NUMBER:
                return value.getDoubleValue();
            case DATETIME:
                return value.getDatetimeValue();
            case MUISTRING:
                String langId = getCurrentLocale();
                String muiStr = null;
                for (BLSMessageParser.MUIString.MUIValue muiValue : value.getMuistringValue().getStrValuesList()) {
                    if (langId.toLowerCase().equals(muiValue.getLangID())) {
                        muiStr = muiValue.getValue();
                        break;
                    }
                }
                return muiStr;
            case OBJECT_LINK:
                return value.getObjectLinkValue();
            case FILE:
                return value.getFileValue();
            case INTEGER:
                return value.getIntValue();
            case LONG:
                return value.getLongValue();
            case BOOLEAN:
                return value.getBooleanValue();
            case ARRAY:
                return value.getValueArray().getValuesList();
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }
    }

    public JmsBridgeService getJmsBridge() {
        return jmsBridge;
    }

    public BusinessLogicService getBlService() {
        return blService;
    }

    protected void logAcceptedMessage(SendRequestParser.SendRequest requestMsg) {
        StringBuilder sb = new StringBuilder();
        sb.append("Request ACCEPTED. ").append(requestMsg.getInstanceId())
                .append("/").append(requestMsg.getRequestedMessageType())
                .append("/").append(requestMsg.getMessageType());
        LOGGER.log(Level.INFO, sb.toString());
    }

    protected void logProcessedMessage(SendRequestParser.SendRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("Request is PROCESSED. ").append(request.getInstanceId())
                .append("/").append(request.getRequestedMessageType())
                .append("/").append(request.getMessageType());
        LOGGER.log(Level.INFO, sb.toString());
    }

    protected void logErrorMessage(SendRequestParser.SendRequest request, String cause, Exception exception) {
        StringBuilder sb = new StringBuilder();
        sb.append("Request is FAILED. ").append(request.getInstanceId())
                .append("/").append(request.getRequestedMessageType())
                .append("/").append(request.getMessageType())
                .append(". Because of ").append(cause);
        LOGGER.log(Level.WARN, sb.toString(), exception);
    }

    protected abstract BLSMessageParser.SelectRequest buildQueryRequest(SendRequestParser.SendRequest request);

    protected abstract int getResponseMessageType();

    protected abstract int getRequestMessageType();

    protected abstract List<MessageLite> buildResponseMessages(SendRequestParser.SendRequest request, List<List<ResponseEntry>> responseEntities);

    protected abstract MessageLite getDefectiveResponse(SendRequestParser.SendRequest request);

    protected abstract MessageLite getEmptyResponse(SendRequestParser.SendRequest request);

    public static class ResponseEntry {
        private long entityId;

        private Map<Long, List<Parameter>> attributes;

        public ResponseEntry(long entityId, Map<Long, List<Parameter>> attributes) {
            this.entityId = entityId;
            this.attributes = attributes;
        }

        public long getEntityId() {
            return entityId;
        }

        public void setEntityId(long entityId) {
            this.entityId = entityId;
        }

        public Map<Long, List<Parameter>> getAttributes() {
            return attributes;
        }

        public void setAttributes(Map<Long, List<Parameter>> attributes) {
            this.attributes = attributes;
        }
    }

}
