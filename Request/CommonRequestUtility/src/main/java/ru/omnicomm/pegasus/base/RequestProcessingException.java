/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 27.06.12
 */
package ru.omnicomm.pegasus.base;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class RequestProcessingException extends RuntimeException {

    public RequestProcessingException() {
    }

    public RequestProcessingException(String message) {
        super(message);
    }

    public RequestProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestProcessingException(Throwable cause) {
        super(cause);
    }

}
