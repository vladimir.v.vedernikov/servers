/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omnicomm.pegasus.processingPlatform.processing;

import ru.omnicomm.pegasus.messaging.data.movement.MovementAggregatedDataParser.AGRMileageV;
import ru.omnicomm.pegasus.messaging.data.movement.MovementAggregatedDataParser.AGRVelocityAvr;
import ru.omnicomm.pegasus.messaging.data.movement.MovementAggregatedDataParser.AGRVelocityMax;


/**
 *
 * @author alexander
 */
public enum AggregationType {
    
    DEFAULT(AGRMileageV.getDefaultInstance().getMessageType(),
            AGRVelocityAvr.getDefaultInstance().getMessageType(),
            AGRVelocityMax.getDefaultInstance().getMessageType()),
    DAY(93,94,95),
    WEEK(96,97,98),
    MONTH(99,100,101);

    private final int agrMileageVMessageType;
    private final int agrVelocityAvrMessageType;
    private final int agrVelocityMaxMessageType;

    private AggregationType(int agrMileageVMessageType, int agrVelocityAvrMessageType, int agrVelocityMaxMessageType) {
        this.agrMileageVMessageType = agrMileageVMessageType;
        this.agrVelocityAvrMessageType = agrVelocityAvrMessageType;
        this.agrVelocityMaxMessageType = agrVelocityMaxMessageType;
    }

    public int getAgrMileageVMessageType() {
        return agrMileageVMessageType;
    }

    public int getAgrVelocityAvrMessageType() {
        return agrVelocityAvrMessageType;
    }

    public int getAgrVelocityMaxMessageType() {
        return agrVelocityMaxMessageType;
    }


    
}
