/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.omnicomm.pegasus.processingPlatform.processing;

import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ru.omnicomm.pegasus.messaging.data.movement.MovementAggregatedDataParser.AGRMileageV;
import ru.omnicomm.pegasus.messaging.data.movement.MovementAggregatedDataParser.AGRVelocityAvr;
import ru.omnicomm.pegasus.messaging.data.movement.MovementAggregatedDataParser.AGRVelocityMax;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementAggregatingSettingsParser;
import ru.omnicomm.pegasus.messaging.settings.movement.MovementAggregatingSettingsParser.MovementAggregatingBuffer.BasicData;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.router.Router;


/**
 * Буфер алгоритма агрегации движения
 * @author alexander
 */
public class MovementAggregatingBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger();
    private final AggregationType aggregationType;

    private final int sourceId;
    private final int idleMax;
    private final int aggrInterval;
    private final List<BasicData> buffer;
  //  private int t_0;

    /**
     * Конструктор
     * @param settings настройки алгоритма агрегации движения для заданного источника данных (id источника данных находится в самих настройках)
     * @throws IllegalArgumentException в случае некорретных настроек
     */
    public MovementAggregatingBuffer(AggregationType aggregationType, MovementAggregatingSettingsParser.MovementAggregatingSettings  settings) throws IllegalArgumentException {
        this.aggregationType = aggregationType;
        this.sourceId = settings.getSourceId();
        this.idleMax = settings.getIdleMax();
        this.aggrInterval = settings.getAggrInterval();
        this.buffer = new ArrayList<BasicData>();

        if(this.aggrInterval<=0){
            throw new IllegalArgumentException("aggrInterval should be a positive value");
        }
    }

    /**
     * Конструктор
     * @param settings настройки алгоритма агрегации движения для заданного источника данных (id источника данных находится в самих настройках)
     * @param buffer накопленный ранее буфер
     * @throws IllegalArgumentException
     */
    public MovementAggregatingBuffer(AggregationType aggregationType, MovementAggregatingSettingsParser.MovementAggregatingSettings settings,
            MovementAggregatingSettingsParser.MovementAggregatingBuffer buffer) throws IllegalArgumentException {

        this(aggregationType, settings);
        if(settings.getIdleMax() == buffer.getIdleMax() &&
           settings.getAggrInterval() == buffer.getAggrInterval()){
           
            this.buffer.addAll(buffer.getBufferList());
       //     this.t_0 = buffer.getT0();
        }
        
    }

    /**
     * Возвращает представление объекта в формате protobuf
     * @return представление объекта в формате protobuf
     */
    public MessageLite getMessageLite(){

        return MovementAggregatingSettingsParser.MovementAggregatingBuffer.newBuilder()
                .setSourceId(this.sourceId)
                .setMessageType(MovementAggregatingSettingsParser.MovementAggregatingBuffer.getDefaultInstance().getMessageType())
                .setMessageTime(0)
                .setIdleMax(this.idleMax)
                .setAggrInterval(this.aggrInterval)
//                .setT0(this.t_0)
                .addAllBuffer(this.buffer)
                .build();

    }

    /**
     * Выполняет обработку входных данных согласно алгоритму и 
     * отправляет результаты на сервисы платформы обработки
     *
     * @param router ссылка на маршрутизатор сообщений
     * @param jmsBridgeService ссылка на сервис jms bridge
     * @param dataStorageService ссылка на сервис хранения данных
     * @param t время
     * @param v скорость
     */
    public void processData(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, int t, int v) {
        
        //LOGGER.info("Processing data: t="+t+", v="+v);
               
        //Добавляем сообщение в буфер
        this.buffer.add(BasicData.newBuilder().setDataId(t).setValue(v).build());
        //в случае непоследовательных данных - сортируем буфер
        if(this.buffer.size()>1 && (this.buffer.get(this.buffer.size()-1).getDataId() <  this.buffer.get(this.buffer.size()-2).getDataId())){
            Collections.sort(buffer, new Comparator<BasicData>() {
                @Override
                public int compare(BasicData obj1, BasicData obj2) {
                    return ((Integer) obj1.getDataId()).compareTo(obj2.getDataId());
                }
            });
        }
        if(this.buffer.size()<3){
            return;
        }
        
        int delta_t = this.buffer.get(this.buffer.size()-1).getDataId() - this.buffer.get(1).getDataId();
        if(delta_t>=this.aggrInterval){
            
            int m = 0;
            int v_max = 0;
            
            for (int i = 1; i < this.buffer.size(); i++) {
                int dt = this.buffer.get(i).getDataId() - this.buffer.get(i-1).getDataId();
                if(dt<this.idleMax){
                    m+= this.buffer.get(i).getValue()*dt;
                    if(this.buffer.get(i).getValue()>v_max){
                        v_max=this.buffer.get(i).getValue();
                    }
                }
            }
            int dt = this.buffer.get(this.buffer.size()-1).getDataId()-this.buffer.get(0).getDataId();
            int v_avr = 0;//m/delta_t;
            if(dt>0){
                v_avr = m/dt;
            }
            int messageTime = this.buffer.get(this.buffer.size()-1).getDataId();
            messageTime = messageTime - messageTime%this.aggrInterval;
            this.sendToServices(router, jmsBridgeService, dataStorageService, this.getAGRMileageVMessage(sourceId, messageTime, m));
            this.sendToServices(router, jmsBridgeService, dataStorageService, this.getAGRVelocityAvrMessage(sourceId, messageTime, v_avr));
            this.sendToServices(router, jmsBridgeService, dataStorageService, this.getAGRVelocityMaxMessage(sourceId, messageTime, v_max));
            
            while(this.buffer.size()>2){
                this.buffer.remove(0);
            }
        }

    }
    
    

    private int mileageCalculation(int t_0, int t_1){
        int m = 0;
        
        //Находим в буфере индексы сообщений, соответствующих началу и концу периода
        int startIndex = -1, endIndex = -1;
        
        //Ищем индекс начала периода
        for (int i = 0; i < this.buffer.size(); i++) {
            if(this.buffer.get(i).getDataId()>t_0){
                break;
            } else {
                startIndex=i;
            }
        }
        
        //Ищем индекс конца периода
        for (int i = this.buffer.size()-1; i >= 0; i--) {
            if(this.buffer.get(i).getDataId()<t_1){
                break;
            } else {
                endIndex=i;
            }
        }

        
        if(startIndex<0 || endIndex<0){
            return m;
        }

        if(endIndex<startIndex){
            LOGGER.warn("Mileage calculation error; buffer: "+this.getMessageLite());
            return m;
        }

        //Считаем пробег
        int t_bgn = this.buffer.get(startIndex).getDataId();
        int t_end = this.buffer.get(endIndex).getDataId();
        for (int i = startIndex; i <= endIndex; i++) {

            int v = this.buffer.get(i).getValue();
            int tau = 0;
            int tau_max = this.idleMax;
            if(i>startIndex){

                int t_i = this.buffer.get(i).getDataId();
                int t_i_minus_1 = this.buffer.get(i-1).getDataId();

                if(t_i - t_i_minus_1 < tau_max) {
                    tau = min(t_i,t_end)-max(t_i_minus_1,t_bgn);
                }

            } 
            m+=(v*tau);

        }

        return m;

    }

    private int maxSpeedCalculation(int t_0, int t_1){
        int v_max = 0;

        //Находим в буфере индексы сообщений, соответствующих началу и концу периода
        int startIndex = -1, endIndex = -1;
        
        //Ищем индекс начала периода
        for (int i = 0; i < this.buffer.size(); i++) {
            if(this.buffer.get(i).getDataId()>t_0){
                break;
            } else {
                startIndex=i;
            }
        }
        
        //Ищем индекс конца периода
        for (int i = this.buffer.size()-1; i >= 0; i--) {
            if(this.buffer.get(i).getDataId()<t_1){
                break;
            } else {
                endIndex=i;
            }
        }

        if(startIndex<0 || endIndex<0){
            return v_max;
        }

        if(endIndex<startIndex){
            LOGGER.warn("Max speed calculation error; buffer: "+this.getMessageLite());
            return v_max;
        }
        
        for (int i = startIndex; i < endIndex; i++) {
            int tau = 0;
            int tau_max = this.idleMax;
            if(i>startIndex){

                int t_i = this.buffer.get(i).getDataId();
                int t_i_minus_1 = this.buffer.get(i-1).getDataId();

                if(t_i - t_i_minus_1 < tau_max) {
                    tau = 1;
                }

            }

            int v = this.buffer.get(i).getValue() * tau;
            if(v>v_max){
                v_max = v;
            }
        }



        return v_max;
    }

    private void cleanupBuffer(int t_1){

        //7)	Сервер находит в буфере сообщение с минимальным timestamp, большим T1.
        int t_next = -1;
        for(BasicData data : this.buffer){
            int t = data.getDataId();
            if(t > t_1){
                t_next = t;
                break;
            } 
        }
        //8)	Сервер удаляет из буфера все сообщения, имеющие Timestamp меньший, чем timestamp сообщения, найденного на предыдущем шаге.
        while(this.buffer.size()>0){
            int t = this.buffer.get(0).getDataId();
            if(t < t_next){
                this.buffer.remove(0);
            } else {
                break;
            }
        }


    }

    private int min(int a, int b){
        return a < b ? a : b;
    }

    private int max(int a, int b){
        return a > b ? a : b;
    }

    private void sendToServices(Router router, JmsBridgeService jmsBridgeService, DataStorageService dataStorageService, MessageLite message){
        router.send(message);
        try{
            jmsBridgeService.send(message);
        } catch (JmsBridgeServiceException ex) {
            LOGGER.info("Can't send message to the jms bridge",ex);
        }
        try{
            dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.info("Can't send message to the data storage",ex);
        }

    }

    private MessageLite getAGRMileageVMessage(int sourceId, int messageTime, long value) {
        return AGRMileageV.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(this.aggregationType.getAgrMileageVMessageType())
                .setMessageTime(messageTime)
                .setValue(value)
                .build();
    }

    private MessageLite getAGRVelocityAvrMessage(int sourceId, int messageTime, int value) {
        return AGRVelocityAvr.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(this.aggregationType.getAgrVelocityAvrMessageType())
                .setMessageTime(messageTime)
                .setValue(value)
                .build();
    }

    private MessageLite getAGRVelocityMaxMessage(int sourceId, int messageTime, int value) {
        return AGRVelocityMax.newBuilder()
                .setSourceId(sourceId)
                .setMessageType(this.aggregationType.getAgrVelocityMaxMessageType())
                .setMessageTime(messageTime)
                .setValue(value)
                .build();
    }








}
