#!/bin/sh

DEST_PATH=../java

########################################
# Create parsers for required messages #
########################################

# SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/base/
# for FILE_NAME in BasePlatformMessages.proto  BaseProtocolMessages.proto  BaseRequestMessages.proto  BaseResponseMessages.proto  BaseServiceMessages.proto; do
#     ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
#     protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
#     rm ${FILE_NAME}
# done

SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/online/
for FILE_NAME in FuelData.proto LocationData.proto AnalogValue.proto BinaryStates.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done

echo "done"
