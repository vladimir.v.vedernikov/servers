/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.handler;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author alexander
 */
public enum TeltonikaDataItemType {
   
    EVENT_OID("EventOID",-1),
    PRIORITY("Priority",-1),
    LONGITUDE("Longitude",-1),
    LATITUDE("Latitude",-1),
    ALTITUDE("Altitude",-1),
    AZIMUTH("Azimuth",-1),
    SUTELLITES_NUMBER("SutellitesNumber",-1),
    GPS_VALID("GPSValid",-1),
    SPEED_GPS("SpeedGPS",-1),
    ANGLE("Angle",-1),
    DIGITAL_INPUT_STATUS_1("DigitalInputStatus1",1),
    DIGITAL_INPUT_STATUS_2("DigitalInputStatus2",2),
    SPEEDOMETER("Speedometer",24),
    EXTERNAL_POWER_VOLTAGE("ExternalPowerVoltage",66),
    BATTERY_VOLTAGE("BatteryVoltage",67),
    BATTERY_CURRENT("BatteryCurrent",68),
    GPS_POWER("GPSPower",69),
    PCB_TEMPERATURE("PCBTemperature",70),
    GEOZONE1("Geozone1",155),
    GEOZONE2("Geozone2",156),
    GEOZONE3("Geozone3",157),
    GEOZONE4("Geozone4",158),
    GEOZONE5("Geozone5",159),
    VIRTUAL_ODOMETER("VirtualOdometer",199),
    MOVEMENT("Movement",240),
    DIGITAL_OUTPUT_STATE_1("DigitalOutputState1",179),
    DIGITAL_OUTPUT_STATE_2("DigitalOutputState2",180),
    PDOP("PDOP",181),
    HDOP("HDOP",182),
    CURRENT_OPERATOR_CODE("CurrentOperatorCode",241);
    
    
    private final String stringId;
    private final int ioId; //IOID - см. док-цию
    
    private static final Map<String, TeltonikaDataItemType> stringIdMap = new HashMap<>();
    private static final Map<Integer, TeltonikaDataItemType> ioIdMap = new HashMap<>();
    
    private TeltonikaDataItemType(String id, int ioId) {
        this.stringId = id;
        this.ioId = ioId;
    }

    static {
        for (TeltonikaDataItemType type : EnumSet.allOf(TeltonikaDataItemType.class)) {
            stringIdMap.put(type.getStringId(), type);
            if(type.getIoId()>=0){
                ioIdMap.put(type.getIoId(), type);
            }
        }
    }
    

    public static TeltonikaDataItemType lookupByStringId(String id) {
        return stringIdMap.get(id);
    }
    
    public static TeltonikaDataItemType lookupByIoId(int id) {
        return ioIdMap.get(id);
    }

    public String getStringId() {
        return stringId;
    }

    public int getIoId() {
        return ioId;
    }
    
    

    
  
    
}
