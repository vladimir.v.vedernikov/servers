/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.teltonika.exceptions;

/**
 * Исключение, возникающее при загрузке профиля регистратора Teltonika
 * @author alexander
 */
public class ProfileLoadingException extends RuntimeException{

    private static final long serialVersionUID = -6965344603456125334L;

    public ProfileLoadingException(Throwable cause) {
        super(cause);
    }

    public ProfileLoadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProfileLoadingException(String message) {
        super(message);
    }

    public ProfileLoadingException() {
    }



}
