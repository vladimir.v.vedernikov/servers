/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.filter;

import com.ascatel.asymbix.connectors.teltonika.avl.data.AVLData;
import com.ascatel.asymbix.connectors.teltonika.avl.data.AVLDataBuffer;
import java.util.ArrayList;
import java.util.List;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.CorruptedFrameException;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

/**
 *
 * @author alexander
 */
public class AVLDataDecoder extends FrameDecoder {

    @Override
    protected List<AVLData> decode(ChannelHandlerContext ctx, Channel chnl, ChannelBuffer buffer) throws Exception {
        
        List<AVLData> res = new ArrayList<>();
        AVLDataBuffer messageBuffer = this.getMessageBuffer(ctx);
        
        while(buffer.readable()){
            byte b = buffer.readByte();

            messageBuffer.next(b);
            if (messageBuffer.getStatus() == AVLDataBuffer.Status.READY){
                res.add(new AVLData(messageBuffer.getData()));
                messageBuffer.reset();
                
            } else if (messageBuffer.getStatus() == AVLDataBuffer.Status.FAILED) {
                messageBuffer.reset();
                throw new CorruptedFrameException("Wrong byte sequence");
            }

        }
        
        return res;

    }
    
    private AVLDataBuffer getMessageBuffer(ChannelHandlerContext ctx){

        if(ctx.getAttachment()==null){
            ctx.setAttachment(new AVLDataBuffer());
        }

        return (AVLDataBuffer)ctx.getAttachment();

    }
    
}
