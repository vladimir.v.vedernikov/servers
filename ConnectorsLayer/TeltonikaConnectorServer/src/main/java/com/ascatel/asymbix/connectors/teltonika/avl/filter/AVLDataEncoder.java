/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.filter;

import com.ascatel.asymbix.connectors.teltonika.avl.data.AVLData;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

/**
 *
 * @author alexander
 */
public class AVLDataEncoder extends OneToOneEncoder{

    @Override
    protected Object encode(ChannelHandlerContext chc, Channel chnl, Object message) throws Exception {
        if (!(message instanceof AVLData)) {
            throw new IllegalArgumentException("Wrong message");
        }
        byte[] bytes = ((AVLData)message).getData();
        
        ChannelBuffer buffer = ChannelBuffers.dynamicBuffer();
        
        for (int i = 0; i < 4; i++) {
            buffer.writeByte(0);
        }
        buffer.writeInt(bytes.length);
        buffer.writeBytes(bytes);
        buffer.writeInt(this.getCrc(bytes));
        
        return buffer;
    }
    
    private short getCrc(byte[] array) {
        int crc = 0xFFFF;
        int length = array.length;
        int arrayIndex = 0;
        while (length-- > 0) {
            crc ^= array[arrayIndex++] << 8;
            for (int forIndex = 0; forIndex < 8; forIndex++) {
                crc = (crc & 0x8000) != 0 ? (crc << 1) ^ 0x1021 : crc << 1;
            }
        }
        return (short)crc;
    }
    
}
