/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.handler;

/**
 *
 * @author alexander
 */
public class TeltonikaDataItem{
        
    private final TeltonikaDataItemType dataId;
    private final long value;

    public TeltonikaDataItem(TeltonikaDataItemType dataId, long value) {
        this.dataId = dataId;
        this.value = value;
    }
    
    public TeltonikaDataItem(TeltonikaDataItemType dataId, boolean value) {
        this.dataId = dataId;
        this.value = value ? 1 : 0;
    }
    
    public TeltonikaDataItem(TeltonikaDataItemType dataId) {
        this.dataId = dataId;
        this.value = 0;
    }

    public TeltonikaDataItemType getDataId() {
        return dataId;
    }

    public long getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.dataId != null ? this.dataId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TeltonikaDataItem other = (TeltonikaDataItem) obj;
        if (this.dataId != other.dataId) {
            return false;
        }
        return true;
    }
    
    



}
