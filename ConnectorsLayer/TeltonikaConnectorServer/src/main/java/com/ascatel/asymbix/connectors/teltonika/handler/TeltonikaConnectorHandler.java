/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.teltonika.handler;

import com.ascatel.asymbix.connectors.teltonika.TeltonikaConnectorServer;
import com.ascatel.asymbix.connectors.teltonika.avl.data.AVLData;
import com.ascatel.asymbix.connectors.teltonika.avl.data.DataContainer;
import com.ascatel.asymbix.connectors.teltonika.avl.data.DataContainer.DataContainerItem;
import com.ascatel.asymbix.connectors.teltonika.avl.data.DataContainerMessage;
import com.ascatel.asymbix.connectors.teltonika.avl.data.WelcomeMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Обработчик сообщений регистратора Teltonika
 * @author alexander
 */
public class TeltonikaConnectorHandler extends IdleStateAwareChannelHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final JmsBridgeService messageBrokerService;  
    private final SettingsStorageService settingsStorageService;
    private final DataStorageService dataStorageService;
    private final BlockService blockService;
    private final TeltonikaConnectorServer parent;
    private final ChannelGroup channels;

    /**
     * Конструктор
     * @param router роутер
     * @param settingsStorageService сервис настроек
     * @param channels
     */
    public TeltonikaConnectorHandler(JmsBridgeService messageBrokerService, SettingsStorageService settingsStorageService, DataStorageService dataStorageService, BlockService blockService, TeltonikaConnectorServer parent, ChannelGroup channels) {
        super();

        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.parent = parent;
        this.channels = channels;
        this.dataStorageService = dataStorageService;
        this.blockService = blockService;
    }
    
    

    @Override
    public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {

        if (e instanceof ChannelStateEvent) {
            LOGGER.info(e.toString());
        }
        super.handleUpstream(ctx, e);

    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {

        List<AVLData> messages = (List<AVLData>) e.getMessage();


        for(AVLData message : messages){
            this.processMessage(ctx, message);
            
        }

    }

    private void processMessage(final ChannelHandlerContext ctx, AVLData message){
        
        LOGGER.info("MESSAGE RECEIVED: "+message);
        
        
        if(!this.parent.isActive()){
            ctx.getChannel().close();
        }
        final TeltonikaRegistratorProfile profile;
        if(ctx.getAttachment()==null){
            //Пришло сообщение Welcome-message
            long registratorId = new WelcomeMessage(message.getData()).parseData();
            profile  = new TeltonikaRegistratorProfile(registratorId);
            try{
                 int BLOCK_ID = 1; //Неоткуда брать, поэтому const
                 String[] blockIdValues = this.settingsStorageService.get(BLOCK_ID,new StringBuilder().append(profile.getRegistratorId()).append("-TELTONIKA-ROUTE").toString());
                 if(blockIdValues.length>0){
                     profile.setBlockId(Integer.parseInt(blockIdValues[0]));
                 } 
                 loadMessagesConfiguration(new StringBuilder().append(profile.getRegistratorId()).append("-TELTONIKA-MESSAGE").toString(),profile);
 
            } catch (InvalidProtocolBufferException | SettingsStorageServiceException ex) {
                LOGGER.log(Level.WARN, "Can't load settings", ex);
            }

            ctx.setAttachment(profile);
            ctx.getChannel().write(1);
        } else {
            //Пришло сообщение с данными
            profile = (TeltonikaRegistratorProfile)ctx.getAttachment();
            DataContainer container = new DataContainerMessage(message.getData()).parseData();

            Iterator<DataContainerItem> containerIterator = container.getIterator();
            while(containerIterator.hasNext()){
         
                DataContainerItem containerItem = containerIterator.next();
                int messageTime = containerItem.getMessageTime();
                Iterator<TeltonikaDataItem> dataIterator = containerItem.getIterator();
                Map<TeltonikaRegistratorProfile.DataConfigurationItem, OutputMessage> map = new HashMap<>();
                while(dataIterator.hasNext()){
                    TeltonikaDataItem dataItem = dataIterator.next();
                    
                    
                    LOGGER.log(Level.INFO,"MESSAGE: "+dataItem.toString().replaceAll("\n", " "));

                    //определяем по item.getDataId() список заголовков сообщений (sourceId+messageType), в которые эти данные должны попасть,
                    //также определяем индекс данных в сообщении
                    List<TeltonikaRegistratorProfile.DataConfigurationItem> conf = profile.getDataConfiguration(dataItem.getDataId());


                    //помещаем данные в сообщение по заданному индексу
                    for(TeltonikaRegistratorProfile.DataConfigurationItem confItem : conf){
                        OutputMessage outputMessage = map.get(confItem);
                        if(outputMessage==null){
                            outputMessage=new OutputMessage(confItem.getSourceId(),confItem.getMessageType(),messageTime,profile.getBlockId());
                        }
                        outputMessage.addValue(confItem.getIndex(), dataItem.getValue());
                        map.put(confItem, outputMessage);
                    }
                }
                Collection<OutputMessage> outputMessages = map.values();
                for(OutputMessage outputMessage : outputMessages){
                    if(outputMessage.isCompleted()){
                        sendToServices(outputMessage.toMessageLite());
                    }

                }
            }

            ctx.getChannel().write(makeResponseMessage(container.getRecordsCount()));
            
        }

    }
    
    private static AVLData makeResponseMessage(int code){
        ByteBuffer buf = ByteBuffer.allocate(4);
        buf.putInt(code);
        return new AVLData(buf.array());
    }
    
    private void sendToServices(MessageLite message){
//        try{
//            this.messageBrokerService.send(message);
//        } catch (JmsBridgeServiceException ex) {
//            LOGGER.log(Level.WARN, "Can't send message to the Message Broker", ex);
//        }
        try{
            this.dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.log(Level.WARN, "Can't send message to the Data Storage", ex);
        }
        
        this.blockService.send(message);
                            
    }
    
    private void loadMessagesConfiguration(String key, TeltonikaRegistratorProfile profile) throws InvalidProtocolBufferException, SettingsStorageServiceException{
        
        String[] settings = this.settingsStorageService.get(profile.getBlockId(),key);//getSampleMessagesConfiguration();
        
        for(String s : settings) {
            profile.addConfigurationItem(s);
        }
        
    }


    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        if(!this.parent.isActive()){
            ctx.getChannel().close();
        } else {
            channels.add(e.getChannel());
        }
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        LOGGER.log(Level.INFO, new StringBuilder().append("Session closed: ").append(ctx.getAttachment()).toString());

    }


    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) throws Exception {
        
        if(e.getState() == IdleState.READER_IDLE){
            LOGGER.log(Level.WARN, "Channel is closing because of idle. IDLE State is " + e.getState());
            e.getChannel().close();
        } else {
            LOGGER.log(Level.INFO, "IDLE State is " + e.getState());
        }
    }



    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        LOGGER.log(Level.WARN, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        super.channelClosed(ctx, e);
//
//        Channel closedChannel = e.getChannel();
//        ChannelPipeline pipeline = closedChannel.getPipeline();
//        ChannelHandler channelHandler = pipeline.get("idleHandler");
//        if (channelHandler instanceof ExternalResourceReleasable) {            
//            ((ExternalResourceReleasable) channelHandler).releaseExternalResources();
//        }
    }
    

    


    


}
