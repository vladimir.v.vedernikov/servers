/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.handler;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alexander
 */
public enum OutputMessageType {
    
    ANALOG_VALUE_RAW("AnalogValueRaw",504,1),
    ANALOG_VALUE_CORRECTED("AnalogValueCorrected",505,1),
    BINARY_STATE("BinaryState",501,1),
    FUEL_VALUE_VECTOR_RAW("FuelValueVectorRaw",517,0),
    FUEL_VALUE_VECTOR_CALIBRATED("FuelValueVectorCalibrated",518,0),
    FUEL_VALUE_RAW("FuelValueRaw",519,1),
    FUEL_VALUE_CORRECTED("FuelValueCorrected",520,1),
    GPS_RAW("GPSRaw",525,7),
    GPS_CORRECT("GPSCorrect",526,6);

    private final String name;
    private final int code;  
    private final int minValuesCount;
    private static final Map<String, OutputMessageType> map = new HashMap<>();


    static {
        for (OutputMessageType type : EnumSet.allOf(OutputMessageType.class)) {
            map.put(type.getName(), type);
        }
    }

    private OutputMessageType(String name, int code, int minValuesCount) {    
        this.name = name;
        this.code = code;
        this.minValuesCount = minValuesCount;
    }

    public int getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public int getMinValuesCount() {
        return minValuesCount;
    }

    public static OutputMessageType lookup(String name) {
        return map.get(name);
    }
    
}
