/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.data;

/**
 *
 * @author alexander
 */
public abstract class InputMessage<T> {
    
    private final byte[] bytes;

    public InputMessage(byte[] bytes) {
        this.bytes = bytes;
    }
    
    public abstract T parseData();

    public byte[] getBytes() {
        return bytes;
    }
    
    


    
    
    
}
