/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.data;

import com.ascatel.asymbix.connectors.teltonika.handler.TeltonikaDataItem;
import com.ascatel.asymbix.connectors.teltonika.handler.TeltonikaDataItemType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author alexander
 */
public class DataContainer {
    
    private final List<DataContainerItem> data = new ArrayList<>();

    public void add(DataContainerItem item){
        this.data.add(item);
    }

    public void addAll(Collection<DataContainerItem> items){
        this.data.addAll(items);
    }

    public Iterator<DataContainerItem> getIterator(){
        return this.data.iterator();
    }
    
    public int getRecordsCount(){
        return this.data.size();
    }
    
    public static class DataContainerItem {
        private final int messageTime;
        private final List<TeltonikaDataItem> data = new ArrayList<>();

        public DataContainerItem(int messageTime) {
            this.messageTime = messageTime;
        }

        public int getMessageTime() {
            return this.messageTime;
        }

        public void add(TeltonikaDataItemType type, long value){
            this.data.add(new TeltonikaDataItem(type,value));
        }      

        public Iterator<TeltonikaDataItem> getIterator(){
            return this.data.iterator();
        }
        
    }
    
}
