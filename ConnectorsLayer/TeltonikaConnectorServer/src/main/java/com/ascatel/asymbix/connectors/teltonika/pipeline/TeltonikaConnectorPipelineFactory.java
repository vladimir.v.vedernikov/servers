/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.teltonika.pipeline;

import com.ascatel.asymbix.connectors.teltonika.TeltonikaConnectorServer;
import com.ascatel.asymbix.connectors.teltonika.avl.filter.AVLDataDecoder;
import com.ascatel.asymbix.connectors.teltonika.avl.filter.AVLDataEncoder;
import com.ascatel.asymbix.connectors.teltonika.handler.TeltonikaConnectorHandler;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import static org.jboss.netty.channel.Channels.pipeline;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Channel Pipeline Factory for the Teltonika Data Server 
 * @author alexander
 */
public class TeltonikaConnectorPipelineFactory implements ChannelPipelineFactory {


    private final TeltonikaConnectorServer parent;
    
    private final JmsBridgeService messageBrokerService;  
    private final SettingsStorageService settingsStorageService;
    private final DataStorageService dataStorageService;
    private final BlockService blockService;

    private ChannelGroup channels;

    public TeltonikaConnectorPipelineFactory(JmsBridgeService messageBrokerService, 
            SettingsStorageService settingsStorageService, 
            DataStorageService dataStorageService, BlockService blockService, 
            TeltonikaConnectorServer parent, ChannelGroup channels) {
        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.parent = parent;
        this.channels = channels;
        this.dataStorageService = dataStorageService;
        this.blockService = blockService;
    }

    

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = pipeline();

        // Add the number codec first,
        pipeline.addLast("teltonika-decoder", new AVLDataDecoder());
        pipeline.addLast("teltonika-encoder", new AVLDataEncoder());

        // and then business logic.
        // Please note we create a handler for every new channel
        // because it has stateful properties.
        HashedWheelTimer timer = new HashedWheelTimer();
        pipeline.addLast("idleHandler", new IdleStateHandler(timer, 60, 60, 0));
        pipeline.addLast("handler", new TeltonikaConnectorHandler(messageBrokerService, settingsStorageService, dataStorageService, blockService, parent, channels));

        return pipeline;

    }

  }
