/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.teltonika.avl.data;

import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

/**
 *
 * @author akarpov
 */
public class AVLDataBuffer {

    private static final Logger logger = LoggerFactory.getLogger();
    
    private int bytesCounter;
    
    private State state;
    private Status status;
    private byte[] array;
    private int index;
    private int length;
    
    
    public enum Status {

        READY,
        NEED_MORE,
        FAILED;
    }

    private enum State {

        START,
        LEN,
        BODY,
        STOP;
    }
    
   

    public AVLDataBuffer() {
        this.status = Status.NEED_MORE;
        this.state = State.START;
        this.bytesCounter=0;
        this.length=0;
    }

    public void reset() {
        this.status = Status.NEED_MORE;
        this.state = State.START;
        this.bytesCounter=0;
        this.length=0;
    }

    public void next(byte nextByte) {
        if (this.status == Status.NEED_MORE) {
        } else {
            throw new IllegalStateException("Buffer isn't ready to accept data");
        }
        switch (state) {
            case START:
                if (nextByte == 0) {
                    if(++this.bytesCounter>=4){
                        this.bytesCounter = 0;
                        this.state = State.LEN;
                    }
                } else {
                    this.state = State.STOP;
                    this.status = Status.FAILED;
                    this.bytesCounter = 0;   
                }
                this.length=0;
                break;
     
            case LEN:
                int val =  (nextByte & 0xFF) << (4-this.bytesCounter)*8;
                this.length = this.length & val;
                if(++this.bytesCounter>=4){
                    this.bytesCounter = 0;
                    this.state = State.BODY;
                    this.index = 0;
                    this.array = new byte[this.length+4]; //+4 байта CRC
                }
               
                break;

            case BODY:
            
                this.array[this.index++] = nextByte;
                if (this.index >= this.array.length) {
                    if (getCrc() == extractCrc()) {
                        this.status = Status.READY;
                    } else {
                        logger.log(Level.INFO,"Wrong CRC");
                        this.status = Status.FAILED; 
                    }
                    this.state = State.STOP;
                }
                
                break;

     
            case STOP:
            default:
                break;
        }
    }

    private short getCrc() {
        int crc = 0xFFFF;
        int size = this.array.length - 4;
        int arrayIndex = 0;
        while (size-- > 0) {
            crc ^= this.array[arrayIndex++] << 8;
            for (int forIndex = 0; forIndex < 8; forIndex++) {
                crc = (crc & 0x8000) != 0 ? (crc << 1) ^ 0x1021 : crc << 1;
            }
        }
        return (short) crc;
    }

    private short extractCrc() {
        int crcIndex = this.array.length - 2;
        int crc = (((this.array[crcIndex] & 0xff) << 8) |
                (this.array[crcIndex + 1] & 0xff));
        return (short) crc;
    }

    public byte[] getData() {
        if (getStatus() == Status.READY) {
            byte[] returnArray = new byte[this.array.length - 4];
            System.arraycopy(this.array, 0, returnArray, 0, returnArray.length);
            return returnArray;
        }
        throw new IllegalStateException("Buffer isn't ready to return data");
    }

    public Status getStatus() {
        return this.status;
    }

}

