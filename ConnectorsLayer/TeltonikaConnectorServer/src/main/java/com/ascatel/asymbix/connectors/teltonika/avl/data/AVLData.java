/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.data;

/**
 *
 * @author alexander
 */
public class AVLData {
    
    private final byte[] data;

    public AVLData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

}
