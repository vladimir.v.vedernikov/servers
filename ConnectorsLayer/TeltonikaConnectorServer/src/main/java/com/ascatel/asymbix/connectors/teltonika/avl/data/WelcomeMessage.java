/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.data;

/**
 *
 * @author alexander
 */
public class WelcomeMessage extends InputMessage<Long>{

    public WelcomeMessage(byte[] bytes) {
        super(bytes);
    }

    @Override
    public Long parseData() {
        byte[] bytes = this.getBytes();
        StringBuilder sb = new StringBuilder();
        
        for (int i = 2; i < bytes.length; i++) {
            char c = (char)(bytes[i]&0xFF);
            sb.append(c);
        }
        
        return Long.parseLong(sb.toString());
    }

}
