/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.teltonika.avl.data;

import com.ascatel.asymbix.connectors.teltonika.avl.data.DataContainer.DataContainerItem;
import com.ascatel.asymbix.connectors.teltonika.handler.TeltonikaDataItemType;
import java.nio.ByteBuffer;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

/**
 *
 * @author alexander
 */
public class DataContainerMessage extends InputMessage<DataContainer>{
    
    private static final Logger logger = LoggerFactory.getLogger();
    
    private static final long STARTING_POINT = 1293840000000L; // 01.01.2011 00:00:00 GMT (UTC);

    public final static int CODEC_ID = 8;
    
    public DataContainerMessage(byte[] bytes) {
        super(bytes);
    }
    

    @Override
    public DataContainer parseData() {
        DataContainer res = new DataContainer();
        byte[] bytes = this.getBytes();
        ByteBuffer buf = ByteBuffer.wrap(bytes);
        
        byte codecId = buf.get();
        if(codecId!=CODEC_ID){
            throw new IllegalArgumentException("Unknown codec id: "+codecId);
        }
        int numberOfData = buf.get();
        
        for (int i = 0; i < numberOfData; i++) {
            long timeStamp = buf.getLong();
            DataContainerItem dataContainerItem = new DataContainerItem(getMessageTime(timeStamp));
            int priority = buf.get();
            dataContainerItem.add(TeltonikaDataItemType.PRIORITY,priority);
            long latitude = buf.getLong();
            dataContainerItem.add(TeltonikaDataItemType.LATITUDE,(int)(latitude/100) ); //Teltonika передает координаты с точностью до 7 знаков после запятой, а платформа работает с координатами с точностью до 5-ти знаков
            long longitude = buf.getLong();
            dataContainerItem.add(TeltonikaDataItemType.LONGITUDE,(int)(longitude/100) );
            int altitude = buf.getShort();
            dataContainerItem.add(TeltonikaDataItemType.ALTITUDE,altitude);
            int sattelites = buf.get();
            dataContainerItem.add(TeltonikaDataItemType.SUTELLITES_NUMBER,sattelites);
            int speed = buf.getShort();
            dataContainerItem.add(TeltonikaDataItemType.SPEED_GPS,speed);
            
            int eventOID = buf.get();
            if(eventOID>0){
                dataContainerItem.add(TeltonikaDataItemType.EVENT_OID, eventOID);
            }
            int elementsInRecord = buf.get();
            for (int pow2 = 0; pow2 < elementsInRecord; pow2++) { //pow2 - степень двойки; определяет размер данных в байтах: 2^0 = байт, 2^1 = 2 байта и т.д.
                int n = buf.get(); //количество элементов текущего типа
                for (int k = 0; k < n; k++) {
                    
                    int ioId = buf.get();
                    long value;
                    switch(pow2){
                        case 0: 
                            value = buf.get();
                            break;
                        case 1:
                            value = buf.getShort();
                            break;
                        case 2:
                            value = buf.getInt();
                            break;
                        case 3:
                            value = buf.getLong();
                            break;
                        default:
                            throw new IllegalArgumentException("Unsupported data type: "+Math.pow(2, pow2)+" bytes");
                        
                    }
                    
                    TeltonikaDataItemType type = TeltonikaDataItemType.lookupByIoId(ioId);
                    if(type!=null){
                        dataContainerItem.add(type, value);
                    } else {
                        logger.log(Level.INFO, "Unsupported IO Element id: "+ioId);
                    }
                    
                }
            
                
            }
            
            
            
        }
        
        
        return res;
    }
    
    private static int getMessageTime(long unixTimeStamp){
        return (int)((unixTimeStamp-STARTING_POINT)/1000L);
    }
    
}
