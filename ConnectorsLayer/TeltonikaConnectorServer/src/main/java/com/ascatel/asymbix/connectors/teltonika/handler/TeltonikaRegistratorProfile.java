/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.teltonika.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Профиль регистратора Teltonika
 * @author alexander
 */
public class TeltonikaRegistratorProfile {
    
    private final long registratorId;
    private int blockId = 0;
    private final HashMap<TeltonikaDataItemType, List<DataConfigurationItem>> map = new HashMap<>();

    public TeltonikaRegistratorProfile(long registratorId) {
        this.registratorId = registratorId;
    }

    public long getRegistratorId() {
        return registratorId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }
    
    

    public int getBlockId() {
        return blockId;
    }

    
    public void addConfigurationItem(String settingsValue) {
        String[] items = settingsValue.split("-");
        if(items.length<2){
            throw new IllegalArgumentException("Wrong settingsValue: "+settingsValue);
        }
        int sourceId = Integer.parseInt(items[0]);
        OutputMessageType messageType =  OutputMessageType.lookup(items[1]);
        
 
        for (int i = 2; i < items.length; i++) {
            TeltonikaDataItemType teltonikaDataItemType = TeltonikaDataItemType.lookupByStringId(items[i]);
            List<DataConfigurationItem> list = this.map.get(teltonikaDataItemType);
            if(list==null){
                list = new ArrayList<>();
            }
            list.add(new DataConfigurationItem(sourceId,messageType,i-2));        
            this.map.put(teltonikaDataItemType, list);          
        }    
    }
    
    List<DataConfigurationItem> getDataConfiguration(TeltonikaDataItemType teltonikaDataItemType) {
        return this.map.get(teltonikaDataItemType);
    }

    @Override
    public String toString() {
        return "RegistratorProfile{" + "registratorId=" + registratorId + '}';
    }

    static class DataConfigurationItem {
        private final int sourceId;
        private final OutputMessageType messageType;
        private final int index;

        public DataConfigurationItem(int sourceId, OutputMessageType messageType, int index) {
            this.sourceId = sourceId;
            this.messageType = messageType;
            this.index = index;
        }

        

        public int getSourceId() {
            return sourceId;
        }

        public int getIndex() {
            return index;
        }
        
        

        public OutputMessageType getMessageType() {
            return messageType;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 97 * hash + this.sourceId;
            hash = 97 * hash + (this.messageType != null ? this.messageType.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final DataConfigurationItem other = (DataConfigurationItem) obj;
            if (this.sourceId != other.sourceId) {
                return false;
            }
            if (this.messageType != other.messageType) {
                return false;
            }
            return true;
        }
        
        
    }

    static class MessageConfigurationItem {
        private final TeltonikaDataItemType[] dataIds;

        public MessageConfigurationItem(TeltonikaDataItemType[] dataIds) {

            this.dataIds = dataIds;
        }

        public TeltonikaDataItemType[] getDataIds() {
            return dataIds;
        }
        
        

      
        
    }



}
