/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages.events;

import java.util.List;
import com.ascatel.asymbix.connectors.fas.exceptions.NotDecodableDataException;
import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.ascatel.asymbix.connectors.fas.handler.FasDataItemType;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

/**
 *
 * @author alexander
 */
public class AlarmEvent extends TimerOperInfoEvent{
    
    private static final Logger logger = LoggerFactory.getLogger();
    
    /**
     * Конструктор
     */
    public AlarmEvent(){
        super(MessageType.EV_ALARM);
    }

    @Override
    public List<FasDataItem> getMessages() throws NotDecodableDataException {
        
        byte[] body = this.getData();
        
        logger.log(Level.INFO, "ALARM length: "+body.length);
        
        List<FasDataItem> messages  = super.getMessages();
        
        messages.add(new FasDataItem(FasDataItemType.ALARM_STATE));
        
        return messages;
        
    }
}