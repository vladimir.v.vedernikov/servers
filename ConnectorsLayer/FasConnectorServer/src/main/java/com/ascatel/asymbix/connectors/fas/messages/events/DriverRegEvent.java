/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;
import java.util.ArrayList;
import java.util.List;

/**
 * Событие регистрации водителя
 * @author Nelly
 */
public class DriverRegEvent extends EventMessage{

    /**
     * Конструктор
     */
    public DriverRegEvent(){
        super(MessageType.EV_DRIVER_REG);
    }
    
    @Override
    public int getLength() {
        return super.getLength()+6;
    }

    @Override
    public List<FasDataItem> getMessages() {
        return new ArrayList<>();
    }
}
