/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages;



/**
 * Команда отправки новых настроек регистратору
 */
public class SendSettingsMessage extends BasicMessage {

    /**
     * Конструктор
     * @param body тело сообщения
     */
    public SendSettingsMessage(byte[] body) {
        super(MessageType.CMD_SEND_SETTINGS);
        this.body = body;
            }

    @Override
    public int getLength() {
        return this.body.length;
    }

    @Override
    public String toString() {
        String ret = super.toString();
        return ret;
    }

    @Override
    public byte[] getData() {
        return this.body;
    }

    private byte[] body;
}
