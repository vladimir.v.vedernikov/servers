/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import java.util.ArrayList;
import java.util.List;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;


/**
 * Событие выключения зажигания
 * @author Nelly
 */
public class IgnitionOffEvent extends EventMessage{
    
    /**
     * Конструктор
     */
    public IgnitionOffEvent(){
        super(MessageType.EV_IGT_OFF);
    }

    @Override
    public List<FasDataItem> getMessages() {
        List<FasDataItem> messages  = new ArrayList<>();
//        if(this.getIgnitionSourceId()!=null){
//            messages.add(makeBinaryState(this.getIgnitionSourceId(),false));
//        }
        return messages;
    }

    
    
}
