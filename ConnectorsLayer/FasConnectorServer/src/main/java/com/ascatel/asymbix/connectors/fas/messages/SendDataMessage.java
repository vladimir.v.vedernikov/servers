/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages;


/**
 * Контейнер данных
 */
public class SendDataMessage extends BasicMessage {

    /**
     * Конструктор
     * @param body тело сообщения
     */
    public SendDataMessage(byte[] body) {
        super(MessageType.CMD_SEND_DATA);
        this.body = body;
    }

    /**
     * Возвращает длину сообщения
     * @return длину сообщения
     */
    @Override
    public int getLength() {
        return this.body != null ? this.body.length : 0;
    }

    /**
     * Возвращает тело сообщения
     * @return the body тело сообщения
     */
    public byte[] getBody() {
        return body;
    }


    
    private byte[] body;

    @Override
    public byte[] getData() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
