/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.handler;

import com.ascatel.asymbix.connectors.fas.FasConnectorServer;
import com.ascatel.asymbix.connectors.fas.exceptions.NotDecodableDataException;
import com.ascatel.asymbix.connectors.fas.messages.BasicMessage;
import com.ascatel.asymbix.connectors.fas.messages.ClearDataMessage;
import com.ascatel.asymbix.connectors.fas.messages.GetDataMessage;
import com.ascatel.asymbix.connectors.fas.messages.HelloMessage;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;
import com.ascatel.asymbix.connectors.fas.messages.SendDataMessage;
import com.ascatel.asymbix.connectors.fas.messages.events.AccessPointEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.AlarmEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.DriverRegEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.ErrorEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.EventMessage;
import com.ascatel.asymbix.connectors.fas.messages.events.IgnitionOffEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.IgnitionOnEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.IncomingCallEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.PowerOffEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.PowerOnEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.SettingsEvent;
import com.ascatel.asymbix.connectors.fas.messages.events.TimerOperInfoEvent;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;

/**
 * Обработчик сообщений регистратора FAS
 * @author alexander
 */
public class FasConnectorHandler extends IdleStateAwareChannelHandler{

    private static final Logger LOGGER = LoggerFactory.getLogger();

    private final JmsBridgeService messageBrokerService;  
    private final SettingsStorageService settingsStorageService;
    private final DataStorageService dataStorageService;
    private final BlockService blockService;
    
    private final FasConnectorServer parent;

    private final ChannelGroup channels;
    
  //  private final static int BLOCK_ID = 1; //ID блока сообщений от регистраторов (регистратор об id блока ничего не знает, поэтому забиваем константой)

    /**
     * Конструктор
     * @param router роутер
     * @param settingsStorageService сервис настроек
     * @param channels
     */
    public FasConnectorHandler(JmsBridgeService messageBrokerService, SettingsStorageService settingsStorageService, DataStorageService dataStorageService, BlockService blockService, FasConnectorServer parent, ChannelGroup channels) {
        super();

        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.parent = parent;
        this.channels = channels;
        this.dataStorageService = dataStorageService;
        this.blockService = blockService;
    }
    
    

    @Override
    public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {

        if (e instanceof ChannelStateEvent) {
            LOGGER.info(e.toString());
        }
        super.handleUpstream(ctx, e);

    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {

        List<BasicMessage> messages = (List<BasicMessage>) e.getMessage();

        for(BasicMessage message : messages){
            this.processMessage(ctx, message);
            
        }

    }

    private void processMessage(final ChannelHandlerContext ctx, BasicMessage message){
        
        LOGGER.info("MESSAGE RECEIVED: "+message);
        
        
        if(!this.parent.isActive()){
            ctx.getChannel().close();
        }

        if(ctx.getAttachment()==null && message.getCommand()!=MessageType.CMD_HELLO){
            ctx.getChannel().close();
            return;
        }

        final FasRegistratorProfile profile = (FasRegistratorProfile)ctx.getAttachment();
        
        
        LOGGER.info("COMMAND: "+message.getCommand());
        switch(message.getCommand()){

            case CMD_HELLO:          
                int registratorId = ((HelloMessage)message).getRegistratorId();
                final FasRegistratorProfile p = new FasRegistratorProfile(registratorId);
                try{
                    byte[] bytes = this.settingsStorageService.getBinaryData(getLastDateIdKey(p.getRegistratorId()));
                    if(bytes!=null){
                        p.setLastDataId(ByteBuffer.wrap(bytes).getLong());
                    }
                } catch (SettingsStorageServiceException ex) {
                    LOGGER.log(Level.INFO, "Can't load settings", ex);
                }
                
                //ByteBuffer buffer = ByteBuffer.wrap(bytes).getLong()
                //buffer.getLong();
                
                LOGGER.log(Level.INFO, "FAS Registrator with id "+p.getRegistratorId()+" has been connected");

                //загрузка конфигурации выходных сообщений
                
                try{
                     int BLOCK_ID = 1; //Неоткуда брать, поэтому const
                     
                     String[] blockIdValues = this.settingsStorageService.get(BLOCK_ID,new StringBuilder().append(p.getRegistratorId()).append("-FAS-ROUTE").toString());
                     if(blockIdValues.length>0){
                         p.setBlockId(Integer.parseInt(blockIdValues[0]));
                     } 
                     loadMessagesConfiguration(new StringBuilder().append(p.getRegistratorId()).append("-FAS-MESSAGE").toString(),p);
      
                } catch (InvalidProtocolBufferException | SettingsStorageServiceException ex) {
                    LOGGER.log(Level.WARN, "Can't load settings", ex);
                }
                
                ctx.setAttachment( p);
                if(p.getLastDataId()>0){
                    LOGGER.log(Level.INFO,"CLEAR DATA TILL: "+p.getLastDataId());
                    ctx.getChannel().write(new ClearDataMessage(p.getLastDataId())).addListener(new ChannelFutureListener() {
                       
                        
                        @Override
                        public void operationComplete(ChannelFuture future) throws Exception {
                            p.setLastDataId(0L);
                            ctx.getChannel().write(new GetDataMessage());
                        }
                    });
                } else {
                    ctx.getChannel().write(new GetDataMessage());
                }
                
                break;

            case CMD_SEND_DATA:
                SendDataMessage dataMessage = (SendDataMessage)message;
                int length = dataMessage.getLength();
                long lastDataId = profile.getLastDataId();
                if (length == 0){
                    LOGGER.log(Level.INFO,"EMPTY DATA CONTAINER");
                    //Empty data container
                    if(lastDataId>=0){
                        LOGGER.log(Level.INFO,"CLEAR DATA TILL: "+lastDataId);
                        ctx.getChannel().write(new ClearDataMessage(lastDataId)).addListener(new ChannelFutureListener() {

                            @Override
                            public void operationComplete(ChannelFuture future) throws Exception {
                                profile.setLastDataId(0L);
                                future.getChannel().close();
                            }

                        });

                    } else {
                        ctx.getChannel().close();
                    }


                    break;
                }


                byte[] body = dataMessage.getBody();
                int pos = 0;
                EventMessage event = null;

                while (pos < length) {

                    try{
                        event = parseEvent(body, pos);
                      //  LOGGER.log(Level.INFO,"EVENT: "+event);
                        List<FasDataItem> msgs = event.getMessages();
                        int messageTime = event.getAscatelMessageTime();
                        
                        Map<FasRegistratorProfile.DataConfigurationItem, OutputMessage> map = new HashMap<>();
                        for(FasDataItem item: msgs){
                            //LOGGER.log(Level.INFO,"MESSAGE: "+item.toString().replaceAll("\n", " "));
                            
                            //определяем по item.getDataId() список заголовков сообщений (sourceId+messageType), в которые эти данные должны попасть,
                            //также определяем индекс данных в сообщении
                            List<FasRegistratorProfile.DataConfigurationItem> conf = profile.getDataConfiguration(item.getDataId());
                            
                            if(conf==null){
                                //LOGGER.log(Level.INFO, "Output message type wasn't found for "+item.getDataId()+" data id, registrator id = "+profile.getRegistratorId());
                                continue;
                            }
                            
                            //помещаем данные в сообщение по заданному индексу
                            for(FasRegistratorProfile.DataConfigurationItem confItem : conf){
                                OutputMessage outputMessage = map.get(confItem);
                                if(outputMessage==null){
                                    outputMessage=new OutputMessage(confItem.getSourceId(),confItem.getMessageType(),messageTime,profile.getBlockId());
                                }
                                outputMessage.addValue(confItem.getIndex(), item.getValue());
                                map.put(confItem, outputMessage);
                            }

                        }
                        Collection<OutputMessage> outputMessages = map.values();
                        StringBuilder sb = new StringBuilder().append("Output messages: ");
                        for(OutputMessage outputMessage : outputMessages){
                            sb.append("\n").append(outputMessage);
                            if(outputMessage.isCompleted()){
                                sendToServices(outputMessage.toMessageLite());
                            }
                            
                        }

                        

                    } catch (IllegalArgumentException e){
                        LOGGER.log(Level.INFO, "Archive error", e);
                        profile.setLastDataId(0xFFFFFFFFl);
                        throw e;
                    } catch (NotDecodableDataException e){
                        LOGGER.log(Level.INFO, "Can't parse message container", e);
                    } 
//                    catch (JmsBridgeServiceException e){
//                        LOGGER.log(Level.INFO, "Can't send message to the message broker", e);
//                    }

                    long tmpDataId = event.getFasMessageTime();
                    pos += event.getLength();

                    if(tmpDataId>lastDataId){
                        lastDataId  = tmpDataId;
                        profile.setLastDataId(lastDataId);
                    }

                }

                
                break; //from case CMD_SEND_DATA

        }



    }
    
    private void sendToServices(MessageLite message){
       // LOGGER.log(Level.INFO, "OUTPUT: "+message);
//        try{
//            this.messageBrokerService.send(message);
//        } catch (JmsBridgeServiceException ex) {
//            LOGGER.log(Level.WARN, "Can't send message to the Message Broker", ex);
//        }
        try{
            this.dataStorageService.add(message);
        } catch (DataStorageServiceException ex) {
            LOGGER.log(Level.WARN, "Can't send message to the Data Storage", ex);
        }
        
        this.blockService.send(message);
                            
    }
    
    private void loadMessagesConfiguration(String key, FasRegistratorProfile profile) throws InvalidProtocolBufferException, SettingsStorageServiceException{
        
        String[] settings = this.settingsStorageService.get(profile.getBlockId(),key);//getSampleMessagesConfiguration();
        
        StringBuilder sb = new StringBuilder()
                .append("Output messages settings for FAS-Registrator #")
                .append(profile.getRegistratorId())
                .append(":\nkey: ").append(key).append("\nvalues: ");
        
        for (int i = 0; i < settings.length; i++) {
            if(i>0){
                sb.append(", ");
            }
            sb.append(settings[i]);
            profile.addConfigurationItem(settings[i]);          
        }
        LOGGER.log(Level.INFO, sb.toString());
 
        
    }
    
    private List<String> getSampleMessagesConfiguration(){
        
        List<String> settings = new ArrayList<>();
        
        settings.add("AnalogValueRaw-100-NumberOfPulsesEngine");
        settings.add("AnalogValueCorrected-101-NumberOfPulsesEngine");
        settings.add("BinaryState-102-IgnitionState");
        settings.add("BinaryState-103-GSMState");
        settings.add("BinaryState-104-GPSValid");
        settings.add("FuelValueRaw-105-LLS1Scale-LLS2Scale-LLS3Scale-LLS4Scale");
        settings.add("GPSRaw-106-Latitude-Longitude-Altitude-SpeedGPS-Azimuth-SutellitesNumber-GPSValid");
        
        
        return settings;
        
    }

    private EventMessage parseEvent(byte[] body, int pos) {


        if(body.length<5) {
            throw new IllegalArgumentException("Too short event body length: "+body.length);
        }

        int type = body[pos];//[pos+4];
        LOGGER.log(Level.INFO, "EVENT TYPE CODE: "+type);
        MessageType eventType = MessageType.lookup(type);
        LOGGER.log(Level.INFO, "EVENT TYPE: "+eventType);
        if (eventType == null) {
            throw new IllegalArgumentException("Unknown event type received 0x" + Integer.toHexString(type).toUpperCase());
        }


        EventMessage event;

        switch (eventType) {
            case EV_TMR_OPER_INFO:
                event = new TimerOperInfoEvent();
                break;
            case EV_IGT_ON:
                event = new IgnitionOnEvent();
                break;
            case EV_IGT_OFF:
                event = new IgnitionOffEvent();
                break;
            case EV_PWR_ON:
                event = new PowerOnEvent();
                break;
            case EV_EXT_PWR_OFF:
                event = new PowerOffEvent();
                break;
            case EV_DRIVER_REG:
                event = new DriverRegEvent();
                break;
            case EV_INCOMING_CALL:
                event = new IncomingCallEvent();
                break;
            case EV_SETTINGS:
                event = new SettingsEvent();
                break;
            case EV_ACCESS_POINT:
                event = new AccessPointEvent();
                break;
            case EV_ERROR:
                event = new ErrorEvent();
                break;
            case EV_ALARM:
                event = new AlarmEvent();
                break;
            default:
                throw new IllegalArgumentException("Unknown event type " + type);
        }

        int length = event.getLength();
        byte[] eventBody = new byte[length];
        System.arraycopy(body, pos, eventBody, 0, length);



        event.parseBody(eventBody);
        return event;
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        if(!this.parent.isActive()){
            ctx.getChannel().close();
        } else {
            channels.add(e.getChannel());
        }
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        LOGGER.log(Level.INFO, new StringBuilder().append("Session closed: ").append(ctx.getAttachment()).toString());

        if(ctx.getAttachment() != null){
            FasRegistratorProfile profile = (FasRegistratorProfile)ctx.getAttachment();
    //        this.settingsStorageService.bind(profile.getMessageLite(), getProfileSettingsName(profile.getSourceId()));
            this.settingsStorageService.deleteBinaryData(getLastDateIdKey(profile.getRegistratorId()));
            if(profile.getLastDataId()>0){
                ByteBuffer bb = ByteBuffer.allocate(8);
                bb.putLong(profile.getLastDataId());
                byte[] bytes = bb.array();
                this.settingsStorageService.addBinaryData(
                        getLastDateIdKey(profile.getRegistratorId()), 
                        bytes);
             //   this.settingsStorageService.add(getLastDateIdKey(profile.getRegistratorId()),  Long.toString(profile.getLastDataId()));            
            } 
            
            
        }

    }
    
    private static String getLastDateIdKey(int registratorId){
        return
                new StringBuilder()
                .append(registratorId)
                .append("-FAS-LASTDATEID")
                .toString();
    }

    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) throws Exception {
        
        if(e.getState() == IdleState.READER_IDLE){
            LOGGER.log(Level.WARN, "Channel is closing because of idle. IDLE State is " + e.getState());
            e.getChannel().close();
        } else {
            LOGGER.log(Level.INFO, "IDLE State is " + e.getState());
        }
    }



    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        LOGGER.log(Level.WARN, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        super.channelClosed(ctx, e);
//
//        Channel closedChannel = e.getChannel();
//        ChannelPipeline pipeline = closedChannel.getPipeline();
//        ChannelHandler channelHandler = pipeline.get("idleHandler");
//        if (channelHandler instanceof ExternalResourceReleasable) {            
//            ((ExternalResourceReleasable) channelHandler).releaseExternalResources();
//        }
    }
    

    


    


}
