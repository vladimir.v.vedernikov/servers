/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.handler;

/**
 *
 * @author alexander
 */
public class FasDataItem{
        
    private final FasDataItemType dataId;
    private final int value;

    public FasDataItem(FasDataItemType dataId, int value) {
        this.dataId = dataId;
        this.value = value;
    }
    
    public FasDataItem(FasDataItemType dataId, boolean value) {
        this.dataId = dataId;
        this.value = value ? 1 : 0;
    }
    
    public FasDataItem(FasDataItemType dataId) {
        this.dataId = dataId;
        this.value = 0;
    }

    public FasDataItemType getDataId() {
        return dataId;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.dataId != null ? this.dataId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FasDataItem other = (FasDataItem) obj;
        if (this.dataId != other.dataId) {
            return false;
        }
        return true;
    }
    
    



}
