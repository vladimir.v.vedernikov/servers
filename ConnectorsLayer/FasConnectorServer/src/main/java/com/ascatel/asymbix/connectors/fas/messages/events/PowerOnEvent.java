/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import com.ascatel.asymbix.connectors.fas.handler.FasRegistratorProfile;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;


/**
 * Событие включения зажигания
 * @author Nelly
 */
public class PowerOnEvent extends EventMessage{

    /**
     * Конструктор
     */
    public PowerOnEvent(){
        super(MessageType.EV_PWR_ON);
    }

    @Override
    public List<FasDataItem> getMessages() {
        return new ArrayList<>();
    }
}
