/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages;


/**
 * Базовый класс для описания сообщений взаимодействия с регистраторами
 * @author alexander
 */
public abstract class BasicMessage{
    
    private final MessageType type;

    /**
     * Конструктор
     * @param cmd тип сообщения
     */
    public BasicMessage(MessageType cmd){
        this.type = cmd;
    }

    /**
     * Возвращает длину тела сообщения
     * @return длину тела сообщения
     */
    public abstract int getLength();

    /**
     * Возвращает тип сообщения
     * @return тип сообщения
     */
    public MessageType getCommand() {
        return this.type;
    }

    /**
     * Записывает бинарное представление сообщения в набор байт
     * @param buf набор байт
     */
    public byte[] write () {
        throw new UnsupportedOperationException("Not supported for this type of message.");
    }

    
    /**
     * Возвращает тело сообщения в виде массива байт
     * @return массив байт
     */
    public abstract byte[] getData();

    @Override
    public String toString() {
        String ret = " command " + type.getDescription() + ", body length = " + getLength();
        return ret;
    }
    
    /**
     * Converts two bytes into short value.
     * @param b1 high byte
     * @param b0 low byte
     * @return short value
     */
    public short makeShort(byte b1, byte b0) {
            return (short) (((b1 & 0xff) << 8) | (b0 & 0xff));
    }

    /**
     * Converts 4 bytes into integer value
     * @param b3 very high byte
     * @param b2 second byte
     * @param b1 third byte
     * @param b0 low byte
     * @return integer value
     */
    public int makeInt(byte b3, byte b2, byte b1, byte b0) {
            return ((((b3 & 0xff) << 24) |
                            ((b2 & 0xff) << 16) |
                            ((b1 & 0xff) << 8) |
                            ((b0 & 0xff) << 0)));
    }

    /**
     * Converts 8 bytes into long value
     * @param byte0 element of the array of 8 bytes to be converted
     * @param byte1 element of the array of 8 bytes to be converted
     * @param byte2 element of the array of 8 bytes to be converted
     * @param byte3 element of the array of 8 bytes to be converted
     * @param byte4 element of the array of 8 bytes to be converted
     * @param byte5 element of the array of 8 bytes to be converted
     * @param byte6 element of the array of 8 bytes to be converted
     * @param byte7 element of the array of 8 bytes to be converted
     * @return long value
     */
    public long makeLong(byte byte0, byte byte1, byte byte2, byte byte3, byte byte4,
                    byte byte5, byte byte6, byte byte7) {
            return ((((long) byte0 & 0xff) << 56) |
                            (((long) byte1 & 0xff) << 48) |
                            (((long) byte2 & 0xff) << 40) |
                            (((long) byte3 & 0xff) << 32) |
                            (((long) byte4 & 0xff) << 24) |
                            (((long) byte5 & 0xff) << 16) |
                            (((long) byte6 & 0xff) << 8) |
                            (((long) byte7 & 0xff) << 0));
    }

    public MessageType getType() {
        return type;
    }

    
    
}
