/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.ascatel.asymbix.connectors.fas.handler.FasRegistratorProfile;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;
import java.util.ArrayList;
import java.util.List;


/**
 * Событие "Точка доступа"
 * @author akarpov
 */
public class AccessPointEvent extends EventMessage {

    /**
     * Конструктор
     */
    public AccessPointEvent() {
        super(MessageType.EV_ACCESS_POINT);
    }

    @Override
    public int getLength() {
        return 45;
    }

    @Override
    public List<FasDataItem> getMessages() {
        return new ArrayList<>();
    }



}
