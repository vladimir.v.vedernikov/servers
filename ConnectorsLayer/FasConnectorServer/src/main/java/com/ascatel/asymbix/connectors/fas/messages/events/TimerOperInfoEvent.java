/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.exceptions.NotDecodableDataException;
import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.ascatel.asymbix.connectors.fas.handler.FasDataItemType;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;
import java.util.ArrayList;
import java.util.List;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;


/**
 * Событие "Сбор данных по таймеру"
 * @author Nelly
 */
public class TimerOperInfoEvent extends EventMessage{
    
    private static final Logger logger = LoggerFactory.getLogger();

   /**
    * Конструктор
    */
   public TimerOperInfoEvent(){
       super(MessageType.EV_TMR_OPER_INFO);
   }
   
   public TimerOperInfoEvent(MessageType type){
       super(type);
   }
   
   

    @Override
    public List<FasDataItem> getMessages() throws NotDecodableDataException{
        List<FasDataItem> messages = new ArrayList<>();


        byte[] body = this.getData();
        
        logger.log(Level.INFO, "TIMER_INFO length: "+body.length);

        short flags = makeShort(body[1], body[0]);
        messages.add(new FasDataItem(FasDataItemType.GPS_VALID,(flags >> 2) & 1));
        messages.add(new FasDataItem(FasDataItemType.IGNITION_STATE,(flags & 1)));
        messages.add(new FasDataItem(FasDataItemType.LATITUDE,makeInt(body[8], body[7], body[6], body[5])/100)); //fas-регистратор
        messages.add(new FasDataItem(FasDataItemType.LONGITUDE,makeInt(body[12], body[11], body[10], body[9])/100));
        messages.add(new FasDataItem(FasDataItemType.ALTITUDE,makeShort(body[17], body[16]) * 1000));
        
        int gpsdata = makeInt((byte)0, body[15], body[14], body[13]);
        messages.add(new FasDataItem(FasDataItemType.SATELLITES_NUMBER,(gpsdata >> 19) & 0xF));
        messages.add(new FasDataItem(FasDataItemType.SPEED_GPS,(gpsdata & 0x3FF)*100));//от регистратора приходит в дм/с, соответственно, чтобы получить мм/с умножаем на 100
        messages.add(new FasDataItem(FasDataItemType.AZIMUTH,(gpsdata >> 10) & 0x1FF));
        
        int pos = 18;
        
        try{
            for (int i = 0; i < 4; i++) {
                
                int typeoffset = i*5;
                int temperature;
                if (i == 3) {
                    temperature = 0xFF & body[pos];
                }
                else {
                    temperature = body[pos];
                }
//                sensorData.setTemperature(temperature);
                messages.add(new FasDataItem(llsDateTypes[typeoffset],temperature));

                short packedData = makeShort(body[pos + 2], body[pos + 1]);
                
                messages.add(new FasDataItem(llsDateTypes[typeoffset+1],packedData & 0xfff)); //level code
                messages.add(new FasDataItem(llsDateTypes[typeoffset+2],((packedData>>12)&1)>0)); //error
                messages.add(new FasDataItem(llsDateTypes[typeoffset+3],((packedData>>13)&1)>0)); //exists
                messages.add(new FasDataItem(llsDateTypes[typeoffset+4],((packedData>>14)&1)>0)); //ready
//                sensorData.setLevelCode(packedData & 0xfff);
//                sensorData.setError(((packedData>>12)&1)>0);
//                sensorData.setExist(((packedData>>13)&1)>0);
//                sensorData.setSensorReadiness(((packedData>>14)&1)>0);


                pos += 3;
            }
            
        } catch (Exception ex) {
            throw new NotDecodableDataException("Can't parse data",ex);
        }
   
        return messages;
    }
    
    
    
    private static FasDataItemType[] llsDateTypes = {
        
        FasDataItemType.LLS1_TEMPERATURE,
        FasDataItemType.LLS1_SCALE,
        FasDataItemType.LLS1_ERROR_STATE,
        FasDataItemType.LLS1_AVAILABILITY,
        FasDataItemType.LLS1_READINESS,
        
        FasDataItemType.LLS2_TEMPERATURE,
        FasDataItemType.LLS2_SCALE,
        FasDataItemType.LLS2_ERROR_STATE,
        FasDataItemType.LLS2_AVAILABILITY,
        FasDataItemType.LLS2_READINESS,
        
        FasDataItemType.LLS3_TEMPERATURE,
        FasDataItemType.LLS3_SCALE,
        FasDataItemType.LLS3_ERROR_STATE,
        FasDataItemType.LLS3_AVAILABILITY,
        FasDataItemType.LLS3_READINESS,
        
        FasDataItemType.LLS4_TEMPERATURE,
        FasDataItemType.LLS4_SCALE,
        FasDataItemType.LLS4_ERROR_STATE,
        FasDataItemType.LLS4_AVAILABILITY,
        FasDataItemType.LLS4_READINESS

        
    
    };
    
    

}
