/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.handler;

import com.google.protobuf.MessageLite;
import java.util.TreeSet;
import ru.omnicomm.pegasus.messaging.online.analogSensor.AnalogValueParser.AnalogValueCorrected;
import ru.omnicomm.pegasus.messaging.online.analogSensor.AnalogValueParser.AnalogValueRaw;
import ru.omnicomm.pegasus.messaging.online.binarySensor.BinaryStateParser.BinaryState;
import ru.omnicomm.pegasus.messaging.online.fuel.FuelDataParser.FuelValueCorrected;
import ru.omnicomm.pegasus.messaging.online.fuel.FuelDataParser.FuelValueRaw;
import ru.omnicomm.pegasus.messaging.online.fuel.FuelDataParser.FuelValueVectorCalibrated;
import ru.omnicomm.pegasus.messaging.online.fuel.FuelDataParser.FuelValueVectorRaw;
import ru.omnicomm.pegasus.messaging.online.location.LocationDataParser.GPSCorrect;
import ru.omnicomm.pegasus.messaging.online.location.LocationDataParser.GPSRaw;

/**
 *
 * @author alexander
 */
public class OutputMessage  {
    
  //  private final MessageLite wrappedMessage;
    private final TreeSet<ValueWrapper> valuesSet = new TreeSet<>();
    private final int sourceId;
    private final OutputMessageType outputMessageType;
    private final int messageTime;
    private final int blockId;

    public OutputMessage(int sourceId, OutputMessageType outputMessageType, int messageTime, int blockId) {
        this.sourceId = sourceId;
        this.outputMessageType = outputMessageType;
        this.messageTime = messageTime;
        this.blockId = blockId;
    }
    
    public void addValue(int index, int value){
        this.valuesSet.add(new ValueWrapper(index,value));
    }
    
    public boolean isCompleted(){
        return valuesSet.size()>=outputMessageType.getMinValuesCount();
    }
    
    public MessageLite toMessageLite() {
        
        MessageLite wrappedMessage;
        
        if(!this.isCompleted()){
            StringBuilder sb = new StringBuilder()
                    .append("Not enough values for the ")
                    .append(outputMessageType.getName())
                    .append(" message: expected ")
                    .append(outputMessageType.getMinValuesCount())
                    .append(", but found ").append(valuesSet.size());
            throw new IllegalArgumentException(sb.toString());
        }
        
        ValueWrapper[] values = valuesSet.toArray(new ValueWrapper[valuesSet.size()]);

        
        switch(outputMessageType){
            
            case ANALOG_VALUE_RAW:
                wrappedMessage =                         
                        AnalogValueRaw.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)

                                .setValue(values[0].value)

                                .build();
                break;
                
            case ANALOG_VALUE_CORRECTED:
                wrappedMessage =                         
                        AnalogValueCorrected.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)
                        
                                .setValue(values[0].value)

                                .build();
                break;    
              
            case BINARY_STATE:
                wrappedMessage =                         
                        BinaryState.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)

                                .setValue(values[0].value)

                                .build();
                break;      
                
            case FUEL_VALUE_VECTOR_RAW:
                FuelValueVectorRaw.Builder fuelValueVectorRawBuilder =
                        FuelValueVectorRaw.newBuilder()
                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId);
                for(ValueWrapper value : values){
                    fuelValueVectorRawBuilder.addValues(value.value);
                }
                
                wrappedMessage = fuelValueVectorRawBuilder.build();  
                break;  
                
            case FUEL_VALUE_VECTOR_CALIBRATED:
                FuelValueVectorCalibrated.Builder fuelValueVectorCalibratedBuilder =
                        FuelValueVectorCalibrated.newBuilder()
                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId);
                for(ValueWrapper value : values){
                    fuelValueVectorCalibratedBuilder.addValues(value.value);
                }
                
                wrappedMessage = fuelValueVectorCalibratedBuilder.build();  
                break; 
                
            case FUEL_VALUE_RAW:
                wrappedMessage =                         
                        FuelValueRaw.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)
                                .setValue(values[0].value)

                                .build();
                break;    
                
            case FUEL_VALUE_CORRECTED:
                wrappedMessage =                         
                        FuelValueCorrected.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)
                                .setValue(values[0].value)

                                .build();
                break;  
                
            case GPS_RAW:
                wrappedMessage =                         
                        GPSRaw.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)
                                .setLatitude(values[0].value)
                                .setLongitude(values[1].value)
                                .setAltitude(values[2].value)
                                .setSpeed(values[3].value)
                                .setAzimuth(values[4].value)
                                .setSatellitesNr(values[5].value)
                                .setValid(values[6].value>0)
                        
                                .build();
                break;  
                
            case GPS_CORRECT:
                wrappedMessage =                         
                        GPSCorrect.newBuilder()

                                .setSourceId(sourceId)
                                .setMessageType(outputMessageType.getCode())
                                .setMessageTime(messageTime)
                                .setMessageBlock(blockId)
                                .setLatitude(values[0].value)
                                .setLongitude(values[1].value)
                                .setAltitude(values[2].value)
                                .setSpeed(values[3].value)
                                .setAzimuth(values[4].value)
                                .setSatellitesNr(values[5].value)

                                .build();
                break;  
               

                
            default: 
                throw new IllegalArgumentException(new StringBuilder()
                        .append("Unhandled message type: ")
                        .append(outputMessageType.getName())
                        .toString());
        }
        
        return wrappedMessage;
    }

    
    private class ValueWrapper implements Comparable<ValueWrapper>{
        private final Integer index;
        private final int value;

        public ValueWrapper(int index, int value) {
            this.index = index;
            this.value = value;
        }

        @Override
        public int compareTo(ValueWrapper t) {
            return this.index.compareTo(t.index);
        }
        
          
    }
    
}
