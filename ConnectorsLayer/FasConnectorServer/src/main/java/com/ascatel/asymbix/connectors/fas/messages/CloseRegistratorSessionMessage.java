/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages;

/**
 * Внутренняя команда КС для закрытия соединения с регистратором
 * @author akarpov
 */
public class CloseRegistratorSessionMessage extends BasicMessage {

    private String cause;

    /**
     * Конструктор
     * @param cause причина закрытия соединения
     */
    public CloseRegistratorSessionMessage(String cause){
        super(MessageType.SERV_CLOSE_SESSION);
        this.cause = cause;
    }

    @Override
    public int getLength() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public byte[] getData() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getCause(){
        return this.cause;
    }

}
