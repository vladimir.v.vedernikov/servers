/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import com.ascatel.asymbix.connectors.fas.handler.FasRegistratorProfile;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;


/**
 * Событие "Входящий звонок"
 * (на данный момент (10.2010) не реализовано на стороне регистратора)
 * @author akarpov
 */
public class IncomingCallEvent extends EventMessage {
    
    /**
     * Конструктор
     */
    public IncomingCallEvent(){
        super(MessageType.EV_INCOMING_CALL);
    }

    @Override
    public List<FasDataItem> getMessages() {
        return new ArrayList<>();
    }
}
