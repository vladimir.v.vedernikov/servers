/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Профиль регистратора FAS
 * @author alexander
 */
public class FasRegistratorProfile {
    
    private final int registratorId;
    private int blockId;
    private long lastDataId;
    private final HashMap<FasDataItemType, List<DataConfigurationItem>> map = new HashMap<>();

    public FasRegistratorProfile(int registratorId) {
        this.registratorId = registratorId;

    }

    public int getRegistratorId() {
        return registratorId;
    }

  
    public void setBlockId(int blockId){
        this.blockId = blockId;
    }
    

    public int getBlockId() {
        return blockId;
    }
  
 
    /**
     * Возвращает id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     * @return id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     */
    public long getLastDataId() {
        return lastDataId;
    }

    /**
     * Устанавливает id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     * @param lastDataId id последнего сообщения, принятого от регистратора FAS (в формате FAS)
     */
    public void setLastDataId(long lastDataId) {
        this.lastDataId = lastDataId;
    }
    
    public void addConfigurationItem(String settingsValue) {
        String[] items = settingsValue.split("-");
        if(items.length<2){
            throw new IllegalArgumentException("Wrong settingsValue: "+settingsValue);
        }
        int sourceId = Integer.parseInt(items[1]);
        OutputMessageType messageType =  OutputMessageType.lookup(items[0]);
        
 
        for (int i = 2; i < items.length; i++) {
            FasDataItemType fasDataItemType = FasDataItemType.lookup(items[i]);
            List<DataConfigurationItem> list = this.map.get(fasDataItemType);
            if(list==null){
                list = new ArrayList<>();
            }
            list.add(new DataConfigurationItem(sourceId,messageType,i-2));        
            this.map.put(fasDataItemType, list);          
        }    
    }
    
    List<DataConfigurationItem> getDataConfiguration(FasDataItemType fasDataItemType) {
        return this.map.get(fasDataItemType);
    }

    @Override
    public String toString() {
        return "RegistratorProfile{" + "registratorId=" + registratorId + "; lastDataId=" + lastDataId + '}';
    }

    static class DataConfigurationItem {
        private final int sourceId;
        private final OutputMessageType messageType;
        private final int index;

        public DataConfigurationItem(int sourceId, OutputMessageType messageType, int index) {
            this.sourceId = sourceId;
            this.messageType = messageType;
            this.index = index;
        }

        

        public int getSourceId() {
            return sourceId;
        }

        public int getIndex() {
            return index;
        }
        
        

        public OutputMessageType getMessageType() {
            return messageType;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 97 * hash + this.sourceId;
            hash = 97 * hash + (this.messageType != null ? this.messageType.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final DataConfigurationItem other = (DataConfigurationItem) obj;
            if (this.sourceId != other.sourceId) {
                return false;
            }
            if (this.messageType != other.messageType) {
                return false;
            }
            return true;
        }
        
        
    }

    static class MessageConfigurationItem {
        private final FasDataItemType[] dataIds;

        public MessageConfigurationItem(FasDataItemType[] dataIds) {

            this.dataIds = dataIds;
        }

        public FasDataItemType[] getDataIds() {
            return dataIds;
        }
        
        

      
        
    }



}
