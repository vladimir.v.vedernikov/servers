/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages;

import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;

/**
 *
 * @author akarpov
 */
public class MessageBuffer {

    private static final Logger logger = LoggerFactory.getLogger();

    public final static byte SYN = (byte) 0xC0;
    public final static byte ESC = (byte) 0xDB;
    public final static byte ESC_SYN = (byte) 0xDC;
    public final static byte ESC_ESC = (byte) 0xDD;

    public enum Status {

        READY,
        NEED_MORE,
        FAILED;
    }
    //private static final Logger logger = LoggerFactory.getLogger();

    private enum State {

        START,
        CMD,
        LEN,
        ESC_LEN,
        BODY,
        ESC_BODY,
        STOP;
    }
    private State state;
    private Status status;
    private byte[] array;
    private int index;
    private byte cmd;

    public MessageBuffer() {
        this.status = Status.NEED_MORE;
        this.state = State.START;
    }

    public void reset() {
        this.status = Status.NEED_MORE;
        this.state = State.START;
    }

    public void next(byte nextByte) {
        if (this.status == Status.NEED_MORE) {
        } else {
            throw new IllegalStateException("Buffer isn't ready to accept data");
        }
        switch (state) {
            case START:
                if (nextByte == SYN) {
                    this.state = State.CMD;
                } else {
                    this.state = State.STOP;
                    this.status = Status.FAILED;
                }
                break;
            case CMD:
                if (nextByte == SYN || nextByte == ESC) {
                    this.state = State.STOP;
                    this.status = Status.FAILED;
                } else {
                    this.state = State.LEN;
                    this.cmd = nextByte;
                }
                break;
            case LEN:
                if (nextByte == ESC) {
                    this.state = State.ESC_LEN;
                } else {
                    this.array = new byte[(nextByte & 0xff)+ 4];
                    this.index = 0;
                    this.array[index++] = cmd;
                    this.array[index++] = nextByte;
                    this.state = State.BODY;
                }
                break;
            case ESC_LEN:
                if (nextByte == ESC_SYN) {
                    this.array = new byte[(SYN & 0xff) + 4];
                    this.index = 0;
                    this.array[index++] = cmd;
                    this.array[index++] = SYN;
                    this.state = State.BODY;
                } else if (nextByte == ESC_ESC) {
                    this.array = new byte[(ESC & 0xff)+ 4];
                    this.index = 0;
                    this.array[index++] = cmd;
                    this.array[index++] = ESC;
                    this.state = State.BODY;
                } else {
                    this.state = State.STOP;
                    this.status = Status.FAILED;
                }
                break;
            case BODY:
                if (nextByte == ESC) {
                    this.state = State.ESC_BODY;
                } else {
                    this.array[this.index++] = nextByte;
                    if (this.index >= this.array.length) {
                        if (getCrc() == extractCrc()) {
                            this.status = Status.READY;
                        } else {
                            logger.log(Level.INFO,"Wrong CRC");
                            this.status = Status.FAILED; 
                        }
                        this.state = State.STOP;
                    }
                }
                break;

            case ESC_BODY:
                if (nextByte == ESC_SYN) {
                    this.array[this.index++] = SYN;
                    if (this.index >= this.array.length) {
                        if (getCrc() == extractCrc()) {
                            this.status = Status.READY;
                        } else {
                            logger.log(Level.INFO,"Wrong CRC");
                            this.status = Status.FAILED; 
                        }
                        this.state = State.STOP;
                    } else {
                        this.state = State.BODY;
                    }
                } else if (nextByte == ESC_ESC) {
                    this.array[this.index++] = ESC;
                    if (this.index >= this.array.length) {
                        if (getCrc() == extractCrc()) {
                            this.status = Status.READY;
                        } else {
                            logger.log(Level.INFO,"Wrong CRC");
                            this.status = Status.FAILED; 
                        }
                        this.state = State.STOP;
                    } else {
                        this.state = State.BODY;
                    }
                } else {
                    this.state = State.STOP;
                    this.status = Status.FAILED;
                }
                break;
            case STOP:
            default:
                break;
        }
    }

    private short getCrc() {
        int crc = 0xFFFF;
        int length = this.array.length - 2;
        int arrayIndex = 0;
        while (length-- > 0) {
            crc ^= this.array[arrayIndex++] << 8;
            for (int forIndex = 0; forIndex < 8; forIndex++) {
                crc = (crc & 0x8000) != 0 ? (crc << 1) ^ 0x1021 : crc << 1;
            }
        }
        return (short) crc;
    }

    private short extractCrc() {
        int crcIndex = this.array.length - 2;
        int crc = (((this.array[crcIndex] & 0xff) << 8) |
                (this.array[crcIndex + 1] & 0xff));
        return (short) crc;
    }

    public byte[] getBody() {
        if (getStatus() == Status.READY) {
            byte[] returnArray = new byte[this.array.length - 4];
            System.arraycopy(this.array, 2, returnArray, 0, returnArray.length);
            return returnArray;
        }
        throw new IllegalStateException("Buffer isn't ready to return data");
    }

    public Status getStatus() {
        return this.status;
    }

    public int getCommand() {
        return this.cmd;
    }
}

