/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import java.util.ArrayList;
import java.util.List;
import com.ascatel.asymbix.connectors.fas.handler.FasRegistratorProfile;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;

/**
 * Событие "Ошибка"
 * @author akarpov
 */
public class ErrorEvent extends EventMessage{


    /**
     * Конструктор
     */
    public ErrorEvent() {
        super(MessageType.EV_ERROR);
    }

    @Override
    public int getLength() {
        return 9;
    }

    @Override
    public List<FasDataItem> getMessages() {
        return new ArrayList<>();
    }

}
