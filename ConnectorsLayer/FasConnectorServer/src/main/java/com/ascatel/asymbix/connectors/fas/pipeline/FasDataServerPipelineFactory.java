/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.pipeline;

import com.ascatel.asymbix.connectors.fas.FasConnectorServer;
import com.ascatel.asymbix.connectors.fas.filter.FasProtocolDecoder;
import com.ascatel.asymbix.connectors.fas.filter.FasProtocolEncoder;
import com.ascatel.asymbix.connectors.fas.handler.FasConnectorHandler;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import static org.jboss.netty.channel.Channels.pipeline;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Channel Pipeline Factory for the FAS Data Server 
 * @author alexander
 */
public class FasDataServerPipelineFactory implements ChannelPipelineFactory {


    private final FasConnectorServer parent;
    
    private final JmsBridgeService messageBrokerService;  
    private final SettingsStorageService settingsStorageService;
    private final DataStorageService dataStorageService;
    private final BlockService blockService;

    private ChannelGroup channels;

    public FasDataServerPipelineFactory(JmsBridgeService messageBrokerService, 
            SettingsStorageService settingsStorageService, 
            DataStorageService dataStorageService, BlockService blockService, 
            FasConnectorServer parent, ChannelGroup channels) {
        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.parent = parent;
        this.channels = channels;
        this.dataStorageService = dataStorageService;
        this.blockService = blockService;
    }

    

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = pipeline();

        // Add the number codec first,
        pipeline.addLast("fas-decoder", new FasProtocolDecoder());
        pipeline.addLast("fas-encoder", new FasProtocolEncoder());

        // and then business logic.
        // Please note we create a handler for every new channel
        // because it has stateful properties.
        HashedWheelTimer timer = new HashedWheelTimer();
        pipeline.addLast("idleHandler", new IdleStateHandler(timer, 600, 600, 0));
        pipeline.addLast("handler", new FasConnectorHandler(messageBrokerService, settingsStorageService, dataStorageService, blockService, parent, channels));

        return pipeline;

    }

  }
