/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.google.protobuf.MessageLite;
import java.util.ArrayList;
import java.util.List;
import com.ascatel.asymbix.connectors.fas.handler.FasRegistratorProfile;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;


/**
 * Событие включения зажигания
 * @author Nelly
 */
public class IgnitionOnEvent extends EventMessage{
    
    /**
     * Конструктор
     */
    public IgnitionOnEvent(){
        super(MessageType.EV_IGT_ON);
    }

    @Override
    public List<FasDataItem> getMessages() {
        List<FasDataItem> messages  = new ArrayList<>();
//        if(this.getIgnitionSourceId()!=null){
//            messages.add(makeBinaryState(this.getIgnitionSourceId(),true));
//        }
        return messages;
    }

}
