/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages;


/**
 *
 * @author alexander
 */
public class AcknowledgeStartFirmwareUpdating extends BasicMessage{

    private final byte[] body;

    public AcknowledgeStartFirmwareUpdating(byte[] body) {
        super(MessageType.CMD_ACKN_START_FW_UPDTG);
        this.body = body;
    }



    @Override
    public int getLength() {
        return this.body.length;
    }

    @Override
    public byte[] getData() {
        return this.body;
    }

}
