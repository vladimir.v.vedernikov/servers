/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages.events;

import com.google.protobuf.MessageLite;
import java.util.List;
import com.ascatel.asymbix.connectors.fas.exceptions.NotDecodableDataException;
import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.ascatel.asymbix.connectors.fas.handler.FasRegistratorProfile;
import com.ascatel.asymbix.connectors.fas.messages.BasicMessage;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;



/**
 * Базовый класс для сообщений, содержащихся в контейнере данных
 * SendDataMessage
 * @author alexander
 */
public abstract class EventMessage extends BasicMessage {
    
    private long dataID;
    private byte[] data;
    
//    protected static final int GROUP_RAW_DATA_TYPE = GroupRawDataParser.GroupRawData.getDefaultInstance().getMessageType();
//    protected static final int MULTIPLY_SPEED_TYPE = MultiplySpeedParser.MultiplySpeed.getDefaultInstance().getMessageType();
//    protected static final int COORDINATE_TYPE     = CoordinateParser.Coordinate.getDefaultInstance().getMessageType();
//    protected static final int SPEED_AND_AZMT_TYPE = SpeedAndAzimuthParser.SpeedAndAzimuth.getDefaultInstance().getMessageType();
//    protected static final int ANALOG_VALUE_TYPE   = AnalogValueRawParser.AnalogValueRaw.getDefaultInstance().getMessageType();
//    protected static final int BINARY_STATE_ON_TYPE   = BinaryStateParser.BinaryStateON.getDefaultInstance().getMessageType();


    /**
     *  Конструктор
     * @param type тип сообщения по протоколу взаимодействия с регистратором
     */
    public EventMessage(MessageType type) {
        super(type);

    }
        

    @Override
        public byte[] getData(){
            return this.data;
        }


    /**
     * Декодирует тело сообщения
     * @param body тело сообщения
     */
    public void parseBody(byte[] body) {
        if (body == null || body.length != getLength()) {
                throw new IllegalArgumentException("Wrong body length for message " +
                                getCommand().getDescription());
        }

        this.dataID = makeLong((byte)0,(byte)0,(byte)0,(byte)0, body[4], body[3], body[2], body[1]);
        this.data = new byte[body.length-5];
        System.arraycopy(body, 5, this.data, 0, body.length-5);


    }


    @Override
    public int getLength() {
        return 39;
    }

    /**
     * Возвращает id сообщения в формате FAS
     * @return id сообщения в формате FAS
     */
    public long getFasMessageTime() {
        return this.dataID;
    }

    /**
     * Возвращает id сообщения в формате Pegasus
     * @return id сообщения в формате Pegasus
     */
    public int getAscatelMessageTime(){
        return (int)(this.dataID - 63072000L);
        //63072000 сек. - разница между 01.01.2011 и 01.01.2009
    }


	@Override
	public String toString() {
            String ret = " Event type = " + getCommand().getDescription() + ", dataID = " +
                            dataID + ", body length = " + getLength();

            return ret;
	}





    //63072000


    /**
     * Возвращает список сообщений, содержащихся в пакете данных FAS
     * (данные по топливу, местоположение и т.д.)
     * @return список сообщений, содержащихся в пакете данных FAS
     * @throws NotDecodableDataException в случае ошибки декодирования
     */
    public abstract List<FasDataItem> getMessages() throws NotDecodableDataException;
	

}
