/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas.messages.events;

import com.ascatel.asymbix.connectors.fas.handler.FasDataItem;
import com.ascatel.asymbix.connectors.fas.messages.MessageType;
import java.util.ArrayList;
import java.util.List;


/**
 * Событие "настройки регистратора"
 * @author akarpov
 */
public class SettingsEvent extends EventMessage{

    /**
     * Конструктор
     */
    public SettingsEvent(){
        super(MessageType.EV_SETTINGS);
    }
    
    @Override
    public int getLength() {
        return 30;
    }

    @Override
    public List<FasDataItem> getMessages() {
        return new ArrayList<>();
    }
    
   

}
