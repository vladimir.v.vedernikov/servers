/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.handler;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author alexander
 */
public enum FasDataItemType {
   
    IGNITION_STATE("IgnitionState"),
    GSM_STATE("GSMState"),
    GPS_VALID("GPSValid"),
    ROAMING_STATE("RoamingState"),
    ALARM_STATE("AlarmState"),
    LATITUDE("Latitude"),
    LONGITUDE("Longitude"),
    SPEED_GPS("SpeedGPS"),
    AZIMUTH("Azimuth"),
    SATELLITES_NUMBER("SatellitesNumber"),
    ALTITUDE("Altitude"),
    LLS1_TEMPERATURE("LLS1_Temperature"),
    LLS2_TEMPERATURE("LLS2_Temperature"),
    LLS3_TEMPERATURE("LLS3_Temperature"),
    LLS4_TEMPERATURE("LLS4_Temperature"),
    BOARD_VOLTAGE("BoardVoltage"),
    LLS1_SCALE("LLS1Scale"),
    LLS2_SCALE("LLS2Scale"),
    LLS3_SCALE("LLS3Scale"),
    LLS4_SCALE("LLS4Scale"),
    LLS1_ERROR_STATE("LLS1ErrorState"),
    LLS2_ERROR_STATE("LLS2ErrorState"),
    LLS3_ERROR_STATE("LLS3ErrorState"),
    LLS4_ERROR_STATE("LLS4ErrorState"),
    LLS1_AVAILABILITY("LLS1Availability"),
    LLS2_AVAILABILITY("LLS2Availability"),
    LLS3_AVAILABILITY("LLS3Availability"),
    LLS4_AVAILABILITY("LLS4Availability"),
    LLS1_READINESS("LLS1Readiness"),
    LLS2_READINESS("LLS2Readiness"),
    LLS3_READINESS("LLS3Readiness"),
    LLS4_READINESS("LLS4Readiness"),
    MILEAGE("Mileage"),
    NUMBER_OF_PULSES_SPEED("NumberOfPulsesSpeed"),
    NUMBER_OF_PULSES_ENGINE("NumberOfPulsesEngine"),
    DRIVER_ID ("DriverId"),
    EVENT_TYPE("EventType");
    
    
    private final String id;
    private static final Map<String, FasDataItemType> map = new HashMap<>();
    
    private FasDataItemType(String id) {
        this.id = id;
    }

    static {
        for (FasDataItemType type : EnumSet.allOf(FasDataItemType.class)) {
            map.put(type.getId(), type);
        }
    }
    

    public static FasDataItemType lookup(String id) {
        return map.get(id);
    }

    public String getId() {
        return id;
    }

    
  
    
}
