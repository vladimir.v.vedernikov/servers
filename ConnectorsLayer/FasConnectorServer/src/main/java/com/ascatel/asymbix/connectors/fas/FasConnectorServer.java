/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.fas;

import com.ascatel.asymbix.connectors.fas.pipeline.FasDataServerPipelineFactory;
import com.google.protobuf.MessageLite;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.ChannelGroupFutureListener;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.exceptions.SendMessageException;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.*;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.data.DataStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;

/**
 * Сервер приема данных от регистратора FAS
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("FasConnectorServerImplementation")
public class FasConnectorServer implements ServerImplementation {


    private final JmsBridgeService messageBrokerService;  
    private final SettingsStorageService settingsStorageService;
    private final DataStorageService dataStorageService;
    private final BlockService blockService;
    
    private Server source;
    private AtomicBoolean active = new AtomicBoolean(false);
    private NettyServer nettyServer = new NettyServer();
    private static final Logger logger = LoggerFactory.getLogger();

    /**
     * Конструктор.
     *
     * @param router                 сервис маршрутизатор сообщений.
     * @param settingsStorageService сервис хранения настроек.
     */
    @ServerConstructor
    public FasConnectorServer(JmsBridgeService messageBrokerService, SettingsStorageService settingsStorageService, DataStorageService dataStorageService, BlockService blockService) {
        this.messageBrokerService = messageBrokerService;
        this.settingsStorageService = settingsStorageService;
        this.dataStorageService = dataStorageService;
        this.blockService = blockService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
    }

    

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(Init signal) {
                logger.log(Level.INFO, "Init FasDataServer");
                
                source = signal.getSelf();
                logger.log(Level.INFO, "Starting netty server...");
                nettyServer.start();

                active.set(true);
            }

            @Override
            public void visit(Pause signal) {
                active.set(false);

                try {
                    signal.source().send(new Paused() {

                        private static final long serialVersionUID = 1941887840224135544L;

                        @Override
                        public Server source() {
                            return source;
                        }

                        @Override
                        public <E extends Throwable> void accept(Visitor<E> visitor)
                                throws E {
                            visitor.visit(this);
                        }
                    });
                } catch (SendMessageException ex) {
                    logger.log(Level.WARN, "Can't send signal", ex);
                }
            }

            @Override
            public void visit(Resume signal) {
                active.set(true);
            }

            @Override
            public void visit(Terminate signal) {
                nettyServer.stop();
            }
        });
    }

    public boolean isActive() {
        return this.active.get();
    }

    private class NettyServer {

        private ChannelGroup channels;

        private ChannelFactory channelFactory;

        private NettyServer() {
            channels = new DefaultChannelGroup("FasDataServer_channel_group");
            channelFactory = new NioServerSocketChannelFactory(
                    Executors.newCachedThreadPool(), Executors.newCachedThreadPool());
        }

        public void start() {
            Properties pr = new Properties();
            int port = 9977;
            
            try (FileInputStream fs = new FileInputStream("config/fasdataserver.properties")) {
                pr.load(fs);
                port = Integer.parseInt(pr.getProperty("fas.protocol.port", "9977"));
            } catch (IOException | NumberFormatException ex) {
                logger.log(Level.INFO, "Can''t load properties", ex);
            }
            // Configure the server.
            ServerBootstrap bootstrap = new ServerBootstrap(channelFactory);
            // Set up the evsent pipeline factory.
            final FasDataServerPipelineFactory pipelineFactory
                    = new FasDataServerPipelineFactory(messageBrokerService, settingsStorageService, dataStorageService, blockService, FasConnectorServer.this, channels);
            bootstrap.setPipelineFactory(pipelineFactory);

            // Bind and start to accept incoming connections.
            Channel serverChannel = bootstrap.bind(new InetSocketAddress(port));
            channels.add(serverChannel);
            logger.log(Level.INFO,"Netty server started on "+port);
        }

        public void stop() {
            channels.close().addListener(new ChannelGroupFutureListener() {
                @Override
                public void operationComplete(ChannelGroupFuture future) throws Exception {
                    channelFactory.releaseExternalResources();
                }
            });
        }
    }

}
