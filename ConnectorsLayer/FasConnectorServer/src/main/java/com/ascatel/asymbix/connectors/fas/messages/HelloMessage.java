/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ascatel.asymbix.connectors.fas.messages;


/**
 * Запрос авторизации от регистратора
 * @author Nelly
 */
public class HelloMessage extends BasicMessage {

    private int registratorId;
    private int fwVersion;
    private byte[] body;

    /**
     * Конструктор
     * @param body тело сообщения
     */
    public HelloMessage(byte[] body) {
        super(MessageType.CMD_HELLO);

        this.body = body;
        if (body != null && body.length == 4) {

            int id = (body[1] & 0xff)*0x100 + (body[0] & 0xff);
            this.registratorId = id;//0x02000000 | id;
            this.fwVersion = (body[3] & 0xff)*0x100 + (body[2] & 0xff);
        } else

        if (body != null && body.length == 8) {
            int id = this.makeInt(body[3], body[2], body[1], body[0]);
            this.registratorId = id;
//            int id = (body[1] & 0xff)*0x100 + (body[0] & 0xff);
//            this.vehicleID = 0x02000000 | id;
            this.fwVersion = this.makeInt(body[7], body[6], body[5], body[4]);

        } else


        {
            throw new IllegalArgumentException("Wrong body length for HelloMessage");
        }

    }

    @Override
    public int getLength() {
        return body.length;
    }

    /**
     * Возвращает id регистратора
     * @return id регистратора
     */
    public int getRegistratorId() {
        return registratorId;
    }

    /**
     * Возвращает версию прошивки
     * @return версию прошивки
     */
    public int getFwVersion() {
        return fwVersion;
    }

    @Override
    public String toString() {
        return super.toString()
                + " [vehicle ID = " + registratorId
                + ", firmware version = " + fwVersion + " ]";
    }       

    @Override
    public byte[] getData() {
        return this.body;
    }
}
