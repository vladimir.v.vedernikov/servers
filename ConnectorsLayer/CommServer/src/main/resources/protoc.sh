#!/bin/sh

DEST_PATH=../java


SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/online/
for FILE_NAME in CommunicationMessages.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done


SRC_PATH=../../../../../../../MessageStorage/src/main/resources/protobuf/common/
for FILE_NAME in EmptyMessage.proto; do
    ln -s ${SRC_PATH}/${FILE_NAME} ${FILE_NAME}
    protoc ./${FILE_NAME} --java_out "${DEST_PATH}"
    rm ${FILE_NAME}
done

echo "done"
