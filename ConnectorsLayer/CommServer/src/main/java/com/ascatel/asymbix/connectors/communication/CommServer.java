/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ascatel.asymbix.connectors.communication;

import com.ascatel.asymbix.rendering.platform.messages.dao.EmptyMessageParser.EmptyMessage;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.UnknownFieldSet.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import ru.omnicomm.pegasus.messaging.online.communication.CommunicationMessagesParser.RegisterAT;
import ru.omnicomm.pegasus.messaging.online.communication.CommunicationMessagesParser.RegisterSensor;
import ru.omnicomm.pegasus.messaging.online.communication.CommunicationMessagesParser.UnregisterAT;
import ru.omnicomm.pegasus.messaging.online.communication.CommunicationMessagesParser.UnregisterSensor;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageService;
import ru.omnicomm.pegasus.processingPlatform.services.settings.SettingsStorageServiceException;


/**
 * Сервер коммуникации в слое подключений
 *
 * @author alexander
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("ConnectorsLayerCommServerImplementation")
public class CommServer implements ServerImplementation {

    private Server self;
    
    private final static int MESSAGE_TYPE_FIELD_ID = 2;
    
    private final static int REGISTER_AT_MESSAGE_TYPE = 535;
    private final static int UNREGISTER_AT_MESSAGE_TYPE = 536;
    private final static int REGISTER_SENSOR_MESSAGE_TYPE = 537;
    private final static int UREGISTER_SENSOR_MESSAGE_TYPE = 538;
    

    private final SettingsStorageService settingsStorageService;
    private final BlockService blockService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger();

    @ServerConstructor
    public CommServer(SettingsStorageService settingsStorageService, BlockService blockService) {
        this.settingsStorageService = settingsStorageService;
        this.blockService = blockService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {
        try{
            ByteString bytes = message.toByteString();
            int messageType = extractMessageType(bytes);
            
            switch(messageType) {
                case REGISTER_AT_MESSAGE_TYPE:
                    processMessage(RegisterAT.parseFrom(bytes));
                    break;
                case UNREGISTER_AT_MESSAGE_TYPE:
                    processMessage(UnregisterAT.parseFrom(bytes));
                    break;
                case REGISTER_SENSOR_MESSAGE_TYPE:
                    processMessage(RegisterSensor.parseFrom(bytes));
                    break;
                case UREGISTER_SENSOR_MESSAGE_TYPE:
                    processMessage(UnregisterSensor.parseFrom(bytes));
                    break;
                default:
                    LOGGER.log(Level.WARN, "Unsupported message: "+message);
            }
           
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, "Can't process message: "+message, ex);
        }
        
    }
    
    private static int extractMessageType(ByteString bytes) throws InvalidProtocolBufferException{
        
        EmptyMessage header = EmptyMessage.parseFrom(bytes);
        if(header.getUnknownFields().hasField(MESSAGE_TYPE_FIELD_ID)){
            Field messageTypeField = header.getUnknownFields().getField(MESSAGE_TYPE_FIELD_ID);
            List<Long> list = messageTypeField.getVarintList();
            if(!list.isEmpty()){
                return list.get(0).intValue();
            } else {
                throw new IllegalArgumentException ("Field "+MESSAGE_TYPE_FIELD_ID+" does't contain int value (message "+header+")");
            }
        } else {
            throw new IllegalArgumentException ("Field "+MESSAGE_TYPE_FIELD_ID+" wasn't found in the message "+header);
        }
    }

    

    @Override
    public void onSignal(Signal signal) {
        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();

                blockService.registerServer(CommServer.class, self, 
                        
                        REGISTER_AT_MESSAGE_TYPE,
                        UNREGISTER_AT_MESSAGE_TYPE,
                        REGISTER_SENSOR_MESSAGE_TYPE,
                        UREGISTER_SENSOR_MESSAGE_TYPE
                );
            }

            @Override
            public void visit(Terminate signal) {
                super.visit(signal);  
            }
        });
    }
    
    private void processMessage(RegisterAT message) throws SettingsStorageServiceException{
        String regType = message.getATType();
        int blockId = message.getMessageBlock();
        
        //Если задано старое значение id регистратора и это значение не равно значению нового id регистратора; 
        //также необходимо учесть, что новое значение id регистратора может быть не задано
        if(message.hasCurrentATId() && (!message.hasNewATId() ||( message.hasNewATId() && message.getCurrentATId()!=message.getNewATId()))){
            Long regNumOld = message.getCurrentATId();
            String routeByTypeKey = makeSettingsItem(regNumOld,regType,"ROUTE");
            String sourceIdByKey = makeSettingsItem(regNumOld,regType,"SOURCEID");
            String messageByTypeKey = makeSettingsItem(regNumOld,regType,"MESSAGE");
            
            if(message.hasNewATId()){
                
                Long regNum = message.getNewATId();
                
                String[] routeByTypeValues = this.settingsStorageService.get(blockId,routeByTypeKey);
                String[] sourceIdByTypeValues = this.settingsStorageService.get(blockId,sourceIdByKey);
                String[] messageByTypeValues = this.settingsStorageService.get(blockId,messageByTypeKey);
                //TODO: List<String> routeByTypeValues ... 
                
                String newRouteByTypeKey = makeSettingsItem(regNum,regType,"ROUTE");
                String newSourceIdByTypeKey = makeSettingsItem(regNum,regType,"SOURCEID");
                String newMessageByTypeKey = makeSettingsItem(regNum,regType,"MESSAGE");
                
                this.settingsStorageService.add(blockId,newRouteByTypeKey, routeByTypeValues);
                this.settingsStorageService.add(blockId,newSourceIdByTypeKey, sourceIdByTypeValues);
                this.settingsStorageService.add(blockId,newMessageByTypeKey, messageByTypeValues);
                
            }
            
            this.settingsStorageService.delete(blockId,routeByTypeKey);
            this.settingsStorageService.delete(blockId,sourceIdByKey);
            this.settingsStorageService.delete(blockId,messageByTypeKey);
            
            if(!message.hasNewATId()){
                return;
            }      
        }
        //Далее документ TMP-PSS-10-(CT COMM Server ) содержит следующую чушь:
        /*
         4. Сервер считывает из сервиса настроек значение настройки с key = <RegNum>–<RegId>-ROUTE
         5. Если значение настройки найдено и не пусто – прерывание данной ветви (если абонентский терминал уже зарегистрирован на какую-либо прикладную систему, не отменяем этой регистрации)
         6. Сервер формирует и сохраняет в сервисе настроек настройку key = <RegNum>–<RegType>-ROUTE, value = ApplicationID
         7. Сервер обновляет (не создаёт новую запись) настройку с key = <RegType>–<RegNum>-SOURCEID присваивая значение value = ApplicationID
         */
        //Что за RegId - не понятно! видимо, message.getATSourceId() (методом исключения)
        //настройку с ключем <RegNum>–<RegType>-ROUTE записывали на предыдущих шагах - ок, пишем еще раз
        //почему у ROUTE ключ начинается с "<RegNum>–<RegType>", а у SOURCEID с "<RegType>–<RegNum>" также не совсем понятно!
        //Реализую:
        Long regNum = message.getNewATId();
        Long regId = message.getATSourceId(); 
        String routeByIdKey = makeSettingsItem(regNum,regId,"ROUTE");
        String[] routeByIdValues = this.settingsStorageService.get(blockId,routeByIdKey);
        if(routeByIdValues.length==0){ // TODO: if(routeByIdValues.isEmpty) ...

            Integer applicationId = message.getMessageBlock();
            this.settingsStorageService.add(blockId,makeSettingsItem(regNum,regType,"ROUTE"), regType);
            
            String key = makeSettingsItem(regType,regNum,"SOURCEID");
            this.settingsStorageService.delete(blockId,key);
            this.settingsStorageService.add(blockId,key, applicationId.toString());
            
        }
    }
    
    private void processMessage(UnregisterAT message) throws SettingsStorageServiceException{
        Long regId = message.getATId();
        String regType = message.getATType();
        int blockId = message.getMessageBlock();
     
        this.settingsStorageService.delete(blockId,makeSettingsItem(regId,regType,"ROUTE"));   
        this.settingsStorageService.delete(blockId,makeSettingsItem(regId,regType,"SOURCEID"));
        this.settingsStorageService.delete(blockId,makeSettingsItem(regId,regType,"MESSAGE"));         
    }
    
    private void processMessage(RegisterSensor message) throws SettingsStorageServiceException{
        int blockId = message.getMessageBlock();
        String regType = message.getATType();
        Long regNum = message.getATId();
        String mt = message.getOutputMessageType();
        String dataOldId = null; //
        if(message.hasCurrentATDataId()){
            dataOldId= message.getCurrentATDataId();
        }
        //String dataId = message.getNewATDataId();
        Integer theNumber = message.getSerialNumber();
        Long sid = message.getSensorSourceId();
        
        String key = makeSettingsItem(regNum,regType,"MESSAGE");        
   
        String[] values = this.settingsStorageService.get(blockId,key); //TODO: read all possible values

        List<String> suitableValues = new ArrayList<>();
        String valuePrefix = makeSettingsItem(mt,sid);
        for(String v : values) {
           if(v.startsWith(valuePrefix)){
               suitableValues.add(v);
           }
        }
        List<String> newSettings = new ArrayList<>();
        for(String v : suitableValues){
            TreeSet<DataIdWrapper> dataIds = new TreeSet<>();
            String items[] = v.split("-");
            for (int i = 2; i < items.length; i++) {
                if(dataOldId==null || (dataOldId!=null && !dataOldId.equalsIgnoreCase(items[i]))) {
                    dataIds.add(new DataIdWrapper(i-1,items[i]));
                }   
            }
            if(message.hasNewATDataId()){
                dataIds.add(new DataIdWrapper(theNumber,message.getNewATDataId()));
            }
            
       
            if(!dataIds.isEmpty()) {
                newSettings.add( makeSettingsItem(mt,sid,dataIds.toArray(new DataIdWrapper[dataIds.size()])));
            } 
        }
        

        
        if(suitableValues.isEmpty() && message.hasNewATDataId()){
            newSettings.add( makeSettingsItem(mt,sid,message.getNewATDataId()));
        }
        
        if(newSettings.isEmpty()){
            this.settingsStorageService.delete(blockId,key);
        } else {
            this.settingsStorageService.add(blockId,key,newSettings.toArray(new String[newSettings.size()]));
        }     
    }
    
    private void processMessage(UnregisterSensor message) throws SettingsStorageServiceException{
        int blockId = message.getMessageBlock();
        String regType = message.getATType();
        Long regNum = message.getATId();
        String mt = message.getOutputMessageType();
   
        //String dataId = message.getNewATDataId();
        int theNumber = message.getSerialNumber();

        
        String key = makeSettingsItem(regNum,regType,"MESSAGE");        

        String[] values = this.settingsStorageService.get(blockId,key); 


        List<String> newSettings = new ArrayList<>();
        for(String v : values){
            List<String> dataIds = new ArrayList<>();
            String items[] = v.split("-");
            for (int i = 2; i < items.length; i++) {       
                dataIds.add(items[i]);
            }
        
            dataIds.remove(theNumber);
       
            if(!dataIds.isEmpty()) {
                newSettings.add( makeSettingsItem(mt,items[1],dataIds.toArray(new String[dataIds.size()])));
            } 
        }
        
        if(newSettings.isEmpty()){
            this.settingsStorageService.delete(blockId,key);
        } else {
            this.settingsStorageService.add(blockId,key,newSettings.toArray(new String[newSettings.size()]));
        }   
        
    }
    
    private static String makeSettingsItem(Object... args){
        if(args.length>0){
            StringBuilder s = new StringBuilder(args[0].toString());
            for (int i = 1; i < args.length; i++) {
                s.append("-").append(args[i].toString());
            }
            return s.toString();
        } else {
            throw new IllegalArgumentException ("Empty array");
        }
   
    }
    
    private final class DataIdWrapper implements Comparable<DataIdWrapper> {
        private final Integer index;
        private final String value;

        public DataIdWrapper(Integer index, String value) {
            this.index = index;
            this.value = value;
        }
        
        @Override
        public int compareTo(DataIdWrapper t) {
            return this.index.compareTo(t.index);
        }

        @Override
        public String toString() {
            return this.value;
        }
        
        
        
    }

}
