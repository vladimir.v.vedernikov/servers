/**
 * Class Environment
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 03.04.13
 */
package com.ascatel.asymbix.server.blp;

import com.ascatel.asymbix.server.blp.op.AsyncOperation;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class Environment {

    private Server server;
    private JmsBridgeService jmsBridge;
    private BusinessLogicService blService;

//    private Map<String, AsyncHandler> blpRequests;

    private Map<String, AsyncOperation> blpOperations;


    public Environment(Server server, JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.server = server;
        this.jmsBridge = jmsBridge;
        this.blService = blService;

//        this.blpRequests = new HashMap<>();
        this.blpOperations = new HashMap<>();
    }

    public Server getServer() {
        return server;
    }

    public JmsBridgeService getJmsBridge() {
        return jmsBridge;
    }

    public BusinessLogicService getBlService() {
        return blService;
    }

//    public void addHandler(String bplInstanceId, AsyncHandler asyncHandler) {
//        blpRequests.put(bplInstanceId, asyncHandler);
//    }

    public void addOperation(String bplInstanceId, AsyncOperation asyncOperation) {
        blpOperations.put(bplInstanceId, asyncOperation);
    }

//    public AsyncHandler getHandler(String blpInstanceId) {
//        return blpRequests.get(blpInstanceId);
//    }

    public AsyncOperation getOperation(String blpInstanceId) {
        return blpOperations.get(blpInstanceId);
    }

//    public void removeHandler(String instanceId) {
//        blpRequests.remove(instanceId);
//    }

    public void removeOperation(String instanceId) {
        blpOperations.remove(instanceId);
    }

}
