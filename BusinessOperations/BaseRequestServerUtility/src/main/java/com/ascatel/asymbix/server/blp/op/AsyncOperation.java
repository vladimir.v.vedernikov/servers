/**
 * Class AsyncOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public interface AsyncOperation {

    void handleErrorResponse(BLSMessageParser.ErrorMessage errMsg);

    void handleCRUDResponse(BLSMessageParser.CRUDResponse crud);

    void handleSelectResponse(BLSMessageParser.ResultSet resultSet);

}
