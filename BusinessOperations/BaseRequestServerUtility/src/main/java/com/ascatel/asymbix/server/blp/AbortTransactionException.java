/**
 * Class AbortTransactionException
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 29.04.13
 */
package com.ascatel.asymbix.server.blp;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class AbortTransactionException extends RuntimeException {

    public AbortTransactionException() {
    }

    public AbortTransactionException(String message) {
        super(message);
    }

    public AbortTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbortTransactionException(Throwable cause) {
        super(cause);
    }

    public AbortTransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
