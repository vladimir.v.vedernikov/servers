/**
 * Class AttrRange
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 26.04.13
 */
package com.ascatel.asymbix.server.blp.util;

import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

/**
 * Используется для передачи значения 'AttributeRange' для атрибутов типа ObjectLink в читабельном виде.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class ObjectLinkAttrRange {

    private String entityType;

    private String attributeId;

    /**
     * Множественность связи: 0, если можно задать только 1 объект и 1 - если можно задать много объектов.
     */
    private int multiplicity;

    public static ObjectLinkAttrRange parseAttrRange(String string)
            throws BusinessLogicServiceException {
        final String[] spltitted = string.split(";");

        if (spltitted.length != 3) {
            final String errMsg = new StringBuilder()
                    .append("Invalid format of AttributeRange parameter value.")
                    .append("Should be <pairedEntityType;multiplicity;pairedAttributeId>")
                    .append(" instead of ").append(string).toString();
            throw new BusinessLogicServiceException(
                    errMsg);
        }


        final ObjectLinkAttrRange attrRange = new ObjectLinkAttrRange();
        attrRange.entityType = spltitted[0].trim();

        final String pairedAttrId = spltitted[2].trim();
        if ("0".equals(pairedAttrId)) {
            attrRange.attributeId = null;
        } else {
            attrRange.attributeId = pairedAttrId;
        }

        final String multiplicityStr = spltitted[1].trim();
        try {
            attrRange.multiplicity = Integer.parseInt(multiplicityStr);
        } catch (NumberFormatException e) {
            throw new BusinessLogicServiceException("Invalid multiplicity value=" + multiplicityStr);
        }

        return attrRange;
    }

    public String getEntityType() {
        return entityType;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public int getMultiplicity() {
        return multiplicity;
    }

}
