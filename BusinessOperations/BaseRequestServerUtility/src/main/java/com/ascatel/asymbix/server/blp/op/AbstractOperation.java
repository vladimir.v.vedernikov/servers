/**
 * Class AbstractOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op;

import com.ascatel.asymbix.server.blp.Environment;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ErrorMessage;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CRUDResponse;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.UpdateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

import java.util.List;

import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildDeleteRequest;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildReadRequest;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildUpdateRequest;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class AbstractOperation implements AsyncOperation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    private static final String DEFAULT_LOCALE = "en";

    /**
     * Используемая локаль.
     */
    private String locale;

    /**
     * Идентификатор запросного сообщения. С таким же значением долже быть отправлен ответ.
     */
    private String instanceId;

    /**
     * Идентификатор блока - логическое разделение данных.
     */
    private int blockId;

    /**
     * Пишутся всевозможные ошибки. Если это поле не null то надо оправлять сообщение с ошибкой.
     */
    private StringBuilder errorMessage;

    /**
     * Контекст сервера.
     */
    private Environment env;

    public AbstractOperation(AbstractOperation handler) {
        this(handler.getEnv());

        setBlockId(handler.getBlockId());
        setInstanceId(handler.getInstanceId());
        setLocale(handler.getLocale());
    }

    public AbstractOperation(Environment env) {
        this.env = env;
    }

    public Environment getEnv() {
        return env;
    }

    public void setLocale(String locale) {
        if (locale == null || locale.isEmpty()) {
            this.locale = DEFAULT_LOCALE;
        } else {
            this.locale = locale;
        }
    }

    public String getLocale() {
        return locale;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public int getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    public String getErrorMessage() {
        if (errorMessage == null) {
            return null;
        } else {
            return errorMessage.toString();
        }
    }

    public void addErrorMessage(String message) {
        if (errorMessage == null) {
            errorMessage = new StringBuilder();
        }
        errorMessage.append(message).append(";");
    }

    @Override
    public void handleErrorResponse(ErrorMessage errMsg) {
        final String msg = new StringBuilder().append("[").append(instanceId).append("] ").append(errMsg.getCause())
                .toString();
        addErrorMessage(msg);
    }

    @Override
    public void handleCRUDResponse(CRUDResponse crud) {
        LoggerFactory.getLogger().log(Level.WARN, "Empty handleCRUDResponse invocation");
    }

    @Override
    public void handleSelectResponse(ResultSet resultSet) {
        LoggerFactory.getLogger().log(Level.WARN, "Empty handleSelectResponse invocation");
    }

    protected void sendResponse(MessageLite response) {
        try {
            getEnv().getJmsBridge().send(response);
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    protected String doSelect(CreateRequest createRequest)
            throws BusinessLogicServiceException {
        final BusinessLogicService blService = getEnv().getBlService();
        final Server server = getEnv().getServer();
        final byte[] requestData = createRequest.toByteArray();


        final String blpInstanceId = blService.createEntityAsync(server, requestData);
        getEnv().addOperation(blpInstanceId, this);
        return blpInstanceId;
    }

    protected String doSelect(UpdateRequest updateRequest)
            throws BusinessLogicServiceException {
        final BusinessLogicService blService = getEnv().getBlService();
        final Server server = getEnv().getServer();
        final byte[] requestData = updateRequest.toByteArray();


        final String blpInstanceId = blService.setParameterValueAsync(server, requestData);
        getEnv().addOperation(blpInstanceId, this);
        return blpInstanceId;
    }

    protected String doSelect(ReadRequest readRequest)
            throws BusinessLogicServiceException {
        final BusinessLogicService blService = getEnv().getBlService();
        final Server server = getEnv().getServer();
        final byte[] requestData = readRequest.toByteArray();


        final String blpInstanceId = blService.getParameterValueAsync(server, requestData);
        getEnv().addOperation(blpInstanceId, this);
        return blpInstanceId;
    }

    protected String doSelect(DeleteRequest deleteRequest)
            throws BusinessLogicServiceException {
        final BusinessLogicService blService = getEnv().getBlService();
        final Server server = getEnv().getServer();
        final byte[] requestData = deleteRequest.toByteArray();


        final String blpInstanceId = blService.deleteEntityAsync(server, requestData);
        getEnv().addOperation(blpInstanceId, this);
        return blpInstanceId;
    }

    protected String doSelect(SelectRequest selectRequest)
            throws BusinessLogicServiceException {
        final BusinessLogicService blService = getEnv().getBlService();
        final Server server = getEnv().getServer();
        final byte[] requestData = selectRequest.toByteArray();


        final String blpInstanceId = blService.selectByParamsAsync(server, requestData);
        getEnv().addOperation(blpInstanceId, this);
        return blpInstanceId;
    }

    protected ParameterValue readParameterValue(String entityType, String entityId, String parameterId)
            throws BusinessLogicServiceException {
        final ReadRequest request = buildReadRequest(getBlockId(), entityType, entityId, parameterId);
        final byte[] responseData = getEnv().getBlService().getParameterValue(request.toByteArray());
        try {
            return ParameterValue.parseFrom(responseData);
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    protected void updateParameterValue(String entityType, String entityId, String parameterId, ParameterValue value)
            throws BusinessLogicServiceException {
        final UpdateRequest request = buildUpdateRequest(getBlockId(), entityType, entityId, parameterId, value);

        if (!getEnv().getBlService().setParameterValue(request.toByteArray())) {
            final String errMsg = new StringBuilder().append("Cannot update parameter value for ")
                    .append(entityType).append("/").append(entityId).append("/").append(parameterId).toString();
            throw new BusinessLogicServiceException(errMsg);
        }
    }

    protected void deleteEntity(String entityType, String entityId)
            throws BusinessLogicServiceException {
        final DeleteRequest request = buildDeleteRequest(getBlockId(), entityType, entityId);
        if (!getEnv().getBlService().deleteEntity(request.toByteArray())) {
            final String errMsg = new StringBuilder().append("Cannot delete entity ")
                    .append(entityType).append("/").append(entityId).toString();
            throw new BusinessLogicServiceException(errMsg);
        }
    }



    protected ResultSet selectEntities(SelectRequest request)
            throws BusinessLogicServiceException {
        final byte[] data = getEnv().getBlService().selectByParams(request.toByteArray());
        try {
            return ResultSet.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

}
