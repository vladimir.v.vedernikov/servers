/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 28.09.12
 */
package com.ascatel.asymbix.server.blp.util;

import com.google.protobuf.ByteString;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateTemporaryRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.UpdateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.MUIString;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ValueArray;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;

import java.util.*;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public class MessageUtil {

    private static int DEFAULT_MESSAGE_BLOCK = 1;

    public static CreateRequest buildCreateRequest(String entityType) {
        return buildCreateRequest(DEFAULT_MESSAGE_BLOCK, entityType);
    }

    public static CreateRequest buildCreateRequest(int blockId, String entityType) {
        return CreateRequest.newBuilder()
                .setMessageType(Constants.CREATE_REQUEST_MESSAGE_TYPE)
                .setMessageBlock(blockId)
                .setEntityType(entityType).build();
    }

    public static CreateTemporaryRequest buildCreateTemporaryRequest(String entityType) {
        return buildCreateTemporaryRequest(DEFAULT_MESSAGE_BLOCK, entityType);
    }

    public static CreateTemporaryRequest buildCreateTemporaryRequest(int blockId, String entityType) {
        return CreateTemporaryRequest.newBuilder()
                .setMessageType(Constants.CREATE_TEMPORARY_REQUEST_MESSAGE_TYPE)
                .setMessageBlock(blockId)
                .setEntityType(entityType).build();
    }

    public static UpdateRequest buildUpdateRequest(String entityType, String entityId, String parameterId,
                                                   ParameterValue value) {
        return buildUpdateRequest(DEFAULT_MESSAGE_BLOCK, entityType, entityId, parameterId, value);
    }

    public static UpdateRequest buildUpdateRequest(int blockId, String entityType, String entityId, String parameterId,
                                                   ParameterValue value) {
        return UpdateRequest.newBuilder()
                .setMessageType(Constants.UPDATE_REQUEST_MESSAGE_TYPE)
                .setMessageBlock(blockId)
                .setEntityType(entityType)
                .setEntityId(entityId)
                .setParameterId(parameterId)
                .setValue(value)
                .build();
    }

    public static ReadRequest buildReadRequest(String entityType, String entityId, String parameterId) {
        return buildReadRequest(DEFAULT_MESSAGE_BLOCK, entityType, entityId, parameterId, null, null);
    }

    public static ReadRequest buildReadRequest(int blockId, String entityType, String entityId, String parameterId) {
        return buildReadRequest(blockId, entityType, entityId, parameterId, null, null);
    }

    public static ReadRequest buildReadRequest(int blockId, String entityType, String entityId, String parameterId,
                                               Integer version, Integer timestamp) {
        ReadRequest.Builder builder = ReadRequest.newBuilder()
                .setMessageType(Constants.READ_REQUEST_MESSAGE_TYPE)
                .setMessageBlock(DEFAULT_MESSAGE_BLOCK)
                .setEntityType(entityType)
                .setEntityId(entityId)
                .setParameterId(parameterId);

        if (version != null) {
            builder.setVersion(version);
        }

        if (timestamp != null) {
            builder.setTimestamp(timestamp);
        }

        return builder.build();
    }

    public static DeleteRequest buildDeleteRequest(String entityType, String entityId) {
        return buildDeleteRequest(DEFAULT_MESSAGE_BLOCK, entityType, entityId);
    }

    public static DeleteRequest buildDeleteRequest(int blockId, String entityType, String entityId) {
        return DeleteRequest.newBuilder()
                .setMessageType(Constants.DELETE_REQUEST_MESSAGE_TYPE)
                .setMessageBlock(blockId)
                .setEntityType(entityType)
                .setEntityId(entityId).build();
    }


    public static SelectRequest buildSelectRequest(String entityType, List<QueryItem> filtes) {
        return buildSelectRequest(DEFAULT_MESSAGE_BLOCK, null, entityType, filtes, null, null);
    }

    public static SelectRequest buildSelectRequest(String locale, String entityType, List<QueryItem> filtes) {
        return buildSelectRequest(DEFAULT_MESSAGE_BLOCK, locale, entityType, filtes, null, null);
    }

    public static SelectRequest buildSelectRequest(int blockId, String locale, String entityType,
                                                   List<QueryItem> filtes) {
        return buildSelectRequest(blockId, locale, entityType, filtes, null, null);
    }

    public static SelectRequest buildSelectRequest(int blockId, String locale, String entityType, List<QueryItem> filtes,
                                                   Integer startTime, Integer endTime) {
        SelectRequest.Builder builder = SelectRequest.newBuilder();

        builder.setMessageType(Constants.SELECT_REQUEST_MESSAGE_TYPE)
                .setMessageBlock(blockId)
                .setEntityType(entityType).addAllFilter(filtes);

        if (locale != null) {
            builder.setLocale(locale);
        }

        if (startTime != null) {
            builder.setStartTime(startTime);
        }

        if (endTime != null) {
            builder.setEndTime(endTime);
        }

        return builder.build();
    }

    public static QueryItem buildQueryItem(String attributeId, Operation op, ParameterValue value) {
        return QueryItem.newBuilder().setAttributeId(attributeId).setOperation(op).setValue(value).build();
    }

    public static ParameterValue buildParameterValue(String value) {
        return ParameterValue.newBuilder().setStringValue(value).build();
    }

    public static ParameterValue buildParameterValue(Double value) {
        return ParameterValue.newBuilder().setDoubleValue(value).build();
    }

    public static ParameterValue buildParameterValue(Long value) {
        return ParameterValue.newBuilder().setLongValue(value).build();
    }

    public static ParameterValue buildParameterValue(MUIString value) {
        return ParameterValue.newBuilder().setMuistringValue(value).build();
    }

    public static ParameterValue buildParameterValue(ObjectLink value) {
        return ParameterValue.newBuilder().setObjectLinkValue(value).build();
    }

    public static ParameterValue buildParameterValue(ByteString value) {
        return ParameterValue.newBuilder().setFileValue(value).build();
    }

    public static ParameterValue buildParameterValue(Integer value) {
        return ParameterValue.newBuilder().setIntValue(value).build();
    }

    public static ParameterValue buildParameterValueDate(Long value) {
        return ParameterValue.newBuilder().setDatetimeValue(value).build();
    }

    public static ParameterValue buildParameterValue(Boolean value) {
        return ParameterValue.newBuilder().setBooleanValue(value).build();
    }

    public static ParameterValue buildParameterValue(ValueArray value) {
        return ParameterValue.newBuilder().setValueArray(value).build();
    }

    public static ParameterValue buildParameterValue(Collection<String> links) {
        final List<ParameterValue> linksValue = new ArrayList<>(links.size());
        for (String link : links) {
            linksValue.add(buildParameterValue(link));
        }

        final ValueArray valueArray = buildValueArray(linksValue);
        return buildParameterValue(valueArray);
    }

    public static ValueArray buildValueArray(List<ParameterValue> values) {
        return ValueArray.newBuilder()
                .addAllValues(values).build();
    }

    public static ByteString buildByteString(byte[] bytes) {
        return ByteString.copyFrom(bytes);
    }

    public static ObjectLink buildObjectLink(List<String> links) {
        return ObjectLink.newBuilder()
                .addAllLinks(links).build();
    }

    public static MUIString buildMUIString(Map<String, String> muiValues) {
        MUIString.Builder builder = MUIString.newBuilder();

        for (String langId : muiValues.keySet()) {
            String muiString = muiValues.get(langId);
            MUIString.MUIValue muiValue = MUIString.MUIValue.newBuilder().setLangID(langId).setValue(muiString).build();
            builder.addStrValues(muiValue);
        }

        return builder.build();
    }

    public static String getMUIValue(String locale, MUIString muiString) {
        for (MUIString.MUIValue muiValue : muiString.getStrValuesList()) {
            if (muiValue.getLangID().equals(locale)) {
                return muiValue.getValue();
            }
        }
        return null;
    }

    public static List<String> extractObjectLinks(ParameterValue value) {
        if (!value.hasObjectLinkValue()) {
            return Collections.emptyList();
        } else {
            return value.getObjectLinkValue().getLinksList();
        }
    }

    public static String extractObjectLink(ParameterValue value) {
        List<String> links = extractObjectLinks(value);
        if (links.isEmpty()) {
            return null;
        }

        return links.get(0);
    }
}
