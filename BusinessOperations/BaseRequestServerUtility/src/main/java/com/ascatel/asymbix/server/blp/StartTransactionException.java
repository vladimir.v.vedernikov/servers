/**
 * Class StartTransactionException
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 29.04.13
 */
package com.ascatel.asymbix.server.blp;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class StartTransactionException extends RuntimeException {

    public StartTransactionException() {
    }

    public StartTransactionException(String message) {
        super(message);
    }

    public StartTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public StartTransactionException(Throwable cause) {
        super(cause);
    }

    public StartTransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
