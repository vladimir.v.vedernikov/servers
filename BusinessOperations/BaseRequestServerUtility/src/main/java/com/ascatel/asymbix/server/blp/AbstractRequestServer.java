/**
 * Copyright © 2011 ООО "Омникомм Технологии"
 *
 * User: Sergey.Sitishev
 * Date: 02.07.12
 */
package com.ascatel.asymbix.server.blp;

import com.ascatel.asymbix.server.blp.op.AsyncOperation;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.CaughtException;
import ru.omnicomm.pegasus.processingPlatform.messages.Init;
import ru.omnicomm.pegasus.processingPlatform.messages.Signal;
import ru.omnicomm.pegasus.processingPlatform.messages.Terminate;
import ru.omnicomm.pegasus.processingPlatform.messages.TimerEvent;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ErrorMessage;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CRUDResponse;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;

import com.ascatel.asymbix.server.blp.messaging.HeaderParser;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeServiceException;

/**
 * @author Sergey.Sitishev (sitishev@omnicomm.ru)
 */
public abstract class AbstractRequestServer implements ServerImplementation {

    private static Logger LOGGER = LoggerFactory.getLogger();

    private JmsBridgeService jmsBridge;

    private BusinessLogicService blService;

    private Server self;

    private Handler handler;

    private Environment environment;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    public AbstractRequestServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        this.jmsBridge = jmsBridge;
        this.blService = blService;
    }

    @Override
    public void onMessage(Server source, MessageLite message) {

        final byte[] data = message.toByteArray();
        HeaderParser.MessageHeader header;
        try {
            header = HeaderParser.MessageHeader.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.INFO, "Invalid message.", e);
            return;
        }

        try {
            long messageType = header.getMessageType();
            if (messageType == getRequestMessageType()) {
                processRequest(message);
            } else if (messageType == Constants.ERROR_MESSAGE_TYPE) {
                processError(data);
            } else if (messageType == Constants.CRUD_RESPONSE_MESSAGE_TYPE) {
                processCRUD(data);
            } else if (messageType == Constants.RESULT_SET_MESSAGE_TYPE) {
                processResultSet(data);
            } else {
                LOGGER.log(Level.INFO, "Invalid message type=" + messageType);
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Error while message processing", e);
        }
    }

    //    @Override
    public void onSignal(Signal signal) {
        final Class serverType = getClass();

        signal.accept(new Signal.EmptyVisitor<RuntimeException>() {
            @Override
            public void visit(CaughtException signal) {
                Throwable error = signal.getException();
                LOGGER.log(Level.ERROR, null, error);
            }

            @Override
            public void visit(Init signal) {
                self = signal.getSelf();
                handler = signal.getHandler();

                jmsBridge.registerServer(serverType, self, getRequestMessageType());

                environment = new Environment(self, getJmsBridge(), getBlService());

                initServer(self, handler);
            }

            @Override
            public void visit(Terminate signal) {
                jmsBridge.unregisterServer(serverType, self);
            }

            @Override
            public void visit(TimerEvent signal) {
                processTimerEvent(signal);
            }
        });
    }

    /**
     * Предназначен для тестов и отладки.
     */
    protected void processTimerEvent(TimerEvent signal) {

    }

    /**
     * Предназначен для тестов и отладки.
     */
    protected void initServer(Server server, Handler handler) {
        //empty
    }

    private void processError(byte[] bytes) {
        try {
            final ErrorMessage errMsg = ErrorMessage.parseFrom(bytes);
            processError(errMsg);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.INFO, "Invalid error message.", e);
        }
    }

    private void processCRUD(byte[] bytes) {
        try {
            final CRUDResponse crud = CRUDResponse.parseFrom(bytes);
            processCRUD(crud);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.INFO, "Invalid crudResponse message.", e);
        }
    }

    protected void processResultSet(byte[] bytes) {
        try {
            final ResultSet resultSet = ResultSet.parseFrom(bytes);
            processResultSet(resultSet);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.log(Level.INFO, "Invalid crudResponse message.", e);
        }
    }

    protected void processError(ErrorMessage errMsg) {
        final String blpInstanceId = errMsg.getInstanceId();
        final AsyncOperation operation = getOperation(blpInstanceId);
        if (operation != null) {
            operation.handleErrorResponse(errMsg);
        }
    }

    protected void processCRUD(CRUDResponse crud) {
        final String blpInstanceId = crud.getInstanceId();
        final AsyncOperation operation = getOperation(blpInstanceId);
        if (operation != null) {
            operation.handleCRUDResponse(crud);
        }
    }

    protected void processResultSet(ResultSet resultSet) {
        final String blpInstanceId = resultSet.getInstanceId();
        final AsyncOperation operation = getOperation(blpInstanceId);
        if (operation != null) {
            operation.handleSelectResponse(resultSet);
        }
    }

    private AsyncOperation getOperation(String blpInstanceId) {
        final AsyncOperation operation = environment.getOperation(blpInstanceId);
        if (operation == null) {
            LOGGER.log(Level.WARN, "AsyncOperation for blpInstanceId=" + blpInstanceId + " was not found.");
        } else {
            environment.removeOperation(blpInstanceId);
        }
        return operation;
    }

    /**
     * Начинаем транзакцию.
     *
     * @throws StartTransactionException ошибки при выполнении операции.
     */
    public void startTransaction() {
        try {
            getEnvironment().getBlService().startTransaction();
        } catch (BusinessLogicServiceException e) {
            throw new StartTransactionException("Start transaction error.", e);
        }
    }

    /**
     * Применяем изменения.
     *
     * @throws CommitTransactionException ошибки при выполнении операции или conflict при коммите.
     */
    public void commitTransaction() {
        try {
            final BusinessLogicService.CommitStatus status = getEnvironment().getBlService().commitTransaction();
            if (BusinessLogicService.CommitStatus.SUCCESS != status) {
                throw new CommitTransactionException("Commit CONFLICT.");
            }
        } catch (BusinessLogicServiceException e) {
            throw new CommitTransactionException("Commit transaction error.", e);
        }
    }

    /**
     * Отменить изменения.
     *
     * @throws AbortTransactionException ошибки при выполнении операции.
     */
    public void abortTransaction() {
        try {
            getEnvironment().getBlService().abortTransaction();
        } catch (BusinessLogicServiceException e) {
            throw new AbortTransactionException("Abort transaction error.", e);
        }
    }

    public void sendResponse(MessageLite response) {
        try {
            getEnvironment().getJmsBridge().send(response);
        } catch (JmsBridgeServiceException e) {
            LOGGER.log(Level.INFO, "Cannot send response message to jmsBridge", e);
        }
    }

    protected abstract void processRequest(MessageLite request)
            throws InvalidProtocolBufferException;

    public JmsBridgeService getJmsBridge() {
        return jmsBridge;
    }

    public BusinessLogicService getBlService() {
        return blService;
    }

    public Environment getEnvironment() {
        return environment;
    }

    //    protected void logAcceptedMessage(SendRequestParser.SendRequest requestMsg) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("Request ACCEPTED. ").append(requestMsg.getInstanceId())
//                .append("/").append(requestMsg.getRequestedMessageType())
//                .append("/").append(requestMsg.getMessageType());
//        LOGGER.log(Level.INFO, sb.toString());
//    }
//
//    protected void logProcessedMessage(SendRequestParser.SendRequest request) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("Request is PROCESSED. ").append(request.getInstanceId())
//                .append("/").append(request.getRequestedMessageType())
//                .append("/").append(request.getMessageType());
//        LOGGER.log(Level.INFO, sb.toString());
//    }
//
//    protected void logErrorMessage(SendRequestParser.SendRequest request, String cause, Exception exception) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("Request is FAILED. ").append(request.getInstanceId())
//                .append("/").append(request.getRequestedMessageType())
//                .append("/").append(request.getMessageType())
//                .append(". Because of ").append(cause);
//        LOGGER.log(Level.WARN, sb.toString(), exception);
//    }

    protected abstract int getRequestMessageType();

}
