/**
 * Class OMIdentifiers
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 16.04.13
 */
package com.ascatel.asymbix.server.blp;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class OMIdentifiers {

    /**
     * SoureKey.
     */
    public static final String SID_ENTITY_TYPE = "1200";


    ////////////////////////////////////////////////////////////////////
    ///////////////////////// Message Type /////////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Тип сообщения. (Message Type)
     */
    public static final String MT_ENTITY_TYPE = "1300";

    /**
     * Указывает, является ли сообщение событием.
     */
    public static final String MT_IS_EVENT_PARAMETER_ID = "1305";


    ////////////////////////////////////////////////////////////////////
    //////////////////////// Simple Settings ///////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Настройка (Simple Settings).
     */
    public static final String SIMPLE_SETTING_ENTITY_TYPE = "1400";

    /**
     * Ключ.
     */
    public static final String SIMPLE_SETTING_KEY_PARAMETER_ID = "1401";

    /**
     * Значение.
     */
    public static final String SIMPLE_SETTING_VALUE_PARAMETER_ID = "1402";


    ////////////////////////////////////////////////////////////////////
    ////////////////////// Controlled Parameter ////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Контролируемый параметр.
     */
    public static final String CP_ENTITY_TYPE = "1500";


    ////////////////////////////////////////////////////////////////////
    //////////////////////// Controlled Unit ///////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Агрегат (Controlled Unit).
     */
    public static final String CU_ENTITY_TYPE = "1550";

    /**
     * Атрибут для связи с Controlled Unit Type.
     */
    public static final String CU_BIND_CUT_PARAMETER_ID = "1551";

    /**
     * Атрибут для связи с объектом {@link #CO_ENTITY_TYPE} ('Контролируемый объект').
     */
    public static final String CU_BIND_CO_PARAMETER_ID = "1553";


    ////////////////////////////////////////////////////////////////////
    ///////////////////////// Atomic Sensor ////////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Логический датчик (Atomic Sensor).
     */
    public static final String AS_ENTITY_TYPE = "1750";

    /**
     * Уникальный идентификатор датчика, (например, заводской или серийный номер).
     */
    public static final String AS_NUMBER_PARAMETER_ID = "1751";

    /**
     * Датчик. Атрибут для связи с {@link #IS_ENTITY_TYPE}.
     */
    public static final String AS_BIND_IS_PARAMETER_ID = "1752";

    /**
     * Атрибут для связи регистратора с SourceId.
     */
    public static final String AS_BIND_SOURCE_PARAMETER_ID = "1753";

    /**
     * Контролируемый параметр. Атрибут для связи с объектом Controlled Parameter. Указывает разрешённые
     * контролируемый параметр который измеряется логическим датчиком.
     */
    public static final String AS_BIND_CP_PARAMETER_ID = "1754";

    /**
     * Атрибут для связи с объектом {@link #RDID_ENTITY_TYPE}.
     */
    public static final String AS_BIND_DTID_PARAMETER_ID = "1755";


    ////////////////////////////////////////////////////////////////////
    ///////////////////// Controlled Object ////////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Контролируемый объект.
     */
    public static final String CO_ENTITY_TYPE = "1800";

    /**
     * Связь с объектом CO Category.
     */
    public static final String CO_BIND_CAT_PARAMETER_ID = "1802";


    ////////////////////////////////////////////////////////////////////
    /////////////////////// CO Category  ///////////////////////////////
    ////////////////////////////////////////////////////////////////////

    /**
     * Категория (группы) контролируемого объекта.
     */
    public static final String COC_ENTITY_TYPE = "1870";

    /**
     * Связь с объектом {@link #CO_ENTITY_TYPE} (Controlled Object).
     */
    public static final String COC_BIND_CO_PARAMETER_ID = "1872";


    ////////////////////////////////////////////////////////////////////
    //////////////////// CPMessageTypeExtension ////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Расширение для типа сообщения (CPMessageTypeExtension).
     */
    public static final String CPMTE_ENTITY_TYPE = "2000";

    /**
     * Ссылка на соответствующий контролируемый параметр.
     */
    public static final String CPMTE_BIND_CP_PARAMETER_ID = "2001";

    /**
     * Ссылка на соответствующий тип сообщения {@link #MT_ENTITY_TYPE}.
     */
    public static final String CPMTE_BIND_MT_PARAMETER_ID = "2002";

    /**
     * Название типа сообщения, специфическое для данного контролируемого параметра.
     */
    public static final String CPMTE_NAME_PARAMETER_ID = "2004";

    /**
     * Пиктограмма, соответствующая сообщению (если применимо).
     */
    public static final String CPMTE_IMAGE_PARAMETER_ID = "2005";


    ////////////////////////////////////////////////////////////////////
    ////////// Controlled Parameter Processing Setting /////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Параметр сервера (Controlled Parameter Processing Setting).
     */
    public static final String CPPS_ENTITY_TYPE = "2020";

    /**
     * Внутреннее имя для обращения к настройке по имени из сервера.
     */
    public static final String CPPS_NAME_PARAMETER_ID = "2021";

    /**
     * Указывается тип настройки (Number=1, Datetime=2, String=3, Table=4, Object=5).
     */
    public static final String CPPS_TYPE_PARAMETER_ID = "2023";

    /**
     * Массив дополнительных параметров.
     */
    public static final String CPPS_VALUES_RANGE_PARAMETER_ID = "2024";

    /**
     * Является ли настройка глобальной. Если да, то её значение должно быть одинаковым для всех типов и источников
     * сообщений, связанных с данным КО.
     */
    public static final String CPPS_IS_GLOBAL_PARAMETER_ID = "2027";

    /**
     * Данный признак, указывает является ли настройка уникальной на уровне CP, т.е. не зависит от конкретных датчиков,
     * измеряющих данный параметр. Например: настройка Порог слива является уникальной на уровне CP,
     * а настройка Калибровочная таблица таковой не является, т.к. настраивается отдельно для каждого датчика.
     */
    public static final String CPPS_IS_SENSINDEP_PARAMETER_ID = "2028";

    /**
     * Класс связанных объектов. Атрибут заполняется только для настроек, значениями которых является ссылка на
     * некоторый объект.
     */
    public static final String CPPS_BINDED_CLASS_PARAMETER_ID = "2029";


    ////////////////////////////////////////////////////////////////////
    ////////// Controlled Parameter Processing Setting Value ///////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Значение параметра сервера (Controlled Parameter Processing Setting Value).
     */
    public static final String CPPSV_ENTITY_TYPE = "2200";

    /**
     * Значение соответствующего параметра.
     */
    public static final String CPPSV_VALUE_PARAMETER_ID = "2201";

    /**
     * Множество простых настроек {@link #SIMPLE_SETTING_ENTITY_TYPE}
     */
    public static final String CPPSV_BIND_SIMP_SET_PARAMETER_ID = "2202";

    /**
     * Атрибут для связи с объектом {@link #CPPS_ENTITY_TYPE}.
     */
    public static final String CPPSV_BIND_CPPS_PARAMETER_ID = "2203";

    /**
     * Атрибут для связи с объектом {@link #AS_ENTITY_TYPE}.
     */
    public static final String CPPSV_BIND_AS_PARAMETER_ID = "2204";


    ////////////////////////////////////////////////////////////////////
    //////////////////////////// Registrator ///////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Абонентский терминал (Registrator).
     */
    public static final String R_ENTITY_TYPE = "3000";

    /**
     * Идентификатор регистратора в формате производителя (этот KEY присылает регистратор при обмене данными).
     */
    public static final String R_REG_NUM_PARAMETER_ID = "3001";

    /**
     * Атрибут для связи регистратора с SourceID.
     */
    public static final String R_BIND_SID_PARAMETER_ID = "3002";

    /**
     * Атрибут для связи с объектом {@link #RT_ENTITY_TYPE}.
     */
    public static final String R_BIND_RT_PARAMETER_ID = "3003";


    /**
     * Атрибут для связи с объектом {@link #CO_ENTITY_TYPE}.
     */
    public static final String R_BIND_CO_PARAMETER_ID = "3004";


    ////////////////////////////////////////////////////////////////////
    ///////////////////////// Registrator Type /////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Тип регистратора (Registrator Type).
     */
    public static final String RT_ENTITY_TYPE = "3010";


    ////////////////////////////////////////////////////////////////////
    /////////////////////// Registrator Data Id ////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Элемент данных регистратора (RegistratorDataId).
     */
    public static final String RDID_ENTITY_TYPE = "3020";

    /**
     * Атрибут для связи с объектом {@link #R_ENTITY_TYPE}.
     */
    public static final String RDID_BIND_REG_PARAMETER_ID = "3024";


    ////////////////////////////////////////////////////////////////////
    ///////////////////////// Installed Sensor /////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Установленный датчик (Installed Sensor).
     */
    public static final String IS_ENTITY_TYPE = "3050";

    /**
     * Уникальный идентификатор датчика, (например, заводской или серийный номер). Однако допускается отсутствие
     * задания этого параметра.
     */
    public static final String IS_SENSOR_NUM_PARAMETER_ID = "3051";

    /**
     * Абонентский терминал. Атрибут для связи с объектом {@link #R_ENTITY_TYPE}.
     */
    public static final String IS_BIND_REG_PARAMETER_ID = "3053";

    /**
     * Атрибут для связи с объектом {@link #ST_ENTITY_TYPE}. Указывает тип датчика, который устанавливается.
     */
    public static final String IS_BIND_ST_PARAMETER_ID = "3054";

    /**
     * Атрибут для связи с объектом {@link #CU_ENTITY_TYPE}. Указывает агрегат, в который установлен датчик.
     */
    public static final String IS_BIND_CU_PARAMETER_ID = "3055";


    ////////////////////////////////////////////////////////////////////
    /////////////////////////// Sensor Mode ////////////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Тип датчика (Sensor Type).
     */
    public static final String ST_ENTITY_TYPE = "3060";

    /**
     * Тип датчика. 1 – Стандартный датчик, 2 – Исполнительное устройство.
     */
    public static final String ST_SENSOR_MODE_PARAMETER_ID = "3061";

    /**
     * Название типа датчика (например, 'ДУТ LLS').
     */
    public static final String ST_NAME_PARAMETER_ID = "3062";

    ////////////////////////////////////////////////////////////////////
    ////////////////// Assigned Registrator Command ////////////////////
    ////////////////////////////////////////////////////////////////////
    /**
     * Назначенная команда (Assigned Registrator Command).
     */
    public static final String ARC_ENTITY_TYPE = "3070";

    /**
     * Указывает ссылку на команду регистратора (класс RegistratorCommand), к которому относится данная
     * назначенная команда.
     */
    public static final String ARC_BIND_RC_PARAMETER_ID = "3071";

    /**
     * Атрибут для связи с {@link #IS_ENTITY_TYPE} ('Установленный датчик').
     */
    public static final String ARC_BIND_IS_PARAMETER_ID = "3072";

}
