/**
 * Class DeleteATOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 25.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.Environment;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;

/**
 * Операция 'Удалить регистратор'.
 * Операция удаляет регистратор (абонентский терминал). Регистратор может быть удалён, только если удалены все его
 * физические датчики (т.е. объекты Installed Sensor). Иначе операция вернёт ошибку.
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code regId} - идентификатор удаляемого объекта типа Registrator.</li>
 * <li>{@code referenceId} – идентификатор класса удаляемой сущности (должно быть равно идентификатору класса
 * Registrator).</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class DeleteATOperation extends AbstractOperation {

    public DeleteATOperation(AbstractOperation handler) {
        super(handler);
    }

    public DeleteATOperation(Environment env) {
        super(env);
    }

    public void deleteAt(String referenceId, String regId)
            throws BusinessLogicServiceException {

        // Проверяем, что ReferenceID совпадает с идентификатором класса Registrator, иначе поднимаем ошибку.
        if (!R_ENTITY_TYPE.equals(referenceId)) {
            final String errCause = new StringBuilder()
                    .append("Registrator entityType=").append(R_ENTITY_TYPE)
                    .append(" is not equal to parameter referenceId=").append(referenceId).toString();
            throw new BusinessLogicServiceException(errCause);
        }

        // Проверяем, что все датчики сняты с регистратора.
        checkIsISDeleted(regId);

        // Удаляем лишние объекты типа RegistratorDataId.
        deleteRDID(regId);

        // Убираем абонентский терминал из слоя подключений.
        unregisterRegistrator(regId);

        // Удаляем текущий регистратор.
        deleteRegistrator(regId);
    }

    private void deleteRegistrator(String regId)
            throws BusinessLogicServiceException {
        final ParameterValue bindSidValue = readParameterValue(R_ENTITY_TYPE, regId, R_BIND_SID_PARAMETER_ID);
        final List<String> sidIds = extractObjectLinks(bindSidValue);

        for (String sidId : sidIds) {
            // Удаляем из настроек (класс Simple Settings) все настройки с Key = SRC-<SId> (таким образом прекращён
            // приём сообщений с через сервер коммуникации, см. док. PSS-01).
            deleteSimpleSettings(sidId);

            // Вызываем операцию Удалить сущность (п. 6.2.2 док. BP-SRS-01). Передаём в неё в качестве входного
            // параметра SId.
            final DeleteRequest deleteRequest
                    = buildDeleteRequest(getBlockId(), SID_ENTITY_TYPE, sidId);
            getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
        }

        // Вызываем операцию Удалить сущность (п. 6.2.2 док. BP-SRS-01).
        // Передаём в неё в качестве входного параметра RegId.
        final DeleteRequest deleteRequest
                = buildDeleteRequest(getBlockId(), R_ENTITY_TYPE, regId);
        getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
    }

    private void deleteSimpleSettings(String sidId)
            throws BusinessLogicServiceException {
        final String key = "SRC-" + sidId;

        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(SIMPLE_SETTING_KEY_PARAMETER_ID, Operation.EQ, buildParameterValue(key)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), SIMPLE_SETTING_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Collection<String> simpleSettingsIds = new HashSet<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            simpleSettingsIds.add(parameter.getEntityId());
        }

        for (String settingId : simpleSettingsIds) {
            final DeleteRequest deleteRequest
                    = buildDeleteRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE, settingId);
            getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
        }
    }

    private void unregisterRegistrator(String regId) {
        //TODO IMPLEMENT
        // Формируем сообщение изменения настроек подключения со следующими параметрами:
        //   Заголовок:
        //     SourceID = значение настройки BLOCK_ID
        //     MessageType = UnregisterAT
        //     Timestamp = Текущее время
        //   Тело:
        //     RegistratorID
        //     RegistratorType

    }

    private void deleteRDID(String regId)
            throws BusinessLogicServiceException {
        // Ищем все объекты класса RegistratorDataId, установленные на данном регистраторе.
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(RDID_BIND_REG_PARAMETER_ID, Operation.EQ, buildParameterValue(regId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), RDID_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Collection<String> rdIds = new HashSet<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            rdIds.add(parameter.getEntityId());
        }


        for (String rdId : rdIds) {
            // Ищем все объекты класса Atomic Sensor, ссылающиеся на текущий dtID.
            checkAS2RDIDDeleted(rdId);

            // Выполняем шаги 6 - 9 стандартной операции удаления сущности.
            //TODO IMPLEMENT
            throw new UnsupportedOperationException("Выполняем шаги 6 - 9 стандартной операции удаления сущности");
        }


    }

    private void checkAS2RDIDDeleted(String rdId) throws BusinessLogicServiceException {
        // Ищем все объекты класса Installed Sensor, подключённые к данному регистратору.
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(AS_BIND_DTID_PARAMETER_ID, Operation.EQ, buildParameterValue(rdId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), AS_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        // Если предыдущий запрос вернул больше нуля записей, поднимаем ошибку.
        // (мы должны были удалить все датчики ассоциированные с данным регистратором ещё перед вызовом данной
        // операции, и, более того, успешно проверили это на шаге 2)
        if (resultSet.getParametersCount() > 0) {
            final String errMsg = new StringBuilder().append("RegistratorDataId ").append(rdId)
                    .append(" still have Atomic Sensors").toString();
            throw new BusinessLogicServiceException(errMsg);
        }
    }

    private void checkIsISDeleted(String regId)
            throws BusinessLogicServiceException {
        // Ищем все объекты класса Installed Sensor, подключённые к данному регистратору.
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(IS_BIND_REG_PARAMETER_ID, Operation.EQ, buildParameterValue(regId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), IS_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);
        // Если операция, выполненная на предыдущем шаге вернула хотя бы одно значение id объекта типа
        // Installed Sensor, поднимаем ошибку
        if (resultSet.getParametersCount() > 0) {
            final String errMsg = new StringBuilder().append("Registrator ").append(regId)
                    .append(" still have Installed Sensors").toString();
            throw new BusinessLogicServiceException(errMsg);
        }
    }

}
