/**
 * Class UpdateATServer
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 04.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.AbstractRequestServer;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser.UpdateATResponse;
import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser.UpdateATRequest;
import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser.State;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildObjectLink;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("UpdateATServer_Implementation")
public class UpdateATServer extends AbstractRequestServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    public static final int REQUEST_MESSAGE_TYPE = 437;

    public static final int RESPONSE_MESSAGE_TYPE = 438;

    private BlockService blockService;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public UpdateATServer(JmsBridgeService jmsBridge, BusinessLogicService blService, BlockService blockService) {
        super(jmsBridge, blService);
        this.blockService = blockService;
    }

    @Override
    protected void processRequest(MessageLite request)
            throws InvalidProtocolBufferException {
        final UpdateATRequest proto = UpdateATRequest.parseFrom(request.toByteString());
        final UpdateATOperation operation = new UpdateATOperation(getEnvironment(), blockService);

        operation.setInstanceId(proto.getInstanceId());
        operation.setLocale(null);
        operation.setBlockId(proto.getMessageBlock());

        final String referenceId = Long.toString(proto.getReferenceId());
        final String refDataId = proto.getRefDataId() == 0 ? null : Long.toString(proto.getRefDataId());
        final Map<String, String> reqParams = extractAndValidateParameters(proto.getAttributeValuesList());
        operation.updateAt(referenceId, refDataId, reqParams);
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    private Map<String, ParameterValue> extractParameters(List<UpdateATRequest.AttributeValue> attributes) {
        try {
            final Map<String, ParameterValue> params = new HashMap<>();
            for (UpdateATRequest.AttributeValue attribute : attributes) {
                final long attrId = attribute.getId();
                final UpdateAtParser.ParameterValue value = attribute.getValue();

                if (value.hasObjectLinkValue()) {
                    final UpdateAtParser.ObjectLink objectLink = value.getObjectLinkValue();
                    final List<String> newLinks = new ArrayList<>(objectLink.getLinksCount());
                    for (Long link : objectLink.getLinksList()) {
                        newLinks.add(link.toString());
                    }

                    final ObjectLink newObjectLink = buildObjectLink(newLinks);
                    params.put(Long.toString(attrId), buildParameterValue(newObjectLink));
                } else {
                    final ParameterValue newValue = ParameterValue.parseFrom(value.toByteString());
                    params.put(Long.toString(attrId), newValue);
                }

            }
            return params;
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException("Invalid ParameterValue format", e);
        }
    }

    private Map<String, String> extractAndValidateParameters(List<UpdateATRequest.AttributeValue> attributes) {
        final Map<String, ParameterValue> params = extractParameters(attributes);

        final Map<String, String> simpleParams = new HashMap<>();

        final String rtEntityId = extractRTEntityId(params.get(R_BIND_RT_PARAMETER_ID));
        simpleParams.put(R_BIND_RT_PARAMETER_ID, rtEntityId);

        final String regNumber = extractRegNumber(params.get(R_REG_NUM_PARAMETER_ID));
        simpleParams.put(R_REG_NUM_PARAMETER_ID, regNumber);

        final String coEntityId = extractCOEntityId(params.get(R_BIND_CO_PARAMETER_ID));
        simpleParams.put(R_BIND_CO_PARAMETER_ID, coEntityId);

        return simpleParams;
    }

    private String extractRTEntityId(ParameterValue rtEntityIdValue) {
        if (rtEntityIdValue == null) {
            throw new RuntimeException("Request parameters doesn't not contain BindRT value");
        }

        if (rtEntityIdValue.hasLongValue()) {
            return Long.toString(rtEntityIdValue.getLongValue());
        } else if (rtEntityIdValue.hasObjectLinkValue()) {
            final ObjectLink objectLink = rtEntityIdValue.getObjectLinkValue();
            final List<String> links = objectLink.getLinksList();
            if (links.size() > 0) {
                return links.get(0);
            } else {
                throw new RuntimeException("ObjectLink is empty. Cannot get RT identifier.");
            }
        } else if (rtEntityIdValue.hasStringValue()) {
            return rtEntityIdValue.getStringValue();
        } else {
            throw new RuntimeException("Cannot get RT identifier.");
        }
    }

    private String extractCOEntityId(ParameterValue parameterValue) {
        if (parameterValue == null) {
            throw new RuntimeException("Request parameters doesn't not contain BindCO value");
        }

        if (parameterValue.hasLongValue()) {
            return Long.toString(parameterValue.getLongValue());
        } else if (parameterValue.hasObjectLinkValue()) {
            final ObjectLink objectLink = parameterValue.getObjectLinkValue();
            final List<String> links = objectLink.getLinksList();
            if (links.size() > 0) {
                return links.get(0);
            } else {
                throw new RuntimeException("ObjectLink is empty. Cannot get CO identifier.");
            }
        } else if (parameterValue.hasStringValue()) {
            return parameterValue.getStringValue();
        } else {
            throw new RuntimeException("Cannot get CO identifier.");
        }
    }

    private String extractRegNumber(ParameterValue regNumberValue) {
        if (regNumberValue == null) {
            throw new RuntimeException("Request parameters doesn't not contain RegNum value");
        }

        if (regNumberValue.hasStringValue()) {
            return regNumberValue.getStringValue();
        } else {
            throw new RuntimeException("Cannot get RegNumber value.");
        }
    }

    public static class ResponseHelper {
        public static MessageLite getDefectiveResponse(String instanceId, String errMsg) {
            UpdateATResponse.Builder builder = getResponseBuilder(0, instanceId);
            builder.setBlockState(State.DEFECTIVE).setErrorMessage(errMsg);

            return builder.build();
        }

        public static MessageLite getEmptyResponse(String instanceId) {
            UpdateATResponse.Builder builder = getResponseBuilder(0, instanceId);
            builder.setBlockState(State.LAST);
            return builder.build();
        }

        public static UpdateATResponse.Builder getResponseBuilder(int blockNumber, String instanceId) {
            UpdateATResponse.Builder builder = UpdateATResponse.newBuilder();

            builder.setMessageType(RESPONSE_MESSAGE_TYPE)
                    .setInstanceId(instanceId)
                    .setBlockNumber(blockNumber)
                    .setRefDataId(0);

            return builder;
        }
    }

//    //FOR DEBUG ONLY!
//    @Override
//    protected void initServer(Server server, Handler handler) {
//        handler.setTimer("1", Handler.TimerType.DELAY_BASED, 1000L, 0L);
//        handler.setTimer("2", Handler.TimerType.DELAY_BASED, 5000L, 0L);
//    }
//
//    @Override
//    protected void processTimerEvent(TimerEvent signal) {
//        if ("1".equals(signal.id())) {
//            List<UpdateATRequest.AttributeValue> attrValues = new ArrayList<>();
//            attrValues.add(UpdateATRequest.AttributeValue.newBuilder()
//                .setId(3003).setValue(UpdateAtParser.ParameterValue.newBuilder().setLongValue(14100).build()).build());
////                    .setId(3003).setValue(UpdateAtParser.ParameterValue.newBuilder().setLongValue(88888888).build()).build());
//            attrValues.add(UpdateATRequest.AttributeValue.newBuilder()
////                .setId(3004).setValue(UpdateAtParser.ParameterValue.newBuilder().setLongValue(10401).build()).build());
//                .setId(3004).setValue(UpdateAtParser.ParameterValue.newBuilder().setLongValue(10401).build()).build());
////                    .setId(3004).setValue(UpdateAtParser.ParameterValue.newBuilder().setLongValue(99999999).build()).build());
//            attrValues.add(UpdateATRequest.AttributeValue.newBuilder()
////                .setId(3001).setValue(UpdateAtParser.ParameterValue.newBuilder().setStringValue("1BABA1").build()).build());
////                    .setId(3001).setValue(UpdateAtParser.ParameterValue.newBuilder().setStringValue("6666aassdf").build()).build());
//                  .setId(3001).setValue(UpdateAtParser.ParameterValue.newBuilder().setStringValue("1BABA2").build()).build());
//
//            final UpdateATRequest request = UpdateATRequest.newBuilder()
//                    .setMessageType(REQUEST_MESSAGE_TYPE)
//                    .setMessageBlock(1)
//                    .setInstanceId("12345")
//                    .setReferenceId(3000)
////                    .setRefDataId(0)
//                    .setRefDataId(14000)
//                    .addAllAttributeValues(attrValues).build();
//
//
//            try {
//                getEnvironment().getServer().send(null, request);
//            } catch (SendMessageException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            }
//        } else if ("2".equals(signal.id())) {
//            List<BLSMessageParser.QueryItem> filter = new ArrayList<>();
//            BLSMessageParser.SelectRequest select = MessageUtil.buildSelectRequest(UpdateATHandler.R_ENTITY_TYPE, filter);
//
//            try {
//                byte[] data = getEnvironment().getBlService().selectByParams(select.toByteArray());
//                BLSMessageParser.ResultSet resultSet = BLSMessageParser.ResultSet.parseFrom(data);
//                StringBuilder sb = new StringBuilder("\n");
//                for (BLSMessageParser.Parameter parameter : resultSet.getParametersList()) {
//                    if (UpdateATHandler.R_REG_NUM_PARAMETER_ID.equals(parameter.getAttributeId())) {
//                        sb.append(parameter.getEntityType()).append(" | ")
//                                .append(parameter.getEntityId()).append(" | ")
//                                .append(parameter.getAttributeId()).append(" | ")
//                                .append(parameter.getValue().getStringValue()).append("\n");
//                    }
//                }
//                LOGGER.log(Level.WARN, sb.toString());
//            } catch (BusinessLogicServiceException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            } catch (InvalidProtocolBufferException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            }
//        }
//    }


}
