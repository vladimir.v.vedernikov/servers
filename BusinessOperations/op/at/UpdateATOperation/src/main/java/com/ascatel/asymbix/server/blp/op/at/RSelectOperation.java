/**
 * Class RegistratorSelectHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 05.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.Environment;

import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ErrorMessage;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import java.util.ArrayList;
import java.util.List;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildQueryItem;
import static ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService.ENTITY_ID_ATTRIBUTE;

/**
 * Внутренняя операция, которая ищет в системе сущность класса Registrator c идентификатором registratorID.
 * Выpывается из {@link UpdateATOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class RSelectOperation extends AbstractOperation {

    private String rEntityId;

    private String rtEntityId;

    private String regNumber;

    private String coEntityId;

    private BlockService blockService;

    public RSelectOperation(AbstractOperation operation, BlockService blockService) {
        super(operation);
        this.blockService = blockService;
    }

    public RSelectOperation(Environment env) {
        super(env);
    }

    public void selectRegistrator(String rEntityId, String rtEntityId, String coEntityId, String regNumber) {

        // 5.a Ищем в системе сущность класса Registrator c идентификатором RegistratorID. Если не найдена поднимаем
        // ошибку и завершаем операцию.

        // 5.b Считываем значения атрибутов BindRT и BindCO считанной на предыдущем шаге сущности и сравниваем их
        // соответственно с входящими параметрами RegistratorTypeId и IdCO. Если хотя бы одно из сравнений выявило
        // несоответствие, поднимаем ошибку (нельзя менять тип регистратора и КО, на котором он установлен) и
        // завершаем операцию.

        this.rEntityId = rEntityId;
        this.rtEntityId = rtEntityId;
        this.regNumber = regNumber;
        this.coEntityId = coEntityId;

        final SelectRequest selectRequest = buildCheckRExistenceRequest(rEntityId, rtEntityId, coEntityId);
        try {
            doSelect(selectRequest);
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());
            final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
            sendResponse(message);
        }
    }


    @Override
    public void handleErrorResponse(ErrorMessage errMsg) {
        super.handleErrorResponse(errMsg);

        final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
        sendResponse(message);
    }

    @Override
    public void handleSelectResponse(ResultSet resultSet) {
        try {
            final List<Parameter> parametersList = resultSet.getParametersList();
            final String oldRegNumber = parseOldRegNumber(parametersList);
            final String sid = parseSID(parametersList);

            if (regNumber == null ? oldRegNumber == null : regNumber.equals(oldRegNumber)) {
                // there is nothing to change
                final RNotifyOperation notifyOperation = new RNotifyOperation(this, blockService);
                notifyOperation.notify(rEntityId, rtEntityId, sid, regNumber, oldRegNumber);
            } else {
                final RValidateRegNumOperation validateOperation = new RValidateRegNumOperation(this, blockService);
                validateOperation.validateRegNumber(rEntityId, rtEntityId, sid, regNumber, oldRegNumber);
            }
        } catch (RuntimeException e) {
            addErrorMessage(e.getMessage());
            final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
            sendResponse(message);
        }
    }

    private SelectRequest buildCheckRExistenceRequest(String rEntityId, String rtEntityId, String coEntityId) {
        List<QueryItem> filter = new ArrayList<>();

        filter.add(buildQueryItem(ENTITY_ID_ATTRIBUTE, Operation.EQ, buildParameterValue(rEntityId)));
        filter.add(buildQueryItem(R_BIND_CO_PARAMETER_ID, Operation.EQ, buildParameterValue(coEntityId)));
        filter.add(buildQueryItem(R_BIND_RT_PARAMETER_ID, Operation.EQ, buildParameterValue(rtEntityId)));

        return MessageUtil.buildSelectRequest(getBlockId(), getLocale(), R_ENTITY_TYPE, filter);
    }

    private String parseOldRegNumber(List<Parameter> parameters) {
        if (parameters.isEmpty()) {
            final String errMsg = new StringBuilder().append("Entity Registrator with ID=").append(rEntityId)
                    .append(" and bindCo=").append(coEntityId)
                    .append(" and bindRT=").append(rtEntityId).append(" does not exist").toString();

            throw new RuntimeException(errMsg);
        } else {
            for (Parameter parameter : parameters) {
                if (R_REG_NUM_PARAMETER_ID.equals(parameter.getAttributeId())) {
                    return parameter.getValue().getStringValue();
                }
            }
            return null;
        }
    }

    private String parseSID(List<Parameter> parameters) {
        if (parameters.isEmpty()) {
            final String errMsg = new StringBuilder().append("Entity Registrator with ID=").append(rEntityId)
                    .append(" and bindCo=").append(coEntityId)
                    .append(" and bindRT=").append(rtEntityId).append(" does not exist").toString();

            throw new RuntimeException(errMsg);
        } else {
            for (Parameter parameter : parameters) {
                if (R_BIND_SID_PARAMETER_ID.equals(parameter.getAttributeId())) {
                    final List<String> sids = parameter.getValue().getObjectLinkValue().getLinksList();
                    if (sids.isEmpty()) {
                        return null;
                    } else {
                        return sids.get(0);
                    }
                }
            }
            return null;
        }
    }
}
