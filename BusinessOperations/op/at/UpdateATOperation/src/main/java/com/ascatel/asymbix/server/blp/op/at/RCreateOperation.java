/**
 * Class RegistratorCreateHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 05.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildObjectLink;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.UpdateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import java.util.Arrays;

/**
 * Внутренняя операция, которая выполняем создания регистратора послде проверки данных.
 * Выpывается из {@link RValidateDataOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class RCreateOperation extends AbstractOperation {

    private BlockService blockService;

    public RCreateOperation(AbstractOperation operation, BlockService blockService) {
        super(operation);
        this.blockService = blockService;
    }

    public void createRegistrator(String regNumber, String rtEntityId, String coEntityId) {

        final BusinessLogicService blService = getEnv().getBlService();

        try {
            blService.startTransaction();
        } catch (BusinessLogicServiceException e) {
            sendResponse(getDefectiveResponse(getInstanceId(), e.getMessage()));
            return;
        }

        try {
            // Создаём новую сущность класса Registrator.
            final CreateRequest createRRequest = buildCreateBlpRequest(R_ENTITY_TYPE);
            final String rEntityId = blService.createEntity(createRRequest.toByteArray());

            // Создаём новую сущность класса SourceID.
            final CreateRequest createSIDRequest = buildCreateBlpRequest(SID_ENTITY_TYPE);
            final String sidEntityId = blService.createEntity(createSIDRequest.toByteArray());

            // Создаём новую настройку (класс Simple Settings).
            final CreateRequest createSettingRequest = buildCreateBlpRequest(SIMPLE_SETTING_ENTITY_TYPE);
            final String settingEntityId = blService.createEntity(createSettingRequest.toByteArray());

            // Обновим параметры настройки.
            updateSettingParameters(settingEntityId, sidEntityId);

            // Устанавим значения атрибутов новой созданной сущности Registrator.
            updateRegistratorParameters(rEntityId, regNumber, rtEntityId, coEntityId, sidEntityId);

            try {
                final BusinessLogicService.CommitStatus commitStatus = blService.commitTransaction();
                if (BusinessLogicService.CommitStatus.CONFLICT == commitStatus) {
                    sendResponse(getDefectiveResponse(getInstanceId(), "CONFLICT commit"));
                } else {
                    //Отправим ответ.
                    final RNotifyOperation notifyOperation = new RNotifyOperation(this, blockService);
                    notifyOperation.notify(rEntityId, rtEntityId, sidEntityId, regNumber, null);
                }
            } catch (BusinessLogicServiceException e) {
                sendResponse(getDefectiveResponse(getInstanceId(), e.getMessage()));
            }
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());

            try {
                blService.abortTransaction();
            } catch (BusinessLogicServiceException e1) {
                addErrorMessage(e1.getMessage());
            }

            sendResponse(getDefectiveResponse(getInstanceId(), getErrorMessage()));
        }
    }

    private void updateRegistratorParameters(String rEntityId, String regNumber, String rtEntityId, String coEntityId,
                                             String sidEntityId)
            throws BusinessLogicServiceException {
//        i.	BindRT = RegistratorTypeId
//        ii.	RegNum = RNumber
//        iii.	BindSourceId = SId
//        iv.	BindCO = IdCO

        final ParameterValue regNumValue = buildParameterValue(regNumber);
        final ParameterValue bindSIDValue = buildParameterValue(buildObjectLink(Arrays.asList(sidEntityId)));
        final ParameterValue bindRTValue = buildParameterValue(buildObjectLink(Arrays.asList(rtEntityId)));
        final ParameterValue bindCOValue = buildParameterValue(buildObjectLink(Arrays.asList(coEntityId)));

        final UpdateRequest updRegNumRequest
                = buildUpdateBlpRequest(R_ENTITY_TYPE, rEntityId, R_REG_NUM_PARAMETER_ID, regNumValue);
        final UpdateRequest updSIDRequest
                = buildUpdateBlpRequest(R_ENTITY_TYPE, rEntityId, R_BIND_SID_PARAMETER_ID, bindSIDValue);
        final UpdateRequest updRTRequest
                = buildUpdateBlpRequest(R_ENTITY_TYPE, rEntityId, R_BIND_RT_PARAMETER_ID, bindRTValue);
        final UpdateRequest updCORequest
                = buildUpdateBlpRequest(R_ENTITY_TYPE, rEntityId, R_BIND_CO_PARAMETER_ID, bindCOValue);

        getEnv().getBlService().setParameterValue(updRegNumRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updSIDRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updRTRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updCORequest.toByteArray());
    }

    private void updateSettingParameters(String settingEntityId, String sidEntityId)
            throws BusinessLogicServiceException {
        //1.	key = SRC-<Sid>
        //2.	Value = 1
        final String key = new StringBuilder().append("SRC-").append(sidEntityId).toString();
        final ParameterValue keyValue = buildParameterValue(key);
        final UpdateRequest updKeyRequest = buildUpdateBlpRequest(SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_KEY_PARAMETER_ID, keyValue);

        final ParameterValue valueValue = buildParameterValue("1");
        final UpdateRequest updValueRequest = buildUpdateBlpRequest(SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_VALUE_PARAMETER_ID, valueValue);

        getEnv().getBlService().setParameterValue(updKeyRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updValueRequest.toByteArray());
    }

    private CreateRequest buildCreateBlpRequest(String entityType) {
        return MessageUtil.buildCreateRequest(getBlockId(), entityType);
    }

    private UpdateRequest buildUpdateBlpRequest(String entityType, String entityId, String parameterId,
                                                ParameterValue value) {
        return MessageUtil.buildUpdateRequest(getBlockId(), entityType, entityId, parameterId, value);
    }

}
