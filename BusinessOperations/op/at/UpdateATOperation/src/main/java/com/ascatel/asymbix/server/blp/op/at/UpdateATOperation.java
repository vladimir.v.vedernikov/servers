/**
 * Class ProcessRequestHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 04.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.Environment;

import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import java.util.Map;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getDefectiveResponse;

/**
 * Операция 'Создать/Обновить регистратор'.
 * Операция регистрирует на заданном КО новый абонентский терминал, либо обновляет параметры уже зарегистрированного
 * терминала (в частности номер регистратора).
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code referenceId} - идентификатор класса, сущность которого создаётся или обновляется.</li>
 * <li>{@code refDataId (registratorID)} – идентификатор регистратора (0, в случае если необходимо зарегистрировать
 * новый, в случае обновления – идентификатор обновляемого регистратора)</li>
 * <li>Массив значений атрибутов создаваемой или обновляемой сущности.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class UpdateATOperation extends AbstractOperation {

    private BlockService blockService;

    public UpdateATOperation(Environment env, BlockService blockService) {
        super(env);
        this.blockService = blockService;
    }

    public void updateAt(String referenceId, String refDataId, Map<String, String> reqParams) {

        if (!R_ENTITY_TYPE.equals(referenceId)) {
            final String errCause = new StringBuilder()
                    .append("Registrator entityType=").append(R_ENTITY_TYPE)
                    .append(" is not equal to parameter referenceId=").append(referenceId).toString();
            final MessageLite errMsg = getDefectiveResponse(getInstanceId(), errCause);
            sendResponse(errMsg);
            return;
        }

        if (refDataId == null) {
            createRegistrator(reqParams);
        } else {
            updateRegistrator(refDataId, reqParams);
        }
    }

    private void createRegistrator(Map<String, String> reqParams) {
        final RValidateDataOperation operation = new RValidateDataOperation(this, blockService);

        final String rtEntityId = reqParams.get(R_BIND_RT_PARAMETER_ID);
        final String regNumber = reqParams.get(R_REG_NUM_PARAMETER_ID);
        final String coEntityId = reqParams.get(R_BIND_CO_PARAMETER_ID);
        operation.validateData(rtEntityId, regNumber, coEntityId);
    }

    private void updateRegistrator(String refDataId, Map<String, String> reqParams) {
        final RSelectOperation operation = new RSelectOperation(this, blockService);

        final String rtEntityId = reqParams.get(R_BIND_RT_PARAMETER_ID);
        final String coEntityId = reqParams.get(R_BIND_CO_PARAMETER_ID);
        final String regNumber = reqParams.get(R_REG_NUM_PARAMETER_ID);
        operation.selectRegistrator(refDataId, rtEntityId, coEntityId, regNumber);
    }
}
