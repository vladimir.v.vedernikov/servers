/**
 * Class RegistratorValidateRegNumHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 08.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ErrorMessage;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import java.util.ArrayList;
import java.util.List;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildQueryItem;
import static ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService.ENTITY_ID_ATTRIBUTE;

/**
 * Внутренняя операция, которая провкряет корректность задания идентификатора регистратора {@code (RegNum)}.
 * Выpывается из {@link RSelectOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class RValidateRegNumOperation extends AbstractOperation {

    private String rEntityId;

    private String rtEntityId;

    private String sid;

    private String regNumber;

    private String oldRegNumber;

    private BlockService blockService;

    public RValidateRegNumOperation(AbstractOperation operation, BlockService blockService) {
        super(operation);
        this.blockService = blockService;
    }

    public void validateRegNumber(String rEntityId, String rtEntityId, String sid,
                                  String regNumber, String oldRegNumber) {
        this.rEntityId = rEntityId;
        this.rtEntityId = rtEntityId;
        this.sid = sid;
        this.regNumber = regNumber;
        this.oldRegNumber = oldRegNumber;

        // 5.d Проверяем корректность задания входного параметра RNumber: если RNumber задан (is not NULL),
        // ищем сущность класса Registrator, по условию RegNum = RNumber AND BindRT = RegistratorTypeId
        // AND EntityID != RegistratorID. Если такая сущность найдена (это означает, что в системе уже есть другой
        // абонентский терминал такого типа с таким номером), поднимаем ошибку и завершаем операцию.

        final SelectRequest selectRequest = buildCheckUniqueRegNumberRequest(rEntityId, rtEntityId, regNumber);
        try {
            doSelect(selectRequest);
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());
            final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
            sendResponse(message);
        }

    }

    @Override
    public void handleErrorResponse(ErrorMessage errMsg) {
        final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
        sendResponse(message);
    }

    @Override
    public void handleSelectResponse(ResultSet resultSet) {
        if (!resultSet.getParametersList().isEmpty()) {
            // Нашли регистратор с такимже regNumber - поднимаем ошибку.
            final String errMsg = new StringBuilder().append("RegNumber ").append(regNumber)
                    .append(" is not unique").toString();
            final MessageLite message = getDefectiveResponse(getInstanceId(), errMsg);
            sendResponse(message);
        } else {
            RUpdateOperation updateOperation = new RUpdateOperation(this, blockService);
            updateOperation.updateRegistrator(rEntityId, rtEntityId, sid, regNumber, oldRegNumber);
        }
    }

    private SelectRequest buildCheckUniqueRegNumberRequest(String rEntityId, String rtEntityId, String regNumber) {
        List<QueryItem> filter = new ArrayList<>();

        filter.add(buildQueryItem(ENTITY_ID_ATTRIBUTE, Operation.NOT_EQ, buildParameterValue(rEntityId)));
        filter.add(buildQueryItem(R_REG_NUM_PARAMETER_ID, Operation.EQ, buildParameterValue(regNumber)));
        filter.add(buildQueryItem(R_BIND_RT_PARAMETER_ID, Operation.EQ, buildParameterValue(rtEntityId)));

        return MessageUtil.buildSelectRequest(getBlockId(), getLocale(), R_ENTITY_TYPE, filter);
    }

}
