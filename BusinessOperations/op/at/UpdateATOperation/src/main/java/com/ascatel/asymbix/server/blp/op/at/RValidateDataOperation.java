/**
 * Class RegistratorValidateDataHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 05.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ErrorMessage;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CRUDResponse;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getDefectiveResponse;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildQueryItem;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.TYPE_ENTITY_PARAMETER;

/**
 * Внутренняя операция, которая параллельно выполняет проверку валидности входных данных при создании регистратора.
 * Выpывается из {@link UpdateATOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class RValidateDataOperation extends AbstractOperation {

    private String rtEntityId;

    private String regNumber;

    private String coEntityId;

    private Map<String, ResponseValidator> validators = new HashMap<>();

    private BlockService blockService;

    public RValidateDataOperation(AbstractOperation operation, BlockService blockService) {
        super(operation);
        this.blockService = blockService;
    }

    public void validateData(final String rtEntityId, final String regNumber, final String coEntityId) {

        this.rtEntityId = rtEntityId;//reqParams.get(R_BIND_RT_PARAMETER_ID);
        final ReadRequest checkRTRequest = buildCheckRTBlpRequest(rtEntityId);

        this.regNumber = regNumber;//reqParams.get(R_REG_NUM_PARAMETER_ID);
        final SelectRequest checkRExistenceRequest = buildCheckRExistenceRequest(rtEntityId, regNumber);

        this.coEntityId = coEntityId;//reqParams.get(R_BIND_CO_PARAMETER_ID);
        final ReadRequest checkCORequest = buildCheckCOBlpRequest(coEntityId);

        // Проверяем существование типа регистратора RegistratorTypeId.
        // Если заданный тип регистратора не существует, поднимаем ошибку, и завершаем операцию.
        doSelect(checkRTRequest, new ResponseValidator() {
            @Override
            public void validateResponse(MessageLite response) {
                final CRUDResponse crudResponse = (CRUDResponse) response;

                final ParameterValue value = crudResponse.getValue();
                if (value == null) {
                    // Заданный тип регистратора не существует, поднимаем ошибку, и завершаем операцию.
                    final String errMsg = new StringBuilder().append("Entity RegistratorTypr with ID=")
                            .append(rtEntityId).append(" does not exist").toString();
                    addErrorMessage(errMsg);
                }
            }
        });

        // Если RNumber задан, ищем сущность класса Registrator, по условию RegNum AND BindRT.
        // Если такая сущность найдена (это означает, что в системе уже есть абонентский терминал такого типа
        // с таким номером), поднимаем ошибку и завершаем операцию.
        doSelect(checkRExistenceRequest, new ResponseValidator() {
            @Override
            public void validateResponse(MessageLite response) {
                final ResultSet resultSet = (ResultSet) response;
                if (!resultSet.getParametersList().isEmpty()) {
                    final String errMsg = new StringBuilder().append("Registrator with regNumber=").append(regNumber)
                            .append(" and rtEntityId=").append(rtEntityId).append(" already exists").toString();
                    addErrorMessage(errMsg);
                }
            }
        });

        // Проверяем существование контролируемого объекта (класс Controlled Object) c идентификатором idCO.
        doSelect(checkCORequest, new ResponseValidator() {
            @Override
            public void validateResponse(MessageLite response) {
                final CRUDResponse crudResponse = (CRUDResponse) response;

                final ParameterValue value = crudResponse.getValue();
                if (value == null) {
                    // Заданный КО не существует, поднимаем ошибку, и завершаем операцию.
                    final String errMsg = new StringBuilder().append("Entity ControlledObject with ID=")
                            .append(coEntityId).append(" does not exist").toString();
                    addErrorMessage(errMsg);
                }
            }
        });
    }

    private void doSelect(ReadRequest readRequest, ResponseValidator validator) {

        try {
            final String blpInstanceId = doSelect(readRequest);
            validators.put(blpInstanceId, validator);
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());
        }
    }

    private void doSelect(SelectRequest selectRequest, ResponseValidator validator) {

        try {
            final String blpInstanceId = doSelect(selectRequest);
            validators.put(blpInstanceId, validator);
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());
        }
    }

    @Override
    public void handleCRUDResponse(CRUDResponse crud) {
        final String blpInstanceId = crud.getInstanceId();
        final ResponseValidator validator = validators.remove(blpInstanceId);
        if (validator == null) {
            throw new RuntimeException("Validator with blpInstanceId=" + blpInstanceId + " already invoked.");
        }

        validator.validateResponse(crud);

        if (validators.isEmpty()) {
            nextStep();
        }
    }

    @Override
    public void handleSelectResponse(ResultSet resultSet) {
        final String blpInstanceId = resultSet.getInstanceId();
        final ResponseValidator validator = validators.remove(blpInstanceId);
        if (validator == null) {
            throw new RuntimeException("Validator with blpInstanceId=" + blpInstanceId + " already invoked.");
        }

        validator.validateResponse(resultSet);

        if (validators.isEmpty()) {
            nextStep();
        }
    }

    @Override
    public void handleErrorResponse(ErrorMessage errMsg) {
        super.handleErrorResponse(errMsg);

        validators.remove(errMsg.getInstanceId());

        if (validators.isEmpty()) {
            nextStep();
        }
    }

    private void nextStep() {
        if (getErrorMessage() != null) {
            final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
            sendResponse(message);
        } else {
            final RCreateOperation nextOperation = new RCreateOperation(this, blockService);
            nextOperation.createRegistrator(regNumber, rtEntityId, coEntityId);
        }
    }

    private ReadRequest buildCheckRTBlpRequest(String rtEntityId) {
        return MessageUtil.buildReadRequest(getBlockId(), RT_ENTITY_TYPE, rtEntityId, TYPE_ENTITY_PARAMETER);
    }

    private ReadRequest buildCheckCOBlpRequest(String coEntityId) {
        return MessageUtil.buildReadRequest(getBlockId(), CO_ENTITY_TYPE, coEntityId, TYPE_ENTITY_PARAMETER);
    }

    private SelectRequest buildCheckRExistenceRequest(String rtEntityId, String regNumber) {
        List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(R_REG_NUM_PARAMETER_ID, Operation.EQ, buildParameterValue(regNumber)));
        filter.add(buildQueryItem(R_BIND_RT_PARAMETER_ID, Operation.EQ, buildParameterValue(rtEntityId)));

        return MessageUtil.buildSelectRequest(getBlockId(), getLocale(), R_ENTITY_TYPE, filter);
    }

    /**
     * Для обработки ответного сообщения.
     */
    private interface ResponseValidator {
        void validateResponse(MessageLite response);
    }

}