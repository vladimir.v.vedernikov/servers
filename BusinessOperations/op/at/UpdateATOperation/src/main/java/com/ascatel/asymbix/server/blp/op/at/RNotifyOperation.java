/**
 * Class RNotifyHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 08.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import ru.omnicomm.pegasus.processingPlatform.TimeConverter;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser.RegisterAT;
import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser.UpdateATResponse;
import com.ascatel.asymbix.server.blp.messaging.UpdateAtParser.State;

import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getResponseBuilder;

/**
 * Внутренняя операция, которая выполняем регистрацию в слое подключений.
 * Выpывается из {@link RCreateOperation}, {@link RSelectOperation} и {@link RUpdateOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class RNotifyOperation extends AbstractOperation {

    private BlockService blockService;

    public RNotifyOperation(AbstractOperation operation, BlockService blockService) {
        super(operation);
        this.blockService = blockService;
    }

    public void notify(String rEntityId, String rtEntityType, String sid, String regNumber, String oldRegNumber) {
        sendNotification(oldRegNumber, regNumber, rtEntityType, sid);
        sendResponse(rEntityId);
    }

    private void sendResponse(String rEntityId) {
        UpdateATResponse.Builder builder = getResponseBuilder(getBlockId(), getInstanceId());
        builder.setBlockState(State.LAST);
        builder.setRefDataId(Long.parseLong(rEntityId));
        sendResponse(builder.build());
    }

    private void sendNotification(String oldRegNumber, String regNumber, String rtEntityId, String sid) {
        try {
            final RegisterAT notification = buildNotifyMessage(oldRegNumber, regNumber, rtEntityId, sid);
            blockService.send(notification);
        } catch (NumberFormatException e) {
            LoggerFactory.getLogger().log(Level.WARN, e.getMessage(), e);
        }
    }

    private RegisterAT buildNotifyMessage(String oldRegNumber, String regNumber, String rtEntityId, String sid) {

        // Заголовок:
        // SourceID = значение настройки BLOCK_ID
        // MessageType = RegisterAT
        // Timestamp = Текущее время
        //
        // Тело:
        // RNumberOld
        // RNumber
        // RegistratorType
        // Sid

        final RegisterAT.Builder builder = RegisterAT.newBuilder()
                .setSourceId(getBlockId())
                .setMessageType(535)
                .setMessageTime(TimeConverter.getTimeStamp())
                .setMessageBlock(getBlockId())


                .setRegNumber(regNumber)
                .setATType(Long.parseLong(rtEntityId))
                .setATSourceId(Long.parseLong(sid));

        if (oldRegNumber != null) {
            builder.setRegNumberOld(oldRegNumber);
        }

        return builder
                .build();
    }
}
