/**
 * Class RegistratorUpdateHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 05.04.13
 */
package com.ascatel.asymbix.server.blp.op.at;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.UpdateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CRUDResponse;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.block.BlockService;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.op.at.UpdateATServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildUpdateRequest;

/**
 * Внутренняя операция, которая выполняет обновление данных регистратора.
 * Выpывается из {@link RValidateRegNumOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class RUpdateOperation extends AbstractOperation {

    private String rEntityId;
    private String rtEntityId;
    private String sid;
    private String regNumber;
    private String oldRegNumber;
    private BlockService blockService;

    public RUpdateOperation(AbstractOperation handler, BlockService blockService) {
        super(handler);
        this.blockService = blockService;
    }

    public void updateRegistrator(String rEntityId, String rtEntityId, String sid, String regNumber,
                                  String oldRegNumber) {

        this.rEntityId = rEntityId;
        this.rtEntityId = rtEntityId;
        this.sid = sid;
        this.regNumber = regNumber;
        this.oldRegNumber = oldRegNumber;

        // Устанавливаем значение атрибута RegNum сущности RegistratorId равным RNumber.
        final UpdateRequest update = buildUpdateBlpRequest(rEntityId, regNumber);
        try {
            doSelect(update);
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());
            final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
            sendResponse(message);
        }
    }

    @Override
    public void handleErrorResponse(BLSMessageParser.ErrorMessage errMsg) {
        final MessageLite message = getDefectiveResponse(getInstanceId(), getErrorMessage());
        sendResponse(message);
    }

    @Override
    public void handleCRUDResponse(CRUDResponse crud) {

        if (crud.getValue().getBooleanValue()) {
            final RNotifyOperation notifyOperation = new RNotifyOperation(this, blockService);
            notifyOperation.notify(rEntityId, rtEntityId, sid, regNumber, oldRegNumber);
        } else {
            final String errMsg = new StringBuilder().append("Error while updating renNumber fir Registrator=")
                    .append(rEntityId).toString();
            final MessageLite message = getDefectiveResponse(getInstanceId(), errMsg);
            sendResponse(message);
        }
    }

    private UpdateRequest buildUpdateBlpRequest(String entityId, String regNumber) {
        ParameterValue regNumValue = buildParameterValue(regNumber);
        return buildUpdateRequest(getBlockId(), R_ENTITY_TYPE, entityId, R_REG_NUM_PARAMETER_ID, regNumValue);
    }

}
