/**
 * Class EventTypesServer
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 02.04.13
 */
package com.ascatel.asymbix.server.blp.op.cp;

import com.ascatel.asymbix.server.blp.AbstractRequestServer;
import com.ascatel.asymbix.server.blp.messaging.EventTypesParser;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import com.ascatel.asymbix.server.blp.messaging.EventTypesParser.GetEventTypesRequest;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("EventTypesServer_Implementation")
public class EventTypesServer extends AbstractRequestServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    public static final int REQUEST_MESSAGE_TYPE = 443;

    public static final int RESPONSE_MESSAGE_TYPE = 444;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public EventTypesServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }


    @Override
    protected void processRequest(MessageLite request)
            throws InvalidProtocolBufferException {
        final GetEventTypesRequest proto = GetEventTypesRequest.parseFrom(request.toByteString());
        final EventTypeOperation operation = new EventTypeOperation(getEnvironment());

        operation.setInstanceId(proto.getInstanceId());
        operation.setLocale(proto.getLocale());
        operation.setBlockId(proto.getMessageBlock());

        final String cpId = Long.toString(proto.getCpId());

        operation.selectEventTypes(cpId);
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    public static class ResponseHelper {

        private ResponseHelper() {
        }

        public static MessageLite getDefectiveResponse(String instanceId, String errMsg) {
            EventTypesParser.GetEventTypesResponse.Builder builder = getResponseBuilder(0, instanceId);
            builder.setBlockState(EventTypesParser.State.DEFECTIVE).setErrorMessage(errMsg);

            return builder.build();
        }

        public static MessageLite getEmptyResponse(String instanceId) {
            EventTypesParser.GetEventTypesResponse.Builder builder = getResponseBuilder(0, instanceId);
            builder.setBlockState(EventTypesParser.State.LAST);
            return builder.build();
        }

        public static EventTypesParser.GetEventTypesResponse.Builder getResponseBuilder(int blockNumber, String instanceId) {
            EventTypesParser.GetEventTypesResponse.Builder builder = EventTypesParser.GetEventTypesResponse.newBuilder();

            builder.setMessageType(RESPONSE_MESSAGE_TYPE)
                    .setInstanceId(instanceId)
                    .setBlockNumber(blockNumber);

            return builder;
        }

    }


    //FOR DEBUG ONLY!
//    @Override
//    protected void initServer(Server server, Handler handler) {
//        handler.setTimer("1111111111111111", DELAY_BASED, 1000L, 0L);
//    }
//
//    @Override
//    protected void processTimerEvent(TimerEvent signal) {
//        final long cpId = 10600L;
//        final long cpId2 = 10601L;
//
//        final GetEventTypesRequest request = GetEventTypesRequest.newBuilder()
//                .setMessageType(443)
//                .setInstanceId("12345")
//                .setLocale("en")
//                .setMessageBlock(1)
//                .setCpId(cpId).build();
//
//        final GetEventTypesRequest request2 = GetEventTypesRequest.newBuilder()
//                        .setMessageType(443)
//                        .setInstanceId("12346")
//                        .setLocale("en")
//                        .setMessageBlock(1)
//                        .setCpId(cpId2).build();
//
//
//        try {
//            getEnvironment().getServer().send(null, request);
//            getEnvironment().getServer().send(null, request2);
//        } catch (SendMessageException e) {
//            LOGGER.log(Level.ERROR, null, e);
//        }
//    }

}
