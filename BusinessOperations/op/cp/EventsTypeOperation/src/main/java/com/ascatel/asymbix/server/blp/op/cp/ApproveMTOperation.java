/**
 * Class ApproveMTHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 03.04.13
 */
package com.ascatel.asymbix.server.blp.op.cp;

import com.ascatel.asymbix.server.blp.messaging.EventTypesParser;
import com.ascatel.asymbix.server.blp.op.cp.entity.CPMTEEntity;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CRUDResponse;

import com.ascatel.asymbix.server.blp.messaging.EventTypesParser.GetEventTypesResponse.EventList;

import static com.ascatel.asymbix.server.blp.op.cp.EventTypesServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.op.cp.EventTypesServer.ResponseHelper.getEmptyResponse;
import static com.ascatel.asymbix.server.blp.op.cp.EventTypesServer.ResponseHelper.getResponseBuilder;

/**
 * Внутреннея операция которая вызывается из {@link EventTypeOperation}.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class ApproveMTOperation extends AbstractOperation {

    public static final String MT_ENTITY_TYPE = "1300";

    public static final String MT_IS_EVENT_PARAMETER_ID = "1305";

    private Map<String, CPMTEEntity> needApprove;

    private List<CPMTEEntity> approved;

    public ApproveMTOperation(AbstractOperation operation) {
        super(operation);

        this.needApprove = new HashMap<>();
        this.approved = new ArrayList<>();
    }

    public void approveCPMTE(Collection<CPMTEEntity> values) {
        for (CPMTEEntity cpmteEntity : values) {
            final ObjectLink mtLink = cpmteEntity.getMtBind();

            if (mtLink.getLinksCount() > 0) {
                // Если несколько ссылок , то берем первый.
                final String mtEntityId = mtLink.getLinks(0);
                final ReadRequest readRequest = buildReadRequest(mtEntityId);

                doSelect(readRequest, cpmteEntity);
            }
        }
    }

    private void doSelect(ReadRequest readRequest, CPMTEEntity cpmteEntity) {
        final BusinessLogicService blService = getEnv().getBlService();
        final Server server = getEnv().getServer();
        final byte[] requestData = readRequest.toByteArray();

        try {
            final String blpInstanceId = blService.getParameterValueAsync(server, requestData);
            getEnv().addOperation(blpInstanceId, this);
            needApprove.put(blpInstanceId, cpmteEntity);
        } catch (BusinessLogicServiceException e) {
            addErrorMessage(e.getMessage());
        }
    }

    @Override
    public void handleCRUDResponse(CRUDResponse crud) {
        final String blpInstanceId = crud.getInstanceId();
        final CPMTEEntity entity = needApprove.remove(blpInstanceId);
        if (entity == null) {
            throw new RuntimeException("CPMTEEntity with blpInstanceId=" + blpInstanceId + " already approved.");
        }

        if (crud.getValue().getBooleanValue()) {
            approved.add(entity);
        }

        if (needApprove.isEmpty()) {
            sendResponse();
        }
    }

    @Override
    public void handleErrorResponse(BLSMessageParser.ErrorMessage errMsg) {
        super.handleErrorResponse(errMsg);

        if (needApprove.isEmpty()) {
            sendResponse();
        }
    }

    private void sendResponse() {

        final MessageLite message;
        if (getErrorMessage() != null) {
            message = getDefectiveResponse(getInstanceId(), getErrorMessage());
        } else {
            if (approved.isEmpty()) {
                message = getEmptyResponse(getInstanceId());
            } else {
                message = buildResponse();
            }
        }

        sendResponse(message);
    }

    private MessageLite buildResponse() {
        List<EventList> events = new ArrayList<>(approved.size());
        for (CPMTEEntity cpmteEntity : approved) {

            final long mtId = getMtId(cpmteEntity);
            final String name = getName(cpmteEntity);
            final ByteString image = cpmteEntity.getImage();

            final EventList.Builder builder = EventList.newBuilder().setMtId(mtId);
            if (name != null) {
                builder.setEventName(name);
            }
            if (image != null) {
                builder.setImage(image);
            }
            final EventList event = builder.build();
            events.add(event);
        }

        return getResponseBuilder(0, getInstanceId()).addAllEventList(events).build();
    }

    private long getMtId(CPMTEEntity cpmteEntity) {
        final ObjectLink mtLink = cpmteEntity.getMtBind();

        if (mtLink.getLinksCount() > 0) {
            // Если несколько ссылок , то берем первый.
            final String mtEntityId = mtLink.getLinks(0);
            return Long.parseLong(mtEntityId);
        } else {
            throw new RuntimeException("Invlid entity CPMTEEntity with id=" + cpmteEntity.getEntityId()
                    + ". It doesn't contain any mtIds.");
        }
    }


    private ReadRequest buildReadRequest(String mtEntityId) {
        return MessageUtil.buildReadRequest(getBlockId(), MT_ENTITY_TYPE, mtEntityId, MT_IS_EVENT_PARAMETER_ID);
    }

    private String getName(CPMTEEntity entity) {
        final BLSMessageParser.MUIString muiString = entity.getMuiString();
        if (muiString == null) {
            return null;
        }

        return MessageUtil.getMUIValue(getLocale(), muiString);
    }

}
