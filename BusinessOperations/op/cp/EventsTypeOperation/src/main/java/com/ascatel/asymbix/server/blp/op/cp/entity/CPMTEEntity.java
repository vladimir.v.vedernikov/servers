/**
 * Class CPEntity
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 03.04.13
 */
package com.ascatel.asymbix.server.blp.op.cp.entity;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.MUIString;
import com.google.protobuf.ByteString;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class CPMTEEntity {

    private String entityId;

    private ObjectLink cpBind;

    private ObjectLink mtBind;

    private MUIString muiString;

    private ByteString image;

    public CPMTEEntity(String entityId) {
        this.entityId = entityId;
    }

    public ObjectLink getCpBind() {
        return cpBind;
    }

    public void setCpBind(ObjectLink cpBind) {
        this.cpBind = cpBind;
    }

    public ObjectLink getMtBind() {
        return mtBind;
    }

    public void setMtBind(ObjectLink mtBind) {
        this.mtBind = mtBind;
    }

    public MUIString getMuiString() {
        return muiString;
    }

    public void setMuiString(MUIString muiString) {
        this.muiString = muiString;
    }

    public ByteString getImage() {
        return image;
    }

    public void setImage(ByteString image) {
        this.image = image;
    }

    public String getEntityId() {
        return entityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CPMTEEntity)) return false;

        CPMTEEntity that = (CPMTEEntity) o;

        if (!entityId.equals(that.entityId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return entityId.hashCode();
    }
}
