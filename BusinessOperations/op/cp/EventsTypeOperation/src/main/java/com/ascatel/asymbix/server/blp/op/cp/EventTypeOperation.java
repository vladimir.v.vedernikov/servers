/**
 * Class SelectHandler
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 03.04.13
 */
package com.ascatel.asymbix.server.blp.op.cp;

import com.ascatel.asymbix.server.blp.op.cp.entity.CPMTEEntity;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.Environment;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ErrorMessage;

import static com.ascatel.asymbix.server.blp.op.cp.EventTypesServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildQueryItem;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildSelectRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Операция 'Получить список типов событий'.
 * Операция определяет типы событий, которые могут отображаться на графике. Операция сразу возвращает набор типов
 * событий, а также пиктограммы для них.
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code controlledParameterId (cpId)} - идентификатор контролируемого параметра, по которому
 * строится график.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class EventTypeOperation extends AbstractOperation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    public EventTypeOperation(Environment env) {
        super(env);
    }

    public void selectEventTypes(String cpId) {
        // Создаем запрос.
        final ParameterValue value = buildParameterValue(cpId);
        final QueryItem item = buildQueryItem(CPMTE_BIND_CP_PARAMETER_ID, Operation.EQ, value);
        final SelectRequest select = buildSelectRequest(getLocale(), CPMTE_ENTITY_TYPE, Arrays.asList(item));

        // Выполняем запрос асинхронно.
        try {
            final String bplInstanceId = getEnv().getBlService().selectByParamsAsync(getEnv().getServer(), select.toByteArray());
            getEnv().addOperation(bplInstanceId, this);
        } catch (BusinessLogicServiceException e) {
            final MessageLite errMsg = getDefectiveResponse(getInstanceId(), e.getMessage());
            sendResponse(errMsg);
        }
    }


    @Override
    public void handleErrorResponse(ErrorMessage errMsg) {
        super.handleErrorResponse(errMsg);

        final MessageLite response = getDefectiveResponse(getInstanceId(), getErrorMessage());
        sendResponse(response);
    }

    @Override
    public void handleSelectResponse(ResultSet resultSet) {
        final List<BLSMessageParser.Parameter> parameterList = resultSet.getParametersList();

        final Map<String, CPMTEEntity> cpmteCollection = new HashMap<>();
        for (Parameter parameter : parameterList) {

            final String entityId = parameter.getEntityId();
            final String attrId = parameter.getAttributeId();
            final ParameterValue parameterValue = parameter.getValue();

            CPMTEEntity entity = cpmteCollection.get(entityId);
            if (entity == null) {
                entity = new CPMTEEntity(entityId);
                cpmteCollection.put(entityId, entity);
            }

            switch (attrId) {
                case CPMTE_BIND_CP_PARAMETER_ID:
                    entity.setCpBind(parameterValue.getObjectLinkValue());
                    break;
                case CPMTE_BIND_MT_PARAMETER_ID:
                    entity.setMtBind(parameterValue.getObjectLinkValue());
                    break;
                case CPMTE_NAME_PARAMETER_ID:
                    entity.setMuiString(parameterValue.getMuistringValue());
                    break;
                case CPMTE_IMAGE_PARAMETER_ID:
                    entity.setImage(parameterValue.getFileValue());
                    break;
                default:
                    //unused attributes

            }
        }

        final ApproveMTOperation mtOperation = new ApproveMTOperation(this);
        mtOperation.approveCPMTE(cpmteCollection.values());
    }

}
