/**
 * Class UpdateAllObjectsOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 26.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import com.ascatel.asymbix.server.blp.op.general.DeleteObjectOperation.EntityAttribute;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.ArrayList;
import java.util.List;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;

import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Внутренняя операция.
 * Операция вызывается перед удалением экземпляра класса.
 * Удаляет все ссылки на передаваемый экземпляр у объектов заданного класса определенного атрибута.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class UpdateAllObjectsOperation extends AbstractOperation {

    public UpdateAllObjectsOperation(AbstractOperation handler) {
        super(handler);
    }

    /**
     * Удаляет все ссылки на сущность с идентификатором {@code entityId}.
     *
     * @param entityId  удаляемый объект.
     * @param attribute атрибут ссылающийся на класс удаляемого объекта.
     * @throws BusinessLogicServiceException при каких-либо ошибках.
     */
    public void updateAllObject(String entityId, EntityAttribute attribute)
            throws BusinessLogicServiceException {
        final String parameterId = attribute.getAttributeId();
        final String classLink = attribute.getClassLink();
        updateObjects(classLink, parameterId, entityId);
    }

    private void updateObjects(String linkedClassId, String linkedParameterId,
                               String deletingEntity)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(linkedParameterId, Operation.EQ, buildParameterValue(deletingEntity)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), linkedClassId, filter);
        final ResultSet resultSet = selectEntities(request);

        for (Parameter parameter : resultSet.getParametersList()) {
            if (linkedParameterId.equals(parameter.getAttributeId())) {
                // тот параметр который нужно заменить.
                final String entityType = parameter.getEntityType();
                final String entityId = parameter.getEntityId();
                final String attrId = parameter.getAttributeId();
                final ParameterValue value = extractDeletingId(deletingEntity, parameter.getValue());
                updateParameterValue(entityType, entityId, attrId, value);
            }
        }
    }

    private ParameterValue extractDeletingId(String deletingEntity, ParameterValue value)
            throws BusinessLogicServiceException {


        if (!value.hasObjectLinkValue()) {
            throw new BusinessLogicServiceException("Invalid type parameter value");
        }

        final List<String> newLinks = new ArrayList<>();
        final ObjectLink objectLink = value.getObjectLinkValue();
        for (String link : objectLink.getLinksList()) {
            if (!deletingEntity.equals(link)) {
                newLinks.add(link);
            }
        }

        return buildParameterValue(buildObjectLink(newLinks));
    }
}
