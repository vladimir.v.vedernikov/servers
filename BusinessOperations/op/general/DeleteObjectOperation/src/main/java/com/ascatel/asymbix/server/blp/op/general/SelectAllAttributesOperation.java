/**
 * Class SelectAllAttributesOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 26.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.ObjectLinkAttrRange;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;


import java.util.*;

import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

import com.ascatel.asymbix.server.blp.op.general.DeleteObjectOperation.EntityAttribute;

/**
 * Внутренняя операция.
 * Выбиравет все атрибуты системы которые имеют значение атрибута {@link Constants#TYPE_ATTRIBUTE_PARAMETER} равное
 * {@link Constants#OBJECT_ATTRIBUTE_TYPE}, а также ссылаются на передаваемый класс.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class SelectAllAttributesOperation extends AbstractOperation {

    public static final String ATTRIBUTE_RANGE_PARAMETER_ID = "72";

    public SelectAllAttributesOperation(AbstractOperation handler) {
        super(handler);
    }

    public EntityAttribute[] selectAttributes(String entityType)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(ATTRIBUTE_RANGE_PARAMETER_ID, Operation.LIKE, buildParameterValue(entityType)));

        final SelectRequest request
                = buildSelectRequest(getBlockId(), getLocale(), Constants.ATTRIBUTE_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Map<String, EntityAttribute> attributes = new HashMap<>();

        for (Parameter parameter : resultSet.getParametersList()) {
            final String attrId = parameter.getEntityId();
            final String paramId = parameter.getAttributeId();
            final ParameterValue value = parameter.getValue();

            if (ATTRIBUTE_RANGE_PARAMETER_ID.equals(paramId)) {
                if (!value.hasStringValue()) {
                    final String errMsg = new StringBuilder().append("Attribute range doesn't defined in Attribute ")
                            .append(attrId).toString();
                    throw new BusinessLogicServiceException(errMsg);
                }

                final EntityAttribute attribute = getEntityAttribute(attributes, attrId);
                attribute.setAttrRange(value.getStringValue());
            } else if (Constants.CLASS_ID_ATTRIBUTE_PARAMETER.equals(paramId)) {
                final String classLink = value.getStringValue();
                final EntityAttribute attribute = getEntityAttribute(attributes, attrId);
                attribute.setClassLinks(classLink);
            } else if (Constants.TYPE_ATTRIBUTE_PARAMETER.equals(paramId)) {
                final EntityAttribute attribute = getEntityAttribute(attributes, attrId);
                attribute.setType(value.getIntValue());
            }
        }

        final Collection<EntityAttribute> values = attributes.values();
        final List<EntityAttribute> result = new ArrayList<>(values.size());
        for (EntityAttribute attribute : values) {
            // Проверим здесь валидность найденых атрибутов.
            // Интересуют только атрибуты с типом Constants.OBJECT_ATTRIBUTE_TYPE
            // Случай когда attribute.getClassLinks() == null это когда, атрибут есть, но он не привязан ни к одному
            // классу - такой атрибут нам не нужен.
            if (Constants.OBJECT_ATTRIBUTE_TYPE == attribute.getType() && attribute.getClassLink() != null) {
                final String rangeStr = attribute.getAttrRange();
                if (rangeStr == null) {
                    final String errMsg = new StringBuilder().append("Attribute range doesn't defined for attribute ")
                            .append(attribute.getAttributeId()).toString();
                    throw new BusinessLogicServiceException(errMsg);
                }

                final ObjectLinkAttrRange attrRange = ObjectLinkAttrRange.parseAttrRange(rangeStr);
                // Влозможен случай когда атрибут не ссылается на требуемый класс - это возможно т.к. применялась
                // маска, т.е. могли попасть идентификаторы содержащие в себе entityType.
                if (attrRange.getEntityType().equals(entityType)) {
                    attribute.setBindedClass(attrRange.getEntityType());
                    result.add(attribute);
                }
            }
        }

        final int size = result.size();
        return result.toArray(new EntityAttribute[size]);
    }

    private EntityAttribute getEntityAttribute(Map<String, EntityAttribute> attributes, String attrId) {
        EntityAttribute attribute = attributes.get(attrId);
        if (attribute == null) {
            attribute = new EntityAttribute(attrId);
            attributes.put(attrId, attribute);
        }
        return attribute;
    }

}
