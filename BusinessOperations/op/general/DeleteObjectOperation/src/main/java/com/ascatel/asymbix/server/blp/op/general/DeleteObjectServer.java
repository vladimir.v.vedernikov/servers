/**
 * Class DeleteObjectServer
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 26.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.AbstractRequestServer;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.Handler;
import ru.omnicomm.pegasus.processingPlatform.Server;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.messages.TimerEvent;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

/**
 * !!!!!!! DO NOT USE !!!!!!!
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DeleteObjectServer_Implementation")
public class DeleteObjectServer extends AbstractRequestServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    public static final int REQUEST_MESSAGE_TYPE = 999999;

    public static final int RESPONSE_MESSAGE_TYPE = 9999991;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public DeleteObjectServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected void processRequest(MessageLite request)
            throws InvalidProtocolBufferException {
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }


    //FOR DEBUG ONLY!
    @Override
    protected void initServer(Server server, Handler handler) {
        handler.setTimer("1", Handler.TimerType.DELAY_BASED, 1000L, 0L);
        handler.setTimer("2", Handler.TimerType.DELAY_BASED, 5000L, 0L);
    }

    @Override
    protected void processTimerEvent(TimerEvent signal) {
        if ("1".equals(signal.id())) {
            final DeleteObjectOperation operation = new DeleteObjectOperation(getEnvironment());
            operation.setInstanceId("123456789");
            operation.setLocale(null);
            operation.setBlockId(1);

            try {
                operation.deleteObject("1950", "10103");
            } catch (BusinessLogicServiceException e) {
                LOGGER.log(Level.ERROR, null, e);
            }
        } else if ("2".equals(signal.id())) {

        }
    }
}
