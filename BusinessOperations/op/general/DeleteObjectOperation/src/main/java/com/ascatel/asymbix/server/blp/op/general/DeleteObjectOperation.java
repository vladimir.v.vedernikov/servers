/**
 * Class DeleteObjectOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 25.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.Environment;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

/**
 * Операция 'Удалить экземпляр класса'.
 * В ходе операции удаляется сущность. Также в ходе операции обновляются сопряжённые атрибуты для связей (если есть).
 * Если для класса сущностей задана операция DeleteProcedure, то она выполняется вместо данной стандартной.
 * Входные параметры:
 * <ul>
 * <li>{@code referenceId} - идентификатор класса удаляемой сущности.</li>
 * <li>{@code refDataId} – идентификатор удаляемой сущности.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class DeleteObjectOperation extends AbstractOperation {

    public DeleteObjectOperation(AbstractOperation handler) {
        super(handler);
    }

    public DeleteObjectOperation(Environment env) {
        super(env);
    }


    public void deleteObject(String referenceId, String refDataId)
            throws BusinessLogicServiceException {
        // TODO Считываем из класса ReferenceId параметр DeleteProcedure.
        // TODO Если данный параметр задан (не пуст), то необходимо выполнить эту процедуру, иначе:

        // Удаляем ссылки из других сущностей на данную.
        deleteLinkedObjects(referenceId, refDataId);

        // Если удаляемый объект задан в каких-либо настройках, нужно удалить эти настройки.
        deleteSettings(referenceId, refDataId);

        // Удаляем сущность RefDataId
        deleteEntity(referenceId, refDataId);
    }

    private void deleteSettings(String entityType, String entityId)
            throws BusinessLogicServiceException {
        final DeleteObjectSettingsOperation operation = new DeleteObjectSettingsOperation(this);
        operation.deleteSettings(entityType, entityId);
    }

    private void deleteLinkedObjects(String entityType, String entityId)
            throws BusinessLogicServiceException {
        // Здесь надо перебрать все объекты в которых есть ссылка на удаляемый объект и удалить эту ссылку.

        // Выберем все атрибуты у которых тип 'ссылка' и у которых в attrRange содержит entityType.
        final EntityAttribute[] allAttributes = selectAllAttributes(entityType);

        // Обойдем все значения параметров объектов в которых есть ссылка на данный экземпляр сущности и удалим
        // себя от туда.
        for (EntityAttribute attribute : allAttributes) {
            final UpdateAllObjectsOperation operation = new UpdateAllObjectsOperation(this);
            operation.updateAllObject(entityId, attribute);
        }
    }

    private EntityAttribute[] selectAllAttributes(String entityType)
            throws BusinessLogicServiceException {
        final SelectAllAttributesOperation operation = new SelectAllAttributesOperation(this);
        return operation.selectAttributes(entityType);
    }

    public static class EntityAttribute {
        /**
         * Идентификатор атрибута.
         */
        private String attributeId;

        /**
         * На какаой класс ссылается данный атрибут.
         */
        private String bindedClass;

        /**
         * Какому классу принадлежит данный атрибут.
         */
        private String classLink;

        /**
         * Тип атрибута.
         */
        private int type;

        private String attrRange;

        public EntityAttribute(String attributeId) {
            this.attributeId = attributeId;
        }

        public String getAttributeId() {
            return attributeId;
        }

        public String getBindedClass() {
            return bindedClass;
        }

        public void setBindedClass(String bindedClass) {
            this.bindedClass = bindedClass;
        }

        public String getClassLink() {
            return classLink;
        }

        public void setClassLinks(String classLink) {
            this.classLink = classLink;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getAttrRange() {
            return attrRange;
        }

        public void setAttrRange(String attrRange) {
            this.attrRange = attrRange;
        }
    }

}
