/**
 * Class DeleteObjectSettingsOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 26.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.*;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.OMIdentifiers.CPPS_ENTITY_TYPE;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Внутренняя операция.
 * Если удаляемый объект задан в каких-либо настройках, нужно удалить эти настройки.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class DeleteObjectSettingsOperation extends AbstractOperation {

    DeleteObjectSettingsOperation(AbstractOperation handler) {
        super(handler);
    }

    void deleteSettings(String entityType, String entityId)
            throws BusinessLogicServiceException {
        // Находим все объекты класса Controlled Parameter Processing Setting значениями которых могут быть сущности
        // данного класса. Вызываем базовую операцию Найти сущности по условию, для класса CPPS передавая в качестве
        // условия BindedClassID = <ReferenceID> AND Type = 5 (object)
        final Collection<String> cppsIds = selectCPPS(entityType);

        // Ищем значение настройки совпадающее с обновляемой сущностью. Ищем сущность типа
        // 'Controlled Parameter Processing Setting Value' по условию: (BindCPPS = <CPPSid> AND Value = RefDataID)
        final Collection<CPPSVEntry> cppsvs = selectCPPSV(cppsIds, entityId);

        deleteSettings(cppsvs);
    }

    private void deleteSettings(Collection<CPPSVEntry> cppsvs)
            throws BusinessLogicServiceException {

        for (CPPSVEntry cppsv : cppsvs) {
            final List<String> simpSettingsList = cppsv.getBindSimpSet();
            for (String simpSetId : simpSettingsList) {
                deleteEntity(SIMPLE_SETTING_ENTITY_TYPE, simpSetId);
            }

            deleteEntity(CPPSV_ENTITY_TYPE, cppsv.getCppsvId());
        }
    }

    private Collection<CPPSVEntry> selectCPPSV(Collection<String> cppsIds, String refDataId)
            throws BusinessLogicServiceException {
        if (cppsIds.isEmpty()) {
            return Collections.emptyList();
        }

        final QueryItem refDataIdItem
                = buildQueryItem(CPPSV_VALUE_PARAMETER_ID, Operation.EQ, buildParameterValue(refDataId));

        final List<CPPSVEntry> result = new ArrayList<>();
        for (String cppsId : cppsIds) {
            final List<QueryItem> filter = new ArrayList<>();
            filter.add(buildQueryItem(CPPSV_BIND_CPPS_PARAMETER_ID, Operation.EQ, buildParameterValue(cppsId)));
            filter.add(refDataIdItem);

            final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
            final ResultSet resultSet = selectEntities(request);

            final List<Parameter> parameters = resultSet.getParametersList();
            if (!parameters.isEmpty()) {
                for (Parameter parameter : parameters) {
                    if (CPPSV_BIND_SIMP_SET_PARAMETER_ID.equals(parameter.getAttributeId())) {
                        final CPPSVEntry entry = new CPPSVEntry(parameter.getEntityId());
                        final List<String> simpSetList = extractObjectLinks(parameter.getValue());
                        entry.setBindSimpSet(simpSetList);
                        result.add(entry);
                    } else {
                        // skip other parameters
                    }
                }
            }
        }

        return result;
    }

    private Collection<String> selectCPPS(String entityType)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(CPPS_TYPE_PARAMETER_ID, Operation.EQ, buildParameterValue(5)));
        filter.add(buildQueryItem(CPPS_BINDED_CLASS_PARAMETER_ID, Operation.EQ, buildParameterValue(Long.parseLong(entityType))));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPS_ENTITY_TYPE, filter);
        final byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());
        final Set<String> foundId = new HashSet<>();
        try {

            final ResultSet resultSet = ResultSet.parseFrom(responseData);
            final List<Parameter> parameters = resultSet.getParametersList();
            for (Parameter parameter : parameters) {
                foundId.add(parameter.getEntityId());
            }
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }

        return foundId;
    }

    private class CPPSVEntry {

        private String cppsvId;
        private List<String> bindSimpSet;

        private CPPSVEntry(String cppsvId) {
            this.cppsvId = cppsvId;
        }

        public String getCppsvId() {
            return cppsvId;
        }

        public List<String> getBindSimpSet() {
            return bindSimpSet;
        }

        public void setBindSimpSet(List<String> bindSimpSet) {
            this.bindSimpSet = bindSimpSet;
        }

    }
}
