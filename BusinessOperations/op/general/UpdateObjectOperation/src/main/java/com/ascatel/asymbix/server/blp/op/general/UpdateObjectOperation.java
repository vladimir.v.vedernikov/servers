/**
 * Class UpdateObjectOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.op.co.SaveSettingOperation;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.TYPE_ENTITY_PARAMETER;

/**
 * Операция "3.1.1	Добавить/обновить экземпляр класса".
 * Входные параметры:
 * <ul>
 * <li>{@code referenceId} (entityType) - идентификатор класса, сущность которого создаётся или обновляется</li>
 * <li>{@code refDataId} – идентификатор сущности, которая создаётся или обновляется (если равен 0, то
 * производится создание, иначе - обновление)</li>
 * <li>массив значений атрибутов создаваемой или обновляемой сущности.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class UpdateObjectOperation extends AbstractOperation {

    public UpdateObjectOperation(AbstractOperation operation) {
        super(operation);
    }

    private boolean newEntity;

    public String updateObject(String entityType, String refDataId, Map<String, ParameterValue> attrArray)
            throws BusinessLogicServiceException {
        // Проверяем
        String entityId = validateRefDataId(entityType, refDataId);

        // Сохраняем атрибуты сущности.
        updateAttributes(entityType, entityId, attrArray);

        // Выполняем стандартную проверку, что заданы значения всех атрибутов, обязательных к заполнению.
        // Обновляем их если можем.
        updateRequiredAttributes(entityType, entityId, attrArray, newEntity);

        // Если обновляемый объект задан в каких-либо настройках, нужно обновить эти настройки.
        if (!newEntity) {
            updateObjectSettings(entityType, entityId);
        }

        return entityId;
    }

    /**
     * Выполняет валидацию {@code refDataId}.
     * <p/>
     * Если RefDataId НЕ null, то проверяем, что существует такой объект - считываем значение атрибута Type, как
     * указано в базовой операции "Получить значение параметра сущности". (Передаваемые значения: RefDataId,
     * Id параметра Type = 6). Если значение не найдено, то возвращаем ошибку и прерываем операцию, иначе возвращаем
     * значение {@code refDataId}.
     * <p/>
     * Если RefDataId null, то создаём новый объект и возвращаем присвоенный ему идентификатор.
     *
     * @param entityType идентификатор типа сущности.
     * @param refDataId  идентификатор сущности для валидации.
     * @return подтвержденный идентификтаор.
     * @throws BusinessLogicServiceException - если сущности с переданным {@code refDataId} не существует.
     */
    private String validateRefDataId(String entityType, String refDataId)
            throws BusinessLogicServiceException {
        if (refDataId == null) {
            newEntity = true;
            final CreateRequest request = buildCreateRequest(getBlockId(), entityType);
            return getEnv().getBlService().createEntity(request.toByteArray());
        } else {
            newEntity = false;
            final ReadRequest request = buildReadRequest(getBlockId(), entityType, refDataId, TYPE_ENTITY_PARAMETER);
            final byte[] data = getEnv().getBlService().getParameterValue(request.toByteArray());
            try {
                final ParameterValue value = ParameterValue.parseFrom(data);
                if (!value.hasStringValue()) {
                    throw new BusinessLogicServiceException("Entity with id " + refDataId + " is not exist");
                }
            } catch (InvalidProtocolBufferException e) {
                throw new BusinessLogicServiceException(e);
            }
            // если не получили ошибку BusinessLogicServiceException, то это значит, что сущность существует.
            return refDataId;
        }
    }

    private void updateAttributes(String entityType, String entityId, Map<String, ParameterValue> attrArray)
            throws BusinessLogicServiceException {
        UpdateObjectAttributeOperation attrHandler = new UpdateObjectAttributeOperation(this);
        attrHandler.updateAttributes(entityType, entityId, attrArray);
    }

    private void updateRequiredAttributes(String entityType, String entityId, Map<String, ParameterValue> attrArray,
                                          boolean newEntity)
            throws BusinessLogicServiceException {
        UpdateRequiredAttributeOperation requiredHandler = new UpdateRequiredAttributeOperation(this);
        requiredHandler.updateRequiredAttributes(entityType, entityId, attrArray, newEntity);
    }

    private void updateObjectSettings(String entityType, String refDataId)
            throws BusinessLogicServiceException {
        // Находим все объекты класса 'Controlled Parameter Processing Setting' значениями которых могут быть сущности
        // данного класса. Вызываем базовую операцию 'Найти сущности по условию', для класса CPPS передавая в качестве
        // условия BindedClassID = <ReferenceID> AND Type = 5 (object).
        final Collection<String> cppsIds = selectCPPS(entityType);

        // Ищем значение настройки совпадающее с обновляемой сущностью. Ищем сущность типа
        // 'Controlled Parameter Processing Setting Value' по условию: (BindCPPS = <CPPSid> AND Value = RefDataID)
        final Collection<CPPSVEntry> cppsvs = selecyCPPSV(cppsIds, refDataId);

        saveSettingsByCPPSV(cppsvs);
    }

    private void saveSettingsByCPPSV(Collection<CPPSVEntry> cppsvs)
            throws BusinessLogicServiceException {
        for (CPPSVEntry cppsv : cppsvs) {
            final String cppsId = cppsv.getCppsId();
            final String asId = cppsv.getAsID();
            final String value = cppsv.getValue();
            final SaveSettingOperation settingHandler = new SaveSettingOperation(this);
            settingHandler.saveSetting(cppsId, asId, value);
        }
    }

    private Collection<CPPSVEntry> selecyCPPSV(Collection<String> cppsIds, String refDataId)
            throws BusinessLogicServiceException {
        if (cppsIds.isEmpty()) {
            return Collections.emptyList();
        }

        final QueryItem refDataIdItem
                = buildQueryItem(CPPSV_VALUE_PARAMETER_ID, Operation.EQ, buildParameterValue(refDataId));

        // Если найден объект (Обозн. CPPSVid), подготовим данные для операции 'Сохранение настройки', передавленные
        // в качестве параметров:
        //    1. текущий объект CPPSid
        //    2. AsID = CPPSVid.BindAS
        //    3. Val = CPPSVid.Value
        List<CPPSVEntry> result = new ArrayList<>();
        for (String cppsId : cppsIds) {
            final List<QueryItem> filter = new ArrayList<>();
            filter.add(buildQueryItem(CPPSV_BIND_CPPS_PARAMETER_ID, Operation.EQ, buildParameterValue(cppsId)));
            filter.add(refDataIdItem);

            final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
            final byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());

            try {
                final ResultSet resultSet = ResultSet.parseFrom(responseData);
                final List<Parameter> parameters = resultSet.getParametersList();

                if (!parameters.isEmpty()) {
                    final CPPSVEntry entry = new CPPSVEntry();
                    entry.setCppsId(cppsId);

                    for (Parameter parameter : parameters) {
                        if (CPPSV_BIND_AS_PARAMETER_ID.equals(parameter.getAttributeId())) {
                            final BLSMessageParser.ObjectLink obkLink = parameter.getValue().getObjectLinkValue();
                            if (obkLink.getLinksCount() < 1) {
                                throw new BusinessLogicServiceException("CPPSVid.BindAS is not defined.");
                            }
                            entry.setAsID(obkLink.getLinks(0));
                        } else if (CPPSV_VALUE_PARAMETER_ID.equals(parameter.getAttributeId())) {
                            entry.setValue(parameter.getValue().getStringValue());
                        } else {
                            // skip other parameters
                        }
                    }
                    result.add(entry);
                }
            } catch (InvalidProtocolBufferException e) {
                throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
            }
        }

        return result;
    }

    private Collection<String> selectCPPS(String entityType)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(CPPS_TYPE_PARAMETER_ID, Operation.EQ, buildParameterValue(5)));
        filter.add(buildQueryItem(CPPS_BINDED_CLASS_PARAMETER_ID, Operation.EQ, buildParameterValue(Long.parseLong(entityType))));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPS_ENTITY_TYPE, filter);
        final byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());
        final Set<String> foundId = new HashSet<>();
        try {

            final ResultSet resultSet = ResultSet.parseFrom(responseData);
            final List<Parameter> parameters = resultSet.getParametersList();
            for (Parameter parameter : parameters) {
                foundId.add(parameter.getEntityId());
            }
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }

        return foundId;
    }

    private class CPPSVEntry {
        private String cppsId;
        private String asID;
        private String value;


        public String getCppsId() {
            return cppsId;
        }

        public void setCppsId(String cppsId) {
            this.cppsId = cppsId;
        }

        public String getAsID() {
            return asID;
        }

        public void setAsID(String asID) {
            this.asID = asID;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
