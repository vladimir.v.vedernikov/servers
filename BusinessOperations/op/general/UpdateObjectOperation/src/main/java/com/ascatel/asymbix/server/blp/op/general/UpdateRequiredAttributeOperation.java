/**
 * Class UpdateRequiredAttributeOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.ascatel.asymbix.server.blp.util.TimeFormatter;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.ATTRIBUTE_ENTITY_TYPE;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class UpdateRequiredAttributeOperation extends AbstractOperation {

    public static final String ATTRIBUTE_REQUIRED_PARAMETER_ID = "75";

    public static final String ATTRIBUTE_DEFAULT_VALUE_PARAMETER_ID = "79";

    public UpdateRequiredAttributeOperation(AbstractOperation operation) {
        super(operation);
    }

    public void updateRequiredAttributes(String entityType, String entityId, Map<String, ParameterValue> attrArray,
                                         boolean newEntity)
            throws BusinessLogicServiceException {

        final Collection<RequiredAttrData> requiredAttr = selectRequiredAttributes(entityType);

        for (RequiredAttrData attrData : requiredAttr) {

            final String attrId = attrData.getAttributeId();
            if (!attrArray.containsKey(attrId)) {
                if (newEntity) {
                    // Это новая сущность, т.е. других значений параметров кроме тех что передаются во входном
                    // массиве нет, т.е. значение для Required параметра не задано.
                    final String errMsg = new StringBuilder().append("The value for a required parameter")
                            .append(attrId).append(" class of ").append(entityType)
                            .append(" is not defined.").toString();
                    throw new BusinessLogicServiceException(errMsg);
                }

                // Если нет в задаваемых параметрах и это не новая сущность, то проверим было ли оно установлино ранее.
                final ParameterValue value = readParameterValue(entityType, entityId, attrId);

                final int attrType = attrData.getAttrType();
                if (!validateParameterValue(value, attrType)) {

                    // Значение не установлено, попробуем установить значение по умолчанию.
                    final String defaultValue = attrData.getDefaultValue();
                    if (defaultValue == null) {
                        final String errMsg = new StringBuilder().append("The value for a required parameter")
                                .append(attrId).append(" class of ").append(entityType)
                                .append(" is not defined and default value is not defined").toString();
                        throw new BusinessLogicServiceException(errMsg);
                    }

                    final ParameterValue defParamValue = parseValue(defaultValue, attrType);
                    updateParameterValue(entityType, entityId, attrId, defParamValue);
                }
            }
        }
    }

    private Collection<RequiredAttrData> selectRequiredAttributes(String entityType)
            throws BusinessLogicServiceException {

        final List<QueryItem> filter = new ArrayList<>();

        final ParameterValue requiredValue = buildParameterValue("true");
        final ParameterValue classValue = buildParameterValue(entityType);
        filter.add(buildQueryItem(ATTRIBUTE_REQUIRED_PARAMETER_ID, Operation.EQ, requiredValue));
        filter.add(buildQueryItem(Constants.CLASS_ID_ATTRIBUTE_PARAMETER, Operation.EQ, classValue));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), ATTRIBUTE_ENTITY_TYPE, filter);
        byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());

        final Map<String, RequiredAttrData> attributes = new HashMap<>();
        try {
            final ResultSet resultSet = ResultSet.parseFrom(responseData);
            final List<Parameter> parameters = resultSet.getParametersList();

            for (Parameter parameter : parameters) {
                final String entityId = parameter.getEntityId();
                final String parameterId = parameter.getAttributeId();

                RequiredAttrData attrData = attributes.get(entityId);
                if (attrData == null) {
                    attrData = new RequiredAttrData(entityId);
                    attributes.put(entityId, attrData);
                }

                if (ATTRIBUTE_DEFAULT_VALUE_PARAMETER_ID.equals(parameterId)) {
                    if (parameter.getValue().hasStringValue()) {
                        attrData.setDefaultValue(parameter.getValue().getStringValue());
                    }
                } else if (Constants.TYPE_ATTRIBUTE_PARAMETER.equals(parameterId)) {
                    attrData.setAttrType(parameter.getValue().getIntValue());
                }
            }
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }

        return attributes.values();
    }

    public boolean validateParameterValue(ParameterValue value, int typeCode) throws BusinessLogicServiceException {
        boolean hasCorrectValue;
        switch (typeCode) {
            case Constants.STRING_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasStringValue();
                break;
            case Constants.NUMBER_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasDoubleValue();
                break;
            case Constants.DATETIME_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasDatetimeValue();
                break;
            case Constants.MUISTRING_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasMuistringValue();
                break;
            case Constants.OBJECT_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasObjectLinkValue();
                break;
            case Constants.FILE_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasFileValue();
                break;
            case Constants.INTEGER_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasIntValue();
                break;
            case Constants.LONG_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasLongValue();
                break;
            case Constants.BOOLEAN_ATTRIBUTE_TYPE:
                hasCorrectValue = value.hasBooleanValue();
                break;
            case Constants.ARRAY_REQUEST_TYPE:
                hasCorrectValue = value.hasValueArray();
                break;
            default:
                throw new BusinessLogicServiceException(new StringBuilder("Unknown Parameter type ").append(typeCode)
                        .toString());
        }

        return hasCorrectValue;
    }

    public ParameterValue parseValue(String defaultValue, int parameterType)
            throws BusinessLogicServiceException {
        try {
            switch (parameterType) {
                case 1:
                    return MessageUtil.buildParameterValue(defaultValue);
                case 2:
                    final Double doubleValue = Double.parseDouble(defaultValue);
                    return MessageUtil.buildParameterValue(doubleValue);
                case 3:
                    final long timeValue = TimeFormatter.getInstance().convertMesasgeTime(defaultValue);
                    return MessageUtil.buildParameterValueDate(timeValue);
                case 4:
                    final BLSMessageParser.MUIString muiString = parseMUIStringValue(defaultValue);
                    return MessageUtil.buildParameterValue(muiString);
                case 5:
                    final BLSMessageParser.ObjectLink objectLink = parseObjectValue(defaultValue);
                    return MessageUtil.buildParameterValue(objectLink);
                case 6:
                    throw new UnsupportedOperationException("Unsupported yet");
                case 7:
                    final Integer integerValue = Integer.parseInt(defaultValue);
                    return MessageUtil.buildParameterValue(integerValue);
                case 8:
                    final Long longValue = Long.parseLong(defaultValue);
                    return MessageUtil.buildParameterValue(longValue);
                case 9:
                    final Boolean booleanValue = Boolean.parseBoolean(defaultValue);
                    return MessageUtil.buildParameterValue(booleanValue);
                default:
                    throw new IllegalArgumentException("Unsupported attribute type code=" + parameterType);
            }
        } catch (RuntimeException e) {
            String errMsg = new StringBuilder().append("Cannot parse default value ").append(defaultValue)
                    .append(" for type ").append(parameterType).toString();
            throw new BusinessLogicServiceException(errMsg, e);
        }
    }

    private BLSMessageParser.MUIString parseMUIStringValue(String strValue) {
        Map<String, String> muiValues = new HashMap<String, String>();

        String[] values = strValue.split(";");
        for (String value : values) {
            String[] splited = value.split("=");
            if (splited.length < 2) {
                throw new RuntimeException("Invalid muiString value for extension " + strValue);
            }
            String locale = splited[0].toLowerCase();
            muiValues.put(locale, splited[1]);
        }

        return MessageUtil.buildMUIString(muiValues);
    }

    private BLSMessageParser.ObjectLink parseObjectValue(String strValue) {
        List<String> links = new ArrayList<String>();

        String[] values = strValue.split(";");
        Collections.addAll(links, values);

        return MessageUtil.buildObjectLink(links);
    }

    private class RequiredAttrData {

        private String attributeId;

        private String defaultValue;

        private int attrType;

        private RequiredAttrData(String attributeId) {
            this.attributeId = attributeId;
        }

        public String getAttributeId() {
            return attributeId;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public int getAttrType() {
            return attrType;
        }

        public void setAttrType(int attrType) {
            this.attrType = attrType;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RequiredAttrData)) return false;

            RequiredAttrData attrData = (RequiredAttrData) o;

            if (!attributeId.equals(attrData.attributeId)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return attributeId.hashCode();
        }
    }
}
