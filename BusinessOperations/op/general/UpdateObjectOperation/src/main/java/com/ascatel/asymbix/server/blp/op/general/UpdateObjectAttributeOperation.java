/**
 * Class UpdateObjectAttributeOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.general;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import com.ascatel.asymbix.server.blp.util.ObjectLinkAttrRange;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildReadRequest;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.ATTRIBUTE_ENTITY_TYPE;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.TYPE_ATTRIBUTE_PARAMETER;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class UpdateObjectAttributeOperation extends AbstractOperation {

    public static final String ATTRIBUTE_RANGE_PARAMETER_ID = "72";

    public UpdateObjectAttributeOperation(AbstractOperation operation) {
        super(operation);
    }

    public void updateAttributes(String entityType, String entityId, Map<String, ParameterValue> attrArray)
            throws BusinessLogicServiceException {
        for (String parameterId : attrArray.keySet()) {
            final ParameterValue value = attrArray.get(parameterId);
            updateAttributes(entityType, entityId, parameterId, value);
        }
    }

    private void updateAttributes(String entityType, String entityId, String parameterId, ParameterValue value)
            throws BusinessLogicServiceException {
        final int attrType = readAttributeType(parameterId);
        if (Constants.OBJECT_ATTRIBUTE_TYPE == attrType) {
            updateObjectLinkAttribute(entityType, entityId, parameterId, value);
        } else {
            updateParameterValue(entityType, entityId, parameterId, value);
        }
    }

    /**
     * Сохраняем атрибут сущности {@code entityId} тип которого {@link Constants#OBJECT_ATTRIBUTE_TYPE}.
     * В таком случае считаем из параметра 'AttributeRange' атрибута 'Attr' - значение сопряженного атрибута и если оно
     * не равно 0, то обновляем также связи в сопряжённых объектах.
     *
     * @param entityType  тип сущности.
     * @param entityId    идентификатор сущности.
     * @param parameterId идентификатор атрибута.
     * @param value       значение атрибута.
     * @throws BusinessLogicServiceException при возникновении каких-либо ошибок.
     */
    private void updateObjectLinkAttribute(String entityType, String entityId, String parameterId,
                                           ParameterValue value)
            throws BusinessLogicServiceException {

        final ObjectLinkAttrRange attrRange = readAttributeRange(parameterId);
        if (attrRange.getAttributeId() != null) {
            // Есть сопряженный атрибут, надо обновить значения так же и в нем.

            // Читаем текущее значение.
            ParameterValue currentValue = readParameterValue(entityType, entityId, parameterId);
            if (currentValue.hasObjectLinkValue()) { // значение не пустое - правим сопряженные сущности
                final ObjectLink currentLinks = currentValue.getObjectLinkValue();
                final ObjectLink newLinks = value.getObjectLinkValue();

                // Составляем набор ItemsToDel значений ID сущностей, содержащихся в текущем значении атрибута currentValue,
                // но не содержащихся во входном параметре value.
                String[] itemsToDel = buildItemsToDel(currentLinks, newLinks);

                // Составляем набор ItemsToIns значений ID сущностей, содержащихся во входном параметре value,
                // но не содержащихся в текущем значении атрибута currentValue.
                String[] itemsToIns = buildItemsToIns(currentLinks, newLinks);

                // Удаляем себя из сопряженных сущностей на которые нет ссылок в новом значении атрибута.
                deleteFromPairedItems(entityId, attrRange, itemsToDel);

                // Добавим себя в сопряженные сущности на которые появились ссылки в новом значении атрибута.
                insertToPairedItems(entityId, attrRange, itemsToIns);
            }
        }

        updateParameterValue(entityType, entityId, parameterId, value);
    }

    private void deleteFromPairedItems(String entityId, ObjectLinkAttrRange attrRange, String[] itemsToDel)
            throws BusinessLogicServiceException {

        // Идентификатор сопряженного класса.
        final String pairedType = attrRange.getEntityType();
        // Идентификатор сопряженного атрибута класса.
        final String pairedAttrId = attrRange.getAttributeId();

        for (String pairedId : itemsToDel) {
            final ParameterValue pairedValue = readParameterValue(pairedType, pairedId, pairedAttrId);

            final ObjectLink links = pairedValue.getObjectLinkValue();
            final ObjectLink.Builder newLinksBuilder = ObjectLink.newBuilder();
            for (String link : links.getLinksList()) {
                if (!entityId.equals(link)) {
                    newLinksBuilder.addLinks(link);
                }
            }

            final ObjectLink newLinks = newLinksBuilder.build();
            final ParameterValue newObjectValue = buildParameterValue(newLinks);
            updateParameterValue(pairedType, pairedId, pairedAttrId, newObjectValue);
        }
    }

    private void insertToPairedItems(String entityId, ObjectLinkAttrRange attrRange, String[] itemsToIns)
            throws BusinessLogicServiceException {
        // Идентификатор сопряженного класса.
        final String pairedType = attrRange.getEntityType();
        // Идентификатор сопряженного атрибута класса.
        final String pairedAttrId = attrRange.getAttributeId();

        for (String pairedId : itemsToIns) {
            ParameterValue pairedValue = readParameterValue(pairedType, pairedId, pairedAttrId);

            final List<String> idList;
            if (pairedValue.hasObjectLinkValue()) {
                final ObjectLink links = pairedValue.getObjectLinkValue();

                idList = links.getLinksList();
                if (!idList.contains(entityId)) {
                    idList.add(entityId);
                }
            } else {
                idList = new ArrayList<>();
                idList.add(entityId);
            }

            final ObjectLink newLinks = ObjectLink.newBuilder().addAllLinks(idList).build();
            final ParameterValue newObjectValue = buildParameterValue(newLinks);
            updateParameterValue(pairedType, pairedId, pairedAttrId, newObjectValue);
        }
    }


    /**
     * Составляем набор ItemsToDel значений ID сущностей, содержащихся в текущем значении атрибута {@code currentLinks},
     * но не содержащихся во входном параметре {@code newLinks}.
     *
     * @param currentLinks текущее множество значений.
     * @param newLinks     новое множество значений.
     * @return идентификаторы которые не входят в новое множество значений.
     */
    private String[] buildItemsToDel(ObjectLink currentLinks, ObjectLink newLinks) {
        final Set<String> currentValues = new HashSet<>(currentLinks.getLinksList());
        final Set<String> newValues = new HashSet<>(newLinks.getLinksList());

        currentValues.removeAll(newValues);
        int size = currentValues.size();
        return currentValues.toArray(new String[size]);
    }

    /**
     * Составляем набор ItemsToIns значений ID сущностей, содержащихся во входном параметре {@code newLinks},
     * но не содержащихся в текущем значении атрибута currentValue {@code currentLinks}.
     *
     * @param currentLinks текущее множество значений.
     * @param newLinks     новое множество значений.
     * @return идентификаторы которые не входят в текущее множество значений.
     */
    private String[] buildItemsToIns(ObjectLink currentLinks, ObjectLink newLinks) {
        final Set<String> currentValues = new HashSet<>(currentLinks.getLinksList());
        final Set<String> newValues = new HashSet<>(newLinks.getLinksList());

        newValues.removeAll(currentValues);
        int size = newValues.size();
        return newValues.toArray(new String[size]);
    }

    private ObjectLinkAttrRange readAttributeRange(String parameterId)
            throws BusinessLogicServiceException {
        final ReadRequest request
                = buildReadRequest(getBlockId(), ATTRIBUTE_ENTITY_TYPE, parameterId, ATTRIBUTE_RANGE_PARAMETER_ID);
        byte[] responseData = getEnv().getBlService().getParameterValue(request.toByteArray());
        try {
            final ParameterValue value = ParameterValue.parseFrom(responseData);
            return ObjectLinkAttrRange.parseAttrRange(value.getStringValue());
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    private int readAttributeType(String parameterId)
            throws BusinessLogicServiceException {
        final ReadRequest request
                = buildReadRequest(getBlockId(), ATTRIBUTE_ENTITY_TYPE, parameterId, TYPE_ATTRIBUTE_PARAMETER);

        byte[] responseData = getEnv().getBlService().getParameterValue(request.toByteArray());
        try {
            final ParameterValue value = ParameterValue.parseFrom(responseData);
            return value.getIntValue();
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }
}
