/**
 * Class DeleteCOServer
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 29.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.AbortTransactionException;
import com.ascatel.asymbix.server.blp.AbstractRequestServer;
import com.ascatel.asymbix.server.blp.CommitTransactionException;
import com.ascatel.asymbix.server.blp.StartTransactionException;
import com.ascatel.asymbix.server.blp.messaging.DeleteCOParser.DeleteCORequest;
import com.ascatel.asymbix.server.blp.messaging.DeleteCOParser.DeleteCOResponse;
import com.ascatel.asymbix.server.blp.messaging.DeleteCOParser.State;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import static com.ascatel.asymbix.server.blp.op.co.DeleteCOServer.ResponseHelper.getDefectiveResponse;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("DeleteCOServer_Implementation")
public class DeleteCOServer extends AbstractRequestServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    public static final int REQUEST_MESSAGE_TYPE = 433;

    public static final int RESPONSE_MESSAGE_TYPE = 434;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public DeleteCOServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }


    @Override
    protected void processRequest(MessageLite request)
            throws InvalidProtocolBufferException {
        final DeleteCORequest proto = DeleteCORequest.parseFrom(request.toByteArray());

        final DeleteCOOperation operation = new DeleteCOOperation(getEnvironment());
        operation.setInstanceId(proto.getInstanceId());
        operation.setLocale(null);
        operation.setBlockId(proto.getMessageBlock());

        final String refDataId = Long.toString(proto.getRefDataId());
        final String referenceId = Long.toString(proto.getReferenceId());

        processOperation(operation, referenceId, refDataId);
    }

    private void processOperation(DeleteCOOperation operation, String referenceId, String refDataId) {
        final int blockId = operation.getBlockId();
        final String instanceId = operation.getInstanceId();

        try {
            startTransaction();
            operation.deleteCO(referenceId, refDataId);
            commitTransaction();
            sendResponse(blockId, instanceId);
        } catch (StartTransactionException e) {
            // Упали на старьте транзакции.
            final String errMsg = "Cannot start transaction. " + e.getMessage();
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg));
        } catch (CommitTransactionException e) {
            // Не удалось закоммитить изменения.
            final String errMsg = "Unnable to commit the changes. " + e.getMessage();
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg));
        } catch (BusinessLogicServiceException e) {
            // Какие либо ошибки при выполнении операции.
            // Надо откатитьтранзакцию и послать ошибку.
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(e.getMessage());
            try {
                abortTransaction();
            } catch (AbortTransactionException abortEx) {
                errMsg.append("; ").append(abortEx.getMessage());
            }
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg.toString()));
        }
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    private void sendResponse(int blockId, String instanceId) {
        DeleteCOResponse.Builder builder = ResponseHelper.getResponseBuilder(blockId, instanceId);
        builder.setIsSuccess(true);
        sendResponse(builder.build());
    }

    public static class ResponseHelper {

        public static MessageLite getDefectiveResponse(int blockNumber, String instanceId, String errMsg) {
            DeleteCOResponse.Builder builder = getResponseBuilder(blockNumber, instanceId);
            builder.setBlockState(State.DEFECTIVE).setErrorMessage(errMsg);

            return builder.build();
        }

        public static MessageLite getEmptyResponse(String instanceId) {
            DeleteCOResponse.Builder builder = getResponseBuilder(0, instanceId);
            builder.setBlockState(State.LAST);
            return builder.build();
        }

        public static DeleteCOResponse.Builder getResponseBuilder(int blockNumber, String instanceId) {
            DeleteCOResponse.Builder builder = DeleteCOResponse.newBuilder();

            builder.setMessageType(RESPONSE_MESSAGE_TYPE)
                    .setInstanceId(instanceId)
                    .setBlockNumber(blockNumber);

            return builder;
        }

    }


}
