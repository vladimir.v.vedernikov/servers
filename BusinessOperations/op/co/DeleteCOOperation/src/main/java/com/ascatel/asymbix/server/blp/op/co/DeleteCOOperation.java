/**
 * Class DeleteCOOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 29.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.Environment;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.op.general.DeleteObjectOperation;
import com.ascatel.asymbix.server.blp.op.is.DeregisterISOperation;
import ru.omnicomm.pegasus.processingPlatform.logging.Level;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.OMIdentifiers.R_BIND_CO_PARAMETER_ID;
import static com.ascatel.asymbix.server.blp.OMIdentifiers.R_ENTITY_TYPE;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Операция 'Удалить КО'.
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code referenceId} – идентификатор класса удаляемой сущности.</li>
 * <li>{@code refDataId} - идентификатор удаляемой сущности.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class DeleteCOOperation extends AbstractOperation {

    public DeleteCOOperation(AbstractOperation handler) {
        super(handler);
    }

    public DeleteCOOperation(Environment env) {
        super(env);
    }

    public void deleteCO(String referenceId, String refDataId)
            throws BusinessLogicServiceException {
        // Сначала удаляем все датчики, пользуясь стандартной операцией дерегистрации датчика.
        deleteRegistrators(refDataId);

        // Удаляем "пустые" агрегаты (хотя такие вроде бы должны быть удалены при операции дерегистрации датчика,
        // доп. проверка не помешает)
        deleteEmptyCU(refDataId);

        // Удаляем из категорий.
        deleteItselfFromCAT(refDataId);

        // Удаляем объект с идентификатором COid, выполняя шаги 6-8 пункта 3.1.3.
        // документаTMP-BLS-01-(General Operations).docx
        // !!! Вызываем полностью всю операцию.
        deleteCOInCommon(refDataId);
    }

    private void deleteCOInCommon(String coId)
            throws BusinessLogicServiceException {
        final DeleteObjectOperation deleteOp = new DeleteObjectOperation(this);
        deleteOp.deleteObject(CO_ENTITY_TYPE, coId);
    }

    private void deleteItselfFromCAT(String coId)
            throws BusinessLogicServiceException {

        final ParameterValue bindCatValue = readParameterValue(CO_ENTITY_TYPE, coId, CO_BIND_CAT_PARAMETER_ID);
        final List<String> catIds = extractObjectLinks(bindCatValue);
        for (String catId : catIds) {
            final ParameterValue catBindToCOValue
                    = readParameterValue(COC_ENTITY_TYPE, catId, COC_BIND_CO_PARAMETER_ID);

            final List<String> newLinksList = new ArrayList<>();
            for (String link : catBindToCOValue.getObjectLinkValue().getLinksList()) {
                if (!coId.equals(link)) {
                    newLinksList.add(link);
                }
            }

            final ObjectLink newObjLink = buildObjectLink(newLinksList);
            final ParameterValue newValue = buildParameterValue(newObjLink);
            updateParameterValue(COC_ENTITY_TYPE, catId, COC_BIND_CO_PARAMETER_ID, newValue);
        }
    }

    private void deleteEmptyCU(String coId)
            throws BusinessLogicServiceException {
        // Находим все агрегаты  КО, выполняя базовую операцию 'Найти сущности по условию'
        // [EntityType = idклассаControlled Unit(1300) AND BindCO = <CoId>]
        final List<QueryItem> cuFilter = new ArrayList<>();
        cuFilter.add(buildQueryItem(CU_BIND_CO_PARAMETER_ID, Operation.EQ, buildParameterValue(coId)));

        final SelectRequest cuRequest = buildSelectRequest(getBlockId(), getLocale(), CU_ENTITY_TYPE, cuFilter);
        final ResultSet cuResultSet = selectEntities(cuRequest);
        final Collection<String> cuIds = new HashSet<>();
        for (Parameter parameter : cuResultSet.getParametersList()) {
            cuIds.add(parameter.getEntityId());
        }

        // По набору, полученному на предыдущем шаге находим все сущности класса InstalledSensor, установленные на
        // любом из указанных агрегатов, выполняя базовую операциюНайти сущности по условию
        // [EntityType = <idклассаInstalledSensor> AND BindCU IN <набор значений полученных на шаге "a">]
        final ParameterValue inCUValue = buildParameterValue(cuIds);
        final List<QueryItem> isFilter = new ArrayList<>();
        isFilter.add(buildQueryItem(IS_BIND_CU_PARAMETER_ID, Operation.IN, inCUValue));

        final SelectRequest isRequest = buildSelectRequest(getBlockId(), getLocale(), IS_ENTITY_TYPE, isFilter);
        final ResultSet isResultSet = selectEntities(isRequest);

        // Если запрос вернул хоть одну запись, поднимаем ошибку и откатываем транзакции – EXCEPTION
        // (мы удалили все регистраторы и все их датчики на шаге 2, не должно быть больше никаких ещё датчиков).
        // (ограничение нельзя установить датчик без регистратора).
        if (isResultSet.getParametersCount() > 0) {
            throw new BusinessLogicServiceException("There should not be any more Installed Sensors");
        }

        // Вызываем операцию Удалить экземпляр класса. Передаём CUid.
        for (String cuId : cuIds) {
            deleteEntity(CU_ENTITY_TYPE, cuId);
        }
    }

    private void deleteRegistrators(String coId)
            throws BusinessLogicServiceException {
        //Находим все регистраторы, установленные на КО. EntityType = idклассаRegistrator(1200); ANDBindCO = <CoId>
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(R_BIND_CO_PARAMETER_ID, Operation.EQ, buildParameterValue(coId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), R_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Set<String> rIds = new HashSet<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            rIds.add(parameter.getEntityId());
        }


//        //TODO REMOVE
//        final Logger logger = LoggerFactory.getLogger();
//        logger.log(Level.INFO, "Registrators binded to CO=" + coId);
//        for (String rId : rIds) {
//            logger.log(Level.INFO, "\t> " + rId);
//        }
//        //TODO REMOVE

        //  Дерегистрируем датчики.
        deregistrateIS(rIds);

        // Удаляем все регистраторы.
        for (String rId : rIds) {
            final DeleteCOOperation deleteRegistratorOp = new DeleteCOOperation(this);
            deleteRegistratorOp.deleteRegistrators(rId);
        }
    }

    private void deregistrateIS(Set<String> rIds)
            throws BusinessLogicServiceException {
        // По набору, полученному на предыдущем шаге находим все сущности класса InstalledSensor, установленные на
        // любом из указанных регистраторов, выполняя базовую операцию Найти сущности по условию, устанавливаем:
        // Condition = [EntityType = <idклассаInstalledSensor(1800)> AND BindReg IN <набор значений полученных на
        // шаге "a">]

        final ParameterValue inRegValue = buildParameterValue(rIds);
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(IS_BIND_REG_PARAMETER_ID, Operation.IN, inRegValue));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), IS_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

//        final Logger logger = LoggerFactory.getLogger();//TODO REMOVE

        String currentIsId = null;
        for (Parameter parameter : resultSet.getParametersList()) {
            final String entityId = parameter.getEntityId();
            if (!entityId.equals(currentIsId)) {
                final DeregisterISOperation delOp = new DeregisterISOperation(this);
                delOp.deregisterIS(IS_ENTITY_TYPE, entityId);

//                logger.log(Level.INFO, "IS " + entityId + " has been deregistrated."); //TODO REMOVE

                currentIsId = entityId;
            }
        }
    }

}
