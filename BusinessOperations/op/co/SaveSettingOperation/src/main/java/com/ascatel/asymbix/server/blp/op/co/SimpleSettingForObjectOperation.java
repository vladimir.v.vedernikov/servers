/**
 * Class SimpleSettingForObjectOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.ascatel.asymbix.server.blp.util.TimeFormatter;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.base.Constants;

import java.util.*;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.ATTRIBUTE_ENTITY_TYPE;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class SimpleSettingForObjectOperation extends AbstractOperation {

    public SimpleSettingForObjectOperation(AbstractOperation operation) {
        super(operation);
    }

    public List<String> createSimpleSettingForObject(String cppsId, String s, String setName, String val)
            throws BusinessLogicServiceException {

        // Формируем setting_key = S-SetName
        final String settingKey
                = new StringBuilder(s).append("-").append(setName).toString();

        final String valuesRange = readCPPSValuesRange(cppsId);
        if (valuesRange == null || valuesRange.isEmpty()) {
            throw new BusinessLogicServiceException("Parameter ValuesRange for CPPS=" + cppsId + " doesn't defined");
        }
        final String[] splitted = valuesRange.split(";");
        if (splitted.length < 1) {
            throw new BusinessLogicServiceException("Invalid format for value" + valuesRange
                    + " of parameter ValuesRange for CPPS=" + cppsId + " doesn't defined");
        }

        final String bindedEntityType = splitted[0];
        final String[] bindedEntityIds = val.split(";");


        final Map<String, AttributeData> metadata = readMetadata(bindedEntityType);
        final Collection<ObjectEntity> objects = readObjectEntities(bindedEntityType, bindedEntityIds);

        final List<String> simpleSettingIds = new ArrayList<>();
        for (ObjectEntity objectEntity : objects) {
            StringBuilder sb = new StringBuilder();

            for (BLSMessageParser.Parameter parameter : objectEntity.getParameters()) {
                final String paramId = parameter.getAttributeId();
                if (Constants.TYPE_ENTITY_PARAMETER.equals(paramId) || Constants.BLOCK_ID_PARAMETER.equals(paramId)) {
                    continue;
                }
                final AttributeData attrData = metadata.get(paramId);
                if (attrData == null) {
                    throw new BusinessLogicServiceException("Unknown parameter=" + paramId);
                }

                final String attrName = attrData.getName();
                final int attrType = attrData.getType();
                final String attrValue = extractValue(attrType, parameter.getValue());

                sb.append(attrName).append("=").append(attrValue).append(";");
            }


            final String settingValue = sb.toString();
            simpleSettingIds.add(createSimpleSetting(settingKey, settingValue));
        }

        return simpleSettingIds;
    }

    private String extractValue(int attrType, BLSMessageParser.ParameterValue value) {
        switch (attrType) {
            case 1:
                return value.getStringValue();
            case 2:
                Double doubleValue = value.getDoubleValue();
                return doubleValue.toString();
            case 3:
                int inetegerValue = value.getIntValue();
                return TimeFormatter.getInstance().convertMesasgeTime(inetegerValue);
            case 4:
                StringBuilder sb = new StringBuilder();
                BLSMessageParser.MUIString muiString = value.getMuistringValue();

                List<BLSMessageParser.MUIString.MUIValue> values = muiString.getStrValuesList();
                boolean first = true;
                for (BLSMessageParser.MUIString.MUIValue muiValue : values) {
                    final String locale = muiValue.getLangID();
                    final String string = muiValue.getValue();

                    if (first) {
                        first = false;
                    } else {
                        sb.append(";");
                    }
                    sb.append(locale).append("=").append(string);
                }
                return sb.toString();
            case 5:
                BLSMessageParser.ObjectLink objectLink = value.getObjectLinkValue();
                List<String> links = objectLink.getLinksList();
                sb = new StringBuilder();
                first = true;
                for (String link : links) {
                    if (first) {
                        first = false;
                    } else {
                        sb.append(";");
                    }
                    sb.append(link);
                }
                return sb.toString();
            case 6:
                return "bytes";
            case 7:
                int integer = value.getIntValue();
                return Integer.toString(integer);
            case 8:
                long longValue = value.getLongValue();
                return Long.toString(longValue);
            case 9:
                boolean booleanValue = value.getBooleanValue();
                return Boolean.toString(booleanValue);
            default:
                throw new IllegalArgumentException("Unsupported attribute type code=" + attrType);
        }
    }

    private String createSimpleSetting(String key, String value)
            throws BusinessLogicServiceException {
        final BLSMessageParser.CreateRequest createRequest = buildCreateRequest(SIMPLE_SETTING_ENTITY_TYPE);
        final String settingEntityId = getEnv().getBlService().createEntity(createRequest.toByteArray());

        final BLSMessageParser.ParameterValue keyValue = buildParameterValue(key);
        final BLSMessageParser.UpdateRequest updKeyRequest = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_KEY_PARAMETER_ID, keyValue);

        final BLSMessageParser.ParameterValue valueValue = buildParameterValue(value);
        final BLSMessageParser.UpdateRequest updValueRequest = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_VALUE_PARAMETER_ID, valueValue);

        getEnv().getBlService().setParameterValue(updKeyRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updValueRequest.toByteArray());
        return settingEntityId;
    }

    private Map<String, AttributeData> readMetadata(String bindedEntityType)
            throws BusinessLogicServiceException {

        final List<BLSMessageParser.QueryItem> filter = new ArrayList<>();

        final BLSMessageParser.ParameterValue classValue = buildParameterValue(bindedEntityType);
        filter.add(buildQueryItem(Constants.CLASS_ID_ATTRIBUTE_PARAMETER, BLSMessageParser.QueryItem.Operation.EQ, classValue));

        final BLSMessageParser.SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), ATTRIBUTE_ENTITY_TYPE, filter);
        byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());

        final Map<String, AttributeData> metadata = new HashMap<>();
        try {
            final BLSMessageParser.ResultSet resultSet = BLSMessageParser.ResultSet.parseFrom(responseData);
            final List<BLSMessageParser.Parameter> parameters = resultSet.getParametersList();

            for (BLSMessageParser.Parameter parameter : parameters) {
                final String entityId = parameter.getEntityId();
                final String parameterId = parameter.getAttributeId();

                AttributeData attrData = metadata.get(entityId);
                if (attrData == null) {
                    attrData = new AttributeData();
                    metadata.put(entityId, attrData);
                }

                if (Constants.NAME_ATTRIBUTE_PARAMETER.equals(parameterId)) {
                    if (parameter.getValue().hasStringValue()) {
                        attrData.setName(parameter.getValue().getStringValue());
                    }
                } else if (Constants.TYPE_ATTRIBUTE_PARAMETER.equals(parameterId)) {
                    attrData.setType(parameter.getValue().getIntValue());
                }
            }
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }

        return metadata;
    }

    private Collection<ObjectEntity> readObjectEntities(String entityType, String[] entityIds)
            throws BusinessLogicServiceException {
        List<BLSMessageParser.ParameterValue> paramValues = new ArrayList<>(entityIds.length);
        for (String entityId : entityIds) {
            paramValues.add(MessageUtil.buildParameterValue(entityId));
        }
        BLSMessageParser.ValueArray valueArray = MessageUtil.buildValueArray(paramValues);
        BLSMessageParser.ParameterValue filterValue = MessageUtil.buildParameterValue(valueArray);


        final List<BLSMessageParser.QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(BusinessLogicService.ENTITY_ID_ATTRIBUTE, BLSMessageParser.QueryItem.Operation.IN, filterValue));

        final BLSMessageParser.SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), entityType, filter);
        byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());


        final Map<String, ObjectEntity> objects = new HashMap<>();
        try {
            BLSMessageParser.ResultSet resultSet = BLSMessageParser.ResultSet.parseFrom(responseData);
            for (BLSMessageParser.Parameter parameter : resultSet.getParametersList()) {
                final String entityId = parameter.getEntityId();

                ObjectEntity ebjEntity = objects.get(entityId);
                if (ebjEntity == null) {
                    ebjEntity = new ObjectEntity(entityId);
                    objects.put(entityId, ebjEntity);
                }

                ebjEntity.addParameter(parameter);
            }
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }

        return objects.values();
    }


    private String readCPPSValuesRange(String cppsId)
            throws BusinessLogicServiceException {
        final BLSMessageParser.ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_VALUES_RANGE_PARAMETER_ID);
        return value.getStringValue();
    }

    private class ObjectEntity {

        private String entityId;

        private List<BLSMessageParser.Parameter> parameters = new ArrayList<>();

        private ObjectEntity(String entityId) {
            this.entityId = entityId;
        }

        public void addParameter(BLSMessageParser.Parameter parameter) {
            parameters.add(parameter);
        }

        public List<BLSMessageParser.Parameter> getParameters() {
            return parameters;
        }
    }

    private class AttributeData {

        private String name;
        private int type;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
