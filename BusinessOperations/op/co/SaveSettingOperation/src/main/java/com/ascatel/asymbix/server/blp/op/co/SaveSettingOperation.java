/**
 * Class SaveSettingOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ReadRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.UpdateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ObjectLink;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildDeleteRequest;
import static ru.omnicomm.pegasus.processingPlatform.services.base.Constants.TYPE_ENTITY_PARAMETER;

/**
 * Операция 'Сохранение настройки'.
 * Входные параметры:
 * <ul>
 * <li>{@code cppsId} - ссылка на экземпляр объекта Controlled Parameter Processing Setting.</li>
 * <li>{@code asId} - идентификатор на экземпляр класса Atomic Sensor к которому относится настройка.</li>
 * <li>{@code val} - строка значение в формате соответствующем настройке.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class SaveSettingOperation extends AbstractOperation {

    public static final String GLOBAL_SUFFIX = "1";

    public SaveSettingOperation(AbstractOperation operation) {
        super(operation);
    }

    /**
     * Операция выполняет сохранение значения настройки для заданного SourceId.
     * Операция внутренняя. Вызывается из операции "Сохранение КО".
     *
     * @param cppsId cсылка на экземпляр объекта Controlled Parameter Processing Setting.
     * @param asId   идентификатор на экземпляр класса Atomic Sensor к которому относится настройка.
     * @param val    строка значение в формате соответствующем настройке.
     * @throws BusinessLogicServiceException при ошибках во время работы.
     */
    public void saveSetting(String cppsId, String asId, String val)
            throws BusinessLogicServiceException {

        validateParameters(cppsId, asId);

        final String cppsv = getCPPSV(cppsId, asId);

        updateCPPSValue(cppsv, val);

        //////
        // Далее обновляем соответствующие настройки (т.е. создаём новые объекты типа Simple Settings)
        //////
        final String suffix = defineSuffix(cppsId, asId);

        // Сначала удаляем все текущие настройки.
        deleteCurrentSettings(cppsv);

        final String s = readASBindSourceId(asId);

        final String setName = readCPPSName(cppsId);

        final int type = readCPPSType(cppsId);

        // Формируем новые настройки.
        final List<String> simpleSettingsIds = createSettings(cppsId, type, s, setName, suffix, val);

        // Добавляем к значению атрибута BindSimpSet сущности CPPSV идентификаторы настроек
        updateCurrentSettings(cppsv, simpleSettingsIds);
    }

    private List<String> createSettings(String cppsId, int type, String s, String setName, String suffix, String val)
            throws BusinessLogicServiceException {
        switch (type) {
            case 1: // Number
            case 3: // String
                return createSimpleSettingForNumAndStr(s, setName, suffix, val);
            case 2: // Datetime
                return createSimpleSettingForDatetime(s, setName, suffix, val);
            case 4: // Table
                return createSimpleSettingForTable(s, setName, suffix, val);
            case 5: // Object
                final SimpleSettingForObjectOperation operation = new SimpleSettingForObjectOperation(this);
                return operation.createSimpleSettingForObject(cppsId, s, setName, val);
            default:
                throw new BusinessLogicServiceException("Unsupported CPPS type=" + type);
        }

    }

    private List<String> createSimpleSettingForTable(String s, String setName, String suffix, String val) {
        //TODO IMPLEMENT
        throw new UnsupportedOperationException();
    }

    private List<String> createSimpleSettingForDatetime(String s, String setName, String suffix, String val)
            throws BusinessLogicServiceException {
        // Формируем настройку:
        //     setting_key = S-SetName-I
        //     setting_value = количество секунд в значении Val, прошедших 01.01.2011 (может быть отрицательным)
        final String settingKey
                = new StringBuilder(s).append("-").append(setName).append("-").append(suffix).toString();

        final String simpleSettingId = createSimpleSetting(settingKey, val);
        return Arrays.asList(simpleSettingId);
    }

    private List<String> createSimpleSettingForNumAndStr(String s, String setName, String suffix, String val)
            throws BusinessLogicServiceException {
        // Формируем настройку:
        //     setting_key = S-SetName-I
        //     setting_value = Val
        final String settingKey
                = new StringBuilder(s).append("-").append(setName).append("-").append(suffix).toString();

        final String simpleSettingId = createSimpleSetting(settingKey, val);
        return Arrays.asList(simpleSettingId);
    }

    private void updateCurrentSettings(String cppsv, List<String> simpleSettingsIds)
            throws BusinessLogicServiceException {
        final ObjectLink objectLink = buildObjectLink(simpleSettingsIds);
        final ParameterValue settingsValue = buildParameterValue(objectLink);
        updateParameterValue(CPPSV_ENTITY_TYPE, cppsv, CPPSV_BIND_SIMP_SET_PARAMETER_ID, settingsValue);
    }

    private void deleteCurrentSettings(String cppsvId)
            throws BusinessLogicServiceException {
        final ParameterValue simpSetValue
                = readParameterValue(CPPSV_ENTITY_TYPE, cppsvId, CPPSV_BIND_SIMP_SET_PARAMETER_ID);
        for (String simpSetId : simpSetValue.getObjectLinkValue().getLinksList()) {
            final DeleteRequest deleteRequest = buildDeleteRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE, simpSetId);
            getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
        }
    }

    private String defineSuffix(String cppsId, String asId)
            throws BusinessLogicServiceException {
        boolean isGlobal = readIsGlobal(cppsId);

        boolean isSensIndep = readIsSensIndep(cppsId);

        if (!isGlobal || !isSensIndep) {
            //Считываем значение атрибута Number сущности ASid. Полученное значение присваиваем переменной I
            return readASNumber(asId);
        } else {
            //Suffix = 1 (у глобальных и не зависящих от датчика настроек, будет суффикс 1 - такой же как и настроек
            // для 1-го датчика)
            return GLOBAL_SUFFIX;
        }
    }


    private void validateParameters(String cppsId, String asId)
            throws BusinessLogicServiceException {
        checkExistence(CPPS_ENTITY_TYPE, cppsId);
        checkExistence(AS_ENTITY_TYPE, asId);
    }

    private void checkExistence(String entityType, String entityId)
            throws BusinessLogicServiceException {
        final ReadRequest request = buildReadRequest(getBlockId(), entityType, entityId, TYPE_ENTITY_PARAMETER);
        final byte[] data = getEnv().getBlService().getParameterValue(request.toByteArray());
        try {
            final ParameterValue value = ParameterValue.parseFrom(data);
            if (!value.hasStringValue()) {
                final String errMsg = new StringBuilder().append("Entity with id ").append(entityId)
                        .append(" of class").append(entityType).append(" is not exist").toString();
                throw new BusinessLogicServiceException(errMsg);
            }
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException(e);
        }
    }

    private void updateCPPSValue(String cppsv, String val)
            throws BusinessLogicServiceException {
        final ParameterValue value = MessageUtil.buildParameterValue(val);
        updateParameterValue(CPPSV_ENTITY_TYPE, cppsv, CPPSV_VALUE_PARAMETER_ID, value);
    }

    private String getCPPSV(String cppsId, String asId)
            throws BusinessLogicServiceException {

        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(CPPSV_BIND_CPPS_PARAMETER_ID, Operation.EQ, buildParameterValue(cppsId)));
        filter.add(buildQueryItem(CPPSV_BIND_AS_PARAMETER_ID, Operation.EQ, buildParameterValue(asId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
        byte[] responseData = getEnv().getBlService().selectByParams(request.toByteArray());

        try {
            final ResultSet resultSet = ResultSet.parseFrom(responseData);
            final List<Parameter> parameters = resultSet.getParametersList();

            if (parameters.isEmpty()) {
                return createCPPSV(cppsId, asId);
            } else {
                return parameters.get(0).getEntityId();
            }

        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    private String createCPPSV(String cppsId, String asId)
            throws BusinessLogicServiceException {

        final CreateRequest createRequest = MessageUtil.buildCreateRequest(getBlockId(), CPPSV_ENTITY_TYPE);
        final String cppsvId = getEnv().getBlService().createEntity(createRequest.toByteArray());

        final ParameterValue cppsLink = buildParameterValue(buildObjectLink(Arrays.asList(cppsId)));
        final UpdateRequest updateCPPS
                = buildUpdateRequest(getBlockId(), CPPSV_ENTITY_TYPE, cppsvId, CPPSV_BIND_CPPS_PARAMETER_ID, cppsLink);
        getEnv().getBlService().setParameterValue(updateCPPS.toByteArray());

        final ParameterValue asLink = buildParameterValue(buildObjectLink(Arrays.asList(asId)));
        final UpdateRequest updateAS
                = buildUpdateRequest(getBlockId(), CPPSV_ENTITY_TYPE, cppsvId, CPPSV_BIND_AS_PARAMETER_ID, asLink);
        getEnv().getBlService().setParameterValue(updateAS.toByteArray());

        return cppsvId;
    }


    private String readCPPSName(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_NAME_PARAMETER_ID);
        return value.getStringValue();
    }

    private int readCPPSType(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_TYPE_PARAMETER_ID);
        return value.getIntValue();
    }

    private String readCPPSValuesRange(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_VALUES_RANGE_PARAMETER_ID);
        return value.getStringValue();
    }

    private String readASBindSourceId(String asId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(AS_ENTITY_TYPE, asId, AS_BIND_SOURCE_PARAMETER_ID);
        String sourceId = null;
        if (value.hasObjectLinkValue()) {
            final ObjectLink objLinks = value.getObjectLinkValue();
            final List<String> links = objLinks.getLinksList();
            if (!links.isEmpty()) {
                sourceId = links.get(0);
            }
        }

        if (sourceId == null) {
            throw new BusinessLogicServiceException("Cannot find parameter value BindSourceId for AS=" + asId);
        }
        return sourceId;
    }

    private String readASNumber(String asId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(AS_ENTITY_TYPE, asId, AS_NUMBER_PARAMETER_ID);
        int intValue = Double.valueOf(value.getDoubleValue()).intValue();
        return Integer.toString(intValue);
    }

    private boolean readIsSensIndep(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_IS_SENSINDEP_PARAMETER_ID);
        return value.getBooleanValue();
    }

    private boolean readIsGlobal(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_IS_GLOBAL_PARAMETER_ID);
        return value.getBooleanValue();
    }

    private String createSimpleSetting(String key, String value)
            throws BusinessLogicServiceException {
        final CreateRequest createRequest = buildCreateRequest(SIMPLE_SETTING_ENTITY_TYPE);
        final String settingEntityId = getEnv().getBlService().createEntity(createRequest.toByteArray());

        final ParameterValue keyValue = buildParameterValue(key);
        final UpdateRequest updKeyRequest = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_KEY_PARAMETER_ID, keyValue);

        final ParameterValue valueValue = buildParameterValue(value);
        final UpdateRequest updValueRequest = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_VALUE_PARAMETER_ID, valueValue);

        getEnv().getBlService().setParameterValue(updKeyRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updValueRequest.toByteArray());
        return settingEntityId;
    }

}
