/**
 * Class SaveCOServer
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.AbortTransactionException;
import com.ascatel.asymbix.server.blp.AbstractRequestServer;
import com.ascatel.asymbix.server.blp.CommitTransactionException;
import com.ascatel.asymbix.server.blp.StartTransactionException;
import com.ascatel.asymbix.server.blp.messaging.SaveCOParser;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import ru.omnicomm.pegasus.processingPlatform.ServerImplementation;
import ru.omnicomm.pegasus.processingPlatform.annotations.ServerConstructor;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;
import ru.omnicomm.pegasus.processingPlatform.services.jms.JmsBridgeService;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;

import com.ascatel.asymbix.server.blp.messaging.SaveCOParser.SaveCORequest;
import com.ascatel.asymbix.server.blp.messaging.SaveCOParser.SaveCOResponse;
import com.ascatel.asymbix.server.blp.messaging.SaveCOParser.State;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ascatel.asymbix.server.blp.op.co.SaveCOServer.ResponseHelper.getDefectiveResponse;
import static com.ascatel.asymbix.server.blp.op.co.SaveCOServer.ResponseHelper.getResponseBuilder;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
@ru.omnicomm.pegasus.processingPlatform.annotations.ServerImplementation("SaveCOServer_Implementation")
public class SaveCOServer extends AbstractRequestServer implements ServerImplementation {

    private static final Logger LOGGER = LoggerFactory.getLogger();

    public static final int REQUEST_MESSAGE_TYPE = 431;

    public static final int RESPONSE_MESSAGE_TYPE = 432;

    /**
     * Конструктор.
     *
     * @param jmsBridge сервис брокера сообщений.
     * @param blService сервис бизнес платформы.
     */
    @ServerConstructor
    public SaveCOServer(JmsBridgeService jmsBridge, BusinessLogicService blService) {
        super(jmsBridge, blService);
    }

    @Override
    protected void processRequest(MessageLite request)
            throws InvalidProtocolBufferException {
        final SaveCORequest proto = SaveCORequest.parseFrom(request.toByteArray());
        final SaveCOOperation operation = new SaveCOOperation(getEnvironment());

        final String instanceId = proto.getInstanceId();
        final int messageBlock = proto.getMessageBlock();

        operation.setInstanceId(instanceId);
        operation.setBlockId(messageBlock);
        operation.setLocale(proto.getLocale());

        try {
            final String coId = parseCOId(proto.getCoId());
            final Map<String, ParameterValue> attrArray = extractParameters(proto.getAttributeValuesList());
            final List<SaveCORequest.SetValue> setValues = proto.getSetValuesList();

            processSaveCOOperation(operation, coId, attrArray, setValues);
        } catch (BusinessLogicServiceException e) {
            sendResponse(getDefectiveResponse(messageBlock, instanceId, e.getMessage()));
        }
    }

    private void processSaveCOOperation(SaveCOOperation operation, String coId, Map<String, ParameterValue> attrArray,
                                        List<SaveCORequest.SetValue> setValues) {
        final int blockId = operation.getBlockId();
        final String instanceId = operation.getInstanceId();

        try {
            startTransaction();
            final String savedCOId = operation.saveCO(coId, attrArray, setValues);
            commitTransaction();
            sendResponse(savedCOId, blockId, instanceId);
        } catch (StartTransactionException e) {
            // Упали на старьте транзакции.
            final String errMsg = "Cannot start transaction. " + e.getMessage();
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg));
        } catch (CommitTransactionException e) {
            // Не удалось закоммитить изменения.
            final String errMsg = "Unnable to commit the changes. " + e.getMessage();
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg));
        } catch (NumberFormatException e) {
            // Невалидный savedCOId
            final String errMsg = "Invalid coId. " + e.getMessage();
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg));
        } catch (BusinessLogicServiceException e) {
            // Какие либо ошибки при выполнении операции.
            // Надо откатитьтранзакцию и послать ошибку.
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(e.getMessage());
            try {
                abortTransaction();
            } catch (AbortTransactionException abortEx) {
                errMsg.append("; ").append(abortEx.getMessage());
            }
            sendResponse(getDefectiveResponse(blockId, instanceId, errMsg.toString()));
        }
    }

    private void sendResponse(String entityId, int blockId, String instanceId) {
        SaveCOResponse.Builder builder = getResponseBuilder(blockId, instanceId);
        builder.setCoId(Long.parseLong(entityId));
        sendResponse(builder.build());
    }

    private String parseCOId(long coId) {
        // id редактируемого контролируемого объекта; в случае создания нового объекта равно 0.
        if (coId == 0) {
            return null;
        } else {
            return Long.toString(coId);
        }
    }

    private Map<String, ParameterValue> extractParameters(List<SaveCORequest.AttributeValue> attributes)
            throws BusinessLogicServiceException {
        try {
            final Map<String, ParameterValue> params = new HashMap<>();
            for (SaveCORequest.AttributeValue attribute : attributes) {
                final long attrId = attribute.getId();
                final SaveCOParser.ParameterValue value = attribute.getValue();

                final ParameterValue newValue = ParameterValue.parseFrom(value.toByteString());
                params.put(Long.toString(attrId), newValue);
            }
            return params;
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Invalid ParameterValue format", e);
        }
    }

    @Override
    protected int getRequestMessageType() {
        return REQUEST_MESSAGE_TYPE;
    }

    public static class ResponseHelper {

        public static MessageLite getDefectiveResponse(int blockNumber, String instanceId, String errMsg) {
            SaveCOResponse.Builder builder = getResponseBuilder(blockNumber, instanceId);
            builder.setBlockState(State.DEFECTIVE).setErrorMessage(errMsg);

            return builder.build();
        }

        public static MessageLite getEmptyResponse(String instanceId) {
            SaveCOResponse.Builder builder = getResponseBuilder(0, instanceId);
            builder.setBlockState(State.LAST);
            return builder.build();
        }

        public static SaveCOResponse.Builder getResponseBuilder(int blockNumber, String instanceId) {
            SaveCOResponse.Builder builder = SaveCOResponse.newBuilder();

            builder.setMessageType(RESPONSE_MESSAGE_TYPE)
                    .setInstanceId(instanceId)
                    .setBlockNumber(blockNumber);

            return builder;
        }
    }


//    //FOR DEBUG ONLY!
//    @Override
//    protected void initServer(Server server, Handler handler) {
////        handler.setTimer("1", Handler.TimerType.DELAY_BASED, 1000L, 0L);
////        handler.setTimer("2", Handler.TimerType.DELAY_BASED, 5000L, 0L);
////        handler.setTimer("3", Handler.TimerType.DELAY_BASED, 1000L, 0L);
////        handler.setTimer("4", Handler.TimerType.DELAY_BASED, 2000L, 0L);
//
//        handler.setTimer("5", Handler.TimerType.DELAY_BASED, 1000L, 0L);
//        handler.setTimer("6", Handler.TimerType.DELAY_BASED, 5000L, 0L);
//    }
//
//    @Override
//    protected void processTimerEvent(TimerEvent signal) {
//
//        if ("1".equals(signal.id())) {
//            selectCOC();
//
//            List<SaveCORequest.AttributeValue> atributes = new ArrayList<>();
//
//            //1801
//            atributes.add(SaveCORequest.AttributeValue.newBuilder()
//                    .setId(1801)
//                    .setValue(SaveCOParser.ParameterValue.newBuilder().setStringValue("sdfj")).build());
//
//            //1802
//            SaveCOParser.ObjectLink link = SaveCOParser.ObjectLink.newBuilder()
//                    .addLinks("10051")
//                    .addLinks("10053")
//                    .build();
//            atributes.add(SaveCORequest.AttributeValue.newBuilder()
//                    .setId(1802)
//                    .setValue(SaveCOParser.ParameterValue.newBuilder().setObjectLinkValue(link)).build());
//
//            // request
//            final SaveCORequest request = SaveCORequest.newBuilder()
//                    .setMessageType(REQUEST_MESSAGE_TYPE)
//                    .setMessageBlock(1)
//                    .setInstanceId("12345")
//                    .setLocale("en")
//                    .setCoId(10401)
//
//                    .addAllAttributeValues(atributes).build();
//
//            try {
//                getEnvironment().getServer().send(null, request);
//            } catch (SendMessageException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            }
//        } else if ("2".equals(signal.id())) {
//            selectCOC();
//        } else if ("3".equals(signal.id())) {
//
//            SaveSettingHandler handler = new SaveSettingHandler(getEnvironment());
//            handler.setBlockId(1);
//            handler.setInstanceId("55666");
//            handler.setLocale("en");
//
//            try {
////                handler.saveSetting("10703", "14709", "newV22alll");
//                handler.saveSetting("10710", "14701", "11901");
//            } catch (BusinessLogicServiceException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//
//        } else if ("4".equals(signal.id())) {
//
//            List<BLSMessageParser.QueryItem> filter = new ArrayList<>();
//            BLSMessageParser.SelectRequest select = MessageUtil.buildSelectRequest(SaveSettingHandler.SIMPLE_SETTING_ENTITY_TYPE, filter);
//
//            try {
//                byte[] data = getEnvironment().getBlService().selectByParams(select.toByteArray());
//                BLSMessageParser.ResultSet resultSet = BLSMessageParser.ResultSet.parseFrom(data);
//                StringBuilder sb = new StringBuilder("\n");
//                for (BLSMessageParser.Parameter parameter : resultSet.getParametersList()) {
//
//                        sb.append(parameter.getEntityType()).append(" | ")
//                                .append(parameter.getEntityId()).append(" | ")
//                                .append(parameter.getAttributeId()).append(" | ")
//                                .append(parameter.getValue().getStringValue()).append("\n");
//
//                }
//                LOGGER.log(Level.WARN, sb.toString());
//            } catch (BusinessLogicServiceException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            } catch (InvalidProtocolBufferException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            }
//
//
//        } else if ("5".equals(signal.id())) {
//
//            final UpdateObjectHandler handler = new UpdateObjectHandler(getEnvironment());
//            handler.setBlockId(1);
//            handler.setInstanceId("55777");
//            handler.setLocale("en");
//
//            Map<String, BLSMessageParser.ParameterValue> map = new HashMap<>();
//            map.put("2133", BLSMessageParser.ParameterValue.newBuilder().setDoubleValue(2.0).build());
//
//            try {
//                handler.updateObject("2130", "11900", map);
//            } catch (BusinessLogicServiceException e) {
//                LOGGER.log(Level.ERROR, null, e);
//            }
//
//
//        } else if ("6".equals(signal.id())) {
//
//
//
//
//        }
//    }
//
//
//    private void selectCOC() {
//        List<BLSMessageParser.QueryItem> filter = new ArrayList<>();
//        BLSMessageParser.SelectRequest selectRequest = MessageUtil.buildSelectRequest(1, "en", "1870", filter);
//
//        try {
//            byte[] data = getEnvironment().getBlService().selectByParams(selectRequest.toByteArray());
//            BLSMessageParser.ResultSet resultSet = BLSMessageParser.ResultSet.parseFrom(data);
//            List<BLSMessageParser.Parameter> parameters = resultSet.getParametersList();
//            StringBuilder sb = new StringBuilder("\n");
//            for (BLSMessageParser.Parameter parameter : parameters) {
//                if ("1872".equals(parameter.getAttributeId())) {
//                    sb.append("COC: ")
//                        .append(parameter.getEntityType()).append("/")
//                            .append(parameter.getEntityId()).append("/")
//                            .append(parameter.getAttributeId()).append(" value:")
//                            .append(parameter.getValue().toString().replaceAll("\n", "  "))
//                            .append("\n");
//                }
//            }
//
//            LOGGER.log(Level.INFO, sb.toString());
//        } catch (BusinessLogicServiceException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        } catch (InvalidProtocolBufferException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//
//
//    }
}