/**
 * Class SetsArrayOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ValueArray;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.*;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Внутренний обработчик операции 'Сохранение КО' пункта 4: 'Cохраняем массив настроек обработки.
 * Идём по массиву SetsArray и для каждого его элемента...'
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class SetsArrayOperation extends AbstractOperation {

    public SetsArrayOperation(AbstractOperation operation) {
        super(operation);
    }

    public void updateObjectSettings(String coId, String cppsId, List<WSettingPair> pairs)
            throws BusinessLogicServiceException {
        final boolean isSensIndep = readIsSensIndep(cppsId);

        for (WSettingPair pair : pairs) {

            final String asId = pair.getAsId();
            final String value = pair.getValue();

            if (isSensIndep) {
                // Если IsSI = True, то:
                // a.   Считываем значение атрибута Number для сущности Asid
                // b.   Если полученное на предыдущем шаге значение <>1, поднимаем обшибу
                //      (для настройки не зависящей от датчиков, не может быть несколько отличающихся значений для
                //      разных датчиков, она всегда соответствует первому датчику).
                final double number = readASNumber(asId);
                if (1.0 != number) {
                    throw new BusinessLogicServiceException(
                            "It is not allowed to sensor independent setting have several different values.");
                }
            }

            saveSetting(cppsId, asId, value);
        }
    }

    public void updateGlobalObjectSettings(String coId, String cppsId, String value)
            throws BusinessLogicServiceException {
        // Находим все, связанные с КО регистраторы.
        final Collection<String> registrators =  selectBoundRegistrators(coId);
        if (registrators.isEmpty()) {
            return;
        }

        // Находим все сущности класса InstalledSensor, установленные на любом из указанных регистраторов,
        final Collection<String> isIds = selectBoundIS(registrators);
        if (isIds.isEmpty()) {
            return;
        }

        // Находим все сущности класса AtomicSensor, ассоциированные с любым из физических датчиков.
        final Collection<String> asIds = selectBoundAS(isIds);

        for (String asId : asIds) {
            saveSetting(cppsId, asId, value);
        }
    }

    private void saveSetting(String cppsId, String asId, String value)
            throws BusinessLogicServiceException {
        final SaveSettingOperation saveSettingHandler = new SaveSettingOperation(this);
        saveSettingHandler.saveSetting(cppsId, asId, value);
    }

    private Collection<String> selectBoundAS(Collection<String> isIds)
            throws BusinessLogicServiceException {
        final List<ParameterValue> isValues = new ArrayList<>();
        for (String isId : isIds) {
            isValues.add(MessageUtil.buildParameterValue(isId));
        }
        final ValueArray valueArray = MessageUtil.buildValueArray(isValues);

        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(AS_BIND_IS_PARAMETER_ID, Operation.IN, buildParameterValue(valueArray)));
        filter.add(buildQueryItem(AS_NUMBER_PARAMETER_ID, Operation.EQ, buildParameterValue(1.0)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), AS_ENTITY_TYPE, filter);
        final byte[] data = getEnv().getBlService().selectByParams(request.toByteArray());
        try {
            final ResultSet resultSet = ResultSet.parseFrom(data);

            final Set<String> result = new HashSet<>();
            for (Parameter parameter : resultSet.getParametersList()) {
                result.add(parameter.getEntityId());
            }
            return result;
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    private Collection<String> selectBoundIS(Collection<String> registrators)
            throws BusinessLogicServiceException {

        final List<ParameterValue> rValues = new ArrayList<>();
        for (String registrator : registrators) {
            rValues.add(MessageUtil.buildParameterValue(registrator));
        }
        final ValueArray valueArray = MessageUtil.buildValueArray(rValues);

        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(IS_BIND_REG_PARAMETER_ID, Operation.IN, buildParameterValue(valueArray)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), IS_ENTITY_TYPE, filter);
        final byte[] data = getEnv().getBlService().selectByParams(request.toByteArray());
        try {
            final ResultSet resultSet = ResultSet.parseFrom(data);

            final Set<String> result = new HashSet<>();
            for (Parameter parameter : resultSet.getParametersList()) {
                result.add(parameter.getEntityId());
            }
            return result;
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    private Collection<String> selectBoundRegistrators(String coId)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(R_BIND_CO_PARAMETER_ID, Operation.EQ, buildParameterValue(coId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), R_ENTITY_TYPE, filter);
        final byte[] data = getEnv().getBlService().selectByParams(request.toByteArray());
        try {

            final ResultSet resultSet = ResultSet.parseFrom(data);

            final Set<String> result = new HashSet<>();
            for (Parameter parameter : resultSet.getParametersList()) {
                result.add(parameter.getEntityId());
            }
            return result;
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    private double readASNumber(String asId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(AS_ENTITY_TYPE, asId, AS_NUMBER_PARAMETER_ID);
        return value.getDoubleValue();
    }

    private boolean readIsSensIndep(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_IS_SENSINDEP_PARAMETER_ID);
        return value.getBooleanValue();
    }

    private ParameterValue readParameterValue(String entityType, String entityId, String parameterId)
            throws BusinessLogicServiceException {
        final BLSMessageParser.ReadRequest request = buildReadRequest(getBlockId(), entityType, entityId, parameterId);
        byte[] responseData = getEnv().getBlService().getParameterValue(request.toByteArray());
        try {
            return ParameterValue.parseFrom(responseData);
        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
    }

    /**
     * Пара значений настройки для различных индексов (номеров).
     */
    public static class WSettingPair {
        private String asId;

        private String value;

        public WSettingPair(String asId, String value) {
            this.asId = asId;
            this.value = value;
        }

        public String getAsId() {
            return asId;
        }

        public String getValue() {
            return value;
        }
    }

}
