/**
 * Class PrimarySettingOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser;
import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.google.protobuf.InvalidProtocolBufferException;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.CreateRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;

import java.util.*;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildUpdateRequest;

/**
 * Внутренний обработчик операции 'Сохранение КО' пункта 5: 'Выполняем первичные настройки для сервера определения
 * отсутствия данных...'.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class PrimarySettingOperation extends AbstractOperation {

    public static final String IDLETIMER_LAST_KEY = "IDLETIMER_LAST";

    public static final String IDLETIMER_KEY = "IDLETIMER-";

    public static final String IDLETIMER_REF_KEY = "IDLETIMER_REF-";

    public static final String IDLESTATE_KEY = "IDLESTATE-";

    public PrimarySettingOperation(AbstractOperation handler) {
        super(handler);
    }

    public void doSetting(String coId)
            throws BusinessLogicServiceException {
        // Ищем объект класса SimpleSettingпо ключу IDLETIMER_LAST. Считываем значение данной настройки.
        final String lastTimerValue = selectTimerLastValue();

        int timerValue = 0;
        if (lastTimerValue != null) {
            timerValue = Integer.parseInt(lastTimerValue) + 1;
        }

        // Создаём настройку (объект класса SimpleSettings) со следующими параметрами:
        // i.   Key =IDLETIMER-<T>
        // ii.  Value = COid
        String settingEntityId = createSimpleSetting(IDLETIMER_KEY + Integer.toString(timerValue), coId);

        // Создаём настройку (объект класса SimpleSettings) со следующими параметрами:
        // i.   Key =IDLETIMER_REF-<COid>
        // ii.  Value = Idнастройки созданной на предыдущем шаге
        createSimpleSetting(IDLETIMER_REF_KEY + coId, settingEntityId);

        // Создаём настройку (объект класса SimpleSettings) со следующими параметрами:
        // i.   Key =IDLESTATE-<COid>
        // ii.  Value = 1 (данные поступают)
        createSimpleSetting(IDLESTATE_KEY + coId, "1");
    }

    private String selectTimerLastValue()
            throws BusinessLogicServiceException {
        List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(SIMPLE_SETTING_KEY_PARAMETER_ID, Operation.EQ, buildParameterValue(IDLETIMER_LAST_KEY)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), SIMPLE_SETTING_ENTITY_TYPE, filter);
        final byte[] data = getEnv().getBlService().selectByParams(request.toByteArray());

        try {

            final ResultSet resultSet = ResultSet.parseFrom(data);

            for (Parameter parameter : resultSet.getParametersList()) {
                if (SIMPLE_SETTING_VALUE_PARAMETER_ID.equals(parameter.getAttributeId())) {
                    return parameter.getValue().getStringValue();
                }
            }

        } catch (InvalidProtocolBufferException e) {
            throw new BusinessLogicServiceException("Cannot parse response from BasePlatform", e);
        }
        return null;
    }

    private String createSimpleSetting(String key, String value)
            throws BusinessLogicServiceException {
        final CreateRequest createRequest = buildCreateRequest(SIMPLE_SETTING_ENTITY_TYPE);
        final String settingEntityId = getEnv().getBlService().createEntity(createRequest.toByteArray());

        final BLSMessageParser.ParameterValue keyValue = buildParameterValue(key);
        final BLSMessageParser.UpdateRequest updKeyRequest = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_KEY_PARAMETER_ID, keyValue);

        final BLSMessageParser.ParameterValue valueValue = buildParameterValue(value);
        final BLSMessageParser.UpdateRequest updValueRequest = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                settingEntityId, SIMPLE_SETTING_VALUE_PARAMETER_ID, valueValue);

        getEnv().getBlService().setParameterValue(updKeyRequest.toByteArray());
        getEnv().getBlService().setParameterValue(updValueRequest.toByteArray());
        return settingEntityId;
    }

}
