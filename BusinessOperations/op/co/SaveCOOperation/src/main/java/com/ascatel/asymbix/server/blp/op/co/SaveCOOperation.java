/**
 * Class SaveCOOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 17.04.13
 */
package com.ascatel.asymbix.server.blp.op.co;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.op.general.UpdateObjectOperation;
import com.ascatel.asymbix.server.blp.Environment;
import com.ascatel.asymbix.server.blp.messaging.SaveCOParser.SaveCORequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;

/**
 * Операция 'Сохранение КО'.
 * Операция сохраняет параметры редактируемого контролируемого объекта, а также настройки обработки.
 * Для сохранения настроек объект CO и датчики должны быть уже созданы и существовать в системе (т.е. при создании КО
 * в операцию передаются только атрибуты).
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code coId} - ID редактируемого контролируемого объекта; в случае создания нового объекта равно 0.</li>
 * <li>{@code attrArray} – массив атрибутов и их значений.</li>
 * <li>{@code setsArray} - массив настроек.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class SaveCOOperation extends AbstractOperation {

    public SaveCOOperation(AbstractOperation operation) {
        super(operation);
    }

    public SaveCOOperation(Environment env) {
        super(env);
    }

    public String saveCO(String coId, Map<String, ParameterValue> attrArray, List<SaveCORequest.SetValue> setValues)
            throws BusinessLogicServiceException {
        // Вызываем операцию Добавить/Обновить экземпляр класса из сервера общих операций.
        // Передаём входные параметры:
        // a.  ReferenceId = ID класса Controlled Object ( = 1000)
        // b.  RefDataId = COid
        // c.  Массив атрибутов и их значений = AttrArray
        final UpdateObjectOperation operation = new UpdateObjectOperation(this);

        final String entityId = operation.updateObject(CO_ENTITY_TYPE, coId, attrArray);

        // Cохраняем массив настроек обработки. Идём по массиву SetsArray и для каждого его элемента выполняем
        // обработку.
        updateSettingArray(entityId, setValues);

        // Выполняем первичные настройки для сервера определения отсутствия данных.
        doPrimarySetting(entityId);

        return entityId;
    }

    private void doPrimarySetting(String coId)
            throws BusinessLogicServiceException {
        final PrimarySettingOperation primarySettingHandler = new PrimarySettingOperation(this);
        primarySettingHandler.doSetting(coId);
    }

    private void updateSettingArray(String entityId, List<SaveCORequest.SetValue> setValuesList)
            throws BusinessLogicServiceException {

        for (SaveCORequest.SetValue setValue : setValuesList) {
            final String cppsId = Long.toString(setValue.getCppsId());
            final boolean isGlobal = readIsGlobal(cppsId);

            final int wPairValuesCount = setValue.getWpairValuesCount();

            final SetsArrayOperation setsArrayHandler = new SetsArrayOperation(this);

            if (isGlobal) {
                // Если isG = true, то (здесь нужно создать настройки для каждого из всех различных SourceId).

                if (wPairValuesCount != 1) {
                    // Проверяем, что массив W имеет только один элемент, иначе поднимаем ошибку
                    // (нельзя для глобальной настройки иметь несколько разных значений).
                    throw new BusinessLogicServiceException(
                            "It is not allowed to global setting have several different values.");
                }

                final SaveCORequest.SetValue.WPairValues wPairValues = setValue.getWpairValues(0);
                final String value = wPairValues.getValue();

                setsArrayHandler.updateGlobalObjectSettings(entityId, cppsId, value);
            } else {
                final List<SetsArrayOperation.WSettingPair> pairs = new ArrayList<>(wPairValuesCount);
                for (SaveCORequest.SetValue.WPairValues wPairValues : setValue.getWpairValuesList()) {
                    final String asId = Long.toString(wPairValues.getAsId());
                    final String value = wPairValues.getValue();
                    pairs.add(new SetsArrayOperation.WSettingPair(asId, value));
                }

                setsArrayHandler.updateObjectSettings(entityId, cppsId, pairs);
            }
        }
    }

    private boolean readIsGlobal(String cppsId)
            throws BusinessLogicServiceException {
        final ParameterValue value = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_IS_GLOBAL_PARAMETER_ID);
        return value.getBooleanValue();
    }
}
