/**
 * Class DeregisterISOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 18.04.13
 */
package com.ascatel.asymbix.server.blp.op.is;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;

import java.util.*;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Операция 'Дерегистрировать датчик'/ИУ.
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code referenceId} - идентификатор класса удаляемой сущности.</li>
 * <li>{@code refDataId} – идентификатор удаляемой сущности {@code (iSid)}</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class DeregisterISOperation extends AbstractOperation {

    /**
     * Значение типа стандартного датчика.
     */
    private static final int STANDARD_SENSOR = 1;

    /**
     * Значение типа исполнительного устройства.
     */
    private static final int UNIT_EXECUTION = 2;

    public DeregisterISOperation(AbstractOperation handler) {
        super(handler);
    }

    public void deregisterIS(String referenceId, String refDataId)
            throws BusinessLogicServiceException {

        // Проверяем, что ReferenceId является идентификатором класса Installed Sensor (иначе поднимаем ошибку).
        if (IS_ENTITY_TYPE.equals(referenceId)) {
            final String errMsg = "Passed entity type identifier is not InstalledSensor="
                    + IS_ENTITY_TYPE;
            throw new BusinessLogicServiceException(errMsg);
        }

        // Определяем тип дерегистрируемого датчика.
        final int sensorMode = readSensorMode(refDataId);
        if (STANDARD_SENSOR == sensorMode) {
            // Снимается с регистрации датчик, а не исполнительное устройство.
            deregisterStandardSensor(refDataId);
        } else if (UNIT_EXECUTION == sensorMode) {
            // Снимается с регистрации исполнительное устройство, а не датчик.
            deleteRegistratorCommand(refDataId);
        } else {
            final String errMsg = new StringBuilder().append("Unknown value for sensor mode ")
                    .append(sensorMode).toString();
            throw new BusinessLogicServiceException(errMsg);
        }

        // Удаляем объект IS, вызывая операцию Удалить сущность.
        deleteIS(refDataId);
    }

    private void deleteRegistratorCommand(String isId)
            throws BusinessLogicServiceException {
        // Находим все назначенные команды на данное исполнительное устройство. Выбираем все объекты класса
        // AssignedRegistratorCommand по условию BindIS = IS
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(ARC_BIND_IS_PARAMETER_ID, Operation.EQ, buildParameterValue(isId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), ARC_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Set<String> arcIds = new HashSet<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            arcIds.add(parameter.getEntityId());
        }

        // Для каждого из найденных на предыдущем шаге объектов вызываем операцию Удалить сущность
        for (String arcId : arcIds) {
            final DeleteRequest deleteRequest = buildDeleteRequest(getBlockId(), ARC_ENTITY_TYPE, arcId);
            getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
        }
    }

    private void deleteIS(String isId)
            throws BusinessLogicServiceException {
        final DeleteRequest deleteRequest = buildDeleteRequest(getBlockId(), IS_ENTITY_TYPE, isId);
        getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
    }

    private void deregisterStandardSensor(String isId)
            throws BusinessLogicServiceException {

        // Получим все разные значения идентификаторов ControlledParameter логических датчиков связанных с
        // дерегистрируемым датчиком.
        final Collection<String> ascpIds = selectASCP(isId);

        for (String ascpId : ascpIds) {
            final CancelSensorMeasurementOperation operation = new CancelSensorMeasurementOperation(this);
            operation.cancelSensorMeasurement(isId, ascpId);
        }
    }

    private Collection<String> selectASCP(String isId)
            throws BusinessLogicServiceException {
        // Находим все логические датчики (сущности типа Atomic Sensor) связанные с дерегистрируемым датчиком.
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(AS_BIND_IS_PARAMETER_ID, Operation.EQ, buildParameterValue(isId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), AS_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Set<String> ascpIds = new HashSet<>();
        // Выберем все разные значения идентификаторов ControlledParameter.
        for (Parameter parameter : resultSet.getParametersList()) {
            if (AS_BIND_CP_PARAMETER_ID.equals(parameter.getAttributeId())) {
                final ParameterValue cpLinkValue = parameter.getValue();
                if (cpLinkValue.hasObjectLinkValue()) {
                    for (String cpId : cpLinkValue.getObjectLinkValue().getLinksList()) {
                        ascpIds.add(cpId);
                    }
                }
            }
        }

        return ascpIds;
    }

    private int readSensorMode(String refDataId)
            throws BusinessLogicServiceException {
        final ParameterValue bindSTValue = readParameterValue(IS_ENTITY_TYPE, refDataId, IS_BIND_ST_PARAMETER_ID);
        if (!bindSTValue.hasObjectLinkValue()) {
            final String errMsg = new StringBuilder()
                    .append("Parameter IS_BIND_ST_PARAMETER_ID has no value for entityId=")
                    .append(refDataId).toString();
            throw new BusinessLogicServiceException(errMsg);
        }

        final List<String> linksList = bindSTValue.getObjectLinkValue().getLinksList();
        if (linksList.isEmpty()) {
            final String errMsg = new StringBuilder()
                    .append("Parameter IS_BIND_ST_PARAMETER_ID has no value for entityId=")
                    .append(refDataId).toString();
            throw new BusinessLogicServiceException(errMsg);
        }

        final String stEntitId = linksList.get(0);
        final ParameterValue stModeValue = readParameterValue(ST_ENTITY_TYPE, stEntitId, ST_SENSOR_MODE_PARAMETER_ID);
        return stModeValue.getIntValue();
    }

}
