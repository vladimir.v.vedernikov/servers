 /*
 * com.ascatel.asymbix.server.blp.op.is.UpdateISOperation
 * 
 * Copyright© 2013 Ascatel Inc.. All rights reserved.
 * For internal use only.
 * 
 * Author: Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 * Version: 
 */
package com.ascatel.asymbix.server.blp.op.is;

import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.CreateRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.MUIString;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ObjectLink;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.Parameter;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValue;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ParameterValueType;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.QueryItem.Operation;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ReadRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.ResultSet;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.SelectRequest;
import ru.omnicomm.pegasus.messaging.base.service.BLSMessageParser.UpdateRequest;
import ru.omnicomm.pegasus.processingPlatform.logging.Logger;
import ru.omnicomm.pegasus.processingPlatform.logging.LoggerFactory;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicService;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

/**
 *
 * @author Александр Софьенков <a href="mailto:sofyenkov@omnicomm.ru">&lt;sofyenkov@omnicomm.ru&gt;</a>
 */
public class UpdateISOperation {

    private static final String ATOMIC_SENSOR_ENTITY_TYPE = "1750";
    private static final long AS_IS_ID = 1752;
    private static final long AS_DTID_ID = 1755;
    private static final String CO_ENTITY_TYPE = "1800";
    private static final String REGISTRATOR_ENTITY_TYPE = "3000";
    private static final long REGISTRATOR_CO_ID = 3004;
    private static final String REGISTRATOR_DATA_ID_ENTITY_TYPE = "3020";
    private static final long RDID_DATA_ID = 3021;
    private static final long RDID_NAME_ID = 3022;
    private static final long RDID_PORT_ID = 3023;
    private static final long RDID_REGISTRATOR_ID = 3024;
    private static final String REGISTRATOR_PORT_ENTITY_TYPE = "3030";
    private static final long REGISTRATOR_PORT_VARIANT_DATA_ID = 3032;
    private static final String IS_ENTITY_TYPE = "3050";
    private static final long IS_SENSOR_NUMBER_ID = 3051;
    private static final long IS_REGISTRATOR_PORT_ID = 3052;
    private static final long IS_REGISTRATOR_ID = 3053;
    private static final long IS_SENSOR_TYPE_ID = 3054;
    private static final String SENSOR_TYPE_ENTITY_TYPE = "3060";
    private static final long SENSOR_TYPE_SENSOR_MODE_ID = 3061;
    private static final Logger logger = LoggerFactory.getLogger();
    private BusinessLogicService blService;

    public UpdateISOperation(BusinessLogicService blService) {
        this.blService = blService;
    }

    public String updateIS(String referenceId, long refDataId, Map<Long, ParameterValue> attributes) throws BusinessLogicServiceException {
        if (!referenceId.equals(IS_ENTITY_TYPE)) {
            throw new BusinessLogicServiceException("Wrong entity type: referenceId=" + referenceId);
        }
        String sensorNumber = getParameterValue(ParameterValueType.STRING, attributes.get(IS_SENSOR_NUMBER_ID), null);
        Long sensorTypeId = getParameterValue(ParameterValueType.LONG, attributes.get(IS_SENSOR_TYPE_ID), null);
        Long portId = getParameterValue(ParameterValueType.LONG, attributes.get(IS_REGISTRATOR_PORT_ID), null);
        Long registratorId = getParameterValue(ParameterValueType.LONG, attributes.get(IS_REGISTRATOR_ID), null);
        Long coId = null;
        ReadRequest readRequest = MessageUtil.buildReadRequest(REGISTRATOR_ENTITY_TYPE, registratorId, REGISTRATOR_CO_ID);
        byte[] responseBytes = blService.getParameterValue(readRequest.toByteArray());
        ParameterValue responseParameterValue = ParameterValue.parseFrom(responseBytes);
        ObjectLink objectLink = getParameterValue(ParameterValueType.OBJECT_LINK, responseParameterValue, null);
        if (objectLink != null) {
            List<String> coIds = objectLink.getLinksList();
            if (!coIds.isEmpty()) {
                try {
                    coId = Long.parseLong(coIds.get(0));
                }
                catch (NumberFormatException numberFormatException) {
                    logger.warn("Cannot parse coId: " + coIds.get(0), numberFormatException);
                }
            }
        }
        if (coId == null) {
            throw new BusinessLogicServiceException("Registrator entity has no coId: registratorId=" + registratorId);
        }
        if (refDataId == 0) {
            if (!entityExists(CO_ENTITY_TYPE, coId)) {
                throw new BusinessLogicServiceException("Entity CONTROLLED OBJECT doesn't exist: id=" + coId);
            }
            if (!entityExists(REGISTRATOR_ENTITY_TYPE, registratorId)) {
                throw new BusinessLogicServiceException("Entity REGISTRATOR doesn't exist: id=" + registratorId);
            }
            if (!entityExists(REGISTRATOR_PORT_ENTITY_TYPE, portId)) {
                throw new BusinessLogicServiceException("Entity REGISTRATOR PORT doesn't exist: id=" + portId);
            }
            if (!entityExists(SENSOR_TYPE_ENTITY_TYPE, sensorTypeId)) {
                throw new BusinessLogicServiceException("Entity SENSOR TYPE doesn't exist: id=" + sensorTypeId);
            }
            readRequest = MessageUtil.buildReadRequest(SENSOR_TYPE_ENTITY_TYPE, sensorTypeId, SENSOR_TYPE_SENSOR_MODE_ID);
            responseBytes = blService.getParameterValue(readRequest.toByteArray());
            responseParameterValue = ParameterValue.parseFrom(responseBytes);
            Integer sensorMode = getParameterValue(ParameterValueType.INTEGER, responseParameterValue, null);
            if (sensorMode == null || sensorMode != 2) {
                readRequest = MessageUtil.buildReadRequest(REGISTRATOR_PORT_ENTITY_TYPE, portId, REGISTRATOR_PORT_VARIANT_DATA_ID);
                responseBytes = blService.getParameterValue(readRequest.toByteArray());
                responseParameterValue = ParameterValue.parseFrom(responseBytes);
                Boolean variantDataId = getParameterValue(ParameterValueType.BOOLEAN, responseParameterValue, null);
                if (variantDataId != null && variantDataId) {
                    if (sensorNumber == null || sensorNumber.trim().isEmpty()) {
                        throw new BusinessLogicServiceException("SensorNumber is not defined: sensorNumber=" + sensorNumber);
                    }
                    Map<Long, List<Parameter>> is = getInstalledSensor(sensorTypeId, sensorNumber);
                    if (is != null) {
                        throw new BusinessLogicServiceException("Duplicate sensor number: sensorTypeId=" + sensorTypeId
                                                                + "; sensorNumber=" + sensorNumber);
                    }
                    String registratorDataId = createRegistratorDataId(sensorNumber, registratorId, portId);
                }
            }
            String installedSensorId = createInstalledSensor(sensorTypeId, sensorNumber, registratorId, portId);
        }
        else {
            readRequest = MessageUtil.buildReadRequest(IS_ENTITY_TYPE, refDataId, IS_SENSOR_TYPE_ID);
            responseBytes = blService.getParameterValue(readRequest.toByteArray());
            responseParameterValue = ParameterValue.parseFrom(responseBytes);
            String linkValue = getFirstObjectLinkValue(responseParameterValue);
            if (!portId.toString().equals(linkValue)) {
                throw new BusinessLogicServiceException("Do not modify registrator port: " + portId + " != " + linkValue);
            }
            readRequest = MessageUtil.buildReadRequest(IS_ENTITY_TYPE, refDataId, IS_REGISTRATOR_ID);
            responseBytes = blService.getParameterValue(readRequest.toByteArray());
            responseParameterValue = ParameterValue.parseFrom(responseBytes);
            linkValue = getFirstObjectLinkValue(responseParameterValue);
            if (!registratorId.toString().equals(linkValue)) {
                throw new BusinessLogicServiceException("Do not modify registrator : " + registratorId + " != " + linkValue);
            }
            readRequest = MessageUtil.buildReadRequest(IS_ENTITY_TYPE, refDataId, IS_SENSOR_TYPE_ID);
            responseBytes = blService.getParameterValue(readRequest.toByteArray());
            responseParameterValue = ParameterValue.parseFrom(responseBytes);
            linkValue = getFirstObjectLinkValue(responseParameterValue);
            if (!registratorId.toString().equals(linkValue)) {
                throw new BusinessLogicServiceException("Do not modify sensor type : " + sensorTypeId + " != " + linkValue);
            }
            readRequest = MessageUtil.buildReadRequest(IS_ENTITY_TYPE, refDataId, IS_SENSOR_NUMBER_ID);
            responseBytes = blService.getParameterValue(readRequest.toByteArray());
            responseParameterValue = ParameterValue.parseFrom(responseBytes);
            String sensNumOld = getParameterValue(ParameterValueType.STRING, responseParameterValue, null);
            if (sensorNumber != null && !sensorNumber.equals(sensNumOld)) {
                Map<Long, List<Parameter>> is = getInstalledSensor(sensorTypeId, sensorNumber);
                if (is != null) {
                    throw new BusinessLogicServiceException("Duplicate sensor number: sensorTypeId=" + sensorTypeId
                                                            + "; sensorNumber=" + sensorNumber);
                }
                ParameterValue value = MessageUtil.buildParameterValue(ParameterValueType.STRING, sensorNumber);
                UpdateRequest updateRequest = MessageUtil.buildUpdateRequest(IS_ENTITY_TYPE, String.valueOf(refDataId), IS_SENSOR_NUMBER_ID, value);
                blService.setParameterValue(updateRequest.toByteArray());
                readRequest = MessageUtil.buildReadRequest(REGISTRATOR_PORT_ENTITY_TYPE, portId, REGISTRATOR_PORT_VARIANT_DATA_ID);
                responseBytes = blService.getParameterValue(readRequest.toByteArray());
                responseParameterValue = ParameterValue.parseFrom(responseBytes);
                Boolean variantDataId = getParameterValue(ParameterValueType.BOOLEAN, responseParameterValue, null);
                if (variantDataId != null && variantDataId) {
                    String dtId = getRegistratorDataIdEntityId(registratorId, portId);
                    if (dtId != null) {
                        value = MessageUtil.buildParameterValue(ParameterValueType.STRING, sensorNumber);
                        updateRequest = MessageUtil.buildUpdateRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE, dtId, RDID_DATA_ID, value);
                        blService.setParameterValue(updateRequest.toByteArray());
                    }
                    else {
                        dtId = createRegistratorDataId(sensorNumber, registratorId, portId);
                    }
                    Map<Long, Map<Long, List<Parameter>>> atomicSensors = getAtomicSensors(refDataId, dtId);
                }
            }
        }
    }

    private String getFirstObjectLinkValue(ParameterValue parameterValue) {
        String result = null;
        ObjectLink objectLink = getParameterValue(ParameterValueType.OBJECT_LINK, parameterValue, null);
        if (objectLink != null) {
            List<String> links = objectLink.getLinksList();
            if (!links.isEmpty()) {
                result = links.get(0);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private <T> T getParameterValue(ParameterValueType type, ParameterValue parameterValue, String locale) {
        if (parameterValue == null) {
            return null;
        }
        return (T) extractValue(type, parameterValue, locale);
    }

    private Object extractValue(ParameterValueType type, ParameterValue value, String locale) {
        switch (type) {
            case STRING:
                return value.getStringValue();
            case NUMBER:
                return value.getDoubleValue();
            case DATETIME:
                return value.getDatetimeValue();
            case MUISTRING:
                String muiStr = null;
                for (MUIString.MUIValue muiValue : value.getMuistringValue().getStrValuesList()) {
                    if (muiValue.getLangID() != null && muiValue.getLangID().equals(locale)) {
                        muiStr = muiValue.getValue();
                        break;
                    }
                }
                return muiStr;
            case OBJECT_LINK:
                return value.getObjectLinkValue();
            case FILE:
                return value.getFileValue();
            case INTEGER:
                return value.getIntValue();
            case LONG:
                return value.getLongValue();
            case BOOLEAN:
                return value.getBooleanValue();
            case ARRAY:
                return value.getValueArray().getValuesList();
            default:
                throw new IllegalArgumentException("Unsupported parameter value type=" + type);
        }
    }

    private boolean entityExists(String entityType, Long entityId) throws BusinessLogicServiceException {
        boolean exists = false;
        ParameterValue entityValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, entityId.toString());
        QueryItem entityItem = MessageUtil.buildQueryItem(BusinessLogicService.ENTITY_ID_ATTRIBUTE, Operation.EQ, entityValue);
        SelectRequest selectRequest = MessageUtil.buildSelectRequest(entityType, Arrays.asList(entityItem));
        try {
            byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
            ResultSet resultSet = ResultSet.parseFrom(responseBytes);
            if (resultSet.getParametersCount() > 0) {
                Parameter parameter = resultSet.getParameters(0);
                exists = parameter.getEntityId().equals(entityId.toString());
            }
        }
        catch (BusinessLogicServiceException | InvalidProtocolBufferException ex) {
            throw new BusinessLogicServiceException("Cannot get entity by Id: entityId=" + entityId + "; entityType=" + entityType, ex);
        }
        return exists;
    }

    private String getRegistratorDataIdEntityId(Long registratorId, Long portId) throws BusinessLogicServiceException, InvalidProtocolBufferException {
        ParameterValue registratorIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, registratorId.toString());
        QueryItem registratorIdItem = MessageUtil.buildQueryItem(String.valueOf(RDID_REGISTRATOR_ID), Operation.EQ, registratorIdValue);
        ParameterValue portIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, portId.toString());
        QueryItem portIdItem = MessageUtil.buildQueryItem(String.valueOf(RDID_PORT_ID), Operation.EQ, portIdValue);
        SelectRequest selectRequest = MessageUtil.buildSelectRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE, Arrays.asList(registratorIdItem, portIdItem));
        byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
        ResultSet resultSet = ResultSet.parseFrom(responseBytes);
        Map<Long, Map<Long, List<Parameter>>> rdis = CommonUtil.groupParameters(resultSet.getParametersList());
        if (!rdis.isEmpty()) {
            return rdis.keySet().iterator().next().toString();
        }
        return null;
    }

    private Map<Long, List<Parameter>> getInstalledSensor(Long sensorTypeId, String sensorNumber) throws BusinessLogicServiceException,
                                                                                                         InvalidProtocolBufferException {
        ParameterValue sensorTypeValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, sensorTypeId.toString());
        QueryItem sensorTypeItem = MessageUtil.buildQueryItem(String.valueOf(IS_SENSOR_TYPE_ID), Operation.EQ, sensorTypeValue);
        ParameterValue sensorNumberValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, sensorNumber);
        QueryItem sensorNumberItem = MessageUtil.buildQueryItem(String.valueOf(IS_SENSOR_NUMBER_ID), Operation.EQ, sensorNumberValue);
        SelectRequest selectRequest = MessageUtil.buildSelectRequest(IS_ENTITY_TYPE, Arrays.asList(sensorTypeItem, sensorNumberItem));
        byte[] responseBytes = blService.selectByParams(selectRequest.toByteArray());
        ResultSet resultSet = ResultSet.parseFrom(responseBytes);
        Map<Long, Map<Long, List<Parameter>>> iss = CommonUtil.groupParameters(resultSet.getParametersList());
        if (!iss.isEmpty()) {
            return iss.values().iterator().next();
        }
        return null;
    }

    private String createRegistratorDataId(String sensorNumber, Long registratorId, Long portId) throws BusinessLogicServiceException {
        CreateRequest createRequest = MessageUtil.buildCreateRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE);
        String rdId = blService.createEntity(createRequest.toByteArray());
        ParameterValue value = MessageUtil.buildParameterValue(ParameterValueType.STRING, sensorNumber);
        UpdateRequest updateRequest = MessageUtil.buildUpdateRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE, rdId, RDID_DATA_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        MUIString.Builder muiStringBuilder = MUIString.newBuilder();
        MUIString.MUIValue.Builder muiValueBuilder = MUIString.MUIValue.newBuilder();
        muiValueBuilder.setLangID("en").setValue("Sensor # " + sensorNumber);
        muiStringBuilder.addStrValues(muiValueBuilder);
        value = MessageUtil.buildParameterValue(ParameterValueType.MUISTRING, muiStringBuilder.build());
        updateRequest = MessageUtil.buildUpdateRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE, rdId, RDID_NAME_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        ObjectLink.Builder linkBuilder = ObjectLink.newBuilder();
        linkBuilder.addLinks(portId.toString());
        value = MessageUtil.buildParameterValue(ParameterValueType.OBJECT_LINK, linkBuilder.build());
        updateRequest = MessageUtil.buildUpdateRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE, rdId, RDID_PORT_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        linkBuilder = ObjectLink.newBuilder();
        linkBuilder.addLinks(registratorId.toString());
        value = MessageUtil.buildParameterValue(ParameterValueType.OBJECT_LINK, linkBuilder.build());
        updateRequest = MessageUtil.buildUpdateRequest(REGISTRATOR_DATA_ID_ENTITY_TYPE, rdId, RDID_REGISTRATOR_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        return rdId;
    }

    private String createInstalledSensor(Long sensorTypeId, String sensorNumber, Long registratorId, Long portId) throws BusinessLogicServiceException {
        CreateRequest createRequest = MessageUtil.buildCreateRequest(IS_ENTITY_TYPE);
        String isId = blService.createEntity(createRequest.toByteArray());
        ParameterValue value = MessageUtil.buildParameterValue(ParameterValueType.STRING, sensorNumber);
        UpdateRequest updateRequest = MessageUtil.buildUpdateRequest(IS_ENTITY_TYPE, isId, IS_SENSOR_NUMBER_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        ObjectLink.Builder linkBuilder = ObjectLink.newBuilder();
        linkBuilder.addLinks(portId.toString());
        value = MessageUtil.buildParameterValue(ParameterValueType.OBJECT_LINK, linkBuilder.build());
        updateRequest = MessageUtil.buildUpdateRequest(IS_ENTITY_TYPE, isId, IS_REGISTRATOR_PORT_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        linkBuilder = ObjectLink.newBuilder();
        linkBuilder.addLinks(registratorId.toString());
        value = MessageUtil.buildParameterValue(ParameterValueType.OBJECT_LINK, linkBuilder.build());
        updateRequest = MessageUtil.buildUpdateRequest(IS_ENTITY_TYPE, isId, IS_REGISTRATOR_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        linkBuilder = ObjectLink.newBuilder();
        linkBuilder.addLinks(sensorTypeId.toString());
        value = MessageUtil.buildParameterValue(ParameterValueType.OBJECT_LINK, linkBuilder.build());
        updateRequest = MessageUtil.buildUpdateRequest(IS_ENTITY_TYPE, isId, IS_SENSOR_TYPE_ID, value);
        blService.setParameterValue(updateRequest.toByteArray());
        return isId;
    }

    private Map<Long, Map<Long, List<Parameter>>> getAtomicSensors(long refDataId, String dtId) {
        ParameterValue refDataIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, String.valueOf(refDataId));
        QueryItem refDataIdItem = MessageUtil.buildQueryItem(String.valueOf(AS_IS_ID), Operation.EQ, refDataIdValue);
        ParameterValue dtIdValue = MessageUtil.buildParameterValue(ParameterValueType.STRING, dtId);
        QueryItem dtIdItem = MessageUtil.buildQueryItem(String.valueOf(AS_DTID_ID), Operation.EQ, dtIdValue);
        SelectRequest selectRequest = MessageUtil.buildSelectRequest(ATOMIC_SENSOR_ENTITY_TYPE, Arrays.asList(refDataIdItem, dtIdItem));
    }
}
