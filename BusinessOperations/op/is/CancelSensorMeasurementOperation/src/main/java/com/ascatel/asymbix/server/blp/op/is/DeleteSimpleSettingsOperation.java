/**
 * Class DeleteSimpleSettingsOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 19.04.13
 */
package com.ascatel.asymbix.server.blp.op.is;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;

import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;

/**
 * Внутренняя операция.
 * Удаляет соответствующие SourceId и настройки если не найден 'следующий' по номеру объект класса Atomic Sensor.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class DeleteSimpleSettingsOperation extends AbstractOperation {

    public DeleteSimpleSettingsOperation(AbstractOperation handler) {
        super(handler);
    }

    public void deleteSimpleSettings(String sid)
            throws BusinessLogicServiceException {
        // Удаляем из настроек (класс Simple Settings) все настройки с Key = SRC-<SId>
        // (таким образом прекращён приём сообщений с через сервер коммуникации).
        deleteSRCSettings(sid);

        // Удаляем из настроек (класс Simple Settings) все настройки с Key = COID-<SId>
        // (таким образом прекращён Sid удалён из настроек маппинга CO-SourceId).
        deleteCOIDSettings(sid);

        // Удаляем соответствующий объект SourceId.
        deleteSourceId(sid);
    }

    private void deleteSourceId(String sid)
            throws BusinessLogicServiceException {
        final DeleteRequest deleteRequest = buildDeleteRequest(getBlockId(), SID_ENTITY_TYPE, sid);
        getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());

    }

    private void deleteCOIDSettings(String sid)
            throws BusinessLogicServiceException {
        final String key = new StringBuilder().append("COID-").append(sid).toString();
        deleteSettings(key);
    }

    private void deleteSRCSettings(String sid)
            throws BusinessLogicServiceException {
        final String key = new StringBuilder().append("SRC-").append(sid).toString();
        deleteSettings(key);
    }


    private void deleteSettings(String settingKey)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(SIMPLE_SETTING_KEY_PARAMETER_ID, Operation.EQ, buildParameterValue(settingKey)));
        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Collection<String> settingIds = new HashSet<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            settingIds.add(parameter.getEntityId());
        }

        for (String settingId : settingIds) {
            final DeleteRequest deleteRequest = buildDeleteRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE, settingId);
            getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
        }
    }

}
