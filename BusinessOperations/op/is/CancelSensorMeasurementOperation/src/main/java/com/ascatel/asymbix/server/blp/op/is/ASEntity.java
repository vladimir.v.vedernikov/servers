/**
 * Class ASEntity
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 19.04.13
 */
package com.ascatel.asymbix.server.blp.op.is;

/**
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class ASEntity implements Comparable<ASEntity> {

    private String entityId;

    private double number;

    private String sid;

    private String isId;

    private String cpId;

    public ASEntity(String entityId) {
        this.entityId = entityId;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getIsId() {
        return isId;
    }

    public void setIsId(String iSid) {
        this.isId = iSid;
    }

    public String getCpId() {
        return cpId;
    }

    public void setCpId(String cpId) {
        this.cpId = cpId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ASEntity)) return false;

        ASEntity asEntity = (ASEntity) o;

        if (!entityId.equals(asEntity.entityId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return entityId.hashCode();
    }

    @Override
    public int compareTo(ASEntity o) {
        return Double.compare(number, o.getNumber());
    }
}
