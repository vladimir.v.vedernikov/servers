/**
 * Class DeleteAtomicSensorOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 22.04.13
 */
package com.ascatel.asymbix.server.blp.op.is;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;

import java.util.*;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.DeleteRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;

import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Внутренняя операция.
 * Удаляем логический датчик ASid и связанные с ним настройки.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class DeleteAtomicSensorOperation extends AbstractOperation {

    public DeleteAtomicSensorOperation(AbstractOperation handler) {
        super(handler);
    }

    public void deleteAS(String asId)
            throws BusinessLogicServiceException {
        // Находим связанные с данным ASid объекты класса Controlled Parameter Processing Setting Value.
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(CPPSV_BIND_AS_PARAMETER_ID, QueryItem.Operation.EQ, buildParameterValue(asId)));
        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Collection<CPPSVEntity> cppsvEntities = parseResultSetAsCPPSVEntity(resultSet);

        // Идём по всем сущностям, найденным на предыдущем шаге и для каждой из них :
        //   1.	Считываем значение атрибута BindSimpSet объекта CPPSVid1
        //   2. Идём по полученному набору объектов класса SimpleSettings и удаляем его.
        //   3. Вызываем операцию Удалить сущность. Передаём в неё в качестве входного параметра CPPSVid1.
        for (CPPSVEntity cppsvEntity : cppsvEntities) {
            final List<String> simpSetIds = cppsvEntity.getSimpSetList();

            for (String settingId : simpSetIds) {
                final DeleteRequest deleteRequest
                        = buildDeleteRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE, settingId);
                getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
            }

            final DeleteRequest deleteRequest
                    = buildDeleteRequest(getBlockId(), CPPSV_ENTITY_TYPE, cppsvEntity.getCppsvId());
            getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
        }

        // Вызываем операцию Удалить сущность. Передаём в неё в качестве входного параметра ASid.
        final DeleteRequest deleteRequest
                = buildDeleteRequest(getBlockId(), AS_ENTITY_TYPE, asId);
        getEnv().getBlService().deleteEntity(deleteRequest.toByteArray());
    }

    private Collection<CPPSVEntity> parseResultSetAsCPPSVEntity(ResultSet resultSet)
            throws BusinessLogicServiceException {
        final Map<String, CPPSVEntity> cppsvEntities = new HashMap<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            final String entityId = parameter.getEntityId();
            CPPSVEntity entity = cppsvEntities.get(entityId);
            if (entity == null) {
                entity = new CPPSVEntity(entityId);
                cppsvEntities.put(entityId, entity);
            }

            final String attrId = parameter.getAttributeId();
            if (CPPSV_BIND_SIMP_SET_PARAMETER_ID.equals(attrId)) {
                final ParameterValue bindSIDValue = parameter.getValue();
                final List<String> simpSetList = extractObjectLinks(bindSIDValue);
                entity.setSimpSetList(simpSetList);
            }
        }

        return cppsvEntities.values();
    }

    private class CPPSVEntity {

        private String cppsvId;

        private List<String> simpSetList;

        private CPPSVEntity(String cppsvId) {
            this.cppsvId = cppsvId;
        }

        public String getCppsvId() {
            return cppsvId;
        }

        public List<String> getSimpSetList() {
            return simpSetList;
        }

        public void setSimpSetList(List<String> simpSetList) {
            this.simpSetList = simpSetList;
        }
    }

}
