/**
 * Class UpdateSensorSettings
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 19.04.13
 */
package com.ascatel.asymbix.server.blp.op.is;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.*;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.UpdateRequest;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.*;

/**
 * Внутренняя операция.
 * Обновляет настройки в случае, когда найден 'следующий' по номеру объект класса Atomic Sensor его индекс, которого
 * нужно понизить.
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
class UpdateSensorSettingsOperation extends AbstractOperation {

    private Map<String, String> cppsNameCache;

    public UpdateSensorSettingsOperation(AbstractOperation handler) {
        super(handler);
    }

    public void updateSensorSettings(ASEntity asEntity, String asId2)
            throws BusinessLogicServiceException {

        final double number = asEntity.getNumber();
        if (number == 1.0) {
            // В этом случае нужно глобальные и не зависящие от датчика настройки «перевесить» на ASid2.
            applyGlobalSettingsToAS(asEntity, asId2);
        } else {
            // Нужно обновить все ключи в настройках серверов (key/value) (т.к. например, 3-й датчик теперь
            // становится 2-м)
            remapSettingsToAS(asEntity, asId2);
        }
    }

    private void applyGlobalSettingsToAS(ASEntity asEntity, String asId2)
            throws BusinessLogicServiceException {
        // Выбираем все объекты типа Controlled Parameter Processing Setting Value, связанные с удаляемым объектом ASid
        // и глобальными либо не зависящими от датчика настройками.
        final List<QueryItem> filter = new ArrayList<>();

        final String asId = asEntity.getEntityId();
        // Выберем все cpps у которых CPPS_IS_GLOBAL_PARAMETER_ID или CPPS_IS_SENSINDEP_PARAMETER_ID == true
        final Collection<String> cppsIds = selectGlobalAndSensindepCPPS();

        filter.add(buildQueryItem(CPPSV_BIND_AS_PARAMETER_ID, Operation.EQ, buildParameterValue(asId)));
        filter.add(buildQueryItem(CPPSV_BIND_CPPS_PARAMETER_ID, Operation.IN, buildParameterValue(cppsIds)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Collection<String> cppsvIds = new HashSet<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            cppsvIds.add(parameter.getEntityId());
        }

        // Идём по полученному на предыдущем шаге набору и для каждого его элемента устанавливаем значение
        // атрибута BindAS = ASid2.
        final ParameterValue newBindAs = buildParameterValue(buildObjectLink(Arrays.asList(asId2)));
        for (String cppsvId : cppsvIds) {
            final UpdateRequest update = buildUpdateRequest(getBlockId(),
                    CPPSV_ENTITY_TYPE, cppsvId, CPPSV_BIND_AS_PARAMETER_ID, newBindAs);
            getEnv().getBlService().setParameterValue(update.toByteArray());
        }
    }

    private void remapSettingsToAS(ASEntity asEntity, String asId2)
            throws BusinessLogicServiceException {
        // Находим все связанные с данным ASid2 объекты класса Controlled Parameter Processing Setting Value
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(CPPSV_BIND_AS_PARAMETER_ID, Operation.EQ, buildParameterValue(asId2)));
        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), CPPSV_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final Collection<CPPSVEntity> cppsvEntities = parseResultSetAsCPPSVEntity(resultSet);
        final String s1 = asEntity.getSid();
        final String n = Double.toString(asEntity.getNumber());
        for (CPPSVEntity cppsvEntity : cppsvEntities) {

            final String sName = readCppsName(cppsvEntity.getCppsId());
            final List<String> simpSetList = cppsvEntity.getSimpSetList();
            // Идём по полученному набору объектов класса SimpleSettings и для каждого из них устанавливаем значение
            // атрибута key = <S1>-<sName>-<N>
            final String key = s1 + "-" + sName + "-" + n;
            for (String simpleSettingId : simpSetList) {
                final UpdateRequest update = buildUpdateRequest(getBlockId(), SIMPLE_SETTING_ENTITY_TYPE,
                        simpleSettingId, SIMPLE_SETTING_KEY_PARAMETER_ID, buildParameterValue(key));
                getEnv().getBlService().setParameterValue(update.toByteArray());
            }
        }

        // Устанавливаем для сущности ASid2 значение атрибута Number = N
        final UpdateRequest updateAs = buildUpdateRequest(getBlockId(), AS_ENTITY_TYPE,
                asId2, AS_NUMBER_PARAMETER_ID, buildParameterValue(asEntity.getNumber()));
        getEnv().getBlService().setParameterValue(updateAs.toByteArray());
    }

    private String readCppsName(String cppsId)
            throws BusinessLogicServiceException {

        if (cppsNameCache == null) {
            cppsNameCache = new HashMap<>();
        }

        String name = cppsNameCache.get(cppsId);
        if (name != null) {
            return name;
        }

        final ParameterValue nameValue = readParameterValue(CPPS_ENTITY_TYPE, cppsId, CPPS_NAME_PARAMETER_ID);
        name = nameValue.getStringValue();
        cppsNameCache.put(cppsId, name);
        return name;
    }

    private Collection<String> selectGlobalAndSensindepCPPS()
            throws BusinessLogicServiceException {
        final Set<String> result = new HashSet<>();

        final List<QueryItem> globalFilter = new ArrayList<>();
        globalFilter.add(buildQueryItem(CPPS_IS_GLOBAL_PARAMETER_ID, Operation.EQ, buildParameterValue(true)));
        final SelectRequest globalRequest
                = buildSelectRequest(getBlockId(), getLocale(), CPPS_ENTITY_TYPE, globalFilter);
        final ResultSet globalResultSet = selectEntities(globalRequest);
        for (Parameter parameter : globalResultSet.getParametersList()) {
            result.add(parameter.getEntityId());
        }

        final List<QueryItem> sensIndepFilter = new ArrayList<>();
        sensIndepFilter.add(buildQueryItem(CPPS_IS_SENSINDEP_PARAMETER_ID, Operation.EQ, buildParameterValue(true)));
        final SelectRequest sensRequest
                = buildSelectRequest(getBlockId(), getLocale(), CPPS_ENTITY_TYPE, sensIndepFilter);
        final ResultSet sensResultSet = selectEntities(sensRequest);
        for (Parameter parameter : sensResultSet.getParametersList()) {
            result.add(parameter.getEntityId());
        }

        return result;
    }

    private Collection<CPPSVEntity> parseResultSetAsCPPSVEntity(ResultSet resultSet)
            throws BusinessLogicServiceException {
        final Map<String, CPPSVEntity> cppsvEntities = new HashMap<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            final String entityId = parameter.getEntityId();
            CPPSVEntity entity = cppsvEntities.get(entityId);
            if (entity == null) {
                entity = new CPPSVEntity(entityId);
                cppsvEntities.put(entityId, entity);
            }

            final String attrId = parameter.getAttributeId();
            if (CPPSV_BIND_CPPS_PARAMETER_ID.equals(attrId)) {
                final ParameterValue bindCPPSValue = parameter.getValue();
                final String bindCPPSId = extractObjectLink(bindCPPSValue);
                entity.setCppsId(bindCPPSId);
            } else if (CPPSV_BIND_SIMP_SET_PARAMETER_ID.equals(attrId)) {
                final ParameterValue bindSIDValue = parameter.getValue();
                final List<String> simpSetList = MessageUtil.extractObjectLinks(bindSIDValue);
                entity.setSimpSetList(simpSetList);
            }
        }

        return cppsvEntities.values();
    }

    private String extractObjectLink(ParameterValue value)
            throws BusinessLogicServiceException {
        final String link = MessageUtil.extractObjectLink(value);
        if (link == null) {
            throw new BusinessLogicServiceException("Object link does not have any links");
        }

        return link;
    }

    private class CPPSVEntity {

        private String cppsvId;

        private String cppsId;

        private List<String> simpSetList;

        private CPPSVEntity(String cppsvId) {
            this.cppsvId = cppsvId;
        }

        public String getCppsvId() {
            return cppsvId;
        }

        public String getCppsId() {
            return cppsId;
        }

        public void setCppsId(String cppsId) {
            this.cppsId = cppsId;
        }

        public List<String> getSimpSetList() {
            return simpSetList;
        }

        public void setSimpSetList(List<String> simpSetList) {
            this.simpSetList = simpSetList;
        }
    }
}
