/**
 * Class CancelSensorMeasurementOperation
 * Copyright© 2012 Ascatel Inc.. All rights reserved.
 * For internal use only.
 *
 * Author: Sergey.Sitishev
 * Version: 1.0, 18.04.13
 */
package com.ascatel.asymbix.server.blp.op.is;

import com.ascatel.asymbix.server.blp.op.AbstractOperation;
import com.ascatel.asymbix.server.blp.util.MessageUtil;
import ru.omnicomm.pegasus.processingPlatform.services.base.BusinessLogicServiceException;

import java.util.*;

import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.Parameter;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ParameterValue;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.QueryItem.Operation;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.SelectRequest;
import com.ascatel.asymbix.server.blp.messaging.BLSMessageParser.ResultSet;

import static com.ascatel.asymbix.server.blp.OMIdentifiers.*;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildParameterValue;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildQueryItem;
import static com.ascatel.asymbix.server.blp.util.MessageUtil.buildSelectRequest;

/**
 * Операция 'Снять параметр измерения для датчика'.
 * Данная операция отменяет измерение датчиком ранее заданного контролируемого параметра.
 * <p/>
 * Входные параметры:
 * <ul>
 * <li>{@code isId} - идентификатор датчика (сущности класса Installed Sensor), с которого снимается контролируемый
 * параметр.</li>
 * <li>{@code cpId} – идентификатор контролируемого параметра (сущности класса Controlled Parameter),
 * измерение которого датчиком отменяется.</li>
 * </ul>
 *
 * @author Sergey.Sitishev (sitishev@ascatel.com)
 */
public class CancelSensorMeasurementOperation extends AbstractOperation {

    /**
     * Значение типа стандартного датчика.
     */
    private static final int STANDARD_SENSOR = 1;

    public CancelSensorMeasurementOperation(AbstractOperation handler) {
        super(handler);
    }

    public void cancelSensorMeasurement(String isId, String cpId)
            throws BusinessLogicServiceException {

        // Проверяем, что датчик НЕ является исполнительным устройством.
        checkSensorNotUnitExecution(isId);

        // Выбираем все сущности типа Atomic Sensor.
        final List<ASEntity> asEntities = selectASEntities(isId, cpId);
        for (ASEntity asEntity : asEntities) {
            processEntity(asEntity);
        }
    }

    private void processEntity(ASEntity asEntity)
            throws BusinessLogicServiceException {
        // Ищем "следующий" по номеру объект класса Atomic Sensor: его индекс нужно понизить.
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(AS_BIND_SOURCE_PARAMETER_ID, Operation.EQ, buildParameterValue(asEntity.getSid())));
        filter.add(buildQueryItem(AS_BIND_CP_PARAMETER_ID, Operation.EQ, buildParameterValue(asEntity.getCpId())));
        filter.add(buildQueryItem(AS_NUMBER_PARAMETER_ID, Operation.EQ, buildParameterValue(asEntity.getNumber() + 1)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), AS_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        final List<Parameter> params = resultSet.getParametersList();
        if (!params.isEmpty()) {
            // Сущность найдена, обновляем настройки.
            final String asId2 = params.get(0).getEntityId();
            updateSettings(asEntity, asId2);
        } else {
            // сущность ASid2 не найдена, это означает, что удаляется последний логический датчик для данного
            // контролируемого параметра и надо также удалить соответствующий SourceId.
            final String sid = asEntity.getSid();
            deleteSimpleSettings(sid);
        }

        // Удаляем логический датчик ASid и связанные с ним настройки.
        deleteAtomicSensor(asEntity.getEntityId());

        // Отменяем регистрацию датчика в слое подключений.
        unregisterSensorConnectioLayer();
    }

    private void unregisterSensorConnectioLayer() {
        // Не ясен ряд моментов в аналитической документации...
        //TODO IMPLEMENTS
    }

    private void deleteAtomicSensor(String asId)
            throws BusinessLogicServiceException {
        final DeleteAtomicSensorOperation deleteASOperation = new DeleteAtomicSensorOperation(this);
        deleteASOperation.deleteAS(asId);
    }

    private void deleteSimpleSettings(String sid)
            throws BusinessLogicServiceException {
        final DeleteSimpleSettingsOperation handler = new DeleteSimpleSettingsOperation(this);
        handler.deleteSimpleSettings(sid);
    }

    private void updateSettings(ASEntity asEntity, String asId2)
            throws BusinessLogicServiceException {
        final UpdateSensorSettingsOperation handler = new UpdateSensorSettingsOperation(this);
        handler.updateSensorSettings(asEntity, asId2);
    }

    private List<ASEntity> selectASEntities(String isId, String cpId)
            throws BusinessLogicServiceException {
        final List<QueryItem> filter = new ArrayList<>();
        filter.add(buildQueryItem(AS_BIND_IS_PARAMETER_ID, QueryItem.Operation.EQ, buildParameterValue(isId)));
        filter.add(buildQueryItem(AS_BIND_CP_PARAMETER_ID, QueryItem.Operation.EQ, buildParameterValue(cpId)));

        final SelectRequest request = buildSelectRequest(getBlockId(), getLocale(), AS_ENTITY_TYPE, filter);
        final ResultSet resultSet = selectEntities(request);

        return parseResultSetAsASEntity(isId, cpId, resultSet);
    }

    private List<ASEntity> parseResultSetAsASEntity(String isId, String cpId, ResultSet resultSet)
            throws BusinessLogicServiceException {
        final Map<String, ASEntity> asEntities = new HashMap<>();
        for (Parameter parameter : resultSet.getParametersList()) {
            final String entityId = parameter.getEntityId();
            ASEntity entity = asEntities.get(entityId);
            if (entity == null) {
                entity = new ASEntity(entityId);
                entity.setIsId(isId);
                entity.setCpId(cpId);
                asEntities.put(entityId, entity);
            }

            final String attrId = parameter.getAttributeId();
            if (AS_NUMBER_PARAMETER_ID.equals(attrId)) {
                final ParameterValue numberValue = parameter.getValue();
                entity.setNumber(numberValue.getDoubleValue());
            } else if (AS_BIND_SOURCE_PARAMETER_ID.equals(attrId)) {
                final ParameterValue bindSIDValue = parameter.getValue();
                final String sid = extractObjectLink(bindSIDValue);
                entity.setSid(sid);
            }
        }

        // Сортируем по убыванию атрибута Number.
        final List<ASEntity> result = new ArrayList<>(asEntities.values());
        Collections.sort(result);
        return result;
    }

    private void checkSensorNotUnitExecution(String isId)
            throws BusinessLogicServiceException {
        // Определяем тип дерегистрируемого датчика.
        final int sensorMode = readSensorMode(isId);
        if (STANDARD_SENSOR != sensorMode) {
            throw new BusinessLogicServiceException("Operation works only for standard sensors");
        }
    }

    private int readSensorMode(String isId)
            throws BusinessLogicServiceException {
        final ParameterValue bindSTValue = readParameterValue(IS_ENTITY_TYPE, isId, IS_BIND_ST_PARAMETER_ID);
        if (!bindSTValue.hasObjectLinkValue()) {
            final String errMsg = new StringBuilder()
                    .append("Parameter IS_BIND_ST_PARAMETER_ID has no value for entityId=")
                    .append(isId).toString();
            throw new BusinessLogicServiceException(errMsg);
        }

        final List<String> linksList = bindSTValue.getObjectLinkValue().getLinksList();
        if (linksList.isEmpty()) {
            final String errMsg = new StringBuilder()
                    .append("Parameter IS_BIND_ST_PARAMETER_ID has no value for entityId=")
                    .append(isId).toString();
            throw new BusinessLogicServiceException(errMsg);
        }

        final String stEntitId = linksList.get(0);
        final ParameterValue stModeValue = readParameterValue(ST_ENTITY_TYPE, stEntitId, ST_SENSOR_MODE_PARAMETER_ID);
        return stModeValue.getIntValue();
    }

    private String extractObjectLink(ParameterValue value)
            throws BusinessLogicServiceException {
        final String link = MessageUtil.extractObjectLink(value);
        if (link == null) {
            throw new BusinessLogicServiceException("Object link does not have any links");
        }

        return link;
    }

}
